import { faPause, faPlay } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import _ from 'lodash';
import React, { useEffect } from 'react';
import { Col, Form, ProgressBar, Row } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

const EMPTY_IMAGE =
  'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

const fetchBase64Data = (url: string) =>
  fetch(url)
    .then((response) => response.blob())
    .then(
      (blob) =>
        new Promise<string>((resolve, reject) => {
          const reader = new FileReader();
          reader.onloadend = () =>
            resolve(reader.result?.toString() || EMPTY_IMAGE);
          reader.onerror = reject;
          reader.readAsDataURL(blob);
        }),
    )
    .catch(() => undefined);

export function MoviePlayer({
  images,
  labels,
}: {
  images: string[];
  labels?: string[];
}) {
  const [currentImageCursor, setCurrentImageCursor] = React.useState(0);
  const [play, setPlay] = React.useState(false);
  const [frameRate, setFrameRate] = React.useState(10);
  const [loaded, setLoaded] = React.useState(
    _.range(0, images.length).map((i) => false),
  );
  const [imagesData, setImagesData] = React.useState<(string | undefined)[]>(
    _.range(0, images.length).map((i) => undefined),
  );

  const currentImageIndex = Math.round(currentImageCursor);
  const currentImageData = imagesData[currentImageIndex];
  const currentLabel = labels?.[currentImageIndex];

  useEffect(() => {
    setLoaded(_.range(0, images.length).map((i) => false));
    setImagesData(_.range(0, images.length).map((i) => undefined));
    images.forEach((image, i) => {
      fetchBase64Data(image).then((imageData) => {
        setLoaded((previousLoaded) => {
          if (previousLoaded[i]) {
            return previousLoaded;
          }
          const newLoaded = previousLoaded.slice();
          newLoaded[i] = true;
          return newLoaded;
        });
        setImagesData((previousImagesData) => {
          const newDataUrls = previousImagesData.slice();
          newDataUrls[i] = imageData;
          return newDataUrls;
        });
      });
    });
  }, [images]);

  React.useEffect(() => {
    if (play) {
      const refreshFreq = 30; // 30 fps;
      const skipImages = frameRate / refreshFreq;
      const interval = setInterval(() => {
        setCurrentImageCursor((c) => (c + skipImages) % (images.length - 0.5));
      }, 1000 / refreshFreq);
      return () => clearInterval(interval);
    }
    return () => undefined;
  }, [frameRate, images, play]);

  const nbLoaded = loaded.reduce((acc, cur) => acc + (cur ? 1 : 0), 0);
  const nbTotal = loaded.length;
  const allLoaded = nbLoaded === nbTotal;

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div
        style={{
          background: 'black',
          display: 'flex',
          justifyContent: 'center',
          aspectRatio: '2/1',
          maxWidth: '100%',
          alignItems: 'center',
          position: 'relative',
        }}
      >
        {!allLoaded && (
          <div
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              zIndex: 2,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <ProgressBar
              style={{ width: '80%' }}
              variant="info"
              animated
              now={(nbLoaded / nbTotal) * 100}
              label={`Loading images ${nbLoaded}/${nbTotal}`}
            />
          </div>
        )}
        {loaded[currentImageIndex] &&
          (currentImageData ? (
            <img
              style={{
                objectFit: 'contain',
                overflow: 'auto',
                width: '100%',
                height: '100%',
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 1,
              }}
              src={currentImageData}
              alt="series"
            />
          ) : (
            <div
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                zIndex: 1,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  gap: '0.5rem',
                }}
              >
                <div
                  style={{
                    fontSize: '2rem',
                    fontWeight: 'bold',
                    color: 'white',
                  }}
                >
                  Could not load image
                </div>
                <i
                  style={{
                    fontSize: '1rem',
                    fontWeight: 'bold',
                    color: 'white',
                  }}
                >
                  {currentImageIndex + 1}/{images.length}
                </i>
              </div>
            </div>
          ))}
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: '#fafafa',
          padding: 5,
          borderBottom: '1px solid lightgray',
          borderLeft: '1px solid lightgray',
          borderRight: '1px solid lightgray',
        }}
      >
        <Form.Range
          disabled={!allLoaded}
          value={currentImageCursor}
          min={0}
          max={images.length - 1}
          onChange={(e) => {
            setPlay(false);
            setCurrentImageCursor(Number(e.target.value));
          }}
        />
        <Row
          style={{
            alignItems: 'center',
          }}
        >
          <Col xs={'auto'}>
            <Button
              disabled={!allLoaded}
              size={'sm'}
              variant={'link'}
              onClick={() => setPlay((p) => !p)}
            >
              <FontAwesomeIcon
                style={{ width: 25 }}
                size="2x"
                icon={play ? faPause : faPlay}
              />
            </Button>
          </Col>

          <Col xs={'auto'} className="p-1">
            <strong>Speed: </strong>
          </Col>
          <Col xs={'auto'} className="p-1">
            <Form.Control
              disabled={!allLoaded}
              type="number"
              size={'sm'}
              value={frameRate}
              className="text-center"
              style={{
                width: `${frameRate.toString().length + 5}ch`,
              }}
              onChange={(e) => setFrameRate(Number(e.target.value))}
            />
          </Col>
          <Col xs={'auto'} className="p-1">
            <strong>frames per second</strong>
          </Col>

          <Col
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
            }}
          >
            {currentLabel && <div>{currentLabel}</div>}
          </Col>
        </Row>
      </div>
    </div>
  );
}
