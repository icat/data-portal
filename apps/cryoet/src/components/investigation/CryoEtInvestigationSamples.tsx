import { LazyWrapper, Loading } from '@edata-portal/core';
import {
  type Sample,
  useGetEndpoint,
  DATASET_LIST_ENDPOINT,
  Investigation,
  useEndpointURL,
  DOWNLOAD_FILE_ENDPOINT,
  Dataset,
  DATASET_TYPE_ACQUISITION,
} from '@edata-portal/icat-plus-api';
import { ZoomableImage } from '@edata-portal/core';
import { getGalleryItemByName } from '@edata-portal/core/src/helpers/gallery';
import { Col, Row, Card, Button, Alert } from 'react-bootstrap';

export function CryoEtInvestigationSamples({
  samples,
  investigation,
  grid,
  setSample,
}: {
  samples: {
    grid: string;
    location: string;
    sample: Sample;
  }[];
  investigation: Investigation;
  grid: string;
  setSample: (sample: string) => void;
}) {
  return (
    <Row>
      <Col>
        <Card>
          <Card.Header>
            <strong>{grid}</strong>
          </Card.Header>
          <Card.Body>
            <Row>
              {samples.map((sample) => {
                return (
                  <CryoEtSampleButton
                    key={sample.sample.id}
                    sample={sample}
                    investigation={investigation}
                    setSample={() => {
                      setSample(String(sample.sample.id));
                    }}
                  />
                );
              })}
            </Row>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export function CryoEtSampleButton({
  sample,
  setSample,
  investigation,
}: {
  sample: {
    grid: string;
    location: string;
    sample: Sample;
  };
  setSample: (sample: Sample) => void;
  investigation: Investigation;
}) {
  return (
    <Col xs={12} sm={6} md={4} lg={3} xxl={2} key={sample.sample.id}>
      <Card
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          margin: 10,
        }}
      >
        <LazyWrapper aspectRatio="10:7" placeholder={<Loading />}>
          <CryoEtSampleSnapshot sample={sample} investigation={investigation} />
        </LazyWrapper>
        <strong
          style={{
            margin: 5,
          }}
        >
          {sample.location || sample.sample.name}
        </strong>
        {!sample.location && (
          <Alert
            style={{
              margin: 0,
              padding: 5,
            }}
            variant="danger"
          >
            Unknown location
          </Alert>
        )}
        <Button
          style={{
            margin: 5,
          }}
          onClick={() => {
            setSample(sample.sample);
          }}
          size={'sm'}
        >
          Open
        </Button>
      </Card>
    </Col>
  );
}

function CryoEtSampleSnapshot({
  sample,
  investigation,
}: {
  sample: {
    grid: string;
    location: string;
    sample: Sample;
  };
  investigation: Investigation;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: investigation.id.toString(),
      datasetType: DATASET_TYPE_ACQUISITION,
      sampleId: sample.sample.id,
      limit: 1,
      parameters: 'ResourcesGalleryFilePaths~like~search',
    },
    default: [] as Dataset[],
  });

  const url = useEndpointURL(DOWNLOAD_FILE_ENDPOINT);

  const resourceId = datasets
    .map((dataset) => getGalleryItemByName(dataset, 'search'))
    .find((resourceId) => resourceId !== undefined);

  if (resourceId === undefined) {
    return null;
  }

  return (
    <ZoomableImage
      src={url({
        resourceId: resourceId?.id,
      })}
      alt={`${sample.sample.name} snapshot`}
      showCenter
    />
  );
}
