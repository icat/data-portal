import { getDatasetParamValue } from '@edata-portal/core';
import {
  Dataset,
  DATASET_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { Col, Row } from 'react-bootstrap';
import { CryoETProcessedDataset } from 'components/dataset/cryo-et/processed/CryoETProcessedDataset';

export function CryoETProcessedDatasets({ dataset }: { dataset: Dataset }) {
  const outputIds =
    getDatasetParamValue(dataset, 'output_datasetIds')?.split(' ') || [];

  const nested_outputs = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: outputIds.join(','),
      nested: true,
    },
    default: [] as Dataset[],
    skipFetch: !outputIds.length,
  });

  const flattenedOutputs = nested_outputs.reduce(
    (acc, dataset) => [...acc, dataset, ...(dataset.outputDatasets || [])],
    [] as Dataset[],
  );

  return (
    <Row className="g-1">
      {flattenedOutputs.map((output) => (
        <Col key={output.id} xs={12} lg={6}>
          <CryoETProcessedDataset dataset={output} />
        </Col>
      ))}
    </Row>
  );
}
