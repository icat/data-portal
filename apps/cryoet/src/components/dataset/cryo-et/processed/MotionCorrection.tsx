import { Gallery, DatasetParameters } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { Card, Col, Container, Row } from 'react-bootstrap';

export function MotionCorrectionInfo({ dataset }: { dataset: Dataset }) {
  return (
    <Card style={{ padding: '0.5rem' }}>
      <Container fluid>
        <Col>
          <Row>
            <h4 className="text-center">Motion correction</h4>
          </Row>
          <Gallery dataset={dataset} />
          <DatasetParameters
            filterPrefix="EMMotionCorrection_"
            formatName={true}
            dataset={dataset}
          />
        </Col>
      </Container>
    </Card>
  );
}
