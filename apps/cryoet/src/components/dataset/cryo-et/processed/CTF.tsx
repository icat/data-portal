import { Gallery, DatasetParameters } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { Card, Col, Container, Row } from 'react-bootstrap';

export function CTFInfo({ dataset }: { dataset: Dataset }) {
  return (
    <Card style={{ padding: '0.5rem' }}>
      <Container fluid>
        <Col>
          <Row>
            <h4 className="text-center">CTF</h4>
          </Row>
          <Gallery dataset={dataset} />
          <DatasetParameters
            filterPrefix="EMCTF_"
            formatName={true}
            dataset={dataset}
          />
        </Col>
      </Container>
    </Card>
  );
}
