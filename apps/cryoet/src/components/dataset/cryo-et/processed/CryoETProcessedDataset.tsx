import { isAcquisition } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { CTFInfo } from 'components/dataset/cryo-et/processed/CTF';
import { MotionCorrectionInfo } from 'components/dataset/cryo-et/processed/MotionCorrection';

export function CryoETProcessedDataset({ dataset }: { dataset: Dataset }) {
  if (isAcquisition(dataset)) return <></>;
  if (dataset.location?.toLocaleLowerCase().includes('ctf')) {
    return <CTFInfo dataset={dataset} />;
  }
  if (dataset.location?.toLocaleLowerCase().includes('motioncor')) {
    return <MotionCorrectionInfo dataset={dataset} />;
  }

  return <></>;
}
