import { getDatasetParam } from '@edata-portal/core';
import {
  DATASET_LIST_ENDPOINT,
  Investigation,
  Sample,
  useGetEndpoint,
  DATASET_TYPE_ACQUISITION,
} from '@edata-portal/icat-plus-api';
import { ACQUISITION_CONSTANT_PARAMETERS } from 'constants/params';
import _ from 'lodash';
import { Card, Col, Container, Row } from 'react-bootstrap';

export function CryoEtConstantAcquisitionParameters({
  investigation,
  sample,
}: {
  investigation: Investigation;
  sample: Sample;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: investigation.id.toString(),
      datasetType: DATASET_TYPE_ACQUISITION,
      sortBy: 'STARTDATE',
      sortOrder: -1,
      search: '',
      sampleId: sample.id,
    },
  });

  const getParam = (param: string) => {
    return _(datasets)
      .map((v) => getDatasetParam(v, param))
      .filter((v) => v !== undefined)
      .first();
  };

  const params = ACQUISITION_CONSTANT_PARAMETERS.map(getParam).filter(
    (v) => v !== undefined,
  );

  if (_.isEmpty(params)) {
    return null;
  }

  return (
    <Container fluid>
      <Col>
        <Row>
          <Col>
            <strong>Constant acquisition parameters</strong>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card
              className="bg-light"
              style={{
                padding: '1rem',
              }}
            >
              <Row>
                {params.map((param) =>
                  param ? (
                    <Col
                      xs={'auto'}
                      key={param.name}
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginRight: '1rem',
                        gap: '5px',
                      }}
                    >
                      <strong>
                        {param.name.replaceAll('EM_', '').replaceAll('_', ' ')}
                      </strong>
                      :<span>{param.value}</span>
                      {param.units && param.units !== 'NA' ? (
                        <span>{param.units}</span>
                      ) : null}
                    </Col>
                  ) : null,
                )}
              </Row>
            </Card>
          </Col>
        </Row>
        <hr />
      </Col>
    </Container>
  );
}
