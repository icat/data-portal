import { DatasetList, Loading, useParam } from '@edata-portal/core';
import {
  DATASET_LIST_ENDPOINT,
  Investigation,
  useGetEndpoint,
  Sample,
  Dataset,
  DATASET_TYPE_ACQUISITION,
} from '@edata-portal/icat-plus-api';
import { Suspense, useMemo } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import Button from 'react-bootstrap/esm/Button';
import { AngleSelector } from 'components/dataset/cryo-et/tiltseries/AngleSelector';
import { getTiltAngleByDataset } from 'components/dataset/cryo-et/tiltseries/helper/Angles';
import { CryoEtConstantAcquisitionParameters } from 'components/dataset/cryo-et/tiltseries/ConstantAcquisitionParameters';
import _ from 'lodash';

export function CryoETTiltSeriesDatasets({
  investigation,
  sample,
}: {
  investigation: Investigation;
  sample: Sample;
}) {
  const [search, setSearch] = useParam<string>('search', '');

  const [angle, setAngle] = useParam<string>('angle', 'undefined');

  const [filterAngle, setFilterAngle] = useParam<'true' | 'false'>(
    'filterAngle',
    'false',
  );
  const filteringAngle = filterAngle === 'true';

  return (
    <Col>
      <Row>
        <CryoEtConstantAcquisitionParameters
          investigation={investigation}
          sample={sample}
        />
      </Row>
      <Row className="g-1">
        <Col xs={'auto'}>
          <Button
            onClick={() => {
              setFilterAngle(undefined);
              setSearch(undefined);
              setAngle(undefined);
            }}
            variant={!filteringAngle ? 'primary' : 'outline-primary'}
          >
            Show all
          </Button>
        </Col>
        <Col xs={'auto'}>
          <Button
            onClick={() => {
              setFilterAngle('true');
              setSearch(undefined);
            }}
            variant={filteringAngle ? 'primary' : 'outline-primary'}
          >
            Filter by angle
          </Button>
        </Col>
      </Row>
      <hr />
      <Row>
        {filterAngle === 'true' ? (
          <Suspense fallback={<Loading />}>
            <AngleSelector
              angle={angle}
              setAngle={setAngle}
              investigation={investigation}
              sample={sample}
            />
          </Suspense>
        ) : (
          <Col>
            <Form.Control
              type="text"
              placeholder="Search..."
              value={search}
              onChange={(e) => {
                setSearch(e.target.value);
              }}
              style={{ marginBottom: 20 }}
            />
          </Col>
        )}
      </Row>
      <Suspense fallback={<div>Loading datasets...</div>}>
        <CryoEtDatasets
          investigation={investigation}
          search={filteringAngle ? '' : search}
          angle={filteringAngle ? parseFloat(angle) : undefined}
          sample={sample}
        />
      </Suspense>
    </Col>
  );
}

function CryoEtDatasets({
  investigation,
  search,
  angle,
  sample,
}: {
  investigation: Investigation;
  search?: string;
  angle?: number;
  sample: Sample;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: investigation.id.toString(),
      datasetType: DATASET_TYPE_ACQUISITION,
      sortBy: 'STARTDATE',
      sortOrder: -1,
      search: search?.trim() || '',
      sampleId: sample.id,
    },
  });

  const filteredAngleList = useMemo(
    () =>
      angle !== undefined
        ? _(datasets)
            .filter((dataset: Dataset) => {
              return getTiltAngleByDataset(dataset) === angle;
            })
            .value()
        : _(datasets).value(),
    [angle, datasets],
  );

  return <DatasetList datasets={filteredAngleList} groupBy={'dataset'} />;
}
