import { immutableArray, isAcquisition } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

const ANGLE_PARAMETER_NAME = 'EM_tilt_angle';
/**
 * Given a dataset will return the tilt angle this dataset is associated with
 * In case there is no angle then it returns 0
 * @param dataset
 * @returns
 */
export function getTiltAngleByDataset(dataset: Dataset): number {
  const parameter = dataset.parameters.find(
    (p) => p.name === ANGLE_PARAMETER_NAME,
  );
  if (parameter && parameter.value) return parseFloat(parameter.value);
  return 0;
}

/**
 * Returns the angle step based on the first two positions of the tilt serioes
 * otherwise it returns 0
 * @param angles
 * @returns
 */
export function getAnglesStep(angles: number[]): number {
  if (angles && angles.length > 0) {
    return angles[1] - angles[0];
  }
  return 0;
}

/**
 * Returns an array containing the angles from a list of datasets
 * @param datasetes
 */
export function getAnglesByDatasetList(datasets: Dataset[]): number[] {
  return immutableArray(datasets)
    .filter(isAcquisition)
    .map((dataset) => getTiltAngleByDataset(dataset))
    .sort((a, b) => {
      return a - b;
    })
    .toArray();
}
