import {
  Loading,
  SideNavElement,
  SideNavFilter,
  useParam,
  WithSideNav,
} from '@edata-portal/core';
import type { Investigation, Sample } from '@edata-portal/icat-plus-api';
import { Suspense } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { CryoETTiltSeriesDatasets } from 'components/dataset/cryo-et/tiltseries/TiltSeriesDatasets';
import { CryoETTiltSeriesSummary } from 'components/dataset/cryo-et/tiltseries/TiltSeriesSummary';

export function CryoETTiltSeries({
  investigation,
  sample,
}: {
  investigation: Investigation;
  sample: Sample;
}) {
  const [viewAs, setViewAs] = useParam<'list' | 'summary'>('viewAs', 'list');

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label="Datasets">
          <Col>
            <SideNavFilter label={'View as'}>
              <Row className="g-1">
                <Col xs={'auto'}>
                  <Button
                    variant="outline-primary"
                    onClick={() => setViewAs('list')}
                    active={viewAs === 'list'}
                  >
                    List
                  </Button>
                </Col>
                <Col xs={'auto'}>
                  <Button
                    variant="outline-primary"
                    onClick={() => setViewAs('summary')}
                    active={viewAs === 'summary'}
                  >
                    Summary
                  </Button>
                </Col>
              </Row>
            </SideNavFilter>
          </Col>
        </SideNavElement>
      }
    >
      <Container fluid>
        {viewAs === 'summary' && (
          <Suspense fallback={<Loading />}>
            <CryoETTiltSeriesSummary
              investigation={investigation}
              sample={sample}
            />
          </Suspense>
        )}
        {viewAs === 'list' && (
          <Suspense fallback={<Loading />}>
            <CryoETTiltSeriesDatasets
              investigation={investigation}
              sample={sample}
            />
          </Suspense>
        )}
      </Container>
    </WithSideNav>
  );
}
