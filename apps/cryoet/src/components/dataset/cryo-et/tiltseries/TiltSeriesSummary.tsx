import { NoData } from '@edata-portal/core';
import {
  Dataset,
  Sample,
  DATASET_LIST_ENDPOINT,
  DOWNLOAD_FILE_ENDPOINT,
  Investigation,
  useEndpointURL,
  useGetEndpoint,
  DATASET_TYPE_ACQUISITION,
} from '@edata-portal/icat-plus-api';
import { Col, Row, Table } from 'react-bootstrap';
import { MoviePlayer } from 'components/movie/MoviePlayer';
import {
  getAnglesStep,
  getAnglesByDatasetList,
  getTiltAngleByDataset,
} from 'components/dataset/cryo-et/tiltseries/helper/Angles';
import _ from 'lodash';
import { useMemo } from 'react';
import { getGalleryItemByName } from '@edata-portal/core/src/helpers/gallery';

export function CryoETTiltSeriesSummary({
  investigation,
  sample,
}: {
  investigation: Investigation;
  sample: Sample;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: investigation.id.toString(),
      datasetType: DATASET_TYPE_ACQUISITION,
      sampleId: sample.id,
      sortBy: 'STARTDATE',
      sortOrder: -1,
    },
  });

  const nonEmptyDatasets = useMemo(() => datasets || [], [datasets]);

  const angles = useMemo(
    () => getAnglesByDatasetList(nonEmptyDatasets),
    [nonEmptyDatasets],
  );

  const range = angles.length
    ? `${angles[0]} to ${angles[angles.length - 1]}`
    : 'unknown';

  if (!nonEmptyDatasets.length) return <NoData />;

  return (
    <Col>
      <Row>
        <Col>
          <strong>Angles:</strong>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table
            striped
            bordered
            responsive
            style={{
              maxWidth: 500,
            }}
          >
            <thead>
              <tr>
                <th>total</th>
                <th>step</th>
                <th>range</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{angles.length}</td>
                <td>{getAnglesStep(angles)}</td>
                <td>{range}</td>
              </tr>
            </tbody>
          </Table>
        </Col>
      </Row>
      <TiltSeriesMovie datasets={nonEmptyDatasets} />
    </Col>
  );
}

function TiltSeriesMovie({ datasets }: { datasets: Dataset[] }) {
  const downloadUrl = useEndpointURL(DOWNLOAD_FILE_ENDPOINT);

  const images = _(datasets)
    .orderBy(getTiltAngleByDataset)
    .map((dataset) => getGalleryItemByName(dataset, 'fraction'))
    .map((ressource) => {
      return ressource ? downloadUrl({ resourceId: ressource.id }) : undefined;
    })
    .filter((url) => url !== undefined)
    .value() as string[];

  const labels = _(datasets)
    .orderBy(getTiltAngleByDataset)
    .map(
      (dataset) =>
        `Current angle: ${getTiltAngleByDataset(dataset).toString()}°`,
    )
    .value();

  if (images.length === 0) {
    return null;
  }

  return (
    <Col>
      <strong>Non-aligned micrographs movie</strong>
      <div style={{ maxWidth: 800 }}>
        <MoviePlayer images={images} labels={labels} />
      </div>
    </Col>
  );
}
