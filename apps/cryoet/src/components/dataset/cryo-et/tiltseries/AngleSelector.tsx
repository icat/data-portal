import { immutableArray } from '@edata-portal/core';
import {
  DATASET_LIST_ENDPOINT,
  Investigation,
  useGetEndpoint,
  DATASET_TYPE_ACQUISITION,
  type Sample,
} from '@edata-portal/icat-plus-api';
import { Col, Row, Form } from 'react-bootstrap';
import { getTiltAngleByDataset } from 'components/dataset/cryo-et/tiltseries/helper/Angles';

export function AngleSelector({
  angle,
  setAngle,
  investigation,
  sample,
}: {
  angle: string;
  setAngle: (angle: string) => void;
  investigation: Investigation;
  sample: Sample;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: investigation.id.toString(),
      datasetType: DATASET_TYPE_ACQUISITION,
      sortBy: 'STARTDATE',
      sortOrder: -1,
      search: '',
      sampleId: sample.id,
    },
  });

  const angles = immutableArray(datasets)
    .map(getTiltAngleByDataset)
    .sort((a, b) => a - b)
    .toArray();

  if (angle === 'undefined' && angles?.length) {
    setAngle(angles[0].toString());
  }

  const findClosestDataset = (angle: number) => {
    const closest = angles.reduce((prev, curr) =>
      Math.abs(curr - angle) < Math.abs(prev - angle) ? curr : prev,
    );
    setAngle(closest.toString());
  };

  const labelPosition = (angle: number) => {
    const position =
      ((angle - angles[0]) / (angles[angles.length - 1] - angles[0])) * 100;
    return position;
  };

  if (!angles?.length) return null;

  return (
    <Col>
      <Row>
        <Col xs={'auto'}>{angles[0]}</Col>
        <Col>
          <Form.Range
            min={angles[0]}
            max={angles[angles.length - 1]}
            value={angle || 0}
            onChange={(e) => findClosestDataset(parseFloat(e.target.value))}
          />
          <div
            style={{
              width: '100%',
              paddingRight: 5,
              paddingLeft: 5,
            }}
          >
            <div
              style={{
                position: 'relative',
                height: 30,
                width: '100%',
              }}
            >
              <span
                style={{
                  position: 'absolute',
                  top: -5,
                  left: `${labelPosition(parseFloat(angle) || 0)}%`,
                  transform: 'translate(-50%, 0)',
                  backgroundColor: '#fff',
                }}
              >
                {angle || 0}
              </span>
            </div>
          </div>
        </Col>
        <Col xs={'auto'}>{angles[angles.length - 1]}</Col>
      </Row>
    </Col>
  );
}
