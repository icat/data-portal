import {
  DatasetCard,
  Gallery,
  DatasetParameters,
  getDatasetParamValue,
  Loading,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { LazyWrapper } from '@edata-portal/core';
import { Col, Row, Container, Card } from 'react-bootstrap';
import { getTiltAngleByDataset } from 'components/dataset/cryo-et/tiltseries/helper/Angles';
import { ACQUISITION_CONSTANT_PARAMETERS } from 'constants/params';
import { CryoETProcessedDatasets } from 'components/dataset/cryo-et/processed/CryoEtProcessedDatasets';

export default function CryoETDatasetViewer({ dataset }: { dataset: Dataset }) {
  const title =
    getTiltAngleByDataset(dataset)?.toString() + '° ' + dataset.sampleName;

  const hasOutputs = getDatasetParamValue(dataset, 'output_datasetIds')?.length;

  return (
    <DatasetCard dataset={dataset} title={title}>
      <Row className="g-1 mt-2">
        <Col xs={12} xl={4}>
          <Card style={{ padding: '0.5rem' }}>
            <Container fluid>
              <Col>
                <Row>
                  <h4 className="text-center">Raw data</h4>
                </Row>
                <Gallery
                  dataset={dataset}
                  filter={(name) =>
                    name.toLocaleLowerCase().includes('fraction')
                  }
                />
                <DatasetParameters
                  filterPrefix="EM_"
                  formatName={true}
                  dataset={dataset}
                  exclude={ACQUISITION_CONSTANT_PARAMETERS}
                />
              </Col>
            </Container>
          </Card>
        </Col>
        {hasOutputs && (
          <Col xs={12} xl={8}>
            <LazyWrapper placeholder={<Loading />}>
              <CryoETProcessedDatasets dataset={dataset} />
            </LazyWrapper>
          </Col>
        )}
      </Row>
    </DatasetCard>
  );
}
