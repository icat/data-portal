import {
  SideNavElement,
  useParam,
  WithSideNav,
  NoData,
} from '@edata-portal/core';
import {
  Investigation,
  SAMPLE_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faBackward } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Container, Row, Button } from 'react-bootstrap';
import { CryoETTiltSeries } from 'components/dataset/cryo-et/tiltseries/TiltSeries';
import { CryoEtInvestigationSamples } from 'components/investigation/CryoEtInvestigationSamples';
import _ from 'lodash';
import ReactSelect from 'react-select';
import { useMemo } from 'react';

export default function CryoETInvestigationViewer({
  investigation,
}: {
  investigation: Investigation;
}) {
  const samples = useGetEndpoint({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: {
      investigationId: investigation.id,
      havingAcquisitionDatasets: true,
      sortBy: 'DATE',
    },
  });

  const grids = _(samples)
    .map((sample) => {
      const splitted = sample.name.split('/');
      const grid = splitted[0];
      const location = splitted[1];
      return { grid, location, sample };
    })
    .groupBy('grid')
    .value();

  const gridNames = Object.keys(grids).sort();

  const defaultGrid = gridNames.filter((gridName) => {
    return grids[gridName].some((sample) => sample.location);
  })[0];

  const [grid, setGrid] = useParam<string>(
    'grid',
    defaultGrid || 'undefined',
    true,
  );
  const [sample, setSample] = useParam<string>('sample', 'undefined', true);

  const gridSamples = useMemo(() => {
    return grids[grid] || [];
  }, [grids, grid]);

  const sampleObject =
    sample === 'undefined' || grid === 'undefined'
      ? undefined
      : _(grids[grid]).find((s) => String(s.sample.id) === sample);

  const gridSelector = (
    <ReactSelect
      options={gridNames.map((gridName) => {
        return {
          value: gridName,
          label: gridName,
        };
      })}
      value={{
        value: grid,
        label: grid,
      }}
      onChange={(e) => {
        setGrid(e ? e.value : undefined);
        setSample(undefined);
      }}
    />
  );

  if (!sample?.length || !gridNames?.length) return <NoData />;

  return (
    <WithSideNav
      sideNav={<SideNavElement label="Grid">{gridSelector}</SideNavElement>}
    >
      <Container fluid>
        <Row
          style={{
            marginBottom: 10,
          }}
          className={'g-2 align-items-center'}
        >
          <Col xs={'auto'}>
            <h5>
              <strong>Sample: </strong>
            </h5>
          </Col>
          <Col
            xs={'auto'}
            style={{
              minWidth: 200,
            }}
          >
            {gridSelector}
          </Col>
          {sampleObject !== undefined && (
            <Col
              xs={'auto'}
              style={{
                minWidth: 200,
              }}
            >
              <ReactSelect
                isClearable={true}
                options={gridSamples.map((s) => {
                  return {
                    value: s.sample.id,
                    label: s.location,
                  };
                })}
                value={{
                  value: sampleObject?.sample.id,
                  label: sampleObject?.location,
                }}
                onChange={(e) => {
                  setSample(e ? e.value?.toString() : undefined);
                }}
              />
            </Col>
          )}
        </Row>
        {sampleObject !== undefined && (
          <Row>
            <Col>
              <Button
                variant={'primary'}
                onClick={() => {
                  setSample(undefined);
                }}
                style={{ marginBottom: 10 }}
              >
                <FontAwesomeIcon
                  icon={faBackward}
                  style={{ marginRight: 10 }}
                />
                Back to grid
              </Button>
            </Col>
          </Row>
        )}
        <hr />
        <Row>
          {sampleObject === undefined ? (
            <CryoEtInvestigationSamples
              samples={gridSamples}
              investigation={investigation}
              grid={grid}
              setSample={setSample}
            />
          ) : (
            <CryoETTiltSeries
              sample={sampleObject.sample}
              investigation={investigation}
            />
          )}
        </Row>
      </Container>
    </WithSideNav>
  );
}
