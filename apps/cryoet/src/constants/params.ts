export const ACQUISITION_CONSTANT_PARAMETERS = [
  'EM_grid_name',
  'EM_protein_acronym',
  'EM_voltage',
  'EM_spherical_aberration',
  'EM_amplitude_contrast',
];
