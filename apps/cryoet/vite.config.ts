import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import federation from '@originjs/vite-plugin-federation';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      react(),
      tsconfigPaths(),
      federation({
        name: 'cryoet',
        filename: 'remoteEntry.js',
        exposes: {
          './CryoETInvestigationViewer':
            './src/viewers/CryoETInvestigationViewer',
          './CryoETDatasetViewer': './src/viewers/CryoETDatasetViewer',
        },
        shared: [
          'react',
          'react-dom',
          '@tanstack/react-query',
          'react-router-dom',
          '@edata-portal/core',
        ],
      }),
    ],
    server: {
      port: 3003,
      host: true,
    },
    build: {
      modulePreload: false,
      target: 'esnext',
      minify: mode === 'prod',
      cssCodeSplit: false,
    },
  };
});
