import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import federation from '@originjs/vite-plugin-federation';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      react(),
      tsconfigPaths(),
      federation({
        name: 'datasetViewer',
        filename: 'remoteEntry.js',
        exposes: {
          './HTXRPDDataset': './src/components/technique/htxrpd/HTXRPDDataset',
          './HumanOrganAtlasDataset':
            './src/components/project/hoa/HumanOrganAtlasDataset',
          './PaleoDataset': './src/components/project/paleo/PaleoDataset',
          './XASDatasetDetail':
            './src/components/technique/xas/XASDatasetDetail',
          './XASDatasetTableCell':
            './src/components/technique/xas/XASDatasetTableCell',
          './SAXSHPLCDatasetDetails':
            './src/components/technique/saxs/SAXSHPLCDatasetDetails',
          './SAXSHPLCDatasetSnapshot':
            './src/components/technique/saxs/SAXSHPLCDatasetSnapshot',
          './SAXSSampleChangerDatasetDetails':
            './src/components/technique/saxs/SAXSSampleChangerDatasetDetails',
          './SAXSSampleChangerDatasetSnapshot':
            './src/components/technique/saxs/SAXSSampleChangerDatasetSnapshot',
          './TOMODataset': './src/components/technique/tomo/TOMODataset',
        },
        shared: [
          'react',
          'react-dom',
          '@tanstack/react-query',
          'react-router-dom',
          '@edata-portal/core',
        ],
      }),
    ],
    server: {
      port: 3005,
      host: true,
    },
    build: {
      modulePreload: false,
      target: 'esnext',
      minify: mode === 'prod',
      cssCodeSplit: false,
    },
  };
});
