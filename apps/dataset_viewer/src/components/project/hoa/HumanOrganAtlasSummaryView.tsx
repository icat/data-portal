import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  DoiInfo,
  Gallery,
  LazyWrapper,
  Loading,
  ProjectLink,
} from '@edata-portal/core';
import HumanOrganAtlasMetadata from './HumanOrganAtlasMetadata';
import { Col, Row } from 'react-bootstrap';

export default function HumanOrganAtlasSummaryView({
  dataset,
}: {
  dataset: Dataset;
}) {
  return (
    <Row>
      <Col xs={12} sm={12} md={3}>
        <Row>
          <ProjectLink dataset={dataset} />
        </Row>
        <Row>
          <LazyWrapper placeholder={<Loading />}>
            <DoiInfo dataset={dataset} />
          </LazyWrapper>
        </Row>
        <Row>
          <Gallery dataset={dataset}></Gallery>
        </Row>
      </Col>
      <HumanOrganAtlasMetadata dataset={dataset} />
    </Row>
  );
}
