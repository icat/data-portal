import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  DatasetCard,
  NoData,
  getFilesTab,
  getMetadataTab,
} from '@edata-portal/core';
import HumanOrganAtlasSummaryView from './HumanOrganAtlasSummaryView';

function DatasetComponent({ dataset }: { dataset: Dataset }) {
  const tabs = [
    {
      title: 'Summary',
      key: 'summary',
      content: <HumanOrganAtlasSummaryView dataset={dataset} />,
    },
    getFilesTab(dataset, false, false),
    getMetadataTab(dataset),
  ];

  return <DatasetCard dataset={dataset} tabs={tabs} />;
}

export default function HumanOrganAtlasDataset({
  dataset,
}: {
  dataset: Dataset;
}) {
  if (dataset) {
    return <DatasetComponent dataset={dataset} />;
  } else {
    return <NoData />;
  }
}
