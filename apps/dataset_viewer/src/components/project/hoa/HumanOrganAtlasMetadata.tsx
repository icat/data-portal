import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MetadataTable,
  MetadataTableParameters,
  getDatasetParamValue,
} from '@edata-portal/core';
import { Col, Card } from 'react-bootstrap';

function getDatasetParameters(
  dataset: Dataset,
): Array<{ title: string; parameters: MetadataTableParameters }> {
  return [
    {
      title: 'Patient',
      parameters: [
        {
          caption: 'Technique',
          parameterName: 'definition',
        },
        {
          caption: 'Number',
          parameterName: 'SamplePatient_number',
        },
        {
          caption: 'Age',
          parameterName: 'SamplePatient_age',
          units: 'yo',
        },
        {
          caption: 'Sex',
          parameterName: 'SamplePatient_sex',
        },
        {
          caption: 'Weight',
          parameterName: 'SamplePatient_weight',
          units: 'kg',
        },
        {
          caption: 'Height',
          parameterName: 'SamplePatient_size',
          units: 'cm',
        },
        {
          caption: 'Institute',
          parameterName: 'SamplePatient_institute',
        },
        {
          caption: 'Medical info',
          parameterName: 'SamplePatient_info',
        },
      ],
    },
    {
      title: 'DOI',
      parameters: [
        {
          caption: 'Abstract',
          parameterName: 'DOI_abstract',
        },
        {
          caption: 'Title',
          parameterName: 'DOI_title',
        },
        {
          caption: 'Users',
          parameterName: 'DOI_users',
        },
      ],
    },
    {
      title: 'Sample',
      parameters: [
        {
          caption: 'Name',
          parameterName: 'Sample_name',
        },
        {
          caption: 'Organ',
          parameterName: 'SamplePatient_organ_name',
        },
        {
          caption: 'Info',
          parameterName: 'Sample_description',
        },
        {
          caption: 'Preparation',
          parameterName: 'Sample_preparation_description',
        },
      ],
    },
    {
      title: 'Scan parameters',
      parameters: [
        {
          caption: 'Instrument',
          parameterName: 'TOMO_idNames',
        },
        {
          caption: 'SR Current',
          parameterName: 'InstrumentSource_current',
          units: 'mA',
        },
        {
          caption: 'Exposure Time',
          parameterName: 'TOMO_exposureTime',
          units: 's',
        },
        {
          caption: 'Pixel Size',
          parameterName: 'TOMO_pixelSize',
        },
        {
          caption: 'Scanning mode',
          parameterName: 'scanType',
        },
        {
          caption: 'ScanRadix',
          parameterName: 'TOMO_scanRadix',
        },
        {
          caption: 'Step (x,y,z)',
          value: `${getDatasetParamValue(
            dataset,
            'TOMO_xStep',
          )}, ${getDatasetParamValue(
            dataset,
            'TOMO_yStep',
          )}, ${getDatasetParamValue(dataset, 'TOMO_zStep')}`,
        },
        {
          caption: 'Stages (x,y,z)',
          value: `${getDatasetParamValue(
            dataset,
            'TOMO_xStages',
          )}, ${getDatasetParamValue(
            dataset,
            'TOMO_yStages',
          )}, ${getDatasetParamValue(dataset, 'TOMO_zStages')}`,
        },
        {
          caption: 'Projections',
          parameterName: 'TOMO_projN',
        },
        {
          caption: 'Ref N',
          parameterName: 'TOMO_refN',
        },
        {
          caption: 'Dark N',
          parameterName: 'TOMO_darkN',
        },
        {
          caption: 'Ref on',
          parameterName: 'TOMO_refOn',
        },
        {
          caption: 'Acc. frames count',
          parameterName: 'TOMO_accFramesCount',
        },
        {
          caption: 'Detector Distance',
          parameterName: 'TOMO_detectorDistance',
        },
        {
          caption: 'Detected avg. energy',
          parameterName: 'TOMO_energy',
        },
        {
          caption: 'Scan geometry',
          parameterName: 'TOMO_halfAcquisition',
        },
        {
          caption: 'Scan range',
          parameterName: 'TOMO_scanRange',
        },
        {
          caption: 'Pixel (x,y,z)',
          value: `${getDatasetParamValue(
            dataset,
            'TOMO_x_pixel_n',
          )}, ${getDatasetParamValue(dataset, 'TOMO_y_pixel_n')}`,
        },
        {
          caption: 'Magnification',
          parameterName: 'TOMO_magnification',
        },
        {
          caption: 'Surface dose rate',
          parameterName: 'TOMO_surface_dose',
        },
        {
          caption: 'VOI dose rate',
          parameterName: 'TOMO_voi_dose',
        },
        {
          caption: 'VOI integ. dose',
          parameterName: 'TOMO_total_voi_dose',
        },
        {
          caption: 'Scan time',
          parameterName: 'TOMO_scanTime',
          units: 'min',
        },
        {
          caption: 'Series time',
          parameterName: 'TOMO_seriesTime',
          units: 'h',
        },
      ],
    },
    {
      title: 'Sensor',
      parameters: [
        {
          caption: 'Name',
          parameterName: 'InstrumentDetector01_description',
        },
        {
          caption: 'Mode',
          parameterName: 'InstrumentDetector01_acquisition_mode',
        },
        {
          caption: 'Pixel size',
          parameterName: 'TOMO_ccdPixelSize',
        },
        {
          caption: 'Optics Type',
          parameterName: 'TOMO_opticsType',
        },
      ],
    },
    {
      title: 'Processing',
      parameters: [
        {
          caption: 'Ref. approach',
          parameterName: 'TOMO_reference_description',
        },
        {
          caption: 'Volume X',
          parameterName: 'TOMO_x_volume',
        },
        {
          caption: 'Volume Y',
          parameterName: 'TOMO_y_volume',
        },
        {
          caption: 'Volume Z',
          parameterName: 'TOMO_z_volume',
        },
        {
          caption: '32 to 16 bits min',
          parameterName: 'TOMO_min32to16bits',
        },
        {
          caption: '32 to 16 bits max',
          parameterName: 'TOMO_max32to16bits',
        },
        {
          caption: 'JP2 compr. ratio',
          parameterName: 'TOMO_jp2CompressRatio',
        },
        {
          caption: 'Filters',
          parameterName: 'InstrumentAttenuator01_description',
        },
        {
          caption: 'Technique',
          parameterName: 'TOMO_technique',
        },
        {
          caption: 'Experiment Type',
          parameterName: 'TOMO_experimentType',
        },
      ],
    },
  ];
}

export default function HumanOrganAtlasMetadata({
  dataset,
}: {
  dataset: Dataset;
}) {
  const categories = getDatasetParameters(dataset);
  return (
    <>
      {categories.map((category, index) => (
        <Col key={index} xs={12} md={3}>
          <Card style={{ marginBottom: 8, minWidth: 200 }}>
            <Card.Header>{category.title}</Card.Header>
            <Card.Body>
              <MetadataTable
                entity={dataset}
                valuePlaceholder="-"
                parameters={category.parameters ? category.parameters : []}
              />
            </Card.Body>
          </Card>
        </Col>
      ))}
    </>
  );
}
