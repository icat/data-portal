import type { Dataset } from '@edata-portal/icat-plus-api';
import { DatasetCard, getFilesTab, getMetadataTab } from '@edata-portal/core';
import PaleoSummaryView from './PaleoSummaryView';
import { useState } from 'react';

export default function PaleoDataset({
  dataset,
  expandedDefault = false,
}: {
  dataset: Dataset;
  expandedDefault?: boolean;
}) {
  const tabs = [
    {
      title: 'Summary',
      key: 'summary',
      content: <PaleoSummaryView dataset={dataset} />,
    },
    getFilesTab(dataset, false, false),
    getMetadataTab(dataset),
  ];
  const [expanded, setExpanded] = useState(expandedDefault);

  return (
    <DatasetCard
      dataset={dataset}
      tabs={tabs}
      expanded={expanded}
      setExpanded={(v) => setExpanded(v)}
    />
  );
}
