import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  DoiInfo,
  Gallery,
  LazyWrapper,
  Loading,
  ProjectLink,
} from '@edata-portal/core';
import PaleoMetadata from './PaleoMetadata';
import { Col, Row } from 'react-bootstrap';

export default function PaleoSummaryView({ dataset }: { dataset: Dataset }) {
  return (
    <Row>
      <Col xs={12} sm={12} md={3}>
        <Row>
          <ProjectLink dataset={dataset} />
        </Row>
        <Row>
          <LazyWrapper placeholder={<Loading />}>
            <DoiInfo dataset={dataset} />
          </LazyWrapper>
        </Row>
        <Row>
          <Gallery dataset={dataset}></Gallery>
        </Row>
      </Col>
      <PaleoMetadata dataset={dataset} />
    </Row>
  );
}
