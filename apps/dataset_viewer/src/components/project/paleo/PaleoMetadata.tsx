import type { Dataset } from '@edata-portal/icat-plus-api';
import { MetadataTable, MetadataTableParameters } from '@edata-portal/core';
import { Col, Card } from 'react-bootstrap';

function getDatasetParameters(): Array<{
  title: string;
  parameters: MetadataTableParameters;
}> {
  return [
    {
      title: 'Sample',
      parameters: [
        {
          caption: 'Scientific domain',
          parameterName: 'SamplePaleo_scientific_domain',
        },
        {
          caption: 'Continental region',
          parameterName: 'SampleLocalisation_continental_region',
        },
        {
          caption: 'Country',
          parameterName: 'SampleLocalisation_country',
        },
        {
          caption: 'Localisation',
          parameterName: 'SampleLocalisation_name',
        },
        {
          caption: 'Formation',
          parameterName: 'SamplePaleoGeologicalTime_formation',
        },
        {
          caption: 'Era',
          parameterName: 'SamplePaleoGeologicalTime_era',
        },
        {
          caption: 'Period',
          parameterName: 'SamplePaleoGeologicalTime_period',
        },
        {
          caption: 'Epoch',
          parameterName: 'SamplePaleoGeologicalTime_epoch',
        },
        {
          caption: 'Clade 1',
          parameterName: 'SamplePaleoClassification_clade1',
        },
        {
          caption: 'Clade 2',
          parameterName: 'SamplePaleoClassification_clade2',
        },
        {
          caption: 'Clade 3',
          parameterName: 'SamplePaleoClassification_clade3',
        },
        {
          caption: 'Clade 4',
          parameterName: 'SamplePaleoClassification_clade4',
        },
        {
          caption: 'Clade 5',
          parameterName: 'SamplePaleoClassification_clade5',
        },
        {
          caption: 'Clade 6',
          parameterName: 'SamplePaleoClassification_clade6',
        },
        {
          caption: 'Species',
          parameterName: 'SamplePaleoClassification_species',
        },
        {
          caption: 'Material type',
          parameterName: 'SamplePaleoClassification_material_type',
        },
      ],
    },
    {
      title: 'Additional information',
      parameters: [
        {
          caption: 'Keyword 1',
          parameterName: 'Notes_note_03',
        },
        {
          caption: 'Keyword 2',
          parameterName: 'Notes_note_04',
        },
        {
          caption: 'Comments',
          parameterName: 'Notes_note_00',
        },
        {
          caption: 'Repository Institution',
          parameterName: 'SamplePaleo_repository_institution',
        },
        {
          caption: 'Authors',
          parameterName: 'ExternalReferencesDatacollector_endnote',
        },
        {
          caption: 'Segmentation',
          parameterName: 'Notes_note_01',
        },
      ],
    },
    {
      title: 'DOI',
      parameters: [
        {
          caption: 'Abstract',
          parameterName: 'DOI_abstract',
        },
        {
          caption: 'Title',
          parameterName: 'DOI_title',
        },
        {
          caption: 'Users',
          parameterName: 'DOI_users',
        },
      ],
    },
    {
      title: 'Associated publication',
      parameters: [
        {
          caption: 'DOI paper',
          parameterName: 'ExternalReferencesPublication_doi',
        },
        {
          caption: 'Ref.',
          parameterName: 'ExternalReferencesPublication_endnote',
        },
      ],
    },
  ];
}

export default function PaleoMetadata({ dataset }: { dataset: Dataset }) {
  const categories = getDatasetParameters();
  return (
    <>
      {categories.map((category, index) => (
        <Col key={index} xs={12} md={3}>
          <Card style={{ marginBottom: 8, minWidth: 200 }}>
            <Card.Header>{category.title}</Card.Header>
            <Card.Body>
              <MetadataTable
                entity={dataset}
                valuePlaceholder="-"
                parameters={category.parameters ? category.parameters : []}
              />
            </Card.Body>
          </Card>
        </Col>
      ))}
    </>
  );
}
