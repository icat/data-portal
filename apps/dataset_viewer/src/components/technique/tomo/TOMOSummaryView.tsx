import {
  convertToFixed,
  convertUnitDatasetParameter,
  formatDateToDayAndTime,
  Gallery,
  getDatasetParamValue,
  MetadataCard,
  MetadataTable,
  AttenuatorsWidget,
  type MetadataTableParameter,
} from '@edata-portal/core';
import { InstrumentSlitWidget } from '@edata-portal/core/src/components/metadata/InstrumentSlitWidget';
import type { Dataset } from '@edata-portal/icat-plus-api';

import { Col, Container, Row } from 'react-bootstrap';

const presetSummary = (dataset: Dataset): MetadataTableParameter[] => {
  const params: MetadataTableParameter[] = [];
  const startTime = getDatasetParamValue(dataset, 'startDate')
    ? formatDateToDayAndTime(getDatasetParamValue(dataset, 'startDate'))
    : undefined;
  const endTime = getDatasetParamValue(dataset, 'endDate')
    ? formatDateToDayAndTime(getDatasetParamValue(dataset, 'endDate'))
    : undefined;
  if (startTime) {
    params.push({
      caption: 'Start',
      value: startTime,
      parameterName: 'startDate',
    });
  }
  if (endTime) {
    params.push({
      caption: 'End',
      value: endTime,
      parameterName: 'endDate',
    });
  }
  return params;
};

const acquisitionParameters = () => [
  {
    caption: 'Number of projections',
    parameterName: 'TOMOAcquisition_proj_n',
  },
  {
    caption: 'Number of flat images',
    parameterName: 'TOMOAcquisition_flat_n',
  },
  {
    caption: 'Number of dark images',
    parameterName: 'TOMOAcquisition_dark_n',
  },
  {
    caption: 'Scan range',
    parameterName: 'TOMOAcquisition_scan_range',
  },
  {
    caption: 'Start angle',
    parameterName: 'TOMOAcquisition_start_angle',
  },
  {
    caption: 'Out of beam displacement',
    parameterName: 'TOMOAcquisition_y_step',
  },
];

const beamParameters = (dataset: Dataset) => [
  {
    caption: 'Energy',
    parameterName: 'TOMOAcquisition_energy',
  },
  {
    caption: 'Source to sample distance',
    value: getDatasetParamValue(
      dataset,
      'TOMOAcquisition_source_sample_distance',
    )
      ? `${convertUnitDatasetParameter(dataset, 'TOMOAcquisition_source_sample_distance', 'm', 1)} m`
      : '',
    parameterName: 'TOMOAcquisition_source_sample_distance',
  },
  {
    caption: 'Ring current at start',
    parameterName: 'TOMOAcquisition_srcur_start',
  },
  {
    caption: 'Ring current at end',
    parameterName: 'TOMOAcquisition_srcur_stop',
  },
];

const formatImagesDimensions = (dataset: Dataset) => {
  const value = getDatasetParamValue(dataset, `InstrumentDetector01Rois_value`);
  const dimensions = value?.split(',');
  if (dimensions?.length === 4) {
    const v1 = Number(dimensions[0]);
    const v2 = Number(dimensions[1]);
    const v3 = Number(dimensions[2]);
    const v4 = Number(dimensions[3]);
    if (!isNaN(v1) && !isNaN(v2) && !isNaN(v3) && !isNaN(v4)) {
      return `${v1 + v3}x${v2 + v4}`;
    }
  }
  return value;
};
const cameraParameters = (dataset: Dataset) => [
  {
    caption: 'Camera name',
    parameterName: 'InstrumentDetector01Rois_name',
  },
  {
    caption: 'Images dimensions (HxV)',
    value: formatImagesDimensions(dataset),
    parameterName: 'InstrumentDetector01Rois_value',
  },
  {
    caption: 'Sample pixel size',
    parameterName: 'TOMOAcquisition_sample_pixel_size',
    digits: 3,
  },
  {
    caption: 'Sample detector distance',
    parameterName: 'TOMOAcquisition_sample_detector_distance',
    digits: 2,
  },
  {
    caption: 'Exposure time',
    parameterName: 'TOMOAcquisition_exposure_time',
  },
  {
    caption: 'Latency time',
    value: getDatasetParamValue(dataset, 'TOMOAcquisition_latency_time')
      ? `${convertUnitDatasetParameter(dataset, 'TOMOAcquisition_latency_time', 'ms', 1)} ms`
      : '',
    parameterName: 'TOMOAcquisition_latency_time',
  },
  {
    caption: 'Acq mode',
    parameterName: 'TOMOAcquisition_camera_acq_mode',
  },
  {
    caption: 'Scintillator',
    parameterName: 'TOMOAcquisition_scintillator',
  },
  {
    caption: 'Optic magnification',
    value: getDatasetParamValue(dataset, 'TOMOAcquisition_magnification')
      ? `${convertToFixed(getDatasetParamValue(dataset, 'TOMOAcquisition_magnification'), 2)}x`
      : '',
    parameterName: 'TOMOAcquisition_magnification',
  },
  {
    caption: 'Optic type',
    parameterName: 'TOMOAcquisition_optic_type',
  },
];

export default function TOMOSummaryView({ dataset }: { dataset: Dataset }) {
  return (
    <Container fluid>
      <Row className="g-3">
        {presetSummary && (
          <Col xs="auto">
            <MetadataTable
              entity={dataset}
              parameters={presetSummary(dataset)}
            ></MetadataTable>
          </Col>
        )}
        <Col xs="auto">
          <MetadataCard
            title="Acquisition parameters"
            entity={dataset}
            content={acquisitionParameters()}
          />
        </Col>
        <Col xs="auto">
          <MetadataCard
            title="Beam"
            entity={dataset}
            content={beamParameters(dataset)}
          />
        </Col>
        <AttenuatorsWidget dataset={dataset} />
        <Col xs="auto">
          <MetadataCard
            title="Camera and optics"
            entity={dataset}
            content={cameraParameters(dataset)}
          />
        </Col>
        <InstrumentSlitWidget dataset={dataset} />
        <Col xs="auto">
          <Gallery dataset={dataset.outputDatasets?.[0]}></Gallery>
        </Col>
      </Row>
    </Container>
  );
}
