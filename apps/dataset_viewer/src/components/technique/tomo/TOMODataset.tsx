import {
  DatasetCard,
  NoData,
  getDatasetName,
  getFilesTab,
  getMetadataTab,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

import TOMOSummaryView from './TOMOSummaryView';

function DatasetComponent({ dataset }: { dataset: Dataset }) {
  const tabs = [
    {
      title: 'Summary',
      key: 'summary',
      content: <TOMOSummaryView dataset={dataset} />,
    },
    getFilesTab(dataset, false, false),
    getMetadataTab(dataset),
  ];

  const title = `${dataset.sampleName} ${getDatasetName(dataset)}`;

  return <DatasetCard dataset={dataset} tabs={tabs} title={title} />;
}

export default function TOMODataset({ dataset }: { dataset: Dataset }) {
  if (dataset) {
    return <DatasetComponent dataset={dataset} />;
  } else {
    return <NoData />;
  }
}
