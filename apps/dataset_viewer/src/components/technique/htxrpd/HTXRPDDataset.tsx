import {
  DatasetCard,
  NoData,
  getFilesTab,
  getMetadataTab,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

import HTXRPDSummaryView from './HTXRPDSummaryView';

function DatasetComponent({ dataset }: { dataset: Dataset }) {
  const tabs = [
    {
      title: 'Summary',
      key: 'summary',
      content: <HTXRPDSummaryView dataset={dataset} />,
    },
    getFilesTab(dataset, false, false),
    getMetadataTab(dataset),
  ];

  return <DatasetCard dataset={dataset} tabs={tabs} />;
}

export default function HTXRPDDataset({ dataset }: { dataset: Dataset }) {
  if (dataset) {
    return <DatasetComponent dataset={dataset} />;
  } else {
    return <NoData />;
  }
}
