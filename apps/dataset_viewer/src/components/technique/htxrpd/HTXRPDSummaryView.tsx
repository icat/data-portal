import {
  MetadataTable,
  Gallery,
  formatDateToDayAndTime,
  getDatasetParamValue,
  getDatasetName,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const presetSummary = (dataset: Dataset) => [
  {
    caption: 'Dataset',
    value: getDatasetName(dataset),
  },
  {
    caption: 'Start',
    value: formatDateToDayAndTime(getDatasetParamValue(dataset, 'startDate')),
  },
  {
    caption: 'End',
    value: formatDateToDayAndTime(getDatasetParamValue(dataset, 'endDate')),
  },
  {
    caption: 'Exp. Time',
    parameterName: 'HTXRPD_exposureTime',
    units: 's',
  },
];

const dataCollectionParameters = () => [
  {
    caption: 'Distance',
    parameterName: 'HTXRPD_distance',
    digits: 2,
  },
  {
    caption: 'Energy',
    parameterName: 'HTXRPD_energy',
    digits: 2,
  },
  {
    caption: 'Vibration',
    parameterName: 'HTXRPD_sampleVibration',
    digits: 1,
    units: '%',
  },
];

export default function SummarySampleView({ dataset }: { dataset: Dataset }) {
  return (
    <Row>
      <Col xs="auto">
        <MetadataTable
          entity={dataset}
          parameters={presetSummary(dataset)}
        ></MetadataTable>
      </Col>
      <Col xs="auto">
        <MetadataTable
          entity={dataset}
          parameters={dataCollectionParameters()}
        ></MetadataTable>
      </Col>
      <Col xs="auto">
        <Gallery dataset={dataset.outputDatasets?.[0]}></Gallery>
      </Col>
    </Row>
  );
}
