import {
  formatDateToDayAndTime,
  Gallery,
  getDatasetParamValue,
  MetadataTable,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

import { Col, Container, Row } from 'react-bootstrap';

const presetSummary = (dataset: Dataset) => [
  {
    caption: 'Sample',
    value: dataset.sampleName,
  },
  {
    caption: 'Description',
    parameterName: 'Sample_description',
  },
  {
    caption: 'Notes',
    parameterName: 'Notes_note_00',
  },
];

const datasetSummary = (dataset: Dataset) => [
  {
    caption: 'Technique',
    parameterName: 'definition',
  },
  {
    caption: 'Start',
    value: formatDateToDayAndTime(getDatasetParamValue(dataset, 'startDate')),
  },
  {
    caption: 'End',
    value: formatDateToDayAndTime(getDatasetParamValue(dataset, 'endDate')),
  },
];

const laser1Parameters = () => [
  {
    caption: 'Laser name',
    parameterName: 'InstrumentLaser01_name',
  },
  {
    caption: 'Laser 1 Energy (J)',
    parameterName: 'InstrumentLaser01_energy',
    digits: 2,
  },
  {
    caption: 'Laser 1 Delay (s)',
    parameterName: 'InstrumentLaser01_delay',
    digits: 2,
  },
];

const laser2Parameters = () => [
  {
    caption: 'Laser name',
    parameterName: 'InstrumentLaser02_name',
  },
  {
    caption: 'Laser 2 Energy (J)',
    parameterName: 'InstrumentLaser02_energy',
    digits: 2,
  },
  {
    caption: 'Laser 2 Delay (s)',
    parameterName: 'InstrumentLaser02_delay',
    digits: 2,
  },
];

export function XASDatasetSummary({ dataset }: { dataset: Dataset }) {
  return (
    <Container fluid>
      <Row>
        <Col xs="auto">
          <MetadataTable
            entity={dataset}
            parameters={presetSummary(dataset)}
          ></MetadataTable>
        </Col>
        <Col xs="auto">
          <MetadataTable
            entity={dataset}
            parameters={datasetSummary(dataset)}
          ></MetadataTable>
        </Col>
        <Col xs="auto">
          <MetadataTable
            entity={dataset}
            parameters={laser1Parameters()}
          ></MetadataTable>
        </Col>
        <Col xs="auto">
          <MetadataTable
            entity={dataset}
            parameters={laser2Parameters()}
          ></MetadataTable>
        </Col>
        <Col xs="auto">
          <Gallery dataset={dataset.outputDatasets?.[0]}></Gallery>
        </Col>
      </Row>
    </Container>
  );
}
