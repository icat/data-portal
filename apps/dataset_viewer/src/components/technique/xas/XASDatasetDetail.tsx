import { Dataset } from '@edata-portal/icat-plus-api';
import { XASDatasetViewer } from 'components/technique/xas/XASDatasetViewer';

export default function XASDatasetDetail({ dataset }: { dataset: Dataset }) {
  return <XASDatasetViewer dataset={dataset} isTableCell={false} />;
}
