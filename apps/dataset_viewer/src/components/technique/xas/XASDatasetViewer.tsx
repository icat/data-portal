import {
  DatasetCard,
  Tree,
  getDatasetName,
  getFilesTab,
  getMetadataTab,
  useViewers,
} from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import { XASDatasetSummary } from 'components/technique/xas/XASDatasetSummary';

export function XASDatasetViewer({
  dataset,
  isTableCell,
}: {
  dataset: Dataset;
  isTableCell: boolean;
}) {
  const viewers = useViewers();
  const tabs = [
    {
      title: 'Summary',
      key: 'summary',
      content: <XASDatasetSummary dataset={dataset} />,
    },
    getFilesTab(dataset, false, false),
    getMetadataTab(dataset),
  ];

  const datasetComponent = (
    <DatasetCard
      key={dataset.id}
      classNameBody="p-1"
      dataset={dataset}
      title={getDatasetName(dataset)}
      tabs={tabs}
      hideDatasetTitle={isTableCell}
      hideSelectionButton={isTableCell}
      hideDownloadButton={isTableCell}
    />
  );
  if (!dataset.outputDatasets?.length) return datasetComponent;

  return (
    <Tree
      items={[
        {
          key: dataset.id,
          item: datasetComponent,
          sticky: true,
          children: dataset.outputDatasets.map((output) => ({
            key: output.id,
            item: viewers.viewDataset(output, 'details'),
          })),
        },
      ]}
    />
  );
}
