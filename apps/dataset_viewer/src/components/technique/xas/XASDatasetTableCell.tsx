import { Dataset } from '@edata-portal/icat-plus-api';
import { XASDatasetViewer } from 'components/technique/xas/XASDatasetViewer';

export default function XASDatasetTableCell({ dataset }: { dataset: Dataset }) {
  return <XASDatasetViewer dataset={dataset} isTableCell={true} />;
}
