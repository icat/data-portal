import {
  MetadataCategory,
  SnapshotConfig,
  SnashoptConfigItem,
  getDatasetName,
  getDatasetParamValue,
  getParameterByName,
  isProcessed,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

const isBuffer = (dataset: Dataset) => {
  return getDatasetParamValue(dataset, 'SAXS_sample_type') === 'buffer';
};

const checkScanType = (
  dataset: Dataset,
  scanType: string,
  keyword: string,
): boolean => {
  // Retrieve the scanType parameter from the dataset
  const parameter = getParameterByName('scanType', dataset.parameters);

  // Return true if scanType exists and matches the provided type
  if (parameter?.value === scanType) {
    return true;
  }

  // Check if the dataset name contains the specified keyword
  return getDatasetName(dataset).includes(keyword);
};

const isIntegration = (dataset: Dataset): boolean =>
  checkScanType(dataset, 'integration', 'integrate');
const isSubtraction = (dataset: Dataset): boolean =>
  checkScanType(dataset, 'subtraction', 'subtract');

function sampleParameters() {
  return [
    {
      caption: 'Sample',
      parameterName: 'Sample_name',
    },
    {
      caption: 'Technique',
      parameterName: 'saxs_definition',
    },
    {
      caption: 'Type',
      parameterName: 'SAXS_experiment_type',
    },
    {
      caption: 'Concentration',
      parameterName: 'SAXS_concentration',
      units: 'mg/ml',
    },
    {
      caption: 'Collection Temp.',
      parameterName: 'SAXS_exposure_temperature',
    },
    {
      caption: 'Number of frames',
      parameterName: 'SAXS_numberFrames',
    },
    {
      caption: 'Time per frame',
      parameterName: 'SAXS_timePerFrame',
    },
    {
      caption: 'Wavelength',
      parameterName: 'SAXS_waveLength',
      digits: 2,
    },
    {
      caption: 'Transmission',
      parameterName: 'SAXS_transmission',
    },
  ];
}

function getMetadataParameters(dataset: Dataset) {
  return [
    {
      caption: 'Buffer',
      parameterName: 'Sample_name',
    },
    {
      caption: 'Technique',
      parameterName: 'saxs_definition',
    },
    {
      caption: 'Type',
      parameterName: 'SAXS_experiment_type',
    },
    {
      caption: 'Temperature',
      parameterName: 'SAXS_exposure_temperature',
    },
    {
      caption: 'Wavelength',
      parameterName: 'SAXS_waveLength',
      digits: 2,
    },
    {
      caption: 'Transmission',
      parameterName: 'SAXS_transmission',
    },
    {
      caption: 'Averaged ',
      value: dataset
        ? getDatasetParamValue(dataset, 'SAXS_frames_averaged', 'inte')
        : '',
    },
    { caption: 'Frames', parameterName: 'SAXS_numberFrames' },
  ];
}

function getBufferParameters(): SnashoptConfigItem[] {
  return [
    {
      type: 'metadata',
      metadata: [
        {
          caption: 'Buffer',
          parameterName: 'Sample_name',
        },
        {
          caption: 'Technique',
          parameterName: 'saxs_definition',
        },
        {
          caption: 'Type',
          parameterName: 'SAXS_experiment_type',
        },
      ],
    },
    {
      type: 'metadata',
      metadata: [
        {
          caption: 'Temperature',
          parameterName: 'SAXS_exposure_temperature',
        },
        {
          caption: 'Wavelength',
          parameterName: 'SAXS_waveLength',
          digits: 2,
        },
        {
          caption: 'Transmission',
          parameterName: 'SAXS_transmission',
        },
      ],
    },
  ];
}

export function getSnapshotConfigByProcesssedDataset(
  processedDataset: Dataset,
): SnashoptConfigItem[] {
  /** if subtractiong */
  if (isSubtraction(processedDataset)) {
    return [
      {
        type: 'metadata',
        metadata: [
          {
            caption: 'Guinier Rg',
            parameterName: 'SAXS_guinier_rg',
          },
          {
            caption: 'P(r) Rg',
            parameterName: 'SAXS_rg',
          },
          {
            caption: 'Dmax Rg',
            parameterName: 'SAXS_d_max',
          },
          {
            caption: 'P(r) fir chi2',
          },
          {
            caption: 'Vc',
          },
          {
            caption: 'Mass',
          },
          {
            caption: 'Volume',
            parameterName: 'SAXS_porod_volume',
            digits: 3,
          },
        ],
      },
      {
        type: 'gallery',
        name: /Scattering/i,
      },
      {
        type: 'gallery',
        name: /Guinier/i,
      },
      {
        type: 'gallery',
        name: /Kra/i,
      },
      {
        type: 'gallery',
        name: /Density/i,
      },
    ];
  }

  if (isIntegration(processedDataset)) {
    return [
      {
        type: 'metadata',
        metadata: [
          {
            caption: 'Number Frames',
            parameterName: 'SAXS_numberFrames',
          },
          {
            caption: 'Range averaged',
            parameterName: 'SAXS_frames_averaged',
          },
        ],
      },
    ];
  }

  return [];
}

export function getSnapshotConfigByRawDataset(
  rawDataset: Dataset,
): SnashoptConfigItem[] {
  /** if subtractiong */
  if (isBuffer(rawDataset)) {
    return getBufferParameters();
  }

  return [
    {
      type: 'metadata',
      metadata: [
        {
          caption: 'Sample',
          parameterName: 'Sample_name',
        },
        {
          caption: 'Type',
          parameterName: 'SAXS_experiment_type',
        },
        {
          caption: 'Concentration',
          parameterName: 'SAXS_concentration',
          units: 'mg/ml',
        },
        {
          caption: 'Collection Temp.',
          parameterName: 'SAXS_exposure_temperature',
        },
      ],
    },
    {
      type: 'metadata',
      metadata: [
        {
          caption: 'Number of frames',
          parameterName: 'SAXS_numberFrames',
        },
        {
          caption: 'Time per frame',
          parameterName: 'SAXS_timePerFrame',
        },
        {
          caption: 'Wavelength',
          parameterName: 'SAXS_waveLength',
          digits: 2,
        },
        {
          caption: 'Transmission',
          parameterName: 'SAXS_transmission',
        },
      ],
    },
  ];
}

export function getSaxsSampleChangerSnapshot(dataset: Dataset): SnapshotConfig {
  return {
    showGallery: false,
    main: isProcessed(dataset)
      ? getSnapshotConfigByProcesssedDataset(dataset)
      : getSnapshotConfigByRawDataset(dataset),
  };
}

export function getSaxsSampleChangerDetailedMetadata(
  dataset: Dataset,
): MetadataCategory[] {
  if (isProcessed(dataset)) return processedMetadata(dataset);
  return rawMetadata(dataset);
}
function processedMetadata(dataset: Dataset): MetadataCategory[] {
  const detectorParams = [
    {
      caption: 'Detector distance',
      parameterName: 'SAXS_detector_distance',
      digits: 2,
      units: 'ƛ',
    },
    {
      caption: 'Diode currents',
      parameterName: 'SAXS_diode_currents',
      digits: 5,
      units: '%',
    },
    {
      caption: 'Averaged',
      parameterName: 'SAXS_frames_averaged',
      units: 'C',
    },
    {
      caption: 'Mask',
      parameterName: 'SAXS_maskFile',
    },
  ];
  const imageParams = [
    {
      caption: 'Normalization',
      parameterName: 'SAXS_normalisation',
      digits: 2,
    },
    {
      caption: 'Frames',
      parameterName: 'SAXS_numberFrames',
      digits: 5,
    },
    {
      caption: 'Time/frame',
      parameterName: 'SAXS_timePerFrame',
    },
    {
      caption: 'Pixel size X',
      parameterName: 'SAXS_pixelSizeX',
    },
    {
      caption: 'Pixel size Y',
      parameterName: 'SAXS_pixelSizeY',
    },
  ];
  const datasetParams = [
    {
      caption: 'Wavelegnth',
      parameterName: 'SAXS_waveLength',
    },
    {
      caption: 'Start',
      parameterName: 'startDate',
    },
    {
      caption: 'End',
      parameterName: 'endDate',
    },
  ];

  const substractionParams = [
    {
      caption: 'dMax',
      parameterName: 'SAXS_d_max',
    },
    {
      caption: 'Guiner i0',
      parameterName: 'SAXS_guinier_i0',
    },
    {
      caption: 'Guinier Points',
      parameterName: 'SAXS_guinier_points',
    },
    {
      caption: 'Volume Porod',
      parameterName: 'SAXS_porod_volume',
    },
    {
      caption: 'Rg',
      parameterName: 'SAXS_rg',
    },
  ];

  return [
    {
      title: 'Dataset',
      id: 'dataset',
      content: datasetParams,
    },
    ...(isSubtraction(dataset)
      ? [
          {
            title: 'Subtraction',
            id: 'subtraction',
            content: substractionParams,
          },
        ]
      : []),
    ...(isIntegration(dataset)
      ? [
          {
            title: 'Detector',
            id: 'detector',
            content: detectorParams,
          },
          {
            title: 'Image',
            id: 'image',
            content: imageParams,
          },
        ]
      : []),
  ];
}
function rawMetadata(dataset: Dataset): MetadataCategory[] {
  const dataCollectionParameters = [
    {
      caption: 'Wavelength',
      parameterName: 'SAXS_waveLength',
      digits: 2,
      units: 'ƛ',
    },
    {
      caption: 'Transmission',
      parameterName: 'SAXS_transmission',
      digits: 1,
      units: '%',
    },
    {
      caption: 'Exp. Temperature',
      parameterName: 'SAXS_exposure_temperature',
      units: 'C',
    },
    {
      caption: 'Time/frame',
      parameterName: 'SAXS_timePerFrame',
      units: 's',
    },
    {
      caption: 'Diode Currents',
      parameterName: 'SAXS_diode_currents',
    },
  ];

  const experimentParameters = [
    {
      caption: 'Experiment Type',
      parameterName: 'SAXS_experiment_type',
    },
    {
      caption: 'Sample Type',
      parameterName: 'SAXS_sample_type',
    },
    {
      caption: 'Storage Temperature',
      parameterName: 'SAXS_storage_temperature',
      units: 'C',
    },
  ];

  return [
    {
      title: 'Buffer',
      id: 'buffer',
      content: getMetadataParameters(dataset),
    },

    {
      title: 'Sample',
      id: 'sample',
      content: sampleParameters(),
    },
    {
      title: 'Data Collection',
      id: 'Data Collection',
      content: dataCollectionParameters,
    },
    {
      title: 'Experiment',
      id: 'Experiment',
      content: experimentParameters,
    },
  ];
}
