import {
  MetadataCategory,
  SnapshotConfig,
  isProcessed,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

export function getSaxsHplcSnapshot(dataset: Dataset): SnapshotConfig {
  return {
    showGallery: true,
    main: isProcessed(dataset)
      ? [{ type: 'gallery', name: /.*/i }]
      : [{ type: 'metadata', metadata: datasetParameters() }],
  };
}

export function getSaxsHplcDetailedMetadata(): MetadataCategory[] {
  return [
    {
      title: 'Dataset',
      id: 'dataset',
      content: datasetParameters(),
    },
    {
      title: 'Data Collection',
      id: 'dataCollection',
      content: dataCollectionParameters(),
    },
  ];
}

function datasetParameters() {
  return [
    {
      caption: 'Technique',
      parameterName: 'definition',
    },
    {
      caption: 'Type',
      parameterName: 'SAXS_experiment_type',
    },
    {
      caption: 'Column',
    },
    {
      caption: 'Concentration',
      parameterName: 'SAXS_concentration',
    },
    {
      caption: 'Collection Temp',
      parameterName: 'SAXS_exposure_temperature',
    },
    {
      caption: 'Flow Rate',
      parameterName: 'SAXS_flow_rate',
    },
    {
      caption: 'Number of frames',
      parameterName: 'SAXS_numberFrames',
    },
    {
      caption: 'Frame Time',
    },
  ];
}

function dataCollectionParameters() {
  return [
    { caption: 'Frames', parameterName: 'SAXS_numberFrames' },
    {
      caption: 'Wavelength',
      parameterName: 'SAXS_waveLength',
      digits: 2,
      units: 'ƛ',
    },
    {
      caption: 'Transmission',
      parameterName: 'SAXS_transmission',
      digits: 1,
      units: '%',
    },
    {
      caption: 'Flow Rate',
      parameterName: 'SAXS_flow_rate',
    },
  ];
}
