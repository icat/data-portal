import { GenericDatasetDetailsViewer } from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import { getSaxsSampleChangerDetailedMetadata } from 'components/technique/saxs/saxsSampleChangerConfig';

export default function SAXSSampleChangerDatasetDetails({
  dataset,
  ...props
}: {
  dataset: Dataset;
}) {
  return (
    <GenericDatasetDetailsViewer
      {...props}
      dataset={dataset}
      viewerConfig={(dataset) => ({
        filesTab: true,
        showPathInFilesTab: true,
        metadataTab: false,
        summaryTab: {
          metadata: getSaxsSampleChangerDetailedMetadata(dataset),
          showGallery: false,
        },
      })}
    />
  );
}
