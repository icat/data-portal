import { GenericDatasetSnapshotViewer } from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import {
  getSaxsSampleChangerDetailedMetadata,
  getSaxsSampleChangerSnapshot,
} from 'components/technique/saxs/saxsSampleChangerConfig';

export default function SAXSSampleChangerDatasetSnapshot({
  dataset,
  ...props
}: {
  dataset: Dataset;
}) {
  return (
    <GenericDatasetSnapshotViewer
      dataset={dataset}
      {...props}
      viewerConfig={(dataset) => {
        return {
          snapshot: getSaxsSampleChangerSnapshot(dataset),
          details: {
            filesTab: true,
            showPathInFilesTab: true,
            metadataTab: true,
            summaryTab: {
              metadata: getSaxsSampleChangerDetailedMetadata(dataset),
              showGallery: true,
            },
          },
        };
      }}
    />
  );
}
