import { GenericDatasetSnapshotViewer } from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import {
  getSaxsHplcDetailedMetadata,
  getSaxsHplcSnapshot,
} from 'components/technique/saxs/saxsHplcConfig';

export default function SAXSHPLCDatasetSnapshot({
  dataset,
  ...props
}: {
  dataset: Dataset;
}) {
  return (
    <GenericDatasetSnapshotViewer
      dataset={dataset}
      {...props}
      viewerConfig={(dataset) => {
        return {
          snapshot: getSaxsHplcSnapshot(dataset),
          details: {
            filesTab: true,
            showPathInFilesTab: true,
            metadataTab: true,
            summaryTab: {
              metadata: getSaxsHplcDetailedMetadata(),
              showGallery: true,
            },
          },
        };
      }}
    />
  );
}
