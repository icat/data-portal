import { GenericDatasetDetailsViewer } from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import { getSaxsHplcDetailedMetadata } from 'components/technique/saxs/saxsHplcConfig';

export default function SAXSHPLCDatasetDetails({
  dataset,
  ...props
}: {
  dataset: Dataset;
}) {
  return (
    <GenericDatasetDetailsViewer
      {...props}
      dataset={dataset}
      viewerConfig={() => ({
        filesTab: true,
        showPathInFilesTab: true,
        metadataTab: false,
        summaryTab: {
          metadata: getSaxsHplcDetailedMetadata(),
          showGallery: true,
        },
      })}
    />
  );
}
