import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import federation from '@originjs/vite-plugin-federation';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      react(),
      tsconfigPaths(),
      federation({
        name: 'logistics',
        filename: 'remoteEntry.js',
        exposes: {
          './MyParcels': './src/viewers/MyParcels',
          './MyAddresses': './src/viewers/MyAddresses',
          './InvestigationLogistics': './src/viewers/InvestigationLogistics',
          './InvestigationSampleChanger':
            './src/viewers/InvestigationSampleChanger',
          './ParcelPage': './src/viewers/ParcelPage',
          './MyLogisticsSummary': './src/viewers/MyLogisticsSummary',
          './LogisticsStatistics': './src/viewers/LogisticsStatistics',
        },
        shared: [
          'react',
          'react-dom',
          '@tanstack/react-query',
          'react-router-dom',
          '@edata-portal/core',
        ],
      }),
    ],
    server: {
      port: 3002,
      host: true,
    },
    build: {
      modulePreload: false,
      target: 'esnext',
      minify: mode === 'production',
      cssCodeSplit: false,
    },
  };
});
