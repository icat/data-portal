export const STORAGE_CONDITIONS = [
  'At room temperature',
  'In fridge (4°C)',
  'In freezer (-18°C)',
  'In freezer (-80°C)',
  'Other',
] as const;
