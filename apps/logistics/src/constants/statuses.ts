import type { Parcel } from '@edata-portal/icat-plus-api';
import type { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
  faCalendar,
  faEnvelope,
  faExternalLinkAlt,
  faHome,
  faLock,
  faThumbsUp,
  faTimesCircle,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

/** List of actions sorted chronologically */
export const PARCEL_STATUSES = {
  CREATED: 'CREATED',
  SCHEDULED: 'SCHEDULED',
  SENT: 'SENT',
  STORES: 'STORES',
  BEAMLINE: 'BEAMLINE',
  BACK_STORES: 'BACK_STORES',
  ON_HOLD: 'ON_HOLD',
  BACK_USER: 'BACK_USER',
  DESTROYED: 'DESTROYED',
} as const;

export type ParcelStatusType =
  (typeof PARCEL_STATUSES)[keyof typeof PARCEL_STATUSES];

export type ParcelStatusDescription = {
  desc: string;
  label: string;
  icon: IconProp;
  editable: boolean;
  inactiveParcel: boolean;
  sendable: boolean;
  action?: {
    label: string;
    desc: string;
    requiresSendInfo?: boolean;
    role: 'staff' | 'user';
  };
  allowedActions?: (
    parcel: Parcel,
  ) => (typeof PARCEL_STATUSES)[keyof typeof PARCEL_STATUSES][]; //function to determine which of the next actions are allowed at a given time. If not defined, all next actions are allowed.
  nextActions: (typeof PARCEL_STATUSES)[keyof typeof PARCEL_STATUSES][] | null; //list of all actions that can be performed from this status
};

export const PARCEL_STATUS_DEFS: {
  [key in (typeof PARCEL_STATUSES)[keyof typeof PARCEL_STATUSES]]: ParcelStatusDescription;
} = {
  [PARCEL_STATUSES.CREATED]: {
    desc: 'Parcel has been created',
    label: 'CREATED',
    icon: faExternalLinkAlt,
    editable: true,
    inactiveParcel: false,
    sendable: false,
    nextActions: [PARCEL_STATUSES.SCHEDULED],
  },
  [PARCEL_STATUSES.SCHEDULED]: {
    desc: 'Parcel has been associated with an experimental session',
    label: 'SCHEDULED',
    icon: faCalendar,
    editable: true,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'SCHEDULED',
      desc: 'Associate the parcel with an experimental session',
      role: 'user',
    },
    allowedActions: (parcel) =>
      parcel.content?.length > 0 ? [PARCEL_STATUSES.SENT] : [],
    nextActions: [PARCEL_STATUSES.SENT],
  },
  [PARCEL_STATUSES.SENT]: {
    desc: 'Sent to facility',
    label: 'SENT',
    icon: faEnvelope,
    editable: false,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'Mark as SENT',
      desc: 'Notify that the parcel has been shipped to the facility',
      role: 'user',
      requiresSendInfo: true,
    },
    nextActions: [PARCEL_STATUSES.STORES],
  },
  [PARCEL_STATUSES.STORES]: {
    desc: 'Arrived at facility stores',
    label: 'STORES',
    icon: faHome,
    editable: false,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'Mark as RECEIVED IN STORES',
      desc: 'Notify that the parcel has arrived at the stores',
      role: 'staff',
    },
    nextActions: [PARCEL_STATUSES.BEAMLINE],
  },
  [PARCEL_STATUSES.BEAMLINE]: {
    desc: 'Arrived at the beamline',
    label: 'BEAMLINE',
    icon: faThumbsUp,
    editable: false,
    inactiveParcel: false,
    sendable: true,
    action: {
      label: 'Mark as RECEIVED AT BEAMLINE',
      desc: 'Notify that the parcel has arrived at the beamline',
      role: 'staff',
    },
    allowedActions: (parcel) => {
      return parcel.returnAddress
        ? [PARCEL_STATUSES.BACK_STORES]
        : [PARCEL_STATUSES.BACK_STORES, PARCEL_STATUSES.DESTROYED];
    },
    nextActions: [PARCEL_STATUSES.BACK_STORES],
  },
  [PARCEL_STATUSES.BACK_STORES]: {
    desc: 'Back to facility stores',
    label: 'BACK IN STORES',
    icon: faHome,
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as RETURNED TO STORES',
      desc: 'Notify that the parcel has been returned to the stores',
      role: 'staff',
    },
    allowedActions: (parcel) => {
      return parcel.returnAddress
        ? [PARCEL_STATUSES.BACK_USER, PARCEL_STATUSES.ON_HOLD]
        : [PARCEL_STATUSES.DESTROYED];
    },
    nextActions: [
      PARCEL_STATUSES.BACK_USER,
      PARCEL_STATUSES.ON_HOLD,
      PARCEL_STATUSES.DESTROYED,
    ],
  },
  [PARCEL_STATUSES.BACK_USER]: {
    desc: 'Sent back to return address',
    label: 'SENT BACK TO USER',
    icon: faUser,
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as SENT BACK TO THE USER',
      desc: 'Notify that the parcel has been shipped back to the user',
      role: 'staff',
    },
    nextActions: null,
  },
  [PARCEL_STATUSES.ON_HOLD]: {
    desc: 'Parcel on hold',
    label: 'ON HOLD',
    icon: faLock,
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as ON HOLD',
      desc: 'Notify that the parcel is on hold at the Stores',
      role: 'staff',
    },
    nextActions: [PARCEL_STATUSES.BACK_USER],
  },
  [PARCEL_STATUSES.DESTROYED]: {
    desc: 'Parcel destroyed',
    label: 'DESTROYED',
    icon: faTimesCircle,
    editable: false,
    inactiveParcel: true,
    sendable: true,
    action: {
      label: 'Mark as DESTROYED',
      desc: 'Notify that the parcel has been destroyed',
      role: 'staff',
    },
    nextActions: null,
  },
};

export function getParcelStatus(status: string) {
  if (!(status in PARCEL_STATUSES)) {
    return undefined;
  }
  return PARCEL_STATUS_DEFS[status as ParcelStatusType];
}

export const ENTRY_POINT_ACTIONS = PARCEL_STATUSES.CREATED;
