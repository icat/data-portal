export const SAMPLE_TYPES = ['SAMPLESHEET', 'TOOL', 'OTHER'] as const;

export type SampleType = (typeof SAMPLE_TYPES)[number];
export const SAMPLE_TYPES_LABELS: {
  value: SampleType;
  label: string;
}[] = [
  { value: 'SAMPLESHEET', label: 'Samplesheet' },
  { value: 'TOOL', label: 'Tool' },
  { value: 'OTHER', label: 'Other' },
];
