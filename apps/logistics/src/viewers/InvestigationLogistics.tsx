import { Loading, usePath } from '@edata-portal/core';
import { Suspense } from 'react';

import { ShipmentPage } from '../components/investigation/shipment/ShipmentPage';

export default function InvestigationLogistics() {
  const investigationId = usePath('investigationId');

  return (
    <>
      <Suspense fallback={<Loading />}>
        <ShipmentPage investigationId={investigationId} />
      </Suspense>
    </>
  );
}
