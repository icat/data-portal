import {
  Loading,
  SideNavElement,
  SideNavFilter,
  UI_DATE_DAY_FORMAT,
  WithSideNav,
  addToDate,
  formatDateToIcatDate,
  parseDate,
  useParam,
  SelectInstruments,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  PARCEL_LIST_ENDPOINT,
  type Parcel,
} from '@edata-portal/icat-plus-api';
import { LogisticsStatisticsInvestigationTable } from 'components/statistics/LogisticsStatisticsInvestigationTable';
import { LogisticsStatisticsParcelStatus } from 'components/statistics/LogisticsStatisticsParcelStatus';
import { LogisticsStatisticsSummary } from 'components/statistics/LogisticsStatisticsSummary';
import { Suspense, useMemo } from 'react';
import { Alert, Col, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default function LogisticsStatistics() {
  const [startDate, setStartDate] = useParam(
    'start',
    formatDateToIcatDate(
      addToDate(new Date(), {
        months: -1,
      }),
    ) || '',
  );
  const [endDate, setEndDate] = useParam(
    'end',
    formatDateToIcatDate(new Date()) || '',
  );

  const [instrumentsString, setInstrumentsString] = useParam<string>(
    'instruments',
    '',
  );

  const instruments = useMemo(
    () => instrumentsString.split(',').filter((s) => s.length > 0),
    [instrumentsString],
  );

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label={'Filter parcels'}>
          <>
            <SideNavFilter label={'Experiment session date'}>
              <>
                <DatePicker
                  className="form-control"
                  dateFormat={UI_DATE_DAY_FORMAT}
                  selected={parseDate(startDate)}
                  maxDate={parseDate(endDate)}
                  onChange={(date) => {
                    setStartDate(date ? formatDateToIcatDate(date) : '');
                  }}
                  portalId="root"
                  isClearable
                  placeholderText="Start date"
                />
                <DatePicker
                  className="form-control"
                  dateFormat={UI_DATE_DAY_FORMAT}
                  selected={parseDate(endDate)}
                  minDate={parseDate(startDate)}
                  onChange={(date) => {
                    setEndDate(date ? formatDateToIcatDate(date) : '');
                  }}
                  portalId="root"
                  isClearable
                  placeholderText="End date"
                />
              </>
            </SideNavFilter>
            <SideNavFilter label={'Beamline'}>
              <SelectInstruments
                selectedInstruments={instruments}
                onChange={(v) =>
                  setInstrumentsString(v.map((i) => i.name).join(','))
                }
                forUser={false}
              />
            </SideNavFilter>
          </>
        </SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>
        <RequestAndFilter
          startDate={startDate}
          endDate={endDate}
          instruments={instruments}
        />
      </Suspense>
    </WithSideNav>
  );
}

function RequestAndFilter({
  startDate,
  endDate,
  instruments,
}: {
  startDate: string;
  endDate: string;
  instruments: string[];
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    default: [] as Parcel[],
  });

  const filteredParcels = useMemo(() => {
    return parcels.filter((p) => {
      if (!p.investigation) return false;
      const instrumentName = p.investigation?.instrument?.name;
      if (!instrumentName || instrumentName?.toLocaleUpperCase() === 'ID00')
        return false;
      const investigationStartDate = parseDate(p.investigation?.startDate);
      const investigationEndDate = parseDate(p.investigation?.endDate);
      if (!investigationStartDate || !investigationEndDate) return false;

      const start = parseDate(startDate);
      const end = parseDate(endDate);
      if (start && investigationEndDate < start) return false;

      if (end && investigationStartDate > end) return false;

      if (
        instruments.length > 0 &&
        (!instrumentName || !instruments.includes(instrumentName))
      ) {
        return false;
      }
      return true;
    });
  }, [parcels, startDate, endDate, instruments]);

  return (
    <Row className="g-2">
      <Col xs={12}>
        <Alert variant="warning">
          Test parcels are filtered. A parcel is considered as test if the
          beamline associated is ID00.
        </Alert>
      </Col>
      <Col xs={12}>
        <LogisticsStatisticsSummary parcels={filteredParcels} />
      </Col>
      <Col xs={12}>
        <LogisticsStatisticsParcelStatus parcels={filteredParcels} />
      </Col>
      <Col xs={12}>
        <LogisticsStatisticsInvestigationTable parcels={filteredParcels} />
      </Col>
    </Row>
  );
}
