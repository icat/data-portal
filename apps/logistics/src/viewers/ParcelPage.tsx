import { Button, Loading, useParam, usePath } from '@edata-portal/core';
import { faAnglesLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ParcelAdminSection } from 'components/investigation/shipment/parcel/ParcelAdminSection';
import { ParcelBarcodePage } from 'components/investigation/shipment/parcel/ParcelBarcodePage';
import { ParcelContentEditor } from 'components/investigation/shipment/parcel/ParcelContent';
import { ParcelHistory } from 'components/investigation/shipment/parcel/ParcelHistory';
import { ParcelInfo } from 'components/investigation/shipment/parcel/ParcelInfo';
import { ParcelStatusChart } from 'components/investigation/shipment/parcel/ParcelStatusChart';
import { Suspense } from 'react';

import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ParcelPage() {
  const investigationId = usePath('investigationId');
  const parcelId = usePath('parcelId');

  const [fromBarcode, setFromBarcode] = useParam('fromBarcode', '', true, true);

  if (fromBarcode?.length) {
    return (
      <ParcelBarcodePage
        investigationId={investigationId}
        parcelId={parcelId}
        onSeeContent={() => {
          setFromBarcode(undefined);
        }}
      />
    );
  }

  return (
    <>
      <Suspense fallback={<Loading />}>
        <Row className="g-3">
          <Col xs={12}>
            <Link to={`/investigation/${investigationId}/logistics`}>
              <Button size="sm">
                <FontAwesomeIcon icon={faAnglesLeft} className="me-2" />
                Back to parcels
              </Button>
            </Link>
          </Col>
          <Col xs={12}>
            <ParcelStatusChart
              investigationId={investigationId}
              parcelId={parcelId}
            />
          </Col>
          <Col xs={12}>
            <ParcelInfo investigationId={investigationId} parcelId={parcelId} />
          </Col>
          <Col xs={12}>
            <Suspense fallback={<Loading />}>
              <ParcelContentEditor
                investigationId={investigationId}
                parcelId={parcelId}
              />
            </Suspense>
          </Col>

          <Col xs={12}>
            <ParcelHistory
              investigationId={investigationId}
              parcelId={parcelId}
            />
          </Col>
          <Col xs={12}>
            <ParcelAdminSection
              investigationId={investigationId}
              parcelId={parcelId}
            />
          </Col>
        </Row>
      </Suspense>
    </>
  );
}
