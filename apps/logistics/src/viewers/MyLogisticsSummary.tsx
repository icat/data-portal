import {
  Parcel,
  PARCEL_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { OngoingParcelsSummary } from 'components/summary/OngoingParcelsSummary.tsx';
import { PARCEL_STATUSES } from 'constants/statuses';
import { useMemo } from 'react';
import { Col } from 'react-bootstrap';

export default function MyLogisticsSummary() {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      limit: 100,
    },
    default: [] as Parcel[],
  });

  const ongoingParcels = useMemo(
    () =>
      parcels.filter(
        (parcel) =>
          !(
            [PARCEL_STATUSES.BACK_USER, PARCEL_STATUSES.DESTROYED] as string[]
          ).includes(parcel.status),
      ),
    [parcels],
  );

  if (!ongoingParcels.length) {
    return <>Nothing currently going on.</>;
  }

  return (
    <Col>
      <OngoingParcelsSummary parcels={parcels} />
    </Col>
  );
}
