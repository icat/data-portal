import { ParcelList } from 'components/investigation/shipment/parcel/ParcelList';
import { Container, Row } from 'react-bootstrap';

export default function MyParcels() {
  return (
    <Container fluid>
      <Row>
        <h4>My parcels</h4>
      </Row>
      <Row>
        <ParcelList />
      </Row>
    </Container>
  );
}
