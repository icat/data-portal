import {
  useGetEndpoint,
  GET_ADDRESSES_ENDPOINT,
  ShipmentAddress,
} from '@edata-portal/icat-plus-api';
import { AddressInfo } from 'components/address/AddressCard';
import {
  EditAddressButton,
  NewAddressButton,
} from 'components/address/AddressFormModal';
import { RemoveAddressButton } from 'components/address/AddressList';
import { Alert, Container, Row, Col, Card } from 'react-bootstrap';

export default function MyAddresses() {
  const addresses = useGetEndpoint({
    endpoint: GET_ADDRESSES_ENDPOINT,
    default: [] as ShipmentAddress[],
  });

  return (
    <Container fluid>
      <Row>
        <h4>My addresses</h4>
      </Row>
      <Row>
        <Col>
          <NewAddressButton variant="primary" />
        </Col>
      </Row>
      <hr />
      <Row>
        {!addresses.length && (
          <Col>
            <Alert>No addresses found for this account.</Alert>
          </Col>
        )}
        {addresses.map((address) => (
          <Col xs={12} md={6} lg={4} xxl={3} key={address._id}>
            <Card>
              <Card.Header>
                <Row>
                  <Col
                    xs={'auto'}
                    style={{
                      marginRight: 'auto',
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <strong>{address.investigationName}</strong>
                  </Col>
                  <Col xs={'auto'}>
                    <EditAddressButton address={address} />
                  </Col>
                  <Col xs={'auto'}>
                    <RemoveAddressButton address={address} />
                  </Col>
                </Row>
              </Card.Header>

              <Card.Body>
                <AddressInfo address={address} />
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}
