import { Loading, usePath } from '@edata-portal/core';
import { SampleChanger } from 'components/investigation/changer/SampleChanger';
import { Suspense } from 'react';

export default function InvestigationSampleChanger() {
  const investigationId = usePath('investigationId');

  return (
    <>
      <Suspense fallback={<Loading />}>
        <SampleChanger investigationId={investigationId} />
      </Suspense>
    </>
  );
}
