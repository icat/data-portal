import {
  InvestigationOpenBtn,
  TanstackBootstrapTable,
  formatDateToDay,
} from '@edata-portal/core';
import type {
  Investigation,
  Parcel,
  ParcelContent,
} from '@edata-portal/icat-plus-api';
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { useMemo } from 'react';

export function LogisticsStatisticsInvestigationTable({
  parcels,
}: {
  parcels: Parcel[];
}) {
  const parcelByInvestigation = useMemo(
    () =>
      Object.values(
        parcels.reduce(
          (acc, parcel) => {
            const investigation = parcel.investigation;
            if (!investigation || !investigation.id) return acc;
            if (!acc[investigation.id])
              acc[investigation.id] = {
                investigation: investigation,
                parcels: [],
              };
            acc[investigation.id].parcels.push(parcel);
            return acc;
          },
          {} as {
            [key: number]: {
              investigation: Investigation;
              parcels: Parcel[];
            };
          },
        ),
      ),
    [parcels],
  );

  const table = useReactTable({
    columns: [
      {
        header: 'Experiment session',
        accessorKey: 'investigation.name',
        enableSorting: false,
        enableColumnFilter: true,
        cell: (row) => (
          <InvestigationOpenBtn
            investigation={row.row.original.investigation}
          />
        ),
      },
      {
        header: 'Beamline',
        accessorKey: 'investigation.instrument.name',
        enableSorting: false,
        enableColumnFilter: true,
      },
      {
        header: 'Start',
        accessorKey: 'investigation.startDate',
        cell: (row) => formatDateToDay(row.getValue()),
        enableSorting: true,
        enableColumnFilter: false,
      },
      {
        header: 'End',
        accessorKey: 'investigation.endDate',
        cell: (row) => formatDateToDay(row.getValue()),
        enableSorting: true,
        enableColumnFilter: false,
      },
      {
        header: 'Parcels',
        accessorFn: (row) => getNbParcels(row.parcels),
        enableSorting: true,
        enableColumnFilter: false,
      },
      {
        header: 'Items',
        accessorFn: (row) => getNbItems(row.parcels),
        enableSorting: true,
        enableColumnFilter: false,
      },
      {
        header: 'Samples',
        accessorFn: (row) => getNbSamples(row.parcels),
        enableSorting: true,
        enableColumnFilter: false,
      },
    ],
    data: parcelByInvestigation,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
  });

  return (
    <TanstackBootstrapTable
      table={table}
      dataLength={parcelByInvestigation.length}
      sorting
    />
  );
}

function getNbParcels(parcels: Parcel[]) {
  return parcels.length || 0;
}

function getContentRecursive(content: ParcelContent[]): ParcelContent[] {
  if (!content?.length) return [];
  const items = content.flatMap((item) =>
    item?.content?.length ? item.content : [],
  );

  return [...content, ...getContentRecursive(items)];
}

function getParcelItems(parcels: Parcel[], recursive = false) {
  return parcels.flatMap((parcel) => {
    if (!parcel?.content?.length) return [];
    if (!recursive) return parcel.content;
    return getContentRecursive(parcel.content);
  });
}

function getNbItems(parcels: Parcel[]) {
  return getParcelItems(parcels).length;
}
function getNbSamples(parcels: Parcel[]) {
  return getParcelItems(parcels, true).filter((item) => item.sampleId).length;
}
