import { MetadataTable, PlotWidget } from '@edata-portal/core';
import type { Parcel, ParcelContent } from '@edata-portal/icat-plus-api';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { PARCEL_STATUSES, getParcelStatus } from 'constants/statuses';
import { useMemo } from 'react';
import { Card, Col, Row } from 'react-bootstrap';

export function LogisticsStatisticsSummary({ parcels }: { parcels: Parcel[] }) {
  const items = useMemo(() => {
    return getParcelsItems(parcels);
  }, [parcels]);

  const itemsByType = useMemo(() => {
    return items.reduce(
      (acc, item) => {
        const type = item.type;
        if (!acc[type]) acc[type] = [];
        acc[type].push(item);
        return acc;
      },
      {} as { [key: string]: ParcelContent[] },
    );
  }, [items]);

  const parcelsByStatus = useMemo(() => {
    return parcels.reduce(
      (acc, parcel) => {
        const status = parcel.status;
        if (!acc[status]) acc[status] = [];
        acc[status].push(parcel);
        return acc;
      },
      {} as { [key: string]: Parcel[] },
    );
  }, [parcels]);

  const beamlines = useMemo(() => {
    return parcels
      .map((p) => p.investigation?.instrument?.name || 'Unknown')
      .filter((v, i, a) => a.indexOf(v) === i)
      .sort();
  }, [parcels]);

  const parcelsByBeamline = useMemo(() => {
    return beamlines.reduce(
      (acc, beamline) => {
        const res = parcels.filter(
          (p) => p.investigation?.instrument?.name === beamline,
        );
        if (res.length > 0) acc[beamline] = res;
        return acc;
      },
      {} as { [key: string]: Parcel[] },
    );
  }, [beamlines, parcels]);

  const parcelsAtBeamline = useMemo(() => {
    return beamlines.reduce(
      (acc, beamline) => {
        const res = parcels.filter(
          (p) =>
            p.investigation?.instrument?.name === beamline &&
            p.status.toLocaleUpperCase().includes('BEAMLINE'),
        );
        if (res.length > 0) acc[beamline] = res;
        return acc;
      },
      {} as { [key: string]: Parcel[] },
    );
  }, [beamlines, parcels]);

  return (
    <Card>
      <Card.Header>Summary</Card.Header>
      <Card.Body>
        <Row className="g-2">
          <Col xs={12} md={4}>
            <MetadataTable
              parameters={[
                {
                  caption: 'Number of parcels',
                  value: parcels.length,
                },
                {
                  caption: 'Number of beamlines',
                  value: parcels
                    .map((p) => p.investigation?.instrument?.name)
                    .filter((v, i, a) => a.indexOf(v) === i).length,
                },
                {
                  caption: 'Number of items',
                  value: items.length || '0',
                },
                ...Object.keys(itemsByType).map((type) => ({
                  caption: `Number of ${type}`,
                  value: itemsByType[type].length || '0',
                })),
              ]}
            />
          </Col>
          <Col xs={12} md={4}>
            <MetadataTable
              parameters={[
                ...Object.keys(PARCEL_STATUSES).map((status) => {
                  const statusDefinition = getParcelStatus(status);
                  return {
                    caption: (
                      <>
                        {statusDefinition && (
                          <FontAwesomeIcon
                            icon={statusDefinition.icon}
                            className="me-2"
                          />
                        )}
                        {statusDefinition?.label || status}
                      </>
                    ),
                    value: parcelsByStatus[status]?.length || '0',
                  };
                }),
              ]}
            />
          </Col>
          <Col xs={12} md={4}>
            <PlotWidget
              compact
              useResizeHandler
              layout={{
                title: 'Parcels currently on beamlines',
              }}
              data={[
                {
                  type: 'pie',
                  labels: Object.keys(parcelsAtBeamline),
                  values: Object.values(parcelsAtBeamline).map((v) => v.length),
                  texttemplate: '%{label}: %{value}',
                  textposition: 'inside',
                },
              ]}
              config={{
                displayModeBar: false,
              }}
            />
          </Col>
          <Col xs={12}>
            <PlotWidget
              useResizeHandler
              layout={{
                title: 'Parcel per beamline',
                barmode: 'group',
                legend: {
                  y: 80,
                  orientation: 'h',
                },
                yaxis: { title: 'Parcels' },
                yaxis2: {
                  title: 'Items',
                  overlaying: 'y',
                  side: 'right',
                  showgrid: false,
                },
              }}
              data={[
                {
                  type: 'bar',
                  x: Object.keys(parcelsByBeamline),
                  y: Object.values(parcelsByBeamline).map((v) => v.length),
                  name: 'Parcels',
                  yaxis: 'y',
                  ...{ offsetgroup: 1 },
                },
                {
                  type: 'bar',
                  x: Object.keys(parcelsByBeamline),
                  y: Object.values(parcelsByBeamline).map(
                    (v) => getParcelsItems(v).length || 0,
                  ),
                  name: 'Items',
                  yaxis: 'y2',
                  ...{ offsetgroup: 2 },
                },
              ]}
            />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
}

function getParcelsItems(parcels: Parcel[]) {
  function flattenContent(parcelContent: ParcelContent): ParcelContent[] {
    if (!parcelContent.content?.length) return [parcelContent];
    return [
      parcelContent,
      ...parcelContent.content.flatMap((c) => flattenContent(c)),
    ];
  }
  return parcels.flatMap((p) => {
    if (!p.content?.length) return [];
    return p.content.flatMap((c) => flattenContent(c));
  });
}
