import {
  PlotWidget,
  formatDateToIcatDate,
  parseDate,
} from '@edata-portal/core';
import type { Parcel } from '@edata-portal/icat-plus-api';
import { PARCEL_STATUSES } from 'constants/statuses';
import { useMemo } from 'react';
import { Card } from 'react-bootstrap';

export function LogisticsStatisticsParcelStatus({
  parcels,
}: {
  parcels: Parcel[];
}) {
  const { dates, counts } = useMemo(() => {
    return getStatusesDatesAndCounts(parcels);
  }, [parcels]);

  return (
    <Card>
      <Card.Header>Status of the parcels</Card.Header>
      <Card.Body>
        <PlotWidget
          useResizeHandler
          layout={{
            legend: {
              y: 80,
              orientation: 'h',
            },
            yaxis: { title: 'Parcels' },
            hovermode: 'x unified',
          }}
          data={Object.keys(PARCEL_STATUSES).map((status) => ({
            type: 'scattergl',
            x: dates,
            y: counts.map((statuses) => statuses[status] || 0),
            name: status,
          }))}
        />
      </Card.Body>
    </Card>
  );
}

function getStatusesDatesAndCounts(parcels: Parcel[]) {
  const changesAtDates: {
    [date: string]: {
      [status: string]: number;
    };
  } = {};

  const parcelsLastStatus: { [parcelId: string]: string } = {};

  parcels
    .flatMap((p) =>
      p.statuses
        .map((s) => ({
          ...s,
          parcel: p,
          updateDate: parseDate(s.updatedAt),
        }))
        .sort(
          (a, b) =>
            (a.updateDate?.getTime() || 0) - (b.updateDate?.getTime() || 0),
        ),
    )
    .forEach((s) => {
      const date = formatDateToIcatDate(s.updateDate);

      if (!date) return;

      if (!changesAtDates[date]) changesAtDates[date] = {};
      if (!changesAtDates[date][s.status]) changesAtDates[date][s.status] = 0;
      changesAtDates[date][s.status] += 1;
      const previousStatus = parcelsLastStatus[s.parcel.id];
      if (previousStatus) {
        if (!changesAtDates[date][previousStatus])
          changesAtDates[date][previousStatus] = 0;
        changesAtDates[date][previousStatus] -= 1;
      }
      parcelsLastStatus[s.parcel.id] = s.status;
    });

  const dates = Object.keys(changesAtDates).sort();

  const cumulativeChangesAtDates = dates.reduce(
    (acc, date) => {
      const changes = changesAtDates[date];
      const cumulativeChanges = Object.keys(changes).reduce(
        (acc, status) => {
          const change = changes[status];
          acc[status] = (acc[status] || 0) + change;
          return acc;
        },
        { ...acc[acc.length - 1] },
      );
      acc.push(cumulativeChanges);
      return acc;
    },
    [{} as { [key: string]: number }],
  );

  return {
    dates,
    counts: cumulativeChangesAtDates,
  };
}
