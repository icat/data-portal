import { EditableText, useConfig } from '@edata-portal/core';
import {
  UPDATE_PARCEL_ENDPOINT,
  useMutateEndpoint,
  type Parcel,
} from '@edata-portal/icat-plus-api';
import { getParcelStatus, PARCEL_STATUSES } from 'constants/statuses';
import { useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import ReactSelect from 'react-select';

export function ParcelBarcodePageTransport({
  parcel,
  investigationId,
}: {
  parcel: Parcel;
  investigationId: string;
}) {
  const config = useConfig();
  const status = getParcelStatus(parcel.status);
  const facilityTransporters = config.ui.logistics.facilityForwarderNamePickup;
  const options = [
    ...facilityTransporters.map((transporter: string) => ({
      label: transporter,
      value: transporter,
    })),
    { label: 'Other', value: 'Other' },
  ];

  const [selectedForwarder, setSelectedForwarder] = useState<string>();
  const [otherForwarder, setOtherForwarder] = useState<string>('');
  const [trackingNumber, setTrackingNumber] = useState<string>('');

  const { mutate } = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId: investigationId,
      shipmentId: parcel.shipmentId,
    },
  });

  if (
    !config.ui.logistics.transportOrganizationEnabled ||
    !status ||
    parcel.status !== PARCEL_STATUSES.BACK_STORES
  ) {
    return null;
  }

  return (
    <Row className="g-2">
      <Col xs={12}>
        <ReactSelect
          options={options}
          value={
            selectedForwarder
              ? { label: selectedForwarder, value: selectedForwarder }
              : null
          }
          onChange={(option) => {
            const newValue = option ? option.label : undefined;
            setSelectedForwarder(newValue);
            mutate({
              body: {
                ...parcel,
                returnTrackingInformation: {
                  ...parcel.returnTrackingInformation,
                  forwarder: newValue,
                },
              },
            });
          }}
          isClearable={true}
          placeholder="Select forwarder"
        />
      </Col>
      {selectedForwarder === 'Other' && (
        <Col xs={12}>
          <EditableText
            value={otherForwarder}
            onSave={(e) => {
              setOtherForwarder(e);
              mutate({
                body: {
                  ...parcel,
                  returnTrackingInformation: {
                    ...parcel.returnTrackingInformation,
                    forwarder: e,
                  },
                },
              });
            }}
            label={'Forwarder name'}
            type="input"
          />
        </Col>
      )}
      <Col xs={12}>
        <EditableText
          value={trackingNumber}
          onSave={(e) => {
            setTrackingNumber(e);
            mutate({
              body: {
                ...parcel,
                returnTrackingInformation: {
                  ...parcel.returnTrackingInformation,
                  trackingNumber: e,
                },
              },
            });
          }}
          label={'Tracking number'}
          type="input"
        />
      </Col>
    </Row>
  );
}
