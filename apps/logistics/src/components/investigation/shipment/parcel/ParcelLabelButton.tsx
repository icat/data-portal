import {
  GET_PARCEL_LABEL_ENDPOINT,
  Parcel,
  useEndpointURL,
} from '@edata-portal/icat-plus-api';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getParcelStatus } from 'constants/statuses';
import { OverlayTrigger, Tooltip, Button } from 'react-bootstrap';

export function ParcelLabelButton({ parcel }: { parcel: Parcel }) {
  const status = getParcelStatus(parcel.status);

  const getLabelsUrl = useEndpointURL(GET_PARCEL_LABEL_ENDPOINT);

  if (!status?.sendable) {
    return null;
  }

  return (
    <OverlayTrigger
      placement="auto"
      overlay={
        <Tooltip id="tooltip">{'Get label PDF for this parcel'}</Tooltip>
      }
    >
      <Button
        size="sm"
        key={'btn'}
        href={getLabelsUrl({
          investigationId: String(parcel.investigationId),
          parcelId: parcel.id,
        })}
        onClick={(e) => e.stopPropagation()}
      >
        <FontAwesomeIcon
          style={{
            marginRight: '0.5rem',
          }}
          icon={faDownload}
        />
        Download label
      </Button>
    </OverlayTrigger>
  );
}
