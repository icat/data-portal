import { first, formatDateToDay } from '@edata-portal/core';
import {
  useGetEndpoint,
  PARCEL_LIST_ENDPOINT,
  type Parcel,
} from '@edata-portal/icat-plus-api';
import { UpdateStatusButton } from 'components/investigation/shipment/parcel/UpdateStatusButton';
import { ParcelStatusBadge } from 'components/investigation/shipment/parcel/ParcelStatusBadge';
import {
  ENTRY_POINT_ACTIONS,
  PARCEL_STATUS_DEFS,
  ParcelStatusType,
  getParcelStatus,
} from 'constants/statuses';
import { Alert, Card } from 'react-bootstrap';
import { useMemo } from 'react';

export function ParcelStatusChart({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
    default: [] as Parcel[],
  });

  const parcel = useMemo(() => first(parcels), [parcels]);

  if (!parcel) {
    return <Alert variant="info">Could not find parcel</Alert>;
  }

  const status = getParcelStatus(parcel.status);

  if (!status) {
    return <Alert variant="info">Parcel has invalid status</Alert>;
  }

  const canChangeTo = status.allowedActions
    ? status.allowedActions(parcel)
    : status.nextActions || [];

  const nodes = getNodes();

  return (
    <Card>
      <Card.Header>Parcel status</Card.Header>
      <Card.Body className="p-2">
        <div className="d-flex flex-row gap-2 flex-wrap align-items-center">
          {nodes.map((steps, i) => {
            const isTransitionActive = steps.some((step) =>
              canChangeTo.includes(step),
            );

            return (
              <div
                key={i}
                className="d-flex flex-row gap-2 align-items-center flex-nowrap"
              >
                {i !== 0 ? (
                  <div
                    style={{
                      width: 0,
                      height: 0,
                      borderBottom: '10px solid transparent',
                      borderTop: '10px solid transparent',
                      borderLeft: `10px solid ${
                        isTransitionActive ? 'black' : 'lightgrey'
                      }`,
                    }}
                  />
                ) : null}
                <div className="d-flex flex-column gap-1">
                  {steps.map((step) => {
                    const parcelChangesToStep = parcel.statuses.filter(
                      (status) => status.status === step,
                    );

                    const lastChangeToStep =
                      parcelChangesToStep[parcelChangesToStep.length - 1];

                    const changeDate = formatDateToDay(
                      lastChangeToStep?.updatedAt,
                    );

                    if (canChangeTo.includes(step)) {
                      return (
                        <UpdateStatusButton
                          key={step}
                          status={step}
                          parcel={parcel}
                        />
                      );
                    }
                    return (
                      <ParcelStatusBadge
                        key={step}
                        status={step}
                        isCurrent={step === parcel.status}
                        date={changeDate}
                      />
                    );
                  })}
                </div>
              </div>
            );
          })}
        </div>
      </Card.Body>
    </Card>
  );
}

function getNodes() {
  function getNextSteps(prevSteps: ParcelStatusType[][]) {
    const last = prevSteps[prevSteps.length - 1];
    const nextSteps = last.flatMap((step) => {
      const status = PARCEL_STATUS_DEFS[step];
      return status.nextActions ?? [];
    });
    const res = nextSteps.filter((v) => !prevSteps.flat().includes(v));

    if (res.length === 0) {
      return prevSteps;
    }
    return getNextSteps([...prevSteps, res]);
  }

  return getNextSteps([[ENTRY_POINT_ACTIONS]]);
}
