import { useUser } from '@edata-portal/core';
import {
  INVESTIGATION_USERS_LIST_ENDPOINT,
  Parcel,
  UPDATE_PARCEL_STATUS_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ParcelLabelButton } from 'components/investigation/shipment/parcel/ParcelLabelButton';
import { ParcelStatusBadge } from 'components/investigation/shipment/parcel/ParcelStatusBadge';
import { getParcelStatus } from 'constants/statuses';
import React, { useMemo, useState } from 'react';

import {
  Alert,
  Button,
  OverlayTrigger,
  Tooltip,
  Modal,
  Form,
} from 'react-bootstrap';

export function UpdateStatusButton({
  status,
  parcel,
  size = 'sm',
}: {
  status: string;
  parcel: Parcel;
  size?: 'sm' | 'lg' | undefined;
}) {
  const statusObj = getParcelStatus(status);

  const user = useUser();

  const investigationUsers = useGetEndpoint({
    endpoint: INVESTIGATION_USERS_LIST_ENDPOINT,
    params: {
      investigationId: parcel.investigationId.toString(),
    },
  });

  const isLocalContact = useMemo(() => {
    if (!investigationUsers || !investigationUsers.length) {
      return false;
    }

    return investigationUsers.some(
      (investigationUser) =>
        investigationUser.role === 'Local contact' &&
        investigationUser.name === user?.name,
    );
  }, [user, investigationUsers]);

  const isStaff = user?.isAdministrator || isLocalContact;

  const { mutate } = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_STATUS_ENDPOINT,
    params: {
      investigationId: parcel.investigationId.toString(),
      parcelId: parcel.id,
      status,
    },
  });

  const [showModal, setShowModal] = React.useState(false);

  if (!statusObj) {
    return <ParcelStatusBadge status={status} />;
  }

  const { action } = statusObj;

  if (!action) {
    return <ParcelStatusBadge status={status} />;
  }

  if (!isStaff && action.role === 'staff') {
    return <ParcelStatusBadge status={status} />;
  }

  const ModalComponent = action.requiresSendInfo ? SendInfoModal : undefined;

  const modalElement = ModalComponent ? (
    <ModalComponent
      show={showModal}
      setShow={setShowModal}
      onConfirm={(v) => {
        mutate({ body: v });
      }}
      title={action.desc}
      parcel={parcel}
    />
  ) : null;

  return (
    <>
      <OverlayTrigger
        overlay={<Tooltip key={'desc'}>{action.desc}</Tooltip>}
        placement="auto"
      >
        <Button
          key={action.label}
          size={size}
          onClick={() => {
            if (modalElement) {
              setShowModal(true);
            } else {
              mutate({ body: {} });
            }
          }}
        >
          <FontAwesomeIcon style={{ marginRight: 5 }} icon={statusObj.icon} />
          {action.label}
        </Button>
      </OverlayTrigger>
      {modalElement}
    </>
  );
}

function SendInfoModal({
  onConfirm,
  show,
  setShow,
  parcel,
}: {
  onConfirm: (values: any) => void;
  show: boolean;
  setShow: (show: boolean) => void;
  title: string;
  parcel: Parcel;
}) {
  const [instructionsRead, setInstructionsRead] = useState(false);

  const [certify, setCertify] = useState(false);
  const [containsDangerousGoods, setContainsDangerousGoods] = useState<
    boolean | undefined
  >(undefined);

  const filled = certify && containsDangerousGoods !== undefined;

  const [trackingNumber, setTrackingNumber] = useState('');
  const [forwarderName, setForwarderName] = useState('');

  if (!instructionsRead) {
    return (
      <Modal show={show} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>
            <h5>Sending instructions</h5>
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <p>
            <strong>Before</strong> sending your parcel,{' '}
            <strong>you must</strong>:
          </p>
          <ol>
            <li>
              Download and print your shipping label
              <br />
              <ParcelLabelButton parcel={parcel} />
            </li>
            <li>
              Affix the <strong>outward-bound</strong> address label to your
              parcel
            </li>
            <li>
              Insert the <strong>return-bound</strong> address label in your
              parcel
            </li>
            <li>Complete the paperwork for your courier company. </li>
            <li>
              Request the <b>courier company return label</b> and put it in your
              parcel, otherwise it will not be returned to you.
            </li>
          </ol>

          <Alert variant="danger">
            Please note that any parcel <u>without</u> a label will{' '}
            <u>not be accepted</u> by the ESRF Stores.
          </Alert>
        </Modal.Body>

        <Modal.Footer>
          <Button variant={'light'} onClick={() => setShow(false)}>
            Cancel
          </Button>
          <Button onClick={() => setInstructionsRead(true)}>Done</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  return (
    <Modal show={show} onHide={() => setShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          <h5>Certify parcel content</h5>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form.Check
          type="switch"
          id="switch-herby"
          checked={certify}
          label={'I hereby certify that:'}
          onChange={(e) => {
            setCertify(e.currentTarget.checked);
          }}
        />
        <ul>
          <li>The parcel does not contains any radioactive sources.</li>
          <li>
            The parcel contains only samples that have been declared in the
            Proposal/ Sample-sheet form(s) and that have been approved by the
            ESRF safety group.
          </li>
          <li>
            The parcel will be sent according to national and international
            transport regulations and customs regulations, with support of my
            local safety and logistics referents, where possible.
          </li>
          <li>
            I acknowledge that in no event shall the ESRF be held liable for the
            loss of (and any damage caused to) any content of the parcel sent by
            the user.
          </li>
        </ul>
        <ForwarderForm
          trackingNumber={trackingNumber}
          setTrackingNumber={setTrackingNumber}
          forwarderName={forwarderName}
          setForwarderName={setForwarderName}
        />
        <h4>Dangerous goods</h4>
        <p>
          This declaration is <strong>NOT</strong> replacing the necessary
          parcel content declaration paperwork requested by the transporter &
          customs.
        </p>
        <Alert
          style={{
            marginTop: '1rem',
          }}
          variant="warning"
        >
          It is your responsibility to ensure that the parcel content
          declaration is compliant with international regulations and that the
          appropriate paperworks are provided to the corresponding authorities
          in due time.
        </Alert>
        <p>
          Please consult the safety referent of your Laboratory/Company or the
          Transport Company to help you on this matter if you have any doubt.
        </p>
        <Form.Check
          type="radio"
          label="Yes, this parcel contains dangerous goods"
          name="dangerous-goods"
          id="dangerous-goods-yes"
          checked={containsDangerousGoods === true}
          onChange={() => setContainsDangerousGoods(true)}
        />
        <Form.Check
          type="radio"
          label="No, I certify that this parcel does not contain dangerous goods in regard of International regulations."
          name="dangerous-goods"
          id="dangerous-goods-no"
          checked={containsDangerousGoods === false}
          onChange={() => setContainsDangerousGoods(false)}
        />
      </Modal.Body>

      <Modal.Footer>
        <Button variant={'light'} onClick={() => setShow(false)}>
          Cancel
        </Button>
        <Button
          disabled={!filled}
          onClick={() =>
            onConfirm({
              containsDangerousGoods,
              shippingTrackingInformation: {
                trackingNumber: trackingNumber,
                forwarder: forwarderName,
              },
            })
          }
        >
          Mark as SENT
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export function ForwarderForm({
  trackingNumber,
  setTrackingNumber,
  forwarderName,
  setForwarderName,
}: {
  trackingNumber: string;
  setTrackingNumber: (value: string) => void;
  forwarderName: string;
  setForwarderName: (value: string) => void;
}) {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="tracking-number">
        <Form.Label>Tracking number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Tracking number"
          value={trackingNumber}
          onChange={(e) => setTrackingNumber(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="tracking-number">
        <Form.Label>Forwarder name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Forwarder name"
          value={forwarderName}
          onChange={(e) => setForwarderName(e.target.value)}
        />
      </Form.Group>
    </Form>
  );
}
