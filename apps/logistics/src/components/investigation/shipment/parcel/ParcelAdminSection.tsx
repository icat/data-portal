import { getParcelStatus, PARCEL_STATUS_DEFS } from 'constants/statuses';
import {
  PARCEL_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';

import { Card, Alert, Row, Col } from 'react-bootstrap';
import { UpdateStatusButton } from 'components/investigation/shipment/parcel/UpdateStatusButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { first, useUser } from '@edata-portal/core';
import { useMemo } from 'react';

export function ParcelAdminSection({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
  });

  const parcel = useMemo(() => first(parcels), [parcels]);

  const user = useUser();

  if (!user?.isAdministrator) {
    return null;
  }
  if (!parcel) {
    return null;
  }
  const status = getParcelStatus(parcel.status);

  if (status === PARCEL_STATUS_DEFS.SCHEDULED) {
    return null;
  }

  return (
    <Card>
      <Card.Header>Admin section</Card.Header>
      <Card.Body>
        <Row>
          <Col
            xs={'auto'}
            style={{
              alignItems: 'center',
            }}
          >
            <Row>
              <Col xs={'auto'}>
                <b>Change the status to: </b>
              </Col>
            </Row>
            <Row>
              <Col xs={'auto'}>
                <UpdateStatusButton parcel={parcel} status="SCHEDULED" />
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col>
                <Alert variant="light">
                  <p>
                    <FontAwesomeIcon
                      icon={faInfoCircle}
                      style={{ marginRight: 10 }}
                    />
                    This will set the parcel back in editable mode.
                  </p>
                  <hr />
                  <p>
                    To be used when the user has made mistakes during the parcel
                    preparation and cannot correct them.
                  </p>
                </Alert>
              </Col>
            </Row>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
}
