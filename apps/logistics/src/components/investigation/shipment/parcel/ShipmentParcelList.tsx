import { ParcelList } from 'components/investigation/shipment/parcel/ParcelList';

export function ShipmentParcelList({
  shipmentId,
  investigationId,
}: {
  shipmentId: string;
  investigationId: string;
}) {
  return (
    <ParcelList shipmentId={shipmentId} investigationId={investigationId} />
  );
}
