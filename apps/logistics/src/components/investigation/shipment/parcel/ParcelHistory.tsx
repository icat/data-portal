import {
  formatDateToDayAndTime,
  parseDate,
  immutableArray,
  first,
} from '@edata-portal/core';
import {
  ParcelStatus,
  PARCEL_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';

import React, { useMemo } from 'react';
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from '@tanstack/react-table';
import type { ColumnDef } from '@tanstack/react-table';

import { Card } from 'react-bootstrap';
import { TanstackBootstrapTable } from '@edata-portal/core';
import { ParcelStatusBadge } from 'components/investigation/shipment/parcel/ParcelStatusBadge';

export function ParcelHistory({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
  });
  const parcel = useMemo(() => first(parcels), [parcels]);

  const tableData = React.useMemo(
    () =>
      immutableArray(parcel?.statuses)
        .sort(
          (a, b) =>
            (parseDate(b.updatedAt)?.getTime() || 0) -
            (parseDate(a.updatedAt)?.getTime() || 0),
        )
        .toArray(),
    [parcel],
  );
  const columns: ColumnDef<ParcelStatus>[] = [
    {
      accessorKey: 'status',
      cell: (info) => {
        return <ParcelStatusBadge status={info.row.original.status} />;
      },
      header: 'Status',
      footer: 'Status',
    },
    {
      accessorKey: 'updatedAt',
      cell: (info) => {
        return (
          <div
            style={{
              whiteSpace: 'nowrap',
            }}
          >
            {formatDateToDayAndTime(info.getValue() as string)}
          </div>
        );
      },
      header: 'Updated at',
      footer: 'Updated at',
    },
    {
      accessorKey: 'createdByFullName',
      header: 'Updated by',
      footer: 'Updated by',
    },
    {
      accessorKey: 'comments',
      header: 'Comments',
      footer: 'Comments',
    },
  ];

  const table = useReactTable<ParcelStatus>({
    data: tableData,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getRowId: (row) => row.id,
    autoResetAll: false,
  });

  return (
    <Card>
      <Card.Header>History</Card.Header>
      <Card.Body>
        <TanstackBootstrapTable table={table} dataLength={tableData.length} />
      </Card.Body>
    </Card>
  );
}
