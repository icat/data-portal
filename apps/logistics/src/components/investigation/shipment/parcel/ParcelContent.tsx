import { Loading } from '@edata-portal/core';
import {
  useGetEndpoint,
  GET_TRACKING_SETUP_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { ParcelContentContainers } from 'components/investigation/shipment/parcel/content/ParcelContentContainers';

import React, { Suspense } from 'react';

import { Button, Card, Col, Row } from 'react-bootstrap';

const ParcelContentTable = React.lazy(
  () =>
    import(
      'components/investigation/shipment/parcel/content/ParcelContentTable'
    ),
);

export function ParcelContentEditor({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const [view, setView] = React.useState<'table' | 'containers'>('table');

  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const canHaveContainers = !!setup?.containerTypes?.length;

  return (
    <Card>
      <Card.Header>Content</Card.Header>

      <Card.Body>
        <Col>
          {canHaveContainers && (
            <Row className="g-1">
              <Col xs={'auto'}>
                <Button
                  variant="outline-primary"
                  onClick={() => setView('table')}
                  active={view === 'table'}
                  size="sm"
                >
                  Table
                </Button>
              </Col>
              <Col xs={'auto'}>
                <Button
                  variant="outline-primary"
                  onClick={() => setView('containers')}
                  active={view === 'containers'}
                  size="sm"
                >
                  Containers
                </Button>
              </Col>
              <Col xs={12}>
                <hr className="mt-0 mb-2 mt-1" />
              </Col>
            </Row>
          )}
          <Row>
            <Col>
              <Suspense fallback={<Loading />}>
                {view === 'table' && (
                  <ParcelContentTable
                    investigationId={investigationId}
                    parcelId={parcelId}
                  />
                )}
                {view === 'containers' && (
                  <ParcelContentContainers
                    investigationId={investigationId}
                    parcelId={parcelId}
                  />
                )}
              </Suspense>
            </Col>
          </Row>
        </Col>
      </Card.Body>
    </Card>
  );
}
