import { Button, first } from '@edata-portal/core';
import {
  PARCEL_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faBoxOpen } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AddressCard } from 'components/address/AddressCard';
import { ParcelBarcodePageStatus } from 'components/investigation/shipment/parcel/ParcelBarcodePageStatus';
import { ParcelBarcodePageTransport } from 'components/investigation/shipment/parcel/ParcelBarcodePageTransport';
import { ParcelStatusBadge } from 'components/investigation/shipment/parcel/ParcelStatusBadge';
import { useMemo } from 'react';
import { Alert, Col, Container, Row } from 'react-bootstrap';

export function ParcelBarcodePage({
  parcelId,
  investigationId,
  onSeeContent,
}: {
  parcelId: string;
  investigationId: string;
  onSeeContent: () => void;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
  });

  const parcel = useMemo(() => first(parcels), [parcels]);

  if (!parcel) {
    return <Alert variant="info">Could not find parcel</Alert>;
  }

  return (
    <Container>
      <Row className="g-2">
        <Col xs={12}>
          <Row>
            <Col xs="auto">
              <strong>Status: </strong>
            </Col>
            <Col xs="auto">
              <ParcelStatusBadge status={parcel.status} />
            </Col>
          </Row>
        </Col>
        <Col xs={12}>
          {parcel?.returnAddress ? (
            <AddressCard
              address={parcel?.returnAddress}
              title="Return address"
              investigationId={investigationId}
              isReturnAddress
              editable={false}
            />
          ) : (
            <strong>No return address</strong>
          )}
        </Col>

        <Col xs={12}>
          <ParcelBarcodePageTransport
            parcel={parcel}
            investigationId={investigationId}
          />
        </Col>
        <Col xs={12}>
          <ParcelBarcodePageStatus parcel={parcel} />
        </Col>
        <hr />
        <Col xs={12}>
          <Button size="sm" onClick={onSeeContent}>
            <FontAwesomeIcon icon={faBoxOpen} className="me-2" />
            See content
          </Button>
        </Col>
      </Row>
    </Container>
  );
}
