import type { Parcel } from '@edata-portal/icat-plus-api';
import { UpdateStatusButton } from 'components/investigation/shipment/parcel/UpdateStatusButton';
import { getParcelStatus } from 'constants/statuses';
import { Alert, Col, Row } from 'react-bootstrap';

export function ParcelBarcodePageStatus({ parcel }: { parcel: Parcel }) {
  const status = getParcelStatus(parcel.status);

  if (!status) {
    return <Alert variant="info">Parcel has invalid status</Alert>;
  }

  const canChangeTo = status.allowedActions
    ? status.allowedActions(parcel)
    : status.nextActions || [];

  return (
    <Row className="g-2">
      {canChangeTo.map((nextStatus) => (
        <Col key={nextStatus} xs={12}>
          <UpdateStatusButton parcel={parcel} status={nextStatus} size="lg" />
        </Col>
      ))}
    </Row>
  );
}
