import { faBoxOpen } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type { CellContext, ColumnDef } from '@tanstack/react-table';
import { getCoreRowModel, useReactTable } from '@tanstack/react-table';
import {
  InvestigationDate,
  Loading,
  NoData,
  SearchBar,
  SideNavElement,
  SideNavFilter,
  useConfig,
  useEndpointPagination,
  UserPortalButton,
  useUser,
  WithSideNav,
} from '@edata-portal/core';
import {
  Parcel,
  PARCEL_LIST_ENDPOINT,
  ShipmentAddress,
} from '@edata-portal/icat-plus-api';
import { TanstackBootstrapTable } from '@edata-portal/core';
import { useNavigate } from 'react-router-dom';
import { ParcelStatusBadge } from 'components/investigation/shipment/parcel/ParcelStatusBadge';
import { AddressInfo } from 'components/address/AddressCard';
import { Col, OverlayTrigger, Popover } from 'react-bootstrap';
import { Suspense, useState } from 'react';
import { PARCEL_STATUSES } from 'constants/statuses';
import ReactSelect from 'react-select';

export function ParcelList({
  shipmentId,
  investigationId,
}: {
  shipmentId?: string;
  investigationId?: string;
}) {
  const [search, setSearch] = useState<string>();
  const [status, setStatus] = useState<string>();

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label={'Parcels'}>
          <>
            <SideNavFilter label={'Search'}>
              <SearchBar
                value={search}
                onUpdate={(v) => setSearch(v)}
                placeholder={'Search parcels'}
              />
            </SideNavFilter>
            <SideNavFilter label={'Status'}>
              <ReactSelect
                isClearable
                options={Object.keys(PARCEL_STATUSES).map((status) => ({
                  value: status,
                  label: <ParcelStatusBadge status={status} />,
                }))}
                onChange={(v) => setStatus(v?.value)}
                menuPortalTarget={document.body}
                styles={{
                  menu: (base) => ({
                    ...base,
                    width: 'max-content',
                    minWidth: '100%',
                  }),
                }}
              />
            </SideNavFilter>
          </>
        </SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>
        <LoadAndDisplayParcels
          shipmentId={shipmentId}
          investigationId={investigationId}
          search={search?.trim()}
          status={status}
        />
      </Suspense>
    </WithSideNav>
  );
}

function LoadAndDisplayParcels({
  shipmentId,
  investigationId,
  search,
  status,
}: {
  shipmentId?: string;
  investigationId?: string;
  search?: string;
  status?: string;
}) {
  const user = useUser();
  const config = useConfig();

  const parcels = useEndpointPagination({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      ...(search?.length ? { search } : {}),
      ...(status?.length ? { status } : {}),
      ...(shipmentId && investigationId
        ? {
            shipmentId,
            investigationId,
          }
        : undefined),
    },
    paginationParams: {
      paginationKey: 'parcels',
    },
  });

  const investigationColumns: ColumnDef<Parcel>[] = [
    {
      accessorKey: 'investigation.instrument.name',
      header: 'Beamline',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row: any) => row,
      header: 'A-Form',
      cell: (info: any) => (
        <UserPortalButton
          investigationId={(info.getValue() as Parcel).investigationId}
          investigation={(info.getValue() as Parcel).investigation}
        />
      ),
      enableColumnFilter: false,
    },
    ...(user?.isAdministrator
      ? [
          {
            accessorFn: (row: any) => row.localContactFullnames.join(' \n'),
            cell: (info: any) =>
              info.getValue() && typeof info.getValue() === 'string' ? (
                <>
                  {(info.getValue() as string)
                    .split('\n')
                    .map((name: string) => (
                      <div key={name}>{name}</div>
                    ))}
                </>
              ) : null,
            header: 'Local contact',
            enableColumnFilter: false,
          },
        ]
      : []),
    {
      accessorFn: (row: any) => row,
      header: 'Start date',
      cell: (info: any) => (
        <InvestigationDate
          investigation={(info.getValue() as Parcel).investigation}
          investigationId={(info.getValue() as Parcel).investigationId}
        />
      ),
      enableColumnFilter: false,
    },
  ];

  const columns: ColumnDef<Parcel>[] = [
    {
      accessorKey: 'id',
      cell: () => <ParcelOpenBtn />,
      id: 'open',
      header: '',
      enableColumnFilter: false,
    },
    {
      accessorKey: 'name',
      header: 'Name',
      enableColumnFilter: false,
    },
    ...(!investigationId ? investigationColumns : []),

    {
      accessorFn: (row) => row.content?.length || 0,
      header: 'Items #',
      enableColumnFilter: false,
    },
    {
      accessorKey: 'status',
      cell: (info) => {
        return (
          <div>
            <ParcelStatusBadge status={info.row.original.status} />
          </div>
        );
      },
      header: 'Status',
      enableColumnFilter: false,
    },
    {
      accessorFn: (d) => (d.containsDangerousGoods ? 'Yes' : 'No'),
      cell: (info) => (
        <span className={info.getValue() === 'No' ? '' : 'text-warning'}>
          {info.getValue() as string}
        </span>
      ),
      header: 'Dangerous goods',
      enableColumnFilter: false,
    },
    ...(config.ui.logistics.facilityReimbursmentEnabled
      ? [
          {
            accessorFn: (d: Parcel) => (d.isReimbursed ? 'Yes' : 'No'),
            cell: (info: CellContext<Parcel, unknown>) => (
              <span className={info.getValue() === 'No' ? '' : 'text-success'}>
                {info.getValue() as string}
              </span>
            ),
            header: 'Reimbursed',
            enableColumnFilter: false,
          },
        ]
      : []),
    {
      accessorFn: (d) => (d.returnAddress ? 'Ship back' : 'Destroy'),
      cell: (info) => (
        <span className={info.getValue() === 'Ship back' ? '' : 'text-warning'}>
          {info.getValue() as string}
        </span>
      ),
      header: 'After experiment',
      enableColumnFilter: false,
    },
    ...(user?.isAdministrator
      ? [
          {
            accessorFn: (parcel: any) => parcel.shippingAddress,
            header: 'Sender',
            cell: (info: any) => {
              const value = info.getValue() as ShipmentAddress;
              if (!value) return null;
              return (
                <OverlayTrigger
                  trigger={['hover', 'focus']}
                  overlay={
                    <Popover>
                      <Popover.Body>
                        <AddressInfo address={value} hideName hideAddress />
                      </Popover.Body>
                    </Popover>
                  }
                >
                  <div className="small text-info" key={'address'}>
                    <AddressInfo address={value} hidePhone hideEmail />
                  </div>
                </OverlayTrigger>
              );
            },
            enableColumnFilter: false,
          },
        ]
      : []),
  ];

  const navigate = useNavigate();

  const onRowClick = (parcel: Parcel) => {
    navigate(
      `/investigation/${parcel.investigationId}/logistics/parcel/${parcel.id}`,
    );
  };

  const table = useReactTable<Parcel>({
    data: parcels.data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getRowId: (parcel) => parcel.id,
    autoResetAll: false,
    ...parcels.tablePagination,
  });

  if (!parcels.data?.length)
    return (
      <Col style={{ marginTop: '1rem' }}>
        <NoData />
      </Col>
    );

  return (
    <TanstackBootstrapTable
      table={table}
      onRowClick={onRowClick}
      dataLength={parcels.totalElements}
    />
  );
}

export function ParcelOpenBtn() {
  return <FontAwesomeIcon icon={faBoxOpen} />;
}
