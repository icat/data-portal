import { Button, Modal } from 'react-bootstrap';
import { FormObjectDescription, RenderForm, first } from '@edata-portal/core';
import {
  SHIPMENT_LIST_ENDPOINT,
  UPDATE_PARCEL_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
  Parcel,
  CREATE_PARCEL_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useMemo, useState } from 'react';
import { faEdit, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { STORAGE_CONDITIONS } from 'constants/storage';

export function NewParcelButton({
  onNew,
  shipmentId,
  investigationId,
}: {
  onNew?: (parcel: Parcel) => void;
  shipmentId: string;
  investigationId: string;
}) {
  const [show, setShow] = useState(false);

  const { mutateAsync } = useMutateEndpoint({
    endpoint: CREATE_PARCEL_ENDPOINT,
    params: { investigationId, shipmentId },
  });

  return (
    <>
      <Button
        onClick={() => {
          setShow(true);
        }}
      >
        <FontAwesomeIcon
          icon={faPlus}
          style={{
            marginRight: '0.5rem',
          }}
        />
        New parcel
      </Button>
      <ParcelFormModal
        key={`${show}`}
        show={show}
        setShow={setShow}
        onDone={(v) => {
          mutateAsync({ body: v }).then((parcel) => {
            if (parcel) {
              onNew?.(parcel);
            }
          });
          setShow(false);
        }}
        shipmentId={shipmentId}
        investigationId={investigationId}
      />
    </>
  );
}

export function EditParcelButton({
  onDone,
  parcel,
}: {
  onDone?: (parcel: Parcel) => void;
  parcel: Parcel;
}) {
  const [show, setShow] = useState(false);

  const { mutateAsync } = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId: String(parcel.investigationId),
      shipmentId: parcel.shipmentId,
    },
  });

  return (
    <>
      <Button
        onClick={() => {
          setShow(true);
        }}
        size="sm"
      >
        <FontAwesomeIcon
          icon={faEdit}
          style={{
            marginRight: '0.5rem',
          }}
        />
        Edit parcel
      </Button>
      <ParcelFormModal
        key={`${show}`}
        show={show}
        setShow={setShow}
        onDone={(v) => {
          mutateAsync({ body: v }).then((parcel) => {
            if (parcel) {
              onDone?.(parcel);
            }
          });
          setShow(false);
        }}
        shipmentId={parcel.shipmentId}
        investigationId={String(parcel.investigationId)}
        parcel={parcel}
      />
    </>
  );
}

type ParcelFormType = Pick<
  Parcel,
  | 'name'
  | 'description'
  | 'comments'
  | 'storageConditions'
  | 'shipmentId'
  | 'investigationId'
  | 'shippingAddress'
  | 'returnAddress'
>;
function ParcelFormModal({
  parcel,
  show,
  setShow,
  onDone,
  shipmentId,
  investigationId,
}: {
  parcel?: Parcel;
  show: boolean;
  setShow: (show: boolean) => void;
  onDone?: (address: Parcel) => void;
  shipmentId: string;
  investigationId: string;
}) {
  const shipments = useGetEndpoint({
    endpoint: SHIPMENT_LIST_ENDPOINT,
    params: {
      shipmentId,
      investigationId: investigationId,
    },
  });

  const shipment = useMemo(() => first(shipments), [shipments]);

  const description: FormObjectDescription<ParcelFormType> = {
    name: {
      type: 'text',
      label: 'Name',
      required: true,
      col: { xs: 12 },
    },
    description: {
      type: 'textarea',
      label: 'Description',
      col: { xs: 12 },
    },
    storageConditions: {
      type: 'select',
      label: 'Storage conditions',
      options: STORAGE_CONDITIONS.map((condition) => ({
        value: condition,
        label: condition,
      })),
      required: true,
      col: { xs: 12 },
    },
    comments: ({ rootValue }) => {
      const isOtherStorageCondition = rootValue.storageConditions === 'Other';
      return {
        type: 'textarea',
        label: isOtherStorageCondition
          ? 'Please specify storage conditions'
          : 'Comments',
        col: { xs: 12 },
        required: isOtherStorageCondition,
      };
    },
  };

  const initialValue: ParcelFormType = parcel
    ? parcel
    : ({
        shippingAddress: shipment?.defaultShippingAddress,
        returnAddress: shipment?.defaultReturnAddress,
        storageConditions: STORAGE_CONDITIONS[0],
        investigationId: Number(investigationId),
        shipmentId,
        comments: '',
        description: '',
        name: '',
      } as ParcelFormType);

  return (
    <Modal
      show={show}
      backdrop="static"
      size="lg"
      onHide={() => setShow(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title id="address-modal-title">
          {parcel ? 'Edit' : 'Create'} item
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <RenderForm
          description={description}
          initialValue={initialValue}
          onSubmit={(data) => {
            onDone?.(data as Parcel);
          }}
          submitLabel="Save"
          onCancel={() => setShow(false)}
        />
      </Modal.Body>
    </Modal>
  );
}
