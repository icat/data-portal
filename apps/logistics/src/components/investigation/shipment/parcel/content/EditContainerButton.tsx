import type { ParcelContent, Parcel } from '@edata-portal/icat-plus-api';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ParcelContentContainerEditor } from 'components/investigation/shipment/parcel/content/ParcelContentContainerEditor';
import { useState } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';

export function EditContainerButton({
  container,
  parcel,
  onDone,
  compact = false,
}: {
  container: ParcelContent;
  parcel: Parcel;
  onDone?: (newParcel: Parcel | undefined) => void;
  compact?: boolean;
}) {
  const [editing, setEditing] = useState(false);

  return (
    <>
      <OverlayTrigger
        placement="auto"
        overlay={
          <Tooltip key={'EditContainerButtonTooltip'}>
            {"Edit this container's content"}
          </Tooltip>
        }
      >
        <Button
          key={'EditContainerButton'}
          size={'sm'}
          onClick={() => {
            setEditing(true);
          }}
          variant={compact ? 'link' : 'primary'}
          className={compact ? 'p-0' : ''}
        >
          <FontAwesomeIcon icon={faEdit} />
        </Button>
      </OverlayTrigger>
      {editing && (
        <ParcelContentContainerEditor
          container={container}
          parcel={parcel}
          onCancel={() => {
            setEditing(false);
          }}
          onDone={(newParcel) => {
            setEditing(false);
            if (onDone) {
              onDone(newParcel);
            }
          }}
        />
      )}
    </>
  );
}
