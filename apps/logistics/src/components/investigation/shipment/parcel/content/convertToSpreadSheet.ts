import type { ParcelContent } from '@edata-portal/icat-plus-api';
import {
  CONTAINER_NAME_COLUMN_NAME,
  CONTAINER_POSITION_COLUMN_NAME,
  CONTAINER_TYPE_COLUMN_NAME,
} from 'components/investigation/shipment/parcel/content/tableconfig';
import type { TableConfig } from 'components/investigation/shipment/parcel/content/tablelogic';
import {
  isEmptyValue,
  SpreadSheetItem,
} from 'components/spreadsheet/spreadSheetLogic';
import _ from 'lodash';

export function convertParcelContentToSpreadSheetItems(
  item: ParcelContent,
  config: TableConfig,
  container?: {
    name: string;
    type?: string;
  },
): SpreadSheetItem[] {
  const res: SpreadSheetItem[] = [];

  if (item.type === 'CONTAINER') {
    res.push(
      ...item.content.flatMap((c) =>
        convertParcelContentToSpreadSheetItems(c, config, {
          name: item.name,
          type: item.containerType?.name,
        }),
      ),
    );
  } else {
    const newValue: any = {};

    const { experimentPlan, processingPlan } = item;
    config.sampleColumns.forEach((c) => {
      const name = c.name;
      if (name && name in item) {
        newValue[name] = item[name as keyof ParcelContent];
      }
    });
    if (experimentPlan) {
      config.experimentColumns.forEach((c) => {
        if (c.name) {
          newValue[c.name] = experimentPlan.find(
            (p) => p.key === c.name,
          )?.value;
        }
      });
    }
    if (processingPlan) {
      config.processingColumns.forEach((c) => {
        if (c.name) {
          newValue[c.name] = processingPlan.find(
            (p) => p.key === c.name,
          )?.value;
        }
      });
    }
    if (container) {
      newValue[CONTAINER_NAME_COLUMN_NAME] = container.name;
      newValue[CONTAINER_TYPE_COLUMN_NAME] = container.type;
      newValue[CONTAINER_POSITION_COLUMN_NAME] = item.sampleContainerPosition;
    }
    res.push(newValue);
  }
  return res;
}

export function convertSpreadSheetItemsToParcelContents(
  items: SpreadSheetItem[],
  config: TableConfig,
): (ParcelContent | null)[] {
  const groupedByContainer = _(items)
    .filter((item) => !isEmptyValue(item[CONTAINER_TYPE_COLUMN_NAME]))
    .groupBy((item) => item[CONTAINER_NAME_COLUMN_NAME])
    .value();

  const notInContainer = items
    .filter((item) => isEmptyValue(item[CONTAINER_TYPE_COLUMN_NAME]))
    .map((item) => convertSpreadSheetItemToParcelContent(item, config));

  const containers = Object.keys(groupedByContainer).map((containerName) => {
    const containerItems = groupedByContainer[containerName];
    const containerTypeName = containerItems[0][CONTAINER_TYPE_COLUMN_NAME];
    const containerType = config.containerTypes.find(
      (type) => type.name === containerTypeName,
    );
    return {
      type: 'CONTAINER',
      containerType,
      name: containerName,
      content: containerItems.map((item) =>
        convertSpreadSheetItemToParcelContent(item, config),
      ),
    } as ParcelContent;
  });

  return [...containers, ...notInContainer];
}

export function convertSpreadSheetItemToParcelContent(
  item: SpreadSheetItem,
  config: TableConfig,
): ParcelContent | null {
  if (Object.keys(item).length === 0) return null;
  const res: any = {};
  config.sampleColumns.forEach((c) => {
    if (c.name && c.name in item) {
      res[c.name] = item[c.name];
    }
  });
  res.experimentPlan = [];
  config.experimentColumns.forEach((c) => {
    if (c.name && c.name in item) {
      res.experimentPlan.push({
        key: c.name,
        value: item[c.name],
      });
    }
  });
  res.processingPlan = [];
  config.processingColumns.forEach((c) => {
    if (c.name && c.name in item) {
      res.processingPlan.push({
        key: c.name,
        value: item[c.name],
      });
    }
  });
  res[CONTAINER_POSITION_COLUMN_NAME] = item[CONTAINER_POSITION_COLUMN_NAME];
  return res as ParcelContent;
}
