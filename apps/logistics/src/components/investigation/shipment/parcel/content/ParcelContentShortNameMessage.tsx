import { Alert } from 'react-bootstrap';

export function ParcelContentShortNameMessage() {
  return (
    <Alert variant="info" className="p-2 mt-1 mb-0">
      <small>
        <strong>NOTE:</strong> The short names of the samples are synchronized
        from the User Portal and are only available after the samples have been
        declared on a submitted A Form and have been evaluated by the safety
        group.
      </small>
    </Alert>
  );
}
