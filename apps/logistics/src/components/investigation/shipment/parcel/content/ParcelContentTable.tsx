import {
  useGetEndpoint,
  PARCEL_LIST_ENDPOINT,
  GET_TRACKING_SETUP_ENDPOINT,
  useMutateEndpoint,
  UPDATE_PARCEL_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useCallback, useMemo } from 'react';

import { getParcelStatus } from 'constants/statuses';

import {
  getAllColumns,
  getTableConfig,
} from 'components/investigation/shipment/parcel/content/tableconfig';

import { SpreadSheetTable } from 'components/spreadsheet/SpreadSheetTable';
import type { SpreadSheetItem } from 'components/spreadsheet/spreadSheetLogic';
import {
  convertParcelContentToSpreadSheetItems,
  convertSpreadSheetItemsToParcelContents,
} from 'components/investigation/shipment/parcel/content/convertToSpreadSheet';
import { detectParcelContentErrors } from 'components/investigation/shipment/parcel/content/tablelogic';
import { ParcelContentShortNameMessage } from 'components/investigation/shipment/parcel/content/ParcelContentShortNameMessage';
import { first } from '@edata-portal/core';

export default function ParcelContentTable({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
  });

  const parcel = useMemo(() => first(parcels), [parcels]);

  const updateParcel = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId: parcel?.shipmentId || '',
    },
  });

  const tableConfig = useMemo(() => getTableConfig(setup, false), [setup]);

  const columns = useMemo(() => getAllColumns(tableConfig), [tableConfig]);

  const spreadSheetItems = useMemo(() => {
    if (!parcel || !parcel.content) return [];
    return parcel.content.flatMap((i) =>
      convertParcelContentToSpreadSheetItems(i, tableConfig),
    );
  }, [parcel, tableConfig]);

  const detectExtraItemsErrors = useCallback(
    (items: SpreadSheetItem[]) => {
      return detectParcelContentErrors(items, tableConfig, (item) =>
        items.findIndex((i) => i === item),
      );
    },
    [tableConfig],
  );

  if (!setup) return null;

  const status = parcel ? getParcelStatus(parcel.status) : undefined;

  const editable = status?.editable || false;

  if (!parcel) return null;

  return (
    <>
      <SpreadSheetTable
        name={`parcel-content-${parcel.name}`}
        initialItems={spreadSheetItems}
        columns={columns}
        readonly={!editable}
        onSave={(items: SpreadSheetItem[]) => {
          const res = convertSpreadSheetItemsToParcelContents(
            items,
            tableConfig,
          );

          return updateParcel
            .mutateAsync({
              body: {
                ...parcel,
                content: res,
              },
            })
            .then(() => true)
            .catch(() => false);
        }}
        detectExtraItemsErrors={detectExtraItemsErrors}
        columnGroups={tableConfig.columnsGroups}
        saving={updateParcel.isPending}
      />
      {editable ? <ParcelContentShortNameMessage /> : null}
    </>
  );
}
