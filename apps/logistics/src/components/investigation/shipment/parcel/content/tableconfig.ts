import type {
  InputColumnDescription,
  TrackingSetup,
} from '@edata-portal/icat-plus-api';
import type { TableConfig } from 'components/investigation/shipment/parcel/content/tablelogic';
import { STORAGE_CONDITIONS } from 'constants/storage';

export const SAMPLESHEET_COLUMN_TYPE = 'SAMPLESHEET';
export const SAMPLESHEET_COLUMN_NAME = 'sampleId';

export const SAMPLE_TYPE_COLUMN_NAME = 'type';

export const CONTAINER_TYPE_COLUMN_NAME = 'containerType';
export const CONTAINER_NAME_COLUMN_NAME = 'containerName';
export const CONTAINER_POSITION_COLUMN_NAME = 'sampleContainerPosition';

export const PARCEL_NAME_COLUMN_NAME = 'parcelName';
export const PARCEL_STORAGE_CONDITION_COLUMN_NAME = 'storageCondition';

export function getTableConfig(
  setup: TrackingSetup | undefined,
  showParcelColumns: boolean,
): TableConfig {
  if (!setup) {
    return {
      parcelColumns: [],
      containerColumns: [],
      sampleColumns: [],
      experimentColumns: [],
      processingColumns: [],
      containerTypes: [],
    };
  }

  const containerColumns: InputColumnDescription[] = [];
  if (setup.containerTypes && setup.containerTypes.length > 0) {
    containerColumns.push({
      header: 'Type',
      name: CONTAINER_TYPE_COLUMN_NAME,
      type: 'select',
      values: setup.containerTypes.map((type) => type.name),
      colored: true,
    });
    containerColumns.push({
      header: 'Name',
      name: CONTAINER_NAME_COLUMN_NAME,
      type: 'string',
      fillIncrement: false,
      colored: true,
    });
    containerColumns.push({
      header: 'Sample position',
      name: CONTAINER_POSITION_COLUMN_NAME,
      type: 'string',
      fillIncrement: true,
      colored: false,
    });
  }

  const parcelColumns: InputColumnDescription[] = showParcelColumns
    ? [
        {
          header: 'Parcel',
          name: PARCEL_NAME_COLUMN_NAME,
          type: 'string',
          fillIncrement: true,
          colored: true,
          required: true,
        },
        {
          header: 'Storage condition',
          name: PARCEL_STORAGE_CONDITION_COLUMN_NAME,
          type: 'select',
          values: STORAGE_CONDITIONS.map((condition) => String(condition)),
          colored: false,
          required: true,
        },
      ]
    : [];

  const columnsGroups = [
    {
      header: 'Parcel',
      columns: parcelColumns.length,
    },
    {
      header: 'Container',
      columns: containerColumns.length,
    },
    {
      header: 'Item',
      columns: setup.sampleDescription ? setup.sampleDescription.length : 0,
    },
    {
      header: 'Experiment',
      columns: setup.experimentPlan ? setup.experimentPlan.length : 0,
    },
    {
      header: 'Processing',
      columns: setup.processingPlan ? setup.processingPlan.length : 0,
    },
  ];

  return {
    parcelColumns: parcelColumns,
    containerColumns: containerColumns,
    sampleColumns: setup.sampleDescription ? setup.sampleDescription : [],
    experimentColumns: setup.experimentPlan ? setup.experimentPlan : [],
    processingColumns: setup.processingPlan ? setup.processingPlan : [],
    containerTypes: setup?.containerTypes || [],
    columnsGroups,
  };
}

export function getAllColumns(config: TableConfig): InputColumnDescription[] {
  return [
    ...config.parcelColumns,
    ...config.containerColumns,
    ...config.sampleColumns,
    ...config.experimentColumns,
    ...config.processingColumns,
  ];
}
