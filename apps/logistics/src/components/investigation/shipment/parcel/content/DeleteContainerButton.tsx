import {
  type ParcelContent,
  type Parcel,
  useMutateEndpoint,
  UPDATE_PARCEL_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState, useCallback } from 'react';
import { Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';

export function DeleteContainerButton({
  container,
  parcel,
}: {
  container: ParcelContent;
  parcel: Parcel;
}) {
  const [confirmDelete, setConfirmDelete] = useState(false);

  const updateParcel = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId: parcel?.investigationId.toString(),
      shipmentId: parcel?.shipmentId,
    },
  });

  const onDelete = useCallback(() => {
    setConfirmDelete(false);
    const newParcel = {
      ...parcel,
      content: parcel.content.filter((c) => c._id !== container._id),
    };
    updateParcel.mutate({ body: newParcel });
  }, [container, parcel, updateParcel]);

  return (
    <>
      <OverlayTrigger
        placement="auto"
        overlay={
          <Tooltip key={'DeleteContainerButtonTooltip'}>
            {'Delete this container'}
          </Tooltip>
        }
      >
        <Button key={'DeleteContainerButton'} size={'sm'} variant="danger">
          <FontAwesomeIcon
            onClick={() => {
              setConfirmDelete(true);
            }}
            icon={faTrash}
          />
        </Button>
      </OverlayTrigger>
      <Modal show={confirmDelete} onHide={() => setConfirmDelete(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Delete container</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Are you sure you want to delete this container? The content will be
            lost.
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setConfirmDelete(false)}>
            Cancel
          </Button>
          <Button variant="danger" onClick={onDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
