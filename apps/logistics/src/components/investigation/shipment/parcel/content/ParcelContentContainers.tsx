import { NoData, first } from '@edata-portal/core';
import {
  GET_TRACKING_SETUP_ENDPOINT,
  PARCEL_LIST_ENDPOINT,
  Parcel,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CHANGER_CONFIG } from 'components/investigation/changer/config';
import { DeleteContainerButton } from 'components/investigation/shipment/parcel/content/DeleteContainerButton';
import { EditContainerButton } from 'components/investigation/shipment/parcel/content/EditContainerButton';
import { ParcelContentContainerEditor } from 'components/investigation/shipment/parcel/content/ParcelContentContainerEditor';
import { useMemo, useState } from 'react';
import { Alert, Button, Col, Row } from 'react-bootstrap';

export function ParcelContentContainers({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
  });

  const parcel = useMemo(() => first(parcels), [parcels]);

  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const changerType = useMemo(() => {
    if (!setup?.sampleChangerType) return undefined;
    const changer = setup.sampleChangerType[0]?.name;
    return changer && changer in CHANGER_CONFIG
      ? CHANGER_CONFIG[changer]
      : undefined;
  }, [setup]);

  if (!parcel) {
    return <NoData />;
  }

  if (!changerType) {
    return (
      <Alert variant="warning">
        Sample changer was not configured for this beamline
      </Alert>
    );
  }

  if (!setup) return null;

  if (!parcel) return null;

  return (
    <Col>
      <Row className="mb-2">
        <Col xs={'auto'}>
          <NewContainerButton parcel={parcel} />
        </Col>
      </Row>
      <Row className="g-2">
        {parcel.content.map((parcelContent) => {
          if (!parcelContent.containerType) return null;

          return (
            <Col xs={'auto'} key={parcelContent.id}>
              <div className="p-2 border rounded">
                <Col>
                  <Row className="justify-content-center">
                    <Col xs={'auto'}>
                      <strong>{parcelContent.name}</strong>
                    </Col>
                  </Row>
                  <Row className="justify-content-center">
                    <Col xs={'auto'}>
                      <div
                        style={{
                          position: 'relative',
                          minHeight: 100,
                          minWidth: 100,
                        }}
                      >
                        {changerType.plotContainer(parcelContent)}
                      </div>
                    </Col>
                  </Row>
                  <hr className="my-2" />
                  <Row className="justify-content-between">
                    <Col xs={'auto'}>
                      <EditContainerButton
                        container={parcelContent}
                        parcel={parcel}
                      />
                    </Col>
                    <Col xs={'auto'}>
                      <DeleteContainerButton
                        container={parcelContent}
                        parcel={parcel}
                      />
                    </Col>
                  </Row>
                </Col>
              </div>
            </Col>
          );
        })}
      </Row>
    </Col>
  );
}

function NewContainerButton({ parcel }: { parcel: Parcel }) {
  const [editing, setEditing] = useState(false);

  return (
    <>
      <Button
        size={'sm'}
        onClick={() => {
          setEditing(true);
        }}
      >
        <FontAwesomeIcon icon={faPlus} className="me-2" />
        New container
      </Button>
      {editing && (
        <ParcelContentContainerEditor
          parcel={parcel}
          onCancel={() => {
            setEditing(false);
          }}
          onDone={() => {
            setEditing(false);
          }}
        />
      )}
    </>
  );
}
