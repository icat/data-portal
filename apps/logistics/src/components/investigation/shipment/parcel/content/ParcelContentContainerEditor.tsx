import { Loading, RequiredStar } from '@edata-portal/core';
import {
  GET_TRACKING_SETUP_ENDPOINT,
  useGetEndpoint,
  type Parcel,
  type ParcelContent,
  ContainerType,
  useMutateEndpoint,
  UPDATE_PARCEL_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { ParcelContentShortNameMessage } from 'components/investigation/shipment/parcel/content/ParcelContentShortNameMessage';
import {
  convertParcelContentToSpreadSheetItems,
  convertSpreadSheetItemsToParcelContents,
} from 'components/investigation/shipment/parcel/content/convertToSpreadSheet';
import {
  getAllColumns,
  getTableConfig,
} from 'components/investigation/shipment/parcel/content/tableconfig';
import { detectParcelContentErrors } from 'components/investigation/shipment/parcel/content/tablelogic';
import { SpreadSheetTable } from 'components/spreadsheet/SpreadSheetTable';
import type { SpreadSheetItem } from 'components/spreadsheet/spreadSheetLogic';
import { Suspense, useCallback, useMemo, useState } from 'react';
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';

export function ParcelContentContainerEditor({
  parcel,
  container,
  onDone,
  onCancel,
}: {
  parcel: Parcel;
  container?: ParcelContent;
  onDone: (newParcel: Parcel | undefined) => void;
  onCancel: () => void;
}) {
  const updateParcel = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId: parcel?.investigationId.toString(),
      shipmentId: parcel?.shipmentId,
    },
  });

  const saveContainer = useCallback(
    (container: ParcelContent) => {
      //make sure content does not exceed capacity (if containerType is changed to smaller capacity)
      const newContainer = JSON.parse(
        JSON.stringify(container),
      ) as ParcelContent;
      newContainer.content = newContainer.content.filter(
        (item) =>
          !item.sampleContainerPosition ||
          Number.parseInt(item.sampleContainerPosition) <=
            (newContainer.containerType?.capacity || 0),
      );

      const newContent = parcel.content.map((c) => {
        if (c._id === newContainer._id) {
          return newContainer;
        }
        return c;
      });

      if (!newContent.includes(newContainer)) {
        newContent.push(newContainer);
      }

      const newParcel = {
        ...parcel,
        content: newContent,
      };
      if (JSON.stringify(newParcel) === JSON.stringify(parcel)) return;
      updateParcel.mutateAsync({ body: newParcel }).then((v) => {
        setContainerState(newContainer);
        onDone(v);
      });
    },
    [onDone, parcel, updateParcel],
  );

  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId: parcel.investigationId.toString(),
    },
  });

  const defaultContainer = useMemo(
    () => ({
      name: '',
      type: 'CONTAINER',
      containerType: setup?.containerTypes[0],
      content: [],
    }),
    [setup],
  );

  const [containerState, setContainerState] = useState<ParcelContent>(
    JSON.parse(JSON.stringify(container || defaultContainer)),
  );
  const [contentValid, setContentValid] = useState<boolean>(true);

  const isValid = useMemo(() => {
    return (
      contentValid &&
      containerState.name.length > 0 &&
      containerState.containerType !== undefined
    );
  }, [contentValid, containerState]);

  const onInfoChange = useCallback(
    (info: { name: string; containerType?: ContainerType }) => {
      if (
        info.name === containerState.name &&
        info.containerType === containerState.containerType
      )
        return;
      const newContainer = JSON.parse(
        JSON.stringify(containerState),
      ) as ParcelContent;
      newContainer.name = info.name;
      if (
        newContainer.containerType?.name !== info.containerType?.name ||
        newContainer.containerType?.capacity !== info.containerType?.capacity
      ) {
        newContainer.containerType = info.containerType;
        //if type is changed, unset sampleChangerLocation because it cannot be there anymore
        newContainer.sampleChangerLocation = undefined;
      }

      setContainerState(newContainer);
    },
    [containerState],
  );

  const onContentChange = useCallback(
    (content: ParcelContent[], valid: boolean) => {
      if (JSON.stringify(content) === JSON.stringify(containerState.content))
        return;
      const newContainer = {
        ...containerState,
        content,
      };
      setContainerState(newContainer);
      setContentValid(valid);
    },
    [containerState],
  );

  return (
    <Modal
      show={true}
      onHide={onCancel}
      size="xl"
      centered
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title>
          {container ? <>Edit {container.name}</> : <>New container</>}
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className="p-2">
        <Col>
          <Row>
            <Col>
              <Suspense fallback={<Loading />}>
                <ContainerInfoEditor
                  parcel={parcel}
                  container={containerState}
                  onChange={onInfoChange}
                />
              </Suspense>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col>
              <Suspense fallback={<Loading />}>
                <ContainerContentEditor
                  key={containerState.containerType?.capacity}
                  parcel={parcel}
                  container={containerState}
                  onChange={onContentChange}
                />
              </Suspense>
            </Col>
          </Row>

          <Row className="justify-content-end mt-2 g-2">
            <Col xs={'auto'}>
              <Button
                variant="primary"
                onClick={() => saveContainer(containerState)}
                disabled={!isValid}
              >
                Save
              </Button>
            </Col>
            <Col xs={'auto'}>
              <Button variant="danger" onClick={() => onCancel()}>
                Cancel
              </Button>
            </Col>
          </Row>
        </Col>
      </Modal.Body>
    </Modal>
  );
}

function ContainerInfoEditor({
  parcel,
  container,
  onChange,
}: {
  parcel: Parcel;
  container: ParcelContent;
  onChange: (info: { name: string; containerType?: ContainerType }) => void;
}) {
  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId: parcel.investigationId.toString(),
    },
  });

  const typeOptions = useMemo(() => {
    if (!setup?.containerTypes) return [];
    return setup.containerTypes.map((t) => ({
      label: t.name,
      value: t.name,
    }));
  }, [setup]);

  const findContainerType = useCallback(
    (name?: string) => {
      if (!setup?.containerTypes || !name) return undefined;
      return setup?.containerTypes.find((t) => t.name === name);
    },
    [setup],
  );

  const [containerName, setContainerName] = useState(container.name || '');
  const [containerType, setContainerType] = useState(container.containerType);

  return (
    <Col>
      <Form.Group>
        <Form.Label className="mb-0">
          Name <RequiredStar />
        </Form.Label>
        <Form.Control
          type="text"
          value={containerName}
          onChange={(e) => {
            setContainerName(e.target.value);
            onChange({
              name: e.target.value.trim(),
              containerType,
            });
          }}
        />
      </Form.Group>

      <Form.Group>
        <Form.Label className="mb-0">
          Type <RequiredStar />
        </Form.Label>
        <Form.Select
          value={containerType?.name}
          onChange={(e) => {
            const newType = findContainerType(e.target.value);
            setContainerType(newType);
            onChange({
              name: containerName,
              containerType: newType,
            });
          }}
        >
          {typeOptions.map((t) => (
            <option key={t.value} value={t.value}>
              {t.label}
            </option>
          ))}
        </Form.Select>
      </Form.Group>
    </Col>
  );
}

function ContainerContentEditor({
  parcel,
  container,
  onChange,
}: {
  parcel: Parcel;
  container: ParcelContent;
  onChange: (content: ParcelContent[], valid: boolean) => void;
}) {
  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId: parcel.investigationId.toString(),
    },
  });

  const setupWithoutContainers = useMemo(() => {
    if (!setup) return undefined;
    return {
      ...setup,
      containerTypes: [],
    };
  }, [setup]);

  const containerCapacity = useMemo(() => {
    if (!container || !container.containerType) return undefined;
    return container.containerType.capacity;
  }, [container]);

  const tableConfig = useMemo(
    () => getTableConfig(setupWithoutContainers, false),
    [setupWithoutContainers],
  );

  const columns = useMemo(() => getAllColumns(tableConfig), [tableConfig]);

  const spreadSheetItems = useMemo(() => {
    if (!container || !container.content || !containerCapacity) return [];
    const res: SpreadSheetItem[] = [];
    for (let i = 0; i < containerCapacity; i++) {
      //find item with sampleContainerPosition i+1
      const item = container.content.find((item) => {
        return (
          item.sampleContainerPosition &&
          Number.parseInt(item.sampleContainerPosition) === i + 1
        );
      });
      if (item) {
        res.push(...convertParcelContentToSpreadSheetItems(item, tableConfig));
      } else {
        res.push({});
      }
    }
    return res;
  }, [container, containerCapacity, tableConfig]);

  const detectExtraItemsErrors = useCallback(
    (items: SpreadSheetItem[]) => {
      return detectParcelContentErrors(items, tableConfig, (item) =>
        items.findIndex((i) => i === item),
      );
    },
    [tableConfig],
  );

  if (!setupWithoutContainers) return null;

  return (
    <>
      <SpreadSheetTable
        name={`parcel-content-${parcel.name}`}
        initialItems={spreadSheetItems}
        columns={columns}
        onChanges={(items: SpreadSheetItem[], valid) => {
          const res = convertSpreadSheetItemsToParcelContents(
            items,
            tableConfig,
          )
            .map((item, index) => {
              if (item === null) return null;
              return {
                ...item,
                sampleContainerPosition: String(index + 1),
              } as ParcelContent;
            })
            .filter((item): item is ParcelContent => item !== null);

          onChange(res, valid);
        }}
        detectExtraItemsErrors={detectExtraItemsErrors}
        columnGroups={tableConfig.columnsGroups}
        saving={false}
        maxItems={containerCapacity}
        removeEmptyRows={false}
      />
      <ParcelContentShortNameMessage />
    </>
  );
}
