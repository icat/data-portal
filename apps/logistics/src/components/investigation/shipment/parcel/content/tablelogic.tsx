import type {
  ContainerType,
  InputColumnDescription,
} from '@edata-portal/icat-plus-api';
import {
  CONTAINER_NAME_COLUMN_NAME,
  CONTAINER_POSITION_COLUMN_NAME,
  CONTAINER_TYPE_COLUMN_NAME,
  getAllColumns,
  SAMPLESHEET_COLUMN_NAME,
  SAMPLESHEET_COLUMN_TYPE,
  SAMPLE_TYPE_COLUMN_NAME,
  PARCEL_NAME_COLUMN_NAME,
  PARCEL_STORAGE_CONDITION_COLUMN_NAME,
} from 'components/investigation/shipment/parcel/content/tableconfig';
import {
  isEmptyValue,
  SpreadSheetError,
  SpreadSheetItem,
} from 'components/spreadsheet/spreadSheetLogic';
import _ from 'lodash';

export type TableConfig = {
  parcelColumns: InputColumnDescription[];
  containerColumns: InputColumnDescription[];
  sampleColumns: InputColumnDescription[];
  experimentColumns: InputColumnDescription[];
  processingColumns: InputColumnDescription[];
  containerTypes: ContainerType[];
  columnsGroups?: {
    header: string;
    columns: number;
  }[];
};

export function detectParcelErrors(
  items: SpreadSheetItem[],
  config: TableConfig,
  getItemRow: (item: SpreadSheetItem) => number,
): SpreadSheetError[] {
  const parcelNames = items
    .map((item) => item[PARCEL_NAME_COLUMN_NAME])
    .filter(
      (value, index, self) =>
        !isEmptyValue(value) && self.indexOf(value) === index,
    );

  return parcelNames.flatMap((parcelName) => {
    const parcelItems = items.filter(
      (item) => item[PARCEL_NAME_COLUMN_NAME] === parcelName,
    );

    const uniqueStorageConditions = parcelItems
      .map((item) => item[PARCEL_STORAGE_CONDITION_COLUMN_NAME])
      .filter(
        (value, index, self) =>
          !isEmptyValue(value) && self.indexOf(value) === index,
      );

    if (uniqueStorageConditions.length > 1)
      return parcelItems.map((item) => ({
        row: getItemRow(item),
        column: getAllColumns(config).findIndex(
          (c) => c.name === PARCEL_NAME_COLUMN_NAME,
        ),
        message: `Inconsistent storage conditions for parcel '${parcelName}' (found values: ${uniqueStorageConditions.join(
          ', ',
        )}).`,
      }));
    return [];
  });
}

export function detectParcelContentErrors(
  items: SpreadSheetItem[],
  config: TableConfig,
  getItemRow: (item: SpreadSheetItem) => number,
): SpreadSheetError[] {
  return [
    ...detectContainerErrors(items, config, getItemRow),
    ...items.flatMap((item) =>
      detectItemErrors(item, getItemRow(item), config),
    ),
  ];
}

function detectContainerErrors(
  items: SpreadSheetItem[],
  config: TableConfig,
  getItemRow: (item: SpreadSheetItem) => number,
): SpreadSheetError[] {
  const containerErrors: SpreadSheetError[] = [];

  const containersByName = _(items)
    .filter((item) => !isEmptyValue(item[CONTAINER_TYPE_COLUMN_NAME]))
    .groupBy((item) => item[CONTAINER_NAME_COLUMN_NAME])
    .value();

  Object.keys(containersByName).forEach((containerName) => {
    const containers = containersByName[containerName];

    //check that all containers have the same type
    const containerTypes = containers.map(
      (container) => container[CONTAINER_TYPE_COLUMN_NAME],
    );
    const uniqueContainerTypes = [...new Set(containerTypes)];
    if (uniqueContainerTypes.length > 1) {
      containers.forEach((container) => {
        containerErrors.push({
          row: getItemRow(container),
          column: getAllColumns(config).findIndex(
            (c) => c.name === CONTAINER_NAME_COLUMN_NAME,
          ),
          message: `Inconsistent container type for container '${containerName}' (found types: ${uniqueContainerTypes.join(
            ', ',
          )}).`,
        });
      });
    }

    //check that container position is unique
    const containerPositions = containers.map(
      (container) => container[CONTAINER_POSITION_COLUMN_NAME],
    );
    const countPositions = containerPositions.reduce(
      (acc, curr) => {
        acc[curr] = (acc[curr] || 0) + 1;
        return acc;
      },
      {} as { [key: string]: number },
    );
    Object.keys(countPositions).forEach((position) => {
      if (countPositions[position] > 1) {
        containers.forEach((container) => {
          if (container[CONTAINER_POSITION_COLUMN_NAME] === position) {
            containerErrors.push({
              row: getItemRow(container),
              column: getAllColumns(config).findIndex(
                (c) => c.name === CONTAINER_POSITION_COLUMN_NAME,
              ),
              message: `Container position should be unique within a container`,
            });
          }
        });
      }
    });

    //check that number of item in container is not greater than container capacity
    const numberOfContainers = containers.length;
    const containerType = containers[0][CONTAINER_TYPE_COLUMN_NAME];
    const containerCapacity = config.containerTypes.find(
      (type) => type.name === containerType,
    )?.capacity;
    if (containerCapacity && numberOfContainers > containerCapacity) {
      containers.forEach((container) => {
        containerErrors.push({
          row: getItemRow(container),
          column: getAllColumns(config).findIndex(
            (c) => c.name === CONTAINER_NAME_COLUMN_NAME,
          ),
          message: `Too many items in container '${containerName}' (found: ${numberOfContainers}, max: ${containerCapacity} for type ${containerType}).`,
        });
      });
    }
  });

  return containerErrors;
}

function detectItemErrors(
  item: SpreadSheetItem,
  row: number,
  config: TableConfig,
): SpreadSheetError[] {
  const errors: SpreadSheetError[] = [];

  //empty line has no errors
  if (Object.keys(item).length === 0) return errors;

  // if type is 'SAMPLESHEET' Sample_sheet is required
  if (item[SAMPLE_TYPE_COLUMN_NAME] === SAMPLESHEET_COLUMN_TYPE) {
    if (isEmptyValue(item[SAMPLESHEET_COLUMN_NAME])) {
      errors.push({
        row,
        column: getAllColumns(config).findIndex(
          (c) => c.name === SAMPLESHEET_COLUMN_NAME,
        ),
        message: `Select one from the SAMPLE FORMS declared in the A form for that session using the pull down menu.`,
      });
    }
  }

  // if type is not 'SAMPLESHEET' Sample_sheet should not be provided
  if (item[SAMPLE_TYPE_COLUMN_NAME] !== SAMPLESHEET_COLUMN_TYPE) {
    if (!isEmptyValue(item[SAMPLESHEET_COLUMN_NAME])) {
      errors.push({
        row,
        column: getAllColumns(config).findIndex(
          (c) => c.name === SAMPLESHEET_COLUMN_NAME,
        ),
        message: `Sample should not be provided for type ${item[SAMPLE_TYPE_COLUMN_NAME]}.`,
      });
    }
  }

  //if container type is provided, container name and position are required
  if (!isEmptyValue(item[CONTAINER_TYPE_COLUMN_NAME])) {
    if (isEmptyValue(item[CONTAINER_NAME_COLUMN_NAME])) {
      errors.push({
        row,
        column: getAllColumns(config).findIndex(
          (c) => c.name === CONTAINER_NAME_COLUMN_NAME,
        ),
        message: `Container name is required if container type is provided.`,
      });
    }
    if (isEmptyValue(item[CONTAINER_POSITION_COLUMN_NAME])) {
      errors.push({
        row,
        column: getAllColumns(config).findIndex(
          (c) => c.name === CONTAINER_POSITION_COLUMN_NAME,
        ),
        message: `Container position is required if container type is provided.`,
      });
    }
  }

  //if container type is not provided, container name and position should not be provided
  if (isEmptyValue(item[CONTAINER_TYPE_COLUMN_NAME])) {
    if (!isEmptyValue(item[CONTAINER_NAME_COLUMN_NAME])) {
      errors.push({
        row,
        column: getAllColumns(config).findIndex(
          (c) => c.name === CONTAINER_NAME_COLUMN_NAME,
        ),
        message: `Container name should not be provided if container type is not provided.`,
      });
    }
    if (!isEmptyValue(item[CONTAINER_POSITION_COLUMN_NAME])) {
      errors.push({
        row,
        column: getAllColumns(config).findIndex(
          (c) => c.name === CONTAINER_POSITION_COLUMN_NAME,
        ),
        message: `Container position should not be provided if container type is not provided.`,
      });
    }
  }

  return errors;
}
