import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  ParcelStatusType,
  PARCEL_STATUSES,
  PARCEL_STATUS_DEFS,
} from 'constants/statuses';

import './ParcelStatusBadge.css';
import { Col, Container, Row } from 'react-bootstrap';

export function ParcelStatusBadge({
  status,
  isCurrent,
  date,
}: {
  status: string;
  isCurrent?: boolean;
  date?: string;
}) {
  if (!(status in PARCEL_STATUSES)) {
    return null;
  }

  const statusObj = PARCEL_STATUS_DEFS[status as ParcelStatusType];

  const text = isCurrent ? (
    <strong className={`text-info activeParcelStatus`}>
      {statusObj.label}
    </strong>
  ) : (
    <small className={`inactiveParcelStatus`}>{statusObj.label}</small>
  );

  return (
    <Container fluid className="p-0 m-0">
      <Row className="g-2 flex-nowrap">
        <Col xs={'auto'}>
          <FontAwesomeIcon icon={statusObj.icon} />
        </Col>
        <Col xs={'auto'} className="parcelStatusBadge">
          <Row>
            <Col>{text}</Col>
          </Row>
          <Row>
            <Col>
              <small className="parcelStatusBadgeDate">{date}</small>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}
