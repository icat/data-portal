import { Form, Modal, Alert } from 'react-bootstrap';
import { Button, CopyValue, useConfig } from '@edata-portal/core';
import { useState } from 'react';

export function ParcelReimbursementModal({
  facilityForwarderAccount,
  facilityForwarderName,
  reference,
  show,
  setShow,
  onSaved,
  onCancel,
}: {
  facilityForwarderAccount: string;
  facilityForwarderName: string;
  reference: string;
  show: boolean;
  setShow: (show: boolean) => void;
  onSaved: () => void;
  onCancel: () => void;
}) {
  const [acknowledgeConditions, setAcknowledgeConditions] = useState(false);
  const config = useConfig();
  const facilityName = config.ui.facilityName;
  return (
    <Modal show={show} onHide={() => setShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          <h5>Acknowledge the conditions for the reimbursement</h5>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          By setting this parcel to reimbursed, the labels that will be
          generated for sending the parcel by courier will use the{' '}
          {facilityName} account.
        </p>
        <Alert variant="danger">
          <p>
            Before proceeding, ensure that your parcel is eligible for{' '}
            {facilityName} reimbursement by checking our userguide.
          </p>
          <p>
            You <strong>must not</strong> use this account to ship more than the
            allowed parcels, or any other equipment for this or any other
            experiment. Any abuse of this account will immediately result in
            your proposal being refused access to the parcel reimbursement
            procedure, and eventually to the {facilityName} beamlines.
          </p>
        </Alert>
        <p>
          For {facilityName} reimbursement, you <strong>must</strong>:
        </p>
        <ul>
          <li>
            Copy and paste the following information into the forwarder
            documents
          </li>
          <ul>
            <li>
              {facilityForwarderName} account number:{' '}
              <CopyValue
                label={`${facilityForwarderName} account`}
                type="value"
                value={facilityForwarderAccount}
              />{' '}
            </li>
            <li>
              Your reference:{' '}
              <CopyValue label="Reference" type="value" value={reference} />
            </li>
          </ul>
          <li>
            Select the option <i>Include a return label</i>.
          </li>
        </ul>
        <p>
          Please click on the following checkbox if you agree with these
          conditions and you wish to have this parcel automatically reimbursed
          by the {facilityName}.
        </p>
        <Form.Check
          type="switch"
          label="I agree, please set this parcel to reimbursed."
          name="agreeReimbursedConditions"
          id="switch-agree-reimbursed-conditions"
          checked={acknowledgeConditions}
          onChange={(e) => {
            setAcknowledgeConditions(true);
          }}
        />
      </Modal.Body>

      <Modal.Footer>
        <Button
          variant={'light'}
          onClick={() => {
            setShow(false);
            onCancel();
          }}
        >
          Cancel
        </Button>
        <Button disabled={!acknowledgeConditions} onClick={() => onSaved()}>
          Done
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
