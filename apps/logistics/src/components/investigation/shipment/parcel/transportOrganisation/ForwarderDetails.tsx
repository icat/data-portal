import { Parcel } from '@edata-portal/icat-plus-api';
import { Col, Row } from 'react-bootstrap';
import {
  formatDateToDay,
  MetadataTable,
  useBreakpointValue,
} from '@edata-portal/core';

export default function ForwarderDetails({ parcel }: { parcel: Parcel }) {
  const metadata1 = [
    {
      caption: 'Forwarder name',
      value: parcel.returnParcelInformation?.forwarderName,
    },
    {
      caption: 'Forwarder account',
      value: parcel.returnParcelInformation?.forwarderAccount,
    },
  ];
  const metadata2 = [
    {
      caption: 'Planned pickup date',
      value: parcel.returnParcelInformation?.plannedPickupDate
        ? formatDateToDay(parcel.returnParcelInformation?.plannedPickupDate)
        : '',
    },
    {
      caption: 'Instructions for stores',
      value: parcel.returnInstructions,
    },
  ];

  const metadataElement = useBreakpointValue({
    xs: (
      <Row>
        <Col>
          <MetadataTable parameters={[...metadata1, ...metadata2]} />
        </Col>
      </Row>
    ),
    md: (
      <Row className="g-2">
        <Col xs={6}>
          <MetadataTable parameters={metadata1} />
        </Col>
        <Col xs={6}>
          <MetadataTable parameters={metadata2} />
        </Col>
      </Row>
    ),
  });

  return metadataElement;
}
