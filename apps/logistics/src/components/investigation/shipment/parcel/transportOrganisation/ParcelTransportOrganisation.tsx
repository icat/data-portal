import {
  Investigation,
  Parcel,
  UPDATE_PARCEL_ENDPOINT,
  useMutateEndpoint,
  RETURN_PARCEL_TYPE_ORGANIZE,
  RETURN_PARCEL_TYPE_MANAGE,
} from '@edata-portal/icat-plus-api';
import { INVESTIGATION_REIMBURSED_PARCELS_PARAM } from 'constants/investigation';
import { useState } from 'react';
import { Col, Row, Form, Card } from 'react-bootstrap';
import { Button, formatDateToIcatDate, useConfig } from '@edata-portal/core';
import { ParcelReimbursementModal } from 'components/investigation/shipment/parcel/transportOrganisation/ParcelReimbursementModal';
import ReimbursedParcelDetails from 'components/investigation/shipment/parcel/transportOrganisation/ReimbursedParcelDetails';
import ForwarderDetails from 'components/investigation/shipment/parcel/transportOrganisation/ForwarderDetails';
import { useReimbursedParcels } from 'components/hooks/reimbursedParcels';
import ForwarderDetailsModal, {
  ForwarderDetailsFormType,
} from 'components/investigation/shipment/parcel/transportOrganisation/ForwarderDetailsModal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

export function ParcelTransportOrganisation({
  investigation,
  parcel,
}: {
  investigation: Investigation;
  parcel: Parcel;
}) {
  const config = useConfig();

  const totalReimbursedParcels = useReimbursedParcels({
    investigationId: investigation.id.toString(),
    shipmentId: parcel.shipmentId,
  });
  const { mutate } = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
      shipmentId: parcel.shipmentId,
    },
  });

  const [paidByFacility, setPaidByFacility] = useState<boolean | undefined>(
    parcel.isReimbursed,
  );
  const [pickup, setPickup] = useState<boolean | undefined>(
    parcel.returnParcelInformation?.returnType === RETURN_PARCEL_TYPE_ORGANIZE,
  );
  const [parcelPapersManaged, setParcelPapersManaged] = useState<
    boolean | undefined
  >(parcel.returnParcelInformation?.returnType === RETURN_PARCEL_TYPE_MANAGE);
  const [
    facilityReimbursedAcknowledgeRead,
    setFacilityReimbursedAcknowledgeRead,
  ] = useState(true);

  const [showModal, setShowModal] = useState(false);
  const [showForwarderDetails, setShowForwarderDetails] = useState(false);

  const nbInvReimbursed =
    Number(investigation?.parameters[INVESTIGATION_REIMBURSED_PARCELS_PARAM]) ||
    0;

  const canBeReimbursed = totalReimbursedParcels < nbInvReimbursed;

  const facilityName = config.ui.facilityName;
  const reference = `${investigation?.name}/${investigation?.instrument?.name}/${formatDateToIcatDate(investigation?.startDate)}`;
  const facilityForwarderAccount = config.ui.logistics.facilityForwarderAccount;
  const facilityForwarderName = config.ui.logistics.facilityForwarderName;

  const transportDocumentsText = `Transport documents: All parcels sent to ${facilityName} must have the ${facilityName} label, the forwarder label and the proforma attached. The same documents required for the parcel's return must be put inside the parcel.`;
  const faciltyTransporters =
    config.ui.logistics.facilityForwarderNamePickup.join(', ');
  const displayReimbursed = canBeReimbursed || parcel.isReimbursed;
  if (
    !config.ui.logistics.transportOrganizationEnabled ||
    (!displayReimbursed && !parcel.returnAddress)
  ) {
    return null;
  }
  const updateForwarderDetails = (data: ForwarderDetailsFormType) => {
    const strDate = formatDateToIcatDate(data.plannedPickupDate) || undefined;
    mutate({
      body: {
        ...parcel,
        returnInstructions: data.instructions,
        returnParcelInformation: {
          returnType: RETURN_PARCEL_TYPE_ORGANIZE,
          forwarderName: data.forwarderName,
          forwarderAccount: data.forwarderAccount,
          plannedPickupDate: strDate,
        },
      },
    });
    setShowForwarderDetails(false);
  };

  const cancelForwarderDetails = () => {
    if (!parcel?.returnParcelInformation?.forwarderName) {
      setParcelPapersManaged(true);
      setPickup(false);
      mutate({
        body: {
          ...parcel,
          returnParcelInformation: {
            returnType: RETURN_PARCEL_TYPE_MANAGE,
            forwarderName: undefined,
            plannedPickupDate: undefined,
          },
          returnInstructions: undefined,
        },
      });
    }
    setShowForwarderDetails(false);
  };

  return (
    <>
      {!facilityReimbursedAcknowledgeRead && (
        <ParcelReimbursementModal
          facilityForwarderAccount={facilityForwarderAccount}
          facilityForwarderName={facilityForwarderName}
          reference={reference}
          show={showModal}
          setShow={setShowModal}
          onCancel={() => {
            setFacilityReimbursedAcknowledgeRead(false);
            setPaidByFacility(false);
            mutate({
              body: {
                ...parcel,
                isReimbursed: false,
              },
            });
          }}
          onSaved={() => {
            setFacilityReimbursedAcknowledgeRead(true);
          }}
        />
      )}
      <Card style={{ marginTop: '8px' }}>
        <Card.Title style={{ marginLeft: '4px' }}>
          Transport organisation
        </Card.Title>
        <Card.Body>
          <Row>
            <Col>
              <div className="fst-italic">{transportDocumentsText}</div>
            </Col>
          </Row>
          {config.ui.logistics.facilityReimbursmentEnabled &&
            displayReimbursed && (
              <>
                <Row className="mt-1">
                  <Col>
                    <p>
                      <b>
                        Transport (of samples only) paid by the {facilityName}
                      </b>
                    </p>
                  </Col>
                </Row>
                <Row style={{ marginTop: '8px' }}>
                  <Col>
                    <Form.Check
                      type="radio"
                      label={`I will prepare the transport documents using the ${facilityName}'s forwarder account, and ${facilityName} will organise the return using the return documents I prepared.`}
                      name="radio-paid-facility"
                      id="radio-paid-facility"
                      checked={paidByFacility === true}
                      onChange={(e) => {
                        setPaidByFacility(e.currentTarget.checked);
                        mutate({
                          body: {
                            ...parcel,
                            isReimbursed: e.currentTarget.checked,
                          },
                        });
                        if (e.currentTarget.checked) {
                          setFacilityReimbursedAcknowledgeRead(false);
                          setShowModal(true);
                        }
                      }}
                      style={{ marginLeft: '12px' }}
                    />
                  </Col>
                </Row>
              </>
            )}
          {paidByFacility === true && (
            <ReimbursedParcelDetails
              investigationId={investigation.id.toString()}
              shipmentId={parcel.shipmentId}
              parcel={parcel}
              facilityForwarderAccount={facilityForwarderAccount}
              facilityForwarderName={facilityForwarderName}
              reference={reference}
            />
          )}
          {parcel.returnAddress && paidByFacility !== true && (
            <>
              <Row className="mt-1">
                <Col>
                  <p>
                    <b>Transport paid by the User</b>
                  </p>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Form.Check
                    type="radio"
                    label={`I will prepare the transport documents using my forwarder account, and ${facilityName} will organise the return using the return documents I prepared (daily pickups from ${facilityName} exist for ${faciltyTransporters}).`}
                    name="radio-parcel-papers-included"
                    id="radio-parcel-papers-included"
                    checked={parcelPapersManaged}
                    onChange={(e) => {
                      setParcelPapersManaged(e.currentTarget.checked);
                      setPickup(!e.currentTarget.checked);
                      mutate({
                        body: {
                          ...parcel,
                          returnParcelInformation: {
                            returnType: RETURN_PARCEL_TYPE_MANAGE,
                            forwarderName: undefined,
                            plannedPickupDate: undefined,
                          },
                          returnInstructions: undefined,
                        },
                      });
                    }}
                    style={{ marginLeft: '12px' }}
                  />
                </Col>
              </Row>
              <Row className="align-items-center">
                <Col xs={12} sm={9}>
                  <Form.Check
                    type="radio"
                    label={`I will prepare the transport documents using my forwarder account, and I will organise the return directly with the fowarder (if not ${faciltyTransporters}).`}
                    name="radio-parcel-papers-oganized"
                    id="radio-parcel-papers-oganized"
                    checked={pickup}
                    onChange={(e) => {
                      setPickup(e.currentTarget.checked);
                      setParcelPapersManaged(!e.currentTarget.checked);
                      mutate({
                        body: {
                          ...parcel,
                          returnParcelInformation: {
                            returnType: RETURN_PARCEL_TYPE_ORGANIZE,
                          },
                        },
                      });
                      if (e.currentTarget.checked) {
                        setShowForwarderDetails(true);
                      }
                    }}
                    style={{ marginLeft: '12px' }}
                  />
                </Col>
                {pickup === true && (
                  <Col xs={12} sm={3} className="d-flex">
                    <Button
                      onClick={() => {
                        setShowForwarderDetails(true);
                      }}
                      size="sm"
                    >
                      <FontAwesomeIcon
                        icon={faEdit}
                        style={{
                          marginRight: '0.5rem',
                        }}
                      />
                      Edit return details information
                    </Button>
                  </Col>
                )}
              </Row>
              <ForwarderDetailsModal
                parcel={parcel}
                show={showForwarderDetails}
                onDone={updateForwarderDetails}
                onCancel={cancelForwarderDetails}
              />
              {pickup === true && <ForwarderDetails parcel={parcel} />}
            </>
          )}
        </Card.Body>
      </Card>
    </>
  );
}
