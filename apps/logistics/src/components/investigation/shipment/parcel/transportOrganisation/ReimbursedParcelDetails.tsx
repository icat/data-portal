import {
  Parcel,
  UPDATE_PARCEL_ENDPOINT,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { useState } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import { CopyValue, EditableText } from '@edata-portal/core';

export default function ReimbursedParcelDetails({
  investigationId,
  shipmentId,
  parcel,
  facilityForwarderAccount,
  facilityForwarderName,
  reference,
}: {
  investigationId: string;
  shipmentId: string;
  parcel: Parcel;
  facilityForwarderAccount: string;
  facilityForwarderName: string;
  reference: string;
}) {
  const [customValue, setCustomValue] = useState<string>(
    parcel.reimbursedCustomsValue?.toString() || '50',
  );
  const [instructions, setInstructions] = useState<string>(
    parcel.returnInstructions || '',
  );

  const { mutate } = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId: investigationId,
      shipmentId: shipmentId,
    },
  });
  return (
    <>
      <Row className="g-2">
        <Col xs={6}>
          <Form.Label>{facilityForwarderName} account</Form.Label>
          <CopyValue
            label={`${facilityForwarderName} account`}
            type="value"
            value={facilityForwarderAccount}
          />
        </Col>
        <Col xs={6}>
          <Form.Label>Reference</Form.Label>
          <CopyValue label="Reference" type="value" value={reference} />
        </Col>
      </Row>
      <Row>
        <Col>
          <EditableText
            value={customValue}
            onSave={(e) => {
              setCustomValue(e);
              mutate({
                body: {
                  ...parcel,
                  reimbursedCustomsValue: e,
                },
              });
            }}
            label={'Customs value (€)'}
            type="input"
            required
          />
        </Col>
        <Col>
          <EditableText
            value={instructions}
            onSave={(e) => {
              setInstructions(e);
              mutate({
                body: {
                  ...parcel,
                  returnInstructions: e,
                },
              });
            }}
            label={'Instructions for store'}
            type="textarea"
          />
        </Col>
      </Row>
    </>
  );
}
