import { FormObjectDescription, RenderForm } from '@edata-portal/core';
import { Parcel } from '@edata-portal/icat-plus-api';
import { Modal } from 'react-bootstrap';

export type ForwarderDetailsFormType = {
  forwarderName: string;
  forwarderAccount: string;
  plannedPickupDate: string;
  instructions: string;
};

export default function ForwarderDetailsModal({
  parcel,
  show,
  onDone,
  onCancel,
}: {
  parcel: Parcel;
  show: boolean;
  onDone?: (data: ForwarderDetailsFormType) => void;
  onCancel?: () => void;
}) {
  const description: FormObjectDescription<ForwarderDetailsFormType> = {
    forwarderName: {
      type: 'text',
      label: 'Forwarder Name',
      required: true,
      col: { xs: 12 },
    },
    forwarderAccount: {
      type: 'text',
      label: 'Forwarder Account',
      col: { xs: 12 },
    },
    plannedPickupDate: {
      type: 'date',
      label: 'Planned pickup date',
      col: { xs: 12 },
    },
    instructions: {
      type: 'textarea',
      label: 'Stores instruction',
      col: { xs: 12 },
    },
  };

  const initialValue: ForwarderDetailsFormType = parcel
    ? ({
        forwarderName: parcel.returnParcelInformation
          ? parcel.returnParcelInformation.forwarderName
          : undefined,
        forwarderAccount: parcel.returnParcelInformation
          ? parcel.returnParcelInformation.forwarderAccount
          : undefined,
        plannedPickupDate: parcel.returnParcelInformation
          ? parcel.returnParcelInformation.plannedPickupDate
          : undefined,
        instructions: parcel.returnInstructions,
      } as ForwarderDetailsFormType)
    : ({
        forwarderName: undefined,
        forwarderAccount: undefined,
        plannedPickupDate: undefined,
        instructions: undefined,
      } as unknown as ForwarderDetailsFormType);

  return (
    <Modal show={show} backdrop="static" size="lg" onHide={onCancel}>
      <Modal.Header closeButton>
        <Modal.Title id="forwarder-details-modal-title">
          Return Forwarder Details
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <RenderForm
          description={description}
          initialValue={initialValue}
          onSubmit={(data) => {
            onDone?.(data as ForwarderDetailsFormType);
          }}
          submitLabel="Save"
          onCancel={onCancel}
        />
      </Modal.Body>
    </Modal>
  );
}
