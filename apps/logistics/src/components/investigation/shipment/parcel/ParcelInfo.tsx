import { MetadataTable, first, useBreakpointValue } from '@edata-portal/core';
import {
  PARCEL_LIST_ENDPOINT,
  UPDATE_PARCEL_ENDPOINT,
  DELETE_PARCEL_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
  GET_ADDRESSES_ENDPOINT,
  INVESTIGATION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { AddressCard } from 'components/address/AddressCard';
import { getParcelStatus } from 'constants/statuses';

import { Col, Row, Alert, Card, Button, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import {
  faChevronDown,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useMemo, useState } from 'react';
import { EditParcelButton } from 'components/investigation/shipment/parcel/ParcelFormModal';
import { ParcelLabelButton } from 'components/investigation/shipment/parcel/ParcelLabelButton';
import { ParcelTransportOrganisation } from 'components/investigation/shipment/parcel/transportOrganisation/ParcelTransportOrganisation';

export function ParcelInfo({
  investigationId,
  parcelId,
}: {
  investigationId: string;
  parcelId: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      parcelId,
    },
  });

  const parcel = useMemo(() => first(parcels), [parcels]);

  const [expanded, setExpanded] = useState(true);

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: String(investigationId),
    },
  });

  const investigation =
    investigations && investigations.length > 0 ? investigations[0] : undefined;

  const addresses = useGetEndpoint({
    endpoint: GET_ADDRESSES_ENDPOINT,
  });

  const { mutate, isPending } = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId: parcel?.shipmentId || '',
    },
  });

  const { mutateAsync: deleteParcel } = useMutateEndpoint({
    endpoint: DELETE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId: parcel?.shipmentId || '',
    },
  });

  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const metadata1 = [
    {
      caption: 'Name',
      value: parcel?.name,
    },
    {
      caption: 'Description',
      value: parcel?.description,
    },
  ];
  const metadata2 = [
    {
      caption: 'Storage conditions',
      value: parcel?.storageConditions,
    },
    {
      caption: 'Comments',
      value: parcel?.comments,
    },
  ];

  const metadataElement = useBreakpointValue({
    xs: (
      <Row>
        <Col>
          <MetadataTable parameters={[...metadata1, ...metadata2]} />
        </Col>
      </Row>
    ),
    md: (
      <Row className="g-2">
        <Col xs={6}>
          <MetadataTable parameters={metadata1} />
        </Col>
        <Col xs={6}>
          <MetadataTable parameters={metadata2} />
        </Col>
      </Row>
    ),
  });

  if (!parcel) {
    return <Alert variant="info">Could not find parcel</Alert>;
  }

  const status = getParcelStatus(parcel.status);

  if (!status) {
    return <Alert variant="info">Parcel has invalid status</Alert>;
  }

  const editable = status.editable;

  return (
    <>
      <Card>
        <Card.Header
          onClick={() => setExpanded(!expanded)}
          style={{
            cursor: 'pointer',
          }}
        >
          <Row className="justify-content-between g-2 align-items-center">
            <Col xs={'auto'}>
              <Row className="g-2 align-items-center">
                <Col xs={'auto'}>
                  <FontAwesomeIcon
                    icon={expanded ? faChevronDown : faChevronRight}
                  />
                </Col>
                <Col xs={'auto'}>Parcel Info</Col>
              </Row>
            </Col>
            <Col xs={'auto'}>
              <Row className="g-2 align-items-center">
                {editable && (
                  <>
                    <Col xs={'auto'} onClick={(e) => e.stopPropagation()}>
                      <EditParcelButton parcel={parcel} />
                    </Col>
                    <Col xs={'auto'}>
                      <Button
                        variant="danger"
                        onClick={(e) => {
                          e.stopPropagation();
                          handleShow();
                        }}
                        disabled={isPending}
                        size="sm"
                      >
                        Delete
                      </Button>
                    </Col>
                  </>
                )}
                {status.sendable && parcel.content.length > 0 && (
                  <Col xs={'auto'}>
                    <ParcelLabelButton parcel={parcel} />
                  </Col>
                )}
              </Row>
            </Col>
          </Row>
        </Card.Header>
        {expanded && (
          <Card.Body className="p-2">
            <Col>
              {parcel.containsDangerousGoods && (
                <Row>
                  <Col>
                    <Alert variant="danger">
                      <strong>This parcel contains dangerous goods.</strong>
                    </Alert>
                  </Col>
                </Row>
              )}

              {metadataElement}

              <Row className="mt-3 g-2">
                <Col xs={12} md={6}>
                  <AddressCard
                    title="Sender's address"
                    address={parcel.shippingAddress}
                    choices={addresses || []}
                    onSelect={(address) => {
                      mutate({
                        body: {
                          ...parcel,
                          shippingAddress: address,
                        },
                      });
                    }}
                    isLoading={isPending}
                    investigationId={investigationId}
                    editable={editable}
                    required
                    isReturnAddress={false}
                  />
                </Col>
                <Col xs={12} md={6}>
                  <AddressCard
                    title="Return address"
                    address={parcel.returnAddress}
                    choices={addresses || []}
                    onSelect={(address) => {
                      mutate({
                        body: {
                          ...parcel,
                          returnAddress: address,
                        },
                      });
                    }}
                    isLoading={isPending}
                    investigationId={investigationId}
                    editable={editable}
                    isReturnAddress={true}
                  />
                </Col>
              </Row>
              {investigation && (
                <ParcelTransportOrganisation
                  investigation={investigation}
                  parcel={parcel}
                />
              )}
            </Col>
          </Card.Body>
        )}
      </Card>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Remove parcel</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to remove the parcel {parcel.name}?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleClose}>
            No
          </Button>
          <Button
            variant="secondary"
            onClick={() => {
              handleClose();
              deleteParcel({ body: parcel }).then(() => {
                navigate(`/investigation/${investigationId}/logistics`);
              });
            }}
          >
            Yes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
