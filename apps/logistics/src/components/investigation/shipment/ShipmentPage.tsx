import { Button, EditableText, prettyPrint } from '@edata-portal/core';
import {
  CREATE_SHIPMENT_ENDPOINT,
  GET_ADDRESSES_ENDPOINT,
  INVESTIGATION_ADDRESSES_ENDPOINT,
  Shipment,
  SHIPMENT_LIST_ENDPOINT,
  UPDATE_SHIPMENT_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { useMemo, useState } from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import { AddressCard } from 'components/address/AddressCard';
import { InvestigationReimbursedParcels } from 'components/investigation/InvestigationReimbursedParcels';
import { ShipmentParcels } from 'components/investigation/shipment/ShipmentParcels';
import { ShipmentContents } from 'components/investigation/shipment/ShipmentContents';

export function ShipmentPage({ investigationId }: { investigationId: string }) {
  const shipments = useGetEndpoint({
    endpoint: SHIPMENT_LIST_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const shipment = shipments?.[0];

  const [display, setDisplay] = useState<'parcels' | 'contents'>('parcels');

  return (
    <Container fluid>
      <Row>
        <ShipmentDefaultAddresses
          shipment={shipment}
          investigationId={investigationId}
        />
        {shipment && (
          <Col xs={12} md={12} xl={4}>
            <ShipmentComment shipment={shipment} />
          </Col>
        )}
      </Row>
      <Row className="mt-2">
        <Col>
          <InvestigationReimbursedParcels
            investigationId={investigationId}
            shipmentId={shipment?._id.toString()}
          />
        </Col>
      </Row>

      {shipment && (
        <>
          <hr />

          <Row className="g-2">
            {['parcels', 'contents'].map((v) => (
              <Col key={v} xs={'auto'}>
                <Button
                  onClick={() => setDisplay(v as any)}
                  active={v === display}
                  variant="outline-primary"
                >
                  {prettyPrint(v)}
                </Button>
              </Col>
            ))}
          </Row>

          <hr />

          {display === 'parcels' && (
            <ShipmentParcels
              investigationId={investigationId}
              shipment={shipment}
            />
          )}
          {display === 'contents' && (
            <ShipmentContents
              investigationId={investigationId}
              shipment={shipment}
            />
          )}
        </>
      )}
    </Container>
  );
}

export function ShipmentComment({ shipment }: { shipment: Shipment }) {
  const { isPending, mutate } = useMutateEndpoint({
    endpoint: UPDATE_SHIPMENT_ENDPOINT,
    params: {
      investigationId: shipment.investigationId.toString(),
    },
  });

  return (
    <EditableText
      value={shipment.comments || ''}
      onSave={async (v) => {
        mutate({
          body: {
            ...shipment,
            comments: v,
          },
        });
      }}
      isSaving={isPending}
      label={'Comments'}
      type="textarea"
    />
  );
}

export function ShipmentDefaultAddresses({
  shipment,
  investigationId,
}: {
  shipment?: Shipment;
  investigationId: string;
}) {
  const { mutate, isPending } = useMutateEndpoint({
    endpoint: UPDATE_SHIPMENT_ENDPOINT,
    params: {
      investigationId: investigationId,
    },
  });

  const { mutate: create } = useMutateEndpoint({
    endpoint: CREATE_SHIPMENT_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const investigationAddresses = useGetEndpoint({
    endpoint: INVESTIGATION_ADDRESSES_ENDPOINT,
    params: {
      investigationId: investigationId.toString(),
    },
  });

  const myAddresses = useGetEndpoint({
    endpoint: GET_ADDRESSES_ENDPOINT,
  });

  const addresses = useMemo(() => {
    return [...(myAddresses || []), ...(investigationAddresses || [])]
      .filter((v, i, a) => a.findIndex((t) => t._id === v._id) === i)
      .sort((a, b) => a.updatedAt.localeCompare(b.updatedAt));
  }, [myAddresses, investigationAddresses]);

  const defaultShipmentValues = { name: 'shipment', investigationId };

  return (
    <>
      <Col xs={12} md={6} xl={4}>
        <AddressCard
          title="Default sender's address"
          message="This address will be used by default for all new parcels."
          address={shipment?.defaultShippingAddress}
          choices={addresses || []}
          onSelect={(address) => {
            if (shipment) {
              mutate({
                body: {
                  ...shipment,
                  defaultShippingAddress: address,
                },
              });
            } else {
              create({
                body: {
                  ...defaultShipmentValues,
                  defaultShippingAddress: address,
                  investigationId,
                },
              });
            }
          }}
          isLoading={isPending}
          investigationId={investigationId}
          editable
          required
          isReturnAddress={false}
          testId="default-sender-address"
        />
      </Col>
      {shipment && (
        <Col xs={12} md={6} xl={4}>
          <AddressCard
            title="Default return address"
            message="The return address is the address to which you would like a parcel to be returned at the end of the experiment session. Parcels without a return address are discarded at the end of the experiment session, unless the user takes it with them when leaving the ESRF."
            address={shipment.defaultReturnAddress}
            choices={addresses || []}
            onSelect={(address) => {
              mutate({
                body: {
                  ...shipment,
                  defaultReturnAddress: address,
                },
              });
            }}
            isLoading={isPending}
            investigationId={investigationId}
            editable
            isReturnAddress={true}
            testId="default-return-address"
          />
        </Col>
      )}
    </>
  );
}
