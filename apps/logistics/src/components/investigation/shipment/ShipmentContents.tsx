import { Loading, useNotify } from '@edata-portal/core';
import {
  CREATE_PARCEL_ENDPOINT,
  DELETE_PARCEL_ENDPOINT,
  GET_TRACKING_SETUP_ENDPOINT,
  PARCEL_LIST_ENDPOINT,
  Parcel,
  UPDATE_PARCEL_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
  type Shipment,
} from '@edata-portal/icat-plus-api';
import {
  convertParcelContentToSpreadSheetItems,
  convertSpreadSheetItemsToParcelContents,
} from 'components/investigation/shipment/parcel/content/convertToSpreadSheet';
import {
  PARCEL_NAME_COLUMN_NAME,
  PARCEL_STORAGE_CONDITION_COLUMN_NAME,
  getAllColumns,
  getTableConfig,
} from 'components/investigation/shipment/parcel/content/tableconfig';
import {
  detectParcelContentErrors,
  detectParcelErrors,
} from 'components/investigation/shipment/parcel/content/tablelogic';
import { SpreadSheetTable } from 'components/spreadsheet/SpreadSheetTable';
import type { SpreadSheetItem } from 'components/spreadsheet/spreadSheetLogic';
import { getParcelStatus } from 'constants/statuses';
import { Suspense, useCallback, useMemo } from 'react';

export function ShipmentContents({
  investigationId,
  shipment,
}: {
  investigationId: string;
  shipment: Shipment;
}) {
  return (
    <Suspense fallback={<Loading />}>
      <LoadAndDisplay investigationId={investigationId} shipment={shipment} />
    </Suspense>
  );
}

function LoadAndDisplay({
  investigationId,
  shipment,
}: {
  investigationId: string;
  shipment: Shipment;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId,
      shipmentId: shipment._id.toString(),
    },
    default: [] as Parcel[],
  });

  const editableParcels = useMemo(
    () => parcels.filter((p) => getParcelStatus(p.status)?.editable),
    [parcels],
  );

  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const tableConfig = useMemo(() => getTableConfig(setup, true), [setup]);

  const columns = useMemo(() => getAllColumns(tableConfig), [tableConfig]);

  const spreadSheetItems = useMemo(() => {
    if (!editableParcels.length) return [];
    return editableParcels.flatMap((p) => {
      if (!p.content) return [];
      return p.content.flatMap((i) => {
        return convertParcelContentToSpreadSheetItems(i, tableConfig).map(
          (item) => ({
            ...item,
            [PARCEL_NAME_COLUMN_NAME]: p.name,
            [PARCEL_STORAGE_CONDITION_COLUMN_NAME]: p.storageConditions,
          }),
        );
      });
    });
  }, [editableParcels, tableConfig]);

  const groupItemsByParcel = useCallback((items: SpreadSheetItem[]) => {
    const parcelNames = items
      .map((item) => item[PARCEL_NAME_COLUMN_NAME])
      .filter((value, index, self) => self.indexOf(value) === index);

    return parcelNames.map((parcelName) => {
      return {
        name: parcelName,
        items: items.filter(
          (item) => item[PARCEL_NAME_COLUMN_NAME] === parcelName,
        ),
      };
    });
  }, []);

  const detectExtraItemsErrors = useCallback(
    (items: SpreadSheetItem[]) => {
      const groupedItems = groupItemsByParcel(items);
      const getItemIndex = (item: SpreadSheetItem) =>
        items.findIndex((i) => i === item);
      return [
        ...detectParcelErrors(items, tableConfig, getItemIndex),
        ...groupedItems.flatMap((group) => {
          return detectParcelContentErrors(
            group.items,
            tableConfig,
            getItemIndex,
          );
        }),
      ];
    },
    [groupItemsByParcel, tableConfig],
  );

  const updateParcel = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId: shipment._id.toString(),
    },
  });

  const deleteParcel = useMutateEndpoint({
    endpoint: DELETE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId: shipment._id.toString(),
    },
  });

  const createParcel = useMutateEndpoint({
    endpoint: CREATE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId: shipment._id.toString(),
    },
  });

  const notify = useNotify();

  const onSave = useCallback(
    (items: SpreadSheetItem[]) => {
      const groupedItems = groupItemsByParcel(items);

      const nonEditableParcels = groupedItems
        .map((g) => g.name)
        .filter(
          (group) =>
            parcels.findIndex((p) => p.name === group) !== -1 &&
            editableParcels.findIndex((p) => p.name === group) === -1,
        );

      if (nonEditableParcels.length) {
        notify({
          message:
            'Some parcels are not editable: ' + nonEditableParcels.join(', '),
          type: 'danger',
        });
        return Promise.resolve(false);
      }

      const createUpdatePromises = groupedItems.map((group) => {
        const parcelName = group.name;
        const parcelStorageCondition =
          group.items[0][PARCEL_STORAGE_CONDITION_COLUMN_NAME];
        const content = convertSpreadSheetItemsToParcelContents(
          group.items,
          tableConfig,
        );
        const parcel = editableParcels.find((p) => p.name === parcelName);
        if (parcel) {
          return updateParcel.mutateAsync({
            body: {
              ...parcel,
              storageConditions: parcelStorageCondition,
              content: content,
            },
          });
        } else {
          return createParcel.mutateAsync({
            body: {
              name: parcelName,
              storageConditions: parcelStorageCondition,
              content: content,
              shipmentId: shipment._id.toString(),
              shippingAddress: shipment.defaultShippingAddress,
              returnAddress: shipment.defaultReturnAddress,
            },
          });
        }
      });

      const parcelsToDelete = editableParcels.filter(
        (p) => !groupedItems.find((g) => g.name === p.name),
      );

      const deletePromises = parcelsToDelete.map((p) => {
        return deleteParcel.mutateAsync({
          body: p,
        });
      });

      return Promise.all([...createUpdatePromises, ...deletePromises])
        .then(() => true)
        .catch(() => false);
    },

    [
      createParcel,
      deleteParcel,
      editableParcels,
      groupItemsByParcel,
      notify,
      parcels,
      shipment,
      tableConfig,
      updateParcel,
    ],
  );

  return (
    <SpreadSheetTable
      name={`parcels-contents`}
      initialItems={spreadSheetItems}
      columns={columns}
      onSave={onSave}
      detectExtraItemsErrors={detectExtraItemsErrors}
      columnGroups={tableConfig.columnsGroups}
      saving={
        updateParcel.isPending ||
        deleteParcel.isPending ||
        createParcel.isPending
      }
    />
  );
}
