import { Loading } from '@edata-portal/core';
import type { Shipment } from '@edata-portal/icat-plus-api';
import { NewParcelButton } from 'components/investigation/shipment/parcel/ParcelFormModal';
import { ShipmentParcelList } from 'components/investigation/shipment/parcel/ShipmentParcelList';
import { Suspense } from 'react';
import { Col, Row } from 'react-bootstrap';

export function ShipmentParcels({
  investigationId,
  shipment,
}: {
  investigationId: string;
  shipment: Shipment;
}) {
  return (
    <Col>
      <Row>
        <Col>
          <NewParcelButton
            shipmentId={shipment?._id.toString()}
            investigationId={investigationId}
          />
        </Col>
      </Row>
      <Row>
        <Suspense fallback={<Loading />}>
          <ShipmentParcelList
            shipmentId={shipment._id.toString()}
            investigationId={investigationId}
          />
        </Suspense>
      </Row>
    </Col>
  );
}
