import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { FlexHCDDualChanger } from 'components/investigation/changer/definitions/mx/flex/FlexHCDDualChanger';
import { FlexHCDUnipuckChanger } from 'components/investigation/changer/definitions/mx/flex/FlexHCDUnipuckChanger';
import { ISARAChanger } from 'components/investigation/changer/definitions/mx/flex/ISARAChanger';
import type { Dictionary } from 'lodash';

export const CHANGER_CONFIG: Dictionary<AbstractSampleChanger> = {
  FlexHCDUnipuckPlate: new FlexHCDUnipuckChanger(),
  FlexHCDDual: new FlexHCDDualChanger(),
  Xaira: new ISARAChanger({
    cells: 35,
    skip_cells: 2,
    y_offset: 5,
  }),
  Xaloc: new ISARAChanger({
    cells: 32,
    skip_cells: 72,
    y_offset: 3,
  }),
};
