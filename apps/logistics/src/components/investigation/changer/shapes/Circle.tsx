export function Circle({
  cx,
  cy,
  r,
  backgroundColor,
  borderColor,
  strokeWidth,
}: {
  cx: number;
  cy: number;
  r: number;
  backgroundColor: string;
  borderColor: string;
  strokeWidth: number;
}) {
  return (
    <div
      draggable={false}
      style={{
        position: 'absolute',
        left: cx * 100 + '%',
        bottom: cy * 100 + '%',
        transform: 'translate(-50%, 50%)',
        width: r * 2 * 100 + '%',
        aspectRatio: '1/1',
        borderRadius: '50%',
        backgroundColor,
        borderColor,
        borderWidth: strokeWidth,
        borderStyle: 'solid',
        pointerEvents: 'none',
      }}
    />
  );
}
