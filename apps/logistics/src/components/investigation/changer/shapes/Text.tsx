export function Text({
  cx,
  cy,
  text,
  fontSize,
}: {
  cx: number;
  cy: number;
  text: string;
  fontSize: number;
}) {
  return (
    <div
      draggable={false}
      style={{
        position: 'absolute',
        left: cx * 100 + '%',
        bottom: cy * 100 + '%',
        fontSize: fontSize,
        textAlign: 'center',
        pointerEvents: 'none',
        transform: 'translate(-50%, 50%)',
      }}
    >
      {text}
    </div>
  );
}
