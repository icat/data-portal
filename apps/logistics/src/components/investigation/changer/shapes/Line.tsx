export function Line({
  x1,
  y1,
  x2,
  y2,
  color,
  strokeWidth,
}: {
  x1: number;
  y1: number;
  x2: number;
  y2: number;
  color: string;
  strokeWidth: number;
}) {
  const deltaX = x2 - x1;
  const deltaY = y2 - y1;
  const distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);
  const angle = -Math.atan2(deltaY, deltaX);
  return (
    <div
      draggable={false}
      style={{
        position: 'absolute',
        left: x1 * 100 + '%',
        bottom: y1 * 100 + '%',
        width: distance * 100 + '%',
        height: strokeWidth,
        backgroundColor: color,
        transformOrigin: 'top left',
        transform: `rotate(${angle}rad)`,
        pointerEvents: 'none',
      }}
    />
  );
}
