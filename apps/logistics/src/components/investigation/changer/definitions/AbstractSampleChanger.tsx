import { FullSizeWithRatio } from '@edata-portal/core';
import type { ContainerType, ParcelContent } from '@edata-portal/icat-plus-api';
import { range } from 'lodash';

export abstract class AbstractSampleChanger {
  height: number = 1;
  width: number = 1;

  plotChanger(children: React.ReactNode) {
    const { height, width } = this;

    return (
      <FullSizeWithRatio ratioWidth={width} ratioHeight={height}>
        {this.plotBackground()}
        {this.plotCells()}
        {children}
      </FullSizeWithRatio>
    );
  }

  abstract plotBackground(): JSX.Element;

  abstract plotCells(): JSX.Element;

  abstract plotContainer(container: ParcelContent): JSX.Element;
  abstract plotEmptyContainer(
    cell: number,
    position: number,
    droppable: boolean,
    hovering: boolean,
  ): JSX.Element;

  abstract getPositionCoordinates(
    cell: number,
    position: number,
  ): { x: number; y: number; r: number; xtxt: number; ytxt: number };

  abstract getNbCell(): number;
  abstract getNbContainerInCell(cell: number): number;

  getLocation(cell: number, position: number) {
    let res = 0;
    range(0, cell).forEach((n) => {
      res = res + this.getNbContainerInCell(n);
    });
    return res + position + 1;
  }

  getPosition(
    location: number,
  ): { position: number; cell: number } | undefined {
    if (isNaN(location)) {
      return undefined;
    }
    let minLocation = 1;
    for (const cell of range(0, this.getNbCell())) {
      const nbContainer = this.getNbContainerInCell(cell);
      if (minLocation + nbContainer <= location) {
        minLocation = minLocation + nbContainer;
      } else {
        return {
          cell,
          position: location - minLocation,
        };
      }
    }
    return undefined;
  }

  abstract getContainerType(cell: number, position: number): ContainerType;
}
