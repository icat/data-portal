import { AbstractMXChanger } from 'components/investigation/changer/definitions/mx/AbstractMXChanger';
import { Circle } from 'components/investigation/changer/shapes/Circle';
import { Text } from 'components/investigation/changer/shapes/Text';
import { range } from 'lodash';
import { PUCK_PLOT_CONFIG } from '../container/PuckPlotConfig';
import { FullSizeWithRatio } from '@edata-portal/core';

export type ISARAConfiguration = {
  cells: number;
  skip_cells: number;
  y_offset: number;
};

export class ISARAChanger extends AbstractMXChanger {
  radius = 0.47;
  cx = 0.5;
  cy = 0.5;

  parameters: ISARAConfiguration;

  constructor(parameters: ISARAConfiguration) {
    super();
    this.parameters = parameters;
  }

  plotBackground(): JSX.Element {
    return (
      <Circle
        cx={this.cx}
        cy={this.cy}
        r={this.radius}
        backgroundColor="#CCCCCC"
        borderColor="#000"
        strokeWidth={5}
      />
    );
  }

  getContainerType(cell: number, position: number) {
    return PUCK_PLOT_CONFIG.UNIPUCK.type;
  }

  plotChanger(children: React.ReactNode) {
    const { height, width } = this;

    return (
      <FullSizeWithRatio ratioWidth={width} ratioHeight={height}>
        {this.plotBackground()}
        {this.plotCells()}
        {children}
      </FullSizeWithRatio>
    );
  }

  plotCells(): JSX.Element {
    return (
      <>
        {range(0, this.getNbCell()).map((cell) => {
          const position = 0;
          const coordinates = this.getPositionCoordinates(cell, position);
          const label_text_num =
            cell >= 29 && cell < this.parameters.cells
              ? cell + this.parameters.skip_cells
              : cell + 1;
          return (
            <>
              {
                <Text
                  cx={coordinates.xtxt}
                  cy={coordinates.ytxt}
                  text={`${label_text_num}`}
                  fontSize={15}
                />
              }
            </>
          );
        })}
      </>
    );
  }

  getPositionCoordinates(
    cell: number,
    position: number,
  ): { x: number; y: number; r: number; xtxt: number; ytxt: number } {
    const r = this.radius / 8;
    const offset_even_rows = 0.235;
    const offset_odd_rows = offset_even_rows + r;

    var y: number = this.cy;
    var x: number = this.cx;
    if (cell < 5) {
      x = Math.abs(x + 2 * r * cell - offset_even_rows);
      y = y + 5 * r;
    } else if (cell >= 5 && cell < 11) {
      x = Math.abs(x + 2 * r * (cell - 5) - offset_odd_rows);
      y = y + 3 * r;
    } else if (cell >= 11 && cell < 16) {
      x = Math.abs(x + 2 * r * (cell - 11) - offset_even_rows);
      y = y + 1 * r;
    } else if (cell >= 16 && cell < 22) {
      x = Math.abs(x + 2 * r * (cell - 16) - offset_odd_rows);
      y = y - 1 * r;
    } else if (cell >= 22 && cell < 27) {
      x = Math.abs(x + 2 * r * (cell - 22) - offset_even_rows);
      y = y - 3 * r;
    } else if (cell >= 27 && cell < 29) {
      x = Math.abs(x + 2 * r * (cell - 27) - offset_odd_rows + 4 * r);
      y = y - 5 * r;
    } else if (cell >= 29 && cell < this.parameters.cells) {
      x = x - this.radius * 1.25;
      y = y + this.parameters.y_offset * r - 2.2 * r * (cell - 29);
    }

    return {
      x: x,
      y: y,
      r,
      xtxt: x - 0.4 * r,
      ytxt: y + 1.05 * r,
    };
  }

  getLocation(cell: number, position: number) {
    if (cell >= 29 && cell < this.parameters.cells + 2) {
      return cell + 2;
    }
    return cell + 1;
  }

  getPosition(
    location: number,
  ): { position: number; cell: number } | undefined {
    if (isNaN(location)) {
      return undefined;
    }
    if (location > 29 && location < this.parameters.cells + 2) {
      return { position: 0, cell: location - 2 };
    }
    return { position: 0, cell: location - 1 };
  }

  getNbCell(): number {
    return this.parameters.cells;
  }

  // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
  getNbContainerInCell(cell: number): number {
    return 1;
  }
}
