import type { ContainerType, ParcelContent } from '@edata-portal/icat-plus-api';
import classNames from 'classnames';
import { range } from 'lodash';
import type { PropsWithChildren } from 'react';
import { Badge, OverlayTrigger, Tooltip } from 'react-bootstrap';
import {
  PUCK_PLOT_CONFIG,
  PuckPlotConfig,
  containerTypeToPlotConfig,
} from 'components/investigation/changer/definitions/mx/container/PuckPlotConfig';

import './puck.css';

export const PUCK_SVG_RADIUS = 75;

export function Puck({ container }: { container: ParcelContent }) {
  const { type, containerType, content } = container;

  if (type !== 'CONTAINER' || containerType === undefined) return null;

  const puckPlotConfig = containerTypeToPlotConfig(containerType);

  return (
    <PuckSVG puckPlotConfig={puckPlotConfig}>
      {range(1, containerType.capacity + 1).map((index) => {
        const coordinates = puckPlotConfig.computePos(index);
        const item = content.find(
          (item) => Number(item.sampleContainerPosition) === index,
        );
        return (
          <SampleSVG
            key={index}
            coordinates={coordinates}
            position={index}
            item={item}
          />
        );
      })}
    </PuckSVG>
  );
}

export function EmptyPuck({
  containerType,
  className,
}: {
  containerType: ContainerType;
  className?: string;
}) {
  const puckPlotConfig = containerTypeToPlotConfig(containerType);
  return (
    <PuckSVG puckPlotConfig={puckPlotConfig} className={className}>
      {range(1, puckPlotConfig.type.capacity + 1).map((index) => {
        const coordinates = puckPlotConfig.computePos(index);
        return (
          <SampleSVG
            key={index}
            coordinates={coordinates}
            position={index}
            item={undefined}
          />
        );
      })}
    </PuckSVG>
  );
}

function PuckSVG({
  puckPlotConfig,
  children,
  onClick = undefined,
  className = undefined,
}: PropsWithChildren<{
  puckPlotConfig: PuckPlotConfig;
  onClick?: () => void;
  className?: string;
}>) {
  const squareSize = 0.9;

  const puckClassName = classNames({
    puck: true,
    'puck-clickable': onClick,
    [className || '']: className,
  });

  return (
    <svg
      className={puckClassName}
      viewBox={`-5 -5 ${2 * PUCK_SVG_RADIUS + 10} ${2 * PUCK_SVG_RADIUS + 10}`}
    >
      {puckPlotConfig.isSquare ? (
        <rect
          onClick={onClick}
          x={0 + (1 - squareSize) * PUCK_SVG_RADIUS}
          y={0 + (1 - squareSize) * PUCK_SVG_RADIUS}
          width={2 * squareSize * PUCK_SVG_RADIUS}
          height={2 * squareSize * PUCK_SVG_RADIUS}
          fill="#CCCCCC"
          className={puckClassName}
          rx="5"
        />
      ) : (
        <circle
          onClick={onClick}
          className={puckClassName}
          cx={PUCK_SVG_RADIUS}
          cy={PUCK_SVG_RADIUS}
          r={PUCK_SVG_RADIUS}
          fill="#CCCCCC"
        />
      )}
      {puckPlotConfig === PUCK_PLOT_CONFIG.UNIPUCK && (
        <g fill="#888888" stroke="#888888" pointerEvents="none">
          <circle
            cx={PUCK_SVG_RADIUS}
            cy={PUCK_SVG_RADIUS * 1.05}
            r={PUCK_SVG_RADIUS * 0.1}
          />
          <circle
            cx={PUCK_SVG_RADIUS * 0.9}
            cy={PUCK_SVG_RADIUS * 0.95}
            r={PUCK_SVG_RADIUS * 0.05}
            strokeWidth="1"
          />
          <circle
            cx={PUCK_SVG_RADIUS * 1.1}
            cy={PUCK_SVG_RADIUS * 0.95}
            r={PUCK_SVG_RADIUS * 0.05}
            strokeWidth="1"
          />
          <circle
            cx={PUCK_SVG_RADIUS}
            cy={PUCK_SVG_RADIUS * 1.5}
            r={PUCK_SVG_RADIUS * 0.05}
            strokeWidth="1"
          />
        </g>
      )}
      {children}
    </svg>
  );
}

function SampleSVG({
  coordinates,
  position,
  item,
  onClick = undefined,
  clickableContainer = false,
}: {
  coordinates: { x: number; y: number };
  position: number;
  item?: ParcelContent;
  onClick?: () => void;
  clickableContainer?: boolean;
}) {
  if (!item)
    return (
      <>
        <circle
          pointerEvents={'none'}
          className="puckEmptyPosition"
          cx={coordinates.x}
          cy={coordinates.y}
          r={PUCK_SVG_RADIUS * 0.175}
        />
        <text x={coordinates.x} y={coordinates.y} className="puckSample">
          <tspan dx="0" dy="5" pointerEvents="none">
            {position}
          </tspan>
        </text>
      </>
    );

  const className = classNames({
    puckSample: true,
    puckSampleClickable: onClick && !clickableContainer,
    puckSampleFilled: true,
  });

  const sampleCircle = (
    <circle
      key={'sampleCircle'}
      pointerEvents={clickableContainer ? 'none' : undefined}
      onClick={onClick}
      className={className}
      cursor={onClick && !clickableContainer ? 'pointer' : undefined}
      cx={coordinates.x}
      cy={coordinates.y}
      r={PUCK_SVG_RADIUS * 0.175}
    />
  );
  return (
    <>
      <OverlayTrigger
        key="overlay"
        placement="bottom"
        overlay={
          <Tooltip key="sampleDescription">
            Sample
            <p>
              <Badge style={{ margin: 0 }} bg="info">
                {item?.name}
              </Badge>
            </p>
          </Tooltip>
        }
      >
        {sampleCircle}
      </OverlayTrigger>
      <text
        key={'position'}
        className={className}
        x={coordinates.x}
        y={coordinates.y}
      >
        <tspan dx="0" dy="5" pointerEvents="none">
          {position}
        </tspan>
      </text>
    </>
  );
}
