import { PUCK_PLOT_CONFIG } from 'components/investigation/changer/definitions/mx/container/PuckPlotConfig';
import { AbstractFlexHCDChanger } from 'components/investigation/changer/definitions/mx/flex/AbstractFlexHCDChanger';

export class FlexHCDUnipuckChanger extends AbstractFlexHCDChanger {
  // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
  getContainerType(cell: number, position: number) {
    return PUCK_PLOT_CONFIG.UNIPUCK.type;
  }
}
