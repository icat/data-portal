import type { ContainerType } from '@edata-portal/icat-plus-api';
import { PUCK_SVG_RADIUS } from 'components/investigation/changer/definitions/mx/container/Puck';

export type PuckPlotConfig = {
  type: ContainerType;
  isSquare: boolean;
  computePos: (position: number) => { x: number; y: number };
};

const SPINE: PuckPlotConfig = {
  computePos: (position: number) => {
    return computePosPuck(0.76, 10, position);
  },
  isSquare: false,
  type: {
    name: 'SPINE',
    capacity: 10,
  },
};

const UNIPUCK: PuckPlotConfig = {
  computePos: (position: number) => {
    if (position < 6) {
      return computePosPuck(0.36, 5, position);
    }
    return computePosPuck(0.76, 11, position - 5);
  },
  isSquare: false,
  type: {
    name: 'UNIPUCK',
    capacity: 16,
  },
};

const BOX: PuckPlotConfig = {
  computePos: (position: number) => {
    return {
      x: PUCK_SVG_RADIUS,
      y: PUCK_SVG_RADIUS,
    };
  },
  isSquare: true,
  type: {
    name: 'BOX',
    capacity: 1,
  },
};

const OTHER: PuckPlotConfig = {
  computePos: () => {
    return { x: 0, y: 0 };
  },
  isSquare: true,
  type: {
    name: 'OTHER',
    capacity: 0,
  },
};

export const PUCK_PLOT_CONFIG = {
  SPINE,
  UNIPUCK,
  BOX,
  OTHER,
};

export const containerTypeToPlotConfig = (
  containerType: ContainerType,
): PuckPlotConfig => {
  return (
    Object.values(PUCK_PLOT_CONFIG).find(
      (plotConfig) =>
        plotConfig.type.name === containerType.name &&
        plotConfig.type.capacity === containerType.capacity,
    ) || OTHER
  );
};

function computePosPuck(
  radiusRatio: number,
  maxPosition: number,
  position: number,
): { x: number; y: number } {
  const radius = radiusRatio * PUCK_SVG_RADIUS;
  const step = (Math.PI * 2) / maxPosition;
  const angle = -(position - 1) * step;
  const x = Math.sin(angle) * radius + PUCK_SVG_RADIUS;
  const y = PUCK_SVG_RADIUS - Math.cos(angle) * radius;
  return { x, y };
}
