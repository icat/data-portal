import type { ParcelContent } from '@edata-portal/icat-plus-api';
import classNames from 'classnames';
import {
  EmptyPuck,
  Puck,
} from 'components/investigation/changer/definitions/mx/container/Puck';
import { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';

export abstract class AbstractMXChanger extends AbstractSampleChanger {
  plotContainer(container: ParcelContent) {
    return (
      <svg
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
        }}
      >
        <Puck container={container} />
      </svg>
    );
  }

  plotEmptyContainer(
    cell: number,
    position: number,
    droppable: boolean,
    hovering: boolean,
  ): JSX.Element {
    const type = this.getContainerType(cell, position);

    const classes = classNames({
      'puck-droppable': droppable && !hovering,
      'puck-droppable-hover': hovering && droppable,
      'puck-missing ': true,
    });

    return (
      <svg
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
        }}
      >
        <EmptyPuck containerType={type} className={classes} />
      </svg>
    );
  }
}
