import { AbstractMXChanger } from 'components/investigation/changer/definitions/mx/AbstractMXChanger';
import { Circle } from 'components/investigation/changer/shapes/Circle';
import { Line } from 'components/investigation/changer/shapes/Line';
import { Text } from 'components/investigation/changer/shapes/Text';
import { range } from 'lodash';

function getCellAngle(n: number) {
  const startAngle = -(Math.PI / 8 + Math.PI / 2);
  const step = -Math.PI / 4;
  return startAngle + step * n;
}

export abstract class AbstractFlexHCDChanger extends AbstractMXChanger {
  radius = 0.47;
  cx = 0.5;
  cy = 0.5;

  plotBackground(): JSX.Element {
    return (
      <Circle
        cx={this.cx}
        cy={this.cy}
        r={this.radius}
        backgroundColor="#CCCCCC"
        borderColor="#000"
        strokeWidth={5}
      />
    );
  }

  plotCells(): JSX.Element {
    return (
      <>
        {[0, Math.PI / 2, Math.PI / 4, -(Math.PI / 4)].map((angle) => {
          const coordinates = {
            x1: this.cx + this.radius * Math.cos(angle),
            y1: this.cy + this.radius * Math.sin(angle),
            x2: this.cx + this.radius * Math.cos(angle + Math.PI),
            y2: this.cy + this.radius * Math.sin(angle + Math.PI),
          };
          return (
            <Line key={angle} color={'#000'} strokeWidth={1} {...coordinates} />
          );
        })}
        {range(0, this.getNbCell()).map((cell) => {
          const angle = getCellAngle(cell);
          const x = this.cx + this.radius * Math.cos(angle);
          const y = this.cy + this.radius * Math.sin(angle);

          return (
            <>
              <Circle
                cx={x}
                cy={y}
                r={this.radius / 10}
                borderColor={'#000'}
                backgroundColor="#fff"
                strokeWidth={2}
              />
              <Text cx={x} cy={y} text={`${cell + 1}`} fontSize={15} />
              {range(0, this.getNbContainerInCell(cell)).map((position) => {
                const coordinates = this.getPositionCoordinates(cell, position);
                return (
                  <>
                    <Text
                      cx={coordinates.xtxt}
                      cy={coordinates.ytxt}
                      text={`${position + 1}`}
                      fontSize={15}
                    />
                  </>
                );
              })}
            </>
          );
        })}
      </>
    );
  }

  getPositionCoordinates(
    cell: number,
    position: number,
  ): { x: number; y: number; r: number; xtxt: number; ytxt: number } {
    const cellAngle = getCellAngle(cell);

    const c1 = this.radius * 0.45;
    const c2 = this.radius * 0.75;
    const r = this.radius / 8;

    const x = [
      c2 * Math.cos(cellAngle + Math.PI / 16),
      c2 * Math.cos(cellAngle - Math.PI / 16),
      c1 * Math.cos(cellAngle),
    ];
    const y = [
      c2 * Math.sin(cellAngle + Math.PI / 16),
      c2 * Math.sin(cellAngle - Math.PI / 16),
      c1 * Math.sin(cellAngle),
    ];

    const ctxt1 = this.radius * 0.28;
    const ctxt2 = this.radius * 0.92;

    const xtxt = [
      ctxt2 * Math.cos(cellAngle + Math.PI / 16),
      ctxt2 * Math.cos(cellAngle - Math.PI / 16),
      ctxt1 * Math.cos(cellAngle),
    ];
    const ytxt = [
      ctxt2 * Math.sin(cellAngle + Math.PI / 16),
      ctxt2 * Math.sin(cellAngle - Math.PI / 16),
      ctxt1 * Math.sin(cellAngle),
    ];

    return {
      x: this.cx + x[position],
      y: this.cy + y[position],
      r,
      xtxt: this.cx + xtxt[position],
      ytxt: this.cy + ytxt[position],
    };
  }

  getNbCell(): number {
    return 8;
  }

  // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
  getNbContainerInCell(cell: number): number {
    return 3;
  }
}
