import { useSampleChangerUpdate } from 'components/investigation/changer/SampleChangerUpdateContext';
import { Button } from 'react-bootstrap';

export function SampleChangerUnload() {
  const { unloadAll, containers } = useSampleChangerUpdate();

  const anyLoaded = containers.some((container) => {
    return container.sampleChangerLocation !== undefined;
  });

  return (
    <Button
      disabled={!anyLoaded}
      variant="danger"
      onClick={() => {
        unloadAll();
      }}
      size="sm"
      test-id="changer-unload-all"
    >
      Unload all
    </Button>
  );
}
