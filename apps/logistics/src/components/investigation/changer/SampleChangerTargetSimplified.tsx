import type { ParcelContent } from '@edata-portal/icat-plus-api';
import { faClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSampleChangerUpdate } from 'components/investigation/changer/SampleChangerUpdateContext';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { EditContainerButton } from 'components/investigation/shipment/parcel/content/EditContainerButton';
import { Fragment, useEffect, useMemo } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { useDrag, useDrop } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';

export function SampleChangerTargetSimplified({
  changer,
}: {
  changer: AbstractSampleChanger;
}) {
  const cells = useMemo(() => {
    return Array.from({ length: changer.getNbCell() }, (_, i) => i);
  }, [changer]);

  return (
    <Container fluid className="p-0 overflow-hidden">
      <Row className="g-2">
        {cells.map((cell) => {
          return (
            <Fragment key={cell}>
              <CellTarget cell={cell} changer={changer} />
            </Fragment>
          );
        })}
      </Row>
    </Container>
  );
}

function CellTarget({
  cell,
  changer,
}: {
  cell: number;
  changer: AbstractSampleChanger;
}) {
  const positions = useMemo(() => {
    return Array.from(
      { length: changer.getNbContainerInCell(cell) },
      (_, i) => i,
    );
  }, [cell, changer]);

  return (
    <>
      {positions.map((position) => {
        return (
          <Col key={position} xs={12} md={6} lg={4} xl={3} xxl={2}>
            <PositionTarget cell={cell} position={position} changer={changer} />
          </Col>
        );
      })}
    </>
  );
}

function PositionTarget({
  cell,
  position,
  changer,
}: {
  cell: number;
  position: number;
  changer: AbstractSampleChanger;
}) {
  const {
    updateLocation,
    containersByParcel,
    getContainerParcel,
    updateLocalParcel,
  } = useSampleChangerUpdate();

  const [parcel, container] = useMemo(() => {
    const res = Object.keys(containersByParcel).map((parcel) => {
      const containers = containersByParcel[parcel]
        .filter((container) => container.sampleChangerLocation !== undefined)
        .filter((container) => {
          const containerPosition = changer.getPosition(
            container.sampleChangerLocation || 0,
          );
          if (!containerPosition) return undefined;
          return (
            containerPosition.cell === cell &&
            containerPosition.position === position
          );
        });
      return containers.length ? ([parcel, containers[0]] as const) : undefined;
    });
    return res.find((r) => r !== undefined) || [undefined, undefined];
  }, [containersByParcel, changer, cell, position]);

  const containerType = useMemo(() => {
    return changer.getContainerType(cell, position);
  }, [changer, cell, position]);

  const [{ isDragging }, dragRef, preview] = useDrag({
    type: 'container',
    item: container,
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [{ isOver, item }, dropRef] = useDrop({
    accept: 'container',
    drop: (item: ParcelContent) => {
      if (!item) return;
      if (!(item.containerType?.name === containerType.name)) return;
      if (!(item.containerType?.capacity === containerType.capacity)) return;
      updateLocation(
        item as ParcelContent,
        changer.getLocation(cell, position),
      );
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      item: monitor.getItem(),
    }),
  });

  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true });
  }, [preview]);

  const canDropHere = useMemo(() => {
    if (container) return false;
    const type = (item as any)?.containerType;
    if (!type) return false;
    return (
      type.name === containerType.name &&
      type.capacity === containerType.capacity
    );
  }, [item, containerType, container]);

  const classNameDrop = useMemo(() => {
    if (item) {
      if (canDropHere) {
        return isOver ? 'simplifiedCellCanDropOver' : 'simplifiedCellCanDrop';
      } else {
        return 'simplifiedCellCannotDrop';
      }
    } else {
      return '';
    }
  }, [item, canDropHere, isOver]);
  const classNameFilled = useMemo(() => {
    if (container) {
      return 'simplifiedCellFilled';
    } else {
      return '';
    }
  }, [container]);
  const className = useMemo(() => {
    return [
      'h-100 rounded',
      'simplifiedCell',
      classNameFilled,
      classNameDrop,
    ].join(' ');
  }, [classNameFilled, classNameDrop]);

  return (
    <div
      ref={container ? dragRef : dropRef}
      className={className}
      style={{
        position: 'relative',
      }}
      test-id={container ? 'changer-target-assigned' : 'changer-target-empty'}
    >
      <div className="d-flex flex-row flex-wrap p-2 justify-content-between me-3 gap-2 overflow-auto">
        <div className="d-flex flex-column">
          <strong>
            {cell + 1}-{position + 1}
          </strong>
          <span className="d-flex align-items-center">
            {container?.name || <>&nbsp;</>}
            {container && (
              <div className="ms-1">
                <EditContainerButton
                  container={container}
                  parcel={getContainerParcel(container)}
                  onDone={(newParcel) => {
                    if (newParcel) {
                      updateLocalParcel(newParcel);
                    }
                  }}
                  compact
                />
              </div>
            )}
          </span>
          <small>
            <i>{parcel ? `[${parcel}]` : <>&nbsp;</>}</i>
          </small>
        </div>

        <div
          style={{
            minWidth: 50,
            minHeight: 50,
            position: 'relative',
            opacity: isDragging ? 0.2 : 1,
          }}
          className="text-black"
        >
          {container ? changer.plotContainer(container) : null}
        </div>
      </div>
      {container && (
        <Button
          className="px-1 py-0"
          style={{
            position: 'absolute',
            top: 2,
            right: 2,
          }}
          variant="danger"
          onClick={() => {
            updateLocation(container, undefined);
          }}
          test-id="changer-target-unload"
        >
          <FontAwesomeIcon size="lg" icon={faClose} />
        </Button>
      )}
    </div>
  );
}
