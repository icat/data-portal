import {
  PARCEL_LIST_ENDPOINT,
  Parcel,
  ParcelContent,
  UPDATE_PARCEL_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import React, { useCallback, useMemo, useState } from 'react';

export type SampleChangerUpdateContextType = {
  updateLocation: (container: ParcelContent, location?: number) => void;
  containersByParcel: { [parcel: string]: ParcelContent[] };
  containers: ParcelContent[];
  unloadAll: () => void;
  getContainerParcel: (container: ParcelContent) => Parcel;
  updateLocalParcel: (parcel: Parcel) => void;
};

export const SampleChangerUpdateContext =
  React.createContext<SampleChangerUpdateContextType>(
    {} as SampleChangerUpdateContextType,
  );

/**
 * This manages a local parcel state to speed up UI updates (no need to wait for the server to respond)
 */
export function SampleChangerUpdateContextProvider({
  children,
  investigationId,
  shipmentId,
}: {
  children: React.ReactNode;
  investigationId: string;
  shipmentId: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      shipmentId,
      investigationId,
    },
    default: [] as Parcel[],
  });

  const [localParcels, setLocalParcels] = useState<Parcel[]>(
    JSON.parse(JSON.stringify(parcels)),
  );

  const containers = useMemo(() => {
    return localParcels
      .flatMap((parcel) => parcel.content)
      .filter((container) => container.type === 'CONTAINER');
  }, [localParcels]);

  const containersByParcel = useMemo(() => {
    return localParcels.reduce(
      (acc, parcel) => {
        acc[parcel.name] = parcel.content.filter(
          (content) => content.type === 'CONTAINER',
        );
        return acc;
      },
      {} as { [parcel: string]: ParcelContent[] },
    );
  }, [localParcels]);

  const updateParcel = useMutateEndpoint({
    endpoint: UPDATE_PARCEL_ENDPOINT,
    params: {
      investigationId,
      shipmentId,
    },
  });

  const updateLocation = useCallback(
    (container: ParcelContent, location?: number) => {
      setLocalParcels((parcels) => {
        const newParcels: Parcel[] = JSON.parse(JSON.stringify(parcels));
        let updatedAnyParcel = false;
        newParcels.forEach((newParcel) => {
          let updatedParcel = false;
          newParcel.content.forEach((content) => {
            if (content._id === container._id) {
              content.sampleChangerLocation = location;
              updatedParcel = true;
              updatedAnyParcel = true;
            }
          });
          if (updatedParcel) {
            updateParcel.mutate({
              body: newParcel,
            });
          }
        });
        if (updatedAnyParcel) {
          return newParcels;
        }
        return parcels;
      });
    },
    [updateParcel],
  );

  const unloadAll = useCallback(() => {
    setLocalParcels((parcels) => {
      const newParcels: Parcel[] = JSON.parse(JSON.stringify(parcels));
      let updatedAnyParcel = false;
      newParcels.forEach((newParcel) => {
        let updatedParcel = false;
        newParcel.content.forEach((content) => {
          if (
            content.type === 'CONTAINER' &&
            content.sampleChangerLocation !== undefined
          ) {
            content.sampleChangerLocation = undefined;
            updatedParcel = true;
            updatedAnyParcel = true;
          }
        });
        if (updatedParcel) {
          updateParcel.mutate({
            body: newParcel,
          });
        }
      });
      if (updatedAnyParcel) {
        return newParcels;
      }
      return parcels;
    });
  }, [updateParcel]);

  const getContainerParcel = useCallback(
    (container: ParcelContent) => {
      return localParcels.find((parcel) =>
        parcel.content.some((content) => content._id === container._id),
      ) as Parcel;
    },
    [localParcels],
  );

  const updateLocalParcel = useCallback((parcel: Parcel) => {
    setLocalParcels((parcels) => {
      const newParcels: Parcel[] = JSON.parse(JSON.stringify(parcels));
      let updatedAnyParcel = false;
      newParcels.forEach((newParcel, i) => {
        if (newParcel._id === parcel._id) {
          newParcels[i] = parcel;
          updatedAnyParcel = true;
        }
      });
      if (updatedAnyParcel) {
        return newParcels;
      }
      return parcels;
    });
  }, []);

  const value = useMemo(() => {
    return {
      updateLocation,
      containers,
      containersByParcel,
      unloadAll,
      getContainerParcel,
      updateLocalParcel,
    };
  }, [
    updateLocation,
    containers,
    containersByParcel,
    unloadAll,
    getContainerParcel,
    updateLocalParcel,
  ]);

  return (
    <SampleChangerUpdateContext.Provider value={value}>
      {children}
    </SampleChangerUpdateContext.Provider>
  );
}

export function useSampleChangerUpdate() {
  return React.useContext(SampleChangerUpdateContext);
}
