import { useSampleChangerUpdate } from 'components/investigation/changer/SampleChangerUpdateContext';
import { ChangerContent } from 'components/investigation/changer/content/ChangerContent';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';

export function SampleChangerTarget({
  changer,
}: {
  changer: AbstractSampleChanger;
}) {
  const { containers } = useSampleChangerUpdate();

  return (
    <div className="d-flex h-100 w-100 overflow-hidden justify-content-center">
      {changer.plotChanger(
        <ChangerContent containers={containers} changer={changer} />,
      )}
    </div>
  );
}
