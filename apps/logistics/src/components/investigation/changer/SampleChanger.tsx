import { NoScrollPage, useParam, prettyPrint } from '@edata-portal/core';
import {
  GET_TRACKING_SETUP_ENDPOINT,
  SHIPMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { CHANGER_CONFIG } from 'components/investigation/changer/config';
import { useMemo } from 'react';
import { Alert, Button } from 'react-bootstrap';
import { DndProvider } from 'react-dnd-multi-backend';
import { HTML5toTouch } from 'rdndmb-html5-to-touch';
import './SampleChanger.css';
import { ChangerDraggedContainer } from 'components/investigation/changer/content/ChangerDraggedContainer';
import { SampleChangerTarget } from 'components/investigation/changer/SampleChangerTarget';
import { SampleChangerSource } from 'components/investigation/changer/SampleChangerSource';
import { SampleChangerUpdateContextProvider } from 'components/investigation/changer/SampleChangerUpdateContext';
import { SampleChangerUnload } from 'components/investigation/changer/SampleChangerUnload';
import { SampleChangerTargetSimplified } from 'components/investigation/changer/SampleChangerTargetSimplified';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

export function SampleChanger({
  investigationId,
}: {
  investigationId: string;
}) {
  const shipments = useGetEndpoint({
    endpoint: SHIPMENT_LIST_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const shipmentId = useMemo(() => {
    return shipments?.[0]?._id.toString() || undefined;
  }, [shipments]);

  if (!shipmentId) {
    return (
      <Alert variant="warning">
        You must first configure a shipment for this experiment session.
      </Alert>
    );
  }

  return (
    <NoScrollPage>
      <ShipmentSampleChanger
        investigationId={investigationId}
        shipmentId={shipmentId}
      />
    </NoScrollPage>
  );
}

function ShipmentSampleChanger({
  investigationId,
  shipmentId,
}: {
  investigationId: string;
  shipmentId: string;
}) {
  const setup = useGetEndpoint({
    endpoint: GET_TRACKING_SETUP_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const changerType = useMemo(() => {
    if (!setup?.sampleChangerType) return undefined;
    const changer = setup.sampleChangerType[0]?.name;
    return changer && changer in CHANGER_CONFIG
      ? CHANGER_CONFIG[changer]
      : undefined;
  }, [setup]);

  const [view, setView] = useParam<'changer' | 'simplified'>('view', 'changer');

  if (!changerType) {
    return (
      <Alert variant="warning">
        Sample changer was not configured for this beamline
      </Alert>
    );
  }

  return (
    <DndProvider options={HTML5toTouch}>
      <SampleChangerUpdateContextProvider
        investigationId={investigationId}
        shipmentId={shipmentId}
      >
        <div className="h-100 w-100 p-2 d-flex flex-row flex-nowrap gap-1">
          <div
            className="d-flex flex-column gap-1 flex-nowrap border rounded border-2 border-info p-2"
            style={{
              flexShrink: 0,
              maxWidth: '30vw',
            }}
          >
            <h5 className="text-center">Parcels</h5>
            <div className="d-flex flex-row gap-1 flex-nowrap overflow-auto ">
              <SampleChangerSource changer={changerType} />
            </div>
          </div>
          <div className="d-flex flex-column h-100 justify-content-between align-items-center text-info">
            <div />
            <FontAwesomeIcon icon={faArrowRight} size="2x" />
            <FontAwesomeIcon icon={faArrowRight} size="2x" />
            <FontAwesomeIcon icon={faArrowRight} size="2x" />
            <div />
          </div>
          <div className="d-flex flex-column p-2 border border-2 border-info rounded h-100 w-100 gap-2">
            <h5 className="text-center m-0">
              {setup?.instrumentName || 'Beamline'} sample changer
            </h5>
            <div className="d-flex flex-row gap-2">
              {(['changer', 'simplified'] as const).map((v) => (
                <Button
                  key={v}
                  variant="outline-primary"
                  onClick={() => setView(v)}
                  active={view === v}
                  size="sm"
                  test-id={`changer-${v}-btn`}
                >
                  {prettyPrint(v)}
                </Button>
              ))}
              <SampleChangerUnload />
            </div>
            <div className="h-100 w-100 overflow-auto">
              {view === 'changer' ? (
                <SampleChangerTarget changer={changerType} />
              ) : (
                <SampleChangerTargetSimplified changer={changerType} />
              )}
            </div>
          </div>
        </div>
      </SampleChangerUpdateContextProvider>
      <ChangerDraggedContainer changer={changerType} />
    </DndProvider>
  );
}
