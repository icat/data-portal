import type { ParcelContent } from '@edata-portal/icat-plus-api';
import { useSampleChangerUpdate } from 'components/investigation/changer/SampleChangerUpdateContext';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { EditContainerButton } from 'components/investigation/shipment/parcel/content/EditContainerButton';
import { useEffect } from 'react';
import { useDrag } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';

export function SampleChangerSource({
  changer,
}: {
  changer: AbstractSampleChanger;
}) {
  const { containersByParcel } = useSampleChangerUpdate();

  return (
    <>
      {Object.keys(containersByParcel)
        .sort()
        .map((parcel) => {
          const containers = containersByParcel[parcel];

          return (
            <div
              key={parcel}
              className="p-1 border border-1 rounded bg-light h-100 d-flex flex-column align-items-center"
              style={{
                flexShrink: 0,
              }}
            >
              <strong>{parcel}</strong>

              <div className="d-flex flex-column flex-nowrap h-100 overflow-auto">
                {containers.map((container, i) => {
                  return (
                    <SampleChangerSourceItem
                      key={container._id}
                      container={container}
                      changer={changer}
                    />
                  );
                })}
              </div>
            </div>
          );
        })}
    </>
  );
}

function SampleChangerSourceItem({
  container,
  changer,
}: {
  container: ParcelContent;
  changer: AbstractSampleChanger;
}) {
  const position = container.sampleChangerLocation
    ? changer.getPosition(container.sampleChangerLocation)
    : undefined;

  const [{ isDragging }, dragRef, preview] = useDrag({
    type: 'container',
    item: container,
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true });
  }, [preview]);

  const { getContainerParcel, updateLocalParcel } = useSampleChangerUpdate();

  return (
    <div
      ref={dragRef}
      key={container._id}
      className="d-flex flex-column gap-0 align-items-center justify-content-center bg-white border rounded p-1 m-1"
      test-id="changer-source-item"
    >
      <div
        style={{
          minHeight: 75,
          minWidth: 75,
          position: 'relative',
          opacity: isDragging ? 0.2 : 1,
        }}
      >
        {changer.plotContainer(container)}
      </div>

      <span style={{ pointerEvents: 'none' }}>{container.name}</span>

      {position ? (
        <span
          className="border rounded p-1"
          style={{
            backgroundColor: 'yellow',
            pointerEvents: 'none',
          }}
        >
          <small>
            {position.cell + 1}-{position.position + 1}
          </small>
        </span>
      ) : null}
      <EditContainerButton
        container={container}
        parcel={getContainerParcel(container)}
        onDone={(newParcel) => {
          if (newParcel) {
            updateLocalParcel(newParcel);
          }
        }}
        compact
      />
    </div>
  );
}
