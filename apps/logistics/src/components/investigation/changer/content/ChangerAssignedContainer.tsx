import type { ParcelContent } from '@edata-portal/icat-plus-api';
import { useSampleChangerUpdate } from 'components/investigation/changer/SampleChangerUpdateContext';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { EditContainerButton } from 'components/investigation/shipment/parcel/content/EditContainerButton';
import { useEffect, useMemo } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDrag } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';

export function ChangerAssignedContainer({
  container,
  changer,
}: {
  container: ParcelContent;
  changer: AbstractSampleChanger;
}) {
  const [{ isDragging }, dragRef, preview] = useDrag({
    type: 'container',
    item: container,
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true });
  }, [preview]);

  const { getContainerParcel, updateLocalParcel } = useSampleChangerUpdate();

  const parcel = useMemo(() => {
    return getContainerParcel(container);
  }, [getContainerParcel, container]);

  return (
    <div
      ref={dragRef}
      style={{
        width: '100%',
        height: '100%',
        cursor: 'grab',
        position: 'absolute',
      }}
      test-id="changer-target-assigned"
    >
      <div
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
          opacity: isDragging ? 0.2 : 1,
        }}
      >
        {changer.plotContainer(container)}
      </div>

      <span
        className="bg-white border rounded text-center d-flex flex-row gap-1 align-items-center"
        style={{
          position: 'absolute',
          left: '50%',
          bottom: 15,
          transform: 'translate(-50%, 100%)',
          pointerEvents: 'none',
          fontSize: '0.8rem',
          padding: '0.1rem 0.3rem',
          whiteSpace: 'nowrap',
          lineHeight: 'normal',
          zIndex: 1,
        }}
      >
        <div
          style={{
            pointerEvents: 'all',
          }}
        >
          <EditContainerButton
            container={container}
            parcel={parcel}
            onDone={(v) => {
              if (v) {
                updateLocalParcel(v);
              }
            }}
            compact
          />
        </div>
        <div>
          {container.name}
          {parcel && (
            <i
              style={{
                fontSize: '0.7rem',
              }}
            >
              <br />
              {`[${parcel.name}]`}
            </i>
          )}
        </div>
        <RemoveContainerBtn container={container} />
      </span>
    </div>
  );
}
function RemoveContainerBtn({ container }: { container: ParcelContent }) {
  const changerUpdate = useSampleChangerUpdate();

  return (
    <OverlayTrigger
      placement="auto"
      overlay={
        <Tooltip key={'UnassignContainerButtonTooltip'}>
          {'Un-assign this container'}
        </Tooltip>
      }
    >
      <svg
        style={{
          position: 'absolute',
          right: 0,
          top: 0,
          width: 15,
          height: 15,
          cursor: 'pointer',
          transform: 'translate(50%, -50%)',
          pointerEvents: 'all',
        }}
        viewBox="0 0 1 1"
      >
        <circle
          test-id="changer-target-unload"
          onClick={
            container.sampleChangerLocation
              ? () => changerUpdate.updateLocation(container, undefined)
              : undefined
          }
          className="removeContainerBtn"
          cx={0.5}
          cy={0.5}
          r={0.5}
        ></circle>
        <text className="removeContainerBtnTxt" x={0.5} y={1}>
          ×
        </text>
      </svg>
    </OverlayTrigger>
  );
}
