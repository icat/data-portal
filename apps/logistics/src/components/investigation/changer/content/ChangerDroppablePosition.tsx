import type { ParcelContent } from '@edata-portal/icat-plus-api';
import { useSampleChangerUpdate } from 'components/investigation/changer/SampleChangerUpdateContext';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { useMemo } from 'react';
import { useDrop } from 'react-dnd';

export function ChangerDroppablePosition({
  changer,
  cell,
  position,
}: {
  changer: AbstractSampleChanger;
  cell: number;
  position: number;
}) {
  const changerUpdate = useSampleChangerUpdate();

  const containerType = useMemo(() => {
    return changer.getContainerType(cell, position);
  }, [changer, cell, position]);

  const [{ isOver, item }, dropRef] = useDrop({
    accept: 'container',
    drop: (item: ParcelContent) => {
      if (!item) return;
      if (!(item.containerType?.name === containerType.name)) return;
      if (!(item.containerType?.capacity === containerType.capacity)) return;
      changerUpdate.updateLocation(
        item as ParcelContent,
        changer.getLocation(cell, position),
      );
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      item: monitor.getItem(),
    }),
  });

  const canDropHere = useMemo(() => {
    const type = (item as any)?.containerType;
    if (!type) return false;
    return (
      type.name === containerType.name &&
      type.capacity === containerType.capacity
    );
  }, [item, containerType]);

  return (
    <>
      {changer.plotEmptyContainer(cell, position, canDropHere, isOver)}
      <div
        ref={dropRef}
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
        }}
        test-id="changer-target-empty"
      />
    </>
  );
}
