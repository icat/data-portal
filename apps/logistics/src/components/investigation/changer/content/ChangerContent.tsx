import type { ParcelContent } from '@edata-portal/icat-plus-api';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { ChangerAssignedContainer } from 'components/investigation/changer/content/ChangerAssignedContainer';
import { ChangerDroppablePosition } from 'components/investigation/changer/content/ChangerDroppablePosition';
import { range } from 'lodash';
import { useMemo } from 'react';

export function ChangerContent({
  containers,
  changer,
}: {
  containers: ParcelContent[];
  changer: AbstractSampleChanger;
}) {
  const positionnedContainers = useMemo(() => {
    return containers
      .filter((container) => container.sampleChangerLocation !== undefined)
      .map((container) => {
        const position = changer.getPosition(
          container.sampleChangerLocation || 0,
        );
        if (!position) return undefined;
        return {
          container: container,
          position: position,
        };
      });
  }, [containers, changer]);

  return (
    <>
      {range(changer.getNbCell()).flatMap((cell) => {
        return range(changer.getNbContainerInCell(cell)).map((position) => {
          const { x, y, r } = changer.getPositionCoordinates(cell, position);
          const container = positionnedContainers.find(
            (c) =>
              c?.position.cell === cell && c?.position.position === position,
          )?.container;
          return (
            <div
              key={`${cell}-${position}`}
              style={{
                position: 'absolute',
                left: (x - r) * 100 + '%',
                bottom: (y - r) * 100 + '%',
                width: r * 2 * 100 + '%',
                height: r * 2 * 100 + '%',
              }}
            >
              {container ? (
                <ChangerAssignedContainer
                  container={container}
                  changer={changer}
                />
              ) : (
                <ChangerDroppablePosition
                  changer={changer}
                  cell={cell}
                  position={position}
                />
              )}
            </div>
          );
        });
      })}
    </>
  );
}
