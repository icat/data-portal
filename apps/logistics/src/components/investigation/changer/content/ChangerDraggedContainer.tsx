import type { ParcelContent } from '@edata-portal/icat-plus-api';
import type { AbstractSampleChanger } from 'components/investigation/changer/definitions/AbstractSampleChanger';
import { XYCoord, useDragLayer } from 'react-dnd';

export function ChangerDraggedContainer({
  changer,
}: {
  changer: AbstractSampleChanger;
}) {
  const { itemType, isDragging, item, initialOffset, currentOffset } =
    useDragLayer((monitor) => ({
      item: monitor.getItem(),
      itemType: monitor.getItemType(),
      initialOffset: monitor.getInitialClientOffset(),
      currentOffset: monitor.getClientOffset(),
      isDragging: monitor.isDragging(),
    }));

  return (
    <div
      style={{
        position: 'fixed',
        pointerEvents: 'none',
        zIndex: 100,
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
      }}
    >
      {isDragging ? (
        <div style={getDragLayerStyles(initialOffset, currentOffset)}>
          {renderItem(changer, itemType, item as ParcelContent)}
        </div>
      ) : null}
    </div>
  );
}

function getDragLayerStyles(
  initialOffset: XYCoord | null,
  currentOffset: XYCoord | null,
): React.CSSProperties {
  if (!initialOffset || !currentOffset) {
    return {
      display: 'none',
    };
  }

  const { x, y } = currentOffset;

  const transform = `translate(${x}px, ${y}px) `;
  return {
    transform,
    WebkitTransform: transform,
  };
}

function renderItem(
  changer: AbstractSampleChanger,
  itemType: string | any,
  item: ParcelContent,
) {
  switch (itemType) {
    case 'container':
      return (
        <div
          style={{
            position: 'relative',
            width: 100,
            height: 100,
            transform: 'translate(-50%, -50%)',
          }}
        >
          {changer.plotContainer(item)}
        </div>
      );
    default:
      return null;
  }
}
