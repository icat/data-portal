import { Loading, useConfig } from '@edata-portal/core';
import {
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { useReimbursedParcels } from 'components/hooks';
import { INVESTIGATION_REIMBURSED_PARCELS_PARAM } from 'constants/investigation';
import { Suspense } from 'react';
import { Alert } from 'react-bootstrap';

export function InvestigationReimbursedParcels({
  investigationId,
  shipmentId,
}: {
  investigationId: number | string;
  shipmentId?: string;
}) {
  return (
    <Suspense fallback={<Loading />}>
      <LoadDisplay investigationId={investigationId} shipmentId={shipmentId} />
    </Suspense>
  );
}

function LoadDisplay({
  investigationId,
  shipmentId,
}: {
  investigationId: number | string;
  shipmentId?: string;
}) {
  const config = useConfig();
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: String(investigationId),
    },
  });
  const totalReimbursedParcels = useReimbursedParcels({
    investigationId: investigationId.toString(),
    shipmentId,
  });

  if (!config.ui.logistics.facilityReimbursmentEnabled) {
    return null;
  }
  if (!investigations?.length) return null;

  const investigation = investigations[0];

  if (!(INVESTIGATION_REIMBURSED_PARCELS_PARAM in investigation.parameters))
    return null;

  const nbReimbursed =
    Number(investigation.parameters[INVESTIGATION_REIMBURSED_PARCELS_PARAM]) ||
    0;

  const message =
    nbReimbursed > 0
      ? `you are allowed to have ${nbReimbursed} parcel${
          nbReimbursed > 1 ? 's' : ''
        } reimbursed by the ESRF.`
      : 'you are not allowed to have any parcels reimbursed by the ESRF.';

  const status =
    nbReimbursed > 0
      ? `(${totalReimbursedParcels} reimbursed selected out of ${nbReimbursed} allowed.)`
      : '';

  return (
    <Alert variant="info">{`According to the A-form, ${message} ${status}`}</Alert>
  );
}
