import {
  formatDateToDayAndTime,
  TanstackBootstrapTable,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  SAMPLE_FILES_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import {
  useReactTable,
  getCoreRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  getFilteredRowModel,
} from '@tanstack/react-table';
import { AddNewSampleFile } from 'components/sampleFile/AddNewSampleFile';
import { DownloadSampleFile } from 'components/sampleFile/DownloadSampleFile';
import { useMemo } from 'react';
import { Alert } from 'react-bootstrap';

export function SampleFilePicker({
  sampleId,
  onSelect,
  selected,
  fileType,
  readonly,
  groupname,
}: {
  sampleId: string;
  onSelect?: (file: string) => void;
  selected?: string;
  fileType?: string;
  readonly?: boolean;
  groupname?: string;
}) {
  const sampleFilesInfo = useGetEndpoint({
    endpoint: SAMPLE_FILES_LIST_ENDPOINT,
    params: {
      sampleId: Number(sampleId),
    },
  });

  const files = useMemo(() => {
    return sampleFilesInfo?.resources || [];
  }, [sampleFilesInfo?.resources]);

  const data = useMemo(() => {
    return files
      .filter((file) => !readonly || !selected || file.filename === selected)
      .filter((file) => !groupname || file.groupName === groupname)
      .filter((file) => !fileType || file.fileType === fileType);
  }, [fileType, files, groupname, readonly, selected]);

  const table = useReactTable({
    columns: [
      {
        accessorKey: 'filename',
        header: 'Name',
        enableColumnFilter: true,
        enableSorting: true,
      },
      {
        accessorFn: (row) => formatDateToDayAndTime(row?.createdAt),
        id: 'createdAt',
        header: 'Uploaded on',
        enableColumnFilter: true,
        enableSorting: true,
      },
      {
        accessorKey: '_id',
        id: '_id',
        header: '',
        cell: (row) => {
          return (
            <DownloadSampleFile
              resourceId={row.getValue() as string}
              sampleId={sampleId}
            />
          );
        },
        enableColumnFilter: false,
        enableSorting: false,
      },
    ],
    data: data,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    initialState: {
      sorting: [{ id: 'createdAt', desc: true }],
    },
  });

  return (
    <div className="d-flex flex-column gap-2">
      {!readonly && (
        <>
          <AddNewSampleFile
            sampleId={sampleId}
            onDone={(v) => {
              onSelect && onSelect(v.filename);
            }}
            fileType={fileType}
            groupname={groupname}
          />
          <hr />
        </>
      )}
      {data.length ? (
        <TanstackBootstrapTable
          table={table}
          dataLength={data.length}
          onRowClick={
            readonly || !onSelect
              ? undefined
              : (row) => {
                  onSelect(row.filename);
                }
          }
          striped={false}
          getTrStyle={(row) => {
            if (row.filename === selected) {
              return { backgroundColor: 'lightblue' };
            }
            return {};
          }}
          sorting
        />
      ) : (
        <Alert variant="info">No file found</Alert>
      )}
    </div>
  );
}
