import {
  useMutateEndpoint,
  SAMPLE_UPLOAD_FILE_ENDPOINT,
  ResourceInformation,
} from '@edata-portal/icat-plus-api';
import { Form } from 'react-bootstrap';

export function AddNewSampleFile({
  sampleId,
  onDone,
  fileType,
  groupname,
}: {
  sampleId: string;
  onDone: (file: ResourceInformation) => void;
  fileType?: string;
  groupname?: string;
}) {
  const upload = useMutateEndpoint({
    endpoint: SAMPLE_UPLOAD_FILE_ENDPOINT,
    params: {
      sampleId: Number(sampleId),
    },
  });

  return (
    <div>
      <Form.Label>
        <strong>Upload new file</strong>
      </Form.Label>
      <Form.Control
        type="file"
        multiple={false}
        onChange={(e) => {
          const target = e.target;
          if (!('files' in target)) return;
          const files = (e.target as any).files;
          if (files && files.length > 0) {
            const uploadFile = files[0];
            const data = new FormData();
            data.append('file', uploadFile);
            if (fileType) data.append('fileType', fileType);
            if (groupname) data.append('groupName', groupname);

            upload
              .mutateAsync({
                isFormData: true,
                body: data,
              })
              .then((res) => {
                if (res) {
                  res.resources.forEach((responseFile) => {
                    if (responseFile.filename === uploadFile.name) {
                      onDone(responseFile);
                    }
                  });
                }
              });
          }
        }}
      />
    </div>
  );
}
