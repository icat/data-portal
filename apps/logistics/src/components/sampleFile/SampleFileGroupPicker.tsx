import { Button, TanstackBootstrapTable } from '@edata-portal/core';
import {
  useGetEndpoint,
  SAMPLE_FILES_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { faAnglesLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  useReactTable,
  getCoreRowModel,
  getPaginationRowModel,
  getFilteredRowModel,
} from '@tanstack/react-table';
import { SampleFilePicker } from 'components/sampleFile/SampleFilePicker';
import React, { useMemo } from 'react';
import { Alert, Form } from 'react-bootstrap';

export function SampleFileGroupPicker({
  sampleId,
  selected,
  readonly,
  onSelect,
}: {
  sampleId: string;
  onSelect: (group: string | undefined) => void;
  selected: string;
  readonly?: boolean;
}) {
  const sampleFilesInfo = useGetEndpoint({
    endpoint: SAMPLE_FILES_LIST_ENDPOINT,
    params: {
      sampleId: Number(sampleId),
    },
  });

  const files = useMemo(() => {
    return sampleFilesInfo?.resources || [];
  }, [sampleFilesInfo?.resources]);

  const groups = useMemo(() => {
    return files
      .map((file) => file.groupName)
      .filter((v, i, a) => a.indexOf(v) === i)
      .filter((v): v is string => !!v);
  }, [files]);

  const [creatingGroup, setCreatingGroup] = React.useState<string | undefined>(
    undefined,
  );

  const data = useMemo(() => {
    return groups
      .filter((v, i, a) => a.indexOf(v) === i)
      .filter((v): v is string => !!v && v.length > 0)
      .filter((v) => !readonly || v === selected)
      .map((groupName) => ({
        name: groupName,
        items: files.filter((file) => file.groupName === groupName),
      }));
  }, [groups, readonly, selected, files]);

  const table = useReactTable({
    data: data,
    columns: [
      {
        accessorKey: 'name',
        header: 'Group',
        enableColumnFilter: true,
        enableSorting: false,
      },
      {
        accessorKey: 'items.length',
        header: 'Items',
        enableColumnFilter: false,
        enableSorting: false,
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
  });

  if (selected)
    return (
      <div className="d-flex flex-column gap-2">
        <h5>
          Selected group: <strong>{selected}</strong>
        </h5>
        {!readonly && (
          <>
            <Button
              onClick={() => {
                onSelect(undefined);
              }}
            >
              <FontAwesomeIcon icon={faAnglesLeft} /> Change group
            </Button>
            <hr />
          </>
        )}

        <strong>Group files</strong>
        <div className="bg-light p-2 border rounded">
          <GroupItems
            groupName={selected}
            sampleId={sampleId}
            readonly={readonly}
          />
        </div>
      </div>
    );

  return (
    <div className="d-flex flex-column gap-2">
      {!readonly && (
        <>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              if (creatingGroup) {
                onSelect(creatingGroup);
                setCreatingGroup(undefined);
              }
            }}
          >
            <Form.Label>
              <strong>Create new group</strong>
            </Form.Label>
            <div className="d-flex flex-row gap-2">
              <Form.Control
                style={{ maxWidth: 500 }}
                type="text"
                value={creatingGroup || ''}
                onChange={(e) => setCreatingGroup(e.target.value)}
                disabled={readonly}
                placeholder="Type new group name..."
              />
              <Form.Control
                style={{
                  flex: 0,
                }}
                type="submit"
                as={Button}
                disabled={!creatingGroup?.length}
              >
                Create
              </Form.Control>
            </div>
          </Form>
          <hr />
        </>
      )}
      {data.length ? (
        <TanstackBootstrapTable
          table={table}
          dataLength={data.length}
          onRowClick={(row) => {
            onSelect(row.name);
          }}
        />
      ) : (
        <Alert variant="info">No group found</Alert>
      )}
    </div>
  );
}

export function GroupItems({
  groupName,
  sampleId,
  readonly,
}: {
  groupName: string;
  sampleId: string;
  readonly?: boolean;
}) {
  return (
    <SampleFilePicker
      groupname={groupName}
      sampleId={sampleId}
      readonly={readonly}
    />
  );
}
