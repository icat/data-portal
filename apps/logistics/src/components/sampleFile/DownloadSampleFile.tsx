import {
  useEndpointURL,
  SAMPLE_DOWNLOAD_FILE_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';

export function DownloadSampleFile({
  resourceId,
  sampleId,
}: {
  resourceId: string;
  sampleId: string;
}) {
  const download = useEndpointURL(SAMPLE_DOWNLOAD_FILE_ENDPOINT);

  const url = download({
    sampleId: Number(sampleId),
    resourceId: resourceId as any,
  });

  return (
    <div>
      <Button
        size="sm"
        target="_blank"
        variant="link"
        href={url}
        onClick={(e) => {
          window.open(url, '_blank');
          e.stopPropagation();
        }}
      >
        <FontAwesomeIcon icon={faDownload} />
      </Button>
    </div>
  );
}
