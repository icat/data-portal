import type { Parcel } from '@edata-portal/icat-plus-api';
import { SummaryParcelsGroupedByInvestigation } from 'components/summary/SummaryParcelsGroupedByInvestigation';
import { Col } from 'react-bootstrap';

export function OngoingParcelsSummary({ parcels }: { parcels: Parcel[] }) {
  if (!parcels.length) return null;
  return (
    <Col>
      <strong>Ongoing parcels</strong>
      <SummaryParcelsGroupedByInvestigation parcels={parcels} showStatus />
    </Col>
  );
}
