import {
  HorizontalScroll,
  immutableArray,
  sortInvestigationByDistanceFromNow,
} from '@edata-portal/core';
import type { Investigation, Parcel } from '@edata-portal/icat-plus-api';
import { SummaryInvestigationParcels } from 'components/summary/SummaryInvestigationParcels';

export function SummaryParcelsGroupedByInvestigation({
  parcels,
  showStatus,
}: {
  parcels: Parcel[];
  showStatus?: boolean;
}) {
  const investigations = immutableArray(parcels)
    .map((parcel) => parcel.investigation as Investigation)
    .filter((investigation) => investigation !== undefined)
    .filter((v, i, a) => a.map((v) => v.id).indexOf(v.id) === i)
    .sort(sortInvestigationByDistanceFromNow)
    .toArray();

  return (
    <HorizontalScroll>
      {investigations.map((investigation) => {
        const investigationParcels = parcels.filter(
          (parcel) => parcel.investigationId === investigation.id,
        );
        return (
          <SummaryInvestigationParcels
            key={investigation.id}
            investigation={investigation}
            parcels={investigationParcels}
            showStatus={showStatus}
          />
        );
      })}
    </HorizontalScroll>
  );
}
