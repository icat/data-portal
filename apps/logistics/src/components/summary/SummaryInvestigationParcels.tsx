import { InvestigationLabel } from '@edata-portal/core';
import type { Investigation, Parcel } from '@edata-portal/icat-plus-api';
import { faBoxOpen } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ParcelStatusBadge } from 'components/investigation/shipment/parcel/ParcelStatusBadge';
import { Card, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function SummaryInvestigationParcels({
  investigation,
  parcels,
  showStatus,
}: {
  investigation: Investigation;
  parcels: Parcel[];
  showStatus?: boolean;
}) {
  return (
    <Card
      className="text-nowrap"
      style={{
        minWidth: 'auto',
      }}
    >
      <Card.Header className="p-1">
        <Link to={`/investigation/${investigation.id}/datasets`}>
          <InvestigationLabel investigation={investigation} />
        </Link>
      </Card.Header>
      <Card.Body className="p-0 small">
        <Table striped hover size="sm" className="m-0">
          <tbody>
            {parcels.map((parcel) => (
              <ParcelRow
                key={parcel._id}
                parcel={parcel}
                showStatus={showStatus}
              />
            ))}
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
}

function ParcelRow({
  parcel,
  showStatus,
}: {
  parcel: Parcel;
  showStatus?: boolean;
}) {
  const link = `/investigation/${parcel.investigationId}/logistics/parcel/${parcel._id}`;
  return (
    <tr>
      <td
        style={{
          width: 0,
          textAlign: 'start',
        }}
      >
        <Link to={link}>
          <FontAwesomeIcon icon={faBoxOpen} />
        </Link>
      </td>

      <td>
        <Link to={link} className="w-100 d-block">
          {parcel.name}
        </Link>
      </td>

      {showStatus ? (
        <td
          style={{
            width: 0,
            textAlign: 'end',
          }}
        >
          <Link to={link}>
            <ParcelStatusBadge status={parcel.status} />
          </Link>
        </td>
      ) : null}
    </tr>
  );
}
