import {
  DELETE_INVESTIGATION_ADDRESS_ENDPOINT,
  ShipmentAddress,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';

import { Button } from 'react-bootstrap';

export function RemoveAddressButton({
  address,
  onDone,
}: {
  address: ShipmentAddress;
  onDone?: (address: ShipmentAddress) => void;
}) {
  const { mutateAsync } = useMutateEndpoint({
    endpoint: DELETE_INVESTIGATION_ADDRESS_ENDPOINT,
    params: { investigationId: address.investigationId.toString() },
  });

  return (
    <Button
      variant="danger"
      size={'sm'}
      onClick={() => {
        mutateAsync({ body: address }).then((address) => {
          address && onDone?.(address);
        });
      }}
    >
      Remove
    </Button>
  );
}
