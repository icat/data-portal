import { Button, Modal } from 'react-bootstrap';

import {
  addToDate,
  formatDateToDay,
  formatDateToIcatDate,
  FormObjectDescription,
  InvestigationLabel,
  Loading,
  RenderForm,
  useUser,
} from '@edata-portal/core';
import {
  ShipmentAddress,
  UPDATE_INVESTIGATION_ADDRESS_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
  CREATE_INVESTIGATION_ADDRESS_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { Suspense, useMemo, useState } from 'react';

export function NewAddressButton({
  investigationId,
  onNew,
  variant = 'link',
}: {
  investigationId?: string;
  onNew?: (address: ShipmentAddress) => void;
  variant?: string;
}) {
  const { mutateAsync } = useMutateEndpoint({
    endpoint: CREATE_INVESTIGATION_ADDRESS_ENDPOINT,
  });

  const [show, setShow] = useState(false);
  return (
    <>
      <Button
        variant={variant}
        onClick={() => {
          setShow(true);
        }}
        className={variant === 'link' ? 'p-0' : undefined}
      >
        New address
      </Button>
      <AddressFormModal
        key={`${show}`}
        show={show}
        setShow={setShow}
        onSaved={(address) => {
          mutateAsync({
            body: address,
            params: {
              investigationId: address.investigationId.toString(),
            },
          }).then((address) => {
            address && onNew?.(address);
            setShow(false);
          });
        }}
        currentInvestigationId={investigationId}
      />
    </>
  );
}

export function EditAddressButton({
  address,
  onDone,
}: {
  address: ShipmentAddress;
  onDone?: (address: ShipmentAddress) => void;
}) {
  const { mutateAsync } = useMutateEndpoint({
    endpoint: UPDATE_INVESTIGATION_ADDRESS_ENDPOINT,
    params: { investigationId: address.investigationId.toString() },
  });

  const [show, setShow] = useState(false);

  return (
    <>
      <Button
        variant="primary"
        size={'sm'}
        onClick={() => {
          setShow(true);
        }}
      >
        Edit
      </Button>
      <AddressFormModal
        key={String(show)}
        show={show}
        setShow={setShow}
        onSaved={(address) => {
          mutateAsync({ body: address }).then((address) => {
            address && onDone?.(address);
            setShow(false);
          });
        }}
        address={address}
        currentInvestigationId={address.investigationId.toString()}
      />
    </>
  );
}

/**
 * Gives a list of investigations that the user can use to create an address
 * Investigations that can be used are the ones in the future (starting from two days ago for margin)
 * Administrators can use all future investigations
 * Instrument scientists can use all future investigations for their instruments
 * Participants can use all future investigations they are part of
 * @param currentInvestigationId If the address is created for an investigation, it will be first in the list
 */
function useAddressInvestigationChoices(currentInvestigationId?: string) {
  const startDate = useMemo(
    () =>
      formatDateToIcatDate(
        addToDate(new Date(), {
          days: -2, //two days margin
        }),
      ),
    [],
  );

  const user = useUser();

  const allFutureInvestigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: { startDate },
    default: [] as Investigation[],
    skipFetch: !user?.isAdministrator,
  });

  const myFutureInvestigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: { startDate, filter: 'participant' },
    default: [] as Investigation[],
  });

  const instrumentsFutureInvestigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: { startDate, filter: 'instrumentscientist' },
    default: [] as Investigation[],
    skipFetch: !user?.isInstrumentScientist,
  });

  const currentInvestigation = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: { ids: currentInvestigationId },
    skipFetch: !currentInvestigationId?.length,
    default: [] as Investigation[],
  });

  return useMemo(() => {
    return [
      ...currentInvestigation,
      ...myFutureInvestigations,
      ...instrumentsFutureInvestigations,
      ...allFutureInvestigations,
    ].filter((v, i, a) => a.findIndex((t) => t.id === v.id) === i);
  }, [
    currentInvestigation,
    myFutureInvestigations,
    instrumentsFutureInvestigations,
    allFutureInvestigations,
  ]);
}

function AddressFormModal({
  address,
  show,
  setShow,
  onSaved,
  currentInvestigationId,
}: {
  address?: ShipmentAddress;
  show: boolean;
  setShow: (show: boolean) => void;
  onSaved?: (address: ShipmentAddress) => void;
  currentInvestigationId?: string;
}) {
  return (
    <Modal
      show={show}
      backdrop="static"
      size="lg"
      onHide={() => setShow(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title id="address-modal-title">
          {address ? 'Edit' : 'Create'} address
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Suspense fallback={<Loading />}>
          <AddressFormContent
            address={address}
            onCancel={() => setShow(false)}
            onSaved={onSaved}
            currentInvestigationId={currentInvestigationId}
          />
        </Suspense>
      </Modal.Body>
    </Modal>
  );
}

function AddressFormContent({
  address,
  onCancel,
  onSaved,
  currentInvestigationId,
}: {
  address?: ShipmentAddress;
  onCancel: () => void;
  onSaved?: (address: ShipmentAddress) => void;
  currentInvestigationId?: string;
}) {
  const investigationChoices = useAddressInvestigationChoices(
    currentInvestigationId,
  );

  const descriptionAddress: FormObjectDescription<ShipmentAddress> = {
    investigationId: {
      type: 'select',
      label: 'Experiment session',
      required: true,
      col: { xs: 12 },
      options: investigationChoices.map((i) => ({
        value: i.id,
        label: <InvestigationLabel investigation={i} />,
        searchValue: `${i.name} ${i.instrument?.fullName} ${
          i.instrument?.name
        } ${i.title} ${formatDateToDay(i.startDate)}`,
      })),
    },
    name: {
      type: 'text',
      label: 'First name',
      required: true,
      col: { xs: 6, lg: 3 },
    },
    surname: {
      type: 'text',
      label: 'Last name',
      required: true,
      col: { xs: 6, lg: 3 },
    },
    email: {
      type: 'email',
      label: 'Email',
      required: true,
      col: { xs: 6, lg: 3 },
    },
    phoneNumber: {
      type: 'tel',
      label: 'Phone number',
      required: true,
      validate: (value) => {
        const pattern = /\+?\d+/;
        if (!pattern.test(value)) {
          return 'Invalid phone number';
        }
        if (value.length > 20) {
          return 'Phone number too long';
        }
        return undefined;
      },
      col: { xs: 6, lg: 3 },
    },
    companyName: { type: 'text', label: 'Institution', col: { xs: 12 } },
    address: {
      type: 'textarea',
      required: true,
      label: 'Address',
      col: { xs: 12 },
    },
    postalCode: {
      type: 'text',
      label: 'Postcode',
      required: true,
      validate: (value) => {
        const pattern = /\d+/;
        if (!pattern.test(value)) {
          return 'Invalid post code';
        }
        if (value.length > 7) {
          return 'Post code too long';
        }
        return undefined;
      },

      col: { xs: 12, lg: 4 },
    },
    city: {
      type: 'text',
      label: 'City',
      required: true,
      col: { xs: 12, lg: 4 },
    },
    country: {
      type: 'text',
      label: 'Country',
      required: true,
      col: { xs: 12, lg: 4 },
    },
  };

  return (
    <RenderForm
      description={descriptionAddress}
      initialValue={address}
      onSubmit={(data) => {
        onSaved?.(data);
      }}
      submitLabel="Save"
      onCancel={onCancel}
    />
  );
}
