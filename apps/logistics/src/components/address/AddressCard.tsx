import { HelpIconCol, useConfig } from '@edata-portal/core';
import type { ShipmentAddress } from '@edata-portal/icat-plus-api';
import type { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
  faUser,
  faPhone,
  faAt,
  faAddressCard,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NewAddressButton } from 'components/address/AddressFormModal';
import ReactSelect from 'react-select';
import { Col, Row, Card, Alert } from 'react-bootstrap';

export function getAdressLabel(address: ShipmentAddress) {
  return `${address.name} ${address.surname} - ${address.address} ${address.city} - ${address.country}`;
}

export function AddressCard({
  title,
  address,
  choices = [],
  onSelect = () => {},
  isLoading,
  message,
  investigationId,
  required,
  editable,
  isReturnAddress,
  testId,
}: {
  title?: string;
  address?: ShipmentAddress;
  choices?: ShipmentAddress[];
  onSelect?: (address: ShipmentAddress | undefined) => void;
  isLoading?: boolean;
  message?: string;
  investigationId: string;
  required?: boolean;
  editable?: boolean;
  isReturnAddress: boolean;
  testId?: string;
}) {
  const config = useConfig();
  const facilityName = config.ui.facilityName;
  return (
    <Col>
      {title && (
        <Row>
          <Col xs={'auto'}>
            <strong> {title}</strong>
          </Col>
          {message && <HelpIconCol message={message} />}
        </Row>
      )}
      <Card test-id={testId}>
        <Card.Body className="p-2">
          {editable ? (
            <>
              <ReactSelect
                isDisabled={isLoading}
                isClearable={!required}
                options={choices.map((choice) => ({
                  value: getAdressLabel(choice),
                  label: getAdressLabel(choice),
                }))}
                value={
                  address
                    ? {
                        value: getAdressLabel(address),
                        label: getAdressLabel(address),
                      }
                    : null
                }
                onChange={(value) => {
                  const selectedAddress = choices.find(
                    (choice) => getAdressLabel(choice) === value?.value,
                  );
                  onSelect(selectedAddress);
                }}
              />
              <NewAddressButton
                investigationId={investigationId}
                onNew={onSelect}
              />
            </>
          ) : address ? (
            <AddressInfo address={address} />
          ) : null}
          {!address ? (
            <Alert
              variant={isReturnAddress ? 'warning' : 'info'}
              className="m-0 p-2"
            >
              {isReturnAddress
                ? `This parcel has no return address and will be destroyed at the end of the experiment session unless the user takes it with them when leaving the ${facilityName}.`
                : 'No address selected'}
            </Alert>
          ) : null}
        </Card.Body>
      </Card>
    </Col>
  );
}

export function AddressInfo({
  address,
  hideEmail,
  hidePhone,
  hideAddress,
  hideName,
}: {
  address: ShipmentAddress;
  hideEmail?: boolean;
  hidePhone?: boolean;
  hideAddress?: boolean;
  hideName?: boolean;
}) {
  return (
    <>
      {hideName ? null : (
        <Row>
          <AddressRow icon={faUser}>
            {address.name} {address.surname}
          </AddressRow>
        </Row>
      )}
      <Row>
        {hidePhone ? null : (
          <Col xs={12}>
            <AddressRow icon={faPhone}>{address.phoneNumber}</AddressRow>
          </Col>
        )}
        {hideEmail ? null : (
          <Col xs={12}>
            <AddressRow icon={faAt}>{address.email}</AddressRow>
          </Col>
        )}
      </Row>
      {hideAddress ? null : (
        <Row>
          <AddressRow icon={faAddressCard}>
            <span>{address.companyName}</span>
            <span>{address.address}</span>
            <span>
              {address.postalCode} {address.city}
            </span>
            <span> {address.country}</span>
          </AddressRow>
        </Row>
      )}
    </>
  );
}

export function AddressRow({
  icon,
  children,
}: {
  icon: IconProp;
  children: React.ReactNode;
}) {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        lineHeight: 1,
        marginBottom: '1rem',
        overflowX: 'auto',
        overflowY: 'hidden',
      }}
    >
      <FontAwesomeIcon icon={icon} style={{ marginRight: '1rem' }} />
      <Card.Text
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {children}
      </Card.Text>
    </div>
  );
}
