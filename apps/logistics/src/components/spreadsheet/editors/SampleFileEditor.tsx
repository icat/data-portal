import { Button, Loading } from '@edata-portal/core';
import type { InputColumnDescription } from '@edata-portal/icat-plus-api';
import { BaseEditorComponent, HotEditorProps } from '@handsontable/react';
import { SampleFileGroupPicker } from 'components/sampleFile/SampleFileGroupPicker';
import { SampleFilePicker } from 'components/sampleFile/SampleFilePicker';
import type { SpreadSheetItem } from 'components/spreadsheet/spreadSheetLogic';
import { Suspense } from 'react';
import { Alert, Modal } from 'react-bootstrap';

type PropsType = {
  column: InputColumnDescription;
  readonly?: boolean;
  getRootItem: (row: number) => SpreadSheetItem;
  showParentModal?: () => void;
  hideParentModal?: () => void;
};

export class SampleFileEditor extends BaseEditorComponent<PropsType> {
  constructor(props: HotEditorProps & PropsType) {
    super(props as any);

    this.state = {
      value: undefined,
      showModal: false,
      row: undefined,
    };
  }

  stopMousedownPropagation(e: MouseEvent) {
    e.stopPropagation();
  }

  setValue(value: string | undefined, callback: (() => void) | undefined) {
    this.setState(() => {
      return { value: value };
    }, callback);
  }

  getValue() {
    return this.state.value;
  }

  open() {
    this.props.hideParentModal?.();
    this.setState({ showModal: true });
    // cannot be used in fullscreen mode
    if (document.fullscreenElement) {
      document.exitFullscreen();
    }
  }

  close() {
    this.setState({
      showModal: false,
    });
    this.props.showParentModal?.();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  prepare(
    row: any,
    col: any,
    prop: any,
    td: { getBoundingClientRect: () => any },
    originalValue: any,
    cellProperties: any,
  ) {
    super.prepare(row, col, prop, td, originalValue, cellProperties);
    this.setState({ row: row });
  }

  render() {
    const column = this.props.column;

    if (!column) {
      return null;
    }
    if (column.type !== 'sampleFile' && column.type !== 'sampleFileGroup') {
      return null;
    }
    const row = this.state.row;

    const item = this.props.getRootItem(row);

    const sampleId = item?.sampleId;

    const value = this.getValue();

    return (
      <Modal
        show={this.state.showModal}
        onHide={() => {
          this.close();
          this.finishEditing();
        }}
        onShow={() => this.open()}
        centered
        backdrop="static"
        keyboard={false}
        size="xl"
      >
        <Modal.Header>
          <Modal.Title>
            {this.props.readonly ? 'Viewing' : 'Editing'} value of{' '}
            <strong>
              {"'"}
              {column.header}
              {"'"}
            </strong>{' '}
            on row {row + 1}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            onMouseDown={(e) => {
              e.stopPropagation();
            }}
            className="d-flex flex-column gap-2"
          >
            {sampleId ? (
              <Suspense fallback={<Loading />}>
                {column.type === 'sampleFile' && (
                  <SampleFilePicker
                    fileType={column.fileType}
                    sampleId={sampleId}
                    onSelect={(file) => {
                      this.setValue(file, () => {
                        this.close();
                        this.finishEditing();
                      });
                    }}
                    selected={value}
                    readonly={this.props.readonly}
                  />
                )}
                {column.type === 'sampleFileGroup' && (
                  <SampleFileGroupPicker
                    sampleId={sampleId}
                    onSelect={(group) => {
                      this.setValue(group, () => {});
                    }}
                    selected={value}
                    readonly={this.props.readonly}
                  />
                )}
              </Suspense>
            ) : (
              <NoSample />
            )}
            <hr />

            <div className="d-flex justify-content-center">
              <Button
                onClick={() => {
                  this.close();
                  this.finishEditing();
                }}
              >
                Done
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

function NoSample() {
  return (
    <>
      <Alert variant="warning">
        You must first select a sample short name.
      </Alert>
    </>
  );
}
