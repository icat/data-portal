import type { InputColumnDescription } from '@edata-portal/icat-plus-api';
import { BaseEditorComponent, HotEditorProps } from '@handsontable/react';
import { SpreadSheetTable } from 'components/spreadsheet/SpreadSheetTable';
import type { SpreadSheetItem } from 'components/spreadsheet/spreadSheetLogic';

import React from 'react';
import { Modal } from 'react-bootstrap';

type PropsType = {
  column: InputColumnDescription;
  readonly?: boolean;
  getRootItem: (row: number) => SpreadSheetItem;
  showParentModal?: () => void;
  hideParentModal?: () => void;
};

export class ListEditor extends BaseEditorComponent<PropsType> {
  divRef = React.createRef<HTMLDivElement>();

  constructor(props: HotEditorProps & PropsType) {
    super(props as any);

    this.state = {
      value: undefined,
      showModal: false,
      row: undefined,
      temporaryHide: false,
    };

    this.divRef = React.createRef<HTMLDivElement>();
  }

  stopMousedownPropagation(e: MouseEvent) {
    e.stopPropagation();
  }

  setValue(value: string, callback: (() => void) | undefined) {
    this.setState(() => {
      return { value: value };
    }, callback);
  }

  getValue() {
    return this.state.value;
  }

  open() {
    this.props.hideParentModal?.();
    this.setState({ showModal: true, temporaryHide: false });
    // cannot be used in fullscreen mode
    if (document.fullscreenElement) {
      document.exitFullscreen();
    }
  }

  close() {
    this.setState({
      showModal: false,
    });
    this.props.showParentModal?.();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  prepare(
    row: any,
    col: any,
    prop: any,
    td: { getBoundingClientRect: () => any },
    originalValue: any,
    cellProperties: any,
  ) {
    super.prepare(row, col, prop, td, originalValue, cellProperties);
    this.setState({ row: row });
  }

  render() {
    const column = this.props.column;

    if (!column) {
      return null;
    }
    if (column.type !== 'table') {
      return null;
    }
    const columns = column.columns;
    if (!columns || !columns.length) {
      return null;
    }

    const value = this.getValue();
    const items = value ? JSON.parse(value) : [];

    const row = this.state.row;

    const getRootItem = () => this.props.getRootItem(row);

    return (
      <div
        onMouseDown={(e) => {
          e.stopPropagation();
        }}
      >
        <Modal
          show={this.state.showModal}
          onHide={() => {
            this.close();
          }}
          onShow={() => this.open()}
          centered
          backdrop="static"
          keyboard={false}
          size="xl"
          style={{
            zIndex: this.state.temporaryHide ? -1 : undefined,
          }}
        >
          <Modal.Header>
            <Modal.Title>
              {this.props.readonly ? 'Viewing' : 'Editing'} value of{' '}
              <strong>
                {"'"}
                {column.header}
                {"'"}
              </strong>{' '}
              on row {row + 1}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div
              ref={this.divRef}
              onMouseDown={(e) => {
                e.stopPropagation();
              }}
            >
              <SpreadSheetTable
                name={column.name}
                initialItems={items}
                columns={columns}
                readonly={this.props.readonly}
                onSave={(items) => {
                  this.setValue(JSON.stringify(items), () => {
                    this.finishEditing();
                  });
                  return Promise.resolve(true);
                }}
                onCancel={() => {
                  this.setValue(value, () => {
                    this.finishEditing();
                  });
                }}
                getRootItem={getRootItem}
                showParentModal={() => {
                  this.setState({ temporaryHide: false });
                }}
                hideParentModal={() => {
                  this.setState({ temporaryHide: true });
                }}
              />
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
