import type { Config } from '@edata-portal/core';
import type { InputColumnDescription } from '@edata-portal/icat-plus-api';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { isEmptyValue } from 'components/spreadsheet/spreadSheetLogic';
import Handsontable from 'handsontable';
import _ from 'lodash';

export function getHotTableSetting(
  columns: InputColumnDescription[],
  config: Config,
  columnGroups?: {
    header: string;
    columns: number;
  }[],
): Handsontable.GridSettings {
  const colHeaders = columns.map((c) => {
    const header = c.header || c.name || '';

    if (!c.description) return header;

    //Add help icon if description is available
    const [width, height, , , path] = faQuestionCircle.icon;

    return (
      header +
      ` <svg
      fill="grey"
      style="height:1em;width:1em;pointer-events:none;"
      viewBox="0 0 ${width} ${height}">
      <path d="${Array.isArray(path) ? path.join(' ') : path}"/>
      </svg>`
    );
  });

  const nestedHeaders = columnGroups
    ? columnGroups
        .filter((g) => g.columns)
        .map((g) => ({
          label: g.header,
          colspan: g.columns,
        }))
    : undefined;

  return {
    licenseKey: config.ui.handsonTableLicenseKey,
    stretchH: 'all',
    rowHeaders: true,
    colHeaders: true,
    autoColumnSize: {
      syncLimit: 100,
    },
    collapsibleColumns: false,
    nestedHeaders: nestedHeaders ? [nestedHeaders, colHeaders] : [colHeaders],
  };
}

export function getHotColumnSetting(
  column: InputColumnDescription,
): Handsontable.ColumnSettings {
  if (column.type === 'select') {
    const values = column.values.map((v) =>
      typeof v === 'object' ? v.label : v,
    );
    const longestValue = _(values).maxBy((v) => v.length);
    const width = longestValue ? longestValue.length * 10 + 20 : 100;

    return {
      title: column.header,
      type: 'dropdown',
      filter: true,
      strict: true,
      allowInvalid: false,
      source: values,
      width: width,
    };
  }
  if (column.type === 'table') {
    return {
      title: column.header,
      renderer(instance, TD, row, col, prop, value, cellProperties) {
        Handsontable.renderers.TextRenderer.apply(this, arguments as any);
        if (isEmptyValue(value)) {
          return;
        }
        const items = JSON.parse(value) || [];
        if (items.length === 0) {
          return;
        }
        TD.innerHTML = `${items.length} item${items.length > 1 ? 's' : ''}...`;
        TD.style.fontStyle = 'italic';
        TD.style.color = 'blue';
      },
    };
  }
  if (column.type === 'sampleFile') {
    return {
      title: column.header,
      renderer: Handsontable.renderers.TextRenderer,
    };
  }
  return {
    title: column.header,
  };
}
