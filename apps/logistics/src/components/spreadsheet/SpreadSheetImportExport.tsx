import type { InputColumnDescription } from '@edata-portal/icat-plus-api';
import { faUpload, faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Papa from 'papaparse';
import { useRef, useMemo } from 'react';
import {
  Col,
  Row,
  Button,
  Form,
  DropdownButton,
  Dropdown,
} from 'react-bootstrap';
import * as XLSX from 'xlsx';

const HEADER_SEPARATOR = ' - ';

export function SpreadSheetImportExport({
  onImport,
  data,
  columns,
  name,
}: {
  onImport: (data: any[][]) => void;
  data: any[][];
  columns: InputColumnDescription[];
  name: string;
}) {
  return (
    <Col xs={'auto'}>
      <div
        className="bg-gradient-secondary rounded border p-3 mb-2"
        style={{
          borderColor: 'var(--bs-secondary)',
        }}
      >
        <Row className="g-1">
          <Import onImport={onImport} columns={columns} />
          <Export data={data} columns={columns} name={name} />
        </Row>
        <HandsOnTableHelpMessage />
      </div>
    </Col>
  );
}

function HandsOnTableHelpMessage() {
  return (
    <Col>
      <Row>
        <div className="bg-secondary rounded" style={{ marginTop: '4px' }}>
          <small>
            <strong>Supported formats: CSV, XLS, XLSX</strong>
            <br />
            <i>
              Columns should match the table below. You can download the
              template file to fill in the data.
              <br />
              Import will overwrite the current table data.
            </i>
          </small>
        </div>
      </Row>
    </Col>
  );
}

function Import({
  onImport,
  columns,
}: {
  onImport: (data: any[][]) => void;
  columns: InputColumnDescription[];
}) {
  const parseOptions = {
    header: false,
    dynamicTyping: true,
    skipEmptyLines: true,
    comments: '#',

    complete: function (results: Papa.ParseResult<any>) {
      const data = results.data as any[][];
      const firstRow = data[0];
      const firstRowIsHeader = firstRow.every((cell, i) => {
        const column: InputColumnDescription = columns[i];
        return (
          String(cell).toLowerCase().split(HEADER_SEPARATOR)[0].trim() ===
          (column.header || column.name || '').toLowerCase().trim()
        );
      });
      onImport(firstRowIsHeader ? data.slice(1) : data);
    },
  };

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    // @ts-ignore
    e.target.value = null;

    if (!file) return;

    if (file.name.includes('xls')) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const content = e.target?.result;
        if (!content) return;
        const workbook = XLSX.read(content, { type: 'binary' });
        const sheet = workbook.Sheets[workbook.SheetNames[0]];
        const data = XLSX.utils.sheet_to_csv(sheet);
        Papa.parse(data, parseOptions);
      };
      reader.readAsBinaryString(file);
    }
    if (file.name.includes('csv')) {
      Papa.parse(file, parseOptions);
    }
  };

  const inputRef = useRef<HTMLInputElement>(null);

  const openFileSelector = () => {
    inputRef.current?.click();
  };

  return (
    <Col xs={'auto'}>
      <Button variant="primary" onClick={openFileSelector}>
        <FontAwesomeIcon
          icon={faUpload}
          style={{
            marginRight: '0.5rem',
          }}
        />
        Import file
      </Button>
      <Form.Control
        ref={inputRef}
        type="file"
        accept=".csv,.xls,.xlsx"
        onChange={handleFileChange}
        id="csv-file-input"
        style={{
          display: 'none',
        }}
      />
    </Col>
  );
}

function Export({
  data,
  columns,
  name,
}: {
  data: any[][];
  columns: InputColumnDescription[];
  name: string;
}) {
  const cleanData = useMemo(() => {
    return data.filter((row) => {
      return row.some((cell) => cell !== undefined && cell !== null);
    });
  }, [data]);

  const hasData = cleanData.length > 0;

  const headerCounts = columns.reduce(
    (acc, column) => {
      const key = column.header || column.name || '';
      acc[key] = (acc[key] || 0) + 1;
      return acc;
    },
    {} as Record<string, number>,
  );

  // return the columns and data to be exported
  // if a column header is duplicated, we add the name
  const getColumnsToExport = (exporting: any[][]) => {
    const toExport = [
      columns.map((c) => {
        const header = c.header || c.name || '';
        return headerCounts[header] > 1 &&
          header.toLowerCase() !== c.name.toLowerCase()
          ? `${header}${HEADER_SEPARATOR}${c.name}`
          : header;
      }),
      ...exporting,
    ];
    return toExport;
  };

  const exportToXLSX = (exporting: any[][], name: string) => {
    const toExport = getColumnsToExport(exporting);
    const sheet = XLSX.utils.aoa_to_sheet(toExport);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, sheet, name.slice(0, 31));
    XLSX.writeFile(workbook, name + '.xlsx');
  };

  const exportToCSV = (exporting: any[][], name: string) => {
    const toExport = getColumnsToExport(exporting);
    const csv = Papa.unparse(toExport);
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.setAttribute('href', url);
    link.setAttribute('download', name + '.csv');
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <>
      <Col xs={'auto'}>
        <DropdownButton
          variant="primary"
          disabled={!hasData}
          title={
            <span>
              <FontAwesomeIcon
                icon={faDownload}
                style={{
                  marginRight: '0.5rem',
                }}
              />
              Export data
            </span>
          }
        >
          <Dropdown.Item
            disabled={!hasData}
            onClick={() => exportToXLSX(cleanData, name + '_export')}
          >
            XLSX
          </Dropdown.Item>
          <Dropdown.Item
            disabled={!hasData}
            onClick={() => exportToCSV(cleanData, name + '_export')}
          >
            CSV
          </Dropdown.Item>
        </DropdownButton>
      </Col>
      <Col xs={'auto'}>
        <DropdownButton
          variant="primary"
          title={
            <span>
              <FontAwesomeIcon
                icon={faDownload}
                style={{
                  marginRight: '0.5rem',
                }}
              />
              Download template
            </span>
          }
        >
          <Dropdown.Item onClick={() => exportToXLSX([], name + '_template')}>
            XLSX
          </Dropdown.Item>
          <Dropdown.Item onClick={() => exportToCSV([], name + '_template')}>
            CSV
          </Dropdown.Item>
        </DropdownButton>
      </Col>
    </>
  );
}
