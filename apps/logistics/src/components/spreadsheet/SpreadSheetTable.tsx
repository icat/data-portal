import { useConfig } from '@edata-portal/core';
import type { InputColumnDescription } from '@edata-portal/icat-plus-api';
import {
  faCheckCircle,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { HotColumn, HotTable, HotTableClass } from '@handsontable/react';
import { ListEditor } from 'components/spreadsheet/editors/ListEditor';
import { SpreadSheetImportExport } from 'components/spreadsheet/SpreadSheetImportExport';
import {
  applyChangesToSpreadSheetData,
  detectItemsErrors,
  generateSpreadSheetCellProperties,
  getSpreadSheetTableData,
  handleSpreadSheetFillChanges,
  parseSpreadSheetTableData,
  SpreadSheetError,
  SpreadSheetItem,
} from 'components/spreadsheet/spreadSheetLogic';
import {
  getHotColumnSetting,
  getHotTableSetting,
} from 'components/spreadsheet/spreadSheetSettings';
import Handsontable from 'handsontable';
import isEqual from 'lodash/isEqual';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { Button, Col, Overlay, Popover, Row, Spinner } from 'react-bootstrap';
import { unstable_usePrompt } from 'react-router-dom';

import './spreadsheet.css';
import 'handsontable/dist/handsontable.full.css';
import { registerCellType } from 'handsontable/cellTypes';
import { SampleFileEditor } from 'components/spreadsheet/editors/SampleFileEditor';

registerCellType(Handsontable.cellTypes.dropdown);
registerCellType(Handsontable.cellTypes.handsontable);

export function SpreadSheetTable({
  initialItems,
  columns,
  readonly,
  onSave,
  onChanges,
  detectExtraItemsErrors,
  onClickOnMenus,
  name,
  onCancel,
  columnGroups,
  saving,
  maxItems,
  removeEmptyRows = true,
  getRootItem,
  showParentModal,
  hideParentModal,
}: {
  initialItems: SpreadSheetItem[];
  columns: InputColumnDescription[];
  readonly?: boolean;
  onSave?: (items: SpreadSheetItem[]) => Promise<boolean>; //return true if saving was successful
  onChanges?: (items: SpreadSheetItem[], valid: boolean) => void;
  detectExtraItemsErrors?: (items: SpreadSheetItem[]) => SpreadSheetError[];
  onClickOnMenus?: () => void;
  name: string;
  onCancel?: () => void;
  columnGroups?: {
    header: string;
    columns: number;
  }[];
  saving?: boolean;
  maxItems?: number;
  removeEmptyRows?: boolean;
  getRootItem?: (row: number) => SpreadSheetItem;
  showParentModal?: () => void;
  hideParentModal?: () => void;
}) {
  const maxRows = useMemo(() => {
    const defaultMax = 1000;
    if (maxItems) return Math.max(initialItems.length, maxItems);
    return readonly ? initialItems.length || defaultMax : defaultMax;
  }, [readonly, initialItems.length, maxItems]);

  const height = useMemo(() => {
    return Math.max(Math.min(500, 25 * maxRows + 50), 100);
  }, [maxRows]);

  const [items, setItems] = React.useState<SpreadSheetItem[]>(
    initialItems || [],
  );

  const getErrors = useCallback(
    (items: SpreadSheetItem[]) => {
      return [
        ...detectItemsErrors(items, columns),
        ...(detectExtraItemsErrors ? detectExtraItemsErrors(items) : []),
      ];
    },
    [columns, detectExtraItemsErrors],
  );

  const errors = useMemo(() => getErrors(items), [getErrors, items]);

  const data = useMemo(
    () => getSpreadSheetTableData(items, columns, maxRows),
    [columns, items, maxRows],
  );

  const isSaved = useMemo(() => {
    return isEqual(
      data,
      getSpreadSheetTableData(initialItems, columns, maxRows),
    );
  }, [data, columns, initialItems, maxRows]);

  const [touched, setTouched] = React.useState(false);

  useEffect(() => {
    if (touched) return;
    if (isSaved) return;
    //initial items have changed without any user interaction with the table
    //=> they have been updated from the outside, we need to re-synchronize the table
    setItems(initialItems || []);
  }, [initialItems]); // eslint-disable-line react-hooks/exhaustive-deps

  unstable_usePrompt({
    when: !isSaved,
    message: 'You have unsaved changes. Are you sure you want to leave?',
  });

  const canBeSaved = useMemo(() => {
    return !isSaved && errors.length === 0;
  }, [errors, isSaved]);

  //copy data to avoid mutation
  const hotTableData = useMemo(() => JSON.parse(JSON.stringify(data)), [data]);

  const config = useConfig();

  const settings = useMemo(
    () => getHotTableSetting(columns, config, columnGroups),
    [columns, config, columnGroups],
  );

  const columnSettings = useMemo(
    () => columns.map((c) => getHotColumnSetting(c)),
    [columns],
  );

  const hotTableComponent = useRef<HotTableClass>(null);

  const handleChanges = (
    changes: (Handsontable.CellChange | null)[],
    source: Handsontable.ChangeSource,
  ) => {
    if (readonly) return false;

    let nonNullChanges = changes.filter(
      (c): c is Handsontable.CellChange => c !== null,
    );

    if (nonNullChanges.length === 0) return;

    setTouched(true);

    //if change is autofill, apply increments
    if (source === 'Autofill.fill') {
      nonNullChanges = handleSpreadSheetFillChanges(nonNullChanges, columns);
    }

    // handsontable handles changes by mutating the data array, which conflicts with react's state management
    // so we need to copy the data array and apply the changes to the copy, then update the state with the copy
    // then we return false to tell handsontable to deny the changes, which prevents handsontable from applying them to the original data array

    //ignore empty and null changes
    const filteredChanges = nonNullChanges.filter(
      ([, , oldValue, newValue]) => {
        return oldValue !== newValue;
      },
    );
    if (filteredChanges.length === 0) return;

    const newData = applyChangesToSpreadSheetData(
      filteredChanges,
      data,
      columns,
    );
    const newItems = parseSpreadSheetTableData(
      newData,
      columns,
      removeEmptyRows,
    );
    setItems(newItems);
    if (onChanges) onChanges(newItems, getErrors(newItems).length === 0);
    return false;
  };

  const refForFullScreen = useRef<HTMLDivElement>(null);
  const [isFullScreen, setIsFullScreen] = React.useState(false);

  useEffect(() => {
    const element = refForFullScreen.current;
    if (!element) return;
    if (isFullScreen) {
      element.requestFullscreen();
      const listener = () => {
        if (!document.fullscreenElement) {
          setIsFullScreen(false);
        }
      };
      document.addEventListener('fullscreenchange', listener);
      return () => {
        document.removeEventListener('fullscreenchange', listener);
      };
    } else if (document.fullscreenElement === element) {
      document.exitFullscreen();
    }
    return;
  }, [isFullScreen]);

  const hasColor = useMemo(() => {
    return columns.some((c) => c.colored);
  }, [columns]);

  const separationIndexes = useMemo(() => {
    const indexes: number[] = [];
    if (!columnGroups) return indexes;
    columnGroups.forEach((g) => {
      indexes.push(
        indexes.length ? indexes[indexes.length - 1] + g.columns : g.columns,
      );
    });
    return indexes.slice(0, -1);
  }, [columnGroups]);

  const [tooltipColumn, setTooltipColumn] = React.useState<{
    column: number;
    element: HTMLElement;
  }>();

  const getItem = useCallback(
    (row: number) => {
      return items[row];
    },
    [items],
  );

  return (
    <>
      <Col>
        {!readonly && (
          <Row onClick={onClickOnMenus}>
            <SpreadSheetImportExport
              onImport={(newData) => {
                setTouched(true);
                const newItems = parseSpreadSheetTableData(
                  newData,
                  columns,
                  removeEmptyRows,
                );
                setItems(newItems);
              }}
              data={data}
              columns={columns}
              name={name}
            />
          </Row>
        )}

        <Row className="g-1" onClick={onClickOnMenus}>
          {!readonly && (
            <>
              {onSave && (
                <>
                  <Col xs={'auto'}>
                    <Button
                      variant="primary"
                      onClick={() => {
                        onSave(items).then((saved) => {
                          if (saved) setTouched(false);
                        });
                      }}
                      disabled={!canBeSaved || saving}
                    >
                      {saving && <Spinner size="sm" className="me-2" />}
                      Save
                    </Button>
                  </Col>
                  <Col xs={'auto'}>
                    <Button
                      variant="primary"
                      onClick={() => {
                        setItems(initialItems || []);
                        setTouched(false);
                      }}
                      disabled={isSaved}
                    >
                      Discard changes
                    </Button>
                  </Col>
                </>
              )}
              <Col xs={'auto'}>
                <Button
                  variant="primary"
                  onClick={() => setIsFullScreen(!isFullScreen)}
                >
                  Fullscreen
                </Button>
              </Col>
              <Col xs={'auto'}>
                <div
                  className={`p-2 mb-1 ${
                    errors.length
                      ? 'text-danger'
                      : isSaved
                        ? 'text-success'
                        : 'text-warning'
                  }`}
                >
                  <FontAwesomeIcon
                    className="me-2"
                    icon={
                      isSaved && !errors.length
                        ? faCheckCircle
                        : faExclamationTriangle
                    }
                  />
                  <strong>
                    {errors.length
                      ? 'Errors need to be fixed before saving.'
                      : onSave
                        ? isSaved
                          ? 'No changes to save.'
                          : 'Some changes are not saved.'
                        : ''}
                  </strong>
                </div>
              </Col>
            </>
          )}
          {onCancel && (
            <Col xs={'auto'}>
              <Button variant="warning" onClick={onCancel}>
                Cancel
              </Button>
            </Col>
          )}
        </Row>

        {hasColor && (
          <Row className="g-1" onClick={onClickOnMenus}>
            <Col>
              <small>
                <strong>Same values have same color.</strong>
              </small>
            </Col>
          </Row>
        )}
        <div ref={refForFullScreen}>
          <HotTable
            ref={hotTableComponent}
            colHeaders={true}
            rowHeaders={true}
            comments={{ displayDelay: 2000 }}
            height={isFullScreen ? '100%' : height}
            settings={{
              data: hotTableData,
              ...settings,
              cells(row: number, col: number) {
                return generateSpreadSheetCellProperties(
                  row,
                  col,
                  data,
                  errors,
                  columns,
                  separationIndexes,
                );
              },
              beforeChange: handleChanges,
            }}
            afterGetColHeader={(col, th) => {
              const hasColSpan = () => {
                //if colSpan is defined and greater than 1, it means this is a group header not a column
                return th.colSpan && Number(th.colSpan) > 1;
              };

              if (col < 0) return;
              if (hasColSpan()) return;

              th.onmouseenter = () => {
                if (hasColSpan()) return;
                setTooltipColumn({
                  column: col,
                  element: th,
                });
              };
              th.onmouseleave = () => {
                setTooltipColumn(undefined);
              };
            }}
          >
            {columnSettings.map((c, i) => {
              const column = columns[i];

              return (
                <HotColumn key={column.name} settings={c}>
                  {column && column.type === 'table' ? (
                    <ListEditor
                      hot-editor
                      editorColumnScope={1}
                      column={column}
                      readonly={readonly}
                      getRootItem={getRootItem || getItem}
                      showParentModal={showParentModal}
                      hideParentModal={hideParentModal}
                    />
                  ) : undefined}
                  {column &&
                  (column.type === 'sampleFile' ||
                    column.type === 'sampleFileGroup') ? (
                    <SampleFileEditor
                      hot-editor
                      editorColumnScope={1}
                      column={column}
                      readonly={readonly}
                      getRootItem={getRootItem || getItem}
                      showParentModal={showParentModal}
                      hideParentModal={hideParentModal}
                    />
                  ) : undefined}
                </HotColumn>
              );
            })}
          </HotTable>
        </div>
      </Col>
      {tooltipColumn && (
        <RenderTooltip
          columns={columns}
          column={tooltipColumn.column}
          key={tooltipColumn.column}
          element={tooltipColumn.element}
        />
      )}
    </>
  );
}

export function RenderTooltip({
  columns,
  column,
  element,
}: {
  columns: InputColumnDescription[];
  column: number;
  element: HTMLElement;
}) {
  const columnDescription = columns[column];
  if (!columnDescription) return null;
  if (!columnDescription.description) return null;

  return (
    <>
      <Overlay placement="bottom" show={true} target={element}>
        {(props) => {
          return (
            <Popover {...props}>
              <Popover.Header>{columnDescription.header}</Popover.Header>
              <Popover.Body>{columnDescription.description}</Popover.Body>
            </Popover>
          );
        }}
      </Overlay>
    </>
  );
}
