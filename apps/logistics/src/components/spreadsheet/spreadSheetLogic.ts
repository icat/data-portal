import type { InputColumnDescription } from '@edata-portal/icat-plus-api';
import classNames from 'classnames';
import {
  SAMPLE_TYPE_COLUMN_NAME,
  SAMPLESHEET_COLUMN_NAME,
} from 'components/investigation/shipment/parcel/content/tableconfig';
import type Handsontable from 'handsontable';
import _ from 'lodash';

export type SpreadSheetItem = {
  [key: string]: any;
};

export type SpreadSheetError = {
  row: number;
  column: number;
  message: string;
};

export function detectItemsErrors(
  items: SpreadSheetItem[],
  columns: InputColumnDescription[],
): SpreadSheetError[] {
  return items.flatMap((item, row) => detectItemErrors(item, row, columns));
}

export function detectItemErrors(
  item: SpreadSheetItem,
  row: number,
  columns: InputColumnDescription[],
): SpreadSheetError[] {
  const errors: SpreadSheetError[] = [];

  //empty line has no errors
  if (Object.keys(item).length === 0) return errors;

  //check required fields
  columns.forEach((c, colIndex) => {
    if (c.required) {
      const value = (item as any)[c.name];
      if (isEmptyValue(value)) {
        errors.push({
          row,
          column: colIndex,
          message: `${c.header} is required.`,
        });
      }
    }
  });

  //check select values
  columns.forEach((c, colIndex) => {
    if (c.type === 'select') {
      const value = (item as any)[c.name];
      if (!isEmptyValue(value)) {
        if (
          !_(c.values)
            .map((v) => (typeof v === 'object' ? v.value : v))
            .map((v) => String(v))
            .includes(String(value))
        ) {
          errors.push({
            row,
            column: colIndex,
            message: `This value is not allowed for ${c.header}.`,
          });
        }
      }
    }
  });

  return errors;
}

export function isEmptyValue(value: any) {
  return (
    value === undefined || value === null || String(value).trim().length === 0
  );
}

export function convertValueToLabel(
  value: any,
  column: InputColumnDescription,
) {
  if (isEmptyValue(value)) return value;

  if (column.type === 'select') {
    const option = column.values.find((v) => {
      if (typeof v === 'object') {
        return String(v.value) === String(value);
      } else {
        return String(v) === String(value);
      }
    });
    if (option) {
      return String(typeof option === 'object' ? option.label : option);
    }
    return '';
  }
  return String(value);
}

export function convertLabelToValue(
  label: string,
  column: InputColumnDescription,
) {
  if (isEmptyValue(label)) return label;

  if (column.type === 'select') {
    const option = column.values.find((v) => {
      if (typeof v === 'object') {
        return String(v.label) === String(label);
      } else {
        return String(v) === String(label);
      }
    });
    if (option) {
      return typeof option === 'object' ? option.value : option;
    }
    return undefined;
  }
  return label;
}

export function getSpreadSheetTableData(
  items: SpreadSheetItem[],
  columns: InputColumnDescription[],
  maxRows: number,
): any[][] {
  return _.range(maxRows).map((row) => {
    const item = items[row];
    if (!item) return [];
    return columns.map((c) => {
      return convertValueToLabel(_.get(item, c.name), c);
    });
  });
}

export function parseSpreadSheetTableData(
  data: any[][],
  columns: InputColumnDescription[],
  removeEmptyRows: boolean,
): SpreadSheetItem[] {
  const items = data
    .map((row) => {
      if (row.length === 0) return null;
      if (row.every(isEmptyValue)) return null;

      const parsed: SpreadSheetItem = {};
      columns.forEach((c, i) => {
        const value = convertLabelToValue(row[i], c);
        if (isEmptyValue(value)) return;
        _.set(parsed, c.name, value);
      });
      return parsed;
    })
    .filter((r) => {
      if (!removeEmptyRows) return true;
      // if no field has been set, ignore the row
      return r !== null && Object.keys(r).length > 0;
    })
    .map((r) => {
      if (r !== null && typeof r === 'object') return r;
      return {};
    });

  return items;
}

export function handleSpreadSheetFillChanges(
  changes: Handsontable.CellChange[],
  columns: InputColumnDescription[],
): Handsontable.CellChange[] {
  const column = changes[0][1];
  const value = String(changes[0][3]) || '';

  const isAllSameColumn = changes
    .map(([row, column, oldValue, newValue]) => column)
    .every((val) => val === column);

  const isAllSameValue = changes
    .map(([row, column, oldValue, newValue]) => newValue)
    .every((val) => String(val) === value);

  //only increment if it is a column fill
  if (!isAllSameColumn || !isAllSameValue) return changes;

  const columnConfig = columns[Number(column)];

  if (columnConfig.type !== 'string' || !columnConfig.fillIncrement)
    return changes;

  const initialIncrementString = value.match(/(\d+)$/g)?.pop();
  const initialIncrement = Number(initialIncrementString || '0') + 1;
  const valueWithoutIncrement = initialIncrementString
    ? value.substring(0, value.length - initialIncrementString.length)
    : value;

  const newChanges = changes.map(
    ([row, column, oldValue, newValue], index): Handsontable.CellChange => {
      return [
        row,
        column,
        oldValue,
        valueWithoutIncrement + (initialIncrement + index),
      ];
    },
  );

  return newChanges;
}

export function applyChangesToSpreadSheetData(
  changes: Handsontable.CellChange[],
  data: any[][],
  columns: InputColumnDescription[],
) {
  if (changes.length === 0) return data;
  const newData = JSON.parse(JSON.stringify(data));
  changes.forEach(([row, column, oldValue, newValue]) => {
    const numberColumn = Number(column);
    if (isNaN(numberColumn)) return;
    if (isEmptyValue(newValue)) {
      newData[row][numberColumn] = undefined;
      return;
    }

    const sample_column_index = columns.findIndex(
      (column) => column.name === SAMPLE_TYPE_COLUMN_NAME,
    );

    // If Item.type has been modified then we clean up the "Short name" column
    if (column === sample_column_index) {
      const sample_sheet_column_index = columns.findIndex(
        (column) => column.name === SAMPLESHEET_COLUMN_NAME,
      );
      newData[row][sample_sheet_column_index] = undefined;
    }
    // if column is of type select make sure the value is one of the options
    const columnConfig = columns[numberColumn];
    if (columnConfig.type === 'select') {
      if (
        !_(columnConfig.values)
          .map((v) => (typeof v === 'object' ? v.label : v))
          .map((v) => String(v))
          .includes(String(newValue))
      ) {
        return;
      }
    }
    newData[row][numberColumn] = newValue;
  });
  return newData;
}

export function generateSpreadSheetCellProperties(
  row: number,
  col: number,
  data: any[][],
  errors: SpreadSheetError[],
  columns: InputColumnDescription[],
  separationIndexes: number[],
): Handsontable.CellMeta {
  const item = data[row];
  if (!item) return {};
  const column = columns[col];
  const value = item[col];
  const isEmptyRow = item.length === 0 || item.every(isEmptyValue);
  const isEmpty = isEmptyValue(value);

  const cellErrors = errors.filter((e) => e.row === row && e.column === col);
  const isError = cellErrors.length > 0;
  const commentErrors = cellErrors.length
    ? cellErrors.map((e) => '- ' + e.message).join('\n')
    : undefined;
  const comment = isError
    ? { value: commentErrors, readOnly: true }
    : undefined;

  const isLeftBorder = separationIndexes.includes(col);
  const isRightBorder = separationIndexes.includes(col + 1);

  const classes: any = {
    htEmptyRow: isEmptyRow,
    error: isError,
    borderLeft: isLeftBorder,
    borderRight: isRightBorder,
  };
  if (!isEmpty && column.colored) {
    classes[getClassForValue(col, value)] = true;
  }
  const res = {
    className: classNames(classes),
    comment,
  };

  return res;
}

const lastColor: { [column: number]: number } = {};
const maxColor = 8;
const valueColors: { [key: string]: number } = {};

const getNextColorForColumn = (column: number) => {
  let next = 1;
  if (column in lastColor) {
    const last = lastColor[column];
    if (last < maxColor) {
      next = last + 1;
    }
  }
  lastColor[column] = next;
  return next;
};

const getClassForValue = (
  column: number,
  value: string | number | undefined,
) => {
  if (value === undefined || String(value).trim().length === 0) {
    return '';
  }
  const key = `column=${column}+value=${value}`;
  if (key in valueColors) {
    return `color${valueColors[key]}`;
  } else {
    const newValueColor = getNextColorForColumn(column);
    valueColors[key] = newValueColor;
    return `color${newValueColor}`;
  }
};
