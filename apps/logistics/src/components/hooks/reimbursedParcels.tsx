import {
  PARCEL_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export function useReimbursedParcels(props: {
  investigationId: string;
  shipmentId?: string;
}) {
  const parcels = useGetEndpoint({
    endpoint: PARCEL_LIST_ENDPOINT,
    params: {
      investigationId: props.investigationId,
      shipmentId: props.shipmentId,
    },
    skipFetch: !props.shipmentId,
  });

  return useMemo(() => {
    return parcels?.filter((parcel) => parcel.isReimbursed).length || 0;
  }, [parcels]);
}
