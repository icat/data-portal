import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './App.css';
import { faScrewdriverWrench } from '@fortawesome/free-solid-svg-icons';

function App() {
  return (
    <div className="app">
      <FontAwesomeIcon size={'10x'} icon={faScrewdriverWrench} />
      <h1 className="message">
        The Data Portal is under maintenance.
        <br />
        Please come back later.
      </h1>
    </div>
  );
}

export default App;
