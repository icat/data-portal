import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import federation from '@originjs/vite-plugin-federation';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      react(),
      tsconfigPaths(),
      federation({
        name: 'mx',
        filename: 'remoteEntry.js',
        exposes: {
          './MXInvestigationViewer': './src/viewers/MXInvestigationViewer',
          './MXDatasetSnapshotViewer': './src/viewers/MXDatasetSnapshotViewer',
          './MXDatasetDetailsViewer': './src/viewers/MXDatasetDetailsViewer',
          './MXSampleViewer': './src/viewers/MXSampleViewer',
          './MXDozorExcludeRange': './src/viewers/MXDozorExcludeRange',
        },
        shared: [
          'react',
          'react-dom',
          '@tanstack/react-query',
          'react-router-dom',
          '@edata-portal/core',
          '@edata-portal/h5',
        ],
      }),
    ],
    server: {
      port: 3003,
      host: true,
    },
    build: {
      modulePreload: false,
      target: 'esnext',
      minify: mode === 'production',
      cssCodeSplit: false,
    },
  };
});
