import {
  first,
  flattenOutputs,
  getDatasetParamValue,
  getScanType,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MR_REFINEMENT_STEP,
  MXMR_REFINEMENT_R_FREE,
  MXSAD_CC_PARTIAL_MODEL,
  SAD_MODEL_BUILDING_STEP,
  type PHASING_TYPES,
} from 'constants/phasing';
import { getPhasingStepType } from 'helpers/phasing';

export type PhasingType = (typeof PHASING_TYPES)[number];

export type Phasing = {
  root: Dataset;
  datasets: Dataset[];
  type: PhasingType;
  results: Dataset[];
  bestResult: Dataset | undefined;
};

function getPhasingDatasets(datasets: Dataset[] | Dataset, type: PhasingType) {
  return flattenOutputs(datasets).filter((dataset) => {
    const scanType = getScanType(dataset);
    if (!scanType) return false;
    return scanType === type;
  });
}

function getPhasingRoots(datasets: Dataset[] | Dataset, type: PhasingType) {
  const allSteps = getPhasingDatasets(datasets, type);

  const roots = allSteps.filter((dataset) => {
    return allSteps.every((other) => {
      return (
        !other.outputDatasets ||
        other.outputDatasets.findIndex((output) => output.id === dataset.id) ===
          -1
      );
    });
  });

  return roots;
}

export const PHASING_RESULT_SAD = SAD_MODEL_BUILDING_STEP;
export const PHASING_RESULT_MR = MR_REFINEMENT_STEP;

function getPhasingResults(datasets: Dataset[] | Dataset, type: PhasingType) {
  return flattenOutputs(datasets).filter((dataset) => {
    const scanType = getScanType(dataset);
    if (!scanType) return false;
    const stepType = getPhasingStepType(dataset);
    if (!stepType) return false;
    return (
      scanType === type &&
      [PHASING_RESULT_SAD, PHASING_RESULT_MR].includes(stepType)
    );
  });
}

export function getPhasings(
  datasets: Dataset[] | Dataset,
  type: PhasingType,
): Phasing[] {
  const roots = getPhasingRoots(datasets, type);

  return roots.map((root) => {
    const datasets = getPhasingDatasets(root, type);
    const results = getPhasingResults(root, type);
    const rankedResults = results.sort(comparePhasingResults);
    const bestResult = first(rankedResults);
    return { root, datasets, type, results: rankedResults, bestResult };
  });
}

export function getBestPhasing(
  datasets: Dataset[] | Dataset,
  type: PhasingType,
): Phasing | undefined {
  const ranked = getRankedPhasings(datasets, type).filter(
    (phasing) => phasing.bestResult !== undefined,
  );
  return first(ranked);
}

export function getRankedPhasings(
  datasets: Dataset[] | Dataset,
  type: PhasingType,
): Phasing[] {
  return getPhasings(datasets, type).sort((a, b) => {
    if (!a.bestResult && !b.bestResult) return 0;
    if (!a.bestResult) return 1;
    if (!b.bestResult) return -1;
    return comparePhasingResults(a.bestResult, b.bestResult);
  });
}

export const PHASING_RESULT_RANKING_PARAMS: Record<
  string,
  {
    param: string;
    displayName: string;
    order: number;
  }
> = {
  [PHASING_RESULT_SAD]: {
    param: MXSAD_CC_PARTIAL_MODEL,
    displayName: 'CC of partial model',
    order: -1,
  },
  [PHASING_RESULT_MR]: {
    param: MXMR_REFINEMENT_R_FREE,
    displayName: 'Rfree',
    order: 1,
  },
};

/**
 * Compares two phasing results of the same type.
 *
 * Comparison is based on the value on the parameter defined in PHASING_RESULT_RANKING_PARAMS
 */
function comparePhasingResults(a: Dataset, b: Dataset): number {
  const typeA = getPhasingStepType(a);
  const typeB = getPhasingStepType(b);
  const checkType = compareValuesUndefined(typeA, typeB);
  if (checkType !== undefined) return checkType;

  const paramA = PHASING_RESULT_RANKING_PARAMS[typeA!];
  const paramB = PHASING_RESULT_RANKING_PARAMS[typeB!];
  const checkParam = compareValuesUndefined(paramA, paramB);
  if (checkParam !== undefined) return checkParam;
  if (paramA !== paramB) return 0;

  const valueA = getDatasetParamValue(a, paramA.param);
  const valueB = getDatasetParamValue(b, paramB.param);
  const checkValue = compareValuesUndefined(valueA, valueB);
  if (checkValue !== undefined) return checkValue;

  const numA = parseFloat(valueA!);
  const numB = parseFloat(valueB!);
  if (numA === numB) return 0;
  if (Number.isNaN(numA)) return 1;
  if (Number.isNaN(numB)) return -1;

  return (numA - numB) * paramA.order;
}

function compareValuesUndefined(a: any, b: any): number | undefined {
  if (a === undefined && b === undefined) return 0;
  if (a === undefined) return 1;
  if (b === undefined) return -1;
  return undefined;
}
