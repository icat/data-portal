import { getDatasetParamValue } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  PROCESSED_TYPE_AUTOPROCESSING,
  MX_AUTOPROCESSING_SPACE_GROUP,
} from 'constants/metadatakeys';
import { MX_SHELLS } from 'constants/shell';
import {
  MXAutoProcessingStat,
  MX_AUTOPROCESSING_STATS,
  MX_AUTOPROCESSING_STAT_RANKING_ORDER,
} from 'constants/stats';
import { getUnmatchedCutoffs } from 'helpers/cutoff';
import {
  getMXAutoProcessingStatParam,
  getProcessedOfType,
  isMergedAutoProcessing,
} from 'helpers/processed';
import { getSpaceGroup } from 'helpers/spacegroup';
import { MXSettings, useMXSettings } from 'hooks/mxSettings';
import { useCallback, useMemo } from 'react';

export function useBestAutoProcessing(
  datasets: Dataset[],
  merged: 'only' | 'exclude' | 'include',
  filterValid = true,
): Dataset | undefined {
  const autoProcessings = useRankedAutoProcessing(
    datasets,
    merged,
    filterValid,
  );
  return autoProcessings.length ? autoProcessings[0] : undefined;
}

export function useBestCollection(
  datasets: Dataset[],
  merged: 'only' | 'exclude' | 'include',
  filterValid = true,
): Dataset | undefined {
  const bestAutoProcessing = useBestAutoProcessing(
    datasets,
    merged,
    filterValid,
  );
  if (!bestAutoProcessing) return undefined;
  //now we need to find the collection that has the best auto processing as output
  const collection = datasets?.find((dataset) => {
    return dataset.outputDatasets?.some(
      (output) => output.id === bestAutoProcessing.id,
    );
  });
  return collection;
}

export function useRankedAutoProcessing(
  datasets: Dataset[],
  merged: 'only' | 'exclude' | 'include',
  filterValid = false,
) {
  const compareAutoProcessing = useCompareAutoProcessing();

  const res = useMemo(() => {
    return getAutoProcessing(datasets, merged, filterValid).sort(
      compareAutoProcessing,
    );
  }, [compareAutoProcessing, datasets, filterValid, merged]);
  return res;
}

/**
 * Retrieves a list of auto-processed datasets based on filtering criteria.
 *
 * @param {Dataset[] | Dataset} datasets - A dataset or an array of datasets to process.
 * @param {'only' | 'exclude' | 'include'} merged - Controls handling of merged auto-processing datasets:
 *    - `'only'` → Return only merged auto-processing datasets.
 *    - `'exclude'` → Exclude merged auto-processing datasets.
 *    - `'include'` → Include both merged and non-merged datasets.
 * @param {boolean} [filterValid=false] - If `true`, filters out invalid auto-processing datasets.
 * @returns {Dataset[]} - An array of unique auto-processed datasets matching the criteria.
 */
export function getAutoProcessing(
  datasets: Dataset[] | Dataset,
  merged: 'only' | 'exclude' | 'include',
  filterValid = false,
): Dataset[] {
  // If the input is a single dataset, convert it into an array and call the function recursively
  if (!Array.isArray(datasets))
    return getAutoProcessing([datasets], merged, filterValid);

  // If no datasets are provided, return an empty array
  if (!datasets?.length) return [];

  // Retrieve all auto-processed datasets from the given datasets
  const autoProcessings = getProcessedOfType(
    datasets,
    PROCESSED_TYPE_AUTOPROCESSING,
    false,
  );

  // Filter valid auto-processing datasets if the filterValid flag is set to true
  const filtered = filterValid
    ? autoProcessings.filter(autoProcessingIsValid)
    : autoProcessings;

  // Apply filtering based on the "merged" parameter
  return filtered
    .filter((v) => {
      if (merged === 'only') return isMergedAutoProcessing(v); // Keep only merged auto-processing datasets
      if (merged === 'exclude') return !isMergedAutoProcessing(v); // Exclude merged auto-processing datasets
      return true; // Include all if merged === 'include'
    })
    .filter((v, i, a) => a.findIndex((t) => t.id === v.id) === i); // Ensure uniqueness by filtering duplicates
}

/**
 * Determines whether an auto-processing dataset is valid.
 *
 * A dataset is considered valid if it has at least:
 * - A recognized space group.
 * - Or at least one valid statistical parameter from `MX_AUTOPROCESSING_STATS` across `MX_SHELLS`.
 *
 * @param {Dataset} dataset - The dataset to validate.
 * @returns {boolean} - `true` if the dataset is valid, otherwise `false`.
 */
export function autoProcessingIsValid(dataset: Dataset): boolean {
  // Retrieve the space group value from the dataset
  const spaceGroup = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_SPACE_GROUP,
  );

  // Convert the space group into an object representation
  const spaceGroupObj = getSpaceGroup(spaceGroup);

  // Check if the dataset has a valid space group OR contains at least one valid statistical parameter
  return (
    spaceGroupObj !== undefined || // Valid if space group exists
    MX_AUTOPROCESSING_STATS.some((stat) => {
      return MX_SHELLS.some((shell) => {
        // Construct the key for the auto-processing statistic
        const key = getMXAutoProcessingStatParam(shell, stat);

        // Retrieve the value of the statistic from the dataset
        const value = getDatasetParamValue(dataset, key);

        // Return true if any valid statistic exists
        return !!value;
      });
    })
  );
}

export function useCompareAutoProcessing(): (a: Dataset, b: Dataset) => number {
  // Retrieve the function for comparing ranking values
  const compareRankingValues = useCompareRankingValues();
  // Get cutoff settings and the flag for disabling space group ranking
  const [{ cutoffs, autoProcessingRankingDisableSpaceGroup }] = useMXSettings();

  return useCallback(
    (a: Dataset, b: Dataset) => {
      /**
       * Comparison order:
       * 1. Datasets that meet the cutoffs come first.
       * 2. Merged datasets are prioritized over unmerged ones.
       * 3. Datasets with higher space group symmetry rank higher.
       * 4. Finally, datasets are ranked by their auto-processing values.
       *
       * Each comparison function returns:
       *  - `-1` if `a` should be placed before `b`
       *  - `1` if `b` should be placed before `a`
       *  - `0` if they are equal, so the next comparison applies
       */

      return (
        compareCutOffs(a, b, cutoffs) || // Prioritize datasets that meet cutoffs
        compareMerged(a, b) || // Prefer merged datasets
        compareSpaceGroup(a, b, autoProcessingRankingDisableSpaceGroup) || // Sort by space group symmetry
        compareRankingValues(a, b) // Fallback: Sort by ranking value
      );
    },
    [compareRankingValues, cutoffs, autoProcessingRankingDisableSpaceGroup], // Dependencies for memoization
  );
}

export function compareMerged(a: Dataset, b: Dataset): number {
  const mergedA = isMergedAutoProcessing(a);
  const mergedB = isMergedAutoProcessing(b);
  if (mergedA !== mergedB) {
    return mergedA ? -1 : 1;
  }
  return 0;
}

export function compareCutOffs(
  a: Dataset,
  b: Dataset,
  cutoffs: MXSettings['cutoffs'],
): number {
  const cutoffsA = getUnmatchedCutoffs(a, cutoffs);
  const cutoffsB = getUnmatchedCutoffs(b, cutoffs);
  return cutoffsA.length - cutoffsB.length;
}

export function compareSpaceGroup(
  a: Dataset,
  b: Dataset,
  autoProcessingRankingDisableSpaceGroup?: boolean,
): number {
  if (autoProcessingRankingDisableSpaceGroup) return 0;
  const spaceGroupA =
    getSpaceGroup(getDatasetParamValue(a, MX_AUTOPROCESSING_SPACE_GROUP))
      ?.symopsExcludingCentering || 0;
  const spaceGroupB =
    getSpaceGroup(getDatasetParamValue(b, MX_AUTOPROCESSING_SPACE_GROUP))
      ?.symopsExcludingCentering || 0;
  if (spaceGroupA !== spaceGroupB) {
    return spaceGroupA > spaceGroupB ? -1 : 1;
  }
  return 0;
}

export function useCompareRankingValues(): (a: Dataset, b: Dataset) => number {
  const [settings] = useMXSettings();

  const key = useMemo(
    () =>
      getMXAutoProcessingStatParam(
        settings.autoProcessingRankingShell,
        settings.autoProcessingRankingParameter,
      ),
    [
      settings.autoProcessingRankingParameter,
      settings.autoProcessingRankingShell,
    ],
  );

  return useCallback(
    (a: Dataset, b: Dataset) => {
      const valueA = getDatasetParamValue(a, key);
      const valueB = getDatasetParamValue(b, key);
      return compareRankingValues(
        valueA,
        valueB,
        settings.autoProcessingRankingParameter,
      );
    },
    [key, settings.autoProcessingRankingParameter],
  );
}

export function useGetBestWorstValue(): (
  values: (string | number | undefined)[],
  target: 'best' | 'worst',
) => number | undefined {
  const [settings] = useMXSettings();

  return useCallback(
    (values, target) => {
      const sorted = values
        .filter((v) => v !== undefined)
        .sort((a, b) =>
          compareRankingValues(a, b, settings.autoProcessingRankingParameter),
        );
      const toNumber = Number(
        sorted[target === 'best' ? 0 : sorted.length - 1],
      );
      return isNaN(toNumber) ? undefined : toNumber;
    },
    [settings.autoProcessingRankingParameter],
  );
}

export function compareRankingValues(
  a: number | string | undefined,
  b: number | string | undefined,
  rankParam: MXAutoProcessingStat,
): number {
  const va = Number(a);
  const vb = Number(b);
  if (va === vb) return 0;
  if (isNaN(va)) return 1;
  if (isNaN(vb)) return -1;
  return MX_AUTOPROCESSING_STAT_RANKING_ORDER[rankParam] * (va - vb);
}
