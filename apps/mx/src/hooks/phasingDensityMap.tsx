import { Config, first, useConfig } from '@edata-portal/core';
import {
  DATA_DOWNLOAD_ENDPOINT,
  Datafile,
  DatafileDownloadParams,
  DatafileResponse,
  Dataset,
  DATASET_FILE_LIST_ENDPOINT,
  useAsyncFetchEndpoint,
  useEndpointURL,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { getMTZFileName, getPDBFileName } from 'helpers/phasing';
import { useCallback, useMemo } from 'react';

export type PhasingDensityMapFiles = {
  dataset: Dataset;
  pdbFile: Datafile | undefined;
  mtzFile: Datafile | undefined;
  viewUrl: string | undefined;
};

export function usePhasingDensityMapFiles(
  dataset: Dataset | undefined,
): PhasingDensityMapFiles | undefined {
  const pdb_name = dataset ? getPDBFileName(dataset) : undefined;
  const mtz_name = dataset ? getMTZFileName(dataset) : undefined;
  const config = useConfig();

  const pdb_files = useGetEndpoint({
    endpoint: DATASET_FILE_LIST_ENDPOINT,
    params: {
      datasetId: dataset?.id.toString() || '',
      search: pdb_name,
    },
    skipFetch: !pdb_name,
    default: [] as DatafileResponse[],
  });

  const mtz_files = useGetEndpoint({
    endpoint: DATASET_FILE_LIST_ENDPOINT,
    params: {
      datasetId: dataset?.id.toString() || '',
      search: mtz_name,
    },
    skipFetch: !mtz_name,
    default: [] as DatafileResponse[],
  });

  const getFileUrl = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  return useMemo(() => {
    if (!dataset) return undefined;
    const pdbFile = first(pdb_files)?.Datafile;
    const mtzFile = first(mtz_files)?.Datafile;
    if (!pdbFile)
      return {
        dataset,
        pdbFile: undefined,
        mtzFile: undefined,
        viewUrl: undefined,
      };
    const url = buildViewerUrl(pdbFile, mtzFile, config, getFileUrl);

    return {
      dataset,
      pdbFile,
      mtzFile,
      viewUrl: url,
    };
  }, [config, dataset, getFileUrl, mtz_files, pdb_files]);
}

export type PhasingDensityMapViewer = {
  dataset: Dataset;
  openView: (() => void) | undefined;
};

export function usePhasingDensityMapViewer(
  dataset: Dataset | undefined,
): PhasingDensityMapViewer | undefined {
  const pdb_name = dataset ? getPDBFileName(dataset) : undefined;
  const mtz_name = dataset ? getMTZFileName(dataset) : undefined;
  const config = useConfig();

  const get_files_endpoint = useAsyncFetchEndpoint(DATASET_FILE_LIST_ENDPOINT);
  const getFileUrl = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  const openView = useCallback(() => {
    get_files_endpoint({
      datasetId: dataset?.id.toString() || '',
      search: pdb_name,
    }).then((pdb_files) => {
      get_files_endpoint({
        datasetId: dataset?.id.toString() || '',
        search: mtz_name,
      }).then((mtz_files) => {
        const pdbFile = first(pdb_files || [])?.Datafile;
        const mtzFile = first(mtz_files || [])?.Datafile;
        if (!pdbFile) return;
        const url = buildViewerUrl(pdbFile, mtzFile, config, getFileUrl);
        window.open(url.toString(), '_blank');
      });
    });
  }, [config, dataset?.id, getFileUrl, get_files_endpoint, mtz_name, pdb_name]);

  if (!dataset) return undefined;
  if (!pdb_name) return { dataset, openView: undefined };

  return {
    dataset,
    openView,
  };
}

function buildViewerUrl(
  pdbFile: Datafile,
  mtzFile: Datafile | undefined,
  config: Config,
  getFileUrl: (params?: DatafileDownloadParams | undefined) => string,
) {
  const url = new URL(config.ui.mx.pdb_map_mtz_viewer_url);
  url.searchParams.set('pdb', getFileUrl({ datafileIds: String(pdbFile.id) }));
  if (mtzFile)
    url.searchParams.set(
      'mtz',
      getFileUrl({ datafileIds: String(mtzFile.id) }),
    );
  return url.toString();
}
