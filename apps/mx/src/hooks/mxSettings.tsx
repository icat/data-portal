import { useUserPreferences } from '@edata-portal/core';
import type { MXSettings as APIMXSettings } from '@edata-portal/icat-plus-api';
import {
  SCAN_TYPE_PARAM_AUTOMESH,
  SCAN_TYPE_PARAM_CHARACTERISATION,
  SCAN_TYPE_PARAM_COLLECTION,
  SCAN_TYPE_PARAM_LINE,
  SCAN_TYPE_PARAM_MESH,
} from 'constants/metadatakeys';
import { MXShell, MX_OVERALL_SHELL, MX_SHELLS } from 'constants/shell';
import {
  MXAutoProcessingStat,
  MX_AUTOPROCESSING_STATS,
  MX_AUTOPROCESSING_STAT_ISIGMA,
} from 'constants/stats';
import { useCallback, useMemo } from 'react';

const FILTERS_FOR_PREPARATION = ['Indicator', 'Sample', 'Diffraction'] as const;

export const MX_VIEW_FILTERS = {
  [SCAN_TYPE_PARAM_COLLECTION]: [
    ...FILTERS_FOR_PREPARATION,
    'Auto processing',
    'Phasing',
  ],
  [SCAN_TYPE_PARAM_AUTOMESH]: FILTERS_FOR_PREPARATION,
  [SCAN_TYPE_PARAM_MESH]: FILTERS_FOR_PREPARATION,
  [SCAN_TYPE_PARAM_LINE]: FILTERS_FOR_PREPARATION,
  [SCAN_TYPE_PARAM_CHARACTERISATION]: FILTERS_FOR_PREPARATION,
} as const;

export const DEFAULT_MX_SETTINGS: MXSettings = {
  autoProcessingRankingShell: MX_OVERALL_SHELL,
  autoProcessingRankingParameter: MX_AUTOPROCESSING_STAT_ISIGMA,
  autoProcessingRankingDisableSpaceGroup: false,
  scanTypeFilters: Object.keys(MX_VIEW_FILTERS).flatMap((key) =>
    MX_VIEW_FILTERS[key as keyof typeof MX_VIEW_FILTERS].map((filter) =>
      getMXViewFilterKey(key as keyof typeof MX_VIEW_FILTERS, filter),
    ),
  ),
};
export interface MXSettings extends APIMXSettings {
  autoProcessingRankingShell: MXShell;
  autoProcessingRankingParameter: MXAutoProcessingStat;
}

export type MXSettingsUpdate = (newValue: Partial<MXSettings>) => void;

export function useMXSettings(): [MXSettings, MXSettingsUpdate] {
  const [value, update] = useUserPreferences('mxsettings', DEFAULT_MX_SETTINGS);

  const settings: MXSettings = useMemo(() => {
    return {
      autoProcessingRankingShell: MX_SHELLS.includes(
        value.autoProcessingRankingShell as any,
      )
        ? (value.autoProcessingRankingShell as MXShell)
        : DEFAULT_MX_SETTINGS.autoProcessingRankingShell,
      autoProcessingRankingParameter: MX_AUTOPROCESSING_STATS.includes(
        value.autoProcessingRankingParameter as any,
      )
        ? (value.autoProcessingRankingParameter as MXAutoProcessingStat)
        : DEFAULT_MX_SETTINGS.autoProcessingRankingParameter,
      autoProcessingRankingDisableSpaceGroup:
        value.autoProcessingRankingDisableSpaceGroup,
      scanTypeFilters: value.scanTypeFilters?.length
        ? value.scanTypeFilters
        : DEFAULT_MX_SETTINGS.scanTypeFilters,
      cutoffs: value.cutoffs,
    };
  }, [value]);

  const updateSettings = useCallback(
    (newValue: Partial<MXSettings>) => {
      update({
        ...value,
        ...newValue,
      });
    },
    [update, value],
  );

  return [settings, updateSettings] as const;
}

export function hasMXViewFilter(
  category: keyof typeof MX_VIEW_FILTERS | string | undefined,
  filter: (typeof MX_VIEW_FILTERS)[keyof typeof MX_VIEW_FILTERS][number],
  settings: MXSettings,
) {
  if (!category) return false;

  if (!(category in MX_VIEW_FILTERS)) return true;

  const key = getMXViewFilterKey(category, filter);

  return settings.scanTypeFilters && settings.scanTypeFilters.includes(key);
}

export function setMXViewFilters(
  category: keyof typeof MX_VIEW_FILTERS | string,
  filters: (typeof MX_VIEW_FILTERS)[keyof typeof MX_VIEW_FILTERS][number][],
  value: boolean,
  settings: MXSettings,
  settingsUpdater: MXSettingsUpdate,
) {
  const keys = filters.map((filter) => getMXViewFilterKey(category, filter));

  const newFilters = value
    ? [
        ...settings.scanTypeFilters,
        ...keys.filter((key) => !settings.scanTypeFilters.includes(key)),
      ]
    : settings.scanTypeFilters.filter((key) => !keys.includes(key));

  settingsUpdater({
    ...settings,
    scanTypeFilters: newFilters,
  });
}

function getMXViewFilterKey(
  category: keyof typeof MX_VIEW_FILTERS | string,
  filter: (typeof MX_VIEW_FILTERS)[keyof typeof MX_VIEW_FILTERS][number],
) {
  return `${category}-${filter}`;
}

export function isDefaultMXViewFilters(settings: MXSettings) {
  return (
    settings.scanTypeFilters.length ===
      DEFAULT_MX_SETTINGS.scanTypeFilters.length &&
    DEFAULT_MX_SETTINGS.scanTypeFilters.every((key) =>
      settings.scanTypeFilters.includes(key),
    )
  );
}
