import {
  getDatasetParamValue,
  toFixedUpperLower,
  useLocalStorageValue,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MX_AUTOPROCESSING_STAT_THRESHOLDS } from 'constants/stats';
import { getMXAutoProcessingStatParam } from 'helpers/processed';
import { useGetBestWorstValue } from 'hooks/autoProcessingRanking';
import { useMXSettings } from 'hooks/mxSettings';
import { useCallback, useMemo } from 'react';

export type MXAutoProcessingStatColorScaleValue = {
  good: number | undefined;
  bad: number | undefined;
};

export type MXAutoProcessingStatColorScale = {
  current: MXAutoProcessingStatColorScaleValue;
  default: MXAutoProcessingStatColorScaleValue;
  setValue: (value: MXAutoProcessingStatColorScaleValue | undefined) => void;
};

export function useMXAutoProcessingStatColorScale(
  datasets: Dataset[],
): MXAutoProcessingStatColorScale {
  const [settings] = useMXSettings();
  const statKey = getMXAutoProcessingStatParam(
    settings.autoProcessingRankingShell,
    settings.autoProcessingRankingParameter,
  );

  const getBestWorstValue = useGetBestWorstValue();

  const statValues = useMemo(() => {
    return (datasets || [])
      .map((dataset) => {
        return getDatasetParamValue(dataset, statKey) as string;
      })
      .filter((value) => value !== undefined && value !== null && value.length)
      .map((value) => {
        return parseFloat(value);
      })
      .filter((value) => !isNaN(value));
  }, [datasets, statKey]);

  const initialValue: MXAutoProcessingStatColorScaleValue = useMemo(() => {
    const stat =
      MX_AUTOPROCESSING_STAT_THRESHOLDS[
        settings.autoProcessingRankingParameter
      ];
    const defaultGood =
      'good' in stat
        ? stat.good
        : stat[settings.autoProcessingRankingShell].good;
    const defaultBad =
      'bad' in stat ? stat.bad : stat[settings.autoProcessingRankingShell].bad;

    const bestValue = getBestWorstValue(statValues, 'best');
    const worstValue = getBestWorstValue(statValues, 'worst');
    const initialGood = getBestWorstValue([defaultGood, bestValue], 'worst');
    const initialBad = getBestWorstValue([defaultBad, worstValue], 'best');

    return formatInitialValue({
      good: initialGood,
      bad: initialBad,
    });
  }, [
    getBestWorstValue,
    settings.autoProcessingRankingParameter,
    settings.autoProcessingRankingShell,
    statValues,
  ]);

  const [currentValue, setCurrentValue] = useCurrentValueForStat();

  const safeCurrentValue: MXAutoProcessingStatColorScaleValue = useMemo(() => {
    const res = {
      good: getBestWorstValue(
        [currentValue?.good, initialValue?.good],
        'worst',
      ),
      bad: getBestWorstValue([currentValue?.bad, initialValue?.bad], 'best'),
    };
    return res;
  }, [currentValue, getBestWorstValue, initialValue]);

  return useMemo(
    () => ({
      current: safeCurrentValue ? safeCurrentValue : initialValue,
      default: initialValue,
      setValue: setCurrentValue,
    }),
    [safeCurrentValue, initialValue, setCurrentValue],
  );
}

function formatInitialValue(value: MXAutoProcessingStatColorScaleValue) {
  if (value.good === undefined || value.bad === undefined) return value;

  const fixedGood = toFixedUpperLower(
    value.good,
    2,
    value.good > value.bad ? 'upper' : 'lower',
  );
  const fixedBad = toFixedUpperLower(
    value.bad,
    2,
    value.bad > value.good ? 'upper' : 'lower',
  );
  if (
    fixedGood === undefined ||
    fixedBad === undefined ||
    fixedGood === fixedBad
  )
    return {
      good: undefined,
      bad: undefined,
    };
  return { good: fixedGood, bad: fixedBad };
}

function useCurrentValueForStat(): [
  MXAutoProcessingStatColorScaleValue | undefined,
  (value: MXAutoProcessingStatColorScaleValue | undefined) => void,
] {
  const [settings] = useMXSettings();

  const statKey = getMXAutoProcessingStatParam(
    settings.autoProcessingRankingShell,
    settings.autoProcessingRankingParameter,
  );

  const storageKey = useMemo(() => {
    return `MXAutoProcessingStatColorScaleContext-${statKey}`;
  }, [statKey]);

  const [localStorageValue, setLocalStorageValue] =
    useLocalStorageValue(storageKey);

  const currentValue = useMemo(() => {
    if (localStorageValue === undefined) return undefined;
    return JSON.parse(localStorageValue) as MXAutoProcessingStatColorScaleValue;
  }, [localStorageValue]);

  const updateCurrentValue = useCallback(
    (v: MXAutoProcessingStatColorScaleValue | undefined) => {
      setLocalStorageValue(v ? JSON.stringify(v) : undefined);
    },
    [setLocalStorageValue],
  );

  return [currentValue, updateCurrentValue];
}
