import { getInputDatasetIds, isProcessed } from '@edata-portal/core';
import {
  useGetEndpoint,
  type Dataset,
  DATASET_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { DozorPlotFromCSV } from 'components/processed/dozor/DozorPlotFromCSV';
import { getDozorPlotCsv } from 'components/processed/dozor/dozorHelper';

import { PROCESSED_TYPE_QUALITY_INDICATOR } from 'constants/metadatakeys';
import { getProcessedOfType } from 'helpers/processed';
import { useMemo } from 'react';
import { Alert, Form } from 'react-bootstrap';

export default function MXDozorExcludeRange({
  dataset,
  value,
  onChange,
}: {
  dataset: Dataset;
  value?: string;
  onChange?: (value?: string) => void;
}) {
  const datasetIds = useMemo(() => {
    if (isProcessed(dataset)) {
      return getInputDatasetIds(dataset).join(',');
    }
    return String(dataset.id);
  }, [dataset]);

  const datasetWithOutputs = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: datasetIds,
      nested: true,
    },
    default: [] as Dataset[],
    skipFetch: !dataset?.id,
  });

  const qualityIndicators = getProcessedOfType(
    datasetWithOutputs,
    PROCESSED_TYPE_QUALITY_INDICATOR,
    true,
  );

  const fallBack = (
    <>
      <Alert variant="danger">Dozor CSV could not be found.</Alert>
      <Form.Control
        type="text"
        placeholder='Enter ranges in format: "50-50, 100-200, 300-400".'
        value={value}
        onChange={(e) => onChange && onChange(e.target.value)}
      />
    </>
  );

  if (!qualityIndicators?.length) return fallBack;

  const csv = getDozorPlotCsv(qualityIndicators[0]);

  if (!csv) return fallBack;

  return (
    <DozorPlotFromCSV
      dataset={dataset}
      csv={csv}
      selectedExcludeRanges={value}
      onSelectExcludeRanges={onChange}
    />
  );
}
