import { isAcquisition } from '@edata-portal/core';

import 'blink.css';
import type { Dataset } from '@edata-portal/icat-plus-api';
import MXAcquisitionDatasetDetails from 'components/acquisition/MXAcquisitionDatasetDetails';
import MXProcessedDatasetDetails from 'components/processed/MXProcessedDatasetDetails';

export default function MXDatasetDetailsViewer({
  dataset,
}: {
  dataset: Dataset;
}) {
  return isAcquisition(dataset) ? (
    <MXAcquisitionDatasetDetails dataset={dataset} />
  ) : (
    <MXProcessedDatasetDetails dataset={dataset} />
  );
}
