import { isAcquisition } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAcquisitionDatasetSnapshot } from 'components/acquisition/MXAcquisitionDatasetSnapshot';
import MXProcessedDatasetSnapshot from 'components/processed/MXProcessedDatasetSnapshot';

export default function MXDatasetSnapshotViewer({
  dataset,
  processedActionFilter,
  showSampleSnapshot,
  showDiffractionSnapshot,
  showMainSnapshot,
}: {
  dataset: Dataset;
  processedActionFilter?: (processed: Dataset) => boolean;
  showSampleSnapshot?: boolean;
  showDiffractionSnapshot?: boolean;
  showMainSnapshot?: boolean;
}) {
  return isAcquisition(dataset) ? (
    <MXAcquisitionDatasetSnapshot
      dataset={dataset}
      processedActionFilter={processedActionFilter}
      showSampleSnapshot={showSampleSnapshot}
      showDiffractionSnapshot={showDiffractionSnapshot}
      showMainSnapshot={showMainSnapshot}
    />
  ) : (
    <MXProcessedDatasetSnapshot dataset={dataset} />
  );
}
