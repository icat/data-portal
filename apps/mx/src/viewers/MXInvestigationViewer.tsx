import {
  SampleSelector,
  SideNavElement,
  useParam,
  Loading,
  DatasetList,
  WithSideNav,
  SideNavFilter,
  FilterBadges,
  Button,
  SideNavMetadataFilter,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { MXViewFilterSettings } from 'components/settings/MXViewFilterSettings';
import { MXSettingsAutoProcessing } from 'components/settings/MXSettingsAutoprocessing';
import { MXInvestigationSummary } from 'components/summary/MXInvestigationSummary';
import { getCutOffsFilterBadgeItems } from 'helpers/cutoff';
import { useMXSettings } from 'hooks/mxSettings';
import { Suspense } from 'react';
import { Col, Row } from 'react-bootstrap';

export default function MXInvestigationViewer({
  investigation,
}: {
  investigation: Investigation;
}) {
  const [selectedSampleId, setSelectedSampleId] = useParam<string>(
    'sampleId',
    'undefined',
  );

  const sampleIdNumber = isNaN(Number(selectedSampleId))
    ? undefined
    : Number(selectedSampleId);

  const [showAsSummary, setShowAsSummary] = useParam<'true' | 'false'>(
    'showAsSummary',
    'false',
    true,
    true,
  );
  const showAsSummaryBool = showAsSummary === 'true';

  const [settings, updateSetting] = useMXSettings();

  const [parametersFilters, setParametersFilters] = useParam<string>(
    'parameters',
    '',
  );

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label="Datasets">
          <Col>
            <SideNavFilter label={'View as'}>
              <Row className="g-1">
                <Col xs={'auto'}>
                  <Button
                    variant="outline-primary"
                    active={!showAsSummaryBool}
                    onClick={() => setShowAsSummary('false')}
                    size="sm"
                    test-id="mx-list-button"
                  >
                    List
                  </Button>
                </Col>
                <Col xs={'auto'}>
                  <Button
                    variant="outline-primary"
                    active={showAsSummaryBool}
                    onClick={() => setShowAsSummary('true')}
                    size="sm"
                    test-id="mx-summary-button"
                  >
                    Summary
                  </Button>
                </Col>
              </Row>
            </SideNavFilter>

            {showAsSummaryBool ? null : (
              <>
                <SideNavFilter
                  label={'Sample'}
                  hasValue={sampleIdNumber !== undefined}
                  onClear={() => setSelectedSampleId('undefined')}
                >
                  <Suspense
                    fallback={
                      <small>
                        <i>loading samples...</i>
                      </small>
                    }
                  >
                    <SampleSelector
                      updateSelectedSampleObject={(s) =>
                        setSelectedSampleId(s?.id.toString() || 'undefined')
                      }
                      selectedSampleId={sampleIdNumber}
                      investigation={investigation}
                    />
                  </Suspense>
                </SideNavFilter>
                <SideNavMetadataFilter
                  filterDefinition={{
                    label: 'Protein',
                    type: 'select-multi',
                    parameter: 'SampleProtein_acronym',
                    doPrettyPrint: false,
                  }}
                  state={{
                    investigationId: investigation.id.toString(),
                    currentFilters: parametersFilters,
                    setCurrentFilters: setParametersFilters,
                  }}
                />
              </>
            )}
          </Col>
        </SideNavElement>
      }
    >
      <WithSideNav
        sideNav={
          <SideNavElement label="Collection steps">
            <MXViewFilterSettings />
          </SideNavElement>
        }
      >
        <WithSideNav
          sideNav={
            <SideNavElement label="Auto processing ranking">
              <MXSettingsAutoProcessing />
            </SideNavElement>
          }
        >
          {showAsSummaryBool ? (
            <Suspense fallback={<Loading />}>
              <MXInvestigationSummary investigation={investigation} />
            </Suspense>
          ) : (
            <>
              <FilterBadges
                filters={[
                  {
                    name: 'sampleId',
                    value: sampleIdNumber?.toString(),
                    onRemove: () => setSelectedSampleId(),
                    hide: !sampleIdNumber,
                  },
                  ...getCutOffsFilterBadgeItems(settings, updateSetting),
                ]}
              />
              <DatasetList
                investigationId={investigation.id.toString()}
                groupBy="sample"
                sampleIds={
                  sampleIdNumber ? [sampleIdNumber.toString()] : undefined
                }
                parameterFilter={parametersFilters}
                paginationBarLogo={{
                  src: '/images/ispyb/black_logo_ispyb.gif',
                  alt: 'ISPyB',
                  url: 'https://doi.org/10.1093/bioinformatics/btr535',
                }}
              />
            </>
          )}
        </WithSideNav>
      </WithSideNav>
    </WithSideNav>
  );
}
