import {
  sortDatasetsByDate,
  getScanType,
  getParameterValue,
  SampleWithDatasetGroups,
} from '@edata-portal/core';
import type { Dataset, Sample } from '@edata-portal/icat-plus-api';
import { MXSampleDatasetGroupSummary } from 'components/sample/MXSampleDatasetGroupSummary';
import {
  MX_POSITION_NAME_PARAM,
  SCAN_TYPE_PARAM_COLLECTION,
} from 'constants/metadatakeys';

import { getWorkflowInfo } from 'helpers/workflow';

export default function MXSampleViewer({ sample }: { sample: Sample }) {
  return (
    <SampleWithDatasetGroups
      sample={sample}
      computeGroups={computeGroupsDataset}
      renderGroup={(group) => <MXSampleDatasetGroupSummary group={group} />}
      paginationSize={10}
    />
  );
}

function computeGroupsDataset(datasets: Dataset[]) {
  const sorted = sortDatasetsByDate(datasets);

  return sorted.reduce<Dataset[][]>((acc, dataset) => {
    const lastGroup = acc[acc.length - 1];
    const lastItem = lastGroup?.[lastGroup.length - 1];

    const isFirstItem = !lastGroup || !lastItem;

    const currentWorkflow = getWorkflowInfo(dataset);
    const lastWorkflow = getWorkflowInfo(lastItem);

    const isDifferentWorkflow = currentWorkflow?.id !== lastWorkflow?.id;

    const isWithinWorkflow =
      currentWorkflow && lastWorkflow && currentWorkflow.id === lastWorkflow.id;

    const groupHasCollection = lastGroup?.some(
      (d) => getScanType(d) === SCAN_TYPE_PARAM_COLLECTION,
    );

    const currentPosition = getParameterValue(dataset, MX_POSITION_NAME_PARAM);

    const lastPosition = lastItem
      ? getParameterValue(lastItem, MX_POSITION_NAME_PARAM)
      : undefined;

    const isDifferentPosition = currentPosition !== lastPosition;

    if (
      isFirstItem ||
      isDifferentWorkflow ||
      (isDifferentPosition && !isWithinWorkflow) ||
      (groupHasCollection && !isWithinWorkflow)
    ) {
      // if the current dataset is part of a different workflow
      // or if the last group has a collection and the current dataset is not part of the same workflow
      // we start a new group
      acc.push([dataset]);
    } else {
      lastGroup.push(dataset);
    }

    return acc;
  }, []);
}
