import { getScanType } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXDatasetGrid } from 'components/datasetgrid/MXDatasetGrid';
import {
  isCollectionScanType,
  SCAN_TYPE_PARAM_COLLECTION,
} from 'constants/metadatakeys';
import { getActions } from 'helpers/dataset';
import {
  hasMXViewFilter,
  MX_VIEW_FILTERS,
  useMXSettings,
} from 'hooks/mxSettings';
import { useMemo } from 'react';

export function MXDatasetsSummary({ datasets }: { datasets: Dataset[] }) {
  const [MXSettings] = useMXSettings();

  const actions = getActions(datasets);

  const filteredActions = useMemo(() => {
    return actions.filter((action) => {
      const scanTypeDataset = getScanType(action);
      const scanType = isCollectionScanType(scanTypeDataset)
        ? SCAN_TYPE_PARAM_COLLECTION
        : scanTypeDataset;
      if (scanType === null || scanType === undefined) {
        return false;
      }
      if (!(scanType in MX_VIEW_FILTERS)) {
        return true;
      }
      const filters = MX_VIEW_FILTERS[scanType as keyof typeof MX_VIEW_FILTERS];
      return filters.some((filter) =>
        hasMXViewFilter(
          scanType as keyof typeof MX_VIEW_FILTERS,
          filter,
          MXSettings,
        ),
      );
    });
  }, [actions, MXSettings]);

  return <MXDatasetGrid datasets={filteredActions} />;
}
