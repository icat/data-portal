import { GridGraphRender, HorizontalScroll } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { getDatasetGridConfigForDatasets } from 'helpers/datasetgridgroupingconfig';
import { getMXGridGraphDefinition } from 'helpers/gridgraphdefinition';
import { useBestAutoProcessing } from 'hooks/autoProcessingRanking';
import { useMXSettings } from 'hooks/mxSettings';
import { useMemo } from 'react';

export function MXDatasetGrid({ datasets }: { datasets: Dataset[] }) {
  const bestMerged = useBestAutoProcessing(datasets, 'only');

  const [mxSettings] = useMXSettings();

  const gridGraph = useMemo(() => {
    return getMXGridGraphDefinition(
      datasets,
      getDatasetGridConfigForDatasets(datasets),
      mxSettings,
      bestMerged,
    );
  }, [datasets, bestMerged, mxSettings]);

  return (
    <HorizontalScroll>
      <GridGraphRender grid={gridGraph} />
    </HorizontalScroll>
  );
}
