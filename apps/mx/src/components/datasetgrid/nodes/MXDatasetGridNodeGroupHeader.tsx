export function MXDatasetGridNodeGroupHeader({
  children,
}: {
  children: React.ReactNode | React.ReactNode[];
}) {
  return (
    <div className="border rounded bg-dark text-white p-1 align-self-center text-nowrap">
      {children}
    </div>
  );
}
