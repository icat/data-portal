import {
  DatasetCard,
  getDatasetName,
  prettyPrintScanType,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { isAutoProcessing } from 'helpers/processed';
import { getMxDatasetTabs } from 'helpers/tabs';
import { useRef, useState } from 'react';

export default function MXProcessedDatasetDetails({
  dataset,
}: {
  dataset: Dataset;
}) {
  const isAutoProc = isAutoProcessing(dataset);

  const title = isAutoProc
    ? getDatasetName(dataset)
    : prettyPrintScanType(dataset);

  const [expanded, setExpanded] = useState(isAutoProc);

  const ref = useRef<HTMLDivElement>(null);

  const datasetView = (
    <div ref={ref}>
      <DatasetCard
        key={dataset.id}
        dataset={dataset}
        title={title}
        setExpanded={(v) => setExpanded(v)}
        expanded={expanded}
        tabs={getMxDatasetTabs(dataset, false)}
      />
    </div>
  );

  return datasetView;
}
