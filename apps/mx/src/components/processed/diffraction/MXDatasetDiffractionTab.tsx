import { DatasetImage } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { getDiffractionSnapshots } from 'helpers/snapshot';
import { Col, Container, Row } from 'react-bootstrap';

export function MXDatasetDiffractionTab({ dataset }: { dataset: Dataset }) {
  const snapshots = getDiffractionSnapshots(dataset);

  return (
    <Container fluid>
      <Row className="g-2">
        {snapshots.map((snapshot) => (
          <Col key={snapshot.id}>
            <DatasetImage
              resourceId={snapshot.id}
              name={snapshot.path}
              displayFileName={true}
            />
          </Col>
        ))}
      </Row>
    </Container>
  );
}
