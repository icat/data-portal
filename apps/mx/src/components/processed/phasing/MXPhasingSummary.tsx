import { Gallery, getDatasetName } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXPhasingFlowChart } from 'components/processed/phasing/MXPhasingFlowChart';
import { MXPhasingDensityMapDetails } from 'components/processed/phasing/densitymap/MXPhasingDensityMapDetails';
import { MXMRPhasingDetails } from 'components/processed/phasing/mr/MXMRPhasingDetails';
import { MXSADPhasingDetails } from 'components/processed/phasing/sad/MXSADPhasingDetails';
import { getPhasingStepType } from 'helpers/phasing';
import type { Phasing } from 'hooks/phasingRanking';
import { useMemo } from 'react';

export function MXPhasingSummary({
  phasing,
  dataset,
  setDataset,
}: {
  phasing: Phasing;
  dataset: Dataset;
  setDataset: (dataset: Dataset) => void;
}) {
  return (
    <div className="h-100 w-100 d-flex flex-column gap-2">
      <div className="d-flex h-50">
        <MXPhasingFlowChart
          phasing={phasing}
          onDatasetClick={(dataset: Dataset) => setDataset(dataset)}
          currentDataset={dataset}
        />
      </div>

      <div
        className="d-flex h-50 flex-row gap-2 flex-wrap overflow-auto"
        key={dataset.id.toString()}
      >
        <MXPhasingDetails dataset={dataset} phasing={phasing} />
        <MXPhasingDensityMapDetails dataset={dataset} />
      </div>
    </div>
  );
}

function MXPhasingDetails({
  dataset,
  phasing,
}: {
  dataset: Dataset;
  phasing: Phasing;
}) {
  const details = useMemo(() => {
    if (phasing.type === 'MR') return <MXMRPhasingDetails dataset={dataset} />;
    if (phasing.type === 'SAD')
      return <MXSADPhasingDetails dataset={dataset} />;
    return null;
  }, [dataset, phasing]);
  return (
    <div className="d-flex flex-column p-2 gap-2 border rounded">
      <h5>{getPhasingStepType(dataset) || getDatasetName(dataset)}</h5>
      {details}
      <Gallery inRow dataset={dataset} />
    </div>
  );
}
