import {
  ColoredValue,
  MetadataTable,
  MetadataTableParameters,
  getDatasetName,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MXSAD_AVERAGE_FRAGMENT_LENGTH,
  MXSAD_CC_PARTIAL_MODEL,
  MXSAD_ENANTIOMORPH,
  MXSAD_MAX_RESOLUTION,
  MXSAD_MIN_RESOLUTION,
  MXSAD_PSEUDO_FREE_CC,
  MXSAD_SOLVENT,
  MXSAD_SPACE_GROUP,
  SAD_CC_GRADIENT,
  SAD_FRAGMENT_GRADIENT,
  SAD_MODEL_BUILDING_STEP,
  SAD_PHASING_STEP,
  SAD_PREPARE_STEP,
} from 'constants/phasing';
import { getPhasingStepType } from 'helpers/phasing';
import { useMemo } from 'react';
import { Badge } from 'react-bootstrap';

const SAD_PHASING_PARAMS: MetadataTableParameters = [
  {
    caption: 'Enantiomorph',
    parameterName: MXSAD_ENANTIOMORPH,
  },
  {
    caption: 'Solvent',
    parameterName: MXSAD_SOLVENT,
  },
  {
    caption: 'Free CC',
    parameterName: MXSAD_PSEUDO_FREE_CC,
    formatter: (value) => (
      <ColoredValue gradient={SAD_CC_GRADIENT} value={Number(value)} />
    ),
  },
];

const SAD_MODELBUILDING_PARAMS: MetadataTableParameters = [
  {
    caption: 'Partial CC',
    parameterName: MXSAD_CC_PARTIAL_MODEL,
    formatter: (value) => (
      <ColoredValue gradient={SAD_CC_GRADIENT} value={Number(value)} />
    ),
  },
  {
    caption: 'Avg fragment',
    parameterName: MXSAD_AVERAGE_FRAGMENT_LENGTH,
    digits: 2,
    formatter: (value) => (
      <ColoredValue gradient={SAD_FRAGMENT_GRADIENT} value={Number(value)} />
    ),
  },
];

const SAD_PREPARE_PARAMS: MetadataTableParameters = [
  {
    caption: 'Res. min',
    parameterName: MXSAD_MIN_RESOLUTION,
  },
  {
    caption: 'Res. max',
    parameterName: MXSAD_MAX_RESOLUTION,
  },

  {
    caption: 'Space group',
    parameterName: MXSAD_SPACE_GROUP,
  },
];

export function MXSADPhasingNodeContent({
  dataset,
  isBest,
  hideName,
}: {
  dataset: Dataset;
  isBest?: boolean;
  hideName?: boolean;
}) {
  const params = useMemo(() => {
    const type = getPhasingStepType(dataset);

    if (type === SAD_PHASING_STEP) return SAD_PHASING_PARAMS;
    if (type === SAD_PREPARE_STEP) return SAD_PREPARE_PARAMS;
    if (type === SAD_MODEL_BUILDING_STEP) return SAD_MODELBUILDING_PARAMS;
    return [];
  }, [dataset]);

  return (
    <div
      className={`d-flex flex-column gap-1 justify-content-center align-items-center h-100 w-100`}
    >
      {hideName ? null : (
        <strong>
          {getPhasingStepType(dataset) || getDatasetName(dataset)}
        </strong>
      )}
      {isBest && <Badge bg="info">Best</Badge>}
      <MetadataTable
        parameters={params}
        entity={dataset}
        valuePlaceholder="N/A"
        size="sm"
      />
    </div>
  );
}
