import { MXSADPhasingNodeContent } from 'components/processed/phasing/sad/MXSADPhasingNodeContent';
import type { Phasing } from 'hooks/phasingRanking';

export function MXSADPhasingSnapshot({ phasing }: { phasing: Phasing }) {
  if (!phasing.bestResult) return null;
  return (
    <div
      style={{
        fontSize: '0.9rem',
      }}
    >
      <MXSADPhasingNodeContent dataset={phasing.bestResult} />
    </div>
  );
}
