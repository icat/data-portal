import {
  ColoredValue,
  MetadataCategories,
  type MetadataCategory,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MXSAD_AVERAGE_FRAGMENT_LENGTH,
  MXSAD_CC_PARTIAL_MODEL,
  MXSAD_CHAIN_COUNT,
  MXSAD_ENANTIOMORPH,
  MXSAD_MAX_RESOLUTION,
  MXSAD_MIN_RESOLUTION,
  MXSAD_PSEUDO_FREE_CC,
  MXSAD_RESIDUES_COUNT,
  MXSAD_SOLVENT,
  MXSAD_SPACE_GROUP,
  SAD_CC_GRADIENT,
  SAD_FRAGMENT_GRADIENT,
  SAD_MODEL_BUILDING_STEP,
  SAD_PHASING_STEP,
} from 'constants/phasing';
import { getPhasingStepType } from 'helpers/phasing';
import { useMemo } from 'react';

const SAD_DATA_INFO: MetadataCategory = {
  title: 'Input data',
  id: 'input_data',
  content: [
    {
      caption: 'Res. min',
      parameterName: MXSAD_MIN_RESOLUTION,
    },
    {
      caption: 'Res. max',
      parameterName: MXSAD_MAX_RESOLUTION,
    },
    {
      caption: 'Space group',
      parameterName: MXSAD_SPACE_GROUP,
    },
  ],
};

const SAD_DEFAULT_PARAMS: MetadataCategory[] = [SAD_DATA_INFO];

const SAD_PHASING_PARAMS: MetadataCategory[] = [
  {
    title: 'Inputs parameters',
    id: 'input_params',
    content: [
      {
        caption: 'Enantiomorph',
        parameterName: MXSAD_ENANTIOMORPH,
      },
      {
        caption: 'Solvent',
        parameterName: MXSAD_SOLVENT,
      },
    ],
  },
  SAD_DATA_INFO,
  {
    title: 'Statistics',
    id: 'statistics',
    content: [
      {
        caption: 'Free CC',
        parameterName: MXSAD_PSEUDO_FREE_CC,
        formatter: (value) => (
          <ColoredValue gradient={SAD_CC_GRADIENT} value={Number(value)} />
        ),
      },
    ],
  },
];

const SAD_MODELBUILDING_PARAMS: MetadataCategory[] = [
  {
    title: 'Inputs parameters',
    id: 'input_params',
    content: [
      {
        caption: 'Enantiomorph',
        parameterName: MXSAD_ENANTIOMORPH,
      },
      {
        caption: 'Solvent',
        parameterName: MXSAD_SOLVENT,
      },
    ],
  },
  SAD_DATA_INFO,
  {
    title: 'Statistics',
    id: 'statistics',
    content: [
      {
        caption: 'Partial CC',
        parameterName: MXSAD_CC_PARTIAL_MODEL,
        formatter: (value) => (
          <ColoredValue gradient={SAD_CC_GRADIENT} value={Number(value)} />
        ),
      },
      {
        caption: 'Average fragment length',
        parameterName: MXSAD_AVERAGE_FRAGMENT_LENGTH,
        digits: 2,
        formatter: (value) => (
          <ColoredValue
            gradient={SAD_FRAGMENT_GRADIENT}
            value={Number(value)}
          />
        ),
      },
      {
        caption: 'Chain count',
        parameterName: MXSAD_CHAIN_COUNT,
      },
      {
        caption: 'Residues count',
        parameterName: MXSAD_RESIDUES_COUNT,
      },
    ],
  },
];

export function MXSADPhasingDetails({ dataset }: { dataset: Dataset }) {
  const params = useMemo(() => {
    const type = getPhasingStepType(dataset);

    if (type === SAD_PHASING_STEP) return SAD_PHASING_PARAMS;
    if (type === SAD_MODEL_BUILDING_STEP) return SAD_MODELBUILDING_PARAMS;
    return SAD_DEFAULT_PARAMS;
  }, [dataset]);

  return <MetadataCategories categories={params} entity={dataset} />;
}
