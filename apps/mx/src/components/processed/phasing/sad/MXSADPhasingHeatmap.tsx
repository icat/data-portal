import { getParameterValue } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXPhasingNode } from 'components/processed/phasing/MXPhasingNode';
import { MXSAD_ENANTIOMORPH, MXSAD_SOLVENT } from 'constants/phasing';
import type { Phasing } from 'hooks/phasingRanking';

export function MXSADPhasingHeatmap({
  phasing,
  onDatasetClick,
}: {
  phasing: Phasing;
  onDatasetClick: (dataset: Dataset) => void;
}) {
  const enantiomorph = phasing.results
    .map((r) => getParameterValue(r, MXSAD_ENANTIOMORPH))
    .filter((v) => v !== undefined)
    .filter((v, i, other) => other.indexOf(v) === i)
    .sort();
  const solvent = phasing.results
    .map((r) => getParameterValue(r, MXSAD_SOLVENT))
    .filter((v) => v !== undefined)
    .filter((v, i, other) => other.indexOf(v) === i)
    .sort((a, b) => Number(a) - Number(b));

  const x = enantiomorph;
  const y = solvent;

  if (!phasing.results?.length || !x.length || !y.length) return null;

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: 'auto '.repeat(x.length + 2),
        gridTemplateRows: 'auto '.repeat(y.length + 2),
        gap: 2,
      }}
    >
      <strong
        className="d-flex justify-content-center align-items-center monospace"
        style={{
          gridColumnStart: 3,
          gridColumnEnd: x.length + 3,
          gridRow: 1,
        }}
      >
        Enantiomorph
      </strong>
      <strong
        className="d-flex justify-content-center align-items-center monospace"
        style={{
          gridColumn: 1,
          gridRowStart: 3,
          gridRowEnd: y.length + 3,
          writingMode: 'vertical-rl',
        }}
      >
        Solvent
      </strong>

      {['axis' as const, ...x].map((xv, xi) =>
        ['axis' as const, ...y].map((yv, yi) => {
          function getContent() {
            if (xv === 'axis' && yv === 'axis') return null;
            if (xv === 'axis')
              return <span className="px-1 monospace">{yv}</span>;
            if (yv === 'axis')
              return <span className="px-1 monospace">{xv}</span>;
            const dataset = phasing.results.find((r) => {
              return (
                getParameterValue(r, MXSAD_ENANTIOMORPH) === xv &&
                getParameterValue(r, MXSAD_SOLVENT) === yv
              );
            });
            if (!dataset) return null;

            return (
              <div
                className="d-flex h-100 w-100 justify-content-center"
                style={{
                  fontSize: '0.8rem',
                }}
              >
                <MXPhasingNode
                  key={dataset.id}
                  dataset={dataset}
                  phasing={phasing}
                  onDatasetClick={onDatasetClick}
                />
              </div>
            );
          }
          return (
            <div
              key={`${xv}-${yv}`}
              style={{
                gridColumn: xi + 2,
                gridRow: yi + 2,
              }}
              className="d-flex justify-content-center align-items-center"
            >
              {getContent()}
            </div>
          );
        }),
      )}
    </div>
  );
}
