import { FlowChartLazy } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXPhasingNode } from 'components/processed/phasing/MXPhasingNode';
import type { Phasing } from 'hooks/phasingRanking';
import { useMemo } from 'react';

export function MXPhasingFlowChart({
  phasing,
  currentDataset,
  onDatasetClick,
}: {
  phasing: Phasing;
  currentDataset?: Dataset;
  onDatasetClick?: (dataset: Dataset) => void;
}) {
  const { nodes, edges } = useMemo(
    () => ({
      nodes: phasing.datasets.map((dataset) => ({
        id: String(dataset.id),
        element: (
          <MXPhasingNode
            dataset={dataset}
            phasing={phasing}
            onDatasetClick={onDatasetClick}
            currentDataset={currentDataset}
            key={String(dataset.id) + String(currentDataset?.id)}
          />
        ),
      })),
      edges: phasing.datasets
        .flatMap((dataset) => {
          if (!dataset.outputDatasets) return [];
          return dataset.outputDatasets.map((outputDataset) => ({
            id: `${dataset.id}-${outputDataset.id}`,
            source: String(dataset.id),
            target: String(outputDataset.id),
          }));
        })
        .filter((edge, i, all) => all.findIndex((e) => e.id === edge.id) === i),
    }),
    [currentDataset, onDatasetClick, phasing],
  );

  return (
    <div
      className="d-flex flex-column h-100 w-100"
      style={{
        minHeight: 'inherit',
      }}
    >
      <FlowChartLazy
        nodes={nodes}
        edges={edges}
        defaultLayoutDirection="TB"
        centerOnNode={currentDataset?.id.toString()}
      />
    </div>
  );
}
