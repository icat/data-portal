import {
  DatasetWindow,
  LazyWrapper,
  Loading,
  formatDateToDayAndTime,
  getDatasetName,
  getDatasetParamValue,
  getInputDatasetIds,
} from '@edata-portal/core';
import {
  DATASET_LIST_ENDPOINT,
  useGetEndpoint,
  type Dataset,
} from '@edata-portal/icat-plus-api';
import { MXMRSearchModelLink } from 'components/processed/phasing/mr/MXMRSearchModelLink';
import { MXPhasingFlowChart } from 'components/processed/phasing/MXPhasingFlowChart';
import { getMXPhasingTabs } from 'components/processed/phasing/MXPhasingTabs';
import { MXSADPhasingHeatmap } from 'components/processed/phasing/sad/MXSADPhasingHeatmap';
import { PROCESSED_PROGRAM_PARAM } from 'constants/metadatakeys';
import { MXMR_PHASING_SEARCH_MODEL } from 'constants/phasing';
import { getPhasingSpaceGroup } from 'helpers/phasing';
import { getSpaceGroup } from 'helpers/spacegroup';
import {
  getRankedPhasings,
  type PhasingType,
  type Phasing,
} from 'hooks/phasingRanking';
import { useMemo, useState } from 'react';
import { Badge } from 'react-bootstrap';

export function MXPhasingList({
  datasets,
  type,
}: {
  datasets: Dataset[] | Dataset;
  type: PhasingType;
}) {
  const phasings = getRankedPhasings(datasets, type);

  return (
    <div className="d-flex flex-column gap-2 w-100 h-100">
      {phasings.map((phasing, i, all) => (
        <>
          <LazyWrapper placeholder={<Loading />}>
            <MXPhasingListItem
              key={phasing.root.id}
              phasing={phasing}
              index={i}
            />
          </LazyWrapper>
          {i !== all.length - 1 && <hr className="m-0 p-0" />}
        </>
      ))}
    </div>
  );
}

function MXPhasingListItem({
  phasing,
  index,
}: {
  phasing: Phasing;
  index: number;
}) {
  const [displayDataset, setDisplayDataset] = useState<Dataset | undefined>(
    undefined,
  );

  const inputs_ids = useMemo(
    () => getInputDatasetIds(phasing.root).join(','),
    [phasing.root],
  );

  const inputs = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: inputs_ids,
    },
    skipFetch: !inputs_ids.length,
    default: [] as Dataset[],
  });

  const programs = useMemo(() => {
    return inputs
      .map((input) => getDatasetParamValue(input, PROCESSED_PROGRAM_PARAM))
      .filter((program) => program)
      .join(', ');
  }, [inputs]);

  const spaceGroup = useMemo(() => {
    return phasing.datasets
      .map((dataset) => getPhasingSpaceGroup(dataset))
      .map((sg) => getSpaceGroup(sg)?.name)
      .filter((sg, i, all) => sg && all.indexOf(sg) === i)
      .join(', ');
  }, [phasing.datasets]);

  const searchModelDataset = useMemo(() => {
    return phasing.datasets.filter(
      (dataset) =>
        getDatasetParamValue(dataset, MXMR_PHASING_SEARCH_MODEL) !== undefined,
    );
  }, [phasing.datasets]);

  const programHeader = [
    programs && `From ${programs}`,
    spaceGroup,
    searchModelDataset?.length && (
      <>
        Search model{' '}
        {searchModelDataset.map((dataset) => (
          <MXMRSearchModelLink key={dataset.id} dataset={dataset} />
        ))}
      </>
    ),
  ]
    .filter(Boolean)
    .flatMap((v, i, all) => (v ? [v, i !== all.length - 1 && ' - '] : []));

  return (
    <div className="d-flex w-100 flex-column gap-2">
      <div
        className="d-flex flex-row gap-2"
        style={{
          fontSize: '1.2rem',
        }}
      >
        <Badge className="d-flex align-items-center justify-content-center">
          {index + 1}
        </Badge>
        <div className="d-flex flex-column monospace">
          {formatDateToDayAndTime(phasing.root.startDate)}
          <div
            className="d-flex flex-row gap-2"
            style={{
              fontSize: '0.9rem',
            }}
          >
            <strong>{programHeader}</strong>
          </div>
        </div>
      </div>

      <div className="d-flex w-100 flex-row gap-2" style={{ minHeight: 300 }}>
        {phasing.type === 'SAD' && (
          <div className="text-nowrap">
            <MXSADPhasingHeatmap
              phasing={phasing}
              onDatasetClick={(dataset: Dataset) => setDisplayDataset(dataset)}
            />
          </div>
        )}
        <MXPhasingFlowChart
          phasing={phasing}
          onDatasetClick={(dataset: Dataset) => setDisplayDataset(dataset)}
        />
      </div>
      {displayDataset && (
        <DatasetWindow
          dataset={displayDataset}
          tabs={getMXPhasingTabs(
            phasing,
            displayDataset,
            setDisplayDataset,
            true,
          )}
          onClose={() => {
            setDisplayDataset(undefined);
          }}
          title={getDatasetName(displayDataset)}
        />
      )}
    </div>
  );
}
