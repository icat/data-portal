import { Button, Loading, stringifyWithUnitPrefix } from '@edata-portal/core';
import { DownloadFileButton } from '@edata-portal/core/src/components/datafile/viewer/file/DownloadFileButton';
import { type Dataset, type Datafile } from '@edata-portal/icat-plus-api';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { usePhasingDensityMapFiles } from 'hooks/phasingDensityMap';
import { Suspense } from 'react';
import { Link } from 'react-router-dom';

export function MXPhasingDensityMapDetails({ dataset }: { dataset: Dataset }) {
  return (
    <div className="d-flex flex-column p-2 border rounded">
      <Suspense fallback={<Loading />}>
        <LoadAndDisplay dataset={dataset} />
      </Suspense>
    </div>
  );
}

function LoadAndDisplay({ dataset }: { dataset: Dataset }) {
  const densityMap = usePhasingDensityMapFiles(dataset);

  if (!densityMap) return null;

  return (
    <div className="d-flex flex-column gap-2 h-100">
      <h5>Electron density map</h5>
      <FileItem title="PDB" file={densityMap.pdbFile} />
      <FileItem title="MTZ" file={densityMap.mtzFile} />

      {densityMap.viewUrl && (
        <>
          <hr className="p-0 m-0" />
          <div className="d-flex justify-content-center align-items-center w-100">
            <Link to={densityMap.viewUrl} target="_blank">
              <Button>
                <FontAwesomeIcon icon={faExternalLinkAlt} /> Open viewer
              </Button>
            </Link>
          </div>
        </>
      )}
    </div>
  );
}

function FileItem({
  title,
  file,
}: {
  title: string;
  file: Datafile | undefined;
}) {
  return (
    <div className="d-flex flex-column gap-1 w-100 text-nowrap">
      <strong className={file ? 'text-success' : 'text-warning'}>
        {title} file:
      </strong>
      <div className="d-flex flex-row gap-2 align-items-center">
        <div className="monospace">
          {file ? <span>{file.name}</span> : <i>No file found</i>}
        </div>
        {file && (
          <>
            <span className="text-nowrap">
              {stringifyWithUnitPrefix(file.fileSize, 2, 'B')}
            </span>
            <DownloadFileButton file={file} />
          </>
        )}
      </div>
    </div>
  );
}
