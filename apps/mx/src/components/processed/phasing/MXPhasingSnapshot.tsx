import { MXMRPhasingSnapshot } from 'components/processed/phasing/mr/MXMRPhasingSnapshot';
import { MXSADPhasingSnapshot } from 'components/processed/phasing/sad/MXSADPhasingSnapshot';
import type { Phasing } from 'hooks/phasingRanking';

export function MXPhasingSnapshot({ phasing }: { phasing: Phasing }) {
  if (phasing.type === 'SAD') {
    return <MXSADPhasingSnapshot phasing={phasing} />;
  }
  if (phasing.type === 'MR') {
    return <MXMRPhasingSnapshot phasing={phasing} />;
  }
  return null;
}
