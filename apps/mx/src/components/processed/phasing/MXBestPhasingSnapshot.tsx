import { DatasetSnapshot, TabDefinition } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { faExternalLink } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MXPhasingList } from 'components/processed/phasing/MXPhasingList';
import { MXPhasingRankingHelper } from 'components/processed/phasing/MXPhasingRankingHelper';
import { MXPhasingSnapshot } from 'components/processed/phasing/MXPhasingSnapshot';
import { usePhasingDensityMapViewer } from 'hooks/phasingDensityMap';
import { getBestPhasing } from 'hooks/phasingRanking';

const SIZE = {
  min: 200,
  max: 300,
};

export function MXBestPhasingSnapshot({
  datasets,
}: {
  datasets: Dataset[] | Dataset;
}) {
  const bestSADPhasing = getBestPhasing(datasets, 'SAD');
  const bestMRPhasing = getBestPhasing(datasets, 'MR');

  const SADDensityMap = usePhasingDensityMapViewer(bestSADPhasing?.bestResult);
  const MRDensityMap = usePhasingDensityMapViewer(bestMRPhasing?.bestResult);

  if (!bestSADPhasing && !bestMRPhasing) return null;

  const tabs: TabDefinition[] = [
    {
      content: <MXPhasingList datasets={datasets} type="SAD" />,
      key: 'SAD',
      title: 'SAD',
      hidden: !bestSADPhasing,
    },
    {
      content: <MXPhasingList datasets={datasets} type="MR" />,
      key: 'MR',
      title: 'MR',
      hidden: !bestMRPhasing,
    },
  ];

  const sadSnapshot = bestSADPhasing ? (
    <DatasetSnapshot
      dataset={bestSADPhasing.root}
      width={SIZE}
      primaryItems={[
        {
          element: <MXPhasingSnapshot phasing={bestSADPhasing} />,
          showWindow: 'SAD',
        },
      ]}
      secondaryItems={
        SADDensityMap?.openView
          ? [
              {
                element: <MoorhenIcon />,
                onClick: SADDensityMap.openView,
              },
            ]
          : undefined
      }
      title={`Best SAD phasing`}
      extraHeader={<MXPhasingRankingHelper />}
      windowTitle={
        <>
          Phasing ranking <MXPhasingRankingHelper />
        </>
      }
      windowHideDatasetButtons
      windowTabs={tabs}
    />
  ) : null;

  const mrSnapshot = bestMRPhasing ? (
    <DatasetSnapshot
      dataset={bestMRPhasing.root}
      width={SIZE}
      primaryItems={[
        {
          element: <MXPhasingSnapshot phasing={bestMRPhasing} />,
          showWindow: 'MR',
        },
      ]}
      secondaryItems={
        MRDensityMap?.openView
          ? [
              {
                element: <MoorhenIcon />,
                onClick: MRDensityMap.openView,
              },
            ]
          : undefined
      }
      title={`Best MR phasing`}
      extraHeader={<MXPhasingRankingHelper />}
      windowTitle={
        <>
          Phasing ranking <MXPhasingRankingHelper />
        </>
      }
      windowHideDatasetButtons
      windowTabs={tabs}
    />
  ) : null;

  //if only one of the snapshots is available, return it
  if (!(sadSnapshot && mrSnapshot)) return sadSnapshot || mrSnapshot;

  return (
    <div className="d-flex flex-row gap-1 h-100">
      {sadSnapshot}
      {mrSnapshot}
    </div>
  );
}

function MoorhenIcon() {
  return (
    <div className="d-flex justify-content-center align-items-center w-100 gap-2">
      <FontAwesomeIcon icon={faExternalLink} />
      <small>Open viewer</small>
    </div>
  );
}
