import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  PHASING_RESULT_MR,
  PHASING_RESULT_RANKING_PARAMS,
  PHASING_RESULT_SAD,
} from 'hooks/phasingRanking';
import { OverlayTrigger, Popover } from 'react-bootstrap';

function getOrderDisplay(order: number) {
  return order >= 0 ? 'Lowest' : 'Highest';
}

export function MXPhasingRankingHelper() {
  const sadCriteria = PHASING_RESULT_RANKING_PARAMS[PHASING_RESULT_SAD];
  const mrCriteria = PHASING_RESULT_RANKING_PARAMS[PHASING_RESULT_MR];

  return (
    <OverlayTrigger
      trigger={['hover', 'focus']}
      placement="auto"
      overlay={
        <Popover key={'HelpContent'}>
          <Popover.Header>How do we rank phasing?</Popover.Header>
          <Popover.Body
            style={{
              fontSize: 'smaller',
            }}
          >
            <span>
              Phasing ranking is based on the following criteria:
              <ul>
                <li>
                  SAD Phasing : {getOrderDisplay(sadCriteria.order)}{' '}
                  {sadCriteria.displayName}
                </li>

                <li>
                  MR Phasing : {getOrderDisplay(mrCriteria.order)}{' '}
                  {mrCriteria.displayName}
                </li>
              </ul>
            </span>
          </Popover.Body>
        </Popover>
      }
    >
      <FontAwesomeIcon
        key={'Help'}
        className="text-muted"
        icon={faQuestionCircle}
      />
    </OverlayTrigger>
  );
}
