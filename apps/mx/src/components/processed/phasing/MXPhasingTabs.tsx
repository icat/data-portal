import {
  getFilesTab,
  getInputsTab,
  type TabDefinition,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXPhasingSummary } from 'components/processed/phasing/MXPhasingSummary';
import type { Phasing } from 'hooks/phasingRanking';

export function getMXPhasingTabs(
  phasing: Phasing,
  dataset: Dataset,
  setDataset: (dataset: Dataset) => void,
  showPathInFiles: boolean,
): TabDefinition[] {
  return [
    {
      content: (
        <MXPhasingSummary
          phasing={phasing}
          dataset={dataset}
          setDataset={setDataset}
        />
      ),
      key: 'Summary',
      title: 'Summary',
    },
    getFilesTab(dataset, showPathInFiles, true),
    getInputsTab(phasing.root),
  ];
}
