import { getScanType, getDatasetName } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXMRPhasingNodeContent } from 'components/processed/phasing/mr/MXMRPhasingNodeContent';
import { MXSADPhasingNodeContent } from 'components/processed/phasing/sad/MXSADPhasingNodeContent';
import { MR_PHASING_SCANTYPE, SAD_PHASING_SCANTYPE } from 'constants/phasing';
import type { Phasing } from 'hooks/phasingRanking';
import { useState, useMemo, useCallback } from 'react';

export function MXPhasingNode({
  dataset,
  phasing,
  onDatasetClick,
  currentDataset,
}: {
  dataset: Dataset;
  phasing: Phasing;
  onDatasetClick?: (dataset: Dataset) => void;
  currentDataset?: Dataset;
}) {
  const [hovering, setHovering] = useState(false);

  const selected = useMemo(
    () => currentDataset?.id === dataset.id,
    [currentDataset, dataset],
  );

  const border =
    selected || (onDatasetClick && hovering) ? 'border-info' : 'border-gray';

  const getContent = useCallback(() => {
    const scanType = getScanType(dataset);
    const isBest = phasing.bestResult?.id === dataset.id;
    if (scanType === MR_PHASING_SCANTYPE)
      return <MXMRPhasingNodeContent dataset={dataset} isBest={isBest} />;
    if (scanType === SAD_PHASING_SCANTYPE)
      return <MXSADPhasingNodeContent dataset={dataset} isBest={isBest} />;
    return <small>{getDatasetName(dataset)}</small>;
  }, [dataset, phasing.bestResult?.id]);

  return (
    <div
      className={`border border-3 ${border} rounded p-1 bg-white text-center d-flex flex-column gap-1`}
      onClick={() => onDatasetClick?.(dataset)}
      onMouseEnter={() => setHovering(true)}
      onMouseLeave={() => setHovering(false)}
      style={{
        cursor: onDatasetClick ? 'pointer' : undefined,
        position: 'relative',
      }}
    >
      {selected && (
        <div
          className="bg-info text-white px-3 text-nowrap"
          style={{
            position: 'absolute',
            top: 0,
            left: '50%',
            transform: 'translate(-50%,-100%)',
            borderRadius: '1em 1em 0 0',
          }}
        >
          <small>
            <strong>Selected</strong>
          </small>
        </div>
      )}
      {getContent()}
    </div>
  );
}
