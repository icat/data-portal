import { getInputDatasetIds } from '@edata-portal/core';
import { MXMRPhasingNodeContent } from 'components/processed/phasing/mr/MXMRPhasingNodeContent';
import type { Phasing } from 'hooks/phasingRanking';
import { useMemo } from 'react';

export function MXMRPhasingSnapshot({ phasing }: { phasing: Phasing }) {
  const [best, input] = useMemo(() => {
    const best = phasing.bestResult;
    if (!best) return [undefined, undefined];

    const bestInputIds = getInputDatasetIds(best).map((d) => Number(d));
    const bestInput =
      phasing.datasets.filter((d) => bestInputIds.includes(d.id))[0] ||
      undefined;
    return [best, bestInput];
  }, [phasing]);

  if (!best && !input) return null;

  return (
    <div
      className="d-flex flex-column gap-2"
      style={{
        fontSize: '0.8rem',
      }}
    >
      {input && <MXMRPhasingNodeContent interactive={false} dataset={input} />}
      {best && <MXMRPhasingNodeContent interactive={false} dataset={best} />}
    </div>
  );
}
