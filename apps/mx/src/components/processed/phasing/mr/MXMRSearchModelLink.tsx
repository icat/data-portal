import { getDatasetParamValue } from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import { MXMR_PHASING_SEARCH_MODEL } from 'constants/phasing';

export function MXMRSearchModelLink({ dataset }: { dataset: Dataset }) {
  const searchModel = getDatasetParamValue(dataset, MXMR_PHASING_SEARCH_MODEL);
  if (!searchModel) return null;

  return (
    <a
      onClick={(e) => {
        e.stopPropagation();
      }}
      href={`https://www.rcsb.org/structure/${searchModel}`}
      target="_blank"
      rel="noreferrer"
    >
      {searchModel}
    </a>
  );
}
