import {
  ColoredValue,
  MetadataTable,
  MetadataTableParameters,
  getDatasetName,
  getDatasetParamValue,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXMRSearchModelLink } from 'components/processed/phasing/mr/MXMRSearchModelLink';
import {
  MR_LIGAND_FITTING_STEP,
  MR_PHASING_STEP,
  MR_REFINEMENT_STEP,
  MR_TFZ_GRADIENT,
  MR_TFZ_GRADIENT_MONOCLINIC,
  MXMR_LIGAND_FITTING_LIGAND_NAME,
  MXMR_LIGAND_FITTING_LIGAND_XYZ,
  MXMR_PHASING_BEST_RFZ,
  MXMR_PHASING_BEST_TFZ,
  MXMR_PHASING_LLG,
  MXMR_PHASING_SEARCH_MODEL,
  MXMR_REFINEMENT_R_CRIST,
  MXMR_REFINEMENT_R_FREE,
  MXMR_SPACE_GROUP,
} from 'constants/phasing';
import { getPhasingStepType } from 'helpers/phasing';
import { getSpaceGroup } from 'helpers/spacegroup';
import { useMemo } from 'react';
import { Badge } from 'react-bootstrap';

const MR_REFINEMENT_PARAMS: MetadataTableParameters = [
  {
    caption: 'R-cryst',
    parameterName: MXMR_REFINEMENT_R_CRIST,
  },
  {
    caption: 'R-Free',
    parameterName: MXMR_REFINEMENT_R_FREE,
  },
];

const MR_LIGAND_FITTING_PARAMS: MetadataTableParameters = [
  {
    caption: 'Pos',
    parameterName: MXMR_LIGAND_FITTING_LIGAND_XYZ,
  },
  {
    caption: 'Name',
    parameterName: MXMR_LIGAND_FITTING_LIGAND_NAME,
  },
];

export function MXMRPhasingNodeContent({
  dataset,
  isBest,
  hideName,
  interactive = true,
}: {
  dataset: Dataset;
  isBest?: boolean;
  hideName?: boolean;
  interactive?: boolean;
}) {
  const params = useMemo(() => {
    const type = getPhasingStepType(dataset);

    const spaceGroup = getDatasetParamValue(dataset, MXMR_SPACE_GROUP);

    const spaceGroupInfo = getSpaceGroup(spaceGroup);

    const gradient =
      spaceGroupInfo?.crystalSystem === 'Monoclinic'
        ? MR_TFZ_GRADIENT_MONOCLINIC
        : MR_TFZ_GRADIENT;

    if (type === MR_PHASING_STEP)
      return [
        {
          caption: 'Best RFZ',
          parameterName: MXMR_PHASING_BEST_RFZ,
        },
        {
          caption: 'Best TFZ',
          parameterName: MXMR_PHASING_BEST_TFZ,
          formatter: (value) => (
            <ColoredValue gradient={gradient} value={Number(value)} />
          ),
        },
        {
          caption: 'LLG',
          parameterName: MXMR_PHASING_LLG,
        },
        {
          caption: 'Search model',
          parameterName: MXMR_PHASING_SEARCH_MODEL,
          formatter: interactive
            ? () => <MXMRSearchModelLink dataset={dataset} />
            : undefined,
        },
      ];

    if (type === MR_REFINEMENT_STEP) return MR_REFINEMENT_PARAMS;
    if (type === MR_LIGAND_FITTING_STEP) return MR_LIGAND_FITTING_PARAMS;
    return [];
  }, [dataset, interactive]);

  return (
    <div
      className={`d-flex flex-column gap-1 justify-content-center align-items-center h-100 w-100`}
    >
      {hideName ? null : (
        <strong>
          {getPhasingStepType(dataset) || getDatasetName(dataset)}
        </strong>
      )}
      {isBest && <Badge bg="info">Best</Badge>}
      <MetadataTable
        parameters={params}
        entity={dataset}
        valuePlaceholder="N/A"
        size="sm"
      />
    </div>
  );
}
