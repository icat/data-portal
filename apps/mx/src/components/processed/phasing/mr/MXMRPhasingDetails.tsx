import { MetadataCategories, type MetadataCategory } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXMRSearchModelLink } from 'components/processed/phasing/mr/MXMRSearchModelLink';
import {
  MR_LIGAND_FITTING_STEP,
  MR_PHASING_STEP,
  MR_REFINEMENT_STEP,
  MXMR_LIGAND_FITTING_B_FACTOR,
  MXMR_LIGAND_FITTING_FOFC_CC,
  MXMR_LIGAND_FITTING_LIGAND_NAME,
  MXMR_LIGAND_FITTING_LIGAND_XYZ,
  MXMR_LIGAND_FITTING_R_CRYST,
  MXMR_LIGAND_FITTING_R_FREE,
  MXMR_MAX_RESOLUTION,
  MXMR_MIN_RESOLUTION,
  MXMR_PHASING_BEST_RFZ,
  MXMR_PHASING_BEST_TFZ,
  MXMR_PHASING_LLG,
  MXMR_PHASING_NB_SEARCH_MODELS_FOUND,
  MXMR_PHASING_SEARCH_MODEL,
  MXMR_PHASING_SOURCE,
  MXMR_REFINEMENT_R_CRIST,
  MXMR_REFINEMENT_R_FREE,
  MXMR_SPACE_GROUP,
} from 'constants/phasing';
import { getPhasingStepType } from 'helpers/phasing';
import { useMemo } from 'react';

const MR_DATA_INFO: MetadataCategory = {
  title: 'Input data',
  id: 'input_data',
  content: [
    {
      caption: 'Min resolution',
      parameterName: MXMR_MIN_RESOLUTION,
    },
    {
      caption: 'Max resolution',
      parameterName: MXMR_MAX_RESOLUTION,
    },
    {
      caption: 'Space group',
      parameterName: MXMR_SPACE_GROUP,
    },
  ],
};

const getMRPhasingParams = (dataset: Dataset): MetadataCategory[] => [
  {
    title: 'Inputs parameters',
    id: 'input_params',
    content: [
      { caption: 'Source', parameterName: MXMR_PHASING_SOURCE },
      {
        caption: 'Search model',
        parameterName: MXMR_PHASING_SEARCH_MODEL,
        formatter: () => <MXMRSearchModelLink dataset={dataset} />,
      },
    ],
  },
  MR_DATA_INFO,
  {
    title: 'Outputs',
    id: 'outputs',
    content: [
      {
        caption: 'Nb. search model found',
        parameterName: MXMR_PHASING_NB_SEARCH_MODELS_FOUND,
      },
      {
        caption: 'Best RFZ',
        parameterName: MXMR_PHASING_BEST_RFZ,
      },
      {
        caption: 'Best TFZ',
        parameterName: MXMR_PHASING_BEST_TFZ,
      },
      {
        caption: 'LLG',
        parameterName: MXMR_PHASING_LLG,
      },
    ],
  },
];

const MR_REFINEMENT_PARAMS: MetadataCategory[] = [
  MR_DATA_INFO,
  {
    title: 'Statistics',
    id: 'statistics',
    content: [
      {
        caption: 'R-cryst',
        parameterName: MXMR_REFINEMENT_R_CRIST,
      },
      {
        caption: 'R-Free',
        parameterName: MXMR_REFINEMENT_R_FREE,
      },
    ],
  },
];

const MR_LIGAND_FITTING_PARAMS: MetadataCategory[] = [
  MR_DATA_INFO,
  {
    title: 'Ligand',
    id: 'ligand',
    content: [
      {
        caption: 'Ligand coordinates',
        parameterName: MXMR_LIGAND_FITTING_LIGAND_XYZ,
      },
      {
        caption: 'Ligand name',
        parameterName: MXMR_LIGAND_FITTING_LIGAND_NAME,
      },
    ],
  },
  {
    title: 'Statistics',
    id: 'statistics',
    content: [
      {
        caption: 'FOFC CC',
        parameterName: MXMR_LIGAND_FITTING_FOFC_CC,
      },
      {
        caption: 'R-Free',
        parameterName: MXMR_LIGAND_FITTING_R_FREE,
      },
      {
        caption: 'R-cryst',
        parameterName: MXMR_LIGAND_FITTING_R_CRYST,
      },
      {
        caption: 'B-factor',
        parameterName: MXMR_LIGAND_FITTING_B_FACTOR,
      },
    ],
  },
];

export function MXMRPhasingDetails({ dataset }: { dataset: Dataset }) {
  const params = useMemo(() => {
    const type = getPhasingStepType(dataset);

    if (type === MR_PHASING_STEP) return getMRPhasingParams(dataset);
    if (type === MR_REFINEMENT_STEP) return MR_REFINEMENT_PARAMS;
    if (type === MR_LIGAND_FITTING_STEP) return MR_LIGAND_FITTING_PARAMS;
    return [];
  }, [dataset]);

  return <MetadataCategories categories={params} entity={dataset} />;
}
