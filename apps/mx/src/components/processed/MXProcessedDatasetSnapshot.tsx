import {
  DatasetImage,
  DatasetSnapshot,
  DatasetSnapshotItem,
  prettyPrintScanType,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAutoProcessingSnapshotContent } from 'components/processed/AutoProcessing/MXAutoProcessingSnapshotContent';
import {
  MX_SNAPSHOT_MAX_WIDTH,
  MX_SNAPSHOT_MIN_WIDTH,
} from 'constants/display';
import { isAutoProcessing, isQualityIndicator } from 'helpers/processed';
import { getDozorSnapshot } from 'helpers/snapshot';
import { getMxDatasetTabs } from 'helpers/tabs';

export default function MXProcessedDatasetSnapshot({
  dataset,
}: {
  dataset: Dataset;
}) {
  return (
    <DatasetSnapshot
      dataset={dataset}
      windowTabs={getMxDatasetTabs(dataset, true)}
      primaryItems={getSnapshotItems(dataset)}
      title={getTitle(dataset)}
      width={
        isAutoProcessing(dataset)
          ? {
              min: 420,
              max: 450,
            }
          : {
              min: MX_SNAPSHOT_MIN_WIDTH,
              max: MX_SNAPSHOT_MAX_WIDTH,
            }
      }
    />
  );
}

function getTitle(dataset: Dataset): string {
  if (isAutoProcessing(dataset)) {
    return 'Auto Processing';
  }

  return prettyPrintScanType(dataset);
}

function getSnapshotItems(dataset: Dataset): DatasetSnapshotItem[] {
  if (isQualityIndicator(dataset)) {
    const dozorSnapshot = getDozorSnapshot(dataset);
    return [
      {
        showWindow: 'Dozor',
        element: dozorSnapshot ? (
          <DatasetImage
            resourceId={dozorSnapshot.id}
            name={dozorSnapshot.name}
            displayFileName={false}
            zoomable={false}
          />
        ) : (
          <i>No dozor image</i>
        ),
      },
    ];
  }

  if (isAutoProcessing(dataset)) {
    return [
      {
        showWindow: 'AutoProcessing',
        element: <MXAutoProcessingSnapshotContent dataset={dataset} />,
      },
    ];
  }

  return [];
}
