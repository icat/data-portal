import { LazyWrapper, Loading, Gallery } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAutoProcessingSnapshotContent } from 'components/processed/AutoProcessing/MXAutoProcessingSnapshotContent';
import { MXDozorPlot } from 'components/processed/dozor/MXDozorPlot';
import { isAutoProcessing, isQualityIndicator } from 'helpers/processed';

export function MXProcessedDatasetSummaryContent({
  dataset,
}: {
  dataset: Dataset;
}) {
  if (isQualityIndicator(dataset)) {
    return (
      <LazyWrapper placeholder={<Loading />}>
        <MXDozorPlot dataset={dataset} />
      </LazyWrapper>
    );
  }
  if (isAutoProcessing(dataset)) {
    return (
      <div
        style={{
          fontSize: '1.1rem',
        }}
      >
        <MXAutoProcessingSnapshotContent dataset={dataset} />
      </div>
    );
  }
  return (
    <LazyWrapper placeholder={<Loading />}>
      <Gallery dataset={dataset} />
    </LazyWrapper>
  );
}
