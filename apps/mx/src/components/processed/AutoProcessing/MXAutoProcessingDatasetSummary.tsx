import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAutoProcessingStatValue } from 'components/processed/AutoProcessing/MXAutoProcessingStatValue';
import { MX_SHELLS, MX_SHELL_COLORS } from 'constants/shell';
import {
  MX_AUTOPROCESSING_STATS,
  MX_AUTOPROCESSING_STAT_LABELS,
} from 'constants/stats';

import { Table } from 'react-bootstrap';

export function MXAutoProcessingDatasetSummary({
  dataset,
}: {
  dataset: Dataset;
}) {
  return (
    <Table
      responsive
      size="sm"
      style={{
        whiteSpace: 'nowrap',
        textAlign: 'center',
        margin: 0,
        fontSize: 'smaller',
      }}
      striped
      hover
    >
      <thead>
        <tr>
          <th />

          {MX_AUTOPROCESSING_STATS.map((stat) => (
            <th key={stat}>{MX_AUTOPROCESSING_STAT_LABELS[stat]}</th>
          ))}
        </tr>
      </thead>

      <tbody>
        {MX_SHELLS.map((shell) => {
          return (
            <tr key={shell}>
              <th
                style={{
                  color: MX_SHELL_COLORS[shell],
                }}
              >
                {shell}
              </th>
              {MX_AUTOPROCESSING_STATS.map((stat) => {
                return (
                  <td key={stat}>
                    <MXAutoProcessingStatValue
                      dataset={dataset}
                      shell={shell}
                      stat={stat}
                    />
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}
