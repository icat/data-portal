import {
  Progress,
  convertToFixed,
  getDatasetParamValue,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { type MXShell, MX_SHELL_COLORS } from 'constants/shell';
import {
  MXAutoProcessingStat,
  MX_AUTOPROCESSING_STAT_COMPLETENESS,
} from 'constants/stats';
import {
  getColorForStat,
  getMXAutoProcessingStatParam,
} from 'helpers/processed';
import { useMXSettings } from 'hooks/mxSettings';
import { useMemo } from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';

export function MXAutoProcessingStatValue({
  dataset,
  shell,
  stat,
}: {
  dataset: Dataset;
  shell: MXShell;
  stat: MXAutoProcessingStat;
}) {
  const [settings] = useMXSettings();

  const isRankingStat = useMemo(() => {
    return (
      settings.autoProcessingRankingParameter === stat &&
      settings.autoProcessingRankingShell === shell
    );
  }, [
    settings.autoProcessingRankingParameter,
    settings.autoProcessingRankingShell,
    shell,
    stat,
  ]);

  const key = getMXAutoProcessingStatParam(shell, stat);
  const value = getDatasetParamValue(dataset, key);

  const numberValue = Number(value);

  const formattedValue = convertToFixed(value, 2, '-');

  const colorBg = getColorForStat(shell, stat, numberValue, 0.15);
  const colorProgress = getColorForStat(shell, stat, numberValue, 0.5);

  const content =
    stat === MX_AUTOPROCESSING_STAT_COMPLETENESS && !isNaN(numberValue) ? (
      <Progress
        valuePercent={numberValue}
        color={colorProgress}
        text={
          <>
            {formattedValue}%{' '}
            {isRankingStat ? <AutoProcessingRankingValue /> : null}
          </>
        }
      />
    ) : (
      <span
        className="text-center"
        style={{
          color: MX_SHELL_COLORS[shell],
          backgroundColor: colorBg,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          gap: 4,
          padding: '0 2px',
        }}
      >
        {formattedValue} {isRankingStat ? <AutoProcessingRankingValue /> : null}
      </span>
    );

  return content;
}

function AutoProcessingRankingValue() {
  return (
    <OverlayTrigger
      trigger={['hover', 'focus']}
      placement="auto"
      overlay={
        <Popover key={'HelpContent'}>
          <Popover.Header>This value is used for ranking</Popover.Header>
          <Popover.Body
            style={{
              fontSize: 'smaller',
            }}
          >
            This is the value that is currently in use for ranking this auto
            processing relative to others. You can change the ranking parameter
            in the settings.
          </Popover.Body>
        </Popover>
      }
    >
      <FontAwesomeIcon key={'star'} icon={faStar} />
    </OverlayTrigger>
  );
}
