import { Window } from '@edata-portal/core/src/components/layout/Window';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAutoProcessingTable } from 'components/processed/AutoProcessing/MXAutoProcessingTable';

export function MXAutoProcessingListWindow({
  datasets,
  onClose,
  merged,
}: {
  datasets: Dataset[];
  onClose: () => void;
  merged: 'only' | 'exclude' | 'include';
}) {
  return (
    <Window onClose={onClose} title="Auto processing">
      <MXAutoProcessingTable datasets={datasets} merged={merged} />
    </Window>
  );
}
