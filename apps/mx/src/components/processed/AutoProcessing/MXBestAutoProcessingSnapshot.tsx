import { DatasetSnapshot } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { useBestAutoProcessing } from 'hooks/autoProcessingRanking';
import { MXAutoProcessingTable } from 'components/processed/AutoProcessing/MXAutoProcessingTable';
import { MXAutoProcessingSnapshotContent } from 'components/processed/AutoProcessing/MXAutoProcessingSnapshotContent';
import { MXSettingsAutoProcessingHelper } from 'components/settings/MXSettingsAutoProcessingHelper';

export function MXBestAutoProcessingSnapshot({
  datasets,
  merged,
}: {
  datasets: Dataset[];
  merged: 'only' | 'exclude' | 'include';
}) {
  const bestAutoProcessing = useBestAutoProcessing(datasets, merged);

  if (!bestAutoProcessing || !datasets) return null;

  return (
    <DatasetSnapshot
      dataset={bestAutoProcessing}
      width={{
        min: 350,
        max: 500,
      }}
      title={
        merged === 'only'
          ? `Best merged auto processing`
          : `Best auto processing`
      }
      windowTitle={
        <>
          Auto processing ranking <MXSettingsAutoProcessingHelper />
        </>
      }
      windowHideDatasetButtons
      extraHeader={<MXSettingsAutoProcessingHelper />}
      primaryItems={[
        {
          element: (
            <MXAutoProcessingSnapshotContent dataset={bestAutoProcessing} />
          ),
          showWindow: true,
        },
      ]}
      windowTabs={[
        {
          key: 'autoprocessings',
          title: 'Auto processings',
          content: (
            <MXAutoProcessingTable datasets={datasets} merged={merged} />
          ),
        },
      ]}
    />
  );
}
