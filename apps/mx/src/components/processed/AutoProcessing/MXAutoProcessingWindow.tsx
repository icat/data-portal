import { DatasetWindow, getDatasetName } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

import { getMxDatasetTabs } from 'helpers/tabs';

export function MXAutoProcessingWindow({
  dataset,
  onClose,
}: {
  dataset: Dataset;
  onClose: () => void;
}) {
  return (
    <DatasetWindow
      onClose={onClose}
      dataset={dataset}
      tabs={getMxDatasetTabs(dataset, true)}
      title={getDatasetName(dataset)}
    />
  );
}
