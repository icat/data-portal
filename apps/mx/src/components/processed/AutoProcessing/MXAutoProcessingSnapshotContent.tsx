import { getDatasetParamValue, FilterBadges } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAutoProcessingDatasetSummary } from 'components/processed/AutoProcessing/MXAutoProcessingDatasetSummary';
import { MXUnitCellInfo } from 'components/processed/AutoProcessing/MXUnitCellInfo';
import { PROCESSED_PROGRAM_PARAM } from 'constants/metadatakeys';
import { getUnmatchedCutoffs, getCutoffFilterBadgeItem } from 'helpers/cutoff';
import { isMergedAutoProcessing } from 'helpers/processed';
import { useMXSettings } from 'hooks/mxSettings';
import { Badge } from 'react-bootstrap';

export function MXAutoProcessingSnapshotContent({
  dataset,
}: {
  dataset: Dataset;
}) {
  const [settings, setSettings] = useMXSettings();

  const cutoffs = settings.cutoffs;

  const unmatchedCutoffs = getUnmatchedCutoffs(dataset, cutoffs);

  return (
    <div className="d-flex flex-column justify-content-center text-center w-100">
      <i>
        From{' '}
        {getDatasetParamValue(dataset, PROCESSED_PROGRAM_PARAM) ||
          'unknown program'}
      </i>
      {isMergedAutoProcessing(dataset) && (
        <span>
          <Badge bg="info">Merged</Badge>
        </span>
      )}
      {unmatchedCutoffs?.length ? (
        <FilterBadges
          gap={1}
          centered
          filters={unmatchedCutoffs.map(({ stat, shell }) =>
            getCutoffFilterBadgeItem(stat, shell, settings, setSettings),
          )}
        />
      ) : null}
      <MXUnitCellInfo dataset={dataset} />
      <MXAutoProcessingDatasetSummary dataset={dataset} />
    </div>
  );
}
