import {
  convertToFixed,
  CopyValue,
  DatasetActionsButton,
  DatasetTypeIcon,
  FilterBadges,
  formatDateToDayAndShortTime,
  getDatasetParamValue,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAutoProcessingStatValue } from 'components/processed/AutoProcessing/MXAutoProcessingStatValue';
import { MXAutoProcessingWindow } from 'components/processed/AutoProcessing/MXAutoProcessingWindow';
import {
  MX_AUTOPROCESSING_SPACE_GROUP,
  PROCESSED_PROGRAM_PARAM,
  MX_AUTOPROCESSING_CELL_PREFIX,
} from 'constants/metadatakeys';
import { MX_SHELLS, MX_SHELL_COLORS } from 'constants/shell';
import {
  MXAutoProcessingStat,
  MX_AUTOPROCESSING_STATS,
  MX_AUTOPROCESSING_STAT_LABELS,
} from 'constants/stats';
import { getCutoffFilterBadgeItem, getUnmatchedCutoffs } from 'helpers/cutoff';
import { isMergedAutoProcessing } from 'helpers/processed';
import { getSpaceGroup } from 'helpers/spacegroup';
import { useRankedAutoProcessing } from 'hooks/autoProcessingRanking';
import { useMXSettings } from 'hooks/mxSettings';
import { useRef, useState } from 'react';
import { Badge, Table } from 'react-bootstrap';

export function CompactMXAutoProcessingTable({
  datasets,
  merged,
}: {
  datasets: Dataset[];
  merged: 'only' | 'exclude' | 'include';
}) {
  return (
    <div
      style={{
        maxHeight: 300,
        overflowY: 'auto',
        fontSize: 'smaller',
      }}
    >
      <MXAutoProcessingTable datasets={datasets} merged={merged} />
    </div>
  );
}

export function MXAutoProcessingTable({
  datasets,
  filterValid = false,
  merged,
}: {
  datasets: Dataset[];
  filterValid?: boolean;
  merged: 'only' | 'exclude' | 'include';
}) {
  const autoProcessings = useRankedAutoProcessing(
    datasets,
    merged,
    filterValid,
  );

  return (
    <Table
      size="sm"
      style={{
        margin: 0,
        textAlign: 'center',
        verticalAlign: 'middle',
      }}
      responsive
      hover
    >
      <thead>
        <tr>
          <th />
          <th />
          <th>Program</th>
          <th>a,b,c (Å)</th>
          <th>α,β,γ (°)</th>
          <th />
          {MX_AUTOPROCESSING_STATS.map((stat) => (
            <th key={stat}>{MX_AUTOPROCESSING_STAT_LABELS[stat]}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {autoProcessings.map((dataset) => {
          return <DatasetRow key={dataset.id} dataset={dataset} />;
        })}
      </tbody>
    </Table>
  );
}

function DatasetRow({ dataset }: { dataset: Dataset }) {
  const spacegroup = getSpaceGroup(
    getDatasetParamValue(dataset, MX_AUTOPROCESSING_SPACE_GROUP),
  )?.name;

  const program = getDatasetParamValue(dataset, PROCESSED_PROGRAM_PARAM);

  const ref = useRef<HTMLTableRowElement>(null);

  const [showWindow, setShowWindow] = useState(false);

  const [settings, setSettings] = useMXSettings();

  const cutoffs = settings.cutoffs;

  const unmatchedCutoffs = getUnmatchedCutoffs(dataset, cutoffs);

  const isValid = unmatchedCutoffs.length === 0;

  const tdStyle = {
    backgroundColor: 'transparent',
  };

  return (
    <>
      <tr
        key={dataset.id}
        ref={ref}
        onClick={() => {
          setShowWindow((v) => !v);
        }}
        style={{
          cursor: 'pointer',
          backgroundColor: isValid ? undefined : '#ffd5d5',
        }}
      >
        <td style={tdStyle}>
          <div
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <DatasetActionsButton dataset={dataset} />
          </div>
        </td>
        <td>
          <div
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <CopyValue
              type="icon"
              value={dataset.location}
              label="dataset path"
            />
            <DatasetTypeIcon dataset={dataset}></DatasetTypeIcon>
          </div>
        </td>
        <td style={tdStyle}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <span className="monospace">
              {formatDateToDayAndShortTime(dataset.startDate)}
            </span>
            <strong>{program || 'unknown'}</strong>
            <span>{spacegroup}</span>
            {isMergedAutoProcessing(dataset) && (
              <span>
                <Badge bg="info">Merged</Badge>
              </span>
            )}
            {unmatchedCutoffs?.length ? (
              <small>
                <FilterBadges
                  gap={1}
                  centered
                  filters={unmatchedCutoffs.map(({ stat, shell }) =>
                    getCutoffFilterBadgeItem(
                      stat,
                      shell,
                      settings,
                      setSettings,
                    ),
                  )}
                />
              </small>
            ) : null}
          </div>
        </td>
        {[
          ['a', 'b', 'c'],
          ['alpha', 'beta', 'gamma'],
        ].map((cellGroup) => (
          <td key={cellGroup.join()} style={tdStyle}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                whiteSpace: 'nowrap',
              }}
            >
              {cellGroup.map((cell) => (
                <span key={cell}>
                  {convertToFixed(
                    getDatasetParamValue(
                      dataset,
                      `${MX_AUTOPROCESSING_CELL_PREFIX}_${cell}`,
                    ),
                    1,
                    '-',
                  )}
                </span>
              ))}
            </div>
          </td>
        ))}
        <td style={tdStyle}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              whiteSpace: 'nowrap',
            }}
          >
            {MX_SHELLS.map((shell) => (
              <span
                key={shell}
                style={{
                  color: MX_SHELL_COLORS[shell],
                }}
              >
                {shell}
              </span>
            ))}
          </div>
        </td>
        {MX_AUTOPROCESSING_STATS.map((stat) => (
          <td key={stat} style={tdStyle}>
            <DatasetShellStatInfo dataset={dataset} stat={stat} />
          </td>
        ))}
      </tr>
      {showWindow && (
        <MXAutoProcessingWindow
          dataset={dataset}
          onClose={() => {
            setShowWindow(false);
          }}
        />
      )}
    </>
  );
}

function DatasetShellStatInfo({
  dataset,
  stat,
}: {
  dataset: Dataset;
  stat: MXAutoProcessingStat;
}) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        whiteSpace: 'nowrap',
      }}
    >
      {MX_SHELLS.map((shell) => {
        return (
          <MXAutoProcessingStatValue
            key={shell}
            dataset={dataset}
            shell={shell}
            stat={stat}
          />
        );
      })}
    </div>
  );
}
