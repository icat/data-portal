import { convertToFixed, getDatasetParamValue } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MX_AUTOPROCESSING_SPACE_GROUP,
  MX_AUTOPROCESSING_CELL_PREFIX,
} from 'constants/metadatakeys';

import { getSpaceGroup } from 'helpers/spacegroup';
import { Col, Row } from 'react-bootstrap';

function formatCell(value: any) {
  return convertToFixed(value, 1);
}

export function MXUnitCellInfo({ dataset }: { dataset: Dataset }) {
  const spaceGroup = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_SPACE_GROUP,
  );
  const spacegroupInfo = getSpaceGroup(spaceGroup);

  if (!spacegroupInfo) return null;

  let args: cellShowParams = spacegroupInfo?.crystalSystem
    ? {
        Triclinic: {},
        Monoclinic: { show_cell_alpha: false, show_cell_gamma: false },
        Orthorhombic: {
          show_cell_alpha: false,
          show_cell_beta: false,
          show_cell_gamma: false,
        },
        Tetragonal: {
          show_cell_b: false,
          show_cell_alpha: false,
          show_cell_beta: false,
          show_cell_gamma: false,
        },
        Trigonal: {
          show_cell_b: false,
          show_cell_alpha: false,
          show_cell_beta: false,
          show_cell_gamma: false,
        },
        Hexagonal: {
          show_cell_b: false,
          show_cell_alpha: false,
          show_cell_beta: false,
          show_cell_gamma: false,
        },
        Cubic: {
          show_cell_b: false,
          show_cell_c: false,
          show_cell_alpha: false,
          show_cell_beta: false,
          show_cell_gamma: false,
        },
      }[spacegroupInfo.crystalSystem]
    : {};

  //special case trigonal starts by R
  if (
    spacegroupInfo?.crystalSystem === 'Trigonal' &&
    spacegroupInfo.name.startsWith('R')
  ) {
    args = {
      show_cell_b: false,
      show_cell_c: false,
      show_cell_beta: false,
      show_cell_gamma: false,
    };
  }

  const cell_a = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_CELL_PREFIX + '_a',
  );
  const cell_b = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_CELL_PREFIX + '_b',
  );
  const cell_c = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_CELL_PREFIX + '_c',
  );
  const cell_alpha = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_CELL_PREFIX + '_alpha',
  );
  const cell_beta = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_CELL_PREFIX + '_beta',
  );
  const cell_gamma = getDatasetParamValue(
    dataset,
    MX_AUTOPROCESSING_CELL_PREFIX + '_gamma',
  );

  return (
    <div>
      <div className={'text-center'}>
        {spacegroupInfo?.crystalSystem ? (
          <span>
            <strong>
              {spacegroupInfo?.crystalSystem} system ({spacegroupInfo.name})
            </strong>
          </span>
        ) : null}
      </div>
      <div>
        <UnitCellTable
          cell_a={cell_a}
          cell_b={cell_b}
          cell_c={cell_c}
          cell_alpha={cell_alpha}
          cell_beta={cell_beta}
          cell_gamma={cell_gamma}
          {...args}
        />
      </div>
    </div>
  );
}

type cellShowParams = {
  show_cell_a?: boolean;
  show_cell_b?: boolean;
  show_cell_c?: boolean;
  show_cell_alpha?: boolean;
  show_cell_beta?: boolean;
  show_cell_gamma?: boolean;
};

function UnitCellTable({
  cell_a,
  cell_b,
  cell_c,
  cell_alpha,
  cell_beta,
  cell_gamma,
  show_cell_a = true,
  show_cell_b = true,
  show_cell_c = true,
  show_cell_alpha = true,
  show_cell_beta = true,
  show_cell_gamma = true,
}: {
  cell_a: number | string | undefined;
  cell_b: number | string | undefined;
  cell_c: number | string | undefined;
  cell_alpha: number | string | undefined;
  cell_beta: number | string | undefined;
  cell_gamma: number | string | undefined;
} & cellShowParams) {
  const colStyle = {
    whiteSpace: 'nowrap',
  } as const;
  return (
    <small>
      <Col>
        <Row className="g-2">
          {show_cell_a && (
            <Col style={colStyle}>
              <strong>
                a{!show_cell_b && '=b'}
                {!show_cell_c && '=c'}=
              </strong>
              <span>{formatCell(cell_a) || cell_a} Å</span>
            </Col>
          )}
          {show_cell_b && (
            <Col style={colStyle}>
              <strong>b=</strong>
              <span>{formatCell(cell_b) || cell_b} Å</span>
            </Col>
          )}
          {show_cell_c && (
            <Col style={colStyle}>
              <strong>c=</strong>
              <span>{formatCell(cell_c) || cell_c} Å</span>
            </Col>
          )}
        </Row>

        {(show_cell_alpha || show_cell_beta || show_cell_gamma) && (
          <Row className="g-2">
            {show_cell_alpha && (
              <Col style={colStyle}>
                <strong>
                  α{!show_cell_beta && '=β'}
                  {!show_cell_gamma && '=γ'}=
                </strong>
                <span>{formatCell(cell_alpha) || cell_alpha} °</span>
              </Col>
            )}
            {show_cell_beta && (
              <Col style={colStyle}>
                <strong>β=</strong>
                <span>{formatCell(cell_beta) || cell_beta} °</span>
              </Col>
            )}
            {show_cell_gamma && (
              <Col style={colStyle}>
                <strong>γ=</strong>
                <span>{formatCell(cell_gamma) || cell_gamma} °</span>
              </Col>
            )}
          </Row>
        )}
      </Col>
    </small>
  );
}
