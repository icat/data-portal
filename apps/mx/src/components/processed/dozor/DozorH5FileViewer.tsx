import { Loading, Window } from '@edata-portal/core';
import { MXH5ImageViewer } from 'components/image/MXH5ImageViewer';
import { Suspense } from 'react';

interface Props {
  selectedImage: number;
  onClose: () => void;
  inputDatasetId: string;
}

export function DozorH5FileViewer(props: Props) {
  const { selectedImage, onClose, inputDatasetId } = props;

  const title = `Image #${selectedImage + 1}`;

  return (
    <Window title={title} onClose={onClose}>
      <Suspense fallback={<Loading />}>
        <MXH5ImageViewer
          datasetId={inputDatasetId}
          imageIndex={selectedImage}
        />
      </Suspense>
    </Window>
  );
}
