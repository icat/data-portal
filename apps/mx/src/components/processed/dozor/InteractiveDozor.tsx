import { Loading, PlotWidget } from '@edata-portal/core';
import { getDozorPlotConfig } from 'components/processed/dozor/dozorHelper';
import { Suspense, useState } from 'react';

import { Alert } from 'react-bootstrap';
import { DozorH5FileViewer } from 'components/processed/dozor/DozorH5FileViewer';

export function InteractiveDozor({
  inputDatasetId,
  data,
}: {
  inputDatasetId: string;
  data: number[][];
}) {
  const plotConfig = getDozorPlotConfig(data);
  const [selectedImage, setSelectedImage] = useState<number>();

  return (
    <Suspense fallback={<Loading />}>
      <div className="d-flex flex-column gap-2">
        <PlotWidget
          {...plotConfig}
          onClick={(evt) => {
            setSelectedImage(evt.points[0].pointNumber);
          }}
        />

        <Alert variant="info">
          Click on the plot to visualize collected images.
        </Alert>

        {selectedImage !== undefined ? (
          <DozorH5FileViewer
            selectedImage={selectedImage}
            onClose={() => setSelectedImage(undefined)}
            inputDatasetId={inputDatasetId}
          />
        ) : null}
      </div>
    </Suspense>
  );
}
