import { Gallery } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

import { getDozorPlotCsv } from 'components/processed/dozor/dozorHelper';
import { DozorPlotFromCSV } from 'components/processed/dozor/DozorPlotFromCSV';

export function MXDozorPlot({ dataset }: { dataset: Dataset }) {
  const csv = getDozorPlotCsv(dataset);

  if (!csv) return <Gallery dataset={dataset} />;

  return <DozorPlotFromCSV dataset={dataset} csv={csv} />;
}
