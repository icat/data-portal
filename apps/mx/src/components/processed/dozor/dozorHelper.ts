import { PlotWidgetProps, getGalleryItemByName } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import range from 'lodash/range';
import unzip from 'lodash/unzip';

export function getDozorPlotCsv(dataset: Dataset) {
  const csv = getGalleryItemByName(dataset, '.csv');
  return csv;
}

const SELECTED_STYLE = {
  selected: { marker: { opacity: 0.2 } },
  unselected: { marker: { opacity: 1 } },
};

export function getDozorPlotConfig(
  data: number[][],
  excludeRanges?: [number, number][],
): PlotWidgetProps {
  const [imageNumbers, angles, spotsCount, mainScores, , resolutions] =
    unzip(data);

  const [imageMin, imageMax] = [
    Math.min(...imageNumbers),
    Math.max(...imageNumbers),
  ];
  const [angleMin, angleMax] = [Math.min(...angles), Math.max(...angles)];
  const [resolutionMin, resolutionMax] = [
    Math.min(...resolutions, 1),
    Math.max(...resolutions.filter((v) => v <= 10), 4.5),
  ];
  const [yMin, yMax] = [
    Math.min(...spotsCount, ...mainScores),
    Math.max(...spotsCount, ...mainScores),
  ];

  const selections = excludeRanges?.map((r) => ({
    type: 'rect',
    xref: 'x',
    yref: 'y',
    line: { width: 1, dash: 'dot', color: 'red' },
    x0: r[0],
    x1: r[1],
    y0: yMin,
    y1: yMax,
  }));

  const selectedPointsIndexes = excludeRanges?.flatMap((r) =>
    range(r[0], r[1] + 1),
  );

  return {
    compact: true,
    layout: {
      dragmode: selections ? 'select' : 'pan',
      hovermode: 'x unified',
      ...(selections && {
        selections,
        selectdirection: 'h',
        selectionrevision: JSON.stringify(excludeRanges),
      }),
      xaxis: {
        title: 'Image number',
        type: 'linear',
        side: 'bottom',
        range: [imageMin, imageMax],
        showline: true,
        showgrid: false,
        zeroline: false,
      },
      xaxis2: {
        title: 'Angle (deg)',
        type: 'linear',
        side: 'top',
        range: [angleMin, angleMax],
        showline: true,
        showgrid: true,
        zeroline: false,
        overlaying: 'x',
      },
      yaxis: {
        title: 'Number of spots / Dozor score (*10)',
        type: 'linear',
        side: 'left',
        range: [yMin, yMax],
        showline: true,
        showgrid: false,
        zeroline: false,
      },
      yaxis2: {
        title: 'Resolution (Å)',
        type: 'linear',
        side: 'right',
        range: [resolutionMax, resolutionMin],
        showline: true,
        showgrid: true,
        zeroline: false,
        overlaying: 'y',
      },
    },
    data: [
      {
        x: imageNumbers,
        y: spotsCount,
        ...(selections && SELECTED_STYLE),
        type: 'scattergl',
        mode: 'markers',
        marker: { color: 'gold' },
        name: 'Number of spots',
        xaxis: 'x',
        yaxis: 'y',
      },
      {
        x: imageNumbers,
        y: mainScores,
        ...(selections && SELECTED_STYLE),
        type: 'scattergl',
        mode: 'markers',
        marker: { color: 'lightblue' },
        name: 'ExecDozor score',
        xaxis: 'x',
        yaxis: 'y',
      },
      {
        x: imageNumbers,
        y: resolutions,
        ...(selections && SELECTED_STYLE),
        type: 'scattergl',
        mode: 'markers',
        marker: { color: 'purple' },
        name: 'Visible resolution',
        xaxis: 'x',
        yaxis: 'y2',
        selectedpoints: selectedPointsIndexes,
      },
      //dummy data to make sure xaxis2 is visible because plotly will not show an axis without data
      {
        x: [angles[0]],
        y: [spotsCount[0]],
        type: 'scattergl',
        mode: 'markers',
        marker: { color: 'transparent' },
        name: '',
        xaxis: 'x2',
        yaxis: 'y',
        showlegend: false,
      },
    ],
  };
}

export function rangeToString(range: [number, number][]) {
  return range
    .map((r) => {
      const [x0, x1] = r;
      return `${x0}-${x1}`;
    })
    .join(', ');
}
export function rangeFromString(range?: string) {
  if (!range?.length) return [];
  return range
    .split(',')
    .map((r) => {
      const [x0, x1] = r
        .split('-')
        .filter((v) => v)
        .map((x) => x.trim());
      if (!x0 || Number.isNaN(Number(x0))) return null;
      const nb0 = Number(x0);
      const nb1 = Number(x1);
      if (Number.isNaN(nb1)) return [nb0, nb0] as const;
      return [Math.min(nb0, nb1), Math.max(nb0, nb1)] as const;
    })
    .filter((r): r is [number, number] => r !== null)
    .sort((a, b) => a[0] - b[0]);
}
