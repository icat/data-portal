import {
  EditableText,
  Loading,
  PlotSelectionEvent,
  PlotWidget,
} from '@edata-portal/core';
import {
  getDozorPlotConfig,
  rangeFromString,
  rangeToString,
} from 'components/processed/dozor/dozorHelper';
import { Suspense, useCallback } from 'react';
import { Card, Col, Row } from 'react-bootstrap';

interface Props {
  data: number[][];
  selectedExcludeRanges?: string;
  onSelectExcludeRanges?(ranges: string): void;
}

export function InteractiveDozorExcludeSelection(props: Props) {
  const { data, selectedExcludeRanges = '', onSelectExcludeRanges } = props;

  const plotConfig = getDozorPlotConfig(
    data,
    rangeFromString(selectedExcludeRanges),
  );

  const handleSelection = useCallback(
    (e: PlotSelectionEvent) => {
      if (!('selections' in e) || !Array.isArray(e.selections)) return;

      const ranges = e.selections
        .map((s) => {
          const { x0, x1 } = s;
          if (Number.isNaN(x0) || Number.isNaN(x1)) return null;
          const rx0 = Math.floor(Math.min(x0, x1));
          const rx1 = Math.floor(Math.max(x0, x1));
          return [rx0, rx1];
        })
        .filter((r): r is [number, number] => r !== null)
        .filter((r, i, a) => {
          return a.findIndex((r2) => r2[0] === r[0] && r2[1] === r[1]) === i;
        })
        .sort((a, b) => a[0] - b[0]);

      onSelectExcludeRanges?.(rangeToString(ranges));
    },
    [onSelectExcludeRanges],
  );

  return (
    <Suspense fallback={<Loading />}>
      <Card>
        <Card.Body className="p-2">
          <Row className="g-1">
            <Col xs={12}>
              <EditableText
                value={selectedExcludeRanges || ''}
                label="Exclude ranges"
                showLabel={false}
                placeholder='e.g. "50-50, 100-200, 300-400"'
                onSave={(value) => {
                  if (!onSelectExcludeRanges) return;
                  onSelectExcludeRanges(value);
                }}
                saveOnBlur={true}
              />
              <small>
                <strong>Supported format:</strong> {'"50-50, 100-200, 300-400"'}
                .
                <br /> You can edit the range in text or select it on the graph
                below. Hold <strong>SHIFT</strong> for multi selection.
              </small>
            </Col>
            <Col xs={12}>
              <Suspense fallback={<Loading />}>
                <PlotWidget
                  {...plotConfig}
                  onSelect={onSelectExcludeRanges && handleSelection}
                />
              </Suspense>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Suspense>
  );
}
