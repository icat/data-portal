import {
  GalleryItem,
  Loading,
  NoData,
  getDatasetParamValue,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  DOWNLOAD_FILE_ENDPOINT,
  Dataset,
} from '@edata-portal/icat-plus-api';
import { useEffect, useState } from 'react';

import Papa from 'papaparse';

import { InteractiveDozorExcludeSelection } from 'components/processed/dozor/InteractiveDozorExcludeSelection';
import { InteractiveDozor } from 'components/processed/dozor/InteractiveDozor';

export function DozorPlotFromCSV({
  dataset,
  csv,
  selectedExcludeRanges,
  onSelectExcludeRanges,
}: {
  dataset: Dataset;
  csv: GalleryItem;
  selectedExcludeRanges?: string;
  onSelectExcludeRanges?(ranges: string): void;
}) {
  const data = useGetEndpoint({
    endpoint: DOWNLOAD_FILE_ENDPOINT,
    params: {
      resourceId: csv?.id,
    },
  });

  const [parsed, setParsed] = useState<number[][] | undefined>(undefined);

  useEffect(() => {
    if (data)
      Papa.parse<number[]>(data, {
        comments: '#',
        header: false,
        worker: true,
        complete: function (results, file) {
          setParsed(results.data);
        },
        dynamicTyping: true,
        skipEmptyLines: true,
      });
    return () => {
      setParsed(undefined);
    };
  }, [data]);

  if (!data) return <NoData />;

  if (!parsed) return <Loading />;

  if (!parsed.length || !parsed[0].length || !parsed[0].every((n) => !isNaN(n)))
    return <NoData />;

  if (onSelectExcludeRanges)
    return (
      <InteractiveDozorExcludeSelection
        data={parsed}
        selectedExcludeRanges={selectedExcludeRanges}
        onSelectExcludeRanges={onSelectExcludeRanges}
      />
    );

  const inputDatasetId = getDatasetParamValue(dataset, 'input_datasetIds');

  if (!inputDatasetId) return <NoData />;

  return <InteractiveDozor inputDatasetId={inputDatasetId} data={parsed} />;
}
