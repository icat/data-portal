import { Button, FilterBadges } from '@edata-portal/core';
import { MX_SHELLS, MX_SHELL_LABELS } from 'constants/shell';
import { MX_AUTOPROCESSING_STATS } from 'constants/stats';
import {
  getCutOffsFilterBadgeItems,
  getCutoffLabel,
  getCutoffValue,
  setCutOffValue,
} from 'helpers/cutoff';
import { MXSettings, useMXSettings } from 'hooks/mxSettings';
import { useState } from 'react';
import { Col, Form, Modal, Row, Table } from 'react-bootstrap';

export function MXSettingsCutoffs() {
  const [settings, setSettings] = useMXSettings();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <small>
        <FilterBadges
          gap={1}
          filters={getCutOffsFilterBadgeItems(settings, setSettings)}
        />
      </small>
      <Row>
        <Col xs={12}>
          <Button
            size="sm"
            onClick={() => {
              setShowModal(true);
            }}
            variant="secondary"
            className="px-2 py-0 mt-2"
          >
            <small>Add...</small>
          </Button>
        </Col>
      </Row>
      {showModal && (
        <ShowModal
          onClose={() => {
            setShowModal(false);
          }}
        />
      )}
    </>
  );
}

function ShowModal({ onClose }: { onClose: () => void }) {
  const [settings, updateSetting] = useMXSettings();

  const [localCutoffs, setLocalCutoffs] = useState<
    NonNullable<MXSettings['cutoffs']>
  >(settings.cutoffs || {});

  return (
    <Modal show onHide={onClose} backdrop={'static'} size="lg" centered>
      <Modal.Header closeButton>
        <Modal.Title>Cutoff Configuration</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Col>
          <Table hover responsive>
            <thead>
              <tr>
                <th></th>
                {MX_SHELLS.map((shell) => {
                  return (
                    <th className="text-center" key={shell}>
                      {MX_SHELL_LABELS[shell]}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody>
              {MX_AUTOPROCESSING_STATS.map((stat) => {
                return (
                  <tr key={stat}>
                    <td
                      style={{
                        verticalAlign: 'middle',
                      }}
                      className="text-right"
                    >
                      <strong>{getCutoffLabel(stat)}</strong>
                    </td>
                    {MX_SHELLS.map((shell) => {
                      const cutoffValue = getCutoffValue(
                        localCutoffs,
                        stat,
                        shell,
                      );
                      const onChange = (
                        e: React.ChangeEvent<HTMLInputElement>,
                      ) => {
                        setLocalCutoffs((prev) => {
                          const newValue = e.target.value?.length
                            ? Number(e.target.value)
                            : undefined;
                          return setCutOffValue(
                            prev,
                            stat,
                            shell,
                            Number.isNaN(newValue) ? undefined : newValue,
                          );
                        });
                      };

                      return (
                        <td key={shell}>
                          <Form.Control
                            type="number"
                            step={1}
                            value={cutoffValue}
                            onChange={onChange}
                            style={{
                              minWidth: '5em',
                            }}
                          />
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </Table>
          <Row className="g-2">
            <Col xs={'auto'}>
              <Button
                onClick={() => {
                  updateSetting({ cutoffs: localCutoffs });
                  onClose();
                }}
              >
                Apply
              </Button>
            </Col>
            <Col xs={'auto'}>
              <Button
                onClick={() => {
                  onClose();
                }}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </Col>
      </Modal.Body>
    </Modal>
  );
}
