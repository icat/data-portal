import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MXSettingsAutoProcessing } from 'components/settings/MXSettingsAutoprocessing';
import { useState } from 'react';

import { Modal } from 'react-bootstrap';

export function MXSettingsAutoProcessingHelper() {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <FontAwesomeIcon
        key={'Help'}
        className="text-muted"
        icon={faQuestionCircle}
        style={{ cursor: 'pointer' }}
        onClick={() => setShowModal(!showModal)}
      />
      <Modal show={showModal} onHide={() => setShowModal(false)} centered>
        <Modal.Header closeButton>
          <Modal.Title>How do we rank auto processing?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <MXSettingsAutoProcessing />
        </Modal.Body>
      </Modal>
    </>
  );
}
