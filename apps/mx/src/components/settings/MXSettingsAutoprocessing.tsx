import { REACT_SELECT_COMPACT_STYLES } from '@edata-portal/core';
import { MXSettingsCutoffs } from 'components/settings/MXSettingsCutoffs';
import { MXShell, MX_SHELLS, MX_SHELL_LABELS } from 'constants/shell';
import {
  MXAutoProcessingStat,
  MX_AUTOPROCESSING_STATS,
  MX_AUTOPROCESSING_STAT_LABELS,
} from 'constants/stats';

import { useMXSettings } from 'hooks/mxSettings';
import { FormCheck } from 'react-bootstrap';
import ReactSelect from 'react-select';

function getOptionFromStat(value: MXAutoProcessingStat) {
  return {
    value,
    label: MX_AUTOPROCESSING_STAT_LABELS[value],
  };
}
function getOptionFromShell(value: MXShell) {
  return {
    value,
    label: MX_SHELL_LABELS[value],
  };
}

export function MXSettingsAutoProcessing() {
  const [settings, updateSetting] = useMXSettings();
  return (
    <span>
      <i>We use the following criteria by order of priority:</i>
      <hr className="my-2" />
      <ol
        style={{
          paddingLeft: '1em',
        }}
      >
        <li>
          <strong>Matches all set filter cutoffs</strong>
          <br />
          <MXSettingsCutoffs />
        </li>
        <li>
          <strong>Highest symmetry space group</strong>
          <br />
          <i>
            <FormCheck
              type="switch"
              checked={!settings.autoProcessingRankingDisableSpaceGroup}
              onChange={(e) => {
                updateSetting({
                  ...settings,
                  autoProcessingRankingDisableSpaceGroup: !e.target.checked,
                });
              }}
              label={
                settings.autoProcessingRankingDisableSpaceGroup
                  ? 'Disabled'
                  : 'Enabled'
              }
            />
          </i>
        </li>
        <li>
          <strong>Selected criteria</strong>
          <br />
          <div className="d-flex flex-row align-items-center gap-1 justify-content-stretch flex-wrap">
            <ReactSelect
              value={getOptionFromShell(settings.autoProcessingRankingShell)}
              options={MX_SHELLS.map(getOptionFromShell)}
              onChange={(option) => {
                if (!option?.value) return;
                updateSetting({
                  ...settings,
                  autoProcessingRankingShell: option.value,
                });
              }}
              styles={{
                container: (provided) => ({
                  ...provided,
                  flex: 1,
                }),
                ...REACT_SELECT_COMPACT_STYLES,
              }}
            />
            <ReactSelect
              value={getOptionFromStat(settings.autoProcessingRankingParameter)}
              options={MX_AUTOPROCESSING_STATS.map(getOptionFromStat)}
              onChange={(option) => {
                if (!option?.value) return;
                updateSetting({
                  ...settings,
                  autoProcessingRankingParameter: option.value,
                });
              }}
              styles={{
                container: (provided) => ({
                  ...provided,
                  flex: 1,
                }),
                ...REACT_SELECT_COMPACT_STYLES,
              }}
            />
          </div>
        </li>
      </ol>
    </span>
  );
}
