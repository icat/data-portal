import { Button, prettyPrintScanType, SideNavFilter } from '@edata-portal/core';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  hasMXViewFilter,
  isDefaultMXViewFilters,
  MX_VIEW_FILTERS,
  setMXViewFilters,
  useMXSettings,
} from 'hooks/mxSettings';
import { useEffect, useMemo, useRef, useState } from 'react';
import { Form, FormCheck } from 'react-bootstrap';

export function MXViewFilterSettings() {
  const [settings, updateSetting] = useMXSettings();
  return (
    <SideNavFilter
      label={'Show snapshots for'}
      onClear={() => {
        updateSetting({
          ...settings,
          scanTypeFilters: [],
        });
      }}
      hasValue={!isDefaultMXViewFilters(settings)}
    >
      <div className="d-flex flex-column gap-1">
        {Object.keys(MX_VIEW_FILTERS).map((category) => {
          return (
            <MXViewFilterSettingCategory
              key={category}
              category={category as keyof typeof MX_VIEW_FILTERS}
            />
          );
        })}
      </div>
    </SideNavFilter>
  );
}

function MXViewFilterSettingCategory({
  category,
}: {
  category: keyof typeof MX_VIEW_FILTERS;
}) {
  const [settings, updateSetting] = useMXSettings();

  const filters = MX_VIEW_FILTERS[category as keyof typeof MX_VIEW_FILTERS];

  const anyTrue = useMemo(() => {
    return filters.some((filter) => {
      return hasMXViewFilter(category, filter, settings);
    });
  }, [category, filters, settings]);

  const allTrue = useMemo(() => {
    return filters.every((filter) => {
      return hasMXViewFilter(category, filter, settings);
    });
  }, [category, filters, settings]);

  const [expanded, setExpanded] = useState(anyTrue && !allTrue);

  const ref = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (!ref.current) return;
    if (allTrue) {
      ref.current.indeterminate = false;
    } else if (anyTrue) {
      ref.current.indeterminate = true;
    } else {
      ref.current.indeterminate = false;
    }
  }, [allTrue, anyTrue]);

  return (
    <div className="d-flex flex-row align-items-top gap-2">
      <FormCheck
        type="checkbox"
        checked={allTrue}
        ref={ref}
        onChange={(e) => {
          setMXViewFilters(
            category,
            [...filters],
            e.target.checked,
            settings,
            updateSetting,
          );
        }}
      />
      <div className="d-flex flex-column">
        <div className="d-flex flex-row align-items-center gap-1">
          <Button
            className="p-0"
            variant="link"
            onClick={(e) => {
              setExpanded((v) => !v);
            }}
          >
            <FontAwesomeIcon icon={expanded ? faAngleDown : faAngleRight} />
          </Button>
          {prettyPrintScanType(category)}
        </div>
        {expanded && (
          <>
            {filters.map((filter) => {
              return (
                <Form.Check
                  style={{ marginBottom: 0 }}
                  key={filter}
                  type="checkbox"
                  label={filter}
                  checked={hasMXViewFilter(category, filter, settings)}
                  onChange={(e) => {
                    setMXViewFilters(
                      category,
                      [filter],
                      e.target.checked,
                      settings,
                      updateSetting,
                    );
                  }}
                />
              );
            })}
          </>
        )}
      </div>
    </div>
  );
}
