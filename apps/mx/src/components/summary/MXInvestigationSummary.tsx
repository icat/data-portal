import { SCAN_TYPE_PARAM } from '@edata-portal/core';
import {
  DATASET_PARAMETERS_ENDPOINT,
  useMultiGetEndpoint,
  Investigation,
  DatasetParameterSearchResult,
} from '@edata-portal/icat-plus-api';
import { MXAutoProcessingStatColorScaleSelect } from 'components/summary/MXAutoProcessingStatColorScaleSelect';
import {
  MX_AUTOPROCESSING_SPACE_GROUP,
  MX_SAMPLE_CONTAINER_ID_PARAM,
  MX_SAMPLE_CONTAINER_POSITION_PARAM,
} from 'constants/metadatakeys';
import { getMXAutoProcessingStatParam } from 'helpers/processed';
import { useMXSettings } from 'hooks/mxSettings';
import { useMemo } from 'react';
import { Col, Row } from 'react-bootstrap';
import { MXInvestigationSummaryContainer } from 'components/summary/MXInvestigationSummaryContainer';
import { SampleCircleLegend } from 'components/sample/summary/SampleCircleLegend';
import {
  getSampleContainersFromMetadata,
  groupSamplesByContainerId,
} from 'components/summary/helper';
import { MXInvestigationSummaryStatistics } from 'components/summary/MXInvestigationSummaryStatistics';

export function MXInvestigationSummary({
  investigation,
  filterContainerId,
  selectedSampleId,
}: {
  investigation: Investigation;
  filterContainerId?: string;
  selectedSampleId?: number;
}) {
  const [settings] = useMXSettings();

  const statKey = getMXAutoProcessingStatParam(
    settings.autoProcessingRankingShell,
    settings.autoProcessingRankingParameter,
  );

  const [
    parametersSpaceGroup,
    parametersStat,
    parametersScanType,
    parametersContainerId,
    parametersContainerPosition,
  ] = useMultiGetEndpoint({
    endpoint: DATASET_PARAMETERS_ENDPOINT,
    params: [
      {
        investigationId: investigation.id.toString(),
        name: MX_AUTOPROCESSING_SPACE_GROUP,
      },
      {
        investigationId: investigation.id.toString(),
        name: statKey,
      },
      {
        investigationId: investigation.id.toString(),
        name: SCAN_TYPE_PARAM,
      },
      {
        investigationId: investigation.id.toString(),
        name: MX_SAMPLE_CONTAINER_ID_PARAM,
      },
      {
        investigationId: investigation.id.toString(),
        name: MX_SAMPLE_CONTAINER_POSITION_PARAM,
      },
    ],
    default: [] as DatasetParameterSearchResult[],
  });

  const datasetSamples = useMemo(
    () =>
      getSampleContainersFromMetadata(
        investigation.id,
        parametersSpaceGroup,
        parametersStat,
        parametersScanType,
        parametersContainerId,
        parametersContainerPosition,
      ),
    [
      investigation.id,
      parametersSpaceGroup,
      parametersStat,
      parametersScanType,
      parametersContainerId,
      parametersContainerPosition,
    ],
  );

  const containers = groupSamplesByContainerId(datasetSamples.samples);
  return (
    <Col>
      <Row>
        <Col>
          <MXAutoProcessingStatColorScaleSelect
            datasets={datasetSamples.datasets}
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <SampleCircleLegend
            selectedSampleId={selectedSampleId !== undefined}
          />
        </Col>
      </Row>

      <MXInvestigationSummaryStatistics samples={datasetSamples.samples} />
      {Object.keys(containers)
        .filter(
          (container) => !filterContainerId || container === filterContainerId,
        )
        .sort()
        .map((container) => {
          return (
            <Row key={container}>
              <Col>
                <MXInvestigationSummaryContainer
                  containerId={container}
                  containerSamples={containers[container].samples}
                  allDatasets={datasetSamples.datasets}
                  selectedSampleId={selectedSampleId}
                />
              </Col>
            </Row>
          );
        })}
    </Col>
  );
}
