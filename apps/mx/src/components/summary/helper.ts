import { getDatasetParamValue } from '@edata-portal/core';
import {
  DATASET_TYPE_PROCESSED,
  Dataset,
  DatasetParameterSearchResult,
} from '@edata-portal/icat-plus-api';

/**
 * This function groups the samples by container and returns a dictionary with the samples indexed by SampleTrackingContainer_id
 * @param sampleContainerList
 * @returns
 */
export function groupSamplesByContainerId(
  sampleContainerList: SampleContainer[],
) {
  const containers: {
    [id: string]: { datasets: Dataset[]; samples: SampleContainer[] };
  } = {};
  sampleContainerList.forEach((sample) => {
    const { datasets } = sample;
    if (datasets && datasets.length > 0) {
      const containerId =
        getFirstParameterWithValue(datasets, 'SampleTrackingContainer_id') ||
        'NA';

      if (!containers[containerId]) {
        containers[containerId] = { samples: [], datasets: [] };
      }
      containers[containerId].samples.push(sample);
      containers[containerId].datasets = containers[
        containerId
      ].datasets.concat(sample.datasets);
    }
  });
  return containers;
}

export interface SampleContainer {
  id: number;
  investigation: { id: number };
  datasets: Dataset[];
  name: string;
}

/**
 * This functions makes a data structure similar to the samples and datasets based on certain metadata
 * It returns and array of Samples (with populated datasets) and an array of datasets (with all datasets)
 *
 * @param investigationId
 * @param parametersSpaceGroup
 * @param parametersStat
 * @param parametersScanType
 * @param parametersContainerId
 * @param parametersContainerPosition
 * @returns
 */
export function getSampleContainersFromMetadata(
  investigationId: number,
  parametersSpaceGroup: DatasetParameterSearchResult[],
  parametersStat: DatasetParameterSearchResult[],
  parametersScanType: DatasetParameterSearchResult[],
  parametersContainerId: DatasetParameterSearchResult[],
  parametersContainerPosition: DatasetParameterSearchResult[],
): { samples: SampleContainer[]; datasets: Dataset[] } {
  const datasets: Dataset[] = [];
  let samples: SampleContainer[] = [];

  parametersScanType.forEach((paramScanType) => {
    const paramSpaceGroup = parametersSpaceGroup.find(
      (paramSpaceGroup) =>
        paramSpaceGroup.datasetId === paramScanType.datasetId,
    );
    const paramStat = parametersStat.find(
      (paramStat) => paramStat.datasetId === paramScanType.datasetId,
    );
    const paramContainerId = parametersContainerId.find(
      (paramContainerId) =>
        paramContainerId.datasetId === paramScanType.datasetId,
    );
    const paramContainerPosition = parametersContainerPosition.find(
      (paramContainerPosition) =>
        paramContainerPosition.datasetId === paramScanType.datasetId,
    );

    datasets.push({
      id: paramScanType.datasetId,
      sampleId: paramScanType.sampleId,
      type: DATASET_TYPE_PROCESSED,
      sampleName: paramScanType.sampleName,
      parameters: [
        ...(paramStat ? [paramStat] : []),
        ...(paramSpaceGroup ? [paramSpaceGroup] : []),
        ...(paramScanType ? [paramScanType] : []),
        ...(paramContainerId ? [paramContainerId] : []),
        ...(paramContainerPosition ? [paramContainerPosition] : []),
      ],
    } as any);
  });
  /** gets a list of the samples based on the dataset information */
  samples = datasets
    .map((dataset) => {
      const datasets: Dataset[] = [];
      return {
        id: dataset.sampleId,
        name: dataset.sampleName || 'NA',
        investigation: { id: investigationId },
        datasets,
      };
    })
    .filter(
      (value, index, self) =>
        index === self.findIndex((t) => t.id === value.id),
    );
  /** attach the datasets to the samples */
  samples.forEach((sample) => {
    sample.datasets = datasets.filter(
      (dataset) => dataset.sampleId === sample.id,
    );
  });

  return { samples, datasets };
}

export function getFirstParameterWithValue(
  datasets: Dataset[],
  parameterName: string,
) {
  return datasets
    .map((dataset) => getDatasetParamValue(dataset, parameterName))
    .find((value) => value !== undefined);
}
