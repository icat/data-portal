import { SampleContainer } from 'components/summary/helper';
import {
  isSampleAnalysed,
  isSampleCollected,
  isSampleProcessed,
} from 'helpers/sample';
import { useMemo } from 'react';

export function MXInvestigationSummaryStatistics({
  samples,
}: {
  samples: SampleContainer[];
}) {
  const analyzed = useMemo(
    () => samples.filter(isSampleAnalysed)?.length || 0,
    [samples],
  );
  const collected = useMemo(
    () => samples.filter(isSampleCollected)?.length || 0,
    [samples],
  );
  const autoprocessed = useMemo(
    () => samples.filter(isSampleProcessed)?.length || 0,
    [samples],
  );

  return (
    <div className="d-flex flex-row flex-wrap gap-3 align-items-center">
      <span>
        <strong>Analyzed: </strong>
        {analyzed}
      </span>
      <span>
        <strong>Collected: </strong>
        {collected}
      </span>
      <span>
        <strong>Autoprocessed: </strong>
        {autoprocessed}
      </span>
    </div>
  );
}
