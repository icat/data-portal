import { Button, RangeSelector } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MX_SHELL_LABELS } from 'constants/shell';
import { MX_AUTOPROCESSING_STAT_LABELS } from 'constants/stats';
import { getGradientForStat } from 'helpers/processed';
import { useMXAutoProcessingStatColorScale } from 'hooks/autoProcessingStatColorScale';
import { useMXSettings } from 'hooks/mxSettings';
import { useCallback, useMemo } from 'react';
import { Alert } from 'react-bootstrap';

export function MXAutoProcessingStatColorScaleSelect({
  datasets,
}: {
  datasets: Dataset[];
}) {
  const [settings] = useMXSettings();
  const scale = useMXAutoProcessingStatColorScale(datasets);

  const currentBad = scale.current.bad;
  const currentGood = scale.current.good;
  const defaultBad = scale.default.bad;
  const defaultGood = scale.default.good;

  const updateCurrentMax = useCallback(
    (v: number) => {
      if (defaultBad === undefined || defaultGood === undefined) return;
      const newValue =
        defaultBad > defaultGood
          ? {
              good: currentGood,
              bad: v,
            }
          : {
              good: v,
              bad: currentBad,
            };
      scale.setValue(newValue);
    },
    [currentBad, currentGood, defaultBad, defaultGood, scale],
  );
  const updateCurrentMin = useCallback(
    (v: number) => {
      if (defaultBad === undefined || defaultGood === undefined) return;

      const newValue =
        defaultBad > defaultGood
          ? {
              good: v,
              bad: currentBad,
            }
          : {
              good: currentGood,
              bad: v,
            };
      scale.setValue(newValue);
    },
    [currentBad, currentGood, defaultBad, defaultGood, scale],
  );

  const min =
    defaultGood !== undefined && defaultBad !== undefined
      ? Math.min(defaultBad, defaultGood)
      : undefined;
  const max =
    defaultGood !== undefined && defaultBad !== undefined
      ? Math.max(defaultBad, defaultGood)
      : undefined;

  const selectedMin =
    currentGood !== undefined && currentBad !== undefined
      ? Math.min(currentGood, currentBad)
      : undefined;
  const selectedMax =
    currentGood !== undefined && currentBad !== undefined
      ? Math.max(currentGood, currentBad)
      : undefined;

  const gradient = useMemo(
    () =>
      getGradientForStat(
        settings.autoProcessingRankingShell,
        settings.autoProcessingRankingParameter,
        scale.current,
      ),
    [
      settings.autoProcessingRankingShell,
      settings.autoProcessingRankingParameter,
      scale,
    ],
  );

  if (
    min === undefined ||
    max === undefined ||
    selectedMin === undefined ||
    selectedMax === undefined
  )
    return (
      <Alert variant="warning">
        Could not compute scale for selected parameter
      </Alert>
    );

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Button
        variant="link"
        onClick={() => {
          scale.setValue(undefined);
        }}
      >
        Reset color scale to defaults
      </Button>
      <span className="text-center">
        <strong>Selected statistics: </strong>
        {MX_SHELL_LABELS[settings.autoProcessingRankingShell]}{' '}
        {MX_AUTOPROCESSING_STAT_LABELS[settings.autoProcessingRankingParameter]}{' '}
      </span>
      <RangeSelector
        min={min}
        max={max}
        selectedMin={selectedMin}
        selectedMax={selectedMax}
        setSelectedMin={updateCurrentMin}
        setSelectedMax={updateCurrentMax}
        backgroundGradient={gradient}
        height={30}
      />
    </div>
  );
}
