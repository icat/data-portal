import { immutableArray } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXSampleCircle } from 'components/sample/summary/MXSampleCircle';
import {
  SampleContainer,
  getFirstParameterWithValue,
} from 'components/summary/helper';
import { MXInvestigationSummaryStatistics } from 'components/summary/MXInvestigationSummaryStatistics';
import { useMemo } from 'react';

export function MXInvestigationSummaryContainer({
  containerId,
  allDatasets,
  containerSamples,
  selectedSampleId,
}: {
  containerId: string;
  allDatasets: Dataset[];
  containerSamples: SampleContainer[];
  selectedSampleId?: number;
}) {
  const isNAContainer = containerId === 'NA';

  const sortedSamples = useMemo(
    () =>
      immutableArray(containerSamples)
        .sort((a, b) => {
          const vA = getFirstParameterWithValue(
            a.datasets,
            'SampleTrackingContainer_position',
          );
          const vB = getFirstParameterWithValue(
            b.datasets,
            'SampleTrackingContainer_position',
          );
          if (vA && vB) return Number(vA) - Number(vB);
          if (vA) return -1;
          if (vB) return 1;
          return a.name.localeCompare(b.name);
        })
        .toArray(),
    [containerSamples],
  );

  return (
    <div
      test-id="mx-investigation-summary-container"
      className="border rounded p-2 m-1 bg-light"
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div className="d-flex flex-row gap-2 align-items-center">
        {isNAContainer ? (
          <strong>Samples</strong>
        ) : (
          <strong>Puck: {containerId || 'unknown'}</strong>
        )}
        <span>-</span>
        <small className="text-muted">
          <i>
            <MXInvestigationSummaryStatistics samples={sortedSamples} />
          </i>
        </small>
      </div>
      <hr className="mt-2 mb-2" />
      <div className="d-flex flex-row flex-wrap gap-2 align-items-center">
        {sortedSamples.map((sample) => {
          return (
            <MXSampleCircle
              key={sample.id}
              sample={sample}
              allDatasets={allDatasets}
              selected={selectedSampleId === sample.id}
            />
          );
        })}
      </div>
    </div>
  );
}
