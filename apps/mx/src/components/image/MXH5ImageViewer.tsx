import { Loading } from '@edata-portal/core';
import { H5Provider, ImageVisForMXData } from '@edata-portal/h5';
import {
  DATASET_FILE_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { Suspense } from 'react';
import { Alert } from 'react-bootstrap';

const IMAGES_PER_FILE = 100;

export function MXH5ImageViewer({
  datasetId,
  imageIndex,
}: {
  datasetId: string;
  imageIndex: number;
}) {
  const h5Files = useGetEndpoint({
    endpoint: DATASET_FILE_LIST_ENDPOINT,
    params: { datasetId: datasetId, search: '%.h5' },
  });

  if (!h5Files?.length)
    return <Alert variant="danger">No HDF5 files in dataset.</Alert>;

  const targetFileIndex = Math.floor(imageIndex / IMAGES_PER_FILE) + 1;

  const targetFile = h5Files
    .map((f) => f.Datafile)
    .find((f) => f?.name?.endsWith(`${targetFileIndex}.h5`));

  if (!targetFile)
    return <Alert variant="danger">Target HDF5 file not found</Alert>;

  const imageIndexInFile = imageIndex % IMAGES_PER_FILE;
  const title = `Image #${imageIndexInFile + 1} in file ${targetFile.name}`;

  return (
    <Suspense fallback={<Loading />}>
      <H5Provider dataFileId={targetFile.id}>
        <ImageVisForMXData title={title} imageIndex={imageIndexInFile} />
      </H5Provider>
    </Suspense>
  );
}
