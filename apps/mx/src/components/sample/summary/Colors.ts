export const COLOR_DEFAULT = 'white';
export const COLOR_ANALYZED = 'rgb(169, 221, 255)';
export const COLOR_COLLECTED = 'rgb(54, 85, 143)';
