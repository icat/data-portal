import { COLOR_DEFAULT } from 'components/sample/summary/Colors';

export default function SampleCircleElement({
  backgroundColor,
  ringColor,
  position,
  sampleName,
  onClick,
  selected,
}: {
  backgroundColor: string;
  ringColor?: string;
  position?: string;
  sampleName?: string;
  onClick?: () => void;
  selected?: boolean;
}) {
  const className = selected
    ? 'border border-info border-4'
    : 'border border-black border-2';

  const isCircle = position || !sampleName;
  return (
    <div
      test-id="mx-sample-circle"
      className={className}
      onClick={onClick}
      style={{
        cursor: onClick ? 'pointer' : undefined,
        borderRadius: isCircle ? '50%' : '5px',
        backgroundColor: ringColor ? ringColor : backgroundColor,
        padding: 4,
        maxWidth: 'fit-content',
        maxHeight: 'fit-content',
        position: 'relative',
      }}
    >
      <div
        style={{
          width: isCircle ? '25px' : undefined,
          height: '25px',
          borderRadius: isCircle ? '50%' : '5px',
          backgroundColor: backgroundColor,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          color: backgroundColor === COLOR_DEFAULT ? 'black' : 'white',
        }}
      >
        {isCircle ? position : <span className="p-1">{sampleName}</span>}
      </div>
    </div>
  );
}
