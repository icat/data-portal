import SampleCircleElement from 'components/sample/summary/SampleCircleElement';
import {
  COLOR_ANALYZED,
  COLOR_COLLECTED,
  COLOR_DEFAULT,
} from 'components/sample/summary/Colors';

function SampleCircleLegendElement({
  backgroundColor,
  ringColor,
  label,
  selected,
}: {
  backgroundColor: string;
  ringColor?: string;
  label: string;
  selected?: boolean;
}) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <SampleCircleElement
        backgroundColor={backgroundColor}
        ringColor={ringColor}
        selected={selected}
      />
      <i className="text-center">{label}</i>
    </div>
  );
}

export function SampleCircleLegend({
  selectedSampleId,
}: {
  selectedSampleId?: boolean;
}) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 5,
        padding: 5,
      }}
    >
      <strong>Legend:</strong>
      <div className="d-flex flex-row gap-2 align-items-center justify-content-start flex-wrap">
        <SampleCircleLegendElement
          label="not analysed"
          backgroundColor={COLOR_DEFAULT}
        />
        <SampleCircleLegendElement
          label="analysed"
          backgroundColor={COLOR_ANALYZED}
        />
        <SampleCircleLegendElement
          label="collected"
          backgroundColor={COLOR_COLLECTED}
        />
        <SampleCircleLegendElement
          label="autoprocessed"
          backgroundColor={COLOR_COLLECTED}
          ringColor={'rgb(0,255,0)'}
        />
        {selectedSampleId && (
          <SampleCircleLegendElement
            label="selected"
            backgroundColor={COLOR_COLLECTED}
            ringColor={'rgb(0,255,0)'}
            selected
          />
        )}
      </div>
    </div>
  );
}
