import { getDatasetParamValue, LazyWrapper, Loading } from '@edata-portal/core';
import {
  Dataset,
  Sample,
  SAMPLE_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { MXSampleAutoProcessing } from 'components/sample/MXSampleAutoProcessing';
import { MXSampleQualityIndicator } from 'components/sample/MXSampleQualityIndicator';
import {
  getColorForStat,
  getMXAutoProcessingStatParam,
} from 'helpers/processed';
import { useBestAutoProcessing } from 'hooks/autoProcessingRanking';
import { useMXSettings } from 'hooks/mxSettings';
import { Suspense, useMemo } from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import SampleCircleElement from 'components/sample/summary/SampleCircleElement';
import {
  COLOR_ANALYZED,
  COLOR_COLLECTED,
  COLOR_DEFAULT,
} from 'components/sample/summary/Colors';
import {
  getFirstParameterWithValue,
  SampleContainer,
} from 'components/summary/helper';
import { Link } from 'react-router-dom';
import { MXSampleLastAction } from 'components/sample/MxSampleLastAction';
import { useMXAutoProcessingStatColorScale } from 'hooks/autoProcessingStatColorScale';
import {
  isSampleAnalysed,
  isSampleCollected,
  isSampleProcessed,
} from 'helpers/sample';
import { MXSampleSnapshot } from 'components/sample/MXSampleSnapshot';

export function MXSampleCircle({
  sample,
  allDatasets,
  interactive = true,
  selected,
}: {
  sample: SampleContainer;
  allDatasets: Dataset[];
  interactive?: boolean;
  selected?: boolean;
}) {
  const scale = useMXAutoProcessingStatColorScale(allDatasets);

  const scaleValue =
    scale.current.bad !== undefined && scale.current.good !== undefined
      ? scale.current
      : undefined;

  const best = useBestAutoProcessing(sample.datasets, 'include', true);

  const [settings] = useMXSettings();

  const borderColor = useMemo(() => {
    if (!best) return undefined;
    if (!isSampleProcessed(sample)) return;

    const statKey = getMXAutoProcessingStatParam(
      settings.autoProcessingRankingShell,
      settings.autoProcessingRankingParameter,
    );

    const statValue = getDatasetParamValue(best, statKey);
    const color = getColorForStat(
      settings.autoProcessingRankingShell,
      settings.autoProcessingRankingParameter,
      statValue,
      1,
      scaleValue,
    );

    return color || 'white';
  }, [
    best,
    sample,
    settings.autoProcessingRankingParameter,
    settings.autoProcessingRankingShell,
    scaleValue,
  ]);

  const position = useMemo(() => {
    return getFirstParameterWithValue(
      sample.datasets,
      'SampleTrackingContainer_position',
    );
  }, [sample.datasets]);

  const backgroundColor = useMemo(() => getBackgroundColor(sample), [sample]);

  const circleElement = (
    <SampleCircleElement
      backgroundColor={backgroundColor}
      ringColor={borderColor}
      position={position}
      sampleName={sample.name}
      selected={selected}
    />
  );

  const details = (
    <Popover
      style={{
        width: 950,
        maxWidth: '90vw',
        maxHeight: '90vh',
      }}
      key={sample.id + 'popover'}
    >
      <SampleCircleOverlay sample={sample} />
    </Popover>
  );

  if (!interactive) return circleElement;

  return (
    <Link
      to={`/investigation/${sample.investigation.id}/datasets?scrollToSample=${sample.id}`}
      style={{
        textDecoration: 'none',
      }}
    >
      <OverlayTrigger
        trigger={['hover', 'focus']}
        rootClose
        placement="auto"
        overlay={details}
        key={sample.id + 'overlay'}
      >
        <div key={sample.id + 'item'}>{circleElement}</div>
      </OverlayTrigger>
    </Link>
  );
}

function SampleCircleOverlay({ sample }: { sample: SampleContainer | Sample }) {
  return (
    <>
      <Popover.Header>
        <Suspense fallback="Loading...">
          <ContainerSampleName sample={sample} />
        </Suspense>
      </Popover.Header>
      <Popover.Body
        style={{
          padding: 5,
        }}
      >
        <LazyWrapper placeholder={<Loading />}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'wrap',
              gap: 5,
              overflow: 'auto',
            }}
          >
            <MXSampleSnapshot sample={sample} />
            <MXSampleAutoProcessing sample={sample} />
            <MXSampleQualityIndicator sample={sample} />
            <MXSampleLastAction sample={sample} />
          </div>
        </LazyWrapper>
      </Popover.Body>
    </>
  );
}

function ContainerSampleName({ sample }: { sample: SampleContainer | Sample }) {
  const samples = useGetEndpoint({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: {
      sampleIds: sample.id.toString(),
    },
    default: [sample],
    skipFetch:
      'name' in sample && sample.name !== undefined && sample.name !== 'NA',
  });

  const sampleObject = samples[0] || sample;

  return <>{sampleObject.name}</>;
}

const getBackgroundColor = (sample: SampleContainer) => {
  if (isSampleCollected(sample)) return COLOR_COLLECTED;
  if (isSampleAnalysed(sample)) return COLOR_ANALYZED;
  return COLOR_DEFAULT;
};
