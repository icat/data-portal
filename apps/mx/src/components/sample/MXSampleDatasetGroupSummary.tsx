import {
  ActionCardHeader,
  DatasetActionsButton,
  first,
  getDatasetParamValue,
  prettyPrintScanType,
  sortDatasetsByDate,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXDatasetsSummary } from 'components/MXDatasetsSummary';
import { MX_POSITION_NAME_PARAM } from 'constants/metadatakeys';
import {
  groupDatasetsByWorkflow,
  switchWorkflowOrDataset,
  Workflow,
} from 'helpers/workflow';
import { useMemo } from 'react';
import { Card } from 'react-bootstrap';

export function MXSampleDatasetGroupSummary({ group }: { group: Dataset[] }) {
  const withWorkflows = groupDatasetsByWorkflow(group);

  const sortedDatasets = sortDatasetsByDate(withWorkflows);
  const firstDataset = first(sortedDatasets);
  const time = firstDataset?.startDate;

  const position = useMemo(() => {
    if (!firstDataset) return undefined;
    const dataset = switchWorkflowOrDataset({
      value: firstDataset,
      executeIfWorkflow: (workflow) => first(workflow.datasets),
      executeIfDataset: (dataset) => dataset,
    });
    if (!dataset) return undefined;
    return getDatasetParamValue(dataset, MX_POSITION_NAME_PARAM);
  }, [firstDataset]);

  return (
    <Card>
      <ActionCardHeader
        title={''}
        date={time}
        leftAlignedItems={[
          ...(position ? [position, '|'] : []),
          ...sortedDatasets.flatMap((item, i) => {
            return [
              <MXSampleGroupItem key={item.id} item={item} />,
              ...(i < sortedDatasets.length - 1 ? ['>'] : []),
            ];
          }),
        ]}
        rightAlignedItems={[]}
        isProcessed={false}
      />

      <Card.Body className="p-1">
        <MXDatasetsSummary datasets={group} />
      </Card.Body>
    </Card>
  );
}

function MXSampleGroupItem({ item }: { item: Dataset | Workflow }) {
  const name = switchWorkflowOrDataset({
    value: item,
    executeIfWorkflow: (workflow) => workflow.name,
    executeIfDataset: (dataset) => prettyPrintScanType(dataset),
  });
  const dataset = switchWorkflowOrDataset({
    value: item,
    executeIfWorkflow: (workflow) => workflow.datasets[0],
    executeIfDataset: (dataset) => dataset,
  });

  return (
    <div className="d-flex align-items-center">
      {switchWorkflowOrDataset({
        value: item,
        executeIfWorkflow: () => null,
        executeIfDataset: (v) => <DatasetActionsButton dataset={v} />,
      })}
      <strong key={dataset.id}>{name}</strong>
    </div>
  );
}
