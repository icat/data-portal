import { first, getScanType, sortDatasetsByDate } from '@edata-portal/core';
import {
  Dataset,
  DATASET_LIST_ENDPOINT,
  DATASET_TYPE_ACQUISITION,
  Sample,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { MXAcquisitionSampleSnapshot } from 'components/acquisition/MXAcquisitionSampleSnapshot';
import { MXAction } from 'components/acquisition/MXAction';
import { SampleContainer } from 'components/summary/helper';
import { SCAN_TYPE_PARAM_AUTOMESH } from 'constants/metadatakeys';
import { getSampleSnapshots } from 'helpers/snapshot';
import { useMemo } from 'react';

export function MXSampleSnapshot({
  sample,
}: {
  sample: Sample | SampleContainer;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  const sortedDatasets = useMemo(() => {
    return sortDatasetsByDate(datasets).reverse();
  }, [datasets]);

  const automesh = useMemo(() => {
    return first(
      sortedDatasets.filter(
        (dataset) => getScanType(dataset) === SCAN_TYPE_PARAM_AUTOMESH,
      ),
    );
  }, [sortedDatasets]);

  const sampleSnapshot = useMemo(() => {
    return first(
      sortedDatasets.filter((d) => {
        return getSampleSnapshots(d).length > 0;
      }),
    );
  }, [sortedDatasets]);

  if (automesh) {
    return <MXAction action={automesh} />;
  }

  if (sampleSnapshot) {
    return <MXAcquisitionSampleSnapshot dataset={sampleSnapshot} />;
  }

  return null;
}
