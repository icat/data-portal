import { sortDatasetsByDate, useViewers } from '@edata-portal/core';
import {
  DATASET_LIST_ENDPOINT,
  Sample,
  useGetEndpoint,
  DATASET_TYPE_ACQUISITION,
  Dataset,
} from '@edata-portal/icat-plus-api';
import type { SampleContainer } from 'components/summary/helper';
import { PROCESSED_TYPE_QUALITY_INDICATOR } from 'constants/metadatakeys';
import { getProcessedOfType } from 'helpers/processed';
import { useBestCollection } from 'hooks/autoProcessingRanking';
import { first } from 'lodash';

export function MXSampleQualityIndicator({
  sample,
}: {
  sample: Sample | SampleContainer;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  const viewers = useViewers();

  const bestDataset = useBestCollection(datasets, 'exclude');

  if (!datasets) return null;

  const qualityIndicators = getProcessedOfType(
    //if a best dataset is found, we only consider that one
    bestDataset ? [bestDataset] : datasets,
    PROCESSED_TYPE_QUALITY_INDICATOR,
    true,
  );

  if (!qualityIndicators?.length) return null;

  //sort by date and take the most recent one
  const qualityIndicator = first(
    sortDatasetsByDate(qualityIndicators).reverse(),
  );

  if (!qualityIndicator) return null;

  return viewers.viewDataset(qualityIndicator, 'snapshot');
}
