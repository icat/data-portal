import {
  DATASET_LIST_ENDPOINT,
  DATASET_TYPE_ACQUISITION,
  Dataset,
  Sample,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { MXBestAutoProcessingSnapshot } from 'components/processed/AutoProcessing/MXBestAutoProcessingSnapshot';
import type { SampleContainer } from 'components/summary/helper';

export function MXSampleAutoProcessing({
  sample,
}: {
  sample: Sample | SampleContainer;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  return (
    <MXBestAutoProcessingSnapshot datasets={datasets} merged={'include'} />
  );
}
