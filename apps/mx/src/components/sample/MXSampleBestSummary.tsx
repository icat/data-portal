import { flattenOutputs, getInputDatasetIds } from '@edata-portal/core';
import type { Dataset, Sample } from '@edata-portal/icat-plus-api';
import { MXDatasetsSummary } from 'components/MXDatasetsSummary';
import { useBestAutoProcessing } from 'hooks/autoProcessingRanking';
import { useMemo } from 'react';
import { Card } from 'react-bootstrap';

export function MXSampleBestSummary({
  sample,
  groups,
}: {
  sample: Sample;
  groups: Dataset[][];
}) {
  const flattenedGroups = useMemo(
    () => groups.flatMap((group) => group),
    [groups],
  );

  const bestAutoProcessing = useBestAutoProcessing(flattenedGroups, 'include');

  const group = useMemo(() => {
    if (!bestAutoProcessing) return undefined;
    return groups.find((group) => {
      return flattenOutputs(group).some((dataset) => {
        return dataset.id === bestAutoProcessing.id;
      });
    });
  }, [bestAutoProcessing, groups]);

  const bestInputs = useMemo(() => {
    if (!bestAutoProcessing || !group) return [];
    const inputIds = getInputDatasetIds(bestAutoProcessing);
    return group.filter((dataset) => {
      return inputIds.includes(dataset.id.toString());
    });
  }, [bestAutoProcessing, group]);

  return (
    <Card>
      <Card.Header className="bg-light text-info p-1">
        <small>
          <strong>Best result for sample {sample.name}</strong>
        </small>
      </Card.Header>
      <Card.Body className="p-1">
        {group ? (
          <MXDatasetsSummary datasets={bestInputs} />
        ) : (
          'Could not find best result'
        )}
      </Card.Body>
    </Card>
  );
}
