import {
  DatasetImage,
  HorizontalScroll,
  Loading,
  getParameterValue,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXInvestigationSummary } from 'components/summary/MXInvestigationSummary';
import { MX_SAMPLE_CONTAINER_ID_PARAM } from 'constants/metadatakeys';
import { getSampleSnapshots } from 'helpers/snapshot';
import { Suspense } from 'react';
import { Alert, Col, Container, Row } from 'react-bootstrap';

export function MXDatasetSampleTab({ dataset }: { dataset: Dataset }) {
  const snapshots = getSampleSnapshots(dataset);

  return (
    <Container fluid>
      <Row>
        <Col>
          <Row>
            <Col>
              <h5 className="text-center">{dataset.sampleName}</h5>
            </Col>
          </Row>
          <HorizontalScroll>
            {snapshots.map((snapshot) => (
              <div
                key={snapshot.id}
                style={{
                  minWidth: 100,
                  maxWidth: 200,
                }}
              >
                <DatasetImage
                  resourceId={snapshot.id}
                  name={snapshot.name}
                  displayFileName={true}
                />
              </div>
            ))}
          </HorizontalScroll>
          <Row className="pt-2">
            <Suspense fallback={<Loading />}>
              <LoadAndDisplayContainerSamples dataset={dataset} />
            </Suspense>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

function LoadAndDisplayContainerSamples({ dataset }: { dataset: Dataset }) {
  const containerId = getParameterValue(dataset, MX_SAMPLE_CONTAINER_ID_PARAM);

  if (!containerId) {
    return (
      <Alert variant="warning">
        No container information found for this sample.
      </Alert>
    );
  }

  return (
    <MXInvestigationSummary
      investigation={dataset.investigation}
      filterContainerId={containerId}
      selectedSampleId={dataset.sampleId}
    />
  );
}
