import { getScanType } from '@edata-portal/core';
import {
  DATASET_TYPE_ACQUISITION,
  useGetEndpoint,
  DATASET_LIST_ENDPOINT,
  Dataset,
  Sample,
} from '@edata-portal/icat-plus-api';
import { MXAction } from 'components/acquisition/MXAction';
import type { SampleContainer } from 'components/summary/helper';
import { SCAN_TYPE_PARAM_COLLECTION } from 'constants/metadatakeys';
import { getActions } from 'helpers/dataset';
import { useBestCollection } from 'hooks/autoProcessingRanking';

export function MXSampleLastAction({
  sample,
}: {
  sample: Sample | SampleContainer;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  const actions = getActions(datasets).filter(
    (a) => getScanType(a) !== SCAN_TYPE_PARAM_COLLECTION,
  );

  const bestDataset = useBestCollection(datasets, 'include');

  const lastAction = actions[actions.length - 1];

  if (!lastAction) return null;

  if (bestDataset) return null;

  return <MXAction action={lastAction} />;
}
