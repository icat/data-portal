import { prettyPrintScanType, DatasetCard } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { getMxDatasetTabs } from 'helpers/tabs';
import { useState, useRef } from 'react';

export default function MXAcquisitionDatasetDetails({
  dataset,
}: {
  dataset: Dataset;
}) {
  const title = prettyPrintScanType(dataset);

  const [expanded, setExpanded] = useState(true);

  const ref = useRef<HTMLDivElement>(null);

  const datasetView = (
    <div ref={ref}>
      <DatasetCard
        key={dataset.id}
        classNameBody="p-1"
        dataset={dataset}
        title={title}
        setExpanded={(v) => setExpanded(v)}
        expanded={expanded}
        tabs={getMxDatasetTabs(dataset, false)}
      />
    </div>
  );

  return datasetView;
}
