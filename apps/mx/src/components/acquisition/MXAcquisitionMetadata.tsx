import {
  MetadataCategories,
  MetadataCategory,
  convertToFixed,
  formatDateToDayAndTime,
  getDatasetName,
  getParameterValue,
  toExponential,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

import { MX_POSITION_NAME_PARAM } from 'constants/metadatakeys';
import { formatChangerPosition } from 'helpers/changer';

export function MXAcquisitionMetadata({ dataset }: { dataset: Dataset }) {
  return (
    <div className="d-flex flex-column gap-2">
      <MetadataCategories
        categories={getMxMetadataCategories(dataset)}
        entity={dataset}
      />
    </div>
  );
}

function getMxMetadataCategories(dataset: Dataset): MetadataCategory[] {
  const rotationAxis = getParameterValue(dataset, 'MX_rotation_axis');
  const rotationStart = getParameterValue(dataset, 'MX_axis_start');
  const rotationEnd = getParameterValue(dataset, 'MX_axis_end');

  return [
    {
      id: 'Acquisition',
      title: 'Acquisition',
      content: [
        {
          caption: 'Dataset name',
          value: getDatasetName(dataset),
        },
        {
          caption: 'Start time',
          value: formatDateToDayAndTime(dataset.startDate),
        },
        {
          caption: 'Position',
          parameterName: MX_POSITION_NAME_PARAM,
        },
        {
          caption: 'Workflow',
          parameterName: 'Workflow_type',
        },
        {
          caption: 'Prefix',
          parameterName: 'MX_template',
        },

        {
          caption: 'Run #',
          parameterName: 'scanNumber',
        },
        {
          caption: '# Images',
          parameterName: 'MX_numberOfImages',
        },
        {
          caption: 'Transmission',
          parameterName: 'MX_transmission',
          digits: 1,
          units: '%',
        },
      ],
    },
    {
      id: 'Beam',
      title: 'Beam',
      content: [
        {
          caption: 'Res. (corner)',
          parameterName: 'MX_resolution_at_corner',
          units: 'Å',
          digits: 2,
        },
        {
          caption: 'Wavelength',
          parameterName: 'InstrumentMonochromator_wavelength',
          digits: 2,
        },
        {
          value: getParameterValue(dataset, 'MX_axis_range'),
          caption:
            rotationAxis !== undefined
              ? `${rotationAxis} range s`
              : 'Axis range',
          units: '°',
        },
        {
          caption:
            rotationAxis !== undefined
              ? `${rotationAxis} start (end)`
              : 'Axis start (end)',
          value:
            rotationStart !== undefined
              ? `${rotationStart}° (${rotationEnd}°)`
              : '- -',
        },
        {
          caption: 'Exposure Time',
          parameterName: 'MX_exposureTime',
          units: 's',
        },
        {
          caption: 'Flux start',
          value: toExponential(getParameterValue(dataset, 'MX_flux'), 2),
          units: 'ph/s',
          digits: 0,
        },
        {
          caption: 'Flux end',
          value: toExponential(getParameterValue(dataset, 'MX_fluxEnd'), 2),
          units: 'ph/s',
          digits: 0,
        },
        {
          caption: 'X Beam',
          value: convertToFixed(getParameterValue(dataset, 'MX_xBeam'), 2),
          units: 'mm',
        },
        {
          caption: 'Y Beam',
          value: convertToFixed(getParameterValue(dataset, 'MX_yBeam'), 2),
          units: 'mm',
        },
      ],
    },
    {
      id: 'Sample',
      title: 'Sample',
      content: [
        {
          caption: 'Protein',
          parameterName: 'SampleProtein_acronym',
        },
        {
          caption: 'Sample',
          value: dataset.sampleName,
        },
        {
          caption: 'Parcel',
          parameterName: 'SampleTrackingParcel_id',
        },
        {
          caption: 'Container name',
          parameterName: 'SampleTrackingContainer_id',
        },
        {
          caption: 'Container type',
          parameterName: 'SampleTrackingContainer_type',
        },
        {
          caption: 'Position in container',
          parameterName: 'SampleTrackingContainer_position',
        },
        {
          caption: 'Position in changer',
          parameterName: 'SampleChanger_position',
          formatter: formatChangerPosition,
        },
        {
          caption: 'Beamline location',
          value: dataset.instrumentName,
        },
      ],
    },
    {
      id: 'Optics',
      title: 'Optics',
      content: [
        {
          caption: 'Focussing optics',
          value: 'NA',
        },
        {
          caption: 'Mono type',
          value: 'NA',
        },
        {
          caption: 'Beam size X',
          value: 'NA',
          units: 'µm',
        },
        {
          caption: 'Beam size Y',
          value: 'NA',
          units: 'µm',
        },
        {
          caption: 'Beam shape',
          parameterName: 'MX_beamShape',
        },
        {
          caption: 'Beam divergence Hor',
          value: 'NA',
          units: 'μrad',
        },
        {
          caption: 'Beam divergence Vert',
          value: 'NA',
          units: 'μrad',
        },
        {
          caption: 'Polarization',
          value: 'NA',
        },
      ],
    },
    {
      id: 'Synchrotron',
      title: 'Synchrotron',
      content: [
        {
          caption: 'Name',
          parameterName: 'InstrumentSource_name',
        },
        {
          caption: 'Filling mode',
          parameterName: 'InstrumentSource_mode',
        },
        {
          caption: 'Current',
          parameterName: 'InstrumentSource_current',
        },
      ],
    },
    {
      id: 'Detector',
      title: 'Detector',
      content: [
        {
          caption: 'Type',
          parameterName: 'InstrumentDetector01_type',
        },
        {
          caption: 'Model',
          parameterName: 'InstrumentDetector01_name',
        },
        {
          caption: 'Manufacturer',
          parameterName: 'InstrumentDetector01_description',
        },
        {
          caption: 'Pixel size horizontal',
          parameterName: 'InstrumentDetector01_x_pixel_size',
          units: 'µm',
        },
        {
          caption: 'Pixel size vertical',
          parameterName: 'InstrumentDetector01_y_pixel_size',
          units: 'µm',
        },
        {
          caption: 'Distance',
          parameterName: 'InstrumentDetector01_distance',
        },
      ],
    },
  ];
}
