import { useViewers } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

export function MXAction({
  action,
  processedActionFilter,
}: {
  action: Dataset;
  processedActionFilter?: (processed: Dataset) => boolean;
}) {
  const viewers = useViewers();

  return viewers.viewDataset(action, 'snapshot', {
    processedActionFilter,
  });
}
