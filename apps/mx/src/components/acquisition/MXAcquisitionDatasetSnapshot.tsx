import {
  prettyPrintScanType,
  DatasetImage,
  DatasetSnapshotItem,
  DatasetSnapshot,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MXAcquisitionMetadata } from 'components/acquisition/MXAcquisitionMetadata';
import {
  MX_SNAPSHOT_MAX_WIDTH,
  MX_SNAPSHOT_MIN_WIDTH,
} from 'constants/display';

import {
  getActionSnapshot,
  getDiffractionSnapshots,
  getSampleSnapshots,
  SNAPSHOT_FILE_SIZE,
} from 'helpers/snapshot';
import { getMxDatasetTabs } from 'helpers/tabs';

import { useMemo } from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';

export function MXAcquisitionDatasetSnapshot({
  dataset,
  processedActionFilter,
  showSampleSnapshot = false,
  showDiffractionSnapshot = false,
  showMainSnapshot = true,
}: {
  dataset: Dataset;
  processedActionFilter?: (processed: Dataset) => boolean;
  showSampleSnapshot?: boolean;
  showDiffractionSnapshot?: boolean;
  showMainSnapshot?: boolean;
}) {
  const tabs = useMemo(() => {
    return getMxDatasetTabs(dataset, true, processedActionFilter);
  }, [dataset, processedActionFilter]);

  const { primaryItems, secondaryItems } = useMemo(
    () =>
      getSnapshotItems(
        dataset,
        processedActionFilter,
        showSampleSnapshot,
        showDiffractionSnapshot,
        showMainSnapshot,
      ),
    [
      dataset,
      processedActionFilter,
      showDiffractionSnapshot,
      showSampleSnapshot,
      showMainSnapshot,
    ],
  );

  const extraHeader = useMemo(() => {
    return (
      <OverlayTrigger
        key={'metadataTrigger'}
        placement="auto"
        trigger={['hover', 'focus']}
        overlay={
          <Popover
            key={'metadataPopover'}
            style={{
              maxWidth: '80vw',
              width: 'auto',
            }}
          >
            <Popover.Header>Metadata</Popover.Header>
            <Popover.Body>
              <MXAcquisitionMetadata dataset={dataset} />
            </Popover.Body>
          </Popover>
        }
      >
        <FontAwesomeIcon
          className="text-info"
          key={'metadataIcon'}
          icon={faInfoCircle}
        />
      </OverlayTrigger>
    );
  }, [dataset]);

  return (
    <DatasetSnapshot
      dataset={dataset}
      width={{
        min: MX_SNAPSHOT_MIN_WIDTH,
        max: MX_SNAPSHOT_MAX_WIDTH,
      }}
      title={prettyPrintScanType(dataset)}
      windowTabs={tabs}
      primaryItems={primaryItems}
      secondaryItems={secondaryItems}
      extraHeader={extraHeader}
    />
  );
}

function getSnapshotItems(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
  firstSampleIsPrimary = false,
  firstDiffractionIsPrimary = false,
  mainSnapshotIsPrimary = true,
): {
  primaryItems: DatasetSnapshotItem[];
  secondaryItems: DatasetSnapshotItem[];
} {
  const mainSnapshot = getActionSnapshot(dataset, processedActionFilter);
  const sampleSnapshots = getSampleSnapshots(dataset, processedActionFilter);

  const diffractionSnapshots = getDiffractionSnapshots(
    dataset,
    processedActionFilter,
    SNAPSHOT_FILE_SIZE.SMALL,
  );

  const [firstSample, ...otherSamples] = sampleSnapshots.map((s) => ({
    element: (
      <DatasetImage
        resourceId={s.id}
        name={s.name}
        displayFileName={false}
        zoomable={false}
      />
    ),
    showWindow: 'Sample',
  }));

  const [firstDiffraction, ...otherDiffractions] = diffractionSnapshots.map(
    (s) => ({
      element: (
        <DatasetImage
          resourceId={s.id}
          name={s.name}
          displayFileName={false}
          zoomable={false}
        />
      ),
      showWindow: 'Diffraction',
    }),
  );

  const primary: DatasetSnapshotItem[] = [];
  const secondary: DatasetSnapshotItem[] = [];

  if (firstSample) {
    if (firstSampleIsPrimary) {
      primary.push(firstSample);
    } else {
      secondary.push(firstSample);
    }
  }
  if (firstDiffraction) {
    if (firstDiffractionIsPrimary) {
      primary.push(firstDiffraction);
    } else {
      secondary.push(firstDiffraction);
    }
  }

  if (mainSnapshot) {
    const item = {
      element: (
        <DatasetImage
          resourceId={mainSnapshot.id}
          name={mainSnapshot.name}
          displayFileName={false}
          zoomable={false}
        />
      ),
      showWindow: true,
    };
    if (mainSnapshotIsPrimary) {
      primary.push(item);
    } else {
      secondary.push(item);
    }
  }

  if (!primary.length) {
    if (secondary.length) {
      primary.push(secondary.pop()!);
    } else {
      primary.push({
        element: <i>Snapshot not found</i>,
        showWindow: true,
      });
    }
  }

  secondary.push(...otherSamples);

  secondary.push(...otherDiffractions);

  return {
    primaryItems: primary,
    secondaryItems: secondary,
  };
}
