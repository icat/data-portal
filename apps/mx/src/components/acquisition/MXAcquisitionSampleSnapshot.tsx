import {
  DatasetImage,
  DatasetSnapshotItem,
  DatasetSnapshot,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MX_SNAPSHOT_MAX_WIDTH,
  MX_SNAPSHOT_MIN_WIDTH,
} from 'constants/display';

import { getSampleSnapshots } from 'helpers/snapshot';
import { getMxDatasetTabs } from 'helpers/tabs';

import { useMemo } from 'react';

export function MXAcquisitionSampleSnapshot({ dataset }: { dataset: Dataset }) {
  const items = useMemo(
    () =>
      getSampleSnapshots(dataset).map(
        (s) =>
          ({
            element: (
              <DatasetImage
                key={s.id}
                resourceId={s.id}
                name={s.name}
                displayFileName={false}
                zoomable={false}
              />
            ),
            showWindow: 'Sample',
          }) as DatasetSnapshotItem,
      ),
    [dataset],
  );

  if (!items?.length) return null;

  return (
    <DatasetSnapshot
      dataset={dataset}
      width={{
        min: MX_SNAPSHOT_MIN_WIDTH,
        max: MX_SNAPSHOT_MAX_WIDTH,
      }}
      title={'Sample'}
      primaryItems={items}
      windowTabs={getMxDatasetTabs(dataset, true)}
    />
  );
}
