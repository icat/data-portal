import { Button, Loading, NoData, Zoomable } from '@edata-portal/core';
import {
  useGetEndpoint,
  Dataset,
  DOWNLOAD_FILE_ENDPOINT,
  ProgressSuspense,
} from '@edata-portal/icat-plus-api';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getMXReportFile } from 'helpers/report';
import { useMemo, type MouseEventHandler } from 'react';
import { Alert, Col, Container, Row, Table } from 'react-bootstrap';

export function MXReport({
  dataset,
  processedActionFilter,
}: {
  dataset: Dataset;
  processedActionFilter?: (processed: Dataset) => boolean;
}) {
  return (
    <ProgressSuspense render={(progress) => <Loading progress={progress} />}>
      <LoadReport
        dataset={dataset}
        processedActionFilter={processedActionFilter}
      />
    </ProgressSuspense>
  );
}

function LoadReport({
  dataset,
  processedActionFilter,
}: {
  dataset: Dataset;
  processedActionFilter?: (processed: Dataset) => boolean;
}) {
  const file = useMemo(
    () => getMXReportFile(dataset, processedActionFilter),
    [dataset, processedActionFilter],
  );

  const content = useGetEndpoint({
    endpoint: DOWNLOAD_FILE_ENDPOINT,
    params: {
      resourceId: file?.id || '',
    },
    skipFetch: !file,
  });

  if (!content) return <NoData />;

  const objectContent = content as Report;

  //verify that all keys of Report are present as expected
  const keys = ['title', 'type', 'version', 'items'];
  const missingKeys = keys.filter(
    (key) => !Object.keys(objectContent).includes(key),
  );
  if (missingKeys.length > 0) {
    console.error('Missing keys in workflow step: ' + missingKeys.join(', '));
    return <NoData />;
  }

  return <RenderReportContent report={objectContent} />;
}

export interface Report {
  title: string;
  type: string;
  version: number;
  items: ReportItem[];
}
export interface ReportItem {
  type: 'table' | 'warning' | 'info' | 'images' | 'image' | 'logFile';
  title?: string;
  orientation?: string;
  columns?: string[];
  data?: string[][];
  value?: string;
  items?: ReportItem[];
  linkText?: string;
  logText?: string;
}

function RenderReportContent({ report }: { report: Report }) {
  return (
    <Container fluid>
      <Col>
        <h4>{report.title}</h4>
        <hr />
        {report.items.map((item) => {
          switch (item.type) {
            case 'table':
              return <TableContent item={item}></TableContent>;
            case 'warning':
              return <WarningContent item={item}></WarningContent>;
            case 'info':
              return <InfoContent item={item}></InfoContent>;
            case 'images':
              return <ImagesContent item={item}></ImagesContent>;
            case 'image':
              return <ImageContent item={item}></ImageContent>;
            case 'logFile':
              return <LogFileContent item={item}></LogFileContent>;
            default:
              console.error('Unknown report item type: ' + item.type);
              return null;
          }
        })}
      </Col>
    </Container>
  );
}

function WarningContent({ item }: { item: ReportItem }) {
  return (
    <Row style={{ marginLeft: 0 }}>
      <Alert variant="warning">{item.value}</Alert>
    </Row>
  );
}

function InfoContent({ item }: { item: ReportItem }) {
  return (
    <Row style={{ marginLeft: 0 }}>
      <Alert variant="info">{item.value}</Alert>
    </Row>
  );
}

function TableContent({ item }: { item: ReportItem }) {
  return (
    <Row>
      <h5>{item.title}</h5>
      <Table responsive striped bordered hover size="sm">
        <thead>
          <tr>
            {item.columns?.map((column) => {
              return <th key={column}>{column}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {item.data?.map((row, irow) => {
            return (
              <tr key={irow}>
                {row?.map((value, ivalue) => {
                  return <td key={ivalue}>{value}</td>;
                })}
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Row>
  );
}

function ImagesContent({ item }: { item: ReportItem }) {
  return (
    <Row>
      {item.items?.map((value) => {
        return (
          <Col key={value.title}>
            <ImageContent item={value} />
          </Col>
        );
      })}
    </Row>
  );
}

function ImageContent({ item }: { item: ReportItem }) {
  const src = 'data:image/png;base64,' + item.value;
  return (
    <Col
      key={item.title}
      className="text-center"
      style={{ maxWidth: 500, marginBottom: 15 }}
    >
      <Zoomable>
        <img
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'contain',
          }}
          alt={item.title || ''}
          src={src}
        />
      </Zoomable>
      {item.title && <i>{item.title}</i>}
    </Col>
  );
}

function LogFileContent({ item }: { item: ReportItem }) {
  return (
    <Button
      style={{ marginRight: 5, marginBottom: 15 }}
      variant="secondary"
      onClick={downloadHandlerFromValue(item.logText)}
    >
      <FontAwesomeIcon icon={faDownload}></FontAwesomeIcon> {item.linkText}
    </Button>
  );
}

function downloadHandlerFromValue(
  value = '',
): MouseEventHandler<HTMLButtonElement> {
  return () => {
    const element = document.createElement('a');
    const file = new Blob([value], { type: 'text/plain' });
    element.href = URL.createObjectURL(file);
    element.download = 'log.txt';
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  };
}
