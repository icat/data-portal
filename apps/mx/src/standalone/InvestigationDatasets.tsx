import { usePath, useViewers } from '@edata-portal/core';
import {
  useGetEndpoint,
  INVESTIGATION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { Alert } from 'react-bootstrap';

/**
 * Fetches and displays the datasets related to a specific investigation.
 *
 * This component:
 * - Retrieves the `investigationId` from the URL path.
 * - Fetches the corresponding investigation using `useGetEndpoint`.
 * - Renders an alert if no investigation is found.
 * - Uses `viewInvestigation` from `useViewers()` to render the investigation details.
 *
 * @returns {JSX.Element} Investigation details or an error alert.
 */
export default function InvestigationDatasets(): JSX.Element {
  const investigationId = usePath('investigationId');
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: { ids: investigationId },
  });
  const { viewInvestigation } = useViewers();

  if (!investigations || investigations.length === 0) {
    return <Alert variant="danger">Could not find investigation</Alert>;
  }

  return viewInvestigation(investigations[0]);
}
