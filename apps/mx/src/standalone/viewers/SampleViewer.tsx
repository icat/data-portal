import type { Sample } from '@edata-portal/icat-plus-api';
import MXSampleViewer from 'viewers/MXSampleViewer';

export function SampleViewer({
  sample,
  props,
}: {
  sample: Sample;
  props?: any;
}) {
  return <MXSampleViewer sample={sample} {...props} />;
}
