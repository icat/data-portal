import { Investigation } from '@edata-portal/icat-plus-api';
import MXInvestigationViewer from 'viewers/MXInvestigationViewer';

export function InvestigationViewer({
  investigation,
  props,
}: {
  investigation: Investigation;
  props?: any;
}) {
  return (
    <MXInvestigationViewer
      investigation={investigation}
      nested={true}
      {...props}
    />
  );
}
