import { DatasetViewerType } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { Alert } from 'react-bootstrap';
import MXDatasetDetailsViewer from 'viewers/MXDatasetDetailsViewer';
import MXDatasetSnapshotViewer from 'viewers/MXDatasetSnapshotViewer';

/**
 * Renders a dataset viewer based on the specified type.
 *
 * @param {Object} props - Component props.
 * @param {Dataset} props.dataset - The dataset object to display.
 * @param {any} [props.props] - Additional props to be passed to the viewer component.
 * @param {DatasetViewerType} props.type - The type of viewer to display.
 *
 * @returns {JSX.Element} The appropriate dataset viewer component or an error alert.
 */
export function DatasetViewer({
  dataset,
  props,
  type,
}: {
  dataset: Dataset;
  props?: any;
  type: DatasetViewerType;
}) {
  if (type === 'details') {
    return <MXDatasetDetailsViewer dataset={dataset} {...props} />;
  }
  if (type === 'snapshot') {
    return <MXDatasetSnapshotViewer {...props} dataset={dataset} />;
  }
  return <Alert variant="danger">No viewer found</Alert>;
}
