import { ConfigContext } from '@edata-portal/core';
import { useSuspenseQueries } from '@tanstack/react-query';
import { useMemo } from 'react';
declare global {
  interface Window {
    remoteUrls: any;
  }
}

export function ConfigProvider({ children }: { children: React.ReactNode }) {
  const [api, ui, techniques] = useSuspenseQueries({
    queries: [
      {
        queryKey: ['api.config'],
        queryFn: async () => {
          const response = await fetch('../config/api.config.json');
          return await response.json();
        },
      },
      {
        queryKey: ['ui.config'],
        queryFn: async () => {
          const response = await fetch('../config/ui.config.json');
          return await response.json();
        },
      },
      {
        queryKey: ['techniques.config'],
        queryFn: async () => {
          return [
            {
              name: 'Crystallography',
              shortname: 'MX',
            },
          ];
        },
      },
    ],
  });

  const value = useMemo(
    () => ({
      api: api.data,
      ui: ui.data,
      techniques: techniques.data,
    }),
    [api.data, techniques.data, ui.data],
  );

  return (
    <ConfigContext.Provider value={value}>{children}</ConfigContext.Provider>
  );
}
