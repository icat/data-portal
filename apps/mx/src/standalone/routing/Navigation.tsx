import { Container } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';
import { SideNavRenderer } from '@edata-portal/core';

export function Navigation() {
  return (
    <div
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
      }}
    >
      <Container
        style={{
          paddingTop: 20,
          overflow: 'auto',
          height: '100%',
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
        fluid
        className="main"
      >
        <SideNavRenderer>
          <Outlet />
        </SideNavRenderer>
      </Container>
    </div>
  );
}
