import InvestigationDatasets from 'standalone/InvestigationDatasets';
import { useMemo } from 'react';
import {
  createBrowserRouter,
  RouteObject,
  RouterProvider,
} from 'react-router-dom';

export const routes: RouteObject[] = [
  {
    path: '/investigation/:investigationId',
    element: <InvestigationDatasets></InvestigationDatasets>,
  },
  {
    path: '*',
    element: <div>Cheers</div>,
  },
];

export function AppRouter({ children }: { children: JSX.Element }) {
  const router = useMemo(() => {
    return createBrowserRouter([
      {
        element: <>{children}</>,
        children: routes,
        id: 'home',
        handle: { breadcrumb: 'Home' },
      },
    ]);
  }, [children]);

  return <RouterProvider router={router} />;
}
