import {
  TabDefinition,
  getFilesTab,
  getInputsTab,
  isAcquisition,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXAcquisitionMetadata } from 'components/acquisition/MXAcquisitionMetadata';
import { MXAutoProcessingTable } from 'components/processed/AutoProcessing/MXAutoProcessingTable';
import { MXProcessedDatasetSummaryContent } from 'components/processed/MXProcessedDatasetSummaryContent';
import { MXDatasetDiffractionTab } from 'components/processed/diffraction/MXDatasetDiffractionTab';
import { MXDozorPlot } from 'components/processed/dozor/MXDozorPlot';
import { MXPhasingList } from 'components/processed/phasing/MXPhasingList';
import { MXReport } from 'components/report/MXReport';
import { MXDatasetSampleTab } from 'components/sample/MXDatasetSampleTab';
import {
  PROCESSED_TYPE_DIFFRACTION_THUMBNAIL,
  PROCESSED_TYPE_QUALITY_INDICATOR,
} from 'constants/metadatakeys';
import { getProcessedOfType } from 'helpers/processed';
import { hasMXReportFile } from 'helpers/report';
import { getSampleSnapshots } from 'helpers/snapshot';
import { getAutoProcessing } from 'hooks/autoProcessingRanking';
import { getPhasings } from 'hooks/phasingRanking';
import { Badge } from 'react-bootstrap';

export function getMxDatasetTabs(
  dataset: Dataset,
  showPathInFiles: boolean,
  processedActionFilter?: (processed: Dataset) => boolean,
): TabDefinition[] {
  if (isAcquisition(dataset)) {
    return getAcquisitionTabs(dataset, showPathInFiles, processedActionFilter);
  }
  return getProcessedTabs(dataset, showPathInFiles);
}

function getAcquisitionTabs(
  dataset: Dataset,
  showPathInFiles: boolean,
  processedActionFilter?: (processed: Dataset) => boolean,
): TabDefinition[] {
  const dozor = getProcessedOfType(
    [dataset],
    PROCESSED_TYPE_QUALITY_INDICATOR,
    true,
  );

  const diffraction = getProcessedOfType(
    [dataset],
    PROCESSED_TYPE_DIFFRACTION_THUMBNAIL,
    true,
  );

  const sampleSnapshots = getSampleSnapshots(dataset);

  const processingsNb = getAutoProcessing([dataset], 'include', false).length;

  return [
    {
      key: 'Report',
      title: 'Report',
      content: (
        <MXReport
          dataset={dataset}
          processedActionFilter={processedActionFilter}
        />
      ),
      hidden: !hasMXReportFile(dataset, processedActionFilter),
    },
    {
      key: 'Dozor',
      title: 'Dozor',
      hidden: !dozor.length,
      content: <MXDozorPlot dataset={dozor[0]} />,
    },
    {
      key: 'Metadata',
      title: 'Metadata',
      content: <MXAcquisitionMetadata dataset={dataset} />,
    },
    {
      key: 'Sample',
      title: 'Sample',
      content: <MXDatasetSampleTab dataset={dataset} />,
      hidden: !sampleSnapshots.length,
    },
    {
      key: 'Diffraction',
      title: 'Diffraction',
      hidden: !diffraction.length,
      content: <MXDatasetDiffractionTab dataset={dataset} />,
    },
    {
      key: 'Autoprocessing',
      title: (
        <>
          Auto processing
          <Badge
            bg="info"
            style={{
              marginLeft: 5,
            }}
          >
            {processingsNb}
          </Badge>
        </>
      ),
      hidden: !processingsNb,
      content: (
        <MXAutoProcessingTable datasets={[dataset]} merged={'include'} />
      ),
    },
    getFilesTab(dataset, showPathInFiles, true),
    ...getPhasingTabs(dataset, showPathInFiles),
  ];
}

function getProcessedTabs(
  dataset: Dataset,
  showPathInFiles: boolean,
): TabDefinition[] {
  return [
    {
      key: 'Summary',
      title: 'Summary',
      content: <MXProcessedDatasetSummaryContent dataset={dataset} />,
    },
    getInputsTab(dataset),
    ...getPhasingTabs(dataset, showPathInFiles),
    getFilesTab(dataset, showPathInFiles, true),
  ];
}

function getPhasingTabs(dataset: Dataset, showPathInFiles: boolean) {
  const mrPhasing = getPhasings(dataset, 'MR');
  const sadPhasing = getPhasings(dataset, 'SAD');
  return [
    {
      key: 'MR',
      title: (
        <>
          MR
          <Badge
            bg="info"
            style={{
              marginLeft: 5,
            }}
          >
            {mrPhasing.length}
          </Badge>
        </>
      ),
      hidden: !mrPhasing.length,
      content: <MXPhasingList datasets={dataset} type="MR" />,
    },
    {
      key: 'SAD',
      title: (
        <>
          SAD
          <Badge
            bg="info"
            style={{
              marginLeft: 5,
            }}
          >
            {sadPhasing.length}
          </Badge>
        </>
      ),
      hidden: !sadPhasing.length,
      content: <MXPhasingList datasets={dataset} type="SAD" />,
    },
  ];
}
