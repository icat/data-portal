import { getDatasetParamValue, sortDatasetsByDate } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

export interface WorkflowInfo {
  id: string;
  name: string;
  type: string;
  status?: string;
}

export interface Workflow extends WorkflowInfo {
  workflow: true;
  datasets: Dataset[];
  startDate?: string;
  endDate?: string;
}

export const WORKFLOW_NAME_PARAM = 'Workflow_name';
export const WORKFLOW_TYPE_PARAM = 'Workflow_type';
export const WORKFLOW_ID_PARAM = 'Workflow_id';
export const WORKFLOW_STATUS_PARAM = 'Workflow_status';

export type WorkflowOrDataset = Workflow | Dataset;

export function isWorkflow(dataset: WorkflowOrDataset): dataset is Workflow {
  return (dataset as Workflow).workflow === true;
}

export function switchWorkflowOrDataset<A, B>(args: {
  value: WorkflowOrDataset;
  executeIfDataset: (dataset: Dataset) => A;
  executeIfWorkflow: (workflow: Workflow) => B;
}): A | B {
  const { value, executeIfDataset, executeIfWorkflow } = args;
  if (isWorkflow(value)) {
    return executeIfWorkflow(value);
  } else {
    return executeIfDataset(value);
  }
}

export function groupDatasetsByWorkflow(
  datasets?: Dataset[],
): WorkflowOrDataset[] {
  const workflows: Workflow[] = [];
  const datasetsWithoutWorkflow: Dataset[] = [];

  datasets?.forEach((dataset) => {
    const workflowInfo = getWorkflowInfo(dataset);

    if (workflowInfo) {
      const workflow = workflows.find((w) => w.id === workflowInfo.id);
      if (workflow) {
        workflow.datasets.push(dataset);
      } else {
        workflows.push({
          workflow: true,
          ...workflowInfo,
          datasets: [dataset],
        });
      }
    } else {
      datasetsWithoutWorkflow.push(dataset);
    }
  });

  workflows.forEach((workflow) => {
    workflow.datasets = sortDatasetsByDate(workflow.datasets);
    workflow.startDate = workflow.datasets[0].startDate;
    workflow.endDate = workflow.datasets[workflow.datasets.length - 1].endDate;
  });

  return sortDatasetsByDate([...workflows, ...datasetsWithoutWorkflow]);
}

export function getWorkflowInfo(dataset?: Dataset) {
  if (!dataset) return undefined;

  const workflowName = getDatasetParamValue(dataset, WORKFLOW_NAME_PARAM);
  const workflowType = getDatasetParamValue(dataset, WORKFLOW_TYPE_PARAM);
  const workflowStatus = getDatasetParamValue(dataset, WORKFLOW_STATUS_PARAM);
  const workflowId = getDatasetParamValue(dataset, WORKFLOW_ID_PARAM);
  if (workflowId && workflowName && workflowType) {
    return {
      id: workflowId,
      name: workflowName,
      type: workflowType,
      status: workflowStatus,
    };
  } else return undefined;
}
