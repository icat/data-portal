import {
  flattenOutputs,
  isAcquisition,
  sortDatasetsByDate,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

export function getActions(datasets: Dataset[]): Dataset[] {
  const acquisitions = flattenOutputs(datasets).filter(isAcquisition);

  return sortDatasetsByDate(acquisitions);
}
