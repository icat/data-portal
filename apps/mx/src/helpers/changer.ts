export function formatChangerPosition(position: string | number | undefined) {
  const numberPosition = Number(position);
  if (isNaN(numberPosition)) {
    return String(position ?? 'N/A');
  }

  const cell = Math.floor((numberPosition - 1) / 3);
  const cellPosition = (numberPosition - 1) % 3;
  return `${cell}-${cellPosition + 1}`;
}
