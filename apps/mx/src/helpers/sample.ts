import { getScanType } from '@edata-portal/core';
import { SampleContainer } from 'components/summary/helper';
import {
  PROCESSED_TYPE_AUTOPROCESSING,
  SCAN_TYPE_PARAM_COLLECTION,
} from 'constants/metadatakeys';
import { getProcessedOfType } from 'helpers/processed';

export function isSampleAnalysed(sample: SampleContainer): boolean {
  const datasets = sample.datasets || [];
  return datasets.length > 0;
}

export function isSampleCollected(sample: SampleContainer): boolean {
  const datasets = sample.datasets || [];
  if (isSampleProcessed(sample)) return true;

  return (
    datasets.filter(
      (dataset) => getScanType(dataset) === SCAN_TYPE_PARAM_COLLECTION,
    ).length > 0
  );
}

export function isSampleProcessed(sample: SampleContainer): boolean {
  const datasets = sample.datasets || [];
  return (
    getProcessedOfType(datasets, PROCESSED_TYPE_AUTOPROCESSING, false).length >
    0
  );
}
