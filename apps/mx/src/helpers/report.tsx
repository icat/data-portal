import {
  flattenOutputs,
  isSameScanType,
  getScanType,
  getGalleryItemByName,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';

export function getMXReportFile(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
) {
  const processedAction = flattenOutputs(dataset.outputDatasets || [])
    .filter(processedActionFilter ?? (() => true))
    .find((o) => isSameScanType(o, dataset));
  if (!processedAction) return undefined;
  const scanType = getScanType(processedAction);

  const file = getGalleryItemByName(processedAction, `${scanType}-report.json`);
  return file;
}

export function hasMXReportFile(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
) {
  return getMXReportFile(dataset, processedActionFilter) !== undefined;
}
