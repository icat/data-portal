import { first, getDatasetParamValue } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  MXMR_SPACE_GROUP,
  MXSAD_SPACE_GROUP,
  PHASING_MTZ_PARAMS,
  PHASING_PDB_PARAMS,
  PHASING_STEP_PARAMS,
} from 'constants/phasing';

export function getPhasingStepType(dataset: Dataset) {
  return findValueWithParams(dataset, PHASING_STEP_PARAMS);
}

export function getPDBFileName(dataset: Dataset) {
  return findValueWithParams(dataset, PHASING_PDB_PARAMS);
}

export function getMTZFileName(dataset: Dataset) {
  return findValueWithParams(dataset, PHASING_MTZ_PARAMS);
}

export function getPhasingSpaceGroup(dataset: Dataset) {
  return findValueWithParams(dataset, [MXMR_SPACE_GROUP, MXSAD_SPACE_GROUP]);
}

function findValueWithParams(dataset: Dataset, params: string[]) {
  return first(
    params
      .map((param) => getDatasetParamValue(dataset, param))
      .filter((v) => v?.length),
  );
}
