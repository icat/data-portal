import {
  getScanType,
  immutableArray,
  isProcessed,
  COLORS,
  getGradientValue,
  toCSSColor,
  getInputDatasetIds,
  isReprocessed,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  PROCESSED_TYPE_AUTOPROCESSING,
  PROCESSED_TYPE_QUALITY_INDICATOR,
} from 'constants/metadatakeys';
import type { MXShell } from 'constants/shell';
import {
  MXAutoProcessingStat,
  MX_AUTOPROCESSING_STAT_PARAM_PREFIX,
  MX_AUTOPROCESSING_STAT_THRESHOLDS,
} from 'constants/stats';
import type { MXAutoProcessingStatColorScaleValue } from 'hooks/autoProcessingStatColorScale';

export function getMXAutoProcessingStatParam(
  shell: MXShell,
  stat: MXAutoProcessingStat,
) {
  return `${MX_AUTOPROCESSING_STAT_PARAM_PREFIX}_${shell}_${stat}`;
}

/**
 * Retrieves all processed datasets of a specified type from a given list of datasets.
 *
 * @param {Dataset[]} datasets - The list of datasets to filter.
 * @param {string} type - The type of dataset to filter by.
 * @param {boolean} [sorted=true] - Whether the results should be sorted by `startDate` in descending order.
 * @returns {Dataset[]} - A list of datasets that match the given type and are processed or reprocessed.
 *
 * @description
 * This function recursively searches through the provided datasets and their output datasets,
 * returning only those that are either processed or reprocessed and match the specified type.
 *
 * If `sorted` is `true`, the datasets are sorted by `startDate` in descending order.
 * The function ensures immutability by working with a copied array.
 */

export function getProcessedOfType(
  datasets: Dataset[],
  type: string,
  sorted: boolean = true,
): Dataset[] {
  return immutableArray(datasets)
    .sort(
      sorted
        ? (a, b) => {
            return a.startDate.localeCompare(b.startDate);
          }
        : undefined,
    )
    .reverse()
    .flatMap((dataset) => {
      const filteredOutputDatasets = dataset.outputDatasets
        ? getProcessedOfType(dataset.outputDatasets, type, sorted)
        : [];
      const isOfType =
        (isProcessed(dataset) || isReprocessed(dataset)) &&
        getScanType(dataset) === type;
      return isOfType
        ? [dataset, ...filteredOutputDatasets]
        : [...filteredOutputDatasets];
    })
    .toArray();
}

export function getGradientForStat(
  shell: MXShell,
  stat: MXAutoProcessingStat,
  scale?: MXAutoProcessingStatColorScaleValue,
) {
  const thresholds = scale ? scale : MX_AUTOPROCESSING_STAT_THRESHOLDS[stat];

  const shellThresholds = 'good' in thresholds ? thresholds : thresholds[shell];

  if (
    shellThresholds &&
    shellThresholds.good !== undefined &&
    shellThresholds.bad !== undefined
  ) {
    const { bad, good } = shellThresholds;
    const average = (good + bad) / 2;

    const gradient = {
      [good]: COLORS.green,
      [bad]: COLORS.red,
      [average]: COLORS.yellow,
    };

    return gradient;
  }

  return undefined;
}

export function getColorForStat(
  shell: MXShell,
  stat: MXAutoProcessingStat,
  value: number | string | undefined,
  a: number = 1,
  scale?: MXAutoProcessingStatColorScaleValue,
): string | undefined {
  const numberValue = Number(value);
  if (isNaN(numberValue)) return undefined;

  const gradient = getGradientForStat(shell, stat, scale);
  if (gradient) {
    const color = getGradientValue(gradient, numberValue);
    return toCSSColor(color, a);
  }

  return undefined;
}

export function isMergedAutoProcessing(dataset: Dataset): boolean {
  return isAutoProcessing(dataset) && getInputDatasetIds(dataset).length > 1;
}

export function isAutoProcessing(dataset: Dataset): boolean {
  return (
    isProcessed(dataset) &&
    getScanType(dataset) === PROCESSED_TYPE_AUTOPROCESSING
  );
}

export function isQualityIndicator(dataset: Dataset): boolean {
  return (
    isProcessed(dataset) &&
    getScanType(dataset) === PROCESSED_TYPE_QUALITY_INDICATOR
  );
}
