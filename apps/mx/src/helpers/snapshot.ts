import {
  first,
  flattenOutputs,
  getGalleryItems,
  getScanType,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { isCollectionScanType } from 'constants/metadatakeys';

export enum SNAPSHOT_FILE_SIZE {
  SMALL,
  LARGE,
  ALL,
}

const DIFFRACTION_SNAPSHOTS_NAMES = {
  [SNAPSHOT_FILE_SIZE.SMALL]: [/.*diffraction(?!.*large).*?\.(jpg|jpeg|png)$/i], // filenames containing "diffraction" but not "large"
  [SNAPSHOT_FILE_SIZE.LARGE]: [/.*diffraction.*large.*\.(jpg|jpeg|png)$/i], // filenames containing both "diffraction" and "large"
  [SNAPSHOT_FILE_SIZE.ALL]: [/.*diffraction.*\.(jpg|jpeg|png)$/i],
};

const SAMPLE_SNAPSHOT_NAMES: RegExp[] = [/.*snapshot.*\.(jpg|jpeg|png)$/i];

const DOZOR_SNAPSHOT_NAMES: RegExp[] = [/.*dozor.*\.(jpg|jpeg|png)$/i];

function getSnapshots(
  dataset: Dataset,
  namePatterns: RegExp[],
  processedActionFilter?: (processed: Dataset) => boolean,
) {
  const datasets = [
    dataset,
    ...flattenOutputs(dataset.outputDatasets || []).filter(
      processedActionFilter ?? (() => true),
    ),
  ];

  const galleryItems = datasets.flatMap(getGalleryItems);

  return galleryItems.filter((item) => {
    return namePatterns.some((pattern) => pattern.test(item.name));
  });
}

export function getSampleSnapshots(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
) {
  return getSnapshots(dataset, SAMPLE_SNAPSHOT_NAMES, processedActionFilter);
}

export function getDiffractionSnapshots(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
  size: SNAPSHOT_FILE_SIZE = SNAPSHOT_FILE_SIZE.ALL,
) {
  const items = getSnapshots(
    dataset,
    DIFFRACTION_SNAPSHOTS_NAMES[size],
    processedActionFilter,
  );
  /** fallback in case no results are found for small or large */
  if (items.length === 0 && size !== SNAPSHOT_FILE_SIZE.ALL) {
    return getSnapshots(
      dataset,
      DIFFRACTION_SNAPSHOTS_NAMES[SNAPSHOT_FILE_SIZE.ALL],
      processedActionFilter,
    );
  }
  return items;
}

export function getActionSnapshot(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
) {
  const scanType = getScanType(dataset);

  if (isCollectionScanType(scanType))
    return getDozorSnapshot(dataset, processedActionFilter);

  return first(
    getSnapshots(
      dataset,
      [new RegExp(`${scanType}\\.(jpg|jpeg|png)$`, 'i')],
      processedActionFilter,
    ),
  );
}

export function getDozorSnapshot(
  dataset: Dataset,
  processedActionFilter?: (processed: Dataset) => boolean,
) {
  return first(
    getSnapshots(dataset, DOZOR_SNAPSHOT_NAMES, processedActionFilter),
  );
}
