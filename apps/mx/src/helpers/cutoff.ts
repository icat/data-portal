import { FilterBadgeItem, getParameterValue } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MX_SHELL_LABELS, MX_SHELLS, type MXShell } from 'constants/shell';
import {
  MX_AUTOPROCESSING_STAT_LABELS,
  MX_AUTOPROCESSING_STAT_RANKING_ORDER,
  MX_AUTOPROCESSING_STATS,
  type MXAutoProcessingStat,
} from 'constants/stats';
import { getMXAutoProcessingStatParam } from 'helpers/processed';
import type { MXSettings } from 'hooks/mxSettings';

/**
 * Generates a label for a given auto-processing cutoff statistic.
 * The label includes the shell name (if applicable), the statistic name,
 * and the comparison operator ('<' for lower-is-better, '>' for higher-is-better).
 *
 * @param {MXAutoProcessingStat} stat - The statistic type.
 * @param {MXShell} [shell] - The shell category (optional).
 * @returns {string} - Formatted label for the cutoff.
 */
export function getCutoffLabel(stat: MXAutoProcessingStat, shell?: MXShell) {
  const order = MX_AUTOPROCESSING_STAT_RANKING_ORDER[stat];
  return `${typeof shell !== 'undefined' ? MX_SHELL_LABELS[shell] : ''} ${
    MX_AUTOPROCESSING_STAT_LABELS[stat]
  } ${order === -1 ? '<' : '>'}`;
}

/**
 * Retrieves the cutoff value for a given statistic and shell.
 *
 * @param {MXSettings['cutoffs']} cutoffs - The cutoff settings.
 * @param {MXAutoProcessingStat} stat - The statistic type.
 * @param {MXShell} shell - The shell category.
 * @returns {number | undefined} - The cutoff value if available, otherwise `undefined`.
 */
export function getCutoffValue(
  cutoffs: MXSettings['cutoffs'],
  stat: MXAutoProcessingStat,
  shell: MXShell,
) {
  if (!cutoffs || !(stat in cutoffs)) return undefined;
  const statCutOffs = cutoffs[stat];
  if (!statCutOffs || !(shell in statCutOffs)) return undefined;
  return statCutOffs[shell];
}

/**
 * Updates the cutoff value for a given statistic and shell.
 *
 * @param {MXSettings['cutoffs']} cutoffs - The current cutoff settings.
 * @param {MXAutoProcessingStat} stat - The statistic type.
 * @param {MXShell} shell - The shell category.
 * @param {number | undefined} value - The new cutoff value (or `undefined` to remove it).
 * @returns {MXSettings['cutoffs']} - Updated cutoff settings.
 */
export function setCutOffValue(
  cutoffs: MXSettings['cutoffs'],
  stat: MXAutoProcessingStat,
  shell: MXShell,
  value: number | undefined,
) {
  const initialCutOffs = cutoffs ? { ...cutoffs } : {};
  const initialStatCutOffs =
    stat in initialCutOffs ? { ...initialCutOffs[stat] } : {};

  return {
    ...initialCutOffs,
    [stat]: {
      ...initialStatCutOffs,
      [shell]: value,
    },
  };
}

/**
 * Identifies unmatched cutoffs in a dataset by comparing values with predefined thresholds.
 *
 * @param {Dataset} dataset - The dataset to check.
 * @param {MXSettings['cutoffs']} cutoffs - The cutoff settings.
 * @returns {Array<{ stat: MXAutoProcessingStat; shell: MXShell }>} - A list of unmatched cutoffs.
 */
export function getUnmatchedCutoffs(
  dataset: Dataset,
  cutoffs: MXSettings['cutoffs'],
) {
  return MX_AUTOPROCESSING_STATS.flatMap((stat) => {
    return MX_SHELLS.filter((shell) => {
      const value = getCutoffValue(cutoffs, stat, shell);
      if (value === undefined) return false; // No cutoff set

      const param = getMXAutoProcessingStatParam(shell, stat);
      const datasetValueString = getParameterValue(dataset, param);
      const datasetValue = Number(datasetValueString);

      if (Number.isNaN(datasetValue)) return true; // Invalid or missing dataset value

      const order = MX_AUTOPROCESSING_STAT_RANKING_ORDER[stat];
      return order === -1 ? datasetValue < value : datasetValue > value;
    }).map((shell) => ({ stat, shell }));
  });
}

/**
 * Generates a filter badge item for a specific cutoff value.
 *
 * @param {MXAutoProcessingStat} stat - The statistic type.
 * @param {MXShell} shell - The shell category.
 * @param {MXSettings} settings - The settings object.
 * @param {(settings: Partial<MXSettings>) => void} setSettings - Function to update settings.
 * @returns {FilterBadgeItem} - A badge representing the cutoff filter.
 */
export function getCutoffFilterBadgeItem(
  stat: MXAutoProcessingStat,
  shell: MXShell,
  settings: MXSettings,
  setSettings: (settings: Partial<MXSettings>) => void,
): FilterBadgeItem {
  const cutoffs = settings.cutoffs;
  const value = getCutoffValue(cutoffs, stat, shell);

  return {
    name: `${getCutoffLabel(stat, shell)} ${value}`,
    hide: value === undefined, // Hide if there's no value
    onRemove: () => {
      setSettings({
        cutoffs: setCutOffValue(cutoffs, stat, shell, undefined),
      });
    },
  };
}

/**
 * Generates an array of filter badge items for all active cutoff values.
 *
 * @param {MXSettings} settings - The settings object.
 * @param {(settings: Partial<MXSettings>) => void} setSettings - Function to update settings.
 * @returns {FilterBadgeItem[]} - An array of badge items representing active cutoff filters.
 */
export function getCutOffsFilterBadgeItems(
  settings: MXSettings,
  setSettings: (settings: Partial<MXSettings>) => void,
): FilterBadgeItem[] {
  const cutoffs = settings.cutoffs;
  if (!cutoffs) return [];

  return (Object.keys(cutoffs) as (keyof typeof cutoffs)[])
    .filter((stat) => MX_AUTOPROCESSING_STATS.includes(stat as any)) // Ensure valid stats
    .flatMap((stat) => {
      const statCutOffs = cutoffs[stat];
      if (!statCutOffs) return [];
      return (
        Object.keys(statCutOffs) as (keyof (typeof cutoffs)[typeof stat])[]
      )
        .filter((key) => MX_SHELLS.includes(key as any)) // Ensure valid shells
        .map((shell) => {
          return getCutoffFilterBadgeItem(stat, shell, settings, setSettings);
        });
    });
}
