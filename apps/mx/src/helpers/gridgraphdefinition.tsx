import {
  GridGraphCell,
  first,
  getInputDatasetIds,
  getParameterValue,
  getScanType,
  sortDatasetsByDate,
  type GridGraphDefinition,
  type GridGraphItem,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { MXDatasetGridNodeGroupHeader } from 'components/datasetgrid/nodes/MXDatasetGridNodeGroupHeader';
import { MXBestAutoProcessingSnapshot } from 'components/processed/AutoProcessing/MXBestAutoProcessingSnapshot';
import { MXBestPhasingSnapshot } from 'components/processed/phasing/MXBestPhasingSnapshot';
import {
  isCollectionScanType,
  SCAN_TYPE_PARAM_CHARACTERISATION,
  SCAN_TYPE_PARAM_COLLECTION,
} from 'constants/metadatakeys';
import type {
  DatasetGridGroupingCriterion,
  DatasetGridGroupingCriterionActionsSequence,
  DatasetGridGroupingCriterionProcessedAction,
} from 'helpers/datasetgridgroupingconfig';
import { getWorkflowInfo } from 'helpers/workflow';
import { getAutoProcessing } from 'hooks/autoProcessingRanking';
import { hasMXViewFilter, MXSettings } from 'hooks/mxSettings';
import { getBestPhasing } from 'hooks/phasingRanking';

export function getMXGridGraphDefinition(
  datasets: Dataset[],
  criteria: DatasetGridGroupingCriterion[],
  mxSettings: MXSettings,
  bestMerged?: Dataset,
): GridGraphDefinition {
  return {
    root: computeItemWithCriteria(datasets, criteria, mxSettings),
    mergedItem: bestMerged
      ? {
          content: {
            id: bestMerged.id.toString(),
            type: 'cell',
            content: {
              type: 'element',
              element: (
                <MXBestAutoProcessingSnapshot
                  datasets={datasets}
                  merged="only"
                />
              ),
            },
          },
          type: 'merged',
          toIds: getInputDatasetIds(bestMerged),
        }
      : undefined,
  };
}

function computeItemWithCriteria(
  datasets: Dataset[],
  criteria: DatasetGridGroupingCriterion[],
  mxSettings: MXSettings,
): GridGraphItem {
  const criterion = first(criteria);
  if (!criterion) {
    return {
      id: first(datasets)?.id.toString() || 'empty',
      type: 'row',
      items: datasets.map((dataset) =>
        itemFromDataset(dataset, datasets, mxSettings),
      ),
    };
  }
  const nextCriteria = criteria.slice(1);
  switch (criterion.type) {
    case 'processed_action':
      return groupDatasetsByCriterionProcessedAction(
        datasets,
        criterion,
        nextCriteria,
        mxSettings,
      );
    case 'actions_sequence':
      return groupDatasetsByCriterionActionsSequence(
        datasets,
        criterion,
        nextCriteria,
        mxSettings,
      );
  }
}

function groupDatasetsByCriterionProcessedAction(
  datasets: Dataset[],
  criterion: DatasetGridGroupingCriterionProcessedAction,
  nextCriteria: DatasetGridGroupingCriterion[],
  mxSettings: MXSettings,
): GridGraphItem {
  //find all actions that have one or more value for the criterion (they are the source of the groups)
  const sourceActions = datasets.flatMap((dataset) => {
    const values = getCriterionValuesForDataset(dataset, criterion);
    return values
      ? values.map((value) => ({
          value,
          dataset,
        }))
      : [];
  });

  if (!sourceActions?.length)
    return computeItemWithCriteria(datasets, nextCriteria, mxSettings);

  //these actions are not part of any group, they will be displayed inline before the groups
  const inlineActions = datasets
    .filter((dataset) => {
      return sourceActions.every(
        (source) =>
          !datasetIsPartOfValueGroup(source.value, criterion, dataset) &&
          source.dataset.id !== dataset.id,
      );
    })
    .map((d) => itemFromDataset(d, datasets, mxSettings));

  //for each source action, group the datasets that have the same value for the criterion
  const groups = sourceActions.map((source) => {
    const groupItems = datasets.filter(
      (dataset) =>
        datasetIsPartOfValueGroup(source.value, criterion, dataset) &&
        !sourceActions.some((s) => s.dataset.id === dataset.id),
    );

    const res: GridGraphItem = {
      id: source.value,
      type: 'row',
      items: [
        itemFromDataset(source.dataset, datasets, mxSettings, {
          parameter: criterion.parameter,
          value: source.value,
        }),
        computeItemWithCriteria(groupItems, nextCriteria, mxSettings),
      ],
    };
    return res;
  });

  const idPrefix = `${first(datasets)?.id.toString() || 'empty'}-${criterion.parameter}-${criterion.scanType}`;

  return {
    id: idPrefix + '-row',
    type: 'row',
    items: [
      ...inlineActions,
      {
        id: idPrefix + '-groups',
        type: 'column',
        items: groups,
        collapsible: true,
        collapsedLabel: criterion.groupType,
      },
    ],
  };
}

function groupDatasetsByCriterionActionsSequence(
  datasets: Dataset[],
  criterion: DatasetGridGroupingCriterionActionsSequence,
  nextCriteria: DatasetGridGroupingCriterion[],
  mxSettings: MXSettings,
): GridGraphItem {
  const sourceValues = Array.from(
    new Set(
      datasets
        .map((dataset) => getParameterValue(dataset, criterion.parameter))
        .filter((v): v is string => v !== undefined),
    ),
  );
  if (!sourceValues?.length || sourceValues.length === 1)
    return computeItemWithCriteria(datasets, nextCriteria, mxSettings);

  const ungroupedActions = datasets
    .filter((dataset) => {
      const paramValue = getParameterValue(dataset, criterion.parameter);
      return sourceValues.every((v) => v !== paramValue);
    })
    .map((d) => itemFromDataset(d, datasets, mxSettings));

  const groups = sourceValues.map((value) => {
    const groupItems = datasets.filter(
      (dataset) => getParameterValue(dataset, criterion.parameter) === value,
    );
    const res: GridGraphItem = {
      id: value,
      type: 'row',
      items: [
        {
          id: value,
          type: 'cell',
          content: {
            type: 'element',
            element: (
              <MXDatasetGridNodeGroupHeader>
                {criterion.getHeaderForParamValue(value)}
              </MXDatasetGridNodeGroupHeader>
            ),
          },
        },
        computeItemWithCriteria(groupItems, nextCriteria, mxSettings),
      ],
    };
    return res;
  });

  const idPrefix = `${first(datasets)?.id.toString() || 'empty'}-${criterion.parameter}`;

  return {
    id: idPrefix + '-row',
    type: 'row',
    items: [
      ...ungroupedActions,
      {
        id: idPrefix + '-groups',
        type: 'column',
        items: groups,
        collapsible: true,
        collapsedLabel: criterion.groupType,
      },
    ],
  };
}

function itemFromDataset(
  dataset: Dataset,
  allDatasets: Dataset[],
  mxSettings: MXSettings,
  processedActionFilter?: {
    parameter: string;
    value: string;
  },
): GridGraphItem {
  const scanType = isCollectionScanType(getScanType(dataset))
    ? SCAN_TYPE_PARAM_COLLECTION
    : getScanType(dataset);

  const enableMainSnapshot = hasMXViewFilter(scanType, 'Indicator', mxSettings);
  const enableSampleSnapshot = hasMXViewFilter(scanType, 'Sample', mxSettings);
  const enableDiffractionSnapshot = hasMXViewFilter(
    scanType,
    'Diffraction',
    mxSettings,
  );
  const enableAutoprocessing = hasMXViewFilter(
    scanType,
    'Auto processing',
    mxSettings,
  );
  const enablePhasing = hasMXViewFilter(scanType, 'Phasing', mxSettings);

  const isFollowedByCharacterisationOrDc = sortDatasetsByDate(allDatasets)
    .filter(
      (item, index, array) => index > array.indexOf(dataset), // get all datasets after the current one
    )
    .find((item) => {
      const scanTypeItem = getScanType(item);
      return (
        scanTypeItem &&
        [SCAN_TYPE_PARAM_COLLECTION, SCAN_TYPE_PARAM_CHARACTERISATION].includes(
          scanTypeItem,
        )
      );
    });

  const isWorkflow = getWorkflowInfo(dataset) !== undefined;

  const showSampleSnapshot = enableSampleSnapshot && !isWorkflow;
  const showDiffractionSnapshot =
    enableDiffractionSnapshot &&
    (!isWorkflow || !isFollowedByCharacterisationOrDc);

  const processedActionFilterFunction = processedActionFilter
    ? (v: Dataset) => {
        return (
          getParameterValue(v, processedActionFilter.parameter) ===
          processedActionFilter.value
        );
      }
    : undefined;

  const datasetCell: GridGraphCell = {
    id: dataset.id.toString(),
    type: 'cell',
    content: {
      type: 'dataset',
      dataset,
      renderProps: {
        showSampleSnapshot,
        showDiffractionSnapshot,
        showMainSnapshot: enableMainSnapshot,
        processedActionFilter: processedActionFilterFunction,
      },
    },
  };

  const isProcessed = getAutoProcessing(dataset, 'exclude', false).length > 0;
  const isPhased =
    getBestPhasing(dataset, 'MR') !== undefined ||
    getBestPhasing(dataset, 'SAD') !== undefined;

  const details: GridGraphCell[] = [
    datasetCell,
    ...(isProcessed && enableAutoprocessing
      ? [
          {
            id: dataset.id.toString() + '-processing',
            type: 'cell',
            content: {
              type: 'element',
              element: (
                <div className="d-flex h-100 w-100">
                  <MXBestAutoProcessingSnapshot
                    datasets={[dataset]}
                    merged="exclude"
                  />
                </div>
              ),
            },
          } as GridGraphCell,
        ]
      : []),
    ...(isPhased && enablePhasing
      ? [
          {
            id: dataset.id.toString() + '-phasing',
            type: 'cell' as const,
            content: {
              type: 'element' as const,
              element: (
                <div className="d-flex h-100 w-100">
                  <MXBestPhasingSnapshot datasets={dataset} />
                </div>
              ),
            },
          },
        ]
      : []),
  ];

  if (details.length <= 1) return datasetCell;

  return {
    id: dataset.id.toString() + '-row',
    type: 'row',
    items: details,
  };
}

function getCriterionValuesForDataset(
  dataset: Dataset,
  criterion: DatasetGridGroupingCriterionProcessedAction,
): undefined | string[] {
  const scanType = getScanType(dataset);
  if (scanType !== criterion.scanType) return undefined;
  if (!dataset.outputDatasets?.length) return undefined;

  return dataset.outputDatasets
    .flatMap((outputDataset) => {
      return getParameterValue(outputDataset, criterion.parameter);
    })
    .filter((v): v is string => v !== undefined);
}

function datasetIsPartOfValueGroup(
  value: string,
  criterion: DatasetGridGroupingCriterionProcessedAction,
  dataset: Dataset,
) {
  const datasetParamValue = getParameterValue(dataset, criterion.parameter);
  return value === datasetParamValue;
}
