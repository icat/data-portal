import { getParameterValue } from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { WORKFLOW_TYPE_PARAM } from 'helpers/workflow';

export type DatasetGridGroupingCriterionProcessedAction = {
  type: 'processed_action';
  scanType: string;
  parameter: string;
  groupType: string;
};
export type DatasetGridGroupingCriterionActionsSequence = {
  type: 'actions_sequence';
  parameter: string;
  getHeaderForParamValue: (value: string) => string;
  groupType: string;
};

export type DatasetGridGroupingCriterion =
  | DatasetGridGroupingCriterionProcessedAction
  | DatasetGridGroupingCriterionActionsSequence;

const GROUPING_CRITERIA_POSITION: DatasetGridGroupingCriterion = {
  type: 'actions_sequence',
  parameter: 'MX_position_id',
  getHeaderForParamValue: (value: string) => `Position ${value}`,
  groupType: 'positions',
};

const GROUPING_CRITERIA_KAPPA_POSITION: DatasetGridGroupingCriterion = {
  type: 'actions_sequence',
  parameter: 'MX_kappa_settings_id',
  getHeaderForParamValue: (value: string) => `Kappa ${value}`,
  groupType: 'kappa settings',
};

const GROUPING_CRITERIA_CHARACTERISATION: DatasetGridGroupingCriterion = {
  type: 'processed_action',
  scanType: 'characterisation',
  parameter: 'MX_characterisation_id',
  groupType: 'characterisations',
};

const DATASET_GRID_GROUPING_CONFIG_MXPRESS_P: DatasetGridGroupingCriterion[] = [
  GROUPING_CRITERIA_CHARACTERISATION,
  GROUPING_CRITERIA_POSITION,
  GROUPING_CRITERIA_KAPPA_POSITION,
];

const DATASET_GRID_GROUPING_CONFIG_DEFAULT: DatasetGridGroupingCriterion[] = [
  GROUPING_CRITERIA_POSITION,
  GROUPING_CRITERIA_CHARACTERISATION,
  GROUPING_CRITERIA_KAPPA_POSITION,
];

const WORKFLOW_CONFIG = {
  MXPressP: DATASET_GRID_GROUPING_CONFIG_MXPRESS_P,
  default: DATASET_GRID_GROUPING_CONFIG_DEFAULT,
} as const;

/**
 * Get the grouping configuration for the workflow type of the datasets.
 * i.e. the ordered criteria to group the datasets in the grid.
 * TODO: update this to use `group_by` parameter when it is defined by the workflows.
 */
export function getDatasetGridConfigForDatasets(
  datasets: Dataset[],
): DatasetGridGroupingCriterion[] {
  const workflowConfigs = datasets
    .map((dataset) => getParameterValue(dataset, WORKFLOW_TYPE_PARAM))
    .filter((v): v is string => v !== undefined)
    .map((workflow) =>
      workflow in WORKFLOW_CONFIG
        ? WORKFLOW_CONFIG[workflow as keyof typeof WORKFLOW_CONFIG]
        : WORKFLOW_CONFIG.default,
    )
    .filter((workflow, index, self) => self.indexOf(workflow) === index);

  if (!workflowConfigs.length) {
    return WORKFLOW_CONFIG.default;
  }

  if (workflowConfigs.length > 1) {
    console.error(
      'Inconsistent workflow configs found in datasets',
      datasets,
      workflowConfigs,
    );
  }

  return workflowConfigs[0];
}
