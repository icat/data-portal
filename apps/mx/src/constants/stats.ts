import type { MXShell } from 'constants/shell';

export const MX_AUTOPROCESSING_STAT_PARAM_PREFIX =
  'MXAutoprocIntegrationScaling';

export const MX_AUTOPROCESSING_STAT_COMPLETENESS = 'completeness';
export const MX_AUTOPROCESSING_STAT_RESOLUTION_LOW = 'resolution_limit_low';
export const MX_AUTOPROCESSING_STAT_RESOLUTION_HIGH = 'resolution_limit_high';
export const MX_AUTOPROCESSING_STAT_RMEAS = 'r_meas_all_IPlus_IMinus';
export const MX_AUTOPROCESSING_STAT_ISIGMA = 'mean_I_over_sigI';
export const MX_AUTOPROCESSING_STAT_CC12 = 'cc_half';
export const MX_AUTOPROCESSING_STAT_CCANO = 'cc_ano';

export const MX_AUTOPROCESSING_STATS = [
  MX_AUTOPROCESSING_STAT_COMPLETENESS,
  MX_AUTOPROCESSING_STAT_RESOLUTION_LOW,
  MX_AUTOPROCESSING_STAT_RESOLUTION_HIGH,
  MX_AUTOPROCESSING_STAT_RMEAS,
  MX_AUTOPROCESSING_STAT_ISIGMA,
  MX_AUTOPROCESSING_STAT_CC12,
  MX_AUTOPROCESSING_STAT_CCANO,
] as const;

export type MXAutoProcessingStat = (typeof MX_AUTOPROCESSING_STATS)[number];

export const MX_AUTOPROCESSING_STAT_RANKING_ORDER: {
  [key in MXAutoProcessingStat]: number;
} = {
  mean_I_over_sigI: -1,
  r_meas_all_IPlus_IMinus: 1,
  cc_ano: -1,
  cc_half: -1,
  completeness: -1,
  resolution_limit_high: 1,
  resolution_limit_low: -1,
};

export const MX_AUTOPROCESSING_STAT_LABELS: {
  [key in MXAutoProcessingStat]: string;
} = {
  completeness: 'Compl.',
  resolution_limit_low: 'Res. low',
  resolution_limit_high: 'Res. high',
  r_meas_all_IPlus_IMinus: 'Rmeas',
  mean_I_over_sigI: 'I/s(I)',
  cc_half: 'cc1/2',
  cc_ano: 'ccAno',
};

export const MX_AUTOPROCESSING_STAT_THRESHOLDS: {
  [key in MXAutoProcessingStat]:
    | {
        good: number | undefined;
        bad: number | undefined;
      }
    | {
        [key in MXShell]: {
          good: number | undefined;
          bad: number | undefined;
        };
      };
} = {
  completeness: {
    good: 90,
    bad: 50,
  },
  resolution_limit_low: {
    good: undefined,
    bad: undefined,
  },
  resolution_limit_high: {
    good: undefined,
    bad: undefined,
  },
  r_meas_all_IPlus_IMinus: {
    inner: {
      good: 5,
      bad: 15,
    },
    outer: {
      good: 30,
      bad: 50,
    },
    overall: {
      good: 10,
      bad: 25,
    },
  },
  mean_I_over_sigI: {
    inner: {
      good: 10,
      bad: 5,
    },
    outer: {
      good: 1.5,
      bad: 0.5,
    },
    overall: {
      good: 10,
      bad: 5,
    },
  },
  cc_half: {
    inner: {
      good: 0.99,
      bad: 0.9,
    },
    outer: {
      good: 0.4,
      bad: 0.1,
    },
    overall: {
      good: 0.95,
      bad: 0.9,
    },
  },
  cc_ano: {
    inner: {
      good: 80,
      bad: 30,
    },
    outer: {
      good: 10,
      bad: 0,
    },
    overall: {
      good: 30,
      bad: 0,
    },
  },
};
