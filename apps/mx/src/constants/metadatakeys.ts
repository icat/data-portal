export const SCAN_TYPE_PARAM_COLLECTION = 'datacollection';
export const SCAN_TYPE_PARAM_HELICAL = 'Helical';

export function isCollectionScanType(scanType: string | undefined) {
  return (
    scanType === SCAN_TYPE_PARAM_COLLECTION ||
    scanType === SCAN_TYPE_PARAM_HELICAL
  );
}

export const SCAN_TYPE_PARAM_CHARACTERISATION = 'characterisation';
export const SCAN_TYPE_PARAM_AUTOMESH = 'automesh';
export const SCAN_TYPE_PARAM_LINE = 'line';
export const SCAN_TYPE_PARAM_MESH = 'mesh';

export const PROCESSED_PROGRAM_PARAM = 'Process_program';

export const PROCESSED_TYPE_QUALITY_INDICATOR = 'qualityIndicator';
export const PROCESSED_TYPE_AUTOPROCESSING = 'integration';
export const PROCESSED_TYPE_DIFFRACTION_THUMBNAIL = 'diffraction_thumbnail';

export const MX_AUTOPROCESSING_SPACE_GROUP =
  'MXAutoprocIntegration_space_group';
export const MX_AUTOPROCESSING_IS_ANOMALOUS = 'MXAutoprocIntegration_anomalous';
export const MX_AUTOPROCESSING_CELL_PREFIX = 'MXAutoprocIntegration_cell';

export const MX_POSITION_NAME_PARAM = 'MX_positionName';

export const MX_SAMPLE_CONTAINER_ID_PARAM = 'SampleTrackingContainer_id';

export const MX_SAMPLE_CONTAINER_POSITION_PARAM =
  'SampleTrackingContainer_position';
