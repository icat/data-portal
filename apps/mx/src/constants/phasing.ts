import { COLORS, type ColorGradient } from '@edata-portal/core';

export const SAD_FRAGMENT_GRADIENT: ColorGradient = {
  0: COLORS.red,
  5: COLORS.yellow,
  20: COLORS.green,
};

export const SAD_CC_GRADIENT: ColorGradient = {
  0: COLORS.red,
  20: COLORS.yellow,
  40: COLORS.green,
};

export const MR_TFZ_GRADIENT_MONOCLINIC: ColorGradient = {
  5: COLORS.red,
  5.5: COLORS.yellow,
  6: COLORS.green,
};

export const MR_TFZ_GRADIENT: ColorGradient = {
  5: COLORS.red,
  6: COLORS.orange,
  6.5: COLORS.yellow,
  7: COLORS.light_green,
  8: COLORS.green,
};

export const SAD_PHASING_SCANTYPE = 'SAD';
export const MR_PHASING_SCANTYPE = 'MR';

export const PHASING_TYPES = [
  SAD_PHASING_SCANTYPE,
  MR_PHASING_SCANTYPE,
] as const;

export const MR_PHASING_STEP = 'PHASING';
export const MR_REFINEMENT_STEP = 'REFINEMENT';
export const MR_LIGAND_FITTING_STEP = 'LIGAND_FIT';

export const SAD_PREPARE_STEP = 'PREPARE';
export const SAD_PHASING_STEP = 'PHASING';
export const SAD_MODEL_BUILDING_STEP = 'MODELBUILDING';

export const PHASING_STEP_PARAMS = ['MXMR_step', 'MXSAD_step'];
export const PHASING_PDB_PARAMS = [
  'MXMRPhasing_PDB_file',
  'MXMRRefinement_PDB_file',
  'MXMRLigandFitting_PDB_file',
];
export const PHASING_MTZ_PARAMS = [
  'MXMRPhasing_MTZ_file',
  'MXMRRefinement_MTZ_file',
  'MXMRLigandFitting_MTZ_file',
];

export const MXMR_MIN_RESOLUTION = 'MXMR_min_resolution';
export const MXMR_MAX_RESOLUTION = 'MXMR_max_resolution';
export const MXMR_SPACE_GROUP = 'MXMR_space_group';

export const MXMR_PHASING_SOURCE = 'MXMRPhasing_source';
export const MXMR_PHASING_SEARCH_MODEL = 'MXMRPhasing_search_model';

export const MXMR_PHASING_NB_SEARCH_MODELS_FOUND =
  'MXMRPhasing_number_of_search_models_found';
export const MXMR_PHASING_BEST_RFZ = 'MXMRPhasing_best_RFZ';
export const MXMR_PHASING_BEST_TFZ = 'MXMRPhasing_best_TFZ';
export const MXMR_PHASING_LLG = 'MXMRPhasing_LLG';

export const MXMR_REFINEMENT_R_CRIST = 'MXMRRefinement_R_cryst';
export const MXMR_REFINEMENT_R_FREE = 'MXMRRefinement_R_free';

export const MXMR_LIGAND_FITTING_LIGAND_XYZ = 'MXMRLigandFitting_ligand_XYZ';
export const MXMR_LIGAND_FITTING_LIGAND_NAME = 'MXMRLigandFitting_ligand_name';
export const MXMR_LIGAND_FITTING_FOFC_CC = 'MXMRLigandFitting_ligand_FOFC_CC';
export const MXMR_LIGAND_FITTING_R_FREE = 'MXMRLigandFitting_R_free';
export const MXMR_LIGAND_FITTING_R_CRYST = 'MXMRLigandFitting_R_cryst';
export const MXMR_LIGAND_FITTING_B_FACTOR = 'MXMRLigandFitting_B_factor';

export const MXSAD_MIN_RESOLUTION = 'MXSAD_min_resolution';
export const MXSAD_MAX_RESOLUTION = 'MXSAD_max_resolution';
export const MXSAD_SPACE_GROUP = 'MXSAD_space_group';

export const MXSAD_ENANTIOMORPH = 'MXSAD_enantiomorph';
export const MXSAD_SOLVENT = 'MXSAD_solvent';

export const MXSAD_PSEUDO_FREE_CC = 'MXSAD_pseudo_free_cc';

export const MXSAD_CC_PARTIAL_MODEL = 'MXSAD_cc_partial_model';
export const MXSAD_AVERAGE_FRAGMENT_LENGTH = 'MXSAD_average_fragment_length';
export const MXSAD_CHAIN_COUNT = 'MXSAD_chain_count';
export const MXSAD_RESIDUES_COUNT = 'MXSAD_residues_count';
