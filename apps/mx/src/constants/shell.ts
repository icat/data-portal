export const MX_INNER_SHELL = 'inner';
export const MX_OUTER_SHELL = 'outer';
export const MX_OVERALL_SHELL = 'overall';

export const MX_SHELLS = [
  MX_INNER_SHELL,
  MX_OUTER_SHELL,
  MX_OVERALL_SHELL,
] as const;

export type MXShell = (typeof MX_SHELLS)[number];

export const MX_SHELL_COLORS: {
  [key in MXShell]: string;
} = {
  inner: 'purple',
  outer: 'green',
  overall: 'blue',
};

export const MX_SHELL_LABELS: {
  [key in MXShell]: string;
} = {
  inner: 'Inner',
  outer: 'Outer',
  overall: 'Overall',
};
