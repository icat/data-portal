import {
  AuthenticatedAPIProvider,
  AuthenticatorProvider,
  OpenIDProvider,
  SideNavProvider,
  UnauthenticatedAPIProvider,
} from '@edata-portal/core';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { AppRouter } from 'standalone/routing/AppRouter';
import { ViewersProvider } from 'standalone/providers/ViewersProvider';
import { ConfigProvider } from 'standalone/providers/ConfigProvider';
import { Navigation } from 'standalone/routing/Navigation';
import 'standalone/scss/main.scss';

const queryClient = new QueryClient();

function App() {
  return (
    <>
      <h1>MX Microfrontend Development</h1>
      <QueryClientProvider client={queryClient}>
        <ConfigProvider>
          <AppContextProviders>
            <ViewersProvider>
              <SideNavProvider>
                <AppRouter>
                  <Navigation />
                </AppRouter>
              </SideNavProvider>
            </ViewersProvider>
          </AppContextProviders>
        </ConfigProvider>
      </QueryClientProvider>
    </>
  );
}

function AppContextProviders({ children }: { children: React.ReactNode }) {
  return (
    <UnauthenticatedAPIProvider>
      <AuthenticatorProvider>
        <OpenIDProvider>
          <AuthenticatedAPIProvider>{children}</AuthenticatedAPIProvider>
        </OpenIDProvider>
      </AuthenticatorProvider>
    </UnauthenticatedAPIProvider>
  );
}

export default App;
