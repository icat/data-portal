import { defineConfig, loadEnv } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import federation from '@originjs/vite-plugin-federation';
import { visualizer } from 'rollup-plugin-visualizer';

// https://vitejs.dev/config/
export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  return defineConfig({
    build: {
      modulePreload: false,
      target: 'esnext',
      minify: mode === 'prod' ? 'terser' : false,
      cssCodeSplit: false,
      rollupOptions: {
        input: {
          main: './index.html',
        },
      },
      sourcemap: false,
    },
    plugins: [
      react(),
      visualizer({
        filename: 'dist/stats.html',
        open: false,
      }),
      tsconfigPaths(),
      federation({
        name: 'app',
        remotes: {
          // "Dummy to avoid error - ReferenceError: __rf_placeholder__shareScope is not defined"
          dummy: 'dummy.js',
        },
        shared: [
          'react',
          'react-dom',
          '@tanstack/react-query',
          'react-router-dom',
          '@edata-portal/core',
          '@edata-portal/h5',
        ],
      }),
    ],
    server: {
      port: 3000,
      host: true,
    },
    define: {
      'process.env': process.env,
    },
  });
};
