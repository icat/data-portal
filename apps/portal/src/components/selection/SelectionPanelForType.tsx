import {
  NoData,
  DatasetList,
  Loading,
  useUser,
  SelectEntityType,
  useSelection,
  DatasetViewerType,
} from '@edata-portal/core';
import {
  useEndpointURL,
  DATA_DOWNLOAD_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { Suspense } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function SelectionPanelForType({
  type,
  hideReprocess,
  linkToSelectionPage,
  hideDownload,
  hideMintDOI,
  datasetViewerType,
}: {
  type: SelectEntityType;
  hideReprocess?: boolean;
  linkToSelectionPage?: boolean;
  hideDownload?: boolean;
  hideMintDOI?: boolean;
  datasetViewerType?: DatasetViewerType; // This forces the viewer type independently of their remote configuration
}) {
  const { value: selection, clearSelection } = useSelection(type);

  const content = !selection.length ? (
    <NoData />
  ) : type === 'dataset' ? (
    <DatasetList
      groupBy="dataset"
      datasetIds={selection}
      showInvestigation
      datasetViewerType={datasetViewerType}
    />
  ) : (
    <DatasetList groupBy="sample" sampleIds={selection} showInvestigation />
  );

  return (
    <Col>
      <Row className="g-2">
        <Col xs={'auto'}>
          <Button
            variant="danger"
            disabled={selection.length === 0}
            onClick={clearSelection}
            size="sm"
          >
            Clear selection
          </Button>
        </Col>
        {type === 'dataset' && (
          <DatasetSelectionActions
            hideReprocess={hideReprocess}
            linkToSelectionPage={linkToSelectionPage}
            hideDownload={hideDownload}
            hideMintDOI={hideMintDOI}
          />
        )}
      </Row>
      <Suspense fallback={<Loading />}>{content}</Suspense>
    </Col>
  );
}

function DatasetSelectionActions({
  hideReprocess,
  linkToSelectionPage,
  hideDownload,
  hideMintDOI,
}: {
  hideReprocess?: boolean;
  linkToSelectionPage?: boolean;
  hideDownload?: boolean;
  hideMintDOI?: boolean;
}) {
  const user = useUser();

  const { value: selection } = useSelection('dataset');
  const downloadUrl = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  return (
    <>
      {linkToSelectionPage ? (
        <Col xs={'auto'}>
          <Link to="/selection">
            <Button
              variant="primary"
              disabled={selection.length === 0}
              size="sm"
            >
              Manage selection
            </Button>
          </Link>
        </Col>
      ) : null}
      {hideDownload ? null : (
        <Col xs={'auto'}>
          <Button
            variant="primary"
            disabled={selection.length === 0}
            target="_blank"
            href={downloadUrl({
              datasetIds: selection.join(','),
            })}
            size="sm"
          >
            Download all
          </Button>
        </Col>
      )}
      {!user?.isAuthenticated || hideReprocess ? null : (
        <Col xs={'auto'}>
          <Link to="/jobs/new">
            <Button
              variant="primary"
              disabled={selection.length === 0}
              size="sm"
            >
              Reprocess
            </Button>
          </Link>
        </Col>
      )}
      {!user?.isAuthenticated || hideMintDOI ? null : (
        <>
          <Col xs={'auto'}>
            <Link to="/doi/new">
              <Button
                variant="primary"
                disabled={selection.length === 0}
                size="sm"
              >
                Mint DOI
              </Button>
            </Link>
          </Col>
          <Col xs={'auto'}>
            <Link to="/doi/reserve">
              <Button
                variant="primary"
                disabled={selection.length === 0}
                size="sm"
              >
                Reserve DOI
              </Button>
            </Link>
          </Col>
        </>
      )}
    </>
  );
}
