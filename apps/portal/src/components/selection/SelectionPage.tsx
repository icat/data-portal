import { useConfig, Config, useSelection } from '@edata-portal/core';
import { SelectionPanelForType } from 'components/selection/SelectionPanelForType';
import { EmptySelection } from 'components/selection/EmptySelection';

export default function SelectionPage() {
  const { value: selectedDatasets } = useSelection('dataset');

  const config: Config = useConfig();

  if (selectedDatasets.length === 0) {
    return <EmptySelection />;
  }

  const hideReprocess = !config.ui.features.reprocessing;

  return (
    <SelectionPanelForType
      type={'dataset'}
      hideReprocess={hideReprocess}
      datasetViewerType="generic" // This forces the viewer type independently of their remote configuration
    />
  );
}
