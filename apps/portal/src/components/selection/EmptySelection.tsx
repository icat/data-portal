import { Alert, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function EmptySelection() {
  return (
    <Alert variant="info">
      Your selection is empty:
      <Link to="/experiments">
        <Button
          style={{
            marginLeft: '1rem',
          }}
          variant="primary"
          size="sm"
        >
          Go to experiments
        </Button>
      </Link>
    </Alert>
  );
}
