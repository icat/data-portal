import { Button, Modal, Spinner, Form } from 'react-bootstrap';
import { useState } from 'react';
import { ORCID } from '@edata-portal/core';
import { useConfig } from '@edata-portal/core';

export function MintDOIModal({
  show,
  setShow,
  onCancel,
  onMint,
  data,
  submitting,
}: {
  show: boolean;
  setShow: (show: boolean) => void;
  onCancel: () => void;
  onMint: () => void;
  data: any; // TODO : type to be updated when endpoint will be updated
  submitting: boolean;
}) {
  const [certify, setCertify] = useState(false);
  const config = useConfig();

  const onClose = () => {
    setCertify(false);
    onCancel();
  };
  return (
    <Modal show={show} onHide={onClose} size="lg" scrollable={true}>
      <Modal.Header closeButton>
        <Modal.Title>Confirmation</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {data !== null && (
          <>
            <p>
              DOIs are a system of persistent identifiers, therefore minting a
              DOI is permanent i.e. <strong>irreversible</strong>. Data DOIs are
              like publications, they should be as clear as possible. Here is an
              example of a well formatted{' '}
              <a
                href={config.ui.doi.referenceDoi}
                target="_blank"
                rel="noreferrer"
              >
                DOI.
              </a>
              <p>
                We kindly ask you to please carefully review the following
                points before clicking on <strong>Mint</strong>:
              </p>
            </p>
            <ul>
              <li className="p-2">
                <strong>
                  {' '}
                  Number of dataset{data.datasetIdList.length > 1
                    ? 's'
                    : ''}:{' '}
                </strong>
                {data.datasetIdList.length}
              </li>
              <li className="p-2">
                <strong>Title</strong> : Ensure it provides a concise and
                descriptive name for the data, offering a clear understanding of
                its content.
                <div className="fst-italic p-2">{data.title}</div>
              </li>
              <li className="p-2">
                <strong>Abstract</strong> : Confirm it provides detailed
                information about the data pointed to by the DOI, explaining
                what they represent, their purpose and significance. Add
                additional information about the files included.
                <div className="fst-italic p-2">{data.abstract}</div>
              </li>
              <li className="p-2">
                <strong>Authors</strong>: Double-check the list of authors,
                ensure their order is correct (first author on top), and include
                their ORCID IDs wherever possible. The order of authors can be
                modified by selecting an author and moving the author up or
                down.
                <ul className="fst-italic">
                  {data.authors.map((authors: any, index: any) => (
                    <li key={authors.name} className="p-1">
                      {authors.name}, {authors.surname}
                      {authors.orcid !== '' && (
                        <>
                          , <ORCID orcid={authors.orcid} isCompactView={true} />
                        </>
                      )}
                    </li>
                  ))}
                </ul>
              </li>
              {data.keywords && (
                <li className="p-2">
                  <strong>Keywords</strong>: Double-check the list of keywords.
                  <ul className="fst-italic">
                    {data.keywords.map((keyword: any, index: any) => (
                      <li key={`keyword-${index}`}>{keyword}</li>
                    ))}
                  </ul>
                </li>
              )}
              {data.citationPublicationDOI && (
                <li className="p-2">
                  <strong>Publication DOI</strong>: Check that the DOI for the
                  associated publication is correct.
                  <div className="fst-italic">
                    <a
                      href={`https://doi.org/${data?.citationPublicationDOI}`}
                      target={'_blank'}
                      rel="noreferrer"
                    >
                      {data.citationPublicationDOI}
                    </a>
                  </div>
                </li>
              )}
              {data.referenceURL && (
                <li className="p-2">
                  <strong>Reference URL</strong>: Check that the external
                  reference URL is correct.
                  <div className="fst-italic">
                    <a
                      href={data.referenceURL}
                      target={'_blank'}
                      rel="noreferrer"
                    >
                      {data.referenceURL}
                    </a>
                  </div>
                </li>
              )}
            </ul>
          </>
        )}
        <Form.Check
          type="switch"
          id="switch-herby"
          checked={certify}
          label={
            'I confirm there are no mistakes in the fields above and that their contents will help make the data easier to find, understand and reuse.'
          }
          onChange={(e) => {
            setCertify(e.currentTarget.checked);
          }}
          className="m-2"
          required
        />
        <p className="m-2">
          Select &quot;Mint DOI&quot; to mint the DOI with the current values or
          &quot;Return&quot; to edit the values.
        </p>
      </Modal.Body>

      <div className="justify-content-left p-1">
        <Button
          onClick={onMint}
          className="m-1"
          variant="primary"
          disabled={!certify}
        >
          {submitting ? <Spinner size="sm" /> : 'Mint DOI'}
        </Button>
        <Button onClick={onClose} className="m-1" variant="danger">
          Return
        </Button>
      </div>
    </Modal>
  );
}
