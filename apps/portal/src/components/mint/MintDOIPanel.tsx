import {
  useNotify,
  FormObjectDescription,
  RenderForm,
  useSelection,
  useTrackingCallback,
  buildDOIFromDataCollectionId,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  DATASET_LIST_ENDPOINT,
  Dataset,
  INVESTIGATION_USERS_LIST_ENDPOINT,
  InvestigationUser,
  useMutateEndpoint,
  MINT_DOI_ENDPOINT,
  DataCollection,
} from '@edata-portal/icat-plus-api';
import { DOIBadge } from '@edata-portal/doi';
import { useMemo } from 'react';
import { useConfig } from '@edata-portal/core';
import { Alert, Badge, Col, Row } from 'react-bootstrap';
import { MintDOIModal } from 'components/mint/MintDOIModal';
import { useState } from 'react';

export function MintDOIPanel({
  dataCollection,
}: {
  dataCollection?: DataCollection;
}) {
  const { value: selectedDatasets } = useSelection('dataset');
  const config = useConfig();
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: selectedDatasets.join(','),
    },
    skipFetch: selectedDatasets.length === 0,
    default: [] as Dataset[],
  });

  const investigationIds = useMemo(() => {
    return datasets
      ?.map((dataset) => dataset.investigation.id)
      .filter((v, id, self) => self.indexOf(v) === id)
      .filter((v) => v !== undefined)
      .map((v) => v.toString());
  }, [datasets]);

  const investigationParticipants = useGetEndpoint({
    endpoint: INVESTIGATION_USERS_LIST_ENDPOINT,
    params: {
      investigationId: investigationIds.join(','),
    },
    skipFetch: investigationIds.length === 0,
    default: [] as InvestigationUser[],
  });

  const reservedDoi = dataCollection
    ? buildDOIFromDataCollectionId(
        dataCollection.id,
        config.ui.doi.facilityPrefix,
        config.ui.doi.facilitySuffix,
      )
    : '';

  return (
    <Row className="g-2 p-2 overflow-auto">
      <Col xs={12}>
        <InfoSection />
      </Col>
      {dataCollection && (
        <Col xs={12}>
          <h5>
            Reserved DOI: <DOIBadge doi={reservedDoi} reserved={true} />
          </h5>
        </Col>
      )}
      <Col xs={12}>
        <FormSection
          datasets={datasets}
          dataCollection={dataCollection}
          investigationParticipants={investigationParticipants}
        />
      </Col>
    </Row>
  );
}

function InfoSection() {
  return (
    <Alert>
      You can only mint a DOI for datasets which are linked to experiment
      sessions where you are a user.
      <hr />
      Minting the DOI makes the datasets public immediately.
    </Alert>
  );
}

type FormType = {
  title: string;
  abstract: string;
  authors: {
    name: string;
    surname: string;
    orcid?: string;
    existingUser?: number;
  }[];
  keywords?: { keyword: string }[];
  citationPublicationDOI?: string;
  referenceURL?: string;
};

function FormSection({
  datasets,
  dataCollection,
  investigationParticipants,
  onClick,
}: {
  datasets?: Dataset[];
  dataCollection?: DataCollection;
  investigationParticipants: InvestigationUser[];
  onClick?: () => void;
}) {
  const config = useConfig();

  const participantsGroupedByName = useMemo(() => {
    return investigationParticipants.reduce(
      (acc, v) => {
        if (acc[v.fullName]) {
          acc[v.fullName].push(v);
        } else {
          acc[v.fullName] = [v];
        }
        return acc;
      },
      {} as Record<string, InvestigationUser[]>,
    );
  }, [investigationParticipants]);

  const formDescription: FormObjectDescription<FormType> = {
    title: {
      type: 'textarea',
      label: 'Title',
      required: true,
      minLength: config.ui.doi.minimalTitleLength,
    },
    abstract: {
      type: 'textarea',
      label: 'Abstract',
      required: true,
      minLength: config.ui.doi.minimalAbstractLength,
    },
    authors: {
      type: 'array',
      label: 'Authors',
      required: true,
      addFromPreset: Object.keys(participantsGroupedByName).map((name) => {
        const v = participantsGroupedByName[name][0];
        const roles = participantsGroupedByName[name].map(
          (v) => `${v.role} in ${v.investigationName}`,
        );
        return {
          value: {
            name: v.givenName,
            surname: v.familyName,
            orcid: v.orcidId,
            existingUser: v.id,
          },
          label: (
            <>
              <strong>{name}</strong>
              {roles.map((role) => (
                <Badge key={role} bg="primary" className="ms-1">
                  {role}
                </Badge>
              ))}
            </>
          ),
        };
      }),
      newItem: () => ({
        name: '',
        surname: '',
        orcid: '',
      }),
      item: {
        name: (v) => ({
          type: 'text',
          label: 'First name',
          required: !v.parentValue?.existingUser,
          readonly: v.parentValue?.existingUser,
          col: { xs: 12, lg: 4 },
        }),
        surname: (v) => ({
          type: 'text',
          label: 'Last name',
          required: !v.parentValue?.existingUser,
          readonly: v.parentValue?.existingUser,
          col: { xs: 12, lg: 4 },
        }),
        orcid: (v) => ({
          type: 'text',
          label: 'ORCID® iD',
          col: { xs: 12, lg: 4 },
          readonly: v.parentValue?.existingUser,
          validate: (value?: string) => {
            if (
              v.parentValue?.existingUser ||
              value === undefined ||
              !value?.length
            ) {
              return undefined;
            }
            const pattern = /(([a-zA-Z0-9]){4}-){3}([a-zA-Z0-9]){4}/;
            if (!pattern.test(value)) {
              return 'Invalid ORCID® iD';
            }
            return undefined;
          },
        }),
      },
    },
    keywords: {
      type: 'array',
      label: 'Keywords',
      item: {
        keyword: (v) => ({
          type: 'text',
          label: 'Keyword',
          required: true,
        }),
      },
      newItem: () => ({
        keyword: '',
      }),
    },
    citationPublicationDOI: {
      type: 'text',
      label: 'Publication DOI',
      validate: (value?: string) => {
        if (value === undefined || !value?.length) {
          return undefined;
        }
        // crossref DOI regex
        // https://www.crossref.org/blog/dois-and-matching-regular-expressions/
        const pattern = /^10\.\d{4,9}\/[-._;()/:A-Z0-9]+$/i;
        if (!pattern.test(value)) {
          return 'Invalid DOI';
        }
        return undefined;
      },
    },
    referenceURL: {
      type: 'url',
      label: 'External reference URL (PDB,...)',
      validate: (value?: string) => {
        if (value === undefined || !value?.length) {
          return undefined;
        }
        try {
          new URL(value);
          return undefined;
        } catch {
          return 'Invalid URL';
        }
      },
    },
  };

  const mintDOI = useMutateEndpoint({
    endpoint: MINT_DOI_ENDPOINT,
  });

  const notify = useNotify();

  const tracking = useTrackingCallback();

  const [showModal, setShowModal] = useState(false);

  const [formData, setFormData] = useState(null);

  const [mintDOISubmitting, setMintDOISubmitting] = useState(false);

  const [clearFormCallback, setClearFormCallback] = useState<
    (() => void) | null
  >(null);

  const [errorCallback, setErrorCallback] = useState<(() => void) | null>(null);

  const handleClick = (
    postData: any,
    clearForm: () => void,
    errorCallback: () => void,
  ) => {
    setFormData(postData);
    setClearFormCallback(() => clearForm);
    setErrorCallback(() => errorCallback);
    setShowModal(true);
  };

  const handleCancel = () => {
    setShowModal(false);
    if (errorCallback) {
      errorCallback();
    }
  };

  const clearForm = () => {
    if (clearFormCallback) {
      clearFormCallback();
    }
  };
  const datasetLength = datasets
    ? datasets.length
    : dataCollection?.dataCollectionDatasets.length;
  return (
    <>
      <RenderForm
        description={formDescription}
        submitLabel="Next"
        onSubmit={(data, clearForm, onErrorCallBack) => {
          const postData = {
            ...data,
            datasetIdList: datasets?.map((v) => v.id),
            dataCollectionId: dataCollection?.id,
            authors: data.authors.map((v, index) => {
              return {
                name: v.name,
                surname: v.surname,
                orcid: v.orcid,
                id: v.existingUser
                  ? v.existingUser
                  : `${v.name}_${v.surname}_${index}`,
              };
            }),
            keywords: data.keywords?.map((k) => {
              return k.keyword;
            }),
          };
          handleClick({ ...postData }, clearForm, onErrorCallBack);
        }}
        onCancel={clearForm}
        cancelLabel="Clear"
      />
      <MintDOIModal
        show={showModal}
        setShow={setShowModal}
        onCancel={handleCancel}
        submitting={mintDOISubmitting}
        onMint={() => {
          setMintDOISubmitting(true);
          mintDOI
            .mutateAsync({
              body: formData,
            })
            .then((v) => {
              setMintDOISubmitting(false);
              if (!v?.message?.includes('/')) {
                notify({
                  title: <strong>Failed to mint DOI</strong>,
                  message: (
                    <>
                      <h5>DOI minting failed.</h5>
                      <p>
                        You can only mint DOI if you are participant of the
                        investigation.
                      </p>
                    </>
                  ),
                  type: 'danger',
                });
                handleCancel();
              } else {
                tracking('DOI', 'Minted', v.message);
                notify({
                  title: <strong>Successfully minted DOI</strong>,
                  message: (
                    <>
                      <h5>DOI minted for {datasetLength} datasets.</h5>
                      <DOIBadge doi={v?.message} />
                    </>
                  ),
                  type: 'success',
                });
                window.open(`https://doi.org/${v.message}`, '_blank');
                clearForm();
                handleCancel();
              }
            })
            .catch(() => {
              setMintDOISubmitting(false);
              handleCancel();
            });
        }}
        data={formData}
      />
    </>
  );
}
