import { SplitPage, useSelection } from '@edata-portal/core';
import { MintDOIPanel } from 'components/mint/MintDOIPanel';
import { EmptySelection } from 'components/selection/EmptySelection';
import { SelectionPanelForType } from 'components/selection/SelectionPanelForType';
import { Container } from 'react-bootstrap';

export default function MintDOIPage() {
  const { value: selectionDataset } = useSelection('dataset');

  if (selectionDataset.length === 0) {
    return <EmptySelection />;
  }

  return (
    <SplitPage
      left={
        <Container fluid className="mt-2 mb-2 overflow-auto">
          <SelectionPanelForType
            type={'dataset'}
            hideReprocess
            hideMintDOI
            linkToSelectionPage
            hideDownload
          />
        </Container>
      }
      right={
        <Container fluid className="mt-2 mb-2 overflow-auto">
          <MintDOIPanel />
        </Container>
      }
    />
  );
}
