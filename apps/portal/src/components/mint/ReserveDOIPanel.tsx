import { Button, CopyValue, useNotify, useSelection } from '@edata-portal/core';
import { DOIBadge } from '@edata-portal/doi';
import {
  RESERVE_DOI_ENDPOINT,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { useState } from 'react';
import { Alert, Col, Row, Spinner } from 'react-bootstrap';

export function ReserveDOIPanel() {
  const [reserveDOISubmitting, setReserveDOISubmitting] = useState(false);
  const notify = useNotify();
  const { value: selectedDatasets } = useSelection('dataset');

  const reserveDOI = useMutateEndpoint({
    endpoint: RESERVE_DOI_ENDPOINT,
  });

  const [reservedDOI, setReservedDOI] = useState('');

  return (
    <Row className="g-2 p-2 overflow-auto">
      {reservedDOI && (
        <Col xs={12}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <p style={{ marginBottom: 0 }}>You DOI is reserved:</p>
            <CopyValue type="icon" value={`https://doi.org/${reservedDOI}`} />
            <DOIBadge doi={reservedDOI} reserved={true} />
          </div>
        </Col>
      )}
      <Col xs={12}>
        <Alert variant="info">
          In case you want to know the DOI in advance of publication, you can
          reserve a DOI. You will not be able to update the datasets linked to
          this DOI afterwards, so carefully check the list of the selected
          datasets.
        </Alert>
      </Col>
      <Col xs={12}>
        <Button
          disabled={reservedDOI.length > 0}
          onClick={() => {
            reserveDOI
              .mutateAsync({ body: { datasetIdList: selectedDatasets } })
              .then((v) => {
                setReserveDOISubmitting(false);
                if (!v?.message?.includes('/')) {
                  notify({
                    title: <strong>Failed to reserve DOI</strong>,
                    message: (
                      <>
                        <h5>DOI reservation failed.</h5>
                        <p>
                          You can only reserve a DOI if you are participant of
                          the investigation.
                        </p>
                      </>
                    ),
                    type: 'danger',
                  });
                } else {
                  setReservedDOI(v.message);
                }
              })
              .catch(() => {
                setReserveDOISubmitting(false);
              });
          }}
        >
          {reserveDOISubmitting ? <Spinner size="sm" /> : 'Reserve DOI'}
        </Button>
      </Col>
    </Row>
  );
}
