import { useUser } from '@edata-portal/core';
import NotFound from 'components/routing/NotFound';
import { Outlet } from 'react-router-dom';

export function AdminPage() {
  const user = useUser();

  if (!user?.isAdministrator) {
    return <NotFound />;
  }

  return <Outlet />;
}
