import { SideNavElement, useConfig, WithSideNav } from '@edata-portal/core';
import { faChartLine, faFileText } from '@fortawesome/free-solid-svg-icons';
import { NavTab } from 'components/navigation/NavTab';
import { Nav } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';

export default function FacilityMonitoringPage() {
  const config = useConfig();
  if (!config.ui.features.logbook) {
    return <Outlet />;
  }

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label="Facility monitoring">
          <Nav
            variant="pills"
            style={{
              display: 'flex',
              flexDirection: 'column',
              gap: 5,
            }}
          >
            <NavTab
              to={`/admin/facility/statistics`}
              icon={faChartLine}
              label="Statistics"
            />
            <NavTab
              to={`/admin/facility/logbook`}
              icon={faFileText}
              label="Logbook"
            />
          </Nav>
        </SideNavElement>
      }
    >
      <Outlet />
    </WithSideNav>
  );
}
