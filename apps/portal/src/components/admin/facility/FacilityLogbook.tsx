import { RemoteComponent } from 'components/RemoteComponent';

export default function FacilityLogbook() {
  return (
    <RemoteComponent
      remote={{
        name: 'logbook',
        component: './Logbook',
      }}
      props={{
        readOnly: true,
      }}
    />
  );
}
