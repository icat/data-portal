import { Loading, prettyPrint, useConfig, useParam } from '@edata-portal/core';
import { RemoteComponent } from 'components/RemoteComponent';
import { DataStatistics } from 'components/statistics/data/DataStatistics';
import { Suspense, useMemo } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';

const VIEW_TYPES = ['data', 'logistics', 'logbook'] as const;
type ViewType = (typeof VIEW_TYPES)[number];

export default function SiteStatisticsPage() {
  const config = useConfig();
  const [view, setView] = useParam<ViewType>('view', 'data', true);

  const viewTypesToDisplay = useMemo(
    () =>
      VIEW_TYPES.filter((viewType) => {
        if (viewType === 'data') return true;
        return config.ui.features[viewType] ?? false;
      }),
    [config.ui.features],
  );

  return (
    <Container fluid>
      {viewTypesToDisplay?.length > 1 ? (
        <>
          <Row className="g-2">
            {viewTypesToDisplay.map((viewType) => {
              return (
                <Col key={viewType} xs={'auto'}>
                  <Button
                    size="sm"
                    variant="outline-primary"
                    active={viewType === view}
                    onClick={() => setView(viewType)}
                  >
                    {prettyPrint(viewType)}
                  </Button>
                </Col>
              );
            })}
          </Row>
          <hr />
        </>
      ) : null}

      <Suspense fallback={<Loading />}>
        {view === 'data' && <DataStatistics />}
        {view === 'logistics' && (
          <RemoteComponent
            remote={{
              name: 'logistics',
              component: './LogisticsStatistics',
            }}
          />
        )}
        {view === 'logbook' && (
          <RemoteComponent
            remote={{
              name: 'logbook',
              component: './SiteLogbookStatistics',
            }}
          />
        )}
      </Suspense>
    </Container>
  );
}
