import type { Investigation } from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export default function InvestigationRole({
  investigation,
  username,
}: {
  investigation?: Investigation;
  username: string | undefined;
}) {
  const role = useMemo(() => {
    if (!investigation || !username || !investigation.investigationUsers) {
      return '';
    }
    const roles = investigation.investigationUsers
      .filter(
        (investigationUser) =>
          investigationUser.user.name.indexOf(username) > -1,
      )
      .map((investigationUser) => investigationUser.role);
    return roles.sort().join(', ');
  }, [investigation, username]);

  if (!username || !investigation) {
    return null;
  }
  return (
    <>
      <div>{role}</div>
    </>
  );
}
