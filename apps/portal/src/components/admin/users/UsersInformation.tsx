import {
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import UsersInvestigations from 'components/admin/users/UsersInvestigations';
import UsersTable from 'components/admin/users/UsersTable';
import { Row } from 'react-bootstrap';

export default function UsersInformation({
  username,
}: {
  username: string | undefined;
}) {
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      username,
    },
    skipFetch: !username || username.trim() === '',
  });

  if (!investigations) {
    return null;
  }
  return (
    <>
      <Row>
        <UsersTable username={username} investigations={investigations} />
      </Row>
      <Row>
        <UsersInvestigations
          username={username}
          investigations={investigations}
        />
      </Row>
    </>
  );
}
