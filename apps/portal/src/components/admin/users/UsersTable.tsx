import type {
  SimpleIcatUser,
  Investigation,
} from '@edata-portal/icat-plus-api';
import { TanstackBootstrapTable } from '@edata-portal/core';
import {
  useReactTable,
  type ColumnDef,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
} from '@tanstack/react-table';
import { useMemo } from 'react';

export default function UsersTable({
  investigations,
  username,
}: {
  investigations: Investigation[];
  username: string | undefined;
}) {
  const users: SimpleIcatUser[] = useMemo(() => {
    if (!investigations || !username) {
      return [];
    }
    const allusers: SimpleIcatUser[] = investigations
      .map(
        ({ investigationUsers }) =>
          (investigationUsers !== undefined &&
            investigationUsers
              .map((investigationUser) => investigationUser.user)
              .filter((user) => user.name.indexOf(username) > -1)) ||
          [],
      )
      .flat();
    return Object.values(
      allusers.reduce((acc, user) => ({ ...acc, [user.name]: user }), {}),
    );
  }, [investigations, username]);

  const columns: ColumnDef<SimpleIcatUser>[] = [
    {
      accessorFn: (row) => row,
      id: 'name',
      cell: (info) => (info.getValue() as SimpleIcatUser).name,
      header: 'name',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      id: 'email',
      cell: (info) => (info.getValue() as SimpleIcatUser).email,
      header: 'Email',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      id: 'familyName',
      cell: (info) => (info.getValue() as SimpleIcatUser).familyName,
      header: 'Family name',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      id: 'givenName',
      cell: (info) => (info.getValue() as SimpleIcatUser).givenName,
      header: 'Given name',
      enableColumnFilter: false,
    },
  ];

  const table = useReactTable<SimpleIcatUser>({
    data: users,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });

  if (!users.length) {
    return null;
  }
  return (
    <div style={{ fontSize: '0.9em' }}>
      <TanstackBootstrapTable
        dataLength={users.length}
        table={table}
        bordered={true}
      />
    </div>
  );
}
