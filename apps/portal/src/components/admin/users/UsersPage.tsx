import { SearchBar, useUser } from '@edata-portal/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import UsersInformation from 'components/admin/users/UsersInformation';
import NotFound from 'components/routing/NotFound';
import { SelectUsers } from 'components/usermanagement/SelectUsers';
import { Suspense, useMemo, useState } from 'react';
import { Col, Row, Form, InputGroup, Button, Container } from 'react-bootstrap';

export default function UsersPage() {
  const user = useUser();
  const [selectedUser, setSelectedUser] = useState('');
  const [searchUsername, setSearchUsername] = useState('');

  const username = useMemo(() => {
    if (selectedUser) {
      return selectedUser;
    }
    if (searchUsername) {
      return searchUsername;
    }
    return undefined;
  }, [selectedUser, searchUsername]);

  if (!user?.isAdministrator) {
    return <NotFound />;
  }

  return (
    <Container fluid>
      <Row>
        <Col xs={12} md={6} xl={4} xxl={3}>
          <SelectUsers
            selectedUsers={[selectedUser]}
            onSelect={(users) => {
              if (users) {
                setSearchUsername('');
                setSelectedUser(users[0].name);
              } else {
                setSearchUsername('');
                setSelectedUser('');
              }
            }}
            placeholder="Select the user you want to search..."
            isMulti={false}
          />
        </Col>
        <Col xs={12} md={6} xl={4} xxl={3}>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              e.stopPropagation();
            }}
          >
            <InputGroup
              size="sm"
              style={{
                height: '2rem',
                margin: '2px',
              }}
            >
              <SearchBar
                onUpdate={(value) => {
                  if (value) {
                    setSelectedUser('');
                    setSearchUsername(value);
                  } else {
                    setSearchUsername('');
                  }
                }}
                value={searchUsername}
                placeholder="Or search by username"
              />

              <Button
                variant="light"
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
              >
                <FontAwesomeIcon icon={faSearch} />
              </Button>
            </InputGroup>
          </Form>
        </Col>
      </Row>
      <Suspense fallback={<div>Loading information...</div>}>
        <UsersInformation username={username} />
      </Suspense>
    </Container>
  );
}
