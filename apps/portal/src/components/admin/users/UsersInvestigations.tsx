import {
  InvestigationDate,
  InvestigationOpenBtn,
  NoData,
  TanstackBootstrapTable,
  UserPortalButton,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { Col } from 'react-bootstrap';
import {
  useReactTable,
  type ColumnDef,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
} from '@tanstack/react-table';
import { InvestigationTitle } from 'components/browse/Investigation';
import { useNavigate } from 'react-router-dom';
import InvestigationRole from 'components/admin/users/InvestigationRole';

export default function UsersInvestigations({
  username,
  investigations,
}: {
  username: string | undefined;
  investigations: Investigation[];
}) {
  const navigate = useNavigate();

  const columns: ColumnDef<Investigation>[] = [
    {
      accessorFn: (row) => row,
      id: 'open',
      cell: (info) => (
        <InvestigationOpenBtn
          investigation={info.getValue() as Investigation}
        />
      ),
      header: '',
      enableColumnFilter: false,
    },
    {
      accessorKey: 'instrument.name',
      header: 'Beamline',
      footer: 'Beamline',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Start',
      footer: 'Start',
      cell: (info) => (
        <InvestigationDate investigation={info.getValue() as Investigation} />
      ),
      enableColumnFilter: false,
    },
    {
      accessorFn: (i) => (i.title.length < 10 ? i.summary : i.title),
      cell: (i) => <InvestigationTitle investigation={i.row.original} />,
      header: 'Title',
      footer: 'Title',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row: any) => row,
      header: 'A-Form',
      footer: 'A-Form',
      cell: (info: any) => (
        <UserPortalButton investigation={info.getValue() as Investigation} />
      ),
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Role',
      footer: 'Role',
      cell: (info) => (
        <InvestigationRole
          investigation={info.getValue() as Investigation}
          username={username}
        />
      ),
      enableColumnFilter: false,
    },
  ];

  const table = useReactTable<Investigation>({
    data: investigations,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });

  if (!investigations) {
    return null;
  }

  if (!investigations.length) {
    return (
      <Col style={{ marginTop: '8px' }}>
        <NoData />
      </Col>
    );
  }

  return (
    <div style={{ fontSize: '0.9em' }}>
      <TanstackBootstrapTable
        dataLength={investigations.length}
        table={table}
        bordered={true}
        onRowClick={(row) => {
          navigate(`/investigation/${row.id}/datasets`);
        }}
      />
    </div>
  );
}
