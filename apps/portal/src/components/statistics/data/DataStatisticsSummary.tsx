import {
  Loading,
  MetadataTable,
  formatDateToIcatDate,
  stringifyWithUnitPrefix,
} from '@edata-portal/core';
import {
  ELASTICSEARCH_METRICS_ENDPOINT,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { DataStatisticsTable } from 'components/statistics/data/DataStatisticsTable';
import { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';

export function DataStatisticsSummary({
  startDate = '2000-01-01',
  endDate = formatDateToIcatDate(new Date()),
  instrumentName,
}: {
  startDate?: string;
  endDate?: string;
  instrumentName?: string;
}) {
  const fetchData = useMutateEndpoint({
    endpoint: ELASTICSEARCH_METRICS_ENDPOINT,
    params: instrumentName
      ? {
          instrumentName: instrumentName.toLowerCase().replaceAll('-', ''),
        }
      : {},
  });

  const [data, setData] = useState<any>(undefined);

  useEffect(() => {
    setData(undefined);
    let canceled = false;
    fetchData
      .mutateAsync({
        body: {
          ranges: [
            {
              start: startDate,
              end: endDate,
              name: 'range',
            },
          ],
        },
      })
      .then((res) => {
        if (!canceled) setData(res);
      });
    return () => {
      canceled = true;
    };
  }, [startDate, endDate, instrumentName]); // eslint-disable-line react-hooks/exhaustive-deps

  if (!data) return <Loading />;

  const range = data?.startDate?.buckets?.range;

  if (!range) return <div>No data</div>;

  return <DisplayData data={range} instrumentName={instrumentName} />;
}

function DisplayData({
  data,
  instrumentName,
}: {
  data: any;
  instrumentName?: string;
}) {
  return (
    <Col>
      <Row>
        <Col>
          <MetadataTable
            parameters={[
              {
                caption: 'Datasets',
                value: data?.doc_count,
              },
              {
                caption: 'Beamlines',
                value: data?.instrument?.buckets?.length,
              },
              {
                caption: 'Experiment sessions',
                value: data?.investigationId?.buckets?.length,
              },
              {
                caption: 'Techniques (definition)',
                value: data?.definition?.buckets?.length,
              },
              {
                caption: 'Volume',
                value: stringifyWithUnitPrefix(data?.volume_stats?.sum, 2, 'B'),
              },
              {
                caption: 'Files',
                value: data?.fileCount_stats?.sum,
              },
            ]}
          />
        </Col>
        <Col>
          <MetadataTable
            parameters={[
              {
                caption: 'Files (avg)',
                value: data?.fileCount_stats?.avg,
                digits: 2,
              },
              {
                caption: 'Files (max)',
                value: data?.fileCount_stats?.max,
              },
              {
                caption: 'Volume (avg)',
                value: stringifyWithUnitPrefix(data?.volume_stats?.avg, 2, 'B'),
              },
              {
                caption: 'Volume (max)',
                value: stringifyWithUnitPrefix(data?.volume_stats?.max, 2, 'B'),
              },
              {
                caption: 'Metadata (avg)',
                value: data?.parametersCount_stats?.avg,
                digits: 2,
              },
              {
                caption: 'Metadata (max)',
                value: data?.parametersCount_stats?.max,
              },
            ]}
          />
        </Col>
      </Row>
      <Row>
        {instrumentName ? null : (
          <Col xs={12} xl={6}>
            <DataStatisticsTable data={data} aggregate="instrument" />
          </Col>
        )}
        <Col xs={12} xl={instrumentName ? 12 : 6}>
          <DataStatisticsTable data={data} aggregate="definition" />
        </Col>
      </Row>
    </Col>
  );
}
