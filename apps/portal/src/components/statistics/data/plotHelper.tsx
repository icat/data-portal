import { formatDateToDay, toUnitPrefix } from '@edata-portal/core';

const BYTE_UNIT = 'G' as const;

const BYTE_METRICS = ['volume'];

export type ParsedElasticsearchPlotData = ReturnType<
  typeof parseElasticsearchPlotData
>;

export function parseElasticsearchPlotData(data: any) {
  let dates = new Set<string>();

  let keys = new Set<string>();

  let values: {
    [date: string]: {
      [key: string]: {
        [key: string]: number;
      };
    };
  } = {};

  data?.aggregations?.histogram?.buckets?.forEach((v: any) => {
    const date = v.key;
    if (!date) return;
    dates.add(date);
    if (!(date in values)) values[date] = {};

    v?.definition?.buckets?.forEach((d: any) => {
      const key = d.key;
      keys.add(key);

      if (!(key in values[date])) values[date][key] = {};

      Object.keys(d).forEach((metric) => {
        if (!(typeof d[metric] === 'object')) return;
        if (!('value' in d[metric])) return;
        const res = d[metric].value;
        if (typeof res !== 'number') return;
        if (res === 0) return;
        if (BYTE_METRICS.includes(metric)) {
          values[date][key][metric] = toUnitPrefix(res, BYTE_UNIT);
        } else {
          values[date][key][metric] = res;
        }
      });
    });
  });

  const datesRes = Array.from(dates).sort();
  const dateLabelsRes = datesRes.map((date) => formatDateToDay(date) || '');

  return {
    dates: datesRes,
    dateLabels: dateLabelsRes,
    keys: Array.from(keys).sort(),
    values,
    byteUnit: BYTE_UNIT,
    byteMetrics: BYTE_METRICS,
  };
}
