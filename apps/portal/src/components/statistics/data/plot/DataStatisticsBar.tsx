import { PlotWidget, prettyPrint } from '@edata-portal/core';
import type { ParsedElasticsearchPlotData } from 'components/statistics/data/plotHelper';
import { useMemo } from 'react';

export function DataStatisticsBar({
  data,
  metric,
}: {
  data: ParsedElasticsearchPlotData;
  metric: 'fileCount' | 'volume';
}) {
  const { dates, dateLabels, keys, values, byteUnit, byteMetrics } = data;

  const traces = useMemo(
    () =>
      keys.map((trace) => {
        const x = dateLabels;
        const y = dates.map((date) => values[date]?.[trace]?.[metric] || 0);
        return {
          x: x,
          y: y,
          name: trace,
        };
      }),
    [keys, dateLabels, dates, values, metric],
  );

  return (
    <PlotWidget
      data={traces.map((trace) => ({
        ...trace,
        type: 'bar',
      }))}
      layout={{
        barmode: 'stack',
        height: Math.max(500, window.innerHeight - 300),
        title: `${prettyPrint(metric)} over time ${
          byteMetrics.includes(metric) ? `(${byteUnit}B)` : ''
        }`,
      }}
    />
  );
}
