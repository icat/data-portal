import {
  PlotWidget,
  prettyPrint,
  stringifyWithUnitPrefix,
} from '@edata-portal/core';
import type { ParsedElasticsearchPlotData } from 'components/statistics/data/plotHelper';
import { useMemo } from 'react';

export function DataStatisticsHeatmap({
  data,
  metric,
}: {
  data: ParsedElasticsearchPlotData;
  metric: 'fileCount' | 'volume';
}) {
  const { dates, dateLabels, keys, values, byteUnit, byteMetrics } = data;

  const points = useMemo(() => {
    return dates.flatMap((date, dateIndex) =>
      keys.map((key) => {
        const value = values[date]?.[key]?.[metric] || null;
        return {
          x: key,
          y: dateLabels[dateIndex],
          value: value,
          text:
            value && byteMetrics.includes(metric)
              ? stringifyWithUnitPrefix(value, 2, 'B', byteUnit)
              : String(value),
          shortText:
            value &&
            stringifyWithUnitPrefix(
              value,
              0,
              byteMetrics.includes(metric) ? 'B' : '',
              byteMetrics.includes(metric) ? byteUnit : '',
            ),
        };
      }),
    );
  }, [dates, keys, values, metric, dateLabels, byteMetrics, byteUnit]);

  const height = 30 * dateLabels.length + 350;

  return (
    <PlotWidget
      data={[
        {
          x: points.map((v) => v.x),
          y: points.map((v) => v.y),
          z: points.map((v) => v.value),
          text: points.map((v) => v.text),
          type: 'heatmap',
          colorscale: [
            [0, '#95acbf'],
            [1, '#1A237E'],
          ],
          showscale: false,
          hoverinfo: 'x+y+text',
        },
        {
          x: points.map((v) => v.x),
          y: points.map((v) => v.y),
          text: points.map(
            (v) =>
              v.shortText &&
              `<b style="text-shadow: 0px 0px 2px black;">${v.shortText}</b>`,
          ),
          mode: 'text',
          hoverinfo: 'skip',
          textfont: {
            family: 'Arial',
            size: 9,
            color: 'white',
          },
        },
      ]}
      layout={{
        height,
        title: `${prettyPrint(metric)} over time`,
        xaxis: {
          tickmode: 'array',
          tickvals: keys,
          side: 'top',
        },
        yaxis: {
          tickmode: 'array',
          tickvals: dateLabels,
          range: [-0.5, dateLabels.length + 0.5],
          autorange: false,
        },
        margin: {
          t: 200,
        },
      }}
    />
  );
}
