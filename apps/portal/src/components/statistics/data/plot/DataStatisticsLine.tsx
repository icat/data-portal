import { PlotWidget, prettyPrint } from '@edata-portal/core';
import type { ParsedElasticsearchPlotData } from 'components/statistics/data/plotHelper';
import { useMemo } from 'react';

export function DataStatisticsLine({
  data,
  metric,
  cumulative,
}: {
  data: ParsedElasticsearchPlotData;
  metric: 'fileCount' | 'volume';
  cumulative: boolean;
}) {
  const { dates, dateLabels, keys, values, byteUnit, byteMetrics } = data;

  const traces = useMemo(
    () =>
      keys.map((trace) => {
        const x = dateLabels;
        const y = dates.map((date) => values[date]?.[trace]?.[metric] || 0);

        function getCumulativeY() {
          return y.reduce((acc, cur) => {
            acc.push((acc[acc.length - 1] || 0) + cur);
            return acc;
          }, [] as number[]);
        }

        return {
          x: x,
          y: cumulative ? getCumulativeY() : y,
          name: trace,
        };
      }),
    [keys, dateLabels, dates, cumulative, values, metric],
  );

  return (
    <PlotWidget
      data={traces.map((trace) => ({
        ...trace,
        type: 'scattergl',
        mode: 'lines+markers',
      }))}
      layout={{
        height: Math.max(500, window.innerHeight - 300),
        title: `${prettyPrint(metric)} over time ${
          byteMetrics.includes(metric) ? `(${byteUnit}B)` : ''
        }`,
      }}
    />
  );
}
