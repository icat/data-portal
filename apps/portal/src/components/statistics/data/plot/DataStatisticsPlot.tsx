import {
  Loading,
  NoData,
  SideNavElement,
  SideNavFilter,
  WithSideNav,
  formatDateToIcatDate,
  prettyPrint,
  useParam,
} from '@edata-portal/core';
import {
  ELASTICSEARCH_HISTOGRAM_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { DataStatisticsBar } from 'components/statistics/data/plot/DataStatisticsBar';
import { DataStatisticsHeatmap } from 'components/statistics/data/plot/DataStatisticsHeatmap';
import { DataStatisticsLine } from 'components/statistics/data/plot/DataStatisticsLine';
import { parseElasticsearchPlotData } from 'components/statistics/data/plotHelper';
import { Suspense, useMemo } from 'react';
import { Form } from 'react-bootstrap';

const INTERVALS = [
  { label: '1 day', value: '1d' },
  { label: '1 week', value: '7d' },
  { label: '1 month', value: '30d' },
  { label: '3 months', value: '90d' },
];

const PLOTS = ['heatmap', 'line', 'line (cumulative)', 'bar'];
type PlotType = (typeof PLOTS)[number];

export function DataStatisticsPlot({
  startDate = '2000-01-01',
  endDate = formatDateToIcatDate(new Date()) || '',
  instrumentName,
}: {
  startDate?: string;
  endDate?: string;
  instrumentName?: string;
}) {
  const [plotType, setPlotType] = useParam<PlotType>('plotType', PLOTS[0]);

  const [metric, setMetric] = useParam<'fileCount' | 'volume'>(
    'metric',
    'fileCount',
  );
  const [aggregation, setAggregation] = useParam<
    'instrumentName' | 'definition'
  >('aggregation', 'instrumentName');

  const [interval, setInterval] = useParam<string>('interval', '1d');

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label={'Plot'}>
          <>
            <SideNavFilter label={'Type'}>
              <Form.Select
                value={plotType}
                onChange={(e) => setPlotType(e.target.value as any)}
              >
                {PLOTS.map((p) => (
                  <option key={p} value={p}>
                    {prettyPrint(p)}
                  </option>
                ))}
              </Form.Select>
            </SideNavFilter>
            <SideNavFilter label={'Metric'}>
              <Form.Select
                value={metric}
                onChange={(e) => setMetric(e.target.value as any)}
              >
                <option value="fileCount">File Count</option>
                <option value="volume">Volume</option>
              </Form.Select>
            </SideNavFilter>
            {instrumentName ? null : (
              <SideNavFilter label={'Aggregation'}>
                <Form.Select
                  value={aggregation}
                  onChange={(e) => setAggregation(e.target.value as any)}
                >
                  <option value="instrumentName">Beamline</option>
                  <option value="definition">Technique (definition)</option>
                </Form.Select>
              </SideNavFilter>
            )}
            <SideNavFilter label={'Interval'}>
              <Form.Select
                value={interval}
                onChange={(e) => setInterval(e.target.value)}
              >
                {INTERVALS.map((i) => (
                  <option key={i.value} value={i.value}>
                    {i.label}
                  </option>
                ))}
              </Form.Select>
            </SideNavFilter>
          </>
        </SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>
        <LoadAndDisplayData
          instrumentName={instrumentName}
          startDate={startDate}
          endDate={endDate}
          format="json"
          metric={metric}
          aggregation={instrumentName ? 'definition' : aggregation}
          interval={interval}
          plotType={plotType}
        />
      </Suspense>
    </WithSideNav>
  );
}

function LoadAndDisplayData({
  instrumentName,
  startDate,
  endDate,
  format,
  metric,
  aggregation,
  interval,
  plotType,
}: {
  instrumentName?: string;
  startDate: string;
  endDate: string;
  format: 'csv' | 'json' | 'csv_json';
  metric: 'fileCount' | 'volume';
  aggregation: 'instrumentName' | 'definition';
  interval: string;
  plotType: PlotType;
}) {
  const data = useGetEndpoint({
    endpoint: ELASTICSEARCH_HISTOGRAM_ENDPOINT,
    params: {
      ...(instrumentName
        ? { instrumentName: instrumentName.toLowerCase().replaceAll('-', '') }
        : {}),
      startDate,
      endDate,
      format,
      aggregation,
      interval,
    },
  });

  const parsedData = useMemo(() => parseElasticsearchPlotData(data), [data]);

  if (!data) return <NoData />;

  if (plotType === 'heatmap')
    return <DataStatisticsHeatmap data={parsedData} metric={metric} />;
  if (plotType === 'line')
    return (
      <DataStatisticsLine
        data={parsedData}
        metric={metric}
        cumulative={false}
      />
    );
  if (plotType === 'line (cumulative)')
    return (
      <DataStatisticsLine data={parsedData} metric={metric} cumulative={true} />
    );
  if (plotType === 'bar')
    return <DataStatisticsBar data={parsedData} metric={metric} />;

  return <div>Plot type not supported</div>;
}
