import {
  Loading,
  SideNavElement,
  SideNavFilter,
  UI_DATE_DAY_FORMAT,
  WithSideNav,
  addToDate,
  formatDateToIcatDate,
  parseDate,
  useParam,
} from '@edata-portal/core';
import {
  INSTRUMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import {} from '@tanstack/react-table';
import { DataStatisticsPlot } from 'components/statistics/data/plot/DataStatisticsPlot';
import { DataStatisticsSummary } from 'components/statistics/data/DataStatisticsSummary';
import { Suspense, useMemo } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import ReactSelect from 'react-select';

const DATE_PRESETS = [
  {
    label: 'Today',
    startDate: formatDateToIcatDate(new Date()),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'Past 7 days',
    startDate: formatDateToIcatDate(addToDate(new Date(), { days: -7 })),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'Past 30 days',
    startDate: formatDateToIcatDate(addToDate(new Date(), { days: -30 })),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'Past year',
    startDate: formatDateToIcatDate(addToDate(new Date(), { years: -1 })),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'All time',
    startDate: 'undefined',
    endDate: 'undefined',
  },
];

const DEFAULT_START_DATE =
  formatDateToIcatDate(addToDate(new Date(), { days: -30 })) || 'undefined';
const DEFAULT_END_DATE = formatDateToIcatDate(new Date()) || 'undefined';

export function DataStatistics({ forInstrument }: { forInstrument?: string }) {
  const allInstrumentsList = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
    skipFetch: !!forInstrument,
  });

  const instruments = useMemo(() => {
    if (!allInstrumentsList) return [];
    const uniqueInstrumentList = allInstrumentsList.filter(
      (v, i, a) => a.findIndex((t) => t.name === v.name) === i,
    );
    return uniqueInstrumentList.sort((a, b) => a.name.localeCompare(b.name));
  }, [allInstrumentsList]);

  const [view, setView] = useParam<string>('viewAs', 'summary');
  const [startDate, setStartDate] = useParam<string>(
    'startDate',
    DEFAULT_START_DATE,
  );
  const [endDate, setEndDate] = useParam<string>('endDate', DEFAULT_END_DATE);
  const [instrument, setInstrument] = useParam<string>('instrumentName', 'all');

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label={'Data statistics'}>
          <>
            <SideNavFilter label={'Start date'}>
              <DatePicker
                className="form-control"
                selected={
                  startDate !== 'undefined' ? parseDate(startDate) : undefined
                }
                onChange={(date: Date | null) => {
                  setStartDate(
                    formatDateToIcatDate(date ?? undefined) || 'undefined',
                  );
                }}
                dateFormat={UI_DATE_DAY_FORMAT}
                isClearable
                placeholderText="Start date"
              />
            </SideNavFilter>
            <SideNavFilter label={'End date'}>
              <DatePicker
                className="form-control"
                selected={
                  endDate !== 'undefined' ? parseDate(endDate) : undefined
                }
                onChange={(date: Date | null) => {
                  setEndDate(
                    formatDateToIcatDate(date ?? undefined) || 'undefined',
                  );
                }}
                dateFormat={UI_DATE_DAY_FORMAT}
                isClearable
                placeholderText="End date"
              />
            </SideNavFilter>
            <SideNavFilter label={'Date presets'}>
              <Row className="g-1">
                {DATE_PRESETS.map((v) => (
                  <Col key={v.label} xs="auto">
                    <Button
                      variant="outline-primary"
                      size="sm"
                      onClick={() => {
                        setStartDate(v.startDate);
                        setEndDate(v.endDate);
                      }}
                      active={
                        v.startDate === startDate && v.endDate === endDate
                      }
                    >
                      {v.label}
                    </Button>
                  </Col>
                ))}
              </Row>
            </SideNavFilter>
            {forInstrument ? null : (
              <SideNavFilter label={'Beamline'}>
                <ReactSelect
                  isClearable
                  value={
                    instrument !== 'all'
                      ? { value: instrument, label: instrument }
                      : undefined
                  }
                  onChange={(v) => {
                    setInstrument(v?.value || 'all');
                  }}
                  options={instruments?.map((v) => {
                    return { value: v.name, label: v.name };
                  })}
                  placeholder="Beamline"
                />
              </SideNavFilter>
            )}
            <SideNavFilter label={'View'}>
              <Row className="g-1">
                <Col xs={'auto'}>
                  <Button
                    variant="outline-primary"
                    size="sm"
                    onClick={() => setView('summary')}
                    active={view === 'summary'}
                  >
                    Summary
                  </Button>
                </Col>
                <Col xs={'auto'}>
                  <Button
                    variant="outline-primary"
                    size="sm"
                    onClick={() => setView('plot')}
                    active={view === 'plot'}
                  >
                    Plot
                  </Button>
                </Col>
              </Row>
            </SideNavFilter>
          </>
        </SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>
        {view === 'summary' && (
          <DataStatisticsSummary
            startDate={startDate === 'undefined' ? undefined : startDate}
            endDate={endDate === 'undefined' ? undefined : endDate}
            instrumentName={
              forInstrument
                ? forInstrument
                : instrument === 'all'
                  ? undefined
                  : instrument
            }
          />
        )}
        {view === 'plot' && (
          <DataStatisticsPlot
            startDate={startDate === 'undefined' ? undefined : startDate}
            endDate={endDate === 'undefined' ? undefined : endDate}
            instrumentName={
              forInstrument
                ? forInstrument
                : instrument === 'all'
                  ? undefined
                  : instrument
            }
          />
        )}
      </Suspense>
    </WithSideNav>
  );
}
