import {
  prettyPrint,
  Progress,
  stringifyWithUnitPrefix,
  TanstackBootstrapTable,
} from '@edata-portal/core';
import {
  Instrument,
  INSTRUMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import {
  useReactTable,
  getCoreRowModel,
  getSortedRowModel,
  getPaginationRowModel,
  getFilteredRowModel,
} from '@tanstack/react-table';
import { useCallback, useMemo } from 'react';
import { Link } from 'react-router-dom';

export function DataStatisticsTable({
  data,
  aggregate,
}: {
  data: any;
  aggregate: 'instrument' | 'definition';
}) {
  const instruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
    skipFetch: aggregate !== 'instrument',
    default: [] as Instrument[],
  });

  const getInstrument = useCallback(
    (statsInstrumentName: string) => {
      return (
        instruments.find(
          (i) => i.name.toLowerCase() === statsInstrumentName.toLowerCase(),
        ) ||
        instruments.find(
          (i) =>
            i.name.toLowerCase().replaceAll('-', '') ===
            statsInstrumentName.toLowerCase(),
        )
      );
    },
    [instruments],
  );

  const totalVolume = data?.volume_stats?.sum;
  const totalFiles = data?.fileCount_stats?.sum;
  const dataPoints = useMemo(
    () =>
      (data[aggregate]?.buckets as any[] | undefined)?.map((v: any) => {
        return {
          key: v.key as string | undefined,
          fileCount: v.fileCount_stats.sum as number | undefined,
          fileOverTotalPercent: ((v.fileCount_stats.sum / totalFiles) * 100) as
            | number
            | undefined,
          volume: v.volume_stats.sum as number | undefined,
          volumeOverTotalPercent: ((v.volume_stats.sum / totalVolume) * 100) as
            | number
            | undefined,
        };
      }) || [],
    [aggregate, data, totalFiles, totalVolume],
  );

  const table = useReactTable({
    data: dataPoints,
    columns: [
      {
        header: prettyPrint(
          aggregate === 'instrument' ? 'beamline' : aggregate,
        ),
        accessorKey: 'key',
        enableColumnFilter: true,
        enableSorting: false,
        cell: (v) => {
          if (aggregate !== 'instrument') return v.getValue();
          const instrument = getInstrument(v.getValue());
          if (!instrument) return v.getValue();
          return (
            <Link to={`/instruments/${instrument.name}/investigations`}>
              {instrument.name}
            </Link>
          );
        },
      },
      {
        header: 'Volume',
        accessorKey: 'volume',
        enableColumnFilter: false,
        enableSorting: true,
        cell: (v) => stringifyWithUnitPrefix(v.getValue(), 2, 'B'),
      },
      {
        header: '(%)',
        accessorKey: 'volumeOverTotalPercent',
        enableColumnFilter: false,
        enableSorting: false,
        cell: (v) => <Progress valuePercent={v.getValue()} />,
      },
      {
        header: 'Files',
        accessorKey: 'fileCount',
        enableColumnFilter: false,
        enableSorting: true,
      },
      {
        header: '(%)',
        accessorKey: 'fileOverTotalPercent',
        enableColumnFilter: false,
        enableSorting: false,
        cell: (v) => <Progress valuePercent={v.getValue()} />,
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    initialState: {
      sorting: [{ id: 'volume', desc: true }],
    },
  });

  return (
    <TanstackBootstrapTable
      sorting={true}
      table={table}
      dataLength={dataPoints.length}
    />
  );
}
