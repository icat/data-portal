import { PlotWidget, getDatasetURL } from '@edata-portal/core';
import type { DatasetTimeLine } from '@edata-portal/icat-plus-api';
import { CustomTooltip } from 'components/plots/timeseries/CustomToolTip';

export type StatisticsPlotRecord = {
  x: Date;
  y: number;
  time: DatasetTimeLine;
};

export type StatisticsPlotSerie = {
  data: StatisticsPlotRecord[];
  opacity?: number;
  label?: string;
  color?: string;
};

export function TimeSeriesScatteringPlot({
  data,
  color = '',
  units = '',
  label = '',
  accumulative = false,
}: {
  data: StatisticsPlotRecord[];
  units?: string;
  label?: string;
  color?: string;
  accumulative?: boolean;
}) {
  function onClick(e: any) {
    if ('points' in e) {
      const point = e.points[0];
      const index = point.pointIndex;
      const record = data[index];

      window.open(
        getDatasetURL(record.time.datasetId, record.time.investigationId),
        '_blank',
        'noreferrer',
      );
    }
  }

  if (data && data.length > 0) {
    return (
      <PlotWidget
        useResizeHandler
        data={[
          {
            x: data.map((d) => d.x),
            y: data.map((d) => d.y),
            type: 'scattergl',
            mode: accumulative ? 'lines+markers' : 'markers',
            line: accumulative ? { shape: 'hv', width: 1 } : undefined,
            marker: { color: color, size: 4 },
            name: label,
          },
        ]}
        layout={{
          xaxis: { title: { text: 'Date' } },
          yaxis: {
            title: { text: units },
            exponentformat: 'e',
          },
          autosize: true,
          title: label,
          showlegend: false,
          hovermode: 'closest',
        }}
        onClick={onClick}
        compact
        getTooltip={(point: any) => {
          const record = data[point.pointIndex];
          return <CustomTooltip time={record.time} active={true} />;
        }}
      />
    );
  }
  return null;
}
