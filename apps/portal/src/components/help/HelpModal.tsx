import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ContactButton } from 'components/help/FeedbackButton';
import { Button, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function HelpModal({
  expanded,
  setExpanded,
}: {
  expanded: boolean;
  setExpanded: (v: boolean) => void;
}) {
  return (
    <Modal show={expanded} onHide={() => setExpanded(false)} size="lg" centered>
      <Modal.Header closeButton>
        <Modal.Title>
          <FontAwesomeIcon icon={faQuestionCircle} /> Need help?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          Please visit our help page to see all our &lsquo;how to&lsquo; guides:
        </p>
        <p>
          <Link to={`/help?from=${encodeURIComponent(window.location.href)}`}>
            <Button variant="primary" onClick={() => setExpanded(false)}>
              Help page
            </Button>
          </Link>
        </p>
        <p>
          <ContactButton title="You did not find your answer?" />
        </p>
        <p>
          <ContactButton title="Something is not working?" />
        </p>
        <hr />
        <h3>Your feedback is welcome!</h3>
        <ContactButton title="Share your feedback with us" />
      </Modal.Body>
    </Modal>
  );
}
