import { useConfig, Config, useUser } from '@edata-portal/core';
import type { IcatUser } from '@edata-portal/icat-plus-api';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { HeaderItem } from 'components/navigation/header/HeaderItem';
import { Button } from 'react-bootstrap';

export function FeedbackButton({
  setExpanded,
}: {
  setExpanded: (v: boolean) => void;
}) {
  const config = useConfig();
  const user = useUser();

  return (
    <HeaderItem
      title={
        <>
          <FontAwesomeIcon icon={faComment} /> Feedback
        </>
      }
      to={buildMailTo(config, user)}
      target="_blank"
      onClick={() => setExpanded(false)}
    />
  );
}

export function ContactButton({
  title,
  addInfo,
}: {
  title: string;
  addInfo?: string;
}) {
  const config = useConfig();
  const user = useUser();

  return (
    <Button
      variant="link"
      as="a"
      href={buildMailTo(config, user, addInfo)}
      target="_blank"
      size="sm"
      className="p-0"
    >
      <FontAwesomeIcon icon={faComment} /> {title}
    </Button>
  );
}

function buildMailTo(config: Config, user?: IcatUser, addInfo?: string) {
  const body = config.ui.feedback.body;
  const subject = config.ui.feedback.subject;
  const to = config.ui.feedback.email;

  const additionalInfo = `\n\n
Browser information (please do not remove):
Username: ${user?.username}
URL: ${window.location.href}
User agent: ${navigator.userAgent}
`;

  return `mailto:${to}?subject=${subject}&body=${encodeURIComponent(
    body + additionalInfo + (addInfo || ''),
  )}`;
}
