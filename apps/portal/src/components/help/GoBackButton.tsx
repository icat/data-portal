import { useParam } from '@edata-portal/core';
import { faAnglesLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function GoBackButton() {
  const [from] = useParam('from', 'undefined');

  if (!from || from === 'undefined') {
    return null;
  }

  return (
    <Link to={from}>
      <Button variant="primary">
        <FontAwesomeIcon icon={faAnglesLeft} className="me-2" />
        Go back to the app
      </Button>
    </Link>
  );
}
