import { useConfig, useParam } from '@edata-portal/core';
import { ContactButton } from 'components/help/FeedbackButton';
import { GoBackButton } from 'components/help/GoBackButton';
import { HOW_TO_LIST } from 'components/help/HowTo';
import { Accordion, Col, Container, Row } from 'react-bootstrap';

export default function HelpPage() {
  const [from] = useParam('from', 'undefined');

  const config = useConfig();

  return (
    <Container>
      <Row className="g-2">
        <Col xs={'auto'}>
          <GoBackButton />
        </Col>
        <hr />
        <Accordion>
          {HOW_TO_LIST.filter((howTo) => {
            if (!howTo.feature) return true;
            const features = config.ui.features;
            return (
              features &&
              howTo.feature in features &&
              features[howTo.feature as keyof typeof features]
            );
          }).map((howTo) => (
            <Accordion.Item key={howTo.title} eventKey={howTo.title}>
              <Accordion.Header>
                <h5>{howTo.title}</h5>
              </Accordion.Header>
              <Accordion.Body>
                {howTo.description}
                {howTo.video && (
                  <div
                    className="ratio ratio-16x9"
                    style={{
                      maxWidth: 600,
                    }}
                  >
                    <iframe
                      src={`https://www.youtube.com/embed/${howTo.video}`}
                      title={howTo.title}
                      allowFullScreen
                      allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    />
                  </div>
                )}
              </Accordion.Body>
            </Accordion.Item>
          ))}
        </Accordion>
        <Col xs={12}>
          {"Can't find what you're looking for? "}
          <a
            href={config.ui.knowledgeBasePage}
            target="_blank"
            rel="noreferrer"
          >
            Check out our knowledge base
          </a>
        </Col>
        <Col xs={12}>
          <ContactButton
            title="Still need to contact us?"
            addInfo={from && from !== 'undefined' ? `from=${from}` : undefined}
          />
        </Col>
      </Row>
    </Container>
  );
}
