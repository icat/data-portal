import { usePath, Loading } from '@edata-portal/core';
import {
  useGetEndpoint,
  INSTRUMENT_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import Alert from 'react-bootstrap/Alert';
import { Suspense } from 'react';
import InvestigationsPage from 'components/browse/InvestigationsPage';

export default function InstrumentInvestigations() {
  const instrumentName = usePath('instrumentName');

  const instruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
  });

  const instrument = instruments?.find(
    (instrument) => instrument.name === instrumentName,
  );

  return (
    <>
      {instrument ? (
        <>
          <Suspense fallback={<Loading />}>
            <InvestigationsPage instrumentName={instrumentName} />
          </Suspense>
        </>
      ) : (
        <Alert variant="warning">Could not find beamline</Alert>
      )}
    </>
  );
}
