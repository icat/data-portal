import {
  useParam,
  UI_DATE_DAY_FORMAT,
  WithSideNav,
  SideNavFilter,
  isScheduled,
  SideNavElement,
  addToDate,
  parseDate,
  formatDateToDay,
  formatDateToIcatDate,
  SelectInstruments,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  Instrument,
  USER_INSTRUMENT_LIST_ENDPOINT,
  INVESTIGATION_LIST_ENDPOINT,
  Investigation,
} from '@edata-portal/icat-plus-api';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { InstrumentsPage } from 'components/instrument/InstrumentsPage';
import {
  TimelineCalendar,
  TimelineGroupBaseType,
  TimelineItemType,
} from 'components/timeline/TimelineCalendar';
import { useMemo } from 'react';
import { Link } from 'react-router-dom';
import { Button, Col, Row } from 'react-bootstrap';
import { LegendCalendar } from 'components/browse/calendar/LegendCalendar';

const COLOR_EMPTY_SESSION = 'white';
const COLOR_NON_SCHEDULED_SESSION = 'orange';
const COLOR_NON_EMPTY_SESSION = 'LightBlue';

function getBackgroundColor(item: { investigation: Investigation }) {
  if (!isScheduled(item.investigation)) return COLOR_NON_SCHEDULED_SESSION;
  if (item.investigation.parameters.__datasetCount) {
    return COLOR_NON_EMPTY_SESSION;
  }
  return COLOR_EMPTY_SESSION;
}

function itemRenderer(props: {
  item: any;
  itemContext: any;
  getItemProps: any;
  getResizeProps: any;
}) {
  const { item, itemContext, getItemProps, getResizeProps } = props;
  const { left: leftResizeProps, right: rightResizeProps } = getResizeProps();
  return (
    <div
      {...getItemProps({
        style: {
          backgroundColor: getBackgroundColor(item),
          fontSize: '10px',
        },
      })}
    >
      {itemContext.useResizeHandle ? <div {...leftResizeProps} /> : ''}

      <div>
        <Link to={`/investigation/${item.investigation.id}/datasets`}>
          <span style={{ marginLeft: 10 }}>{itemContext.title}</span>
        </Link>
      </div>

      {itemContext.useResizeHandle ? <div {...rightResizeProps} /> : ''}
    </div>
  );
}

/**
 * Array of groups required by timeline calendar
 * @param instruments
 * @returns
 */
function getInstrumentsGroups(
  instruments: Instrument[],
): TimelineGroupBaseType[] {
  return instruments.map((i) => {
    const group: TimelineGroupBaseType = {
      id: i.id,
      title: i.name,
      stackItems: true,
    };
    return group;
  });
}

/**
 * Displays a timeline with the investigations associated to the instruments which I am associated to (as instrumentscinentist)
 * @returns
 */
export default function InstrumentsSchedule() {
  let instruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    params: {
      filter: 'instrumentscientist',
    },
    default: [] as Instrument[],
  });

  const [intervalDaysParam] = useParam<string>('interval', '15');

  const invervalDays = useMemo(() => {
    return Number(intervalDaysParam);
  }, [intervalDaysParam]);

  const [dateParam, setDateParam] = useParam<string>(
    'date',
    formatDateToDay(new Date()) || '',
  );

  const date = useMemo(() => {
    const parsed = parseDate(dateParam);
    return parsed ? parsed : new Date();
  }, [dateParam]);

  const [instrumentsQueryParam, setInstrumentsQueryParam] = useParam<string>(
    'instruments',
    '',
  );

  /** Filters the instrument based on instrument query param */
  if (instrumentsQueryParam !== '') {
    let instrumentNames = instrumentsQueryParam.split(',');
    instruments = instruments.filter((i) => instrumentNames.includes(i.name));
  }

  /** This gets an object representing the time windon that will be displayed based on INTERVAL_DAYS parameter */
  const interval = useMemo(() => {
    return {
      startDate: addToDate(date, { days: -invervalDays }) || new Date(),
      endDate: addToDate(date, { days: invervalDays }) || new Date(),
    };
  }, [invervalDays, date]);

  const groups = useMemo(() => {
    return getInstrumentsGroups(instruments);
  }, [instruments]);

  const onClick = (interval: number) => {
    setDateParam(
      formatDateToDay(
        addToDate(date, {
          days: interval,
        }),
      ),
    );
  };

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    default: [] as Investigation[],
    params: {
      startDate: formatDateToIcatDate(interval.startDate),
      endDate: formatDateToIcatDate(interval.endDate),
    },
  });

  const items: TimelineItemType[] = useMemo(() => {
    return investigations.map((investigation) => {
      const group = groups
        ? groups.find(
            (group) =>
              group.title ===
              (investigation.instrument ? investigation.instrument.name : ''),
          )
        : undefined;

      const startDate = parseDate(investigation.startDate)?.getTime() || 0;

      const endDate =
        parseDate(investigation.endDate)?.getTime() ||
        addToDate(startDate, {
          days: 1,
        })?.getTime() ||
        0;

      return {
        id: investigation.id,
        investigation: investigation,
        group: group ? group.id : '',
        title: investigation.name,
        start_time: startDate,
        end_time: endDate + 100,
        scheduled: investigation.endDate,
      };
    });
  }, [investigations, groups]);

  const nav = (
    <SideNavElement label={'Calendar'}>
      <>
        <SideNavFilter label={'Date'}>
          <DatePicker
            className="form-control"
            dateFormat={UI_DATE_DAY_FORMAT}
            selected={date}
            value={dateParam}
            onChange={(date: any) => {
              const strDate = formatDateToDay(date || new Date());
              setDateParam(strDate);
            }}
            isClearable
            placeholderText="Start date"
          />
        </SideNavFilter>
        <SideNavFilter label={'Beamlines'}>
          <SelectInstruments
            selectedInstruments={
              instrumentsQueryParam ? instrumentsQueryParam.split(',') : []
            }
            onChange={(selectedInstruments) => {
              setInstrumentsQueryParam(
                selectedInstruments.map((i) => i.name).toString(),
              );
            }}
            forUser={true}
            filter="instrumentscientist"
          ></SelectInstruments>
        </SideNavFilter>
      </>
    </SideNavElement>
  );

  return (
    <InstrumentsPage>
      <WithSideNav sideNav={nav}>
        <Row>
          <Col>
            <Button
              style={{ margin: 10 }}
              variant="secondary"
              size="sm"
              onClick={() => onClick(-invervalDays)}
            >
              {'< Prev'}
            </Button>
            <Button
              style={{ margin: 10 }}
              variant="secondary"
              size="sm"
              onClick={() => onClick(invervalDays)}
            >
              {'Next >'}
            </Button>
          </Col>
          <Col md={{ span: 4, offset: 4 }}>
            <LegendCalendar />
          </Col>
        </Row>
        <TimelineCalendar
          startDate={interval.startDate}
          endDate={interval.endDate}
          groups={groups}
          items={items}
          itemRenderer={itemRenderer}
        />
      </WithSideNav>
    </InstrumentsPage>
  );
}
