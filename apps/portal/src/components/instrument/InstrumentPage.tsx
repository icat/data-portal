import {
  usePath,
  SideNavElement,
  WithSideNav,
  Instrument,
  useConfig,
} from '@edata-portal/core';
import {
  INSTRUMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import {
  faChartLine,
  faList,
  faFileText,
  faCalendar,
  faGear,
} from '@fortawesome/free-solid-svg-icons';
import { Outlet } from 'react-router-dom';
import { NavTab } from 'components/navigation/NavTab';
import Alert from 'react-bootstrap/Alert';
import Nav from 'react-bootstrap/Nav';
import { Col, Row } from 'react-bootstrap';

export function InstrumentPage() {
  const config = useConfig();
  const instrumentName = usePath('instrumentName');

  const instruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
  });

  const instrument = instruments?.filter(
    (instrument) => instrument.name === instrumentName,
  )[0];

  const nav = (
    <Nav
      variant="pills"
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 5,
      }}
    >
      <NavTab
        to={`/instruments/${instrumentName}`}
        icon={faList}
        label="Datasets"
        end
      />
      <NavTab
        to={`/instruments/${instrumentName}/statistics`}
        icon={faChartLine}
        label="Statistics"
      />
      <NavTab
        to={`/instruments/${instrumentName}/investigations`}
        icon={faCalendar}
        label="Experiment sessions"
      />
      {config.ui.features.logbook && (
        <NavTab
          to={`/instruments/${instrumentName}/logbook`}
          icon={faFileText}
          label="Logbook"
        />
      )}
      <NavTab
        to={`/instruments/${instrumentName}/manage`}
        icon={faGear}
        label="Manage users"
      />
    </Nav>
  );

  return (
    <WithSideNav
      sideNav={<SideNavElement label={instrumentName}>{nav}</SideNavElement>}
    >
      <Col>
        <Row>
          <Col>
            <h1
              style={{
                fontSize: 'xx-large',
              }}
            >
              <Instrument instrumentName={instrumentName} />
            </h1>
          </Col>
        </Row>

        {instrument ? (
          <Row>
            <p
              style={{
                fontSize: 'large',
              }}
              className="text-muted"
            >
              {instrument.description}
            </p>
          </Row>
        ) : (
          <Alert variant="warning">Could not find beamline</Alert>
        )}
        <Row>
          <Outlet />
        </Row>
      </Col>
    </WithSideNav>
  );
}
