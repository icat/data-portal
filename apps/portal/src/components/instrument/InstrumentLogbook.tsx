import { usePath } from '@edata-portal/core';
import { useRegisterRecentlyVisited } from 'components/navigation/recentlyvisited/RecentlyVisitedContext';
import { RemoteComponent } from 'components/RemoteComponent';

export default function InstrumentLogbook() {
  const instrumentName = usePath('instrumentName');

  useRegisterRecentlyVisited(`Logbook`, {
    instrumentName: instrumentName,
  });

  return (
    <RemoteComponent
      remote={{
        name: 'logbook',
        component: './Logbook',
      }}
      props={{
        instrumentName,
      }}
    />
  );
}
