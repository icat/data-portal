import { SideNavElement, Loading, WithSideNav } from '@edata-portal/core';
import { Nav } from 'react-bootstrap';
import {
  faGear,
  faList,
  faCalendarDays,
} from '@fortawesome/free-solid-svg-icons';
import { NavTab } from 'components/navigation/NavTab';
import { Suspense } from 'react';

export function InstrumentsPage({ children }: { children?: React.ReactNode }) {
  return (
    <WithSideNav
      sideNav={
        <SideNavElement label="Beamlines">
          <Nav
            variant="pills"
            style={{
              display: 'flex',
              flexDirection: 'column',
              gap: 5,
            }}
          >
            <NavTab to={`/instruments`} icon={faList} label="List" end />
            <NavTab
              end
              to={`/instruments/manage`}
              icon={faGear}
              label="Manage users"
            />
            <NavTab
              end
              to={`/instruments/schedule`}
              icon={faCalendarDays}
              label="Schedule"
            />
          </Nav>
        </SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>{children}</Suspense>
    </WithSideNav>
  );
}
