import { TanstackBootstrapTable, OverlayLink } from '@edata-portal/core';
import {
  useGetEndpoint,
  USER_INSTRUMENT_LIST_ENDPOINT,
  Instrument,
} from '@edata-portal/icat-plus-api';
import { Link } from 'react-router-dom';
import { Alert, Button } from 'react-bootstrap';
import {
  ColumnDef,
  getCoreRowModel,
  getFilteredRowModel,
  useReactTable,
} from '@tanstack/react-table';
import {
  faArrowCircleRight,
  faCalendarAlt,
  faSquareArrowUpRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { InstrumentsPage } from 'components/instrument/InstrumentsPage';

export default function InstrumentsList() {
  const instruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    params: {
      filter: 'instrumentscientist',
    },
    default: [] as Instrument[],
  });

  const columns: ColumnDef<Instrument>[] = [
    {
      accessorFn: (row) => row,
      id: 'open',
      cell: (info) => {
        return (
          <Link
            to={`/instruments/${(info.getValue() as Instrument).name}/investigations`}
          >
            <Button
              size="sm"
              style={{
                width: '100%',
                textAlign: 'left',
                whiteSpace: 'nowrap',
                padding: '1px 0.5rem',
              }}
              variant="primary"
            >
              <FontAwesomeIcon icon={faArrowCircleRight} />
              <span style={{ marginLeft: 10 }}>
                {(info.getValue() as Instrument).name}
              </span>
            </Button>
          </Link>
        );
      },
      header: 'Name',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Description',
      cell: (info) => {
        return <>{(info.getValue() as Instrument).description}</>;
      },
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Calendar',
      cell: (info) => {
        return (
          <OverlayLink
            link={
              '/instruments/' +
              (info.getValue() as Instrument).name +
              '/investigations?view=Calendar'
            }
            icon={faCalendarAlt}
            tooltipText={'Open calendar'}
          ></OverlayLink>
        );
      },
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'URL',
      cell: (info) => {
        return (info.getValue() as Instrument).url ? (
          <OverlayLink
            link={(info.getValue() as Instrument).url}
            icon={faSquareArrowUpRight}
            tooltipText={'Open beamline page'}
            openOnNewTab={true}
          ></OverlayLink>
        ) : null;
      },
      enableColumnFilter: false,
    },
  ];

  const table = useReactTable<Instrument>({
    data: instruments,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
  });

  if (!instruments) {
    return <Alert variant="warning">Could not find beamlines</Alert>;
  }
  return (
    <InstrumentsPage>
      <TanstackBootstrapTable
        table={table}
        bordered={true}
        pagination={false}
        dataLength={instruments.length}
      />
    </InstrumentsPage>
  );
}
