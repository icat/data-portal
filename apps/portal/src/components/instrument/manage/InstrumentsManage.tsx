import { InstrumentsPage } from 'components/instrument/InstrumentsPage';
import { InstrumentScientistsManage } from 'components/instrument/manage/InstrumentScientistsManage';

export default function InstrumentsManage() {
  return (
    <InstrumentsPage>
      <InstrumentScientistsManage />
    </InstrumentsPage>
  );
}
