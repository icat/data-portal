import { usePath } from '@edata-portal/core';
import { InstrumentScientistsManage } from 'components/instrument/manage/InstrumentScientistsManage';
import { useRegisterRecentlyVisited } from 'components/navigation/recentlyvisited/RecentlyVisitedContext';
import NotFound from 'components/routing/NotFound';

export default function InstrumentManage() {
  const instrumentName = usePath('instrumentName');

  useRegisterRecentlyVisited(`Manage`, {
    instrumentName: instrumentName,
  });

  if (!instrumentName) return <NotFound />;

  return (
    <>
      <InstrumentScientistsManage instrumentName={instrumentName} />
    </>
  );
}
