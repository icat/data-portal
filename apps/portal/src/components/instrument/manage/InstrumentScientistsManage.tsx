import {
  useGetEndpoint,
  useMutateEndpoint,
  ADD_INSTRUMENT_SCIENTIST_ENDPOINT,
  DELETE_INSTRUMENT_SCIENTIST_ENDPOINT,
  GET_INSTRUMENT_SCIENTIST_LIST_ENDPOINT,
  InstrumentScientist,
} from '@edata-portal/icat-plus-api';
import { TanstackBootstrapTable, SelectInstruments } from '@edata-portal/core';
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { SelectUsers } from 'components/usermanagement/SelectUsers';
import { useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';

export function InstrumentScientistsManage({
  instrumentName,
}: {
  instrumentName?: string;
}) {
  const list = useGetEndpoint({
    endpoint: GET_INSTRUMENT_SCIENTIST_LIST_ENDPOINT,
    params: {
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
    },
    default: [] as InstrumentScientist[],
  });

  const table = useReactTable<InstrumentScientist>({
    data: list,
    columns: [
      { header: 'Name', accessorKey: 'user.fullName' },
      ...(instrumentName
        ? []
        : [
            {
              header: 'Beamline',
              accessorKey: 'instrument.name',
            },
          ]),

      {
        header: '',
        accessorFn: (row) => row,
        cell: (row) => <RevokeButton scientist={row.getValue()} />,
        enableColumnFilter: false,
        id: 'revokebtn',
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
  });

  return (
    <Col>
      <strong>Grant access:</strong>
      <AddScientist
        instrumentNames={instrumentName ? [instrumentName] : undefined}
      />
      <TanstackBootstrapTable dataLength={list.length} table={table} />
    </Col>
  );
}

function AddScientist({ instrumentNames }: { instrumentNames?: string[] }) {
  const [selectedUsernames, setSelectedUserNames] = useState<
    string[] | undefined
  >(undefined);
  const [selectedInstrumentNames, setSelectedInstrumentNames] = useState<
    string[]
  >([]);

  const add = useMutateEndpoint({
    endpoint: ADD_INSTRUMENT_SCIENTIST_ENDPOINT,
  });

  return (
    <Row className="g-2">
      <Col xs={12} md={6} xl={4} xxl={3}>
        <SelectUsers
          selectedUsers={selectedUsernames}
          onSelect={(users) => {
            if (!users) {
              setSelectedUserNames(undefined);
            } else {
              setSelectedUserNames(users?.map((u) => u.name));
            }
          }}
          placeholder="Select users you want to add..."
        />
      </Col>
      {instrumentNames ? null : (
        <Col xs={12} md={6} xl={4} xxl={3}>
          <SelectInstruments
            onChange={(instruments) => {
              setSelectedInstrumentNames(instruments?.map((i) => i.name) ?? []);
            }}
            selectedInstruments={selectedInstrumentNames}
            forUser={true}
            filter="instrumentscientist"
          />
        </Col>
      )}
      <Col xs={'auto'}>
        <Button
          disabled={
            !selectedUsernames?.length ||
            (!instrumentNames?.length && !selectedInstrumentNames?.length)
          }
          onClick={() => {
            add
              .mutateAsync({
                body: {
                  instrumentnames: instrumentNames
                    ? instrumentNames
                    : selectedInstrumentNames,
                  usernames: selectedUsernames,
                },
              })
              .then(() => {
                setSelectedUserNames(undefined);
                setSelectedInstrumentNames([]);
              });
          }}
        >
          Add
        </Button>
      </Col>
    </Row>
  );
}

function RevokeButton({ scientist }: { scientist: InstrumentScientist }) {
  const revoke = useMutateEndpoint({
    endpoint: DELETE_INSTRUMENT_SCIENTIST_ENDPOINT,
  });

  return (
    <Button
      variant="danger"
      onClick={() => {
        revoke.mutateAsync({
          body: {
            instrumentscientistsid: [scientist.id],
          },
        });
      }}
      size="sm"
    >
      Revoke
    </Button>
  );
}
