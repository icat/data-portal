import { useUser } from '@edata-portal/core';
import NotFound from 'components/routing/NotFound';
import { Outlet } from 'react-router-dom';

export function InstrumentScientistPage() {
  const user = useUser();

  if (!user?.isAdministrator && !user?.isInstrumentScientist) {
    return <NotFound />;
  }

  return <Outlet />;
}
