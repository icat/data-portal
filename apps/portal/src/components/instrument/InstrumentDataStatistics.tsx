import { NoData, usePath } from '@edata-portal/core';
import { DataStatistics } from 'components/statistics/data/DataStatistics';

export default function InstrumentDataStatistics() {
  const instrumentName = usePath('instrumentName');

  if (!instrumentName) return <NoData />;

  return <DataStatistics forInstrument={instrumentName} />;
}
