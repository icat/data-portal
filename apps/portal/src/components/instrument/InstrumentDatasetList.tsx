import { usePath, NoData, DatasetList } from '@edata-portal/core';
import { useRegisterRecentlyVisited } from 'components/navigation/recentlyvisited/RecentlyVisitedContext';

export default function InstrumentDatasetList() {
  const instrumentName = usePath('instrumentName');

  useRegisterRecentlyVisited(`Datasets`, {
    instrumentName: instrumentName,
  });

  if (!instrumentName) return <NoData />;

  return (
    <DatasetList
      instrumentName={instrumentName}
      groupBy="sample"
      showInvestigation
    />
  );
}
