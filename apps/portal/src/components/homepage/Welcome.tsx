import { useConfig } from '@edata-portal/core';
import { faWarning } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export function Welcome() {
  const config = useConfig();
  return (
    <div className="d-flex flex-column gap-2 p-3 bg-primary text-white h-100">
      <h1>
        <strong>
          {config.ui.facilityName} {config.ui.applicationTitle}
        </strong>
      </h1>
      <hr className="p-0 m-0" />
      <h3>
        <i>
          Find, access, interact and reuse
          <br /> data acquired at {config.ui.facilityName}
        </i>
      </h3>
      <div className="bg-light p-2 text-black">
        <i>
          <FontAwesomeIcon icon={faWarning} />{' '}
          <span
            dangerouslySetInnerHTML={{
              __html: config.ui.homePage.policyMessage,
            }}
          />
        </i>
      </div>
      {config.ui.projects.length > 0 && (
        <div
          className="d-flex flex-row gap-2 align-content-center"
          style={{
            fontSize: '1rem',
          }}
        >
          <strong>Explore our projects:</strong>
          {config.ui.projects.map((project) => (
            <a
              key={project.homepage}
              href={project.homepage}
              target="_blank"
              rel="noreferrer"
            >
              {project.title}
            </a>
          ))}
        </div>
      )}
    </div>
  );
}
