import { formatDateToDay } from '@edata-portal/core';
import React from 'react';
import { Badge, Table } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

type HomePageShortcutTableItem = {
  url: string;
  label?: string;
  startDate?: string;
  endDate?: string;
  instrument?: string;
  investigation?: string;
};

export const HOMEPAGE_SHORTCUT_TABLE_LIMIT = 7;

export function HomePageShortcutTable({
  items,
}: {
  items: HomePageShortcutTableItem[];
}) {
  const navigate = useNavigate();

  return (
    <Table hover size="sm" striped className=" w-100 m-0">
      <tbody>
        {items.slice(0, HOMEPAGE_SHORTCUT_TABLE_LIMIT).map((item) => {
          const columns = [
            <span key="label">{item.label}</span>,
            <Badge key="Beamline" bg="secondary" className="w-100">
              {item.instrument}
            </Badge>,
            <Badge key="investigation" bg="primary" className="w-100">
              {item.investigation}
            </Badge>,

            <React.Fragment key="date">
              {item.startDate ? (
                <span className="monospace">
                  {formatDateToDay(item.startDate)}{' '}
                  {item.endDate ? <>- {formatDateToDay(item.endDate)}</> : null}
                </span>
              ) : null}
            </React.Fragment>,
          ];
          return (
            <tr
              key={item.url}
              onClick={() => {
                navigate(item.url);
              }}
              style={{ cursor: 'pointer' }}
            >
              {columns.map((column, index) => {
                return (
                  <td
                    key={index}
                    className="text-primary"
                    style={
                      index === columns.length - 1 ? undefined : { width: '1%' }
                    }
                  >
                    {column}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}
