import { useUser } from '@edata-portal/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { HomepageTile } from 'components/homepage/HomepageTile';
import { FormEvent, useCallback, useState } from 'react';
import { Form, Row, Col, Button, InputGroup } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';

const usePresets: () => { params: string; display: string }[] = () => {
  const user = useUser();
  return [
    {
      display: 'All data',
      params: '',
    },
    ...(user?.isAuthenticated
      ? [
          {
            display: 'My data',
            params: '?filter=participant',
          },
        ]
      : []),
    {
      display: 'Public data',
      params: '?filter=released',
    },
    {
      display: 'Embargoed data',
      params: '?filter=embargoed',
    },
    ...(user?.isAuthenticated
      ? [
          {
            display: 'My calendar',
            params: '?filter=participant&view=Calendar',
          },
        ]
      : []),
  ];
};

export function HomepageSearch() {
  const [searchExp, setSearchExp] = useState('');
  const [searchSample, setSearchSample] = useState('');

  const navigate = useNavigate();

  const presets = usePresets();

  const submitExp = useCallback(
    (e: FormEvent<HTMLFormElement> | React.MouseEvent) => {
      navigate(`/experiments?search=${searchExp}`);
      e.preventDefault();
      e.stopPropagation();
    },
    [navigate, searchExp],
  );

  const submitSamples = useCallback(
    (e: FormEvent<HTMLFormElement> | React.MouseEvent) => {
      navigate(`/samples?search=${searchSample}`);
      e.preventDefault();
      e.stopPropagation();
    },
    [navigate, searchSample],
  );

  return (
    <HomepageTile title={'Start searching data'}>
      <Form onSubmit={submitExp}>
        <Row className="g-1">
          <Form.Label>
            <strong>Browse experiments:</strong>
          </Form.Label>
        </Row>
        <Row className="g-1">
          <InputGroup>
            <Form.Control
              value={searchExp}
              onChange={(e) => setSearchExp(e.target.value)}
              placeholder="Search experiment title, abstract, beamline, DOI..."
            />
            <Button onClick={submitExp}>
              <FontAwesomeIcon icon={faSearch} />
            </Button>
          </InputGroup>
        </Row>
      </Form>
      <Row
        className="align-items-center g-2"
        style={{
          marginTop: 0,
        }}
      >
        {presets.map((preset) => (
          <Col xs="auto" key={preset.display}>
            <Link to={`/experiments${preset.params}`}>
              <Button size="sm">{preset.display}</Button>
            </Link>
          </Col>
        ))}
      </Row>
      <hr />
      <Form onSubmit={submitSamples}>
        <Row className="g-1">
          <Form.Label>
            <strong>Browse samples:</strong>
          </Form.Label>
        </Row>
        <Row className="g-1">
          <InputGroup>
            <Form.Control
              value={searchSample}
              onChange={(e) => setSearchSample(e.target.value)}
              placeholder="Search sample name, molecule, organ..."
            />
            <Button onClick={submitSamples}>
              <FontAwesomeIcon icon={faSearch} />
            </Button>
          </InputGroup>
        </Row>
      </Form>
    </HomepageTile>
  );
}
