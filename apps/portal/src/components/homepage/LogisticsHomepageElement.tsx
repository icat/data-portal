import { HomepageTile } from 'components/homepage/HomepageTile';
import { RemoteComponent } from 'components/RemoteComponent';

export function LogisticsHomepageElement() {
  return (
    <HomepageTile
      title={'Logistics'}
      shortcut={{
        to: '/parcels',
        label: 'My parcels',
      }}
    >
      <RemoteComponent
        remote={{
          name: 'logistics',
          component: './MyLogisticsSummary',
        }}
      />
    </HomepageTile>
  );
}
