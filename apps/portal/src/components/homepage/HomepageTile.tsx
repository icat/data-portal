import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Suspense } from 'react';
import { Spinner } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function HomepageTile({
  title,
  children,
  shortcut,
}: {
  title: string;
  children: React.ReactNode;
  shortcut?: { to: string; label: string };
}) {
  return (
    <div className="bg-light p-3 h-100 d-flex flex-column ">
      <div className="d-flex justify-content-between">
        <h4>{title}</h4>
        {shortcut && (
          <div
            className="d-flex align-items-center"
            style={{
              fontSize: '1rem',
            }}
          >
            <Link to={shortcut.to}>
              <FontAwesomeIcon className="me-1" icon={faArrowCircleRight} />
              {shortcut.label}
            </Link>
          </div>
        )}
      </div>

      <hr />
      {children}
    </div>
  );
}

export function HomepageTileSuspense({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Suspense
      fallback={
        <HomepageTile title="Loading...">
          <Spinner />
        </HomepageTile>
      }
    >
      {children}
    </Suspense>
  );
}
