import {
  Instrument,
  useGetEndpoint,
  USER_INSTRUMENT_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { HomepageTile } from 'components/homepage/HomepageTile';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

export function MyBeamlines() {
  const instruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    params: {
      filter: 'instrumentscientist',
    },
    default: [] as Instrument[],
  });

  return (
    <HomepageTile
      title={'My beamlines'}
      shortcut={{
        to: '/instruments/schedule',
        label: 'Beamlines schedule',
      }}
    >
      <div className="d-flex justify-content-stretch gap-2 flex-wrap">
        {instruments.map((i) => (
          <Link
            className="d-flex"
            to={`/instruments/${i.name}/investigations`}
            key={i.id}
            style={{
              flexGrow: 1,
              textDecoration: 'none',
            }}
          >
            <Button
              style={{
                flexGrow: 1,
              }}
              key={i.id}
            >
              {i.name}
            </Button>
          </Link>
        ))}
      </div>
    </HomepageTile>
  );
}
