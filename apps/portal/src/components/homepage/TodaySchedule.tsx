import {
  sortInvestigationByDistanceFromNow,
  formatDateToIcatDate,
  addToDate,
} from '@edata-portal/core';
import {
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
  useMultiGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { CalendarDay } from 'components/browse/Calendar';
import { HomepageTile } from 'components/homepage/HomepageTile';
import dayjs from 'dayjs';
import { useMemo } from 'react';

const dateTomorrow = formatDateToIcatDate(
  addToDate(new Date(), {
    days: 1,
  }),
);
const dateToday = formatDateToIcatDate(new Date());

export function TodaySchedule() {
  const requests = useMemo(() => {
    const roles = ['participant', 'instrumentscientist'] as const;
    return roles.flatMap((role) => {
      return [
        {
          withHasAccess: true,
          filter: role,
          startDate: dateToday,
          endDate: dateTomorrow,
          sortBy: 'STARTDATE',
          sortOrder: 1,
        },
      ] as const;
    });
  }, []);

  const investigations = useMultiGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: requests,
    default: [] as Investigation[],
    options: {},
  });

  const sortedInvestigations = investigations
    .flatMap((list) => list)
    //remove duplicates
    .filter(
      (investigation, index, self) =>
        self.findIndex((i) => i.id === investigation.id) === index,
    )
    .sort(sortInvestigationByDistanceFromNow);

  return (
    <HomepageTile
      title={"Today's schedule"}
      shortcut={{
        to: '/experiments?filter=participant&view=Calendar',
        label: 'Calendar',
      }}
    >
      <div>
        <CalendarDay
          day={dayjs()}
          investigations={sortedInvestigations}
          currentMonth={dayjs()}
          hightlightToday={false}
          showFullDayLabel
        />
      </div>
    </HomepageTile>
  );
}
