import { useConfig, useUser } from '@edata-portal/core';
import { HomepageSearch } from 'components/homepage/HomepageSearch';
import { HomepageTileSuspense } from 'components/homepage/HomepageTile';
import { LogisticsHomepageElement } from 'components/homepage/LogisticsHomepageElement';
import { MyBeamlines } from 'components/homepage/MyBeamlines';
import { MyInvestigations } from 'components/homepage/MyInvestigations';
import { RecentlyVisitedElements } from 'components/homepage/RecentlyVisitedElements';
import { TodaySchedule } from 'components/homepage/TodaySchedule';
import { Welcome } from 'components/homepage/Welcome';
import { Col, Container, Row } from 'react-bootstrap';

export default function Homepage() {
  const user = useUser();
  const config = useConfig();
  return (
    <Container>
      <Col>
        <Row className="g-0">
          <Col xs={12} lg={6}>
            <Welcome />
          </Col>
          <Col xs={12} lg={6}>
            <HomepageSearch />
          </Col>
        </Row>
        <hr />
        {user?.isInstrumentScientist && (
          <Row className="g-2 pt-2">
            <Col xs={12} lg={6}>
              <HomepageTileSuspense>
                <MyBeamlines />
              </HomepageTileSuspense>
            </Col>
            <Col xs={12} lg={6}>
              <HomepageTileSuspense>
                <TodaySchedule />
              </HomepageTileSuspense>
            </Col>
          </Row>
        )}
        <Row className="g-2 pt-2">
          <Col xs={12} lg={6}>
            <HomepageTileSuspense>
              <MyInvestigations />
            </HomepageTileSuspense>
          </Col>
          <Col xs={12} lg={6}>
            <HomepageTileSuspense>
              <RecentlyVisitedElements />
            </HomepageTileSuspense>
          </Col>
        </Row>
        {user?.isAuthenticated && config.ui.features.logistics ? (
          <Row className="g-2 pt-2">
            <Col>
              <HomepageTileSuspense>
                <LogisticsHomepageElement />
              </HomepageTileSuspense>
            </Col>
          </Row>
        ) : null}
      </Col>
    </Container>
  );
}
