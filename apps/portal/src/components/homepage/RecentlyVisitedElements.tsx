import { HomePageShortcutTable } from 'components/homepage/HomePageShortcutTable';
import { HomepageTile } from 'components/homepage/HomepageTile';
import { useRecentlyVisited } from 'components/navigation/recentlyvisited/RecentlyVisitedContext';

export function RecentlyVisitedElements() {
  const { recentlyVisited } = useRecentlyVisited();

  return (
    <HomepageTile title={'Continue where you left off'}>
      {recentlyVisited.length ? (
        <HomePageShortcutTable
          items={recentlyVisited.map((item) => {
            return {
              url: item.url,
              label: item.label,
              startDate: item.target.investigationStartDate,
              endDate: item.target.investigationEndDate,
              instrument: item.target.instrumentName,
              investigation: item.target.investigationName,
            };
          })}
        />
      ) : (
        <span>Nothing yet. Start browsing!</span>
      )}
    </HomepageTile>
  );
}
