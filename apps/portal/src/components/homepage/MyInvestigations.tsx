import {
  parseDate,
  sortInvestigationByDistanceFromNow,
  useUser,
  formatDateToIcatDate,
  addToDate,
} from '@edata-portal/core';
import {
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
  useMultiGetEndpoint,
} from '@edata-portal/icat-plus-api';
import {
  HOMEPAGE_SHORTCUT_TABLE_LIMIT,
  HomePageShortcutTable,
} from 'components/homepage/HomePageShortcutTable';
import { HomepageTile } from 'components/homepage/HomepageTile';
import { useMemo } from 'react';

const dateTomorrow = formatDateToIcatDate(
  addToDate(new Date(), {
    days: 1,
  }),
);
const dateToday = formatDateToIcatDate(new Date());
const dateFutureLimit = formatDateToIcatDate(
  addToDate(new Date(), {
    days: 45,
  }),
);
const datePastLimit = formatDateToIcatDate(
  addToDate(new Date(), {
    days: -365,
  }),
);

export function MyInvestigations({ hideTitle }: { hideTitle?: boolean }) {
  const requests = useMemo(() => {
    const roles = ['participant', 'instrumentscientist'] as const;
    return roles.flatMap((role) => {
      return [
        {
          //get LIMIT next investigations for this role in the future
          withHasAccess: false,
          filter: role,
          startDate: dateTomorrow,
          endDate: dateFutureLimit,
          sortBy: 'STARTDATE',
          sortOrder: 1,
          limit: HOMEPAGE_SHORTCUT_TABLE_LIMIT,
        },
        {
          //get LIMIT last investigations for this role in the past
          withHasAccess: false,
          filter: role,
          startDate: datePastLimit,
          endDate: dateToday,
          sortBy: 'STARTDATE',
          sortOrder: -1,
          limit: HOMEPAGE_SHORTCUT_TABLE_LIMIT,
        },
      ] as const;
    });
  }, []);

  const investigations = useMultiGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: requests,
    default: [] as Investigation[],
    options: {},
  });

  const sortedInvestigations = investigations
    .flatMap((list) => list)
    //remove duplicates
    .filter(
      (investigation, index, self) =>
        self.findIndex((i) => i.id === investigation.id) === index,
    )
    .sort(sortInvestigationByDistanceFromNow);

  const user = useUser();

  const content = (
    <>
      {sortedInvestigations.length ? (
        <HomePageShortcutTable
          items={sortedInvestigations.map((item) => {
            const isStarted =
              (parseDate(item.startDate)?.getTime() || 0) < Date.now();
            const url = `/investigation/${item.id}/${
              isStarted ? 'datasets' : 'logistics'
            }`;
            return {
              url: url,
              label: isStarted ? 'Datasets' : 'Logistics',
              startDate: item.startDate,
              endDate: item.endDate,
              instrument: item.instrument?.name,
              investigation: item.name,
            };
          })}
        />
      ) : (
        <span>
          {user?.isAuthenticated
            ? 'No experiment session found for your account.'
            : 'You need to log-in to see your experiment sessions.'}
        </span>
      )}
    </>
  );

  if (hideTitle) {
    return content;
  }

  return (
    <HomepageTile
      title={'My data'}
      shortcut={{
        to: '/experiments?filter=participant',
        label: 'All my data',
      }}
    >
      {content}
    </HomepageTile>
  );
}
