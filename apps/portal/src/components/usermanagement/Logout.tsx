import { useAppOpenID, useAuthenticator } from '@edata-portal/core';
import { NavDropdown } from 'react-bootstrap';

export function Logout({ onClick }: { onClick: () => void }) {
  const { logout: logOutApp } = useAuthenticator();
  const { logout: logOutOpenid, authenticated: authenticatedOpenid } =
    useAppOpenID();

  return (
    <NavDropdown.Item
      onClick={() => {
        logOutApp();
        if (authenticatedOpenid) {
          logOutOpenid();
        }
        onClick();
      }}
      test-id="logout"
    >
      Logout
    </NavDropdown.Item>
  );
}
