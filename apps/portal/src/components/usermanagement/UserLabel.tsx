import { EllipsisView, useUser } from '@edata-portal/core';
import { SimpleIcatUser } from '@edata-portal/icat-plus-api';
import { UserAffiliationLabel } from 'components/usermanagement/UserAffiliationLabel';

export function UserLabel({ user }: { user: SimpleIcatUser }) {
  const currentUser = useUser();

  return (
    <div
      className="d-flex flex-column p-1"
      style={{
        lineHeight: 1,
      }}
    >
      <span>{user.fullName}</span>
      {(currentUser?.isAdministrator || currentUser?.isInstrumentScientist) && (
        <i>
          <small className="text-muted">
            <EllipsisView side="end">
              <UserAffiliationLabel affiliation={user.affiliation} />
            </EllipsisView>
          </small>
        </i>
      )}
    </div>
  );
}
