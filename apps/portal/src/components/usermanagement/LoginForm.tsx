import {
  useAuthenticator,
  type GenericAuthenticator,
} from '@edata-portal/core';
import type { FormEvent } from 'react';
import { useCallback, useEffect, useRef, useState } from 'react';
import {
  Alert,
  Button,
  Col,
  Container,
  Form,
  Row,
  Spinner,
} from 'react-bootstrap';

export function LoginForm({ auth }: { auth: GenericAuthenticator }) {
  const authenticator = useAuthenticator();

  const [error, setError] = useState<string>('');
  const [pending, setPending] = useState<boolean>(false);
  const [validated, setValidated] = useState<boolean>(false);

  const userRef = useRef<any>();
  const passRef = useRef<any>();
  const formRef = useRef<any>();

  useEffect(() => {
    userRef.current?.focus();
  }, [userRef]);

  useEffect(() => {
    if (authenticator.user) {
      // clear pending state after successful login
      setPending(false);
    }
  }, [authenticator.user]);

  const onSubmit = useCallback(
    (e: FormEvent<HTMLFormElement>) => {
      setPending(true);
      e.preventDefault();
      if (!formRef.current.checkValidity()) {
        e.stopPropagation();
        return;
      }
      const username = userRef.current?.value;
      const password = passRef.current?.value;

      authenticator
        .login(auth, username, password)
        .catch((newError) => {
          setError(newError.message as string);
          setValidated(false);
        })
        .finally(() => setPending(false));

      setValidated(true);

      setError('');
    },
    [
      setError,
      setPending,
      setValidated,
      authenticator,
      userRef,
      passRef,
      formRef,
      auth,
    ],
  );

  return (
    <Container>
      <Row>
        <Col>
          <Form onSubmit={onSubmit} validated={validated} ref={formRef}>
            {error && (
              <Row>
                <Col>
                  <Alert variant="danger">{error}</Alert>
                </Col>
              </Row>
            )}
            <Form.Group as={Row} className="mb-2">
              <Form.Label column>Username</Form.Label>
              <Col md={12} lg={8}>
                <Form.Control
                  type="text"
                  placeholder="Username"
                  ref={userRef}
                  disabled={pending}
                  required
                  test-id="username-input"
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column>Password</Form.Label>
              <Col md={12} lg={8}>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  ref={passRef}
                  disabled={pending}
                  required
                  test-id="password-input"
                />
              </Col>
            </Form.Group>

            <div className="d-grid gap-2 mt-2">
              <Button
                variant="primary"
                type="submit"
                disabled={pending}
                test-id="login-button"
              >
                {!pending && <>Login</>}
                {pending && (
                  <Spinner animation="border" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </Spinner>
                )}
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
