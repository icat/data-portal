import { DateType, formatDateToDay } from '@edata-portal/core';
import { useMemo } from 'react';

export function UserAffiliationLabel({
  affiliation,
  date,
}: {
  affiliation: string | undefined;
  date?: DateType | undefined;
}) {
  const name = useMemo(() => {
    if (!affiliation) return date ? formatDateToDay(date) : null;
    const separator = '*';
    if (!affiliation.includes(separator)) return affiliation;
    return affiliation.split(separator)[0];
  }, [affiliation, date]);

  if (!name) return null;

  return <>{name}</>;
}
