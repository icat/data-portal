import { useAppOpenID, useUser } from '@edata-portal/core';
import { UserAffiliationLabel } from 'components/usermanagement/UserAffiliationLabel';
import { Dropdown } from 'react-bootstrap';

export function AffiliationSwitch({ onClick }: { onClick: () => void }) {
  const user = useUser();

  const openid = useAppOpenID();

  if (!user?.usersByPrefix?.length) return null;
  if (!openid.authenticated) return null;

  const handleAffiliation = (suffix: string) => {
    openid.switchAffiliation(suffix);
  };

  return (
    <>
      <Dropdown.Divider />
      <Dropdown.ItemText className="text-black text-nowrap">
        <strong>Current affiliation:</strong>
        <br />
        <UserAffiliationLabel affiliation={user.affiliation} date={undefined} />
      </Dropdown.ItemText>

      <Dropdown.ItemText className="text-black text-nowrap">
        <strong>Switch affiliation to:</strong>
      </Dropdown.ItemText>
      {user.usersByPrefix.map((u) => {
        const suffix = u.name.split('/')[1];
        if (!suffix) return null;
        return (
          <Dropdown.Item
            className="text-info"
            key={suffix}
            onClick={() => {
              handleAffiliation(suffix);
              onClick();
            }}
          >
            <UserAffiliationLabel
              affiliation={u.affiliation}
              date={u.createTime}
            />
          </Dropdown.Item>
        );
      })}
    </>
  );
}
