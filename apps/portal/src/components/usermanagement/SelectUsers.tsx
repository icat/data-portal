import { removeAccents } from '@edata-portal/core';
import {
  SimpleIcatUser,
  useGetEndpoint,
  USER_LIST_ENDPOINT,
  USER_LIST_PRINCIPAL_INVESTIGATOR_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { UserLabel } from 'components/usermanagement/UserLabel';
import { Suspense, useMemo } from 'react';
import ReactSelect from 'react-select/async';

type SelectUsersProps = {
  selectedUsers: (SimpleIcatUser | string | number)[] | undefined;
  onSelect: (users: SimpleIcatUser[] | undefined) => void;
  placeholder?: string;
  isMulti?: boolean;
};

export function SelectUsers(
  props: SelectUsersProps & {
    investigationId?: number;
  },
) {
  const investigationId = props.investigationId;

  return (
    <Suspense fallback={<div>Loading users...</div>}>
      <div test-id="select-users">
        {investigationId ? (
          <LoadUsersForInvestigation
            investigationId={investigationId}
            {...props}
          />
        ) : (
          <LoadUsersForAdmin {...props} />
        )}
      </div>
    </Suspense>
  );
}

function LoadUsersForInvestigation(
  props: SelectUsersProps & {
    investigationId: number;
  },
) {
  const users = useGetEndpoint({
    endpoint: USER_LIST_PRINCIPAL_INVESTIGATOR_ENDPOINT,
    params: {
      investigationId: props.investigationId,
    },
    default: [] as SimpleIcatUser[],
  });

  return <RenderSelect {...props} users={users} />;
}

function LoadUsersForAdmin(props: SelectUsersProps) {
  const users = useGetEndpoint({
    endpoint: USER_LIST_ENDPOINT,
    default: [] as SimpleIcatUser[],
  });

  return <RenderSelect {...props} users={users} />;
}

function RenderSelect(props: SelectUsersProps & { users: SimpleIcatUser[] }) {
  const selectedOptions = useMemo(() => {
    return props.users.filter(
      (u) =>
        props.selectedUsers &&
        props.selectedUsers.some((v) => {
          if (typeof v === 'string') {
            return v === u.name;
          } else if (typeof v === 'number') {
            return v === u.id;
          } else {
            return v.name === u.name;
          }
        }),
    );
  }, [props.users, props.selectedUsers]);

  const value = useMemo(() => {
    return selectedOptions.map((selected) => ({
      value: selected,
      label: <UserLabel user={selected} />,
    }));
  }, [selectedOptions]);

  const loadOptions = useMemo(() => {
    const normalizeSearch = (s: string | undefined) =>
      removeAccents(s || '').toLocaleLowerCase();
    const options = props.users.map((u) => ({
      value: u,
      label: <UserLabel user={u} />,
      searchValue: normalizeSearch(u.fullName) + ' ' + normalizeSearch(u.name),
    }));
    const limit = 200;
    return (inputValue: string) =>
      new Promise<any[]>((resolve) => {
        if (inputValue?.length) {
          const searchValue = normalizeSearch(inputValue);
          const res = [];
          for (const u of options) {
            try {
              if (u.searchValue.includes(searchValue)) {
                res.push(u);
                if (res.length >= limit) {
                  break;
                }
              }
            } catch (e) {
              console.error(e);
            }
          }
          resolve(res);
        } else {
          resolve(options.slice(0, limit));
        }
      });
  }, [props.users]);

  const isMulti = props.isMulti === undefined ? true : props.isMulti;

  if (isMulti) {
    return (
      <ReactSelect
        value={value}
        loadOptions={loadOptions}
        defaultOptions
        isClearable
        placeholder={props.placeholder || 'Select users...'}
        isMulti
        onChange={(v) => {
          if (!v) {
            props.onSelect(undefined);
          } else {
            props.onSelect(v.map((u) => u.value));
          }
        }}
      />
    );
  } else {
    return (
      <ReactSelect
        value={value}
        loadOptions={loadOptions}
        defaultOptions
        isClearable
        placeholder={props.placeholder || 'Select user...'}
        onChange={(v) => {
          if (!v) {
            props.onSelect(undefined);
          } else {
            props.onSelect([v.value]);
          }
        }}
      />
    );
  }
}
