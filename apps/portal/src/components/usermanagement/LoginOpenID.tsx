import { OpenIDAuthenticator, useAppOpenID } from '@edata-portal/core';
import { faLock, faWarning } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Alert, Button } from 'react-bootstrap';

export function LoginOpenID({
  authenticator,
}: {
  authenticator: OpenIDAuthenticator;
}) {
  const openid = useAppOpenID();

  const handleLogin = () => {
    openid.login();
  };

  if (openid.status === 'unavailable') {
    return (
      <Alert variant="danger">
        <FontAwesomeIcon icon={faWarning} style={{ marginRight: 5 }} />
        SSO is not available
      </Alert>
    );
  }

  return (
    <Button onClick={handleLogin}>
      <FontAwesomeIcon icon={faLock} style={{ marginRight: 5 }} />
      {authenticator.message}
    </Button>
  );
}
