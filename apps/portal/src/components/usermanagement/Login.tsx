import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  type GenericAuthenticator,
  OpenIDAuthenticator,
  useAuthenticator,
  useConfig,
} from '@edata-portal/core';
import { Alert, Modal, Stack } from 'react-bootstrap';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import { LoginForm } from 'components/usermanagement/LoginForm';
import { useMemo } from 'react';
import { LoginOpenID } from 'components/usermanagement/LoginOpenID';

export function ShowLoginPage({ children }: { children: React.ReactNode }) {
  const authenticator = useAuthenticator();

  const showPage = useMemo(
    () => (authenticator.user ? true : false),
    [authenticator],
  );

  return (
    <>
      {showPage ? children : null}
      <LoginPopUp />
    </>
  );
}

function LoginPopUp() {
  const authenticator = useAuthenticator();

  const showLogin = useMemo(
    () => authenticator.showLoginPopup,
    [authenticator],
  );

  const config = useConfig();

  return (
    <Modal
      size="lg"
      centered
      show={showLogin}
      onHide={() => {
        authenticator.setShowLoginPopup(false);
      }}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <h3>Login to {config.ui.applicationTitle}</h3>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Login />
      </Modal.Body>
    </Modal>
  );
}

function Login() {
  const config = useConfig();

  const { accountCreationLink, note } = config.ui.loginForm;
  const { enabled, notes, title } = note;
  const { authenticators } = config.api.authentication;
  const enabledAuthenticators = authenticators.filter((auth) => auth.enabled);
  const feedbackUrl = `mailto:${config.ui.feedback.email}`;
  return (
    <Container>
      <Row>
        <Alert variant="warning">
          <strong>
            You can close this window to access public data, but you need to
            login to access private data.
          </strong>
        </Alert>
        <Tabs
          defaultActiveKey={enabledAuthenticators[0].name}
          id="login-tabs-authenticators"
          className="justify-content-left"
        >
          {enabledAuthenticators
            .filter((auth) => auth.enabled)
            .map((auth) => {
              return (
                <Tab
                  key={`tab-${auth.name}`}
                  eventKey={auth.name}
                  title={
                    <span test-id={`login-tab-${auth.name}`}>{auth.title}</span>
                  }
                >
                  <div
                    style={{
                      padding: 50,
                      display: 'flex',
                      justifyContent: 'center',
                      flexDirection: 'column',
                    }}
                  >
                    {auth.name === 'OpenID' && (
                      <LoginOpenID
                        authenticator={auth as OpenIDAuthenticator}
                      />
                    )}
                    {auth.name === 'Database' && (
                      <LoginElement authenticator={auth}>
                        <LoginForm auth={auth} />
                      </LoginElement>
                    )}
                  </div>
                </Tab>
              );
            })}
        </Tabs>
      </Row>
      <hr />
      <Row>
        <Col>
          <LoginStack
            accountCreationLink={accountCreationLink}
            noteEnabled={enabled}
            notes={notes}
            titleNote={title}
            feedbackUrl={feedbackUrl}
          />
        </Col>
      </Row>
    </Container>
  );
}
function LoginStack({
  accountCreationLink,
  noteEnabled,
  notes,
  titleNote,
  feedbackUrl,
}: {
  accountCreationLink: string;
  noteEnabled: boolean;
  notes: { text: string }[];
  titleNote: string;
  feedbackUrl: string;
}) {
  return (
    <Stack gap={3} style={{ margin: 10 }}>
      <div>
        Don{"'"}t have an account yet?{' '}
        <a target="_blank" rel="noopener noreferrer" href={accountCreationLink}>
          Register now
        </a>
      </div>
      <div>
        <h5 style={{ margin: '40 0 0 0' }}>
          <FontAwesomeIcon icon={faEnvelope} style={{ marginRight: 10 }} />
          <a href={feedbackUrl}>I need further assistance</a>
        </h5>
      </div>
      {noteEnabled && (
        <Alert variant="info">
          <Alert.Heading>{titleNote}</Alert.Heading>

          {notes.map(({ text }, index) => (
            <div key={`note-${index}`}>
              <div
                dangerouslySetInnerHTML={{
                  __html: text,
                }}
              />
              {index < notes.length - 1 && <hr />}
            </div>
          ))}
        </Alert>
      )}
    </Stack>
  );
}

function LoginElement({
  authenticator,
  children,
}: {
  authenticator: GenericAuthenticator;
  children: React.ReactNode;
}) {
  return (
    <>
      {authenticator.message && (
        <Alert key="alert-info-auth" variant="warning">
          <i>{authenticator.message}</i>
        </Alert>
      )}
      {children}
    </>
  );
}
