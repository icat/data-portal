import { usePath } from '@edata-portal/core';
import { DOI } from '@edata-portal/doi';
import { useMemo } from 'react';
import { Container } from 'react-bootstrap';

export default function DOIInfo() {
  const doiPrefix = usePath('doiPrefix');
  const doiSuffix = usePath('doiSuffix');

  const doi = useMemo(
    () => `${doiPrefix}/${doiSuffix}`,
    [doiPrefix, doiSuffix],
  );

  return (
    <Container fluid>
      <DOI doi={doi} />
    </Container>
  );
}
