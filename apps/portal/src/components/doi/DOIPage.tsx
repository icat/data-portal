import {
  Loading,
  parseDOI,
  SideNavElement,
  usePath,
  useTracking,
  WithSideNav,
} from '@edata-portal/core';
import {
  Dataset,
  DATASET_LIST_ENDPOINT,
  DOI_DATASET_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faInfoCircle, faList } from '@fortawesome/free-solid-svg-icons';
import { NavTab } from 'components/navigation/NavTab';
import { Suspense, useMemo } from 'react';
import { Badge, Nav } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';

export default function DOIPage() {
  const doiPrefix = usePath('doiPrefix');
  const doiSuffix = usePath('doiSuffix');

  const doi = useMemo(
    () => `${doiPrefix}/${doiSuffix}`,
    [doiPrefix, doiSuffix],
  );

  useTracking('DOI', 'View', doi);

  const nav = (
    <SideNavElement label={doi}>
      <Nav
        variant="pills"
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: 5,
        }}
      >
        <NavTab
          to={`/doi/${doi}`}
          icon={faInfoCircle}
          label="Description"
          end
        />
        <DOIDatasetsNav />
      </Nav>
    </SideNavElement>
  );

  return (
    <WithSideNav sideNav={nav}>
      <Suspense fallback={<Loading />}>
        <Outlet />
      </Suspense>
    </WithSideNav>
  );
}

function DOIDatasetsNav() {
  const doiPrefix = usePath('doiPrefix');
  const doiSuffix = usePath('doiSuffix');

  const doi = `${doiPrefix}/${doiSuffix}`;

  const parsedDoi = useMemo(() => parseDOI(doi), [doi]);

  const nbDatasetsDc = useGetEndpoint({
    endpoint: DOI_DATASET_LIST_ENDPOINT,
    params: {
      doiPrefix,
      doiSuffix,
      limit: 1,
    },
    default: [] as Dataset[],
    skipFetch: parsedDoi.type !== 'DC',
  });

  const nbDatasetsEs = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: parsedDoi.id,
      limit: 1,
    },
    default: [] as Dataset[],
    skipFetch: parsedDoi.type !== 'ES',
  });

  const nb = useMemo(() => {
    const allValues = [...nbDatasetsDc, ...nbDatasetsEs];
    if (allValues.length >= 1) return allValues[0]?.meta?.page.total || 0;
    return 0;
  }, [nbDatasetsDc, nbDatasetsEs]);

  if (!nb) return null;

  return (
    <NavTab
      to={`/doi/${doi}/datasets`}
      icon={faList}
      label={
        <>
          Datasets <Badge className="bg-secondary ms-1">{nb}</Badge>
        </>
      }
    />
  );
}
