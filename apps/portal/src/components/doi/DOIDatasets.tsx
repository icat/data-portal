import {
  DatasetList,
  PaginationMenu,
  useEndpointPagination,
  usePath,
  parseDOI,
  NoData,
  useViewers,
} from '@edata-portal/core';
import {
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
  Investigation,
  DOI_DATASET_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export default function DOIDatasets() {
  const doiPrefix = usePath('doiPrefix');
  const doiSuffix = usePath('doiSuffix');

  const doi = `${doiPrefix}/${doiSuffix}`;

  const parsedDoi = useMemo(() => parseDOI(doi), [doi]);

  return parsedDoi.type === 'DC' ? (
    <LoadDCDatasets doiPrefix={doiPrefix} doiSuffix={doiSuffix} />
  ) : (
    <LoadESDatasets investigationId={parsedDoi.id} />
  );
}

function LoadESDatasets({ investigationId }: { investigationId: string }) {
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
    default: [] as Investigation[],
  });

  const viewers = useViewers();

  const investigation = useMemo(
    () => (investigations.length > 0 ? investigations[0] : undefined),
    [investigations],
  );

  if (!investigation) return <NoData />;

  return viewers.viewInvestigation(investigation);
}

function LoadDCDatasets({
  doiPrefix,
  doiSuffix,
}: {
  doiPrefix: string;
  doiSuffix: string;
}) {
  const paginatedDatasets = useEndpointPagination({
    endpoint: DOI_DATASET_LIST_ENDPOINT,
    params: {
      doiPrefix,
      doiSuffix,
    },
    paginationParams: {
      paginationKey: 'datasets',
    },
  });

  return (
    <>
      <PaginationMenu {...paginatedDatasets} />
      <DatasetList datasets={paginatedDatasets.data} groupBy="dataset" />
      <PaginationMenu {...paginatedDatasets} />
    </>
  );
}
