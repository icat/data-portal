import { usePath, useViewers } from '@edata-portal/core';
import {
  useGetEndpoint,
  INVESTIGATION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useRegisterInvestigationRecentlyVisited } from 'components/navigation/recentlyvisited/RecentlyVisitedContext';
import { Alert } from 'react-bootstrap';

export default function InvestigationDatasets() {
  const investigationId = usePath('investigationId');

  useRegisterInvestigationRecentlyVisited(investigationId, 'Datasets');

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
  });

  const viewers = useViewers();

  if (!investigations || investigations.length === 0) {
    return <Alert>Could not find investigation</Alert>;
  }

  return <>{viewers.viewInvestigation(investigations[0])}</>;
}
