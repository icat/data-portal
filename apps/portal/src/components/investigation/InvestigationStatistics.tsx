import { Loading, usePath, WithSideNav } from '@edata-portal/core';
import {
  useGetEndpoint,
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { InvestigationTimeline } from 'components/investigation/InvestigationTimeline';
import { DataStatistics } from 'components/investigation/DataStatistics';
import { Suspense, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { GenericStatisticSideNav } from 'components/investigation/sideNavigationBar/GenericStatisticSideNav';
import { InvestigationTitle } from 'components/investigation/summary/InvestigationTitle';
import { InvestigationInfoBox } from 'components/investigation/InvestigationInfoBox';
import { InvestigationBeamtimeStatistics } from 'components/investigation/InvestigationBeamtimeStatistics';

export default function InvestigationStatistics() {
  const investigationId = usePath('investigationId');
  const [statistic, setStatistic] = useState<string | undefined>('__volume');
  const [accumulate, setAccumulate] = useState<boolean>(false);

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
    default: [] as Investigation[],
  });

  const investigation = investigations.length ? investigations[0] : undefined;

  if (!investigation) {
    return <></>;
  }

  return (
    <WithSideNav
      sideNav={
        <GenericStatisticSideNav
          setStatistic={(statistic) => setStatistic(statistic)}
          setAccumulate={(accumulate) => setAccumulate(accumulate)}
        />
      }
    >
      <Container fluid>
        <Col>
          <InvestigationTitle investigation={investigation} />
          <Row className="mt-2">
            <Col>
              <Suspense fallback={<Loading />}>
                <InvestigationInfoBox title="Acquisition timeline">
                  <Container fluid>
                    <InvestigationTimeline investigation={investigation} />
                  </Container>
                </InvestigationInfoBox>
              </Suspense>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col>
              <Suspense fallback={<Loading />}>
                <InvestigationBeamtimeStatistics
                  investigation={investigation}
                />
              </Suspense>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col>
              <Suspense fallback={<Loading />}>
                <InvestigationInfoBox title="Data statistics">
                  <DataStatistics
                    investigation={investigation}
                    accumulative={accumulate}
                    investigationId={investigationId}
                    statistic={statistic}
                  />
                </InvestigationInfoBox>
              </Suspense>
            </Col>
          </Row>
        </Col>
      </Container>
    </WithSideNav>
  );
}
