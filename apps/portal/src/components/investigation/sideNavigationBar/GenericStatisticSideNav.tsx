import { SideNavElement, SideNavFilter } from '@edata-portal/core';
import { useState } from 'react';
import { Col } from 'react-bootstrap';
import ReactSelect from 'react-select';
import Form from 'react-bootstrap/Form';
import { ELAPSED_TIME, FILE_COUNT, DATASET_VOLUME } from '@edata-portal/core';

export function GenericStatisticSideNav({
  setStatistic,
  setAccumulate,
}: {
  setStatistic: (statistics: string) => void;
  setAccumulate: (accumulate: boolean) => void;
}) {
  const [selected, isSelected] = useState<boolean>(false);

  return (
    <SideNavElement label="Statistics">
      <Col>
        <SideNavFilter label={'Parameter'}>
          <ParamSelector setStatistic={setStatistic} />
        </SideNavFilter>
        <SideNavFilter label={'Accumulate'}>
          <Form.Check
            type="switch"
            id="custom-switch"
            checked={selected}
            onChange={(e) => {
              isSelected(e.target.checked);
              setAccumulate(e.target.checked);
            }}
          />
        </SideNavFilter>
      </Col>
    </SideNavElement>
  );
}

function ParamSelector({
  setStatistic,
}: {
  setStatistic: (statistic: string) => void;
}) {
  const options: any = [
    { value: DATASET_VOLUME, label: 'Volume' },
    { value: FILE_COUNT, label: 'File count' },
    { value: ELAPSED_TIME, label: 'Elapsed Time' },
  ];
  const [selected, setSelected] = useState<any>(options[0]);

  return (
    <ReactSelect
      options={options}
      value={selected}
      onChange={(e) => {
        setStatistic(e.value);
        setSelected(e);
      }}
      isClearable={true}
      placeholder="Select statistics"
    />
  );
}
