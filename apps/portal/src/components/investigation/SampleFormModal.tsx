import {
  useConfig,
  getParameterByName,
  FormObjectDescription,
  RenderForm,
} from '@edata-portal/core';
import {
  CREATE_SAMPLE_PARAMETER,
  UPDATE_SAMPLE_PARAMETER,
  type Sample,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { Modal } from 'react-bootstrap';

type FormType = {
  name: string;
  description: string;
};

export function SampleFormModal({
  investigationId,
  sample,
  show,
  handleClose,
}: {
  investigationId: number;
  sample: Sample;
  show: boolean;
  handleClose: () => void;
}) {
  const config = useConfig();

  const sampleParameterDescription = getParameterByName(
    config.ui.sample.descriptionParameterName,
    sample.parameters,
  );

  const updateParam = useMutateEndpoint({
    endpoint: UPDATE_SAMPLE_PARAMETER,
    params: {
      id: sampleParameterDescription?.id.toString(),
      investigationId: investigationId.toString(),
    },
  });

  const createParam = useMutateEndpoint({
    endpoint: CREATE_SAMPLE_PARAMETER,
    params: {
      sampleId: sample.id.toString(),
      investigationId: investigationId.toString(),
    },
  });

  const formDescription: FormObjectDescription<FormType> = {
    name: {
      label: 'Name',
      type: 'text',
      readonly: true,
    },
    description: {
      label: 'Description',
      type: 'textarea',
      required: true,
    },
  };

  const formInitialValue: FormType = {
    name: sample.name,
    description: sampleParameterDescription?.value || '',
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit sample</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <RenderForm
          description={formDescription}
          initialValue={formInitialValue}
          onSubmit={(value) => {
            const values = {
              investigationId: investigationId,
              id: sampleParameterDescription?.id,
              value: value.description,
              name: config.ui.sample.descriptionParameterName,
              sampleId: sample.id,
            };
            if (sampleParameterDescription) {
              updateParam.mutateAsync({ body: values });
            } else {
              createParam.mutateAsync({ body: values });
            }
          }}
          submitLabel="Save"
          onCancel={() => handleClose()}
        />
      </Modal.Body>
    </Modal>
  );
}
