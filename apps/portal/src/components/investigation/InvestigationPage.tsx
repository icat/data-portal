import {
  usePath,
  SideNavElement,
  useUser,
  Config,
  useConfig,
  WithSideNav,
  ACQUISITION_DATASET_COUNT,
  DATASET_COUNT,
} from '@edata-portal/core';
import {
  INVESTIGATION_LIST_ENDPOINT,
  SHIPMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import {
  faChartLine,
  faFileText,
  faGear,
  faInfoCircle,
  faList,
} from '@fortawesome/free-solid-svg-icons';
import { NavTab } from 'components/navigation/NavTab';
import { InvestigationJobsHeader } from 'components/reprocessing/jobs/InvestigationJobs';
import { useMemo } from 'react';
import { Alert, Col, Nav } from 'react-bootstrap';

import { Outlet } from 'react-router-dom';

export default function InvestigationPage() {
  const investigationId = usePath('investigationId');
  const config: Config = useConfig();

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
      withHasAccess: true,
    },
  });

  const investigation = useMemo(
    () =>
      investigations && investigations.length ? investigations[0] : undefined,
    [investigations],
  );

  const shipments = useGetEndpoint({
    endpoint: SHIPMENT_LIST_ENDPOINT,
    params: {
      investigationId,
    },
  });
  const user = useUser();

  const hasAccess = useMemo(() => {
    return investigation?.canAccessDatasets !== undefined
      ? investigation?.canAccessDatasets
      : true;
  }, [investigation]);

  const nav = (
    <Nav
      variant="pills"
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 5,
      }}
    >
      <NavTab
        to={`/investigation/${investigationId}`}
        icon={faInfoCircle}
        label="Experiment session"
        end
        testId="investigation-experiment"
      />
      {hasAccess && (
        <NavTab
          to={`/investigation/${investigationId}/statistics`}
          icon={faChartLine}
          label="Statistics"
          testId="investigation-statistics"
        />
      )}
      {hasAccess && (
        <NavTab
          to={`/investigation/${investigationId}/datasets`}
          icon={faList}
          label="Datasets"
          badge={
            investigation?.parameters?.[ACQUISITION_DATASET_COUNT] ||
            investigation?.parameters?.[DATASET_COUNT]
          }
          testId="investigation-datasets"
        />
      )}
      {hasAccess && config.ui.features.reprocessing && (
        <InvestigationJobsHeader investigationId={investigationId} />
      )}
      {hasAccess && config.ui.features.logbook && (
        <NavTab
          to={`/investigation/${investigationId}/logbook`}
          icon={faFileText}
          label="Logbook"
          testId="investigation-logbook"
        />
      )}
      {user?.isAuthenticated &&
      shipments !== undefined &&
      config.ui.features.logistics ? (
        <NavTab
          to={`/investigation/${investigationId}/logistics`}
          icon={faGear}
          label="Prepare"
          testId="investigation-prepare"
        />
      ) : null}
    </Nav>
  );

  return (
    <WithSideNav sideNav={<SideNavElement>{nav}</SideNavElement>}>
      {investigation ? (
        <Col>
          {!hasAccess ? (
            <Alert variant="warning" test-id="no-access-alert">
              You do not have access to this experiment session.
            </Alert>
          ) : null}
          <Outlet />
        </Col>
      ) : (
        <Alert variant="warning">Could not find experiment session</Alert>
      )}
    </WithSideNav>
  );
}
