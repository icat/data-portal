import { HelpIcon } from '@edata-portal/core';
import type { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Card } from 'react-bootstrap';

enum Align {
  center = 'center',
  left = 'left',
  right = 'right',
}
function isValidAlign(value?: string): value is keyof typeof Align {
  return !!value && value in Align;
}

export function InvestigationInfoBox({
  title,
  children,
  textAlign,
  icon,
  help,
}: {
  title: string;
  children: React.ReactNode | React.ReactNode[];
  textAlign?: string;
  icon?: IconProp;
  help?: string;
}) {
  const align: Align = isValidAlign(textAlign) ? Align[textAlign] : Align.left;
  const bodyStyle = {
    padding: '0.5rem',
    textAlign: Align[align],
  };

  return (
    <Card>
      <Card.Header>
        {icon && <FontAwesomeIcon icon={icon} style={{ marginRight: 10 }} />}
        {title}
        {help && (
          <HelpIcon
            message={help}
            style={{
              marginLeft: 10,
            }}
          />
        )}
      </Card.Header>
      <Card.Body style={bodyStyle}>{children}</Card.Body>
    </Card>
  );
}
