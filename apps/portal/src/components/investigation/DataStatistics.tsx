import {
  toMiliseconds,
  toMinutes,
  getInvestigationParam,
  MetadataTable,
  NoData,
  parseDate,
  immutableArray,
  toUnitPrefix,
  stringUnitPrefix,
  getBestPrefix,
  stringifyWithUnitPrefix,
  ACQUISITION_DATASET_VOLUME,
  PROCESSED_DATASET_VOLUME,
} from '@edata-portal/core';
import {
  DATASET_TYPE_ACQUISITION,
  DATASET_TIMELINE_ENDPOINT,
  DatasetTimeLine,
  useGetEndpoint,
  Investigation,
} from '@edata-portal/icat-plus-api';
import { TimeSeriesScatteringPlot } from 'components/statistics/TimeSeriesScatteringPlot';
import { useMemo } from 'react';
import { Col, Container, Row } from 'react-bootstrap';

export type StatisticsPlotRecord = {
  x: Date;
  y: number;
  time: DatasetTimeLine;
  cummulative: number;
};

export type StatisticsPlotSerie = {
  data: StatisticsPlotRecord;
  opacity?: number;
  title?: string;
  color?: string;
};

function getFormatter(statistic: string, units: string): (v: number) => number {
  if (statistic === '__volume') {
    return (v: number) => toUnitPrefix(v, stringUnitPrefix(units));
  }
  if (statistic === '__elapsedTime') {
    if (units !== undefined)
      switch (units) {
        case 'm':
          return (v: number) => Number(toMinutes(v));
        case 'ms':
          return (v: number) => Number(toMiliseconds(v));
        case 's':
          return (v: number) => v;
        default:
          break;
      }
    return (x) => x;
  }

  return (x) => x;
}

/**
 * Converts the timeline into statisticsplotrecord that allows to plot it
 * @param series
 * @param filterBy
 * @param isAccumulative
 * @param statistic
 * @param units
 * @returns
 */
function parseData(
  series: DatasetTimeLine[] | undefined,
  filterBy: { (d: DatasetTimeLine): boolean },
  isAccumulative: boolean,
  statistic: string,
  units: string,
): StatisticsPlotRecord[] {
  let cummulative: number = 0;

  return series
    ? series
        .filter((time: DatasetTimeLine) => filterBy(time))
        .map((time) => {
          const formatter = getFormatter(statistic, units);
          const value = formatter(Number.parseInt((time as any)[statistic]));

          cummulative = cummulative + value;

          const statistics: StatisticsPlotRecord = {
            x: parseDate(time.endDate) || new Date(0),
            y: isAccumulative ? Number(cummulative) : value,
            time: time,
            cummulative: Number(cummulative),
          };
          return statistics;
        })
    : [];
}

const getVolumeUnits = (timeseries: DatasetTimeLine[] | undefined) => {
  if (timeseries === undefined) return '';
  const values = timeseries.map((d) => d.__volume);
  return getBestPrefix(values);
};

const getTimeUnits = (timeseries: DatasetTimeLine[] | undefined) => {
  if (timeseries === undefined) return '';
  const min: number = timeseries.reduce((min, obj) => {
    return obj.__elapsedTime < min ? obj.__elapsedTime : min;
  }, timeseries[0].__elapsedTime);

  if (min > 60) {
    return 'm';
  }
  if (min > 1) {
    return 's';
  }

  return 'ms';
};

/**
 * Returns the units depending on the statistics
 * @param statistic
 * @param timeseries
 * @returns
 */
const getStatisticsUnits = (
  statistic: string,
  timeseries: DatasetTimeLine[] | undefined,
): string => {
  if (!timeseries?.length) return '';

  if (statistic === '__volume') {
    return getVolumeUnits(timeseries);
  }
  if (statistic === '__fileCount') {
    return '';
  }
  if (statistic === '__elapsedTime') {
    return getTimeUnits(timeseries);
  }
  return '';
};

const getCaption = (statistic: string) => {
  if (statistic === '__volume') {
    return 'Volume';
  }
  if (statistic === '__fileCount') {
    return 'File Count';
  }
  if (statistic === '__elapsedTime') {
    return 'Elapsed Time';
  }
  return '';
};

const sortData = (series: DatasetTimeLine[] | undefined) => {
  if (!series) return [];
  return immutableArray(series)
    .sort((a, b) => {
      return (
        (parseDate(a.endDate)?.getTime() || 0) -
        (parseDate(b.endDate)?.getTime() || 0)
      );
    })
    .toArray();
};

export function DataStatistics({
  investigationId,
  accumulative = false,
  investigation,
  statistic = '__volume',
}: {
  investigationId: string;
  accumulative?: boolean;
  investigation: Investigation;
  statistic?: string;
}) {
  const timeseries = useGetEndpoint({
    endpoint: DATASET_TIMELINE_ENDPOINT,
    params: {
      investigationId,
    },
  });

  const sortedSeries = useMemo(() => sortData(timeseries), [timeseries]);

  let units = useMemo(
    () => getStatisticsUnits(statistic, sortedSeries),
    [sortedSeries, statistic],
  );

  const processed = useMemo(
    () =>
      parseData(
        sortedSeries,
        (d: DatasetTimeLine) => {
          return d.datasetType !== DATASET_TYPE_ACQUISITION;
        },
        accumulative,
        statistic,
        units,
      ),
    [sortedSeries, accumulative, statistic, units],
  );

  const raw = useMemo(
    () =>
      parseData(
        sortedSeries,
        (d: DatasetTimeLine) => {
          return d.datasetType === DATASET_TYPE_ACQUISITION;
        },
        accumulative,
        statistic,
        units,
      ),
    [sortedSeries, accumulative, statistic, units],
  );

  if (!timeseries?.length)
    return (
      <Container fluid>
        <NoData />
      </Container>
    );

  return (
    <Row>
      <Col
        xs={8}
        style={{
          display: 'inline-block',
          float: 'none',
        }}
      >
        {timeseries && (
          <>
            <TimeSeriesScatteringPlot
              units={units}
              data={raw}
              label={'Raw ' + getCaption(statistic) + ' ' + units}
              color={'#82ca9d'}
              accumulative={accumulative}
            />
            <TimeSeriesScatteringPlot
              units={units}
              data={processed}
              label={'Processed ' + getCaption(statistic) + ' ' + units}
              color={'#8884d8'}
              accumulative={accumulative}
            />
          </>
        )}
      </Col>
      <Col xs={4}>
        <h4>Summary</h4>
        <MetadataTable
          parameters={[
            {
              caption: 'Samples',
              value: getInvestigationParam(investigation, '__sampleCount'),
            },
            {
              caption: 'Volume',
              value: stringifyWithUnitPrefix(
                getInvestigationParam(investigation, '__volume'),
                2,
                'B',
              ),
            },
            {
              caption: 'Datasets',
              value: getInvestigationParam(investigation, '__datasetCount'),
            },
            {
              caption: 'Archived',
              value:
                getInvestigationParam(
                  investigation,
                  '__archivedDatasetCount',
                ) === getInvestigationParam(investigation, '__datasetCount')
                  ? 'OK'
                  : 'ERROR',
            },
          ]}
        ></MetadataTable>
        <h4>Datasets</h4>
        <MetadataTable
          parameters={[
            {
              caption: 'Raw ',
              value:
                getInvestigationParam(
                  investigation,
                  '__acquisitionDatasetCount',
                ) +
                ' (' +
                stringifyWithUnitPrefix(
                  getInvestigationParam(
                    investigation,
                    ACQUISITION_DATASET_VOLUME,
                  ),
                  2,
                  'B',
                ) +
                ')',
            },
            {
              caption: 'Processed',
              value:
                getInvestigationParam(
                  investigation,
                  '__processedDatasetCount',
                ) +
                ' (' +
                stringifyWithUnitPrefix(
                  getInvestigationParam(
                    investigation,
                    PROCESSED_DATASET_VOLUME,
                  ),
                  2,
                  'B',
                ) +
                ')',
            },
          ]}
        ></MetadataTable>
      </Col>
    </Row>
  );
}
