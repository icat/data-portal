import {
  formatDateToDayAndShortTime,
  formatDateToDayAndTime,
  formatDateToTime,
  MetadataTable,
  parseDate,
  useTimelineFormatter,
  formatDate,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  Investigation,
  DatasetTimeLine,
  DATASET_TIMELINE_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useEffect, useState } from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';

const TIMELINE_HEIGHT = 40;
const LABEL_HEIGHT_TOP = 20;
const LABEL_HEIGHT_BOTTOM = 20;

export function InvestigationTimeline({
  investigation,
}: {
  investigation: Investigation;
}) {
  const timeseries: DatasetTimeLine[] | undefined = useGetEndpoint({
    endpoint: DATASET_TIMELINE_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
    },
  });

  const investigationStartTimeStamp = parseDate(
    investigation.startDate,
  )?.getTime();
  const investigationEndTimeStamp = parseDate(investigation.endDate)?.getTime();

  const acquisitionStartTimeStamp = timeseries?.reduce((min, dataset) => {
    const startTimeStamp = parseDate(dataset.startDate)?.getTime();
    return startTimeStamp && startTimeStamp < min ? startTimeStamp : min;
  }, Number.MAX_SAFE_INTEGER);

  const acquisitionEndTimeStamp = timeseries?.reduce((max, dataset) => {
    const endTimeStamp = parseDate(dataset.endDate)?.getTime();
    return endTimeStamp && endTimeStamp > max ? endTimeStamp : max;
  }, Number.MIN_SAFE_INTEGER);

  const timelineStartTimeStamp = Math.min(
    investigationStartTimeStamp || Number.MAX_SAFE_INTEGER,
    acquisitionStartTimeStamp || Number.MAX_SAFE_INTEGER,
  );

  const timelineEndTimeStamp = Math.max(
    investigationEndTimeStamp || Number.MIN_SAFE_INTEGER,
    acquisitionEndTimeStamp || Number.MIN_SAFE_INTEGER,
  );

  if (
    timelineStartTimeStamp === Number.MAX_SAFE_INTEGER ||
    timelineEndTimeStamp === Number.MIN_SAFE_INTEGER ||
    timelineStartTimeStamp >= timelineEndTimeStamp
  ) {
    return null;
  }

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
        width: '100%',
        height: TIMELINE_HEIGHT + LABEL_HEIGHT_TOP + 2 * LABEL_HEIGHT_BOTTOM,
      }}
    >
      <div
        style={{
          position: 'absolute',
          top: LABEL_HEIGHT_TOP,
          left: 0,
          right: 0,
          height: TIMELINE_HEIGHT,
          backgroundColor: 'white',
          zIndex: 1,
          border: '1px solid #ddd',
          borderRadius: 5,
          overflow: 'hidden',
        }}
      >
        {timeseries?.map((dataset) => {
          return (
            <DatasetBox
              key={dataset.datasetId}
              dataset={dataset}
              timelineStartTimeStamp={timelineStartTimeStamp}
              timelineEndTimeStamp={timelineEndTimeStamp}
            />
          );
        })}
      </div>
      <div className="d-block">
        <InvestigationTimelineTimeTicks
          timelineStartTimeStamp={timelineStartTimeStamp}
          timelineEndTimeStamp={timelineEndTimeStamp}
        />
      </div>
      <InvestigationStartEndMarkers
        timelineStartTimeStamp={timelineStartTimeStamp}
        timelineEndTimeStamp={timelineEndTimeStamp}
        investigationStartTimeStamp={investigationStartTimeStamp}
        investigationEndTimeStamp={investigationEndTimeStamp}
      />
      <NowMarker
        timelineStartTimeStamp={timelineStartTimeStamp}
        timelineEndTimeStamp={timelineEndTimeStamp}
      />
    </div>
  );
}

function InvestigationStartEndMarkers({
  timelineStartTimeStamp,
  timelineEndTimeStamp,
  investigationStartTimeStamp,
  investigationEndTimeStamp,
}: {
  timelineStartTimeStamp?: number;
  timelineEndTimeStamp?: number;
  investigationStartTimeStamp?: number;
  investigationEndTimeStamp?: number;
}) {
  if (
    !timelineStartTimeStamp ||
    !timelineEndTimeStamp ||
    !investigationStartTimeStamp ||
    !investigationEndTimeStamp
  ) {
    return null;
  }

  const investigationStartPosition =
    ((investigationStartTimeStamp - timelineStartTimeStamp) /
      (timelineEndTimeStamp - timelineStartTimeStamp)) *
    100;

  const investigationEndPosition =
    ((investigationEndTimeStamp - timelineStartTimeStamp) /
      (timelineEndTimeStamp - timelineStartTimeStamp)) *
    100;

  return (
    <>
      {investigationStartTimeStamp !== timelineStartTimeStamp && (
        <>
          <div
            style={{
              position: 'absolute',
              top: 0,
              bottom: 2 * LABEL_HEIGHT_BOTTOM,
              left: `${investigationStartPosition}%`,
              width: 2,
              backgroundColor: 'red',
              zIndex: 2,
            }}
          />
          <div
            style={{
              position: 'absolute',
              top: 0,
              ...(investigationStartPosition < 50
                ? { left: `${investigationStartPosition}%` }
                : { right: `${100 - investigationStartPosition}%` }),
              right: `${100 - investigationStartPosition}%`,
              zIndex: 2,
              padding: '0px 5px',
              fontSize: 12,
              whiteSpace: 'nowrap',
            }}
          >
            Scheduled start:{' '}
            {formatDateToDayAndShortTime(investigationStartTimeStamp)}
          </div>
        </>
      )}
      {investigationEndTimeStamp !== timelineEndTimeStamp && (
        <>
          <div
            style={{
              position: 'absolute',
              top: LABEL_HEIGHT_TOP,
              bottom: 0,
              left: `${investigationEndPosition}%`,
              width: 2,
              backgroundColor: 'red',
              zIndex: 2,
            }}
          />
          <div
            style={{
              position: 'absolute',
              bottom: 0,
              ...(investigationEndPosition < 50
                ? { left: `${investigationEndPosition}%` }
                : { right: `${100 - investigationEndPosition}%` }),
              zIndex: 2,
              padding: '0px 5px',
              fontSize: 12,
              whiteSpace: 'nowrap',
            }}
          >
            Scheduled end:{' '}
            {formatDateToDayAndShortTime(investigationEndTimeStamp)}
          </div>
        </>
      )}
    </>
  );
}

function NowMarker({
  timelineStartTimeStamp,
  timelineEndTimeStamp,
}: {
  timelineStartTimeStamp: number;
  timelineEndTimeStamp: number;
}) {
  const [now, setNow] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => {
      setNow(new Date());
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  if (
    now.getTime() < timelineStartTimeStamp ||
    now.getTime() > timelineEndTimeStamp
  ) {
    return null;
  }

  const nowPosition =
    ((now.getTime() - timelineStartTimeStamp) /
      (timelineEndTimeStamp - timelineStartTimeStamp)) *
    100;

  return (
    <>
      <div
        className="bg-primary text-white"
        style={{
          position: 'absolute',
          top: LABEL_HEIGHT_TOP + TIMELINE_HEIGHT,
          bottom: 0,
          left: `${nowPosition}%`,
          width: 2,
          zIndex: 2,
        }}
      />
      <div
        className="bg-primary text-white"
        style={{
          position: 'absolute',
          bottom: 0,
          left: `${nowPosition}%`,
          transform: 'translateX(-50%)',
          zIndex: 5,
          padding: '0px 5px',
          fontSize: 12,
        }}
      >
        Now: {formatDateToTime(now)}
      </div>
    </>
  );
}

function InvestigationTimelineTimeTicks({
  timelineStartTimeStamp,
  timelineEndTimeStamp,
}: {
  timelineStartTimeStamp: number;
  timelineEndTimeStamp: number;
}) {
  const timelineFormatter = useTimelineFormatter(
    timelineStartTimeStamp,
    timelineEndTimeStamp,
  );

  return (
    <>
      <div
        style={{
          position: 'absolute',
          bottom: LABEL_HEIGHT_BOTTOM,
          fontSize: 12,
          left: 0,
          zIndex: 3,
        }}
      >
        {formatDate(timelineStartTimeStamp, 'dd/MM HH:mm')}
      </div>
      <div
        style={{
          position: 'absolute',
          bottom: LABEL_HEIGHT_BOTTOM,
          fontSize: 12,
          right: 0,
          zIndex: 3,
        }}
      >
        {formatDate(timelineEndTimeStamp, 'dd/MM HH:mm')}
      </div>

      {timelineFormatter.times.map((time) => {
        const label = timelineFormatter.formatter(time);
        const left =
          ((time - timelineStartTimeStamp) /
            (timelineEndTimeStamp - timelineStartTimeStamp)) *
          100;
        return (
          <div key={time}>
            <div
              style={{
                position: 'absolute',
                top: LABEL_HEIGHT_TOP,
                bottom: 2 * LABEL_HEIGHT_BOTTOM - 5,
                left: `${left}%`,
                width: 1,
                borderLeft: '1px dashed black',
                zIndex: 2,
              }}
            />
            <div
              style={{
                position: 'absolute',
                bottom: LABEL_HEIGHT_BOTTOM,
                fontSize: 12,
                left: `${left}%`,
                transform: 'translateX(-50%)',
                zIndex: 3,
              }}
            >
              {label}
            </div>
          </div>
        );
      })}
    </>
  );
}

function DatasetBox({
  dataset,
  timelineStartTimeStamp,
  timelineEndTimeStamp,
}: {
  dataset: DatasetTimeLine;
  timelineStartTimeStamp: number;
  timelineEndTimeStamp: number;
}) {
  const { startDate, endDate } = dataset;
  const startTimeStamp = parseDate(startDate)?.getTime();
  const endTimeStamp = parseDate(endDate)?.getTime();

  const [hovering, setHovering] = useState(false);

  if (!startTimeStamp || !endTimeStamp) {
    return null;
  }

  const left =
    ((startTimeStamp - timelineStartTimeStamp) /
      (timelineEndTimeStamp - timelineStartTimeStamp)) *
    100;

  const width =
    ((endTimeStamp - startTimeStamp) /
      (timelineEndTimeStamp - timelineStartTimeStamp)) *
    100;

  const overlay = (
    <Popover>
      <Popover.Header as="h3">{dataset.datasetName}</Popover.Header>
      <Popover.Body>
        <MetadataTable
          parameters={[
            {
              caption: 'Start',
              value: formatDateToDayAndTime(startDate),
            },
            {
              caption: 'End',
              value: formatDateToDayAndTime(endDate),
            },
            {
              caption: 'Sample',
              value: dataset.sampleName,
            },
          ]}
        />
      </Popover.Body>
    </Popover>
  );

  return (
    <OverlayTrigger show={hovering} overlay={overlay}>
      <div
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: `${left}%`,
          width: `${width}%`,
          minWidth: 2,
          backgroundColor: hovering ? 'red' : 'blue',
          zIndex: 2,
        }}
        onMouseEnter={() => setHovering(true)}
        onMouseLeave={() => setHovering(false)}
      />
    </OverlayTrigger>
  );
}
