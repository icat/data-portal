import {
  usePath,
  SideNavElement,
  useUser,
  WithSideNav,
} from '@edata-portal/core';
import {
  SHIPMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faTruck, faVial } from '@fortawesome/free-solid-svg-icons';
import { NavTab } from 'components/navigation/NavTab';
import { Alert, Nav } from 'react-bootstrap';

import { Outlet } from 'react-router-dom';

export default function InvestigationPreparePage() {
  const investigationId = usePath('investigationId');

  const shipments = useGetEndpoint({
    endpoint: SHIPMENT_LIST_ENDPOINT,
    params: {
      investigationId,
    },
  });
  const user = useUser();

  const isAuthorized = user?.isAuthenticated && shipments !== undefined;

  const nav = (
    <Nav
      variant="pills"
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 5,
      }}
    >
      <NavTab
        to={`/investigation/${investigationId}/logistics`}
        icon={faTruck}
        label="Logistics"
        end
        testId="investigation-logistics"
      />
      <NavTab
        to={`/investigation/${investigationId}/logistics/changer`}
        icon={faVial}
        label="Sample changer"
        testId="investigation-changer"
      />
    </Nav>
  );

  if (!isAuthorized) {
    return (
      <Alert variant="warning">
        You are not allowed to prepare this experiment
      </Alert>
    );
  }

  return (
    <WithSideNav
      sideNav={<SideNavElement label="Prepare">{nav}</SideNavElement>}
    >
      <Outlet />
    </WithSideNav>
  );
}
