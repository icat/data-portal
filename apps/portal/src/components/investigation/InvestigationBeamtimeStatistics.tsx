import { formatDuration, parseDate } from '@edata-portal/core';
import {
  DATASET_TIMELINE_ENDPOINT,
  DatasetTimeLine,
  Investigation,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { InvestigationInfoBox } from 'components/investigation/InvestigationInfoBox';
import { useMemo } from 'react';
import { Col, Row } from 'react-bootstrap';

const EFFECTIVE_USED_TIME_THRESHOLD_MINUTES = 20;
const EFFECTIVE_USED_TIME_THRESHOLD_MILLIS =
  EFFECTIVE_USED_TIME_THRESHOLD_MINUTES * 60 * 1000; // 20 minutes

export function InvestigationBeamtimeStatistics({
  investigation,
}: {
  investigation: Investigation;
}) {
  const timeseries = useGetEndpoint({
    endpoint: DATASET_TIMELINE_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
    },
    default: [] as DatasetTimeLine[],
  });

  const parsedSeries = useMemo(() => {
    return timeseries
      .filter((d) => d.datasetType === 'acquisition')
      .map((d) => {
        const parsedStartDate = parseDate(d.startDate);
        const parsedEndDate = parseDate(d.endDate);
        if (!parsedStartDate) return undefined;
        return {
          startDate: parsedStartDate,
          endDate: parsedEndDate || parsedStartDate,
        };
      })
      .filter(
        (item): item is { startDate: Date; endDate: Date } =>
          item !== undefined,
      )
      .sort((a, b) => a.startDate.getTime() - b.startDate.getTime());
  }, [timeseries]);

  const bookedTime = useMemo(() => {
    const start = parseDate(investigation.startDate);
    const end = parseDate(investigation.endDate);
    if (!start || !end) return undefined;
    return end.getTime() - start.getTime();
  }, [investigation.startDate, investigation.endDate]);

  const usedTime = useMemo(() => {
    if (!parsedSeries.length) return undefined;
    const start = parsedSeries[0].startDate;
    const end = parsedSeries[parsedSeries.length - 1].endDate;
    if (!start || !end) return undefined;
    return end.getTime() - start.getTime();
  }, [parsedSeries]);

  const effectiveUsedTime = useMemo(() => {
    if (!usedTime) return undefined;
    let gapsSum = 0;
    let previousEndDate: Date | undefined = undefined;
    for (let i = 1; i < parsedSeries.length; i++) {
      const currentPoint = parsedSeries[i];
      if (previousEndDate) {
        const gap =
          currentPoint.startDate.getTime() - previousEndDate.getTime();
        if (gap > EFFECTIVE_USED_TIME_THRESHOLD_MILLIS) {
          gapsSum += gap;
        }
      }
      previousEndDate = currentPoint.endDate;
    }
    return usedTime - gapsSum;
  }, [parsedSeries, usedTime]);

  const totalAcquisitionTime = useMemo(() => {
    if (!parsedSeries.length) return undefined;
    return parsedSeries.reduce((acc, d) => {
      const start = parseDate(d.startDate);
      const end = parseDate(d.endDate);
      if (!start || !end) return acc;
      return acc + (end.getTime() - start.getTime());
    }, 0);
  }, [parsedSeries]);

  return (
    <Row className="g-2">
      <Col>
        <InvestigationInfoBox
          title="Allocated time"
          help="The time allocated for the experiment during the planning stage."
        >
          {formatDuration(bookedTime, 'text')}
        </InvestigationInfoBox>
      </Col>
      <Col>
        <InvestigationInfoBox
          title="Used time"
          help="The time between the first and last data point of the experiment."
        >
          {formatDuration(usedTime, 'text')}
        </InvestigationInfoBox>
      </Col>
      <Col>
        <InvestigationInfoBox
          title="Effective used time"
          help={`The time between the first and last data point of the experiment, excluding any gaps > ${EFFECTIVE_USED_TIME_THRESHOLD_MINUTES} minutes (beam dump).`}
        >
          {formatDuration(effectiveUsedTime, 'text')}
        </InvestigationInfoBox>
      </Col>
      <Col>
        <InvestigationInfoBox
          title="Total acquisition time"
          help="Sum of acquisition times of all datasets."
        >
          {formatDuration(totalAcquisitionTime, 'text')}
        </InvestigationInfoBox>
      </Col>
    </Row>
  );
}
