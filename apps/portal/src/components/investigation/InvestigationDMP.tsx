import {
  DMP_QUESTIONNAIRES_ENDPOINT,
  Investigation,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { Suspense, useMemo } from 'react';
import { Spinner } from 'react-bootstrap';

export function InvestigationDMP({
  investigation,
}: {
  investigation: Investigation;
}) {
  return (
    <Suspense
      fallback={
        <>
          <Spinner size="sm" />
        </>
      }
    >
      <LoadDMP investigation={investigation} />
    </Suspense>
  );
}

function LoadDMP({ investigation }: { investigation: Investigation }) {
  const uuids = useGetEndpoint({
    endpoint: DMP_QUESTIONNAIRES_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
      investigationName: investigation.name,
    },
  });

  const value = useMemo(() => {
    if (!uuids || uuids.length === 0) {
      return null;
    }
    return uuids[0].uuid;
  }, [uuids]);

  if (!value) {
    return <>No DMP</>;
  }

  return (
    <a
      target={'_blank'}
      href={`https://dmp.esrf.fr/projects/${value}`}
      rel="noreferrer"
    >
      Open
    </a>
  );
}
