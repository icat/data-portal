import {
  TanstackBootstrapTable,
  ORCID,
  isCollaborator,
} from '@edata-portal/core';
import {
  Investigation,
  InvestigationUser,
  INVESTIGATION_USERS_LIST_ENDPOINT,
  SimpleIcatUser,
  useMutateEndpoint,
  INVESTIGATION_USERS_ADD_ENDPOINT,
  INVESTIGATION_USERS_DELETE_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import {
  useReactTable,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  ColumnDef,
} from '@tanstack/react-table';
import { InvestigationInfoBox } from 'components/investigation/InvestigationInfoBox';
import { SelectUsers } from 'components/usermanagement/SelectUsers';
import { useMemo, useState } from 'react';
import { Alert, Button, Col, Container, Row } from 'react-bootstrap';
import { useIsInvestigationUsersManager } from '@edata-portal/core/src/hooks/investigationUserManager';

export function InvestigationParticipants({
  investigation,
}: {
  investigation: Investigation;
}) {
  const investigationParticipants = useGetEndpoint({
    endpoint: INVESTIGATION_USERS_LIST_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
    },
  });

  const isInvestigationUsersManager =
    useIsInvestigationUsersManager(investigation);

  const data = useMemo(() => {
    if (!investigationParticipants) return [];
    // group by fullName and join roles
    const grouped: any = {};
    for (const participant of investigationParticipants) {
      if (!grouped[participant.fullName]) {
        grouped[participant.fullName] = {
          ...participant,
          role: [participant.role],
        };
      } else {
        grouped[participant.fullName].role.push(participant.role);
      }
    }
    const withJoinedRoles = Object.values(grouped).map((p: any) => ({
      ...p,
      role: p.role.sort().join(', '),
    }));
    return withJoinedRoles.sort((a: any, b: any) =>
      a.fullName.localeCompare(b.fullName),
    );
  }, [investigationParticipants]);

  const columns: ColumnDef<InvestigationUser>[] = [
    {
      header: 'Name',
      accessorKey: 'fullName',
    },
    {
      header: 'Role',
      accessorKey: 'role',
    },
    {
      header: 'ORCID® iD',
      enableColumnFilter: false,
      accessorKey: 'orcidId',
      cell: (v) => (
        <ORCID orcid={v.getValue() as string} isCompactView={true} />
      ),
    },
    ...(isInvestigationUsersManager
      ? [
          {
            header: '',
            accessorFn: (row: any) => row,
            cell: (row: any) => (
              <RevokeParticipant
                users={data}
                investigation={investigation}
                participant={row.getValue()}
              />
            ),
            enableColumnFilter: false,
            id: 'revokebtn',
          },
        ]
      : []),
  ];
  const table = useReactTable<InvestigationUser>({
    data: data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getRowId: (d) => d.id.toString(),
    autoResetAll: false,
  });

  if (!investigationParticipants || investigationParticipants.length === 0) {
    return null;
  }

  return (
    <Container fluid>
      <InvestigationInfoBox title="Participants" icon={faUser}>
        <Col>
          <AddParticipant
            users={investigationParticipants}
            investigation={investigation}
          />
          <TanstackBootstrapTable dataLength={data.length} table={table} />
        </Col>
      </InvestigationInfoBox>
    </Container>
  );
}

function AddParticipant({
  users,
  investigation,
}: {
  users: InvestigationUser[];
  investigation: Investigation;
}) {
  const [selectedUsers, setSelectedUsers] = useState<SimpleIcatUser[]>([]);

  const isInvestigationUsersManager =
    useIsInvestigationUsersManager(investigation);

  const addUser = useMutateEndpoint({
    endpoint: INVESTIGATION_USERS_ADD_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
    },
  });

  if (!isInvestigationUsersManager) return null;

  return (
    <Row>
      <Col>
        <Alert variant="info">
          As principal investigator or local contact or beamline scientist, you
          can allow users to access to the data and read/write the electronic
          logbook. Only users with an account can be added. When a user is
          granted access to an experiment session then it will appear under the
          &apos;My data&apos; filter.
        </Alert>
        <Row>
          <strong>Grant access:</strong>
        </Row>
        <Row className="g-2">
          <Col xs={12} md={6} xl={4} xxl={3}>
            <SelectUsers
              selectedUsers={selectedUsers}
              onSelect={(users) => {
                if (!users) {
                  setSelectedUsers([]);
                } else {
                  setSelectedUsers(users);
                }
              }}
              investigationId={investigation.id}
              placeholder="Select users you want to add..."
            />
          </Col>
          <Col xs={'auto'}>
            <Button
              disabled={!selectedUsers?.length}
              onClick={() => {
                selectedUsers.forEach((user) => {
                  addUser
                    .mutateAsync({
                      body: user,
                    })
                    .then(() => {
                      setSelectedUsers((users) =>
                        users.filter((u) => u.name !== user.name),
                      );
                    });
                });
              }}
              test-id="add-participant-button"
            >
              Add
            </Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

function RevokeParticipant({
  users,
  investigation,
  participant,
}: {
  users: InvestigationUser[];
  investigation: Investigation;
  participant: InvestigationUser;
}) {
  const remove = useMutateEndpoint({
    endpoint: INVESTIGATION_USERS_DELETE_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
    },
  });

  const isInvestigationUsersManager =
    useIsInvestigationUsersManager(investigation);

  // check if participant is collaborator
  const isCollab = useMemo(
    () => isCollaborator(participant, users),
    [participant, users],
  );
  if (!isCollab || !isInvestigationUsersManager) return null;

  return (
    <Button
      variant="danger"
      onClick={() => remove.mutateAsync({ body: participant })}
      size="sm"
    >
      Revoke
    </Button>
  );
}
