import { getParameterByName, useConfig } from '@edata-portal/core';
import type { Sample } from '@edata-portal/icat-plus-api';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { SampleFormModal } from 'components/investigation/SampleFormModal';

function isSampleEditable(sample: Sample, nonEditableParameterName: string) {
  return !getParameterByName(nonEditableParameterName, sample.parameters);
}

export function EditSampleButton({
  sample,
  investigationId,
}: {
  sample: Sample;
  investigationId: number;
}) {
  const config = useConfig();
  const [show, setShow] = useState<boolean>(false);
  const nonEditableParameterName = config.ui.sample.nonEditableParameterName;

  function handleClose() {
    setShow(false);
  }

  if (isSampleEditable(sample, nonEditableParameterName)) {
    return (
      <>
        <OverlayTrigger
          placement="auto"
          overlay={<Tooltip id="tooltip"> Edit the sample description</Tooltip>}
        >
          <Button
            style={{ width: 70, textAlign: 'left', margin: 5 }}
            size="sm"
            variant="primary"
            onClick={() => {
              setShow(true);
            }}
          >
            <FontAwesomeIcon icon={faPencilAlt} style={{ marginRight: 5 }} />
            <span style={{ marginLeft: 2 }}>Edit</span>
          </Button>
        </OverlayTrigger>
        <SampleFormModal
          investigationId={investigationId}
          sample={sample}
          show={show}
          handleClose={handleClose}
        ></SampleFormModal>
      </>
    );
  }
  return null;
}
