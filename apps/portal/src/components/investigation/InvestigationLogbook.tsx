import { usePath } from '@edata-portal/core';
import { useRegisterInvestigationRecentlyVisited } from 'components/navigation/recentlyvisited/RecentlyVisitedContext';
import { RemoteComponent } from 'components/RemoteComponent';

export default function InstrumentLogbook() {
  const investigationId = usePath('investigationId');

  useRegisterInvestigationRecentlyVisited(investigationId, 'Logbook');

  return (
    <RemoteComponent
      remote={{
        name: 'logbook',
        component: './Logbook',
      }}
      props={{
        investigationId,
      }}
    />
  );
}
