import {
  DATASET_COUNT,
  DatasetStatistics,
  FilesStatistics,
  getParameterByName,
  immutableArray,
  TanstackBootstrapTable,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  Investigation,
  SAMPLE_LIST_ENDPOINT,
  Sample,
} from '@edata-portal/icat-plus-api';
import {
  faSquareArrowUpRight,
  faThList,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { InvestigationInfoBox } from 'components/investigation/InvestigationInfoBox';
import { EditSampleButton } from 'components/investigation/EditSampleButton';
import { useMemo } from 'react';
import {
  Button,
  OverlayTrigger,
  Tooltip,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import { useConfig } from '@edata-portal/core';
import { useInvestigationAccess } from '@edata-portal/core/src/hooks/investigationAccess';

function getSamplePageURL(sample: Sample, samplePageTemplateUrl: string) {
  if (!sample.parameters) {
    return undefined;
  }

  const parameter = getParameterByName('Id', sample.parameters);
  if (!parameter) {
    return undefined;
  }

  return `${samplePageTemplateUrl}${parameter.value}`;
}

function userPortalInformationFormatter(
  sample: Sample,
  samplePageTemplateUrl: string,
) {
  const sampleURL = getSamplePageURL(sample, samplePageTemplateUrl);
  if (sampleURL) {
    return (
      <>
        <OverlayTrigger
          placement="bottom"
          overlay={
            <Tooltip id="sampleSheet-tooltip">View Sample Sheet Form</Tooltip>
          }
        >
          <Button
            size="sm"
            style={{ border: 'none' }}
            onClick={(e) => {
              e.stopPropagation();
            }}
            variant="outline-info"
            href={sampleURL}
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon icon={faSquareArrowUpRight} />
          </Button>
        </OverlayTrigger>
        <span style={{ marginLeft: 8 }}>
          {sample.parameters.find((p: any) => p.name === 'SafetyLevel')?.value}
        </span>
      </>
    );
  }
  return null;
}

function getSampleParameter(sample: Sample) {
  let parameters = {};
  sample.parameters.forEach((p) => {
    parameters = { ...parameters, [p.name]: p.value };
  });
  return parameters;
}

function statisticsFormatter(sample: Sample, investigationId: number) {
  const sampleNbDatasets = getParameterByName(DATASET_COUNT, sample.parameters);
  const hasDatasets = sampleNbDatasets && Number(sampleNbDatasets.value) > 0;
  return (
    <Container fluid style={{ margin: 0 }}>
      <Row>
        <Col sm={12} md={9}>
          <DatasetStatistics
            label="Datasets"
            parameterCaption={DATASET_COUNT}
            key={`stat-${sample.id}`}
            parameters={getSampleParameter(sample)}
          />
        </Col>
        {hasDatasets && (
          <Col sm={12} md={3}>
            <OverlayTrigger
              placement="auto"
              overlay={
                <Tooltip id="tooltip">
                  View datasets linked to this sample
                </Tooltip>
              }
            >
              <Button
                size="sm"
                style={{ border: 'none' }}
                onClick={(e) => {
                  e.stopPropagation();
                }}
                variant="outline-info"
                href={`/investigation/${investigationId}/datasets?sampleId=${sample.id}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faSquareArrowUpRight} />
              </Button>
            </OverlayTrigger>
          </Col>
        )}
      </Row>
    </Container>
  );
}

export function InvestigationSamples({
  investigation,
}: {
  investigation: Investigation;
}) {
  const samples = useGetEndpoint({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: {
      investigationId: investigation.id,
      sortBy: 'NAME',
      sortOrder: 1,
    },
  });

  const data = useMemo(() => {
    return immutableArray(samples)
      .sort((a: any, b: any) => a.name.localeCompare(b.name))
      .toArray();
  }, [samples]);

  const config = useConfig();

  const isParticipant = useInvestigationAccess({
    investigation: investigation,
    roles: {
      participant: true,
      instrumentScientist: true,
      administrator: true,
    },
  });

  const table = useReactTable<Sample>({
    data: data,
    columns: [
      {
        header: 'Name',
        accessorKey: 'name',
      },
      {
        header: 'Description',
        enableColumnFilter: false,
        accessorFn: (d) =>
          d.parameters.find((p) => p.name === 'Sample_description')?.value,
      },
      ...(isParticipant
        ? [
            {
              accessorFn: (row: any) => row,
              header: 'Safety Mode',
              enableColumnFilter: false,
              cell: (info: any) =>
                userPortalInformationFormatter(
                  info.getValue() as Sample,
                  config.ui.sample.pageTemplateURL,
                ),
            },
          ]
        : []),
      {
        accessorFn: (row) => row,
        header: 'Datasets',
        enableColumnFilter: false,
        cell: (info) =>
          statisticsFormatter(info.getValue() as Sample, investigation.id),
      },
      {
        accessorFn: (row) => row,
        header: 'Files',
        enableColumnFilter: false,
        cell: (info) => (
          <FilesStatistics
            parameters={getSampleParameter(info.getValue() as Sample)}
          />
        ),
      },
      ...(isParticipant
        ? [
            {
              accessorFn: (row: any) => row,
              header: 'Edit',
              enableColumnFilter: false,
              cell: (info: any) => (
                <EditSampleButton
                  sample={info.getValue() as Sample}
                  investigationId={investigation.id}
                ></EditSampleButton>
              ),
            },
          ]
        : []),
    ],
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getRowId: (d) => d.id.toString(),
    autoResetAll: false,
  });

  if (!samples || samples.length === 0) {
    return null;
  }

  return (
    <Container fluid>
      <InvestigationInfoBox title={'Samples'} icon={faThList}>
        <TanstackBootstrapTable
          dataLength={data.length}
          table={table}
          bordered={true}
        />
      </InvestigationInfoBox>
    </Container>
  );
}
