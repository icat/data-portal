import type { Investigation } from '@edata-portal/icat-plus-api';
import { Col, Row } from 'react-bootstrap';

export function InvestigationTitle({
  investigation,
}: {
  investigation: Investigation;
}) {
  return (
    <Row>
      <Col>
        <h1
          style={{
            fontSize: '2em',
            fontWeight: 'bold',
          }}
        >
          {investigation.title || 'Untitled experiment session'}
        </h1>
      </Col>
    </Row>
  );
}
