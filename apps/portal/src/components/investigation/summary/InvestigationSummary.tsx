import { Loading, usePath } from '@edata-portal/core';
import {
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { InvestigationSamples } from 'components/investigation/InvestigationSamples';
import { InvestigationParticipants } from 'components/investigation/InvestigationParticipants';
import { Suspense, useMemo } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { InvestigationTitle } from 'components/investigation/summary/InvestigationTitle';
import { InvestigationDetails } from 'components/investigation/summary/InvestigationDetails';

export default function InvestigationSummary() {
  const investigationId = usePath('investigationId');

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
  });

  const investigation = useMemo(
    () =>
      investigations && investigations.length ? investigations[0] : undefined,
    [investigations],
  );

  if (!investigation) {
    return null;
  }

  return (
    <Container fluid>
      <Col>
        <InvestigationTitle investigation={investigation} />
        <Suspense fallback={<Loading />}>
          <InvestigationDetails investigation={investigation} />
        </Suspense>

        <Row className="mt-3">
          <Suspense fallback={<Loading />}>
            <InvestigationParticipants investigation={investigation} />
          </Suspense>
        </Row>
        <Row className="mt-3">
          <Suspense fallback={<Loading />}>
            <InvestigationSamples investigation={investigation} />
          </Suspense>
        </Row>
      </Col>
    </Container>
  );
}
