import {
  formatDateToDay,
  formatDateToDayAndShortTime,
  Instrument,
  useConfig,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { Col, Row } from 'react-bootstrap';
import { InvestigationDMP } from 'components/investigation/InvestigationDMP';
import { InvestigationInfoBox } from 'components/investigation/InvestigationInfoBox';
import { DOIBadge } from '@edata-portal/doi';

export function InvestigationDetails({
  investigation,
}: {
  investigation: Investigation;
}) {
  const config = useConfig();
  const description = [
    {
      title: 'Proposal',
      value: investigation.name,
      textAlign: 'center',
    },
    {
      title: 'Beamline',
      value: <Instrument instrumentName={investigation.instrument?.name} />,
      textAlign: 'center',
    },
    {
      title: 'Start',
      value: formatDateToDayAndShortTime(investigation.startDate),
      textAlign: 'center',
    },
    {
      title: 'End',
      value: formatDateToDayAndShortTime(investigation.endDate),
      textAlign: 'center',
    },
    {
      title: 'Release date',
      value: formatDateToDay(investigation.releaseDate),
      textAlign: 'center',
    },
    {
      title: 'DOI',
      value: investigation.doi ? (
        <DOIBadge doi={investigation.doi} />
      ) : undefined,
      textAlign: 'center',
    },
    ...(config.ui.features.dmp
      ? [
          {
            title: 'Data Management Plan',
            value: <InvestigationDMP investigation={investigation} />,
            textAlign: 'center',
          },
        ]
      : []),
  ];
  return (
    <>
      <Row className="g-3 mb-3" test-id="investigation-details">
        {description.map(
          (d) =>
            d.value && (
              <Col key={d.title} xs={'auto'}>
                <InvestigationInfoBox title={d.title} textAlign={d.textAlign}>
                  {d.value}
                </InvestigationInfoBox>
              </Col>
            ),
        )}
      </Row>
      {investigation.summary && (
        <Row test-id="investigation-abstract">
          <Col>
            <InvestigationInfoBox title={'Abstract'}>
              {investigation.summary}
            </InvestigationInfoBox>
          </Col>
        </Row>
      )}
    </>
  );
}
