import {
  Notification,
  NotifyContext,
  formatDistanceToNow,
} from '@edata-portal/core';
import {
  faAngleDown,
  faAngleUp,
  faClose,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { Button, Toast } from 'react-bootstrap';
import { flushSync } from 'react-dom';

type NofificationWithTime = Notification & { time: Date };

type NotificationsContextType = {
  notifications: NofificationWithTime[];
  clear: (notification: NofificationWithTime) => void;
  clearAll: () => void;
  scrollRef: React.RefObject<HTMLDivElement>;
  collapsed: boolean;
  setCollapsed: (collapsed: boolean) => void;
};

const NotificationsContext = createContext<NotificationsContextType>({
  notifications: [],
  clear: () => {},
  clearAll: () => {},
  scrollRef: { current: null },
  collapsed: false,
  setCollapsed: () => {},
});

export function NotifyProvider({ children }: { children: React.ReactNode }) {
  const [notifications, setNotifications] = useState<NofificationWithTime[]>(
    [],
  );

  const scrollRef = useRef<HTMLDivElement>(null);
  const [collapsed, setCollapsed] = useState<boolean>(false);

  const notify = useCallback((notification: Notification) => {
    const newNotification = {
      ...notification,
      time: new Date(),
    };
    flushSync(() => {
      setNotifications((notifications) => [...notifications, newNotification]);
      setCollapsed(false);
    });

    scrollRef.current?.scrollTo({
      top: scrollRef.current.scrollHeight,
      behavior: 'instant',
    });
  }, []);

  const closeNotification = useCallback((notification: Notification) => {
    setNotifications((notifications) =>
      notifications.filter((n) => n !== notification),
    );
  }, []);

  const notifyValue = useMemo(() => ({ notify }), [notify]);

  const notificationsValue = useMemo(() => {
    return {
      notifications,
      clear: closeNotification,
      clearAll: () => {
        setNotifications([]);
      },
      scrollRef,
      collapsed,
      setCollapsed,
    };
  }, [notifications, closeNotification, collapsed]);

  return (
    <NotifyContext.Provider value={notifyValue}>
      <NotificationsContext.Provider value={notificationsValue}>
        {children}
      </NotificationsContext.Provider>
    </NotifyContext.Provider>
  );
}

export function NotifyRenderer() {
  const notificationsContext = useContext(NotificationsContext);
  return (
    <NotificationPanel
      notifications={
        notificationsContext.notifications as NofificationWithTime[]
      }
      onClear={notificationsContext.clearAll}
      onClose={notificationsContext.clear}
      scrollRef={notificationsContext.scrollRef}
      collapsed={notificationsContext.collapsed}
      setCollapsed={notificationsContext.setCollapsed}
    />
  );
}

function NotificationPanel({
  notifications,
  onClear,
  onClose,
  scrollRef,
  collapsed,
  setCollapsed,
}: {
  notifications: NofificationWithTime[];
  onClear: () => void;
  onClose: (notification: NofificationWithTime) => void;
  scrollRef: React.RefObject<HTMLDivElement>;
  collapsed: boolean;
  setCollapsed: (collapsed: boolean) => void;
}) {
  if (!notifications.length) return null;

  const content = (
    <div
      className={`d-${
        collapsed ? 'none' : 'flex'
      } flex-column gap-2 p-2 border-top`}
      style={{
        overflowY: 'auto',
        overflowX: 'hidden',
      }}
      ref={scrollRef}
    >
      {notifications.map((notification) => {
        return (
          <NotificationItem
            key={notification.time.getTime() + (notification.type || 'none')}
            notification={notification}
            onClose={() => onClose(notification)}
          />
        );
      })}
    </div>
  );

  return (
    <div
      className={`d-flex flex-column bg-white shadow border-black overflow-hidden `}
      style={{
        position: 'fixed',
        bottom: 0,
        right: '2rem',
        zIndex: Number.MAX_SAFE_INTEGER,
        maxHeight: '50vh',
        borderTop: '1px solid',
        borderLeft: '1px solid',
        borderRight: '1px solid',
        borderRadius: '10px 10px 0 0 ',
      }}
    >
      <div className="d-flex flex-row align-items-center px-2 py-1">
        <strong>Notifications</strong>

        <Button
          variant="link"
          className="px-1 py-0 ms-auto"
          onClick={() => setCollapsed(!collapsed)}
        >
          <FontAwesomeIcon
            icon={collapsed ? faAngleUp : faAngleDown}
            size={collapsed ? undefined : 'lg'}
          />
        </Button>

        <Button variant="link" className="px-1 py-0 " onClick={onClear}>
          <FontAwesomeIcon icon={faClose} size={collapsed ? undefined : 'lg'} />
        </Button>
      </div>
      {content}
    </div>
  );
}

function NotificationItem({
  notification,
  onClose,
}: {
  notification: NofificationWithTime;
  onClose: () => void;
}) {
  const [timeFormatted, setTimeFormatted] = useState<string>(
    formatDistanceToNow(notification.time),
  );

  useEffect(() => {
    const interval = setInterval(() => {
      setTimeFormatted(formatDistanceToNow(notification.time));
    }, 60000);
    return () => clearInterval(interval);
  }, [notification.time]);

  return (
    <Toast
      style={{
        fontSize: '0.8rem',
        flexShrink: 0,
        boxShadow: 'none',
      }}
      onClose={onClose}
      className="overflow-hidden"
    >
      <Toast.Header
        style={{
          padding: '4px 10px',
        }}
        className={`text-white bg-${notification.type || 'info'}`}
      >
        <strong className="me-auto">
          {notification.title
            ? notification.title
            : getDisplayForType(notification.type || 'info')}
        </strong>
        <small>
          {(timeFormatted + ' ago').replace(
            'less than a minute ago',
            'just now',
          )}
        </small>
      </Toast.Header>
      <Toast.Body
        style={{
          padding: '2px 10px',
        }}
        className="bg-white"
      >
        {notification.message}
      </Toast.Body>
    </Toast>
  );
}

function getDisplayForType(type: string) {
  switch (type) {
    case 'danger':
      return 'Error';
    case 'success':
      return 'Success';
    case 'warning':
      return 'Warning';
    case 'info':
    default:
      return 'Info';
  }
}
