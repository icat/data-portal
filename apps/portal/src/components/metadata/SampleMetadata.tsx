import { useParam } from '@edata-portal/core';
import {
  DATASET_TYPE_ACQUISITION,
  Dataset,
  DatasetParams,
  DATASET_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import SampleMetadataNetworkPlot from 'components/metadata/flow/SampleMetadataNetworkPlot';
import { useMemo, useState } from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import SampleMetadataTable from 'components/metadata/SampleMetadataTable';

export default function SampleMetadata() {
  const [sampleId] = useParam<string>('sampleId', '');
  const [layout] = useParam<string>('layout', '');

  const [params] = useState<DatasetParams>({
    sampleId: sampleId?.trim(),
  });

  const anyParam = useMemo(() => {
    return Object.values(params).some(
      (v) => v && (typeof v !== 'string' || v?.trim().length),
    );
  }, [params]);

  const datasetList = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: { ...params, datasetType: DATASET_TYPE_ACQUISITION, nested: true },
    default: [] as Dataset[],
    skipFetch: !anyParam,
  });

  return (
    <Tabs
      defaultActiveKey={layout === '' ? 'table' : 'visual'}
      id="sample-metadata-tab"
    >
      <Tab eventKey="table" title="Table">
        <div
          style={{
            fontSize: '0.7rem',
            overflow: 'auto',
          }}
          className="bg-light p-2 border rounded border-secondary"
        >
          <SampleMetadataTable datasetList={datasetList}></SampleMetadataTable>
        </div>
      </Tab>
      <Tab
        eventKey="visual"
        title="Visual"
        style={{
          height: '80vh',
          width: '100%',
        }}
      >
        <SampleMetadataNetworkPlot datasetList={datasetList} />
      </Tab>
    </Tabs>
  );
}
