import {
  getDatasetName,
  getDatasetParamValue,
  TanstackBootstrapTable,
  useParam,
  flattenOutputs,
  isProcessed,
  isAcquisition,
  getDatasetURL,
} from '@edata-portal/core';
import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  ColumnDef,
  getCoreRowModel,
  getFilteredRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { useCallback, useMemo } from 'react';
import { Container, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function SampleMetadataTable({
  datasetList,
}: {
  datasetList: Dataset[];
}) {
  const data = useMemo(() => {
    return flattenOutputs(datasetList);
  }, [datasetList]);

  const [filterParam, setFilterParam] = useParam<string>('filterParam', '');

  const columns: ColumnDef<Dataset, string>[] = useMemo(() => {
    if (!data.length) return [];
    const metadataParameters = data
      .flatMap((dataset) => dataset.parameters)
      .map((parameter) => parameter.name)
      .filter((value, index, self) => self.indexOf(value) === index)
      .sort();
    const res: ColumnDef<Dataset, any>[] = [
      {
        id: 'name',
        header: 'Name',
        footer: 'Name',
        accessorFn: (row: Dataset) => row,
        cell: (info: any) => (
          <Link
            to={getDatasetURL(
              (info.getValue() as Dataset).id,
              (info.getValue() as Dataset).investigation.id,
            )}
          >
            {getDatasetName(info.getValue() as Dataset)}
          </Link>
        ),
      },
      {
        id: 'sampleName',
        header: 'Sample',
        footer: 'Sample',
        accessorFn: (row: Dataset) => row,
        cell: (info: any) => <>{(info.getValue() as Dataset).sampleName}</>,
      },
      {
        id: 'sampleId',
        header: 'sampleId',
        footer: 'sampleId',
        accessorFn: (row: Dataset) => row,
        cell: (info: any) => <>{(info.getValue() as Dataset).sampleId} </>,
      },
      ...metadataParameters
        .filter(
          (parameter) =>
            !filterParam?.trim().length ||
            filterParam
              .toLocaleLowerCase()
              .split(',')
              .some((filter) => {
                const trimmedFilter = filter.trim();
                return (
                  trimmedFilter.length &&
                  parameter.toLocaleLowerCase().includes(trimmedFilter)
                );
              }),
        )
        .map((parameter) => ({
          id: parameter,
          accessorFn: (row: Dataset) => getDatasetParamValue(row, parameter),
          cell: (info: any) => {
            return <ParamValue value={info.getValue()} />;
          },
          header: parameter,
          footer: parameter,
        })),
    ];
    return res;
  }, [data, filterParam]);

  const table = useReactTable({
    columns,
    data,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
  });

  const getTrStyle = useCallback((row: Dataset) => {
    if (isAcquisition(row)) {
      return {
        backgroundColor: 'rgba(0, 0, 255, 0.05)',
      };
    } else if (isProcessed(row)) {
      return {
        backgroundColor: 'rgba(0, 255, 0, 0.05)',
      };
    }
    return {};
  }, []);

  return (
    <>
      <Form.Control
        type="text"
        value={filterParam}
        onChange={(e) => {
          setFilterParam(e.target.value);
        }}
        placeholder="Filter comma separated params. Example: sample_name, scantype"
        size="sm"
      />
      <Container fluid>
        <TanstackBootstrapTable
          pagination={false}
          table={table}
          bordered
          responsive={false}
          getTrStyle={getTrStyle}
          dataLength={data.length}
        />
      </Container>
    </>
  );
}

function ParamValue({ value }: { value?: string }) {
  if (!value) return null;

  return (
    <div
      style={{
        whiteSpace: 'nowrap',
        maxWidth: '200px',
        overflowX: 'auto',
      }}
    >
      {value}
    </div>
  );
}
