import type { Dataset } from '@edata-portal/icat-plus-api';

import { useMemo } from 'react';
import { SampleNode } from 'components/metadata/flow/nodes/SampleNode';

import { DatasetNode } from 'components/metadata/flow/nodes/DatasetNode';
import {
  FlowChartLazy,
  LayoutDirection,
  flattenOutputs,
  isAcquisition,
} from '@edata-portal/core';

/** This components returns a ReactFlow with the samples and dataset connected visually */
export default function SampleMetadataNetworkPlot({
  datasetList,
  currentDataset,
  defaultLayout,
}: {
  datasetList: Dataset[];
  currentDataset?: Dataset;
  defaultLayout?: LayoutDirection;
}) {
  const allDatasets = useMemo(
    () =>
      flattenOutputs(datasetList).filter(
        (d, i, o) => o.findIndex((t) => t.id === d.id) === i,
      ),

    [datasetList],
  );

  const samples = useMemo(
    () =>
      allDatasets
        .map((dataset) => dataset.sampleName)
        .filter((s) => s !== undefined && s !== null)
        .filter(
          (sampleName, index, self) => self.indexOf(sampleName) === index,
        ),
    [allDatasets],
  );

  const nodes = useMemo(
    () => [
      ...allDatasets.map((dataset) => ({
        id: String(dataset.id),
        element: (
          <DatasetNode dataset={dataset} currentDataset={currentDataset} />
        ),
      })),
      ...samples.map((sampleName) => ({
        id: sampleName,
        element: <SampleNode sampleName={sampleName} />,
      })),
    ],
    [allDatasets, currentDataset, samples],
  );

  const edges = useMemo(() => {
    return [
      ...allDatasets.flatMap((dataset) => {
        if (!dataset.outputDatasets) return [];
        return dataset.outputDatasets
          .map((output) => ({
            id: `${dataset.id}-${output.id}`,
            source: String(dataset.id),
            target: String(output.id),
          }))
          .filter(
            (edge, index, self) =>
              self.findIndex((t) => t.id === edge.id) === index,
          );
      }),
      ...allDatasets.filter(isAcquisition).flatMap((dataset) => {
        if (!dataset.sampleName) return [];
        return [
          {
            id: `${dataset.id}-${dataset.sampleName}`,
            source: dataset.sampleName,
            target: String(dataset.id),
          },
        ];
      }),
    ];
  }, [allDatasets]);

  return (
    <FlowChartLazy
      nodes={nodes}
      edges={edges}
      defaultLayoutDirection={defaultLayout}
      centerOnNode={currentDataset ? String(currentDataset.id) : undefined}
    />
  );
}
