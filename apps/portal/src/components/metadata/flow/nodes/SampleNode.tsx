export function SampleNode({ sampleName }: { sampleName: string }) {
  return (
    <div
      className="p-1 border border-2 border-primary rounded bg-sample d-flex"
      style={{
        overflow: 'hidden',
      }}
    >
      <strong
        className="text-white d-flex flex-column align-items-center justify-content-center w-100 h-100"
        style={{
          fontSize: '1.2em',
          height: '100%',
          width: '100%',
          overflowX: 'hidden',
          overflowY: 'auto',
          wordBreak: 'break-word',
        }}
      >
        {sampleName}
      </strong>
    </div>
  );
}
