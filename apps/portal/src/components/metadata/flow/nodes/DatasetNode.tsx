import {
  DatasetTypeIcon,
  getDatasetName,
  getDatasetURL,
  isAcquisition,
} from '@edata-portal/core';
import { Dataset } from '@edata-portal/icat-plus-api';
import { Link } from 'react-router-dom';

export function DatasetNode({
  dataset,
  currentDataset,
}: {
  dataset: Dataset;
  currentDataset?: Dataset;
}) {
  if (!dataset) return null;

  const isCurrentDataset = currentDataset && currentDataset.id === dataset.id;

  const bg = isAcquisition(dataset) ? 'bg-dataset-raw' : 'bg-dataset-processed';

  const border = isCurrentDataset ? 'border-info' : 'border-primary';

  return (
    <Link
      to={getDatasetURL(dataset.id, dataset.investigation.id)}
      className={`text-black p-1 border border-2 ${border} rounded ${bg} d-flex`}
      style={{
        position: 'relative',
        fontSize: 10,
      }}
    >
      {isCurrentDataset && (
        <div
          className="bg-info text-white px-3 text-nowrap"
          style={{
            position: 'absolute',
            top: 0,
            left: '50%',
            transform: 'translate(-50%,-100%)',
            borderRadius: '1em 1em 0 0',
          }}
        >
          <strong>This dataset</strong>
        </div>
      )}
      <div
        className="d-flex flex-row align-items-center justify-content-center w-100"
        style={{
          height: '100%',
          width: '100%',
          overflowX: 'hidden',
          overflowY: 'auto',
          wordBreak: 'break-word',
        }}
      >
        <div
          className={`${isCurrentDataset ? 'bg-info' : 'bg-white'} d-flex p-2 rounded align-items-center justify-content-center me-2`}
        >
          <DatasetTypeIcon dataset={dataset} />
        </div>
        <strong className="d-flex flex-row align-items-center justify-content-center w-100">
          {getDatasetName(dataset)}
        </strong>
      </div>
    </Link>
  );
}
