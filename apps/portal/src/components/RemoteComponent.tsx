import { Loading } from '@edata-portal/core';
import { faBug } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ContactButton } from 'components/help/FeedbackButton';
import { Component, ErrorInfo, ReactNode, Suspense } from 'react';
import { Alert, OverlayTrigger, Popover, Stack } from 'react-bootstrap';
import {
  getRemoteComponent,
  RemoteDefinition,
  RemoteLoadingError,
} from 'remotes';

export function RemoteComponent({
  remote,
  props,
}: {
  remote: RemoteDefinition;
  props?: any;
}) {
  return (
    <Suspense fallback={<Loading />}>
      <RemoteErrorBoundary>
        <LoadRemoteComponent remote={remote} props={props} />
      </RemoteErrorBoundary>
    </Suspense>
  );
}

function LoadRemoteComponent({
  remote,
  props,
}: {
  remote: RemoteDefinition;
  props?: any;
}) {
  const RemoteComponent = getRemoteComponent(remote);

  return <RemoteComponent {...props} />;
}
interface Props {
  children?: ReactNode;
}

interface State {
  hasError: boolean;
  error?: Error;
}

class RemoteErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
  };

  public static getDerivedStateFromError(e: Error): State {
    return { hasError: true, error: e };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error('Uncaught error:', error, errorInfo);
  }

  public render() {
    if (this.state.hasError && this.state.error) {
      return (
        <Alert variant="danger">
          <Stack direction="vertical" gap={1}>
            <strong>
              {this.state.error instanceof RemoteLoadingError
                ? this.state.error.message
                : 'An error occurred.'}
              <ErrorDebugInfo error={this.state.error} />
            </strong>

            <small>
              {this.state.error instanceof RemoteLoadingError
                ? 'This might be due to a network error. Please try again later and report this error if it persists.'
                : 'Something went wrong. Please refresh the page and report this error if it persists.'}
            </small>

            <small>
              <ContactButton
                title="Report this error"
                addInfo={`\n\nError: ${this.state.error?.message}\n\nStack trace: ${this.state.error?.stack}`}
              />
            </small>
          </Stack>
        </Alert>
      );
    }

    return this.props.children;
  }
}

function ErrorDebugInfo({ error }: { error: Error }) {
  return (
    <OverlayTrigger
      placement="auto"
      trigger={['click']}
      overlay={
        <Popover
          style={{
            maxWidth: '80vw',
          }}
        >
          <Popover.Header>Stack trace</Popover.Header>
          <Popover.Body>
            <pre
              style={{
                overflow: 'auto',
                whiteSpace: 'pre-wrap',
              }}
            >
              {'remote' in error ? (
                <>
                  <strong>Remote:</strong>
                  <br />
                  {error.remote as string}
                  <hr />
                </>
              ) : null}
              <strong>Error:</strong>
              <br />
              {error.stack}
              {error.cause && 'stack' in (error.cause as any) ? (
                <>
                  <hr />
                  <strong>Cause:</strong>
                  <br />
                  {(error.cause as any).stack}
                </>
              ) : null}
            </pre>
          </Popover.Body>
        </Popover>
      }
    >
      <FontAwesomeIcon
        icon={faBug}
        className="ms-2"
        style={{
          cursor: 'pointer',
        }}
      />
    </OverlayTrigger>
  );
}
