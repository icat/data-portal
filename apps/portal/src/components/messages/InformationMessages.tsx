import { useGetEndpoint } from '@edata-portal/icat-plus-api';
import {
  GET_MESSAGES_ENDPOINT,
  INFORMATION_MESSAGE,
} from '@edata-portal/icat-plus-api';
import { Alert } from 'react-bootstrap';

export default function InformationMessage() {
  const messages = useGetEndpoint({
    endpoint: GET_MESSAGES_ENDPOINT,
    params: { type: INFORMATION_MESSAGE },
  });
  if (!messages || messages.length === 0 || messages[0].message === '') {
    return null;
  }
  return (
    <Alert style={{ margin: '0px', borderRadius: '0' }} variant="info">
      {messages[0].message}
    </Alert>
  );
}
