import {
  DatasetFilesTabName,
  Loading,
  MetadataTab,
  DatasetFiles,
  DoiInfo,
  useConfig,
  getInstrummentTab,
} from '@edata-portal/core';
import { Col, Row, Tabs, Tab } from 'react-bootstrap';
import { Suspense } from 'react';
import type { Dataset, Parameter } from '@edata-portal/icat-plus-api';
import { GenericDatasetSummary } from '@edata-portal/core/src/components/dataset/generic/GenericDatasetSummary';

export function DatasetDetail({ dataset }: { dataset: Dataset }) {
  const instrumentTab = getInstrummentTab(dataset);

  const config = useConfig();

  return (
    <Col>
      <DoiInfo dataset={dataset} />
      <Row>
        <Col>
          <GenericDatasetSummary dataset={dataset} />
        </Col>
      </Row>
      <Row style={{ padding: 0, marginTop: 12 }}>
        <Col>
          <Tabs id="tabs" fill>
            {config.techniques.map((technique, i) => {
              const filter = (parameter: Parameter) =>
                parameter.name.startsWith(technique.shortname);
              const hasParams = dataset.parameters.find(filter) !== undefined;
              if (hasParams) {
                return (
                  <Tab
                    key={technique.shortname}
                    eventKey={i}
                    title={technique.name}
                  >
                    <MetadataTab
                      entity={dataset}
                      filter={(p) => p.name.startsWith(technique.shortname)}
                    />
                  </Tab>
                );
              }
              return null;
            })}
            {instrumentTab && (
              <Tab
                key={instrumentTab.key}
                eventKey={instrumentTab.key}
                title={instrumentTab.title}
              >
                {instrumentTab.content}
              </Tab>
            )}

            <Tab eventKey="metadata" title="Metadata list">
              <MetadataTab entity={dataset} searchable />
            </Tab>
            <Tab
              eventKey="files"
              title={<DatasetFilesTabName dataset={dataset} />}
            >
              <Suspense fallback={<Loading />}>
                <DatasetFiles
                  dataset={dataset}
                  showPath
                  showDatasetDownloadButton={true}
                />
              </Suspense>
            </Tab>
          </Tabs>
        </Col>
      </Row>
    </Col>
  );
}
