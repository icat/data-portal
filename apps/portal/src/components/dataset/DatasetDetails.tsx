import {
  usePath,
  NoData,
  LazyWrapper,
  getDatasetName,
  DatasetActionsButton,
  DatasetTypeIcon,
} from '@edata-portal/core';
import {
  DATASET_TYPE_ACQUISITION,
  useGetEndpoint,
  DATASET_LIST_ENDPOINT,
  Dataset,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { DatasetDetail } from 'components/dataset/DatasetDetail';

import SampleMetadataNetworkPlot from 'components/metadata/flow/SampleMetadataNetworkPlot';

export default function DatasetDetails() {
  const datasetId = usePath('datasetId');
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: datasetId,
      nested: true,
    },
    default: [] as Dataset[],
  });

  const sampleDatasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetType: DATASET_TYPE_ACQUISITION,
      nested: true,
      sampleId: datasets[0]?.sampleId,
    },
    default: [] as Dataset[],
    skipFetch: !datasets[0]?.sampleId,
  });

  const dataset: Dataset | undefined = useMemo(
    () => (datasets && datasets.length ? datasets[0] : undefined),
    [datasets],
  );

  if (!dataset) {
    return <NoData />;
  }

  return (
    <Container fluid>
      <Col>
        <Row>
          <h1
            style={{
              fontSize: '1.5em',
            }}
          >
            <DatasetName dataset={dataset} />
          </h1>
        </Row>
        <Row>
          <div
            className="mb-2"
            style={{
              height: '30vh',
              width: '100%',
            }}
          >
            <SampleMetadataNetworkPlot
              datasetList={sampleDatasets}
              currentDataset={dataset}
              defaultLayout={'TB'}
            />
          </div>
        </Row>

        <Row>
          <LazyWrapper>
            <DatasetDetail dataset={dataset} />
          </LazyWrapper>
        </Row>
      </Col>
    </Container>
  );
}

function DatasetName({ dataset }: { dataset: Dataset }) {
  const datasetName = getDatasetName(dataset);
  return (
    <div className="d-flex flex-row gap-2">
      <DatasetActionsButton dataset={dataset} />
      <DatasetTypeIcon dataset={dataset} />
      <strong className="me-2">{datasetName}</strong>
    </div>
  );
}
