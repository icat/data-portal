import {
  GenericDatasetTable,
  getDatasetTableColumns,
} from '@edata-portal/core';

import type { ExpandDataset } from '@edata-portal/core';
import type { ColumnDef } from '@tanstack/react-table';
import type { Investigation } from '@edata-portal/icat-plus-api';

export function DatasetTable({
  investigation,
  instrumentName,
  datasetIds,
  nested,
}: {
  investigation: Investigation;
  instrumentName?: string;
  datasetIds?: (string | number)[];
  nested?: boolean;
}) {
  const columns: ColumnDef<ExpandDataset, any>[] = getDatasetTableColumns({});

  return (
    <GenericDatasetTable
      investigation={investigation}
      instrumentName={instrumentName}
      datasetIds={datasetIds}
      nested={nested}
      columns={columns}
    />
  );
}
