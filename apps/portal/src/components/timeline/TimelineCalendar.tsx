import Timeline, {
  type TimelineGroupBase,
  type TimelineItem,
  TimelineHeaders,
  TimelineMarkers,
  TodayMarker,
  DateHeader,
} from '@edata-portal/react-calendar-timeline';
import '@edata-portal/react-calendar-timeline/lib/Timeline.css';

export type TimelineGroupBaseType = typeof TimelineGroupBase;
export type TimelineItemType = typeof TimelineItem;
export function TimelineCalendar({
  startDate,
  endDate,
  groups,
  items,
  itemRenderer,
}: {
  startDate: Date;
  endDate: Date;
  groups: TimelineGroupBaseType[];
  items: TimelineItemType[];
  itemRenderer: any;
}) {
  return (
    <Timeline
      groups={groups}
      items={items}
      visibleTimeStart={startDate.getTime()}
      visibleTimeEnd={endDate.getTime()}
      stackItems={false}
      sidebarWidth={85}
      showCursorLine
      itemHeightRatio={0.75}
      itemRenderer={itemRenderer}
      lineHeight={25}
      canMove={false}
    >
      <TimelineHeaders>
        <DateHeader unit="month" />
        <DateHeader unit="day" />
      </TimelineHeaders>
      <TimelineMarkers>
        <TodayMarker />
      </TimelineMarkers>
    </Timeline>
  );
}
