import { ReprocessContext, type Reprocessing } from '@edata-portal/core';
import { ReprocessModal } from 'components/reprocessing/ReprocessModal';
import { useMemo, useState } from 'react';

export function ReprocessContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [reprocessing, setReprocessing] = useState<Reprocessing | undefined>(
    undefined,
  );

  const value = useMemo(
    () => ({
      reprocess: setReprocessing,
    }),
    [setReprocessing],
  );

  return (
    <ReprocessContext.Provider value={value}>
      {children}

      {reprocessing ? (
        <ReprocessModal
          reprocessing={reprocessing}
          onClose={() => setReprocessing(undefined)}
        />
      ) : null}
    </ReprocessContext.Provider>
  );
}
