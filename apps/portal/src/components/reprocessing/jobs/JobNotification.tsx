import type { Notification } from '@edata-portal/core';
import {
  useGetEndpoint,
  EWOKS_JOB_LIST_ENDPOINT,
  Job,
} from '@edata-portal/icat-plus-api';
import { JobStepStatuses } from 'components/reprocessing/jobs/JobPanel';
import StatusBadge from 'components/reprocessing/jobs/StatusBadge';
import { Suspense, useMemo } from 'react';
import { Col, Row, Spinner } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function createJobNotification(job: Job): Notification {
  return {
    message: <JobNotification jobId={job.jobId} />,
    type: 'info',
    title: (
      <Link className="text-white" to={`/jobs?id=${job.jobId}`}>
        {job.identifier}
      </Link>
    ),
  };
}

export function JobNotification({ jobId }: { jobId: string }) {
  return (
    <Suspense
      fallback={
        <div className="text-center">
          <Spinner />
        </div>
      }
    >
      <LoadAndDisplayContent jobId={jobId} />
    </Suspense>
  );
}

function LoadAndDisplayContent({ jobId }: { jobId: string }) {
  const jobs = useGetEndpoint({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    params: {
      jobId,
    },
  });

  const job = useMemo(() => {
    if (!jobs?.length) return undefined;
    return jobs[0];
  }, [jobs]);

  if (!job) return <>Could not find job</>;

  return <JobNotificationContent job={job} />;
}

function JobNotificationContent({ job }: { job: Job }) {
  return (
    <Col>
      <Row>
        <Col xs={'auto'}>
          <strong>Job status: </strong>
          <StatusBadge status={job.status} />
        </Col>
        <Col xs={'auto'}></Col>
      </Row>
      {job.steps?.length ? (
        <Row>
          <Col>
            <small>
              <JobStepStatuses job={job} excludeFinished />
            </small>
          </Col>
        </Row>
      ) : (
        <Row>
          <Col xs={'auto'}>No progress information yet</Col>
        </Row>
      )}
      <Row>
        <Col xs={'auto'}></Col>
      </Row>
    </Col>
  );
}
