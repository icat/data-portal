import {
  jobStatusColors,
  JOB_STATUSES,
} from 'components/reprocessing/jobs/jobStatus';
import { Badge } from 'react-bootstrap';

export default function StatusBadge({ status }: { status: string }) {
  const bg = jobStatusColors[status as keyof typeof JOB_STATUSES] || 'primary';
  return <Badge bg={bg}>{status}</Badge>;
}
