import { Config, useConfig } from '@edata-portal/core';
import JobsPage from 'components/reprocessing/jobs/JobsPage';
import NotFound from 'components/routing/NotFound';

export default function MyJobs() {
  const config: Config = useConfig();

  if (!config.ui.features.reprocessing) {
    return <NotFound />;
  }

  return <JobsPage />;
}
