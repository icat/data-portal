import {
  addToDate,
  formatDateToIcatDate,
  Loading,
  parseDate,
  SearchBar,
  SideNavElement,
  SideNavFilter,
  SplitPage,
  UI_DATE_DAY_FORMAT,
  useConfig,
  useParam,
  WithSideNav,
} from '@edata-portal/core';
import { JobPanel } from 'components/reprocessing/jobs/JobPanel';
import { JobsList } from 'components/reprocessing/jobs/JobsList';
import { josbStatusOptions } from 'components/reprocessing/jobs/jobStatus';
import { Suspense } from 'react';
import { Col } from 'react-bootstrap';
import ReactSelect from 'react-select';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default function JobsPage({
  investigationId,
}: {
  investigationId?: string;
}) {
  const config = useConfig();
  const [jobId, setJobId] = useParam<string>('id', '');
  const [search, setSearch] = useParam<string>('search', '');
  const [startDate, setStartDate] = useParam<string>(
    'startDate',
    formatDateToIcatDate(
      addToDate(new Date(), {
        days: -config.ui.reprocessing.defaultDayPeriodJobs,
      }),
    ) || '',
  );
  const [endDate, setEndDate] = useParam<string>('endDate', 'undefined');
  const [status, setStatus] = useParam<any>('status', '');

  const nav = (
    <SideNavElement label="My jobs">
      <Col>
        <SideNavFilter label={'Search'}>
          <SearchBar
            placeholder="status, step, dataset, sample, proposal..."
            value={search}
            onUpdate={(v) => {
              setSearch(v);
              setJobId(undefined);
            }}
          />
        </SideNavFilter>
        <SideNavFilter label={'Start date'}>
          <DatePicker
            className="form-control"
            dateFormat={UI_DATE_DAY_FORMAT}
            selected={parseDate(startDate)}
            maxDate={parseDate(endDate)}
            onChange={(date: any) => {
              const strDate = formatDateToIcatDate(date) || 'undefined';
              setStartDate(strDate);
              setJobId(undefined);
            }}
            isClearable
            placeholderText="Start date"
          />
        </SideNavFilter>

        <SideNavFilter label={'End date'}>
          <DatePicker
            className="form-control"
            dateFormat={UI_DATE_DAY_FORMAT}
            selected={parseDate(endDate)}
            minDate={parseDate(startDate)}
            onChange={(date: any) => {
              const strDate = formatDateToIcatDate(date) || 'undefined';
              setEndDate(strDate);
              setJobId(undefined);
            }}
            isClearable
            placeholderText="End date"
          />
        </SideNavFilter>
        <SideNavFilter label="Status">
          <ReactSelect
            options={josbStatusOptions}
            value={status
              ?.split(',')
              .map((val: string) =>
                josbStatusOptions.find((option: any) => option.value === val),
              )
              .filter(Boolean)}
            isClearable
            placeholder={'Select status...'}
            isMulti
            onChange={(selectedOptions) => {
              const selectedValues = selectedOptions
                ? selectedOptions.map((opt: any) => opt.value).join(',')
                : '';
              setStatus(selectedValues);
              setJobId(undefined);
            }}
          />
        </SideNavFilter>
      </Col>
    </SideNavElement>
  );

  return (
    <WithSideNav sideNav={nav}>
      <SplitPage
        left={
          <Suspense fallback={<Loading />}>
            <JobsList
              investigationId={investigationId}
              onJobSelected={(job) => setJobId(job.jobId)}
              selectedId={jobId}
              search={search}
              startDate={startDate}
              endDate={endDate}
              status={status}
            />
          </Suspense>
        }
        right={
          jobId.length ? (
            <Suspense fallback={<Loading />}>
              <JobPanel jobId={jobId} />
            </Suspense>
          ) : null
        }
        ratioLeft={0.3}
        ratioRight={0.7}
      />
    </WithSideNav>
  );
}
