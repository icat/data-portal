import { MetadataTable } from '@edata-portal/core';
import type { Job } from '@edata-portal/icat-plus-api';
import { InvestigationLink } from 'components/navigation/InvestigationLink';

export function JobsMetadataTable({ job }: { job: Job }) {
  const investigationsLinks = job.investigations.map((inv) => (
    <InvestigationLink
      key={inv.id}
      investigationId={inv.id}
      title={inv.name}
    ></InvestigationLink>
  ));
  return (
    <MetadataTable
      parameters={[
        {
          caption: 'Proposal(s)',
          component: <>{investigationsLinks}</>,
        },
        {
          caption: 'Sample(s)',
          value: job.sampleNames.toString(),
        },
      ]}
    ></MetadataTable>
  );
}
