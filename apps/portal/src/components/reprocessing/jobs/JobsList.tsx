import {
  formatDateToDayAndShortTime,
  NoData,
  PaginationMenu,
  useEndpointPagination,
} from '@edata-portal/core';
import { Job, EWOKS_JOB_LIST_ENDPOINT } from '@edata-portal/icat-plus-api';
import { Button, Container, Col, Row } from 'react-bootstrap';
import StatusBadge from 'components/reprocessing/jobs/StatusBadge';
import { JobsMetadataTable } from 'components/reprocessing/jobs/JobMetadataTable';
import React from 'react';

export function JobsList({
  investigationId,
  onJobSelected,
  selectedId,
  search,
  startDate,
  endDate,
  status,
}: {
  investigationId?: string;
  onJobSelected: (job: Job) => void;
  selectedId?: string;
  search?: string;
  startDate?: string;
  endDate?: string;
  status?: string;
}) {
  const jobs = useEndpointPagination({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    params: {
      ...(investigationId ? { investigationId: Number(investigationId) } : {}),
      sortBy: 'createdAt',
      sortOder: -1,
      ...(search ? { search } : {}),
      ...(startDate && startDate !== 'undefined' ? { startDate } : {}),
      ...(endDate && endDate !== 'undefined' ? { endDate } : {}),
      ...(status ? { status } : {}),
    },
    paginationParams: {
      paginationKey: 'jobs',
    },
  });

  if (!jobs.data?.length) return <NoData />;

  return (
    <Container
      fluid
      className="p-1"
      style={{
        overflowY: 'auto',
      }}
    >
      <Col>
        <PaginationMenu {...jobs} />
        {jobs.data.map((job, index) => (
          <React.Fragment key={job.jobId}>
            <JobItem
              key={job.jobId}
              job={job}
              onJobSelected={onJobSelected}
              selectedId={selectedId}
            />
            {index < jobs.data.length - 1 && <hr className="m-0" />}
          </React.Fragment>
        ))}
        <PaginationMenu {...jobs} />
      </Col>
    </Container>
  );
}

function JobItem({
  job,
  onJobSelected,
  selectedId,
}: {
  job: Job;
  onJobSelected: (job: Job) => void;
  selectedId?: string;
}) {
  const jobIsSelected = selectedId === job.jobId;

  return (
    <Button
      onClick={() => onJobSelected(job)}
      size="sm"
      variant={!jobIsSelected ? 'white' : 'light'}
      active={jobIsSelected}
      className="w-100 text-center text-break"
    >
      <Col>
        <Row>
          <Col xs={'auto'}>
            <strong>{job.identifier}</strong>
          </Col>
          <Col>
            <StatusBadge status={job.status} />
          </Col>
        </Row>
        <Row className="text-truncate small">
          <JobsMetadataTable job={job} />
        </Row>
        <Row>
          <small>{formatDateToDayAndShortTime(job.createdAt)}</small>
        </Row>
        <Row>
          <small className={'text-muted'}>Launched by {job.userNames}</small>
        </Row>
      </Col>
    </Button>
  );
}
