import {
  DatasetList,
  formatDateToPreciseTime,
  Loading,
  NoData,
  MetadataTable,
  getLastSteps,
  formatDateToDayAndPreciseTime,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  EWOKS_JOB_LIST_ENDPOINT,
  Job,
  JobEvent,
} from '@edata-portal/icat-plus-api';
import { Suspense, useMemo } from 'react';
import { Col, Container, Row, Tab, Table, Tabs } from 'react-bootstrap';
import StatusBadge from 'components/reprocessing/jobs/StatusBadge';
import { JobsMetadataTable } from 'components/reprocessing/jobs/JobMetadataTable';

function TimeCol({ createdAt }: { createdAt: string }) {
  return (
    <Col
      xs={'auto'}
      className="text-muted text-break
    "
    >
      {formatDateToPreciseTime(createdAt)}
    </Col>
  );
}

function StatusStack({ jobEvent }: { jobEvent: JobEvent }) {
  if (jobEvent.type !== 'status' && jobEvent.type !== 'step') return <></>;
  return (
    <>
      <Col>{jobEvent.name}</Col>
      <Col>
        <StatusBadge status={jobEvent.status} />
      </Col>
    </>
  );
}
function LogStack({ jobEvent }: { jobEvent: JobEvent }) {
  if (jobEvent.type !== 'log') return <></>;
  return (
    <>
      <Col>
        <div>
          <pre className="bg-light p-1 border">
            {JSON.stringify(jobEvent.log, null, 2)}
          </pre>
        </div>
      </Col>
    </>
  );
}

/**
 * This component is a container with 3 columns that displays the step + status + logs
 * @param param0
 * @returns
 */
function LogList({ job }: { job: Job }) {
  return (
    <Container fluid>
      {job.events.map((jobEvent) => (
        <Row className="p-2 align-items-start" key={jobEvent._id}>
          <TimeCol createdAt={jobEvent.createdAt} />
          <StatusStack jobEvent={jobEvent} />
          <LogStack jobEvent={jobEvent} />
        </Row>
      ))}
    </Container>
  );
}

export function JobStepStatuses({
  job,
  excludeFinished,
}: {
  job: Job;
  excludeFinished?: boolean;
}) {
  const steps = useMemo(() => {
    return getLastSteps(job).filter(
      (step) =>
        !excludeFinished ||
        !step.status.toLocaleUpperCase().includes('FINISHED'),
    );
  }, [job, excludeFinished]);

  if (!steps.length) return null;

  return (
    <Table hover size="sm" className="m-0">
      <thead>
        <tr>
          <th
            style={{
              width: 200,
            }}
          >
            Step
          </th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {steps.map((step) => (
          <tr key={step.name}>
            <td>{step.name}</td>
            <td>
              <StatusBadge status={step.status || 'unknown'} />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

/**
 * This components displays the information about a single job in multiple tabs.
 * @param param0
 * @returns
 */
export function JobPanel({ jobId }: { jobId: string }) {
  const jobs = useGetEndpoint({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    params: {
      jobId: jobId,
    },
  });

  const job = useMemo(() => {
    if (!jobs?.length) return undefined;
    return jobs[0];
  }, [jobs]);

  const inputValues = useMemo(() => {
    if (!job) return undefined;
    const copy = { ...job.input };
    delete copy.datasets;
    return copy;
  }, [job]);

  if (!job || !inputValues) return null;

  return (
    <Container fluid className="overflow-auto">
      <Row>
        <Col xs={'auto'}>
          <h5 className="pt-3">{job.identifier}</h5>
        </Col>
      </Row>
      <Row>
        <Col>
          <JobsMetadataTable job={job}></JobsMetadataTable>
        </Col>
        <Col>
          <MetadataTable
            parameters={[
              {
                caption: 'Status',
                component: <StatusBadge status={job.status} />,
              },
              {
                caption: 'Created at',
                value: formatDateToDayAndPreciseTime(job.createdAt),
              },
              {
                caption: 'Updated at',
                value: formatDateToDayAndPreciseTime(job.updatedAt),
              },
              {
                caption: 'Launched by',
                value: job.userNames.toString(),
              },
            ]}
          ></MetadataTable>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <JobStepStatuses job={job} />
        </Col>
      </Row>
      <Row>
        <Col>
          <Tabs>
            <Tab eventKey="datasets" title="Datasets">
              <div className="border-bottom border-start border-end p-2 bg-white ">
                <Suspense fallback={<Loading />}>
                  <Container fluid className="p-0">
                    {job.datasetIds?.length ? (
                      <DatasetList
                        datasetIds={
                          job.datasetIds?.map((i) => i.toString()) || []
                        }
                        groupBy="dataset"
                      />
                    ) : (
                      <NoData />
                    )}
                  </Container>
                </Suspense>
              </div>
            </Tab>
            <Tab eventKey="inputs" title="Input values">
              <div className="border-bottom border-start border-end p-2 bg-white ">
                <pre className="bg-light p-2 border rounded m-0">
                  {JSON.stringify(inputValues, null, 2)}
                </pre>
              </div>
            </Tab>
            <Tab eventKey="status" title="Logs">
              <div className="border-bottom border-start border-end p-2 bg-white ">
                <LogList job={job} />
              </div>
            </Tab>
          </Tabs>
        </Col>
      </Row>
    </Container>
  );
}
