export const JOB_STATUSES = {
  CREATED: 'CREATED',
  FINISHED: 'FINISHED',
  RUNNING: 'RUNNING',
  STARTED: 'STARTED',
  TERMINATED: 'TERMINATED',
} as const;

export const josbStatusOptions = [
  { value: JOB_STATUSES.CREATED, label: 'Created' },
  { value: JOB_STATUSES.STARTED, label: 'Started' },
  { value: JOB_STATUSES.RUNNING, label: 'Running' },
  { value: JOB_STATUSES.FINISHED, label: 'Finished' },
  { value: JOB_STATUSES.TERMINATED, label: 'Terminated' },
] as const;

export const jobStatusColors: Record<keyof typeof JOB_STATUSES, string> = {
  CREATED: 'info',
  FINISHED: 'success',
  RUNNING: 'warning',
  STARTED: 'secondary',
  TERMINATED: 'danger',
};
