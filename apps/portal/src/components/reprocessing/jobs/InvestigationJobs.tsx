import { Config, useConfig, usePath } from '@edata-portal/core';
import {
  EWOKS_JOB_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faGears } from '@fortawesome/free-solid-svg-icons';
import { NavTab } from 'components/navigation/NavTab';
import JobsPage from 'components/reprocessing/jobs/JobsPage';
import NotFound from 'components/routing/NotFound';
import { useMemo } from 'react';

export default function InvestigationJobs() {
  const investigationId = usePath('investigationId');

  const config: Config = useConfig();

  if (!config.ui.features.reprocessing) {
    return <NotFound />;
  }

  return <JobsPage investigationId={investigationId} />;
}

export function InvestigationJobsHeader({
  investigationId,
}: {
  investigationId: string;
}) {
  const jobs = useGetEndpoint({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    params: {
      investigationId: Number(investigationId),
      limit: 1,
    },
  });
  const nb = useMemo(() => {
    if (!jobs?.length) {
      return 0;
    }
    return jobs[0].meta.page.total;
  }, [jobs]);

  if (!nb) return null;

  return (
    <NavTab
      to={`/investigation/${investigationId}/jobs`}
      icon={faGears}
      label={'Jobs'}
      badge={nb.toString()}
    />
  );
}
