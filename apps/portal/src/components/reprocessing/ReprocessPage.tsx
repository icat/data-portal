import { Config, SplitPage, useConfig, useSelection } from '@edata-portal/core';
import ReprocessPanel from 'components/reprocessing/ReprocessPanel';
import NotFound from 'components/routing/NotFound';
import { EmptySelection } from 'components/selection/EmptySelection';
import { SelectionPanelForType } from 'components/selection/SelectionPanelForType';
import { Container } from 'react-bootstrap';

export default function ReprocessPage() {
  const { value: selectedDatasets } = useSelection('dataset');

  const config: Config = useConfig();

  if (selectedDatasets.length === 0) {
    return <EmptySelection />;
  }

  if (!config.ui.features.reprocessing) {
    return <NotFound />;
  }

  return (
    <SplitPage
      left={
        <Container fluid className="mt-2 mb-2 overflow-auto">
          <SelectionPanelForType
            type={'dataset'}
            hideReprocess
            hideMintDOI
            linkToSelectionPage
            hideDownload
          />
        </Container>
      }
      right={
        <Container fluid className="mt-2 mb-2 overflow-auto">
          <ReprocessPanel
            reprocessing={{
              datasetIds: selectedDatasets,
            }}
          />
        </Container>
      }
    />
  );
}
