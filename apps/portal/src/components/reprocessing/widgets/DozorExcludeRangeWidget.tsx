import type { Dataset } from '@edata-portal/icat-plus-api';
import type { WidgetProps } from '@rjsf/utils';
import { RemoteComponent } from 'components/RemoteComponent';
import { useMemo } from 'react';

export function DozorExcludeRangeWidget({
  datasets,
  widgetProps,
}: {
  datasets: Dataset[];
  widgetProps: WidgetProps;
}) {
  const refDataset = useMemo(() => {
    return datasets[0];
  }, [datasets]);

  if (!refDataset) return null;

  return (
    <RemoteComponent
      remote={{
        name: 'mx',
        component: './MXDozorExcludeRange',
      }}
      props={{
        dataset: refDataset,
        ...widgetProps,
      }}
    />
  );
}
