import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Alert, Col, Row } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { faRocket } from '@fortawesome/free-solid-svg-icons';
import type { WorkflowDescription } from '@edata-portal/icat-plus-api';

/**
 * This component renders a list of buttons with the valid workflows depeding on the metadata associated to the datasets
 * @param param0
 * @returns
 */
export function ReprocessSelectWorkflow({
  onWorkflowSelected,
  selectedWorkflow,
  workflows,
}: {
  onWorkflowSelected: (workflow: WorkflowDescription) => void;
  selectedWorkflow?: WorkflowDescription;
  workflows?: WorkflowDescription[];
}) {
  if (!workflows?.length) {
    return (
      <Alert className="m-2 p-2" variant="warning">
        No workflows were found for this selection.
      </Alert>
    );
  }
  return (
    <Row className="g-2">
      {workflows.map((workflow: WorkflowDescription) => (
        <Col xs={'auto'} key={workflow.id}>
          <Button
            variant={'outline-success'}
            style={{
              marginTop: '0.5rem',
            }}
            size="sm"
            onClick={() => onWorkflowSelected(workflow)}
            active={selectedWorkflow?.id === workflow.id}
          >
            <FontAwesomeIcon
              icon={faRocket}
              style={{
                marginRight: 5,
              }}
            />
            {workflow.label}
          </Button>
        </Col>
      ))}
    </Row>
  );
}
