import { Alert } from 'react-bootstrap';
import validator from '@rjsf/validator-ajv8';
import Form from '@rjsf/bootstrap-4';
import {
  Dataset,
  WorkflowDescription,
  useMutateEndpoint,
  EWOKS_JOB_LAUNCH_ENDPOINT,
  Job,
} from '@edata-portal/icat-plus-api';
import type { WidgetProps } from '@rjsf/utils';
import { useMemo } from 'react';
import { DozorExcludeRangeWidget } from 'components/reprocessing/widgets/DozorExcludeRangeWidget';
import { getDatasetParamValue } from '@edata-portal/core';

export interface InputSchemaItemDefault {
  type: string;
  value: string;
}

interface InputSchemaItem {
  description?: string;
  title: string;
  type?: string;
  default?: string | number | boolean;
  populate?: InputSchemaItemDefault[];
}

/**
 * Given a workflowId it will built a form based on the first node input parameters
 * @param workflowId id of the ewoks workflow
 * @returns
 */
export default function ReprocessForm({
  workflow,
  datasets,
  onJobLaunched,
}: {
  workflow: WorkflowDescription;
  datasets: Dataset[];
  onJobLaunched: (job: Job) => void;
}) {
  const runWorkflow = useMutateEndpoint({
    endpoint: EWOKS_JOB_LAUNCH_ENDPOINT,
    params: {
      identifier: workflow.id,
      datasetIds: datasets.map((d) => d.id),
    },
  });

  const datasetWidgets = useMemo(
    () => ({
      dozorExcludeRange: (props: WidgetProps) => {
        return (
          <DozorExcludeRangeWidget datasets={datasets} widgetProps={props} />
        );
      },
    }),
    [datasets],
  );

  /**
   * Parses the value of a specified parameter from a dataset.
   * Depending on the item.type defined in the json schema of the workflow, it converts the value to `boolean` or `number` to ensure proper validation.
   * and will parse depending on the item.type defined in the json schema of the workflow
   * We need to do so in order to avoid validation problems in the form
   * @param dataset
   * @param parameterName
   * @param type
   * @returns
   */
  const getValuebyParameterName = (
    dataset: Dataset,
    parameterName: string,
    type?: String,
  ) => {
    const parameterValue = getDatasetParamValue(dataset, parameterName);
    if (parameterValue && type === 'boolean') {
      return Boolean(parameterValue);
    } else if (parameterValue && type === 'number') {
      return parseFloat(parameterValue);
    }

    return parameterValue;
  };
  // This reads the properties of the input_schema and in fills with the default value depending on the default property
  const inputs = useMemo(() => {
    return Object.entries(workflow.input_schema.properties).map((property) => {
      const item = property[1] as InputSchemaItem;
      const parameterName = property[0];
      let obj: any = {};
      obj[parameterName] = ''; // Default values for non populate items

      if (!item.populate) {
        if (item.default) {
          obj[parameterName] = item.default;
        } else {
          obj[parameterName] = '';
        }
      } else {
        for (let index = 0; index < item.populate.length; index++) {
          const element = item.populate[index];
          obj[parameterName] = getValuebyParameterName(
            datasets[0],
            element.value,
            item.type,
          );
          /* in case there is a default but the parameterName did not exist we fallback to default */
          if (
            item.default &&
            index === item.populate.length - 1 &&
            !obj[parameterName]
          ) {
            obj[parameterName] = item.default;
          }

          if (obj[parameterName]) return obj;
        }
      }

      return obj;
    });
  }, [workflow, datasets]);

  const formData: any = {};
  inputs.forEach((item) => {
    const key = Object.keys(item)[0];
    const value = item[key];
    formData[key] = value;
  });

  const { input_schema, ui_schema } = workflow;
  if (!input_schema) return <Alert>No input defined for this workflow</Alert>;

  const onSubmit = ({ formData }: any, e: any) => {
    formData.datasets = datasets;
    runWorkflow
      .mutateAsync({
        body: formData,
      })
      .then((response) => {
        onJobLaunched(response as Job);
      });
  };

  return (
    <Form
      schema={input_schema}
      uiSchema={{
        'ui:submitButtonOptions': {
          submitText: 'Run',
          ...ui_schema['ui:submitButtonOptions'],
        },
        ...ui_schema,
      }}
      formData={formData}
      validator={validator}
      onSubmit={onSubmit}
      widgets={datasetWidgets}
    />
  );
}
