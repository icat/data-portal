import { Loading, type Reprocessing } from '@edata-portal/core';
import ReprocessPanel from 'components/reprocessing/ReprocessPanel';
import { Suspense } from 'react';
import { Modal } from 'react-bootstrap';

export function ReprocessModal({
  reprocessing,
  onClose,
}: {
  reprocessing: Reprocessing;
  onClose: () => void;
}) {
  return (
    <Modal show={true} onHide={onClose} size="lg" centered>
      <Modal.Header closeButton>
        <Modal.Title>Start reprocess</Modal.Title>
      </Modal.Header>
      <Modal.Body
        className="p-2"
        style={{ maxHeight: 'calc(100vh - 150px)', overflowY: 'auto' }}
      >
        <Suspense fallback={<Loading />}>
          <ReprocessPanel reprocessing={reprocessing} onDone={onClose} />
        </Suspense>
      </Modal.Body>
    </Modal>
  );
}
