import {
  useConfig,
  type Config,
  Loading,
  useNotify,
  Reprocessing,
  useAvailableWorkflows,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  DATASET_LIST_ENDPOINT,
  Dataset,
  Job,
  WorkflowDescription,
} from '@edata-portal/icat-plus-api';
import { Col, Row } from 'react-bootstrap';
import { Suspense, useMemo, useState } from 'react';
import ReprocessForm from 'components/reprocessing/ReprocessForm';
import { createJobNotification } from 'components/reprocessing/jobs/JobNotification';
import { ReprocessSelectWorkflow } from 'components/reprocessing/ReprocessSelectWorkflow';

export default function ReprocessPanel({
  reprocessing,
  onDone,
}: {
  reprocessing: Reprocessing;
  onDone?: () => void;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: reprocessing.datasetIds.join(','),
    },
    default: [] as Dataset[],
    skipFetch: reprocessing.datasetIds.length === 0,
  });

  const workflows = useAvailableWorkflows(datasets);

  const preSelectedWorkflow = useMemo(() => {
    return workflows.find((w) => w.id === reprocessing.workflowId);
  }, [workflows, reprocessing.workflowId]);

  const initialWorkflow = useMemo(() => {
    if (preSelectedWorkflow) return preSelectedWorkflow;
    if (workflows.length) return workflows[0];
    return undefined;
  }, [preSelectedWorkflow, workflows]);

  const [workflow, setWorkflow] = useState<WorkflowDescription | undefined>(
    initialWorkflow,
  );

  const onWorkflowSelected = function (workflow: WorkflowDescription) {
    setWorkflow(workflow);
  };

  const notify = useNotify();

  const onJobLaunched = function (job: Job) {
    notify(createJobNotification(job));
    if (onDone) onDone();
  };

  const config: Config = useConfig();

  if (!config.ui.features.reprocessing) {
    return null;
  }
  if (!datasets.length) {
    return (
      <Row>
        <Col className="text-muted">
          <small>No datasets selected</small>
        </Col>
      </Row>
    );
  }

  const form = workflow ? (
    <Suspense fallback={<Loading />}>
      <div className="mt-2 bg-white border p-2">
        <ReprocessForm
          workflow={workflow}
          datasets={datasets}
          onJobLaunched={onJobLaunched}
        />
      </div>
    </Suspense>
  ) : null;

  if (preSelectedWorkflow) return form;

  return (
    <Col>
      <h5>1. Select a workflow</h5>
      <ReprocessSelectWorkflow
        onWorkflowSelected={onWorkflowSelected}
        selectedWorkflow={workflow}
        workflows={workflows}
      />
      <hr />
      <h5>2. Setup the parameters</h5>
      {form}
    </Col>
  );
}
