import { ViewersContext } from '@edata-portal/core';
import { DatasetViewer } from 'components/viewers/DatasetViewer';
import { SampleViewer } from 'components/viewers/SampleViewer';
import { InvestigationViewer } from 'components/viewers/InvestigationViewer';

export function ViewersProvider({ children }: { children: React.ReactNode }) {
  return (
    <ViewersContext.Provider
      value={{
        viewSample: (sample, props) => (
          <SampleViewer key={sample.id} sample={sample} props={props} />
        ),
        viewDataset: (dataset, type, props) => (
          <DatasetViewer
            key={dataset.id}
            dataset={dataset}
            type={type}
            props={props}
          />
        ),
        viewInvestigation: (investigation, props) => (
          <InvestigationViewer
            key={investigation.id}
            investigation={investigation}
            props={props}
          />
        ),
      }}
    >
      {children}
    </ViewersContext.Provider>
  );
}
