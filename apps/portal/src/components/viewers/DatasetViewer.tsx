import {
  getInstrumentNameByInvestigation,
  DatasetViewerType,
  first,
  GenericDatasetSnapshotViewer,
  formatDateToDayAndTime,
  GenericDatasetDetailsViewer,
  DATASET_VIEWER_TYPES,
  getDatasetParamValue,
  PROJECT_NAME_PARAM,
  DEFINITION_PARAM,
} from '@edata-portal/core';
import GenericDatasetTableCellViewer from '@edata-portal/core/src/components/dataset/generic/GenericDatasetTableCellViewer';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { useSuspenseQuery } from '@tanstack/react-query';
import { JSONSchemaType } from 'ajv';
import { RemoteComponent } from 'components/RemoteComponent';
import {
  GenericViewerDefinition,
  useFilterViewers,
  validateViewers,
} from 'components/viewers/viewerDefinition';
import { useEffect, useMemo } from 'react';
import { Alert } from 'react-bootstrap';
import { REMOTE_DEFINITION_SCHEMA, RemoteDefinition } from 'remotes';

export type DatasetViewerDefinition =
  GenericViewerDefinition<DatasetViewerRender>;
export type DatasetViewerRender = {
  [K in DatasetViewerType]?: {
    type: 'remote';
    remote: RemoteDefinition;
  };
};

const RENDER_SCHEMA: JSONSchemaType<DatasetViewerRender> = {
  type: 'object',
  properties: DATASET_VIEWER_TYPES.reduce((acc, type) => {
    acc[type] = {
      type: 'object',
      properties: {
        type: { type: 'string', enum: ['remote'], nullable: false },
        remote: REMOTE_DEFINITION_SCHEMA,
      },
      required: ['type', 'remote'],
      additionalProperties: false,
    };
    return acc;
  }, {} as any),
};

function useViewer(
  dataset?: Dataset,
  viewers?: DatasetViewerDefinition[],
  type?: DatasetViewerType,
) {
  const investigation = dataset?.investigation;

  const viewersWithType = useMemo(() => {
    if (!type) return viewers;
    return viewers?.filter((viewer) => type in viewer.render);
  }, [viewers, type]);

  const applicableViewers = useFilterViewers(
    {
      investigationId: investigation?.id?.toString(),
      datasetId: dataset?.id?.toString(),
      beamline: investigation
        ? getInstrumentNameByInvestigation(investigation)
        : undefined,
      date: dataset?.startDate,
      datasetParameters: dataset?.parameters,
      technique: dataset
        ? getDatasetParamValue(dataset, DEFINITION_PARAM)
        : undefined,
      project: dataset
        ? getDatasetParamValue(dataset, PROJECT_NAME_PARAM)
        : undefined,
    },
    viewersWithType,
  );

  return first(applicableViewers);
}

export function DatasetViewer({
  dataset,
  props,
  type,
}: {
  dataset: Dataset;
  props?: any;
  type: DatasetViewerType;
}) {
  const { data: config } = useSuspenseQuery({
    queryKey: ['dataset.viewer.config'],
    queryFn: async () => {
      const response = await fetch('/config/dataset.viewer.config.json');
      return (await response.json()) as DatasetViewerDefinition[];
    },
  });

  useEffect(() => {
    if (config) {
      validateViewers({
        renderSchema: RENDER_SCHEMA,
        viewers: config,
      });
    }
  }, [config]);

  const viewer = useViewer(dataset, config, type);

  const viewerForType = viewer?.render[type];

  // This forces the display of the viewer despite its remote configuration
  if (type === 'generic') {
    return <GenericDatasetDetailsViewer dataset={dataset} {...props} />;
  }

  if (viewerForType?.type === 'remote') {
    return (
      <RemoteComponent
        remote={viewerForType.remote}
        props={{
          dataset,
          ...props,
        }}
      />
    );
  }

  //default viewer
  if (type === 'details') {
    return <GenericDatasetDetailsViewer dataset={dataset} {...props} />;
  }
  if (type === 'tableCell') {
    return <GenericDatasetTableCellViewer dataset={dataset} {...props} />;
  }
  if (type === 'snapshot') {
    return (
      <GenericDatasetSnapshotViewer
        {...props}
        dataset={dataset}
        viewerConfig={(dataset) => ({
          snapshot: {
            main: [
              {
                type: 'metadata',
                metadata: [
                  {
                    caption: 'Start',
                    value: formatDateToDayAndTime(dataset.startDate),
                  },
                  {
                    caption: 'Technique',
                    parameterName: 'definition',
                  },
                ],
              },
            ],
            showGallery: true,
          },
          details: {
            summaryTab: true,
            filesTab: true,
            metadataTab: true,
            showPathInFilesTab: true,
          },
        })}
      />
    );
  }
  return <Alert variant="danger">No viewer found</Alert>;
}
