import {
  DEFINITION_PARAM,
  parseDate,
  PROJECT_NAME_PARAM,
} from '@edata-portal/core';
import {
  Dataset,
  DATASET_PARAMETER_VALUE_ENDPOINT,
  DatasetParameterValuesResult,
  Parameter,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import Ajv, { JSONSchemaType } from 'ajv';
import { useMemo } from 'react';

export type GenericViewerDefinition<T> = {
  date?: string;
  beamline?: string | string[];
  projectName?: string;
  technique?: string;
  datasetParameter?: { [key: string]: string | string[] | undefined | null };
  render: T;
};

function getViewerDefinitionSchema<T>(
  renderSchema: JSONSchemaType<T>,
): JSONSchemaType<GenericViewerDefinition<T>> {
  return {
    type: 'object',
    properties: {
      date: { type: 'string', nullable: true },
      beamline: {
        type: ['string', 'array'],
        items: { type: 'string' },
        nullable: true,
      },
      projectName: { type: 'string', nullable: true },
      technique: { type: 'string', nullable: true },
      datasetParameter: {
        type: 'object',
        additionalProperties: {
          type: ['string', 'array'],
          items: { type: 'string' },
          nullable: true,
        },
        nullable: true,
      },
      render: { ...renderSchema },
    },
    required: ['render'],
    additionalProperties: false,
  } as any;
}

export function validateViewers<T>({
  renderSchema,
  viewers,
}: {
  renderSchema: JSONSchemaType<T>;
  viewers: GenericViewerDefinition<T>[];
}) {
  const ajv = new Ajv({
    allowUnionTypes: true,
  });
  const schema = getViewerDefinitionSchema(renderSchema);
  const validate = ajv.compile(schema);
  viewers.forEach((viewer) => {
    const valid = validate(viewer);
    if (!valid) {
      console.error(validate.errors);
      throw new Error(
        `Invalid sample viewer config:\n\n${ajv.errorsText(validate.errors)}\n\n${JSON.stringify(viewer)}\n\n`,
      );
    }
  });
}

export function useFilterViewers<T>(
  data: {
    investigationId?: string;
    sampleId?: string;
    datasetId?: string;
    beamline?: string;
    date?: string;
    datasetParameters?: Dataset['parameters'];
    technique?: string;
    project?: string;
  },
  viewers?: GenericViewerDefinition<T>[],
) {
  const { investigationId, sampleId, beamline, date, datasetId } = data;

  const applicableViewers = useMemo(() => {
    if (!investigationId || !beamline || !date || !viewers?.length) return [];
    return viewers.filter((viewer) => {
      //check beamline
      if (viewer.beamline !== undefined) {
        const beamlineUpper = beamline.toUpperCase();
        if (Array.isArray(viewer.beamline)) {
          if (
            !viewer.beamline.map((v) => v.toUpperCase()).includes(beamlineUpper)
          ) {
            return false;
          }
        } else {
          if (viewer.beamline.toUpperCase() !== beamlineUpper) {
            return false;
          }
        }
      }

      //check date
      if (viewer.date !== undefined) {
        const parsedDate = parseDate(data.date);
        const viewerDate = parseDate(viewer.date);
        if (!viewerDate || !parsedDate) return false;
        if (parsedDate.getTime() < viewerDate.getTime()) {
          return false;
        }
      }

      return true;
    });
  }, [investigationId, beamline, date, viewers, data.date]);

  const needsTechniques = useMemo(() => {
    if (data.technique || !investigationId || !applicableViewers?.length)
      return false;
    return applicableViewers.some((v) => !!v.technique);
  }, [applicableViewers, data.technique, investigationId]);

  const experimentTechniques = useGetEndpoint({
    endpoint: DATASET_PARAMETER_VALUE_ENDPOINT,
    params: {
      investigationId: investigationId,
      name: DEFINITION_PARAM,
      datasetType: 'acquisition',
      ...(sampleId ? { sampleId } : {}),
    },
    default: [] as DatasetParameterValuesResult[],
    skipFetch: !needsTechniques,
  });

  const needsProjects = useMemo(() => {
    if (data.project || !investigationId || !applicableViewers?.length)
      return false;
    return applicableViewers.some((v) => !!v.projectName);
  }, [applicableViewers, data.project, investigationId]);

  const experimentProjects = useGetEndpoint({
    endpoint: DATASET_PARAMETER_VALUE_ENDPOINT,
    params: {
      investigationId: investigationId,
      name: PROJECT_NAME_PARAM,
      datasetType: 'acquisition',
      ...(sampleId ? { sampleId } : {}),
    },
    default: [] as DatasetParameterValuesResult[],
    skipFetch: !needsProjects,
  });

  const datasetParameterFilters = useMemo(() => {
    if (
      data.datasetParameters ||
      !investigationId ||
      !applicableViewers?.length
    )
      return [];
    return applicableViewers
      .map((v) => v.datasetParameter)
      .flatMap((v) => Object.keys(v || {}))
      .filter((v): v is string => !!v);
  }, [applicableViewers, data.datasetParameters, investigationId]);

  const experimentDatasetParameters = useGetEndpoint({
    endpoint: DATASET_PARAMETER_VALUE_ENDPOINT,
    params: {
      investigationId: investigationId,
      name: datasetParameterFilters.join(','),
      datasetType: 'acquisition',
      ...(sampleId ? { sampleId } : {}),
    },
    default: [] as DatasetParameterValuesResult[],
    skipFetch: !datasetParameterFilters.length,
  });

  return useMemo(() => {
    const projectData = data.project
      ? [data.project]
      : datasetId
        ? []
        : experimentProjects.flatMap((p) => p.values);
    const techniqueData = data.technique
      ? [data.technique]
      : datasetId
        ? []
        : experimentTechniques.flatMap((p) => p.values);
    const datasetParameterData =
      data.datasetParameters || experimentDatasetParameters;

    return applicableViewers.filter(
      ({ projectName, technique, datasetParameter }) => {
        if (
          (projectName && !projectData.length) ||
          (technique && !techniqueData.length) ||
          (datasetParameter && !datasetParameterData.length)
        ) {
          return false;
        }
        return (
          (!projectName || projectData.includes(projectName)) &&
          (!technique || techniqueData.includes(technique)) &&
          (!datasetParameter ||
            checkDatasetParameters(datasetParameterData, datasetParameter))
        );
      },
    );
  }, [
    applicableViewers,
    data.datasetParameters,
    data.project,
    data.technique,
    datasetId,
    experimentDatasetParameters,
    experimentProjects,
    experimentTechniques,
  ]);
}

function checkDatasetParameters(
  values: DatasetParameterValuesResult[] | Parameter[],
  datasetParameters: { [key: string]: string | string[] | undefined | null },
) {
  function hasValue(key: string, targetValue: string | undefined | null) {
    return values.some((value) => {
      if (value.name !== key) return false;
      if (targetValue === undefined || targetValue === null) return true;
      if ('value' in value) return value.value === targetValue;
      if ('values' in value) return value.values.includes(targetValue);
      return false;
    });
  }

  for (const key in datasetParameters) {
    const target = datasetParameters[key];
    if (Array.isArray(target)) {
      if (!target.some((v) => hasValue(key, v))) return false;
    } else if (!hasValue(key, target)) return false;
  }

  return true;
}
