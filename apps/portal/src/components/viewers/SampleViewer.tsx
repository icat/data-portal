import type { Sample } from '@edata-portal/icat-plus-api';
import { first, getInstrumentNameByInvestigation } from '@edata-portal/core';
import { REMOTE_DEFINITION_SCHEMA, RemoteDefinition } from 'remotes';
import { useSuspenseQuery } from '@tanstack/react-query';
import { RemoteComponent } from 'components/RemoteComponent';
import GenericSampleDatasetGraph from 'components/viewers/generic/sample/GenericSampleDatasetGraph';
import GenericSampleViewer from 'components/viewers/generic/GenericSampleViewer';
import {
  GenericViewerDefinition,
  useFilterViewers,
  validateViewers,
} from 'components/viewers/viewerDefinition';
import { useEffect } from 'react';
import { JSONSchemaType } from 'ajv';

export type SampleViewerDefinition =
  GenericViewerDefinition<SampleViewerRender>;

export type SampleViewerRender =
  | {
      type: 'remote';
      remote: RemoteDefinition;
    }
  | {
      type: 'generic';
      format: 'list' | 'graph';
      props?: any;
    };

const RENDER_SCHEMA: JSONSchemaType<SampleViewerRender> = {
  anyOf: [
    {
      type: 'object',
      properties: {
        type: { type: 'string', enum: ['remote'], nullable: false },
        remote: REMOTE_DEFINITION_SCHEMA,
      },
      required: ['type', 'remote'],
      additionalProperties: false,
    },
    {
      type: 'object',
      properties: {
        type: { type: 'string', enum: ['generic'], nullable: false },
        format: { type: 'string', enum: ['list', 'graph'], nullable: false },
        props: { type: 'object', nullable: true },
      },
      required: ['type', 'format'],
    },
  ],
};

function useViewer(sample?: Sample, viewers?: SampleViewerDefinition[]) {
  const investigation = sample?.investigation;

  const applicableViewers = useFilterViewers(
    {
      investigationId: sample?.investigation?.id?.toString(),
      beamline: investigation
        ? getInstrumentNameByInvestigation(investigation)
        : undefined,
      date: sample?.investigation?.startDate,
      sampleId: sample?.id?.toString(),
    },
    viewers,
  );

  return first(applicableViewers);
}

export function SampleViewer({
  sample,
  props,
}: {
  sample: Sample;
  props?: any;
}) {
  const { data: config } = useSuspenseQuery({
    queryKey: ['sample.viewer.config'],
    queryFn: async () => {
      const response = await fetch('/config/sample.viewer.config.json');
      return (await response.json()) as SampleViewerDefinition[];
    },
  });

  useEffect(() => {
    if (config) {
      validateViewers({
        renderSchema: RENDER_SCHEMA,
        viewers: config,
      });
    }
  }, [config]);

  const viewer = useViewer(sample, config);

  if (viewer?.render.type === 'remote') {
    return (
      <RemoteComponent
        remote={viewer.render.remote}
        props={{
          sample,
          ...props,
        }}
      />
    );
  } else if (viewer?.render.type === 'generic') {
    if (viewer.render.format === 'graph') {
      return (
        <GenericSampleDatasetGraph
          sample={sample}
          {...viewer.render.props}
          {...props}
        />
      );
    }
    if (viewer.render.format === 'list') {
      return (
        <GenericSampleViewer
          sample={sample}
          {...viewer.render.props}
          {...props}
        />
      );
    }
  }

  //default viewer
  return <GenericSampleViewer sample={sample} {...props} />;
}
