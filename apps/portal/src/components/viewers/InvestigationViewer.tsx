import { first, getInstrumentNameByInvestigation } from '@edata-portal/core';
import { Investigation } from '@edata-portal/icat-plus-api';
import { useSuspenseQuery } from '@tanstack/react-query';
import { JSONSchemaType } from 'ajv';
import { DatasetTable } from 'components/dataset/DatasetTable';
import { RemoteComponent } from 'components/RemoteComponent';
import GenericInvestigationViewer from 'components/viewers/generic/GenericInvestigationViewer';
import {
  GenericViewerDefinition,
  useFilterViewers,
  validateViewers,
} from 'components/viewers/viewerDefinition';
import { useEffect } from 'react';

import { REMOTE_DEFINITION_SCHEMA, RemoteDefinition } from 'remotes';

export type InvestigationViewerDefinition =
  GenericViewerDefinition<InvestigationViewerRender>;

export type InvestigationViewerRender =
  | {
      type: 'remote';
      remote: RemoteDefinition;
    }
  | {
      type: 'generic';
    };

const RENDER_SCHEMA: JSONSchemaType<InvestigationViewerRender> = {
  anyOf: [
    {
      type: 'object',
      properties: {
        type: { type: 'string', enum: ['remote'], nullable: false },
        remote: REMOTE_DEFINITION_SCHEMA,
      },
      required: ['type', 'remote'],
      additionalProperties: false,
    },
    {
      type: 'object',
      properties: {
        type: { type: 'string', enum: ['generic'], nullable: false },
      },
      required: ['type'],
    },
  ],
};

function useViewer(
  investigation?: Investigation,
  viewers?: InvestigationViewerDefinition[],
) {
  const applicableViewers = useFilterViewers(
    {
      investigationId: investigation?.id?.toString(),
      beamline: investigation
        ? getInstrumentNameByInvestigation(investigation)
        : undefined,
      date: investigation?.startDate,
    },
    viewers,
  );

  return first(applicableViewers);
}

export function InvestigationViewer({
  investigation,
  props,
}: {
  investigation: Investigation;
  props?: any;
}) {
  const { data: config } = useSuspenseQuery({
    queryKey: ['investigation.viewer.config'],
    queryFn: async () => {
      const response = await fetch('/config/investigation.viewer.config.json');
      return (await response.json()) as InvestigationViewerDefinition[];
    },
  });

  useEffect(() => {
    if (config) {
      validateViewers({
        renderSchema: RENDER_SCHEMA,
        viewers: config,
      });
    }
  }, [config]);

  const viewer = useViewer(investigation, config);

  if (viewer?.render.type === 'remote') {
    return (
      <RemoteComponent
        remote={viewer.render.remote}
        props={{
          investigation,
          ...props,
        }}
      />
    );
  } else if (viewer?.render.type === 'generic') {
    return (
      <GenericInvestigationViewer investigation={investigation} {...props} />
    );
  }

  //default viewer
  return (
    <DatasetTable investigation={investigation} nested={true} {...props} />
  );
}
