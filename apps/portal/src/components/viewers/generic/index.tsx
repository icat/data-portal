import React from 'react';

export const GenericInvestigationViewer = React.lazy(
  () => import('./GenericInvestigationViewer'),
);

export const GenericSampleViewer = React.lazy(
  () => import('./GenericSampleViewer'),
);
