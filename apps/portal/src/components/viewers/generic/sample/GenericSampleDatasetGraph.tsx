import {
  GridGraphDefinition,
  GridGraphItem,
  GridGraphRender,
  HorizontalScroll,
  SampleWithDatasetGroups,
  first,
  sortDatasetsByDate,
} from '@edata-portal/core';
import type { Dataset, Sample } from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export default function GenericSampleDatasetGraph({
  sample,
  paginationSize,
}: {
  sample: Sample;
  paginationSize: number;
}) {
  return (
    <SampleWithDatasetGroups
      sample={sample}
      computeGroups={computeGroupsDataset}
      renderGroup={(group) => <RenderDatasetGroup group={group} />}
      paginationSize={paginationSize}
    />
  );
}

function computeGroupsDataset(datasets: Dataset[]) {
  return datasets.map((dataset) => [dataset]);
}

function RenderDatasetGroup({ group }: { group: Dataset[] }) {
  const grid = useMemo(() => computeGridGraph(group), [group]);
  return (
    <HorizontalScroll>
      <GridGraphRender grid={grid} />
    </HorizontalScroll>
  );
}

function computeGridGraph(datasets: Dataset[]): GridGraphDefinition {
  return {
    root: getDatasetsGrid(datasets),
  };
}

function getDatasetsGrid(datasets: Dataset[]): GridGraphItem {
  const firstDataset = first(datasets);

  return {
    id: (firstDataset?.id || 'empty') + '-list-column',
    type: 'column',
    items: datasets.map(getDatasetGrid),
    collapsible: false,
  };
}

function getDatasetGrid(dataset: Dataset): GridGraphItem {
  const processed = sortDatasetsByDate(dataset.outputDatasets || []);

  const datasetCell: GridGraphItem = {
    type: 'cell',
    id: dataset.id.toString(),
    content: { type: 'dataset', dataset },
  };

  if (!processed?.length) return datasetCell;

  if (processed.length >= 2)
    return {
      type: 'row',
      id: dataset.id.toString() + '-processed-row',
      items: [datasetCell, ...processed.map(getDatasetGrid)],
    };

  return {
    type: 'row',
    id: dataset.id.toString() + '-processed-row',
    items: [
      datasetCell,
      {
        id: dataset.id.toString() + '-processed-column',
        type: 'column',
        items: processed.map(getDatasetGrid),
        collapsible: true,
        collapsedLabel: 'Processed',
      },
    ],
  };
}
