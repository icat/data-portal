import { SampleHeaderWithOutputs } from '@edata-portal/core';
import type { Sample } from '@edata-portal/icat-plus-api';

export default function GenericSampleViewer({ sample }: { sample: Sample }) {
  return <SampleHeaderWithOutputs sample={sample} />;
}
