import {
  DatasetList,
  useParam,
  WithSideNav,
  GenericDatasetsFilter,
  useUserPreferences,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export default function GenericInvestigationViewer({
  investigation,
}: {
  investigation: Investigation;
}) {
  const [filters, setFilters] = useParam<string>('filters', '');
  const [isGroupedBySample, setIsGroupedBySample] = useUserPreferences(
    'isGroupedBySample',
    true,
  );

  const [sampleChecked, setSampleChecked] = useParam<string>(
    'groupBySample',
    isGroupedBySample.toString(),
  );

  const updateSampleChecked = (checked: string) => {
    setSampleChecked(checked);
    setIsGroupedBySample(checked === 'true');
  };

  const [sampleId, setSampleId] = useParam<string>('sampleId', '');
  const [search, setSearch] = useParam<string>('search', '');
  const groupBy = useMemo(() => {
    return sampleChecked === 'true' ? 'sample' : 'dataset';
  }, [sampleChecked]);

  return (
    <WithSideNav
      sideNav={
        <GenericDatasetsFilter
          filters={filters}
          setFilters={setFilters}
          investigation={investigation}
          setSampleId={setSampleId}
          selectedSampleId={Number(sampleId)}
          sampleChecked={sampleChecked}
          setSampleChecked={updateSampleChecked}
          datasetSearch={search}
          setDatasetSearch={setSearch}
        />
      }
    >
      <DatasetList
        groupBy={groupBy}
        parameterFilter={filters?.length ? filters : undefined}
        investigationId={investigation.id.toString()}
        sampleIds={sampleId?.length ? [sampleId] : undefined}
        sampleId={sampleId}
        search={groupBy === 'sample' ? '' : search}
      />
    </WithSideNav>
  );
}
