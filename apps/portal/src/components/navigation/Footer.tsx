import { useConfig } from '@edata-portal/core';

export function Footer() {
  const config = useConfig();

  const images = config.ui.footer.images;
  return (
    <div
      className="text-primary pt-3 pb-1"
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontSize: 11,
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          gap: '1rem',
        }}
      >
        {images.map((image) => (
          <a
            key={image.src}
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}
            className="text-center"
            href={image.href}
            target="_blank"
            rel="noreferrer"
          >
            <img
              style={{
                height: 70,
                width: 'auto',
              }}
              src={image.src}
              alt={image.alt}
            />
          </a>
        ))}
      </div>
    </div>
  );
}
