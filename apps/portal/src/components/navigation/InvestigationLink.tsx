import { Link } from 'react-router-dom';
/**
 * Link to the investigation page
 */
export function InvestigationLink({
  investigationId,
  title,
}: {
  investigationId: number;
  title: string;
}) {
  return (
    <Link to={`/investigation/` + investigationId + `/datasets`}>
      <span className="text-truncate">{title}</span>
    </Link>
  );
}
