import { Nav, Navbar } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import { HeaderItem } from 'components/navigation/header/HeaderItem';
import { HeaderDropdownItem } from 'components/navigation/header/HeaderDropdownItem';
import { HeaderSearchMenu } from 'components/navigation/header/HeaderSearchMenu';
import { HeaderUserMenu } from 'components/navigation/header/HeaderUserMenu';
import { HeaderSelectionMenu } from 'components/navigation/header/HeaderSelectionMenu';
import { HeaderAdminMenu } from 'components/navigation/header/HeaderAdminMenu';
import { HeaderHelpButton } from 'components/navigation/header/HeaderHelpButton';
import { HeaderReprocessMenu } from 'components/navigation/header/HeaderReprocessMenu';
import { useAuthenticator, useConfig } from '@edata-portal/core';
export function Header() {
  const authenticator = useAuthenticator();

  const config = useConfig();

  const [expanded, setExpanded] = useState(false);

  return (
    <Navbar
      bg="primary"
      variant="dark"
      expand="lg"
      className="p-2"
      expanded={expanded}
    >
      <Navbar.Brand as={NavLink} to="/">
        {config.ui.applicationTitle}
      </Navbar.Brand>
      <Navbar.Toggle onClick={() => setExpanded(expanded ? false : true)} />
      <Navbar.Collapse className="justify-content-between flex-wrap">
        {authenticator.user && (
          <>
            <Nav>
              <HeaderItem
                title="Experiments"
                to="/experiments"
                onClick={() => setExpanded(false)}
                testId="experiments-nav"
              />
              <HeaderItem
                title="Samples"
                to="/samples"
                onClick={() => setExpanded(false)}
                testId="samples-nav"
              />
              <HeaderItem
                title="Publications"
                to="/publications"
                onClick={() => setExpanded(false)}
                testId="publications-nav"
              />
              {authenticator.isAuthenticated && config.ui.features.logistics ? (
                <HeaderDropdownItem
                  title="Logistics"
                  items={[
                    {
                      title: 'Addresses',
                      to: '/addresses',
                    },
                    {
                      title: 'Parcels',
                      to: '/parcels',
                    },
                  ]}
                  onClick={() => setExpanded(false)}
                />
              ) : null}
              {authenticator.user?.isAdministrator ||
              authenticator.user?.isInstrumentScientist ? (
                <HeaderItem
                  title="Beamlines"
                  to="/instruments"
                  onClick={() => setExpanded(false)}
                />
              ) : null}
              <HeaderSelectionMenu onClick={() => setExpanded(false)} />
              <HeaderReprocessMenu onClick={() => setExpanded(false)} />
              <HeaderAdminMenu onClick={() => setExpanded(false)} />
            </Nav>
            <Nav>
              <HeaderSearchMenu onClick={() => setExpanded(false)} />
            </Nav>
            <Nav>
              <HeaderHelpButton onClick={() => setExpanded(false)} />
              <HeaderUserMenu onClick={() => setExpanded(false)} />
            </Nav>
          </>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
}
