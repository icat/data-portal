import { useUser } from '@edata-portal/core';
import { HeaderDropdownItem } from 'components/navigation/header/HeaderDropdownItem';

export function HeaderAdminMenu({ onClick }: { onClick: () => void }) {
  const user = useUser();

  if (!user?.isAdministrator) {
    return null;
  }

  return (
    <HeaderDropdownItem
      title="Manager"
      onClick={onClick}
      items={[
        {
          title: 'Facility',
          to: '/admin/facility/statistics',
        },
        {
          title: 'Users',
          to: '/admin/users',
        },
      ]}
    />
  );
}
