import { Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export function HeaderItem({
  title,
  to,
  onClick,
  target,
  testId,
}: {
  title: JSX.Element | string;
  to?: string;
  onClick: () => void;
  target?: string;
  testId?: string;
}) {
  return (
    <Nav.Link
      {...((to ? { as: NavLink, to: to } : {}) as any)}
      onClick={onClick}
      target={target}
      test-id={testId}
    >
      {title}
    </Nav.Link>
  );
}
