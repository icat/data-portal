import { NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export function HeaderDropdownItem({
  title,
  items,
  onClick,
}: {
  title: JSX.Element | string;
  items: { title: JSX.Element | string; to?: string; onClick?: () => void }[];
  onClick?: () => void;
}) {
  return (
    <NavDropdown title={title}>
      {items.map((item) => (
        <NavDropdown.Item
          key={item.title.toString()}
          // only use NavLink if to is defined
          {...((item.to ? { as: NavLink, to: item.to } : {}) as any)}
          onClick={() => {
            item.onClick && item.onClick();
            onClick && onClick();
          }}
        >
          {item.title}
        </NavDropdown.Item>
      ))}
    </NavDropdown>
  );
}
