import {
  Button,
  useAuthenticator,
  useLocalStorageValue,
} from '@edata-portal/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AffiliationSwitch } from 'components/usermanagement/AffiliationSwitch';
import { Logout } from 'components/usermanagement/Logout';
import { useEffect, useState } from 'react';
import { Nav, NavDropdown, OverlayTrigger, Popover } from 'react-bootstrap';

export function HeaderUserMenu({ onClick }: { onClick: () => void }) {
  const authenticator = useAuthenticator();
  const { user } = authenticator;

  const [showedLoginTooltip, setShowedLoginTooltip] = useLocalStorageValue(
    'icat.showedLoginTooltip',
  );

  const [showLoginTooltip, setShowLoginTooltip] = useState(
    showedLoginTooltip !== 'true',
  );

  useEffect(() => {
    setShowedLoginTooltip('true');
  }, [setShowedLoginTooltip]);

  if (!user) {
    return null;
  }
  if (!authenticator.isAuthenticated) {
    return (
      <OverlayTrigger
        show={showLoginTooltip}
        placement="bottom"
        overlay={
          <Popover>
            <Popover.Body
              style={{
                padding: '1rem',
              }}
            >
              <h1
                className="text-primary"
                style={{
                  fontSize: '1rem',
                  margin: 0,
                }}
              >
                You can log in to access your private data.
              </h1>
              <Button
                style={{
                  padding: 0,
                }}
                variant="link"
                onClick={() => setShowLoginTooltip(false)}
              >
                close
              </Button>
            </Popover.Body>
          </Popover>
        }
        onToggle={() => setShowLoginTooltip(!showLoginTooltip)}
      >
        <Nav.Link
          onClick={() => {
            setShowLoginTooltip(false);
            authenticator.setShowLoginPopup(true);
            onClick();
          }}
          className="text-warning"
          test-id="login-nav"
        >
          <FontAwesomeIcon icon={faUser} className="me-2" />
          log in
        </Nav.Link>
      </OverlayTrigger>
    );
  }

  return (
    <NavDropdown
      align={'end'}
      title={
        <span test-id="user-nav">
          <FontAwesomeIcon icon={faUser} className="me-2" />
          {user.fullName || user.username}
        </span>
      }
    >
      <Logout onClick={onClick} />
      <AffiliationSwitch onClick={onClick} />
    </NavDropdown>
  );
}
