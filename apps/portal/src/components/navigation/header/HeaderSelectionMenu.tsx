import { useSelection } from '@edata-portal/core';
import { HeaderDropdownItem } from 'components/navigation/header/HeaderDropdownItem';
import { HeaderItemBadge } from 'components/navigation/header/HeaderItemBadge';

export function HeaderSelectionMenu({ onClick }: { onClick: () => void }) {
  const { value: selectedDatasets, clearSelection } = useSelection('dataset');

  if (!selectedDatasets.length) {
    return null;
  }

  return (
    <HeaderDropdownItem
      onClick={onClick}
      title={
        <>
          My selection
          <HeaderItemBadge>{selectedDatasets.length}</HeaderItemBadge>
        </>
      }
      items={[
        {
          title: 'Manage',
          to: '/selection',
        },
        {
          title: 'Clear',
          onClick: () => clearSelection(),
        },
      ]}
    />
  );
}
