import { Badge } from 'react-bootstrap';

export function HeaderItemBadge({ children }: { children: React.ReactNode }) {
  return (
    <Badge bg="light" className="text-dark ms-1">
      {children}
    </Badge>
  );
}
