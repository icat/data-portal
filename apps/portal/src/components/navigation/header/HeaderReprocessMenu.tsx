import { useConfig, useUser } from '@edata-portal/core';
import {
  EWOKS_JOB_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { HeaderItem } from 'components/navigation/header/HeaderItem';
import { HeaderItemBadge } from 'components/navigation/header/HeaderItemBadge';
import { Suspense, useMemo } from 'react';
import { Spinner } from 'react-bootstrap';

export function HeaderReprocessMenu({ onClick }: { onClick: () => void }) {
  const user = useUser();
  const config = useConfig();

  if (!config.ui.features.reprocessing || !user?.isAuthenticated) {
    return null;
  }

  return (
    <Suspense fallback={<Spinner size="sm" />}>
      <LoadAndDisplay onClick={onClick} />
    </Suspense>
  );
}

function LoadAndDisplay({ onClick }: { onClick: () => void }) {
  const jobs = useGetEndpoint({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    params: {
      limit: 1,
    },
  });

  const nb = useMemo(() => {
    if (!jobs?.length) {
      return 0;
    }
    return jobs[0].meta.page.total;
  }, [jobs]);

  if (!nb) return null;

  return (
    <HeaderItem
      title={
        <>
          My Jobs <HeaderItemBadge>{nb}</HeaderItemBadge>
        </>
      }
      to="/jobs"
      onClick={onClick}
    />
  );
}
