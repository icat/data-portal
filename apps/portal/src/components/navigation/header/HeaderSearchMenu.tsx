import { Button } from '@edata-portal/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

export function HeaderSearchMenu({ onClick }: { onClick: () => void }) {
  const [search, setSearch] = useState('');

  const url = `/experiments?search=${search}`;

  const navigate = useNavigate();

  return (
    <Form
      onSubmit={(e) => {
        navigate(url);
        setSearch('');
        e.preventDefault();
        e.stopPropagation();
      }}
    >
      <InputGroup size="sm">
        <Form.Control
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          placeholder="Search experiments..."
          size="sm"
        />

        <Button
          variant="light"
          onClick={() => {
            navigate(url);
            setSearch('');
            onClick();
          }}
        >
          <FontAwesomeIcon icon={faSearch} />
        </Button>
      </InputGroup>
    </Form>
  );
}
