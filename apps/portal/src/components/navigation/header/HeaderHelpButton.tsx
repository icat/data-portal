import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { HelpModal } from 'components/help/HelpModal';
import { HeaderItem } from 'components/navigation/header/HeaderItem';
import { useState } from 'react';

export function HeaderHelpButton({ onClick }: { onClick: () => void }) {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <HeaderItem
        title={
          <>
            <FontAwesomeIcon icon={faQuestionCircle} /> Help
          </>
        }
        onClick={() => {
          onClick();
          setShowModal(true);
        }}
      />
      <HelpModal expanded={showModal} setExpanded={setShowModal} />
    </>
  );
}
