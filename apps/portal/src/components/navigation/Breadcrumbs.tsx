import {
  getDatasetName,
  InvestigationLabel,
  useConfig,
} from '@edata-portal/core';
import {
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
  Dataset,
  DATASET_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { Suspense, useMemo } from 'react';
import { Breadcrumb, Container } from 'react-bootstrap';
import { Link, Params, useLocation, useMatches } from 'react-router-dom';

export function Breadcrumbs() {
  let matches = useMatches();

  return (
    <div className="bg-light">
      <Container>
        <Breadcrumb>
          {matches.map((m) => (
            <BreadcrumbItem key={m.id} {...m} />
          ))}
        </Breadcrumb>
      </Container>
      <hr
        style={{
          margin: 0,
        }}
      />
    </div>
  );
}

function BreadcrumbItem({
  id,
  pathname,
  params,
  handle,
}: {
  id: string;
  pathname: string;
  params: Params<string>;
  handle: any;
}) {
  const location = useLocation();
  const isActive = useMemo(() => {
    if (location.pathname === pathname) return true;
    // /a/b is the same as /a/b/
    if (location.pathname.endsWith('/')) {
      if (location.pathname.slice(0, location.pathname.length - 1) === pathname)
        return true;
    }
    if (pathname.endsWith('/')) {
      if (pathname.slice(0, pathname.length - 1) === location.pathname)
        return true;
    }
    return false;
  }, [location, pathname]);

  const breadcrumb = useMemo(() => {
    if (!handle) return undefined;
    if (!('breadcrumb' in handle)) return undefined;
    const breadcrumb = handle.breadcrumb;
    if (typeof breadcrumb === 'string') return breadcrumb;
    if (typeof breadcrumb === 'function') return breadcrumb(params);
    return undefined;
  }, [handle, params]);

  if (!breadcrumb) return null;

  return (
    <Breadcrumb.Item
      linkAs={Link}
      linkProps={{ to: pathname }}
      active={isActive}
    >
      <Suspense fallback={'Loading...'}>{breadcrumb}</Suspense>
    </Breadcrumb.Item>
  );
}

export function InvestigationBreadcrumbItem({
  investigationId,
}: {
  investigationId: string;
}) {
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
    default: [] as Investigation[],
  });

  const investigation = investigations[0] || undefined;

  if (!investigation) return <>{investigationId}</>;
  return <InvestigationLabel investigation={investigation} />;
}

export function DatasetBreadcrumbItem({ datasetId }: { datasetId: string }) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: datasetId,
    },
    default: [] as Dataset[],
  });

  const dataset = datasets[0] || undefined;

  if (!dataset) return <>{datasetId}</>;
  return <>{getDatasetName(dataset)}</>;
}

export function FacilityBreadcrumbItem() {
  const config = useConfig();

  return <>{config.ui.facilityName}</>;
}
