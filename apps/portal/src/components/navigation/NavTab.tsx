import type { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Badge, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export function NavTab({
  to,
  icon,
  label,
  end = false,
  disabled = false,
  badge,
  onClick,
  active,
  testId,
}: {
  to?: string;
  icon?: IconProp;
  label: string | React.ReactNode;
  end?: boolean;
  disabled?: boolean;
  badge?: string;
  onClick?: () => void;
  active?: boolean;
  testId?: string;
}) {
  return (
    <Nav.Item
      style={{
        flexGrow: 1,
      }}
    >
      <Nav.Link
        style={{
          paddingLeft: '1rem',
          display: 'flex',
          alignItems: 'center',
        }}
        {...((to ? { as: NavLink, to } : {}) as any)}
        end={end ? end : undefined}
        disabled={disabled}
        onClick={onClick}
        active={active}
        test-id={testId}
      >
        {icon ? (
          <FontAwesomeIcon
            icon={icon}
            style={{
              marginRight: 5,
            }}
          />
        ) : null}
        {label}
        {badge && (
          <Badge
            bg="secondary"
            style={{
              marginLeft: 5,
            }}
          >
            {badge}
          </Badge>
        )}
      </Nav.Link>
    </Nav.Item>
  );
}
