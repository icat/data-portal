import { Container } from 'react-bootstrap';
import { Footer } from 'components/navigation/Footer';
import { Breadcrumbs } from 'components/navigation/Breadcrumbs';
import { Outlet } from 'react-router-dom';
import { ShowLoginPage } from 'components/usermanagement/Login';
import { Header } from 'components/navigation/header/Header';
import InformationMessage from 'components/messages/InformationMessages';
import { Suspense } from 'react';
import { Loading, SideNavRenderer } from '@edata-portal/core';

export function Navigation() {
  return (
    <div
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
      }}
    >
      <Header />
      <Suspense fallback={<Loading />}>
        <InformationMessage />
      </Suspense>
      <Breadcrumbs />
      <Container
        style={{
          paddingTop: 20,
          overflow: 'auto',
          height: '100%',
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
        fluid
        className="main"
      >
        <ShowLoginPage>
          <SideNavRenderer>
            <Outlet />
            <Footer />
          </SideNavRenderer>
        </ShowLoginPage>
      </Container>
    </div>
  );
}
