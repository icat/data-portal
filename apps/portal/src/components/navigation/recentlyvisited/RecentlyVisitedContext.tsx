import { useUserPreferences } from '@edata-portal/core';
import {
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
  RecentlyVisitedObject,
  RecentlyVisitedTarget,
} from '@edata-portal/icat-plus-api';
import { HOMEPAGE_SHORTCUT_TABLE_LIMIT } from 'components/homepage/HomePageShortcutTable';

import { useCallback, useEffect, useMemo } from 'react';
import { useLocation } from 'react-router-dom';

export function useRecentlyVisited() {
  const [recentlyVisited, setRecentlyVisited] = useUserPreferences(
    'recentlyVisited',
    [],
  );

  const addRecentlyVisited = useCallback(
    (value: RecentlyVisitedObject) => {
      const newRecentlyVisited = [value, ...recentlyVisited]
        .filter(
          (item, index, self) =>
            index === self.findIndex((t) => t.url === item.url),
        )
        .slice(0, HOMEPAGE_SHORTCUT_TABLE_LIMIT);
      if (recentlyVisitedListHasChanged(recentlyVisited, newRecentlyVisited)) {
        setRecentlyVisited(newRecentlyVisited);
      }
    },
    [recentlyVisited, setRecentlyVisited],
  );

  return useMemo(
    () => ({
      recentlyVisited,
      addRecentlyVisited,
    }),
    [recentlyVisited, addRecentlyVisited],
  );
}

function recentlyVisitedListHasChanged(
  recentlyVisited: RecentlyVisitedObject[],
  newRecentlyVisited: RecentlyVisitedObject[],
) {
  return (
    JSON.stringify(recentlyVisited.map((v) => v.url)) !==
    JSON.stringify(newRecentlyVisited.map((v) => v.url))
  );
}

export function useRegisterRecentlyVisited(
  label: string,
  target: RecentlyVisitedTarget,
) {
  const { pathname } = useLocation();

  const recentlyVisited = useRecentlyVisited();

  useEffect(() => {
    recentlyVisited.addRecentlyVisited({ url: pathname, label, target });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
}

export function useRegisterInvestigationRecentlyVisited(
  investigationId: string,
  label: string,
) {
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
  });

  const investigation = useMemo(() => {
    if (!investigations?.length) return;
    return investigations[0];
  }, [investigations]);

  useRegisterRecentlyVisited(label, {
    investigationName: investigation?.name,
    investigationStartDate: investigation?.startDate,
    investigationEndDate: investigation?.endDate,
    instrumentName: investigation?.instrument?.name,
  });
}
