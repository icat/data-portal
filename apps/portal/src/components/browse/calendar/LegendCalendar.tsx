import { faDatabase, faFileText } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Row } from 'react-bootstrap';

export function HasDataInvestigationIcon() {
  return <FontAwesomeIcon icon={faDatabase} className={'text-info me-1'} />;
}

export function ScheduledInvestigationIcon() {
  return <FontAwesomeIcon icon={faFileText} className={'text-success me-1'} />;
}

export function NotScheduledInvestigationIcon() {
  return <FontAwesomeIcon icon={faFileText} className={'text-warning me-1'} />;
}

export function LegendCalendar() {
  return (
    <Row className="p-1">
      <Col xs={12}>
        <strong>Legend:</strong>
      </Col>
      <Col xs={'auto'}>
        <i>
          <ScheduledInvestigationIcon />
          Scheduled
        </i>
      </Col>
      <Col xs={'auto'}>
        <i>
          <NotScheduledInvestigationIcon />
          Not scheduled
        </i>
      </Col>
      <Col xs={'auto'}>
        <i>
          <HasDataInvestigationIcon />
          Has data
        </i>
      </Col>
    </Row>
  );
}
