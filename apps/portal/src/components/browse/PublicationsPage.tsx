import {
  Loading,
  SideNavElement,
  useParam,
  useUser,
  WithSideNav,
} from '@edata-portal/core';
import { BespokeDOIsPage } from 'components/browse/BespokeDOIsPage';
import { DocumentsPage } from 'components/browse/DocumentsPage';
import ReservedDOIPage from 'components/browse/ReservedDOIPage';
import { NavTab } from 'components/navigation/NavTab';
import NotFound from 'components/routing/NotFound';
import { Suspense, useMemo } from 'react';
import { Nav } from 'react-bootstrap';

export default function PublicationsPage() {
  const [type, setType] = useParam<'data' | 'documents' | 'reserved'>(
    'type',
    'data',
    true,
    true,
  );
  const user = useUser();

  const nav = (
    <Nav
      variant="pills"
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 5,
      }}
    >
      <NavTab
        label="Data"
        onClick={() => setType('data')}
        active={type === 'data'}
      />
      <NavTab
        label={`Documents`}
        onClick={() => setType('documents')}
        active={type === 'documents'}
      />
      {user?.isAuthenticated && (
        <NavTab
          label={`Reserved`}
          onClick={() => setType('reserved')}
          active={type === 'reserved'}
        />
      )}
    </Nav>
  );

  const element = useMemo(() => {
    if (type === 'data') return <BespokeDOIsPage />;
    if (type === 'documents') return <DocumentsPage />;
    if (type === 'reserved') return <ReservedDOIPage />;
    return <NotFound />;
  }, [type]);

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label={'Publication type'}>{nav}</SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>{element}</Suspense>
    </WithSideNav>
  );
}
