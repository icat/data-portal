import { faFileText } from '@fortawesome/free-solid-svg-icons';
import { OverlayLink } from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';

export function InvestigationLogbookShortcut({
  investigation,
}: {
  investigation: Investigation;
}) {
  return (
    <OverlayLink
      link={`/investigation/${investigation.id}/logbook`}
      icon={faFileText}
      tooltipText={'Open logbook'}
    ></OverlayLink>
  );
}
