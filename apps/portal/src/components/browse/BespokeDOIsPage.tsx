import { DataCollectionsList } from 'components/browse/DataCollectionsList';

export function BespokeDOIsPage() {
  return <DataCollectionsList type={'datacollection'} />;
}
