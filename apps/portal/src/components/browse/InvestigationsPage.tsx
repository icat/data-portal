import {
  useParam,
  Loading,
  useUser,
  SearchBar,
  UI_DATE_DAY_FORMAT,
  SideNavElement,
  WithSideNav,
  useBreakpointValue,
  SideNavFilter,
  formatDateToIcatDate,
  addToDate,
  parseDate,
} from '@edata-portal/core';
import {
  Role,
  useGetEndpoint,
  INSTRUMENT_LIST_ENDPOINT,
  USER_INSTRUMENT_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { InvestigationCards } from 'components/browse/InvestigationCards';
import { InvestigationTable } from 'components/browse/InvestigationTable';
import { Suspense, useEffect, useMemo } from 'react';
import { InvestigationCalendar } from 'components/browse/Calendar';
import { Col, Row, Button } from 'react-bootstrap';
import ReactSelect from 'react-select';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { SelectUsers } from 'components/usermanagement/SelectUsers';

export default function InvestigationsPage({
  instrumentName,
}: {
  instrumentName?: string;
}) {
  /** this returns the list of all instruments */
  const allInstrumentsList = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
    skipFetch: !!instrumentName,
  });
  const instruments = useMemo(() => {
    if (!allInstrumentsList) return [];
    const uniqueInstrumentList = allInstrumentsList.filter(
      (v, i, a) => a.findIndex((t) => t.name === v.name) === i,
    );
    return uniqueInstrumentList.sort((a, b) => a.name.localeCompare(b.name));
  }, [allInstrumentsList]);

  /** this is the list of instruments which I have an investigation attached to or I am instrumentScientist */
  const userInstruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    skipFetch: !!instrumentName,
  });

  const user = useUser();

  const [search, setSearch] = useParam<string>('search', '');
  const [instrument, setInstrument] = useParam<string>('instrument', 'any');
  const [filter, setFilter] = useParam<Role | 'any'>('filter', 'any');
  const [startDate, setStartDate] = useParam<string>('startDate', 'undefined');
  const [endDate, setEndDate] = useParam<string>(
    'endDate',
    formatDateToIcatDate(
      addToDate(new Date(), {
        days: 2,
      }),
    ) || '',
  );
  const [username, setUsernmae] = useParam<string>('username', '');

  const filters: Role[] = user?.isAuthenticated
    ? ['released', 'embargoed', 'participant']
    : ['released', 'embargoed'];
  const filtersDisplay: string[] = user?.isAuthenticated
    ? ['Public data', 'Embargoed data', 'My data']
    : ['Public data', 'Embargoed data'];

  useEffect(() => {
    // if the user is not logged in, we can't show their data
    if (!user?.isAuthenticated) {
      if (filter === 'participant') {
        setFilter('any');
      }
    }
  }, [user, filter, setFilter]);

  const views = ['List', 'Calendar'];
  const viewClassNames = ['d-none d-lg-block', 'd-block', 'd-block'];
  const [view, setView] = useParam<string>('view', 'List');

  const showAsCards = useBreakpointValue({ xs: true, lg: false });

  const showUsersFilter = user?.isAdministrator || user?.isInstrumentScientist;

  const nav = (
    <SideNavElement label="Experiment sessions">
      <Col>
        <SideNavFilter label={'Search'}>
          <SearchBar
            placeholder="name, title, abstract, DOI..."
            value={search}
            onUpdate={(v) => {
              setSearch(v);
            }}
            testId="search-investigations"
          />
        </SideNavFilter>
        <SideNavFilter label={'Filter by'}>
          <Row className="g-1">
            {filters.map((filterValue, index) => (
              <Col xs={'auto'} key={filterValue}>
                <Button
                  size="sm"
                  variant="outline-primary"
                  active={filter === filterValue}
                  onClick={() => {
                    setFilter(filter === filterValue ? 'any' : filterValue);
                  }}
                >
                  {filtersDisplay[index]}
                </Button>
              </Col>
            ))}
          </Row>
        </SideNavFilter>

        {instrumentName ? null : (
          <SideNavFilter label={'Beamline'}>
            <>
              <ReactSelect
                isClearable
                value={{ value: instrument, label: instrument }}
                onChange={(v) => {
                  setInstrument(v?.value || 'any');
                }}
                options={instruments?.map((v) => {
                  return { value: v.name, label: v.name };
                })}
              />
              <Row className="g-1 mt-1">
                {userInstruments?.slice(0, 8).map((v) => (
                  <Col key={v.id} xs={'auto'} className="g-1">
                    <Button
                      style={{
                        padding: '0px 2px',
                      }}
                      onClick={() => {
                        setInstrument(v.name);
                      }}
                      size="sm"
                      variant="outline-primary"
                      active={instrument === v.name}
                    >
                      <small>{v.name}</small>
                    </Button>
                  </Col>
                ))}
              </Row>
            </>
          </SideNavFilter>
        )}

        <SideNavFilter label={'Start date'}>
          <DatePicker
            className="form-control"
            dateFormat={UI_DATE_DAY_FORMAT}
            selected={parseDate(startDate)}
            maxDate={parseDate(endDate)}
            onChange={(date: any) => {
              const strDate = formatDateToIcatDate(date) || 'undefined';
              setStartDate(strDate);
            }}
            isClearable
            placeholderText="Start date"
          />
        </SideNavFilter>

        <SideNavFilter label={'End date'}>
          <DatePicker
            className="form-control"
            dateFormat={UI_DATE_DAY_FORMAT}
            selected={parseDate(endDate)}
            minDate={parseDate(startDate)}
            onChange={(date: any) => {
              const strDate = formatDateToIcatDate(date) || 'undefined';
              setEndDate(strDate);
            }}
            isClearable
            placeholderText="End date"
            disabled={view === 'Calendar'}
          />
        </SideNavFilter>

        {showUsersFilter && (
          <SideNavFilter label={'User'}>
            <SelectUsers
              selectedUsers={[username]}
              onSelect={(users) => {
                if (users) {
                  setUsernmae(users[0].name);
                } else {
                  setUsernmae('');
                }
              }}
              placeholder="Select a user..."
              isMulti={false}
            />
          </SideNavFilter>
        )}
      </Col>
    </SideNavElement>
  );

  const instrumentNameFilterValue = useMemo(
    () => instrumentName ?? (instrument === 'any' ? undefined : instrument),
    [instrument, instrumentName],
  );
  const startDateFilterValue = useMemo(
    () => (startDate === 'undefined' ? undefined : startDate),
    [startDate],
  );
  const endDateFilterValue = useMemo(
    () => (endDate === 'undefined' ? undefined : endDate),
    [endDate],
  );
  const roleFilterValue = useMemo(
    () => (filter === 'any' ? undefined : filter),
    [filter],
  );
  const usernameFilterValue = useMemo(
    () => (username === 'undefined' ? undefined : username),
    [username],
  );

  return (
    <WithSideNav sideNav={nav}>
      <Col>
        <Row style={{ marginBottom: '1rem' }} className="g-1">
          {views.map((v, i) => (
            <Col xs={'auto'} key={v} className={viewClassNames[i]}>
              <Button
                size="sm"
                variant="outline-primary"
                active={v === view}
                onClick={() => {
                  setView(v);
                }}
              >
                {v}
              </Button>
            </Col>
          ))}
        </Row>
        <hr />
        <Row>
          {view === 'List' && (
            <>
              {showAsCards ? (
                <Suspense fallback={<Loading />}>
                  <InvestigationCards
                    search={search}
                    filter={roleFilterValue}
                    instrumentName={instrumentNameFilterValue}
                    startDate={startDateFilterValue}
                    endDate={endDateFilterValue}
                  />
                </Suspense>
              ) : (
                <Suspense fallback={<Loading />}>
                  <InvestigationTable
                    search={search}
                    filter={roleFilterValue}
                    instrumentName={instrumentNameFilterValue}
                    startDate={startDateFilterValue}
                    endDate={endDateFilterValue}
                    username={usernameFilterValue}
                  />
                </Suspense>
              )}
            </>
          )}
          {view === 'Calendar' && (
            <Suspense fallback={<Loading />}>
              <InvestigationCalendar
                search={search}
                filter={roleFilterValue}
                instrumentName={instrumentNameFilterValue}
                startDate={startDateFilterValue}
                endDate={endDateFilterValue}
                setStartDate={setStartDate}
                setEndDate={setEndDate}
                username={usernameFilterValue}
              />
            </Suspense>
          )}
        </Row>
      </Col>
    </WithSideNav>
  );
}
