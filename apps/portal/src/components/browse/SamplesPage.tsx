import {
  DATASET_COUNT,
  DatasetStatistics,
  InvestigationOpenBtn,
  Loading,
  SearchBar,
  SelectInstruments,
  SideNavElement,
  SideNavFilter,
  TanstackBootstrapTable,
  WithSideNav,
  formatDateToDay,
  useEndpointPagination,
  useParam,
} from '@edata-portal/core';
import {
  INSTRUMENT_LIST_ENDPOINT,
  Parameter,
  SAMPLE_LIST_ENDPOINT,
  useGetEndpoint,
  type Instrument,
} from '@edata-portal/icat-plus-api';
import {
  getCoreRowModel,
  getFilteredRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { Suspense, useCallback, useMemo } from 'react';
import { Col } from 'react-bootstrap';

export default function SamplesPage() {
  const [instrumentsStr, setInstrumentsStr] = useParam<string>(
    'instruments',
    '',
  );
  const [search, setSearch] = useParam<string>('search', '');

  const allInstruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
    default: [] as Instrument[],
  });

  const allInstrumentsStr = useMemo(
    () => allInstruments.map((i) => i.name).join(','),
    [allInstruments],
  );

  const instruments = useMemo(() => {
    return instrumentsStr
      .split(',')
      .map((v) => v.trim())
      .filter((v) => v?.length);
  }, [instrumentsStr]);

  const setInstruments = useCallback(
    (v: Instrument[]) => {
      setInstrumentsStr(v.map((i) => i.name).join(','));
    },
    [setInstrumentsStr],
  );

  const nav = (
    <SideNavElement label="Samples">
      <Col>
        <SideNavFilter label={'Search'}>
          <SearchBar
            placeholder="name, molecule, organ..."
            value={search}
            onUpdate={(v) => {
              setSearch(v);
            }}
            testId="search-investigations"
          />
        </SideNavFilter>
        <SideNavFilter label={'Beamlines'}>
          <SelectInstruments
            selectedInstruments={instruments}
            onChange={setInstruments}
            forUser={false}
          />
        </SideNavFilter>
      </Col>
    </SideNavElement>
  );

  return (
    <WithSideNav sideNav={nav}>
      <Suspense fallback={<Loading />}>
        <LoadAndDisplay
          instruments={instrumentsStr || allInstrumentsStr}
          search={search}
        />
      </Suspense>
    </WithSideNav>
  );
}

function LoadAndDisplay({
  search,
  instruments,
}: {
  search: string;
  instruments: string;
}) {
  const samples = useEndpointPagination({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: {
      ...(search ? { search: search } : {}),
      ...(instruments ? { instrumentName: instruments } : {}),
      includeDatasets: false,
      sortBy: 'DATE',
      sortOrder: -1,
    },
    paginationParams: {
      paginationKey: 'samples',
    },
  });

  const table = useReactTable({
    data: samples.data,
    columns: [
      {
        accessorKey: 'name',
        header: 'Sample name',
        footer: 'Sample name',
        enableColumnFilter: false,
        cell: (row) => {
          return (
            <InvestigationOpenBtn
              investigation={row.row.original.investigation}
              sampleId={row.row.original.id}
              title={row.getValue()}
            />
          );
        },
      },
      {
        accessorKey: 'investigation.name',
        header: 'Exp. session',
        footer: 'Exp. session',
        enableColumnFilter: false,
      },
      {
        accessorKey: 'investigation.instrument.name',
        header: 'Beamline',
        footer: 'Beamline',
        enableColumnFilter: false,
      },
      {
        accessorFn: (row) => formatDateToDay(row.investigation.startDate),
        id: 'inv date',
        header: 'Exp. start',
        footer: 'Exp. start',
        enableColumnFilter: false,
      },
      {
        accessorFn: (row) => row,
        id: 'dataset',
        header: 'Datasets',
        footer: 'Datasets',
        enableColumnFilter: false,
        cell: (row) => (
          <DatasetStatistics
            label={'dataset'}
            parameters={Object.fromEntries(
              row
                .getValue()
                .parameters.map((p: Parameter) => [p.name, p.value]),
            )}
            parameterCaption={DATASET_COUNT}
          />
        ),
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    ...samples.tablePagination,
  });
  return (
    <TanstackBootstrapTable
      dataLength={samples.totalElements}
      table={table}
      bordered
    />
  );
}
