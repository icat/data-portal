import type { ColumnDef } from '@tanstack/react-table';
import {
  getCoreRowModel,
  getFilteredRowModel,
  useReactTable,
} from '@tanstack/react-table';
import {
  TanstackBootstrapTable,
  useUser,
  DatasetStatistics,
  NoData,
  InvestigationDate,
  DATASET_COUNT,
  SAMPLE_COUNT,
  FilesStatistics,
  useConfig,
  UserPortalButton,
  useEndpointPagination,
  InvestigationOpenBtn,
} from '@edata-portal/core';
import {
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
  Role,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';
import { Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import {
  InvestigationTitle,
  InvestigationReleaseDate,
} from 'components/browse/Investigation';
import { DOIBadge } from '@edata-portal/doi';
import { InvestigationLogbookShortcut } from 'components/browse/InvestigationLogbookShortcut';

export function InvestigationTable({
  search,
  filter,
  instrumentName,
  startDate,
  endDate,
  withHasAccess = true,
  username,
}: {
  search?: string;
  filter?: Role;
  instrumentName?: string;
  startDate?: string;
  endDate?: string;
  withHasAccess?: boolean;
  username?: string;
}) {
  const user = useUser();

  const config = useConfig();

  const investigations = useEndpointPagination({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      sortBy: 'STARTDATE',
      sortOrder: -1,
      ...(filter ? { filter: filter } : {}),
      ...(search?.trim().length ? { search: search.trim() } : {}),
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
      ...(startDate ? { startDate: startDate } : {}),
      ...(endDate ? { endDate: endDate } : {}),
      ...(username ? { username: username } : {}),
      withHasAccess: withHasAccess,
    },
    paginationParams: {
      paginationKey: 'investigations',
    },
  });

  const anyCanAccessDatasets = useMemo(
    () => investigations.data.some((i) => i.canAccessDatasets),
    [investigations],
  );

  const columns: ColumnDef<Investigation>[] = [
    {
      accessorFn: (row) => row,
      id: 'open',
      cell: (info) => (
        <InvestigationOpenBtn
          investigation={info.getValue() as Investigation}
        />
      ),
      header: '',
      enableColumnFilter: false,
    },
    {
      accessorKey: 'instrument.name',
      header: 'Beamline',
      footer: 'Beamline',
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Start',
      footer: 'Start',
      cell: (info) => (
        <InvestigationDate investigation={info.getValue() as Investigation} />
      ),
      enableColumnFilter: false,
    },
    {
      accessorFn: (i) => (i.title.length < 10 ? i.summary : i.title),
      cell: (i) => <InvestigationTitle investigation={i.row.original} />,
      header: 'Title',
      footer: 'Title',
      enableColumnFilter: false,
    },
    ...(user?.isAuthenticated
      ? [
          {
            accessorFn: (row: any) => row,
            header: 'A-Form',
            footer: 'A-Form',
            cell: (info: any) => (
              <UserPortalButton
                investigation={info.getValue() as Investigation}
              />
            ),
            enableColumnFilter: false,
          },
        ]
      : []),
    ...(anyCanAccessDatasets
      ? [
          {
            accessorFn: (row: any) => row,
            header: 'Samples',
            footer: 'Samples',
            enableColumnFilter: false,
            cell: (info: any) =>
              (info.getValue() as Investigation).canAccessDatasets ? (
                <small>
                  {String(
                    (info.getValue() as Investigation).parameters[
                      SAMPLE_COUNT
                    ] || '',
                  )}
                </small>
              ) : null,
          },
          {
            accessorFn: (row: any) => row,
            header: 'Datasets',
            footer: 'Datasets',
            cell: (info: any) =>
              (info.getValue() as Investigation).canAccessDatasets ? (
                <DatasetStatistics
                  label="Datasets"
                  parameterCaption={DATASET_COUNT}
                  parameters={(info.getValue() as Investigation).parameters}
                />
              ) : null,
            enableColumnFilter: false,
          },
        ]
      : []),
    ...(user?.isAdministrator
      ? [
          {
            accessorFn: (row: any) => row,
            header: 'Files',
            footer: 'Files',
            cell: (info: any) => (
              <FilesStatistics
                parameters={(info.getValue() as Investigation).parameters}
              />
            ),
            enableColumnFilter: false,
          },
        ]
      : []),
    {
      accessorFn: (row) => row,
      header: 'Release',
      footer: 'Release',
      cell: (info) => (
        <div style={{ fontSize: '0.9em' }}>
          <InvestigationReleaseDate
            investigation={info.getValue() as Investigation}
          />{' '}
        </div>
      ),
      enableColumnFilter: false,
    },
    {
      accessorKey: 'doi',
      cell: (v) => (
        <span className="text-nowrap" onClick={(e) => e.stopPropagation()}>
          <DOIBadge doi={v.getValue() as string} />
        </span>
      ),
      header: 'DOI',
      footer: 'DOI',
      enableColumnFilter: false,
    },
    ...(anyCanAccessDatasets && config.ui.features.logbook
      ? [
          {
            accessorFn: (row: any) => row,
            cell: (info: any) =>
              (info.getValue() as Investigation).canAccessDatasets ? (
                <InvestigationLogbookShortcut
                  investigation={info.getValue() as Investigation}
                />
              ) : null,
            header: 'Logbook',
            footer: 'Logbook',
            enableColumnFilter: false,
          },
        ]
      : []),
  ];

  const navigate = useNavigate();
  const table = useReactTable<Investigation>({
    data: investigations.data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    ...investigations.tablePagination,
  });

  if (!investigations.data.length) {
    return (
      <Col>
        <NoData />
      </Col>
    );
  }

  return (
    <div style={{ fontSize: '0.9em' }}>
      <TanstackBootstrapTable
        dataLength={investigations.totalElements}
        table={table}
        bordered={true}
        onRowClick={(row) => {
          if (row.canAccessDatasets) {
            navigate(`/investigation/${row.id}/datasets`);
          } else if (row.doi?.length) {
            window.open(config.ui.doi.link + row.doi, '_blank');
          } else {
            navigate(`/investigation/${row.id}`);
          }
        }}
        testId="investigation-table"
      />
    </div>
  );
}
