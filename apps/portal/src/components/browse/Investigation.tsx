import { faFileText } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatDateToDay, parseDate, DATASET_COUNT } from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { OverlayTrigger, Popover } from 'react-bootstrap';

import 'components/browse/investigation.css';

export function InvestigationTitle({
  investigation,
  showName,
}: {
  investigation: Investigation;
  showName?: boolean;
}) {
  const nbDatasets = Number(investigation.parameters[DATASET_COUNT] || 0) || 0;
  const hasData = nbDatasets > 0;

  const overlay = (
    <Popover style={{ minWidth: 500 }}>
      <Popover.Header>Abstract</Popover.Header>
      <Popover.Body>{investigation.summary}</Popover.Body>
    </Popover>
  );
  return (
    <div className={hasData ? '' : 'text-muted'}>
      {showName ? (
        <>
          <strong
            style={{
              textDecoration: 'underline',
            }}
          >
            {investigation.name}
          </strong>{' '}
        </>
      ) : null}
      {investigation.title}
      {investigation.summary ? (
        <OverlayTrigger overlay={overlay} placement="auto">
          <FontAwesomeIcon
            className="text-info"
            icon={faFileText}
            style={{ marginLeft: '0.5rem' }}
          />
        </OverlayTrigger>
      ) : null}
    </div>
  );
}

export function InvestigationReleaseDate({
  investigation,
}: {
  investigation: Investigation;
}) {
  if (!investigation.releaseDate) return null;

  const date = parseDate(investigation.releaseDate);

  if (!date) return null;

  const isReleased = date < new Date();

  return (
    <div
      className={isReleased ? '' : 'text-muted'}
      style={{ whiteSpace: 'nowrap' }}
    >
      {formatDateToDay(investigation.releaseDate)}
    </div>
  );
}
