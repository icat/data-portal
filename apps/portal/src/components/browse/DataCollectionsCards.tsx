import {
  DATA_COLLECTION_PARAM_INSTRUMENT_NAMES,
  DATA_COLLECTION_PARAM_INVESTIGATION_NAMES,
  DATA_COLLECTION_PARAM_TITLE,
  getDataCollectionParam,
  useEndpointPagination,
  PaginationMenu,
  MetadataTable,
  InvestigationNameButton,
} from '@edata-portal/core';
import {
  DataCollectionType,
  DataCollection,
  DATA_COLLECTION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { DOIBadge } from '@edata-portal/doi';
import { Card, Col, Row } from 'react-bootstrap';

export function DataCollectionsCards({
  type,
  search,
  openCollectionLink,
}: {
  type: DataCollectionType;
  search: string;
  openCollectionLink: (dataCollection: DataCollection) => string;
}) {
  const collections = useEndpointPagination({
    endpoint: DATA_COLLECTION_LIST_ENDPOINT,
    params: {
      type,
      sortBy: 'DATE',
      sortOrder: -1,
      ...(search?.length ? { search } : {}),
    },
    paginationParams: {
      paginationKey: 'collections',
    },
  });

  return (
    <Col>
      <PaginationMenu {...collections} />
      <Row className="g-2">
        {collections.data.map((dataCollection) => (
          <Col xs={12} key={dataCollection.id}>
            <DataCollectionCard
              dataCollection={dataCollection}
              openCollectionLink={openCollectionLink(dataCollection)}
            />
          </Col>
        ))}
      </Row>
      <PaginationMenu {...collections} />
    </Col>
  );
}

export function DataCollectionCard({
  dataCollection,
  openCollectionLink,
}: {
  dataCollection: DataCollection;
  openCollectionLink: string;
}) {
  const investigation = getDataCollectionParam(
    dataCollection,
    DATA_COLLECTION_PARAM_INVESTIGATION_NAMES,
  );

  const instrument = getDataCollectionParam(
    dataCollection,
    DATA_COLLECTION_PARAM_INSTRUMENT_NAMES,
  );

  const title = getDataCollectionParam(
    dataCollection,
    DATA_COLLECTION_PARAM_TITLE,
  );

  const nbDatasets = dataCollection.dataCollectionDatasets?.length || 0;

  const doi = dataCollection.doi;

  return (
    <Card>
      <Card.Header className="p-1">
        {investigation && (
          <InvestigationNameButton
            onClick={(e) => e.stopPropagation()}
            link={openCollectionLink}
            title={investigation}
          />
        )}
      </Card.Header>
      <Card.Body className="p-1">
        <Row>
          <Col>
            <strong>{title}</strong>
          </Col>
        </Row>
        <Row>
          <MetadataTable
            parameters={[
              { caption: 'Beamline', value: instrument },
              {
                caption: 'Datasets',
                value: nbDatasets.toString(),
              },
              {
                caption: 'DOI',
                component: <DOIBadge doi={doi} />,
              },
            ]}
          />
        </Row>
      </Card.Body>
    </Card>
  );
}
