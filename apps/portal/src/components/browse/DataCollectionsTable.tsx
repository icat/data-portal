import {
  DATA_COLLECTION_PARAM_INSTRUMENT_NAMES,
  DATA_COLLECTION_PARAM_INVESTIGATION_NAMES,
  DATA_COLLECTION_PARAM_TITLE,
  getDataCollectionParam,
  InvestigationNameButton,
  TanstackBootstrapTable,
  useEndpointPagination,
} from '@edata-portal/core';
import {
  DataCollectionType,
  DataCollection,
  DATA_COLLECTION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { DOIBadge } from '@edata-portal/doi';
import { getCoreRowModel, useReactTable } from '@tanstack/react-table';
import { useNavigate } from 'react-router-dom';

export function DataCollectionsTable({
  type,
  search,
  openCollectionLink,
}: {
  type: DataCollectionType;
  search: string;
  openCollectionLink: (dataCollection: DataCollection) => string;
}) {
  const navigate = useNavigate();

  const collections = useEndpointPagination({
    endpoint: DATA_COLLECTION_LIST_ENDPOINT,
    params: {
      type,
      sortBy: 'DATE',
      sortOrder: -1,
      ...(search?.length ? { search } : {}),
    },
    paginationParams: {
      paginationKey: 'collections',
    },
  });

  const table = useReactTable({
    data: collections.data,
    columns: [
      {
        header: 'Proposal',
        footer: 'Proposal',
        accessorFn: (v) =>
          getDataCollectionParam(v, DATA_COLLECTION_PARAM_INVESTIGATION_NAMES),
        cell: (v) => (
          <InvestigationNameButton
            onClick={(e) => e.stopPropagation()}
            link={openCollectionLink(v.row.original)}
            title={v.getValue()}
          />
        ),
        enableColumnFilter: false,
      },
      {
        header: 'Beamline',
        footer: 'Beamline',
        accessorFn: (v) =>
          getDataCollectionParam(v, DATA_COLLECTION_PARAM_INSTRUMENT_NAMES),
        enableColumnFilter: false,
      },
      {
        header: 'Title',
        footer: 'Title',
        accessorFn: (v) =>
          getDataCollectionParam(v, DATA_COLLECTION_PARAM_TITLE),
        enableColumnFilter: false,
      },
      {
        header: 'Datasets',
        footer: 'Datasets',
        accessorFn: (v) => v.dataCollectionDatasets?.length,
        enableColumnFilter: false,
      },
      {
        header: 'DOI',
        footer: 'DOI',
        accessorFn: (v) => v.doi,
        cell: (v) => (
          <span className="text-nowrap" onClick={(e) => e.stopPropagation()}>
            <DOIBadge doi={v.getValue()} />
          </span>
        ),
        enableColumnFilter: false,
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    ...collections.tablePagination,
  });

  return (
    <TanstackBootstrapTable
      table={table}
      dataLength={collections.totalElements}
      onRowClick={(row) => {
        navigate(openCollectionLink(row));
      }}
    />
  );
}
