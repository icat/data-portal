import { DataCollectionsList } from 'components/browse/DataCollectionsList';

export function DocumentsPage() {
  return <DataCollectionsList type={'document'} />;
}
