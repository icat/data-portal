import {
  buildDOIFromDataCollectionId,
  Button,
  useConfig,
  useViewers,
} from '@edata-portal/core';
import { DOIBadge } from '@edata-portal/doi';
import {
  DataCollection,
  Dataset,
  DATASET_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { Card, Col, Row } from 'react-bootstrap';

export default function ReservedDOICard({
  dataCollection,
  selected,
  onMint,
}: {
  dataCollection: DataCollection;
  selected: boolean;
  onMint: (dataCollection: DataCollection) => void;
}) {
  const viewers = useViewers();
  const config = useConfig();

  const datasetIds = dataCollection.dataCollectionDatasets.map(
    (dataCollectionDataset) => dataCollectionDataset.dataset.id,
  );
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      datasetIds: datasetIds.join(','),
    },
    skipFetch: datasetIds.length === 0,
    default: [] as Dataset[],
  });

  const doi = buildDOIFromDataCollectionId(
    dataCollection.id,
    config.ui.doi.facilityPrefix,
    config.ui.doi.facilitySuffix,
  );

  const border = selected ? 'primary' : undefined;

  return (
    <Card border={border}>
      <Card.Header>
        <DOIBadge doi={doi} reserved={true} />
      </Card.Header>
      <Card.Body>
        {datasets.map((dataset) => {
          return (
            <Row key={dataset.id} className="mb-4">
              <Col>{viewers.viewDataset(dataset, 'details')}</Col>
            </Row>
          );
        })}
      </Card.Body>
      <Card.Footer>
        <Button onClick={() => onMint(dataCollection)}>Mint DOI</Button>
      </Card.Footer>
    </Card>
  );
}
