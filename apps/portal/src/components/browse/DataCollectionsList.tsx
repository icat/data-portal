import {
  SideNavElement,
  SearchBar,
  useBreakpointValue,
  WithSideNav,
  SideNavFilter,
} from '@edata-portal/core';
import type {
  DataCollection,
  DataCollectionType,
} from '@edata-portal/icat-plus-api';
import { DataCollectionsCards } from 'components/browse/DataCollectionsCards';
import { DataCollectionsTable } from 'components/browse/DataCollectionsTable';

import { useState } from 'react';

export function DataCollectionsList({ type }: { type: DataCollectionType }) {
  const [search, setSearch] = useState<string>('');

  const nav = (
    <SideNavElement label={type === 'datacollection' ? 'Data' : `Documents`}>
      <SideNavFilter label={'Search'}>
        <SearchBar
          onUpdate={(value) => setSearch(value || '')}
          value={search}
          placeholder="Title, beamline, DOI..."
        />
      </SideNavFilter>
    </SideNavElement>
  );

  const showAsCards = useBreakpointValue({ xs: true, lg: false });

  return (
    <WithSideNav sideNav={nav}>
      {showAsCards ? (
        <DataCollectionsCards
          type={type}
          search={search}
          openCollectionLink={getLinkForDatacollection}
        />
      ) : (
        <DataCollectionsTable
          type={type}
          search={search}
          openCollectionLink={getLinkForDatacollection}
        />
      )}
    </WithSideNav>
  );
}

function getLinkForDatacollection(datacollection: DataCollection) {
  return `/doi/${datacollection.doi}/datasets`;
}
