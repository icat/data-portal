import {
  GET_RESERVED_DOIS_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';

import { SplitPage } from '@edata-portal/core';
import { DataCollection } from '@edata-portal/icat-plus-api';
import ReservedDOICard from 'components/browse/ReservedDOICard';
import { MintDOIPanel } from 'components/mint/MintDOIPanel';
import { useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';

export default function ReservedDOIPage() {
  const collections = useGetEndpoint({
    endpoint: GET_RESERVED_DOIS_ENDPOINT,
  });

  const [selectedDataCollection, setSelectedDataCollection] =
    useState<DataCollection>();

  const rightPanel = selectedDataCollection ? (
    <Container fluid className="mt-2 mb-2 overflow-auto">
      <MintDOIPanel dataCollection={selectedDataCollection} />
    </Container>
  ) : (
    <></>
  );

  const onMint = (dataCollection: DataCollection) => {
    setSelectedDataCollection(dataCollection);
  };
  return (
    <SplitPage
      left={
        <Row className="g-2">
          {collections?.map((dataCollection) => (
            <Col xs={12} key={dataCollection.id}>
              <ReservedDOICard
                dataCollection={dataCollection}
                selected={dataCollection.id === selectedDataCollection?.id}
                onMint={onMint}
              />
            </Col>
          ))}
        </Row>
      }
      right={rightPanel}
    />
  );
}
