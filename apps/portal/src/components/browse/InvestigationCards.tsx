import {
  useEndpointPagination,
  PaginationMenu,
  DatasetStatistics,
  FilesStatistics,
  NoData,
  InvestigationDate,
  SAMPLE_COUNT,
} from '@edata-portal/core';
import {
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
  Role,
} from '@edata-portal/icat-plus-api';
import { Button, Card, Col, Row, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { InvestigationTitle } from 'components/browse/Investigation';
import { DOIBadge } from '@edata-portal/doi';

export function InvestigationCards({
  search,
  filter,
  instrumentName,
  startDate,
  endDate,
}: {
  search?: string;
  filter?: Role;
  instrumentName?: string;
  startDate?: string;
  endDate?: string;
}) {
  const { data: investigations, ...pagination } = useEndpointPagination({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      sortBy: 'STARTDATE',
      sortOrder: -1,
      ...(filter ? { filter: filter } : {}),
      ...(search?.trim().length ? { search: search.trim() } : {}),
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
      ...(startDate ? { startDate: startDate } : {}),
      ...(endDate ? { endDate: endDate } : {}),
      withHasAccess: true,
    },
    paginationParams: {
      paginationKey: 'investigations',
    },
  });

  if (!investigations.length) {
    return (
      <Col>
        <NoData />
      </Col>
    );
  }
  return (
    <Col>
      <PaginationMenu {...pagination} />
      {investigations.map((i) => {
        return (
          <Row key={i.id}>
            <Col>
              <InvestigationCard investigation={i} />
            </Col>
          </Row>
        );
      })}
      <PaginationMenu {...pagination} />
    </Col>
  );
}

function InvestigationCard({
  investigation,
}: {
  investigation: Investigation;
}) {
  return (
    <Card className="m-1">
      <Card.Header>
        <Row className="g-1">
          <Col>
            {investigation.canAccessDatasets ? (
              <Link to={`/investigation/${investigation.id}/datasets`}>
                <Button
                  variant="link"
                  className="p-0"
                  style={{
                    textAlign: 'left',
                  }}
                >
                  <InvestigationTitle investigation={investigation} showName />
                </Button>
              </Link>
            ) : (
              <InvestigationTitle investigation={investigation} showName />
            )}
          </Col>
        </Row>
      </Card.Header>
      <Card.Body
        className="p-2"
        style={{
          overflow: 'hidden',
        }}
      >
        <Container fluid>
          <Row>
            <Col>{investigation.instrument?.name}</Col>
            <Col>
              <InvestigationDate investigation={investigation} />
            </Col>
          </Row>
          {investigation.canAccessDatasets ? (
            <Row>
              <Col>
                <FilesStatistics
                  parameters={investigation.parameters}
                  showLabel
                />
              </Col>
              <Col>
                <DatasetStatistics
                  label="Samples"
                  showLabel
                  parameters={investigation.parameters}
                  parameterCaption={SAMPLE_COUNT}
                />
              </Col>
            </Row>
          ) : null}
          {investigation.doi && (
            <Col xs={'auto'}>
              <DOIBadge doi={investigation.doi} />
            </Col>
          )}
        </Container>
      </Card.Body>
    </Card>
  );
}
