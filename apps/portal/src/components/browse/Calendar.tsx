import {
  formatDateToDayAndShortTime,
  getInvestigationParam,
  DATASET_COUNT,
  FILE_COUNT,
  DATASET_VOLUME,
  Loading,
  isScheduled,
} from '@edata-portal/core';
import {
  Role,
  useGetEndpoint,
  Investigation,
  INVESTIGATION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import classNames from 'classnames';
import dayjs from 'dayjs';
import { useMemo, Suspense, useCallback } from 'react';
import {
  Button,
  ButtonGroup,
  Col,
  Container,
  OverlayTrigger,
  Popover,
  Row,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {
  HasDataInvestigationIcon,
  LegendCalendar,
  NotScheduledInvestigationIcon,
  ScheduledInvestigationIcon,
} from 'components/browse/calendar/LegendCalendar';

export function InvestigationCalendar({
  search,
  filter,
  instrumentName,
  startDate,
  setStartDate,
  setEndDate,
  username,
}: {
  search?: string;
  filter?: Role;
  instrumentName?: string;
  startDate?: string;
  endDate?: string;
  setStartDate: (date: string) => void;
  setEndDate: (date: string) => void;
  username?: string;
}) {
  const today = useMemo(() => dayjs(), []);

  const setCurrentMonth = (month: dayjs.Dayjs) => {
    setStartDate(month.startOf('month').format('YYYY-MM-DD'));
    setEndDate(month.endOf('month').format('YYYY-MM-DD'));
  };

  const currentMonth = useMemo(
    () => dayjs(startDate || today),
    [startDate, today],
  );

  const prevYear = currentMonth.add(-1, 'year');
  const nextYear = currentMonth.add(1, 'year');

  const prevMonth = currentMonth.add(-1, 'month');
  const nextMonth = currentMonth.add(1, 'month');

  return (
    <Container fluid>
      <Col>
        <Row className="justify-content-center">
          <Col xs="auto">
            <ButtonGroup>
              <Button
                variant="outline-primary"
                onClick={() => {
                  setCurrentMonth(prevYear);
                }}
              >
                {'<<'} {prevYear.format('YYYY')}
              </Button>
              <Button
                variant="outline-primary"
                onClick={() => {
                  setCurrentMonth(prevMonth);
                }}
              >
                {'<'} {prevMonth.format('MMMM')}
              </Button>
              <Button disabled variant="dark">
                {currentMonth.format('MMMM YYYY')}
              </Button>
              <Button
                variant="outline-primary"
                onClick={() => {
                  setCurrentMonth(nextMonth);
                }}
              >
                {nextMonth.format('MMMM')} {'>'}
              </Button>
              <Button
                variant="outline-primary"
                onClick={() => {
                  setCurrentMonth(nextYear);
                }}
              >
                {nextYear.format('YYYY')} {'>>'}
              </Button>
            </ButtonGroup>
          </Col>
        </Row>
        {currentMonth.isSame(today, 'month') ? null : (
          <Row className="justify-content-center m-1">
            <Col xs="auto">
              <Button
                variant="primary"
                onClick={() => {
                  setCurrentMonth(today);
                }}
              >
                Back to today
              </Button>
            </Col>
          </Row>
        )}
        <hr />
        <Suspense fallback={<Loading />}>
          <InvestigationCalendarDays
            currentMonth={currentMonth}
            filter={filter}
            instrumentName={instrumentName}
            search={search}
            username={username}
          />
        </Suspense>
      </Col>
    </Container>
  );
}

export function InvestigationCalendarDays({
  currentMonth,
  filter,
  instrumentName,
  search,
  username,
}: {
  currentMonth: dayjs.Dayjs;
  filter?: Role;
  instrumentName?: string;
  search?: string;
  username?: string;
}) {
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      sortBy: 'STARTDATE',
      sortOrder: -1,
      startDate: currentMonth.startOf('month').format('YYYY-MM-DD'),
      endDate: currentMonth.endOf('month').format('YYYY-MM-DD'),
      ...(filter ? { filter: filter } : {}),
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
      ...(search?.trim().length ? { search: search.trim() } : {}),
      ...(username ? { username: username } : {}),
      withHasAccess: true,
    },
    default: [] as Investigation[],
  });

  const days = useMemo(() => {
    const res = [];
    const first = currentMonth.startOf('month').startOf('week');
    const last = currentMonth.endOf('month').endOf('week');
    for (let i = first; i.isBefore(last); i = i.add(1, 'day')) {
      res.push(i);
    }
    return res;
  }, [currentMonth]);

  const weeks = useMemo(() => {
    const res = [];
    for (let i = 0; i < days.length; i += 7) {
      res.push(days.slice(i, i + 7));
    }
    return res;
  }, [days]);

  return (
    <>
      <LegendCalendar />
      <Row className="g-1 p-1">
        <CalendarDayHeader />
      </Row>
      {weeks.map((week, i) => {
        return (
          <Row key={i} className="g-1 p-1">
            {week.map((day, j) => {
              return (
                <CalendarDay
                  key={j}
                  day={day}
                  investigations={investigations}
                  currentMonth={currentMonth}
                  hightlightToday
                />
              );
            })}
          </Row>
        );
      })}
    </>
  );
}

function CalendarDayHeader() {
  const days = [...Array(7).keys()].map((i) => {
    return dayjs().startOf('week').add(i, 'day').format('dddd');
  });

  return (
    <>
      {days.map((day, i) => {
        return (
          <Col key={i} className="text-center d-none d-lg-inline">
            <div className="bg-primary text-white p-1">
              <strong>{day}</strong>
            </div>
          </Col>
        );
      })}
    </>
  );
}

export function CalendarDay({
  day,
  investigations,
  currentMonth,
  hightlightToday,
  showFullDayLabel,
}: {
  day: dayjs.Dayjs;
  currentMonth: dayjs.Dayjs;
  investigations: Investigation[];
  hightlightToday: boolean;
  showFullDayLabel?: boolean;
}) {
  const isCurrentMonth = day.isSame(currentMonth, 'month');
  const isToday = day.isSame(dayjs(), 'day');

  const startTodayInvestigations = investigations
    .filter((i) => {
      return i.startDate && day.isSame(i.startDate, 'day');
    })
    .sort((a, b) => {
      return dayjs(a.startDate).diff(dayjs(b.startDate));
    });

  const endTodayInvestigations = investigations
    .filter((i) => {
      return i.endDate && day.isSame(i.endDate, 'day');
    })
    .sort((a, b) => {
      return dayjs(a.startDate).diff(dayjs(b.startDate));
    });

  const continueTodayInvestigations = investigations
    .filter((i) => {
      return (
        i.endDate &&
        i.startDate &&
        day.isAfter(i.startDate, 'day') &&
        day.isBefore(i.endDate, 'day')
      );
    })
    .sort((a, b) => {
      return dayjs(a.startDate).diff(dayjs(b.startDate));
    });

  const dayInvestigations = [
    ...startTodayInvestigations,
    ...endTodayInvestigations.filter(
      (i) => !startTodayInvestigations.includes(i),
    ),
    ...continueTodayInvestigations,
  ];

  const limit = 5;

  const limited = dayInvestigations.slice(0, limit);

  const isInvestigationStartingOrEndingToday = useCallback(
    (i: Investigation) => ({
      startsToday: startTodayInvestigations.includes(i),
      endsToday: endTodayInvestigations.includes(i),
    }),
    [endTodayInvestigations, startTodayInvestigations],
  );

  if (!isCurrentMonth) return <Col></Col>;

  const overlay = (
    <Popover>
      <Popover.Header as="h3">
        Experiment sessions on {day.format('DD/MM/YYYY')}
      </Popover.Header>
      <Popover.Body
        style={{
          maxHeight: '50vh',
          overflowY: 'auto',
        }}
        className="bg-light"
      >
        {dayInvestigations.map((i) => {
          return (
            <InvestigationLink
              key={i.id}
              investigation={i}
              {...isInvestigationStartingOrEndingToday(i)}
            />
          );
        })}
      </Popover.Body>
    </Popover>
  );

  return (
    <Col
      lg={true}
      xs={12}
      style={{
        overflow: 'hidden',
      }}
    >
      <div
        className={classNames({
          'border rounded p-1 h-100 border-secondary': true,
          'bg-light': !(isToday && hightlightToday),
          'bg-secondary': isToday && hightlightToday,
        })}
        style={{
          overflow: 'hidden',
        }}
      >
        <h5
          className={classNames({
            'text-info': !(isToday && hightlightToday),
            'text-white': isToday && hightlightToday,
          })}
        >
          {showFullDayLabel ? day.format('MMMM D') : day.format('DD')}
        </h5>
        <hr className="m-0 mb-2" />

        <Row className="g-1">
          {limited.map((i) => {
            return (
              <InvestigationLink
                key={i.id}
                investigation={i}
                {...isInvestigationStartingOrEndingToday(i)}
              />
            );
          })}
        </Row>
        {dayInvestigations.length > limit && (
          <OverlayTrigger
            rootClose
            trigger={'click'}
            placement="auto"
            overlay={overlay}
          >
            <Button
              variant="link"
              className={classNames({
                'p-0': true,
                'text-info': !isToday,
                'text-white': isToday,
              })}
              style={{
                cursor: 'pointer',
              }}
            >
              and {dayInvestigations.length - limit} more...
            </Button>
          </OverlayTrigger>
        )}
        {dayInvestigations.length === 0 && (
          <small className="text-muted">No Experiment sessions</small>
        )}
      </div>
    </Col>
  );
}

export function InvestigationLink({
  investigation,
  startsToday,
  endsToday,
}: {
  investigation: Investigation;
  startsToday: boolean;
  endsToday: boolean;
}) {
  const startTime = dayjs(investigation.startDate).format('HH:mm');
  const endTime = dayjs(investigation.endDate).format('HH:mm');
  const scheduled = isScheduled(investigation);

  const hasData = [DATASET_VOLUME, DATASET_COUNT, FILE_COUNT].some((p) =>
    getInvestigationParam(investigation, p),
  );

  const overlay = (
    <Popover>
      <Popover.Header as="h3">{investigation.name}</Popover.Header>
      <Popover.Body>
        <div>
          <strong>Title:</strong> {investigation.title}
        </div>
        <div>
          <strong>Start date:</strong>{' '}
          {formatDateToDayAndShortTime(investigation.startDate)}
        </div>
        <div>
          <strong>End date:</strong>{' '}
          {formatDateToDayAndShortTime(investigation.endDate) || 'unknown'}
        </div>
      </Popover.Body>
    </Popover>
  );

  const timeLabel = useMemo(() => {
    if (!scheduled) return 'not scheduled';
    if (!startsToday && !endsToday) return 'all day';

    return `${startsToday ? startTime : 'day before'} to ${
      endsToday ? endTime : 'day after'
    }`;
  }, [endTime, endsToday, scheduled, startTime, startsToday]);

  return (
    <Col xs={'auto'} lg={12}>
      <OverlayTrigger
        trigger={['focus', 'hover']}
        placement="auto"
        overlay={overlay}
      >
        <div
          className="bg-white w-100 text-black"
          style={{
            display: 'flex',
            alignItems: 'center',
            columnGap: 5,
            flexFlow: 'row wrap',
            wordBreak: 'break-all',
            padding: '0 5px',
          }}
        >
          {scheduled ? (
            <ScheduledInvestigationIcon />
          ) : (
            <NotScheduledInvestigationIcon />
          )}
          {hasData ? <HasDataInvestigationIcon /> : null}
          {investigation.canAccessDatasets ? (
            <Link to={`/investigation/${investigation.id}/datasets`}>
              <Button variant="link" className="p-0">
                {investigation.name}
              </Button>
            </Link>
          ) : (
            <span>{investigation.name}</span>
          )}
          <small>
            <i>
              {investigation.instrument?.name} [{timeLabel}]
            </i>
          </small>
        </div>
      </OverlayTrigger>
    </Col>
  );
}
