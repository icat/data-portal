import { Loading, usePageTracking } from '@edata-portal/core';
import { Suspense, useMemo } from 'react';
import { useMatches } from 'react-router-dom';

export function TrackRoutes({ children }: { children: JSX.Element }) {
  let matches = useMatches();

  const { trackname, skipTrack } = useMemo(() => {
    const names = matches.map((m) => {
      if (!m.handle) return undefined;
      if (!(typeof m.handle === 'object')) return undefined;
      if (!('trackname' in m.handle)) return undefined;
      return m.handle.trackname;
    });

    const skipTrack = names.some((m) => m === false);

    const trackname = names.filter((t) => typeof t === 'string').join(' - ');

    return { trackname, skipTrack };
  }, [matches]);

  usePageTracking(trackname, skipTrack);

  return <Suspense fallback={<Loading />}>{children}</Suspense>;
}
