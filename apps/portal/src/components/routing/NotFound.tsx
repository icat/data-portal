import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default function NotFound() {
  return (
    <div
      className="NotFound"
      style={{
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <div
        style={{
          border: '1px solid red',
          borderRadius: '1rem',
          padding: '2rem',
          margin: '1rem',
          width: 'fit-content',
        }}
      >
        <h2>Page not found</h2>
        <Link to="/">
          <Button variant="outline-primary">Go back home</Button>
        </Link>
      </div>
    </div>
  );
}
