import { convertToFixed, formatDateToTime } from '@edata-portal/core';
import type { DatasetTimeLine } from '@edata-portal/icat-plus-api';
import { Modal, Container, Row, Col } from 'react-bootstrap';

export function CustomTooltip({
  active,
  time,
}: {
  active?: boolean;
  time?: DatasetTimeLine;
}) {
  if (active && time) {
    return (
      <Modal.Dialog style={{ backgroundColor: 'white' }} size="sm">
        <Modal.Header>
          <Modal.Title>Dataset</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Container>
            <Row>
              <Col>Dataset:</Col>
              <Col>{time.datasetName}</Col>
            </Row>{' '}
            <Row>
              <Col>Type:</Col>
              <Col>{time.datasetType}</Col>
            </Row>{' '}
            <Row>
              <Col>Sample:</Col>
              <Col>{time.sampleName}</Col>
            </Row>{' '}
            <Row>
              <Col>Volume:</Col>
              <Col>
                {convertToFixed(
                  (time.__volume! as unknown as number) / 1024 / 1024 / 1024,
                  3,
                )}{' '}
                GB
              </Col>
            </Row>
            <Row>
              <Col>Start:</Col>
              <Col>{formatDateToTime(time.startDate)}</Col>
            </Row>{' '}
            <Row>
              <Col>End:</Col>
              <Col>{formatDateToTime(time.endDate)}</Col>
            </Row>{' '}
          </Container>
        </Modal.Body>
      </Modal.Dialog>
    );
  }

  return null;
}
