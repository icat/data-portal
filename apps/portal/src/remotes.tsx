import React, { useEffect } from 'react';
import {
  __federation_method_getRemote,
  __federation_method_setRemote,
  // @ts-ignore
} from '__federation__';
import { useSuspenseQuery } from '@tanstack/react-query';
import { JSONSchemaType } from 'ajv';

export type RemoteDefinition = {
  name: string;
  component: string;
};

export const REMOTE_DEFINITION_SCHEMA: JSONSchemaType<RemoteDefinition> = {
  type: 'object',
  properties: {
    name: { type: 'string', nullable: false },
    component: { type: 'string', nullable: false },
  },
  required: ['name', 'component'],
  additionalProperties: false,
  nullable: false,
};

export type RemoteUrls = {
  [key: string]: string;
};

const promises = new Map();
const results = new Map();
const errors = new Map();

export function RemoteProvider({ children }: { children: React.ReactNode }) {
  const { data: remotes } = useSuspenseQuery({
    queryKey: ['remotes.config'],
    queryFn: async () => {
      const response = await fetch('/config/remotes.config.json');
      return (await response.json()) as RemoteUrls;
    },
  });

  useEffect(() => {
    if (remotes) {
      for (const [name, url] of Object.entries(remotes)) {
        __federation_method_setRemote(name, {
          url: Promise.resolve(url),
          format: 'esm',
          from: 'vite',
        });
      }
    }
  }, [remotes]);

  return <>{children}</>;
}

export function getRemoteComponent(remote: RemoteDefinition) {
  const key = getRemoteKey(remote);

  if (results.has(key)) {
    return results.get(key);
  }

  if (errors.has(key)) {
    throw errors.get(key);
  }

  if (promises.has(key)) {
    throw promises.get(key);
  }

  const promise = __federation_method_getRemote(remote.name, remote.component)
    .then((module: React.Component | Object) => {
      if ('default' in module) {
        //if the module has more than one export, we want to return the default export
        results.set(key, module.default);
      } else {
        results.set(key, module);
      }
      promises.delete(key);
      return module;
    })
    .catch((e: any) => {
      const error = new RemoteLoadingError(remote, e);
      errors.set(key, error);
      promises.delete(key);
      throw error;
    });
  promises.set(key, promise);
  throw promise;
}

function getRemoteKey(remote: RemoteDefinition) {
  return `${remote.name}:${remote.component}`;
}

export class RemoteLoadingError extends Error {
  remote: string;

  constructor(remote: RemoteDefinition, cause: Error) {
    super(`Failed to load remote component.`);
    this.name = 'RemoteLoadingError';
    this.remote = JSON.stringify(remote);
    this.cause = cause;
  }
}
