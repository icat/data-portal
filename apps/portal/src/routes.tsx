import { Loading, usePath } from '@edata-portal/core';
import React, { Suspense } from 'react';
import { InstrumentPage } from 'components/instrument/InstrumentPage';
import { RemoteComponent } from 'components/RemoteComponent';
import { Navigate, type RouteObject } from 'react-router-dom';
import { AdminPage } from 'components/admin/AdminPage';
import {
  DatasetBreadcrumbItem,
  FacilityBreadcrumbItem,
  InvestigationBreadcrumbItem,
} from 'components/navigation/Breadcrumbs';
import { DOIBadge } from '@edata-portal/doi';
import MintDOIPage from 'components/mint/MintDOIPage';
import { InstrumentScientistPage } from 'components/instrument/InstrumentScientistPage';
import ReserveDOIPage from 'components/mint/ReserveDOIPage';

const Homepage = React.lazy(() => import('components/homepage/Homepage'));
const HelpPage = React.lazy(() => import('components/help/HelpPage'));
const InvestigationsPage = React.lazy(
  () => import('components/browse/InvestigationsPage'),
);
const SamplesPage = React.lazy(() => import('components/browse/SamplesPage'));
const PublicationsPage = React.lazy(
  () => import('components/browse/PublicationsPage'),
);

const SelectionPage = React.lazy(
  () => import('components/selection/SelectionPage'),
);
const InstrumentsList = React.lazy(
  () => import('components/instrument/InstrumentsList'),
);
const InstrumentInvestigations = React.lazy(
  () => import('components/instrument/InstrumentInvestigations'),
);
const InstrumentDatasetList = React.lazy(
  () => import('components/instrument/InstrumentDatasetList'),
);
const InstrumentDataStatistics = React.lazy(
  () => import('components/instrument/InstrumentDataStatistics'),
);
const InstrumentLogbook = React.lazy(
  () => import('components/instrument/InstrumentLogbook'),
);
const InstrumentsManage = React.lazy(
  () => import('components/instrument/manage/InstrumentsManage'),
);

const InstrumentsSchedule = React.lazy(
  () => import('components/instrument/schedule/InstrumentsSchedule'),
);

const InstrumentManage = React.lazy(
  () => import('components/instrument/manage/InstrumentManage'),
);
const InvestigationPage = React.lazy(
  () => import('components/investigation/InvestigationPage'),
);
const InvestigationSummary = React.lazy(
  () => import('components/investigation/summary/InvestigationSummary'),
);
const InvestigationStatistics = React.lazy(
  () => import('components/investigation/InvestigationStatistics'),
);
const InvestigationLogbook = React.lazy(
  () => import('components/investigation/InvestigationLogbook'),
);
const InvestigationPreparePage = React.lazy(
  () => import('components/investigation/InvestigationPreparePage'),
);
const InvestigationDatasets = React.lazy(
  () => import('components/investigation/InvestigationDatasets'),
);
const DatasetDetails = React.lazy(
  () => import('components/dataset/DatasetDetails'),
);

const MyJobs = React.lazy(() => import('components/reprocessing/jobs/MyJobs'));
const ReprocessPage = React.lazy(
  () => import('components/reprocessing/ReprocessPage'),
);

const InvestigationJobs = React.lazy(
  () => import('components/reprocessing/jobs/InvestigationJobs'),
);

const NotFound = React.lazy(() => import('components/routing/NotFound'));

const SampleMetadata = React.lazy(
  () => import('components/metadata/SampleMetadata'),
);
const FacilityMonitoringPage = React.lazy(
  () => import('components/admin/facility/FacilityMonitoringPage'),
);
const FacilityLogbook = React.lazy(
  () => import('components/admin/facility/FacilityLogbook'),
);
const SiteStatistics = React.lazy(
  () => import('components/admin/facility/statistics/SiteStatisticsPage'),
);
const UsersPage = React.lazy(() => import('components/admin/users/UsersPage'));

const DOIPage = React.lazy(() => import('components/doi/DOIPage'));
const DOIInfo = React.lazy(() => import('components/doi/DOIInfo'));
const DOIDatasets = React.lazy(() => import('components/doi/DOIDatasets'));

const PublicDOIRedirect = () => {
  const doiPrefix = usePath('doiPrefix');
  const doiSuffix = usePath('doiSuffix');
  return <Navigate to={`/doi/${doiPrefix}/${doiSuffix}`} replace />;
};

export const routes: RouteObject[] = [
  {
    path: '/',
    element: (
      <Suspense fallback={<Loading />}>
        <Homepage />
      </Suspense>
    ),
    handle: {
      breadcrumb: false,
      trackname: 'home',
    },
  },
  {
    path: '/help',
    element: (
      <Suspense fallback={<Loading />}>
        <HelpPage />
      </Suspense>
    ),
    handle: {
      breadcrumb: 'Help',
      trackname: 'help',
    },
  },
  {
    path: '/selection',
    element: (
      <Suspense fallback={<Loading />}>
        <SelectionPage />
      </Suspense>
    ),
    handle: {
      breadcrumb: 'Selection',
      trackname: 'selection',
    },
  },
  {
    path: '/jobs',
    handle: {
      breadcrumb: 'Jobs',
      trackname: 'jobs',
    },
    children: [
      {
        index: true,
        handle: {
          breadcrumb: 'My jobs',
          trackname: 'my-jobs',
        },
        element: (
          <Suspense fallback={<Loading />}>
            <MyJobs />
          </Suspense>
        ),
      },
      {
        path: 'new',
        element: (
          <Suspense fallback={<Loading />}>
            <ReprocessPage />
          </Suspense>
        ),
        handle: {
          breadcrumb: 'New job',
          trackname: 'new-job',
        },
      },
    ],
  },
  {
    path: '/publications',
    element: (
      <Suspense fallback={<Loading />}>
        <PublicationsPage />
      </Suspense>
    ),
    handle: {
      breadcrumb: 'Publications',
      trackname: 'publications',
    },
  },
  {
    path: '/experiments',
    element: (
      <Suspense fallback={<Loading />}>
        <InvestigationsPage />
      </Suspense>
    ),
    handle: {
      breadcrumb: 'Experiments',
      trackname: 'experiments',
    },
  },
  {
    path: '/samples',
    element: (
      <Suspense fallback={<Loading />}>
        <SamplesPage />
      </Suspense>
    ),
    handle: {
      breadcrumb: 'Samples',
      trackname: 'samples',
    },
  },
  {
    path: '/instruments',
    handle: {
      breadcrumb: 'Beamlines',
      trackname: false,
    },
    element: <InstrumentScientistPage />,
    children: [
      {
        index: true,
        element: (
          <Suspense fallback={<Loading />}>
            <InstrumentsList />
          </Suspense>
        ),
        handle: {
          breadcrumb: false,
        },
      },
      {
        path: 'manage',
        element: (
          <Suspense fallback={<Loading />}>
            <InstrumentsManage />
          </Suspense>
        ),
        handle: {
          breadcrumb: 'Manage users',
        },
      },
      {
        path: 'schedule',
        element: (
          <Suspense fallback={<Loading />}>
            <InstrumentsSchedule />
          </Suspense>
        ),
        handle: {
          breadcrumb: 'Schedule',
        },
      },
      {
        path: ':instrumentName',
        element: (
          <Suspense fallback={<Loading />}>
            <InstrumentPage />
          </Suspense>
        ),
        handle: {
          breadcrumb: ({ instrumentName }: any) => instrumentName,
        },
        children: [
          {
            index: true,
            element: (
              <Suspense fallback={<Loading />}>
                <InstrumentDatasetList />
              </Suspense>
            ),
            handle: {
              breadcrumb: 'Datasets',
            },
          },
          {
            path: 'statistics',
            handle: {
              breadcrumb: 'Statistics',
            },
            element: (
              <Suspense fallback={<Loading />}>
                <InstrumentDataStatistics />
              </Suspense>
            ),
          },
          {
            path: 'investigations',
            handle: {
              breadcrumb: 'Experiment sessions',
            },
            element: (
              <Suspense fallback={<Loading />}>
                <InstrumentInvestigations />
              </Suspense>
            ),
          },
          {
            path: 'logbook',
            handle: {
              breadcrumb: 'Logbook',
            },
            element: (
              <Suspense fallback={<Loading />}>
                <InstrumentLogbook />
              </Suspense>
            ),
          },
          {
            path: 'manage',
            handle: {
              breadcrumb: 'Manage users',
            },
            element: (
              <Suspense fallback={<Loading />}>
                <InstrumentManage />
              </Suspense>
            ),
          },
        ],
      },
    ],
  },
  {
    path: '/investigation/:investigationId',
    element: (
      <Suspense fallback={<Loading />}>
        <InvestigationPage />
      </Suspense>
    ),
    handle: {
      breadcrumb: ({ investigationId }: any) => (
        <InvestigationBreadcrumbItem investigationId={investigationId} />
      ),
      trackname: 'investigation',
    },
    children: [
      {
        index: true,
        element: (
          <Suspense fallback={<Loading />}>
            <InvestigationSummary />
          </Suspense>
        ),
        handle: {
          breadcrumb: false,
          trackname: 'summary',
        },
      },
      {
        path: 'statistics',
        element: (
          <Suspense fallback={<Loading />}>
            <InvestigationStatistics />
          </Suspense>
        ),
        index: true,
        handle: {
          breadcrumb: 'Statistics',
          trackname: 'statistics',
        },
      },
      {
        path: 'datasets',
        handle: {
          breadcrumb: 'Datasets',
          trackname: 'datasets',
        },
        children: [
          {
            index: true,
            element: (
              <Suspense fallback={<Loading />}>
                <InvestigationDatasets />
              </Suspense>
            ),
            handle: {
              breadcrumb: false,
              trackname: 'list',
            },
          },
          {
            handle: {
              breadcrumb: ({ datasetId }: any) => (
                <DatasetBreadcrumbItem datasetId={datasetId} />
              ),
              trackname: 'dataset',
            },
            path: ':datasetId',
            element: (
              <Suspense fallback={<Loading />}>
                <DatasetDetails />
              </Suspense>
            ),
          },
        ],
      },
      {
        path: 'jobs',
        element: (
          <Suspense fallback={<Loading />}>
            <InvestigationJobs />
          </Suspense>
        ),
        handle: {
          breadcrumb: 'Jobs',
          trackname: 'jobs',
        },
      },
      {
        path: 'logbook',
        handle: {
          breadcrumb: 'Logbook',
          trackname: 'logbook',
        },
        element: (
          <Suspense fallback={<Loading />}>
            <InvestigationLogbook />
          </Suspense>
        ),
      },
      {
        path: 'logistics',
        handle: {
          breadcrumb: 'Logistics',
          trackname: 'logistics',
        },
        element: <InvestigationPreparePage />,
        children: [
          {
            index: true,
            element: (
              <RemoteComponent
                remote={{
                  name: 'logistics',
                  component: './InvestigationLogistics',
                }}
              />
            ),
            handle: {
              breadcrumb: false,
              trackname: 'logisticshome',
            },
          },
          {
            path: 'changer',
            element: (
              <RemoteComponent
                remote={{
                  name: 'logistics',
                  component: './InvestigationSampleChanger',
                }}
              />
            ),
            handle: {
              breadcrumb: 'Sample changer',
              trackname: 'samplechanger',
            },
          },
          {
            path: 'parcel/:parcelId',
            element: (
              <RemoteComponent
                remote={{
                  name: 'logistics',
                  component: './ParcelPage',
                }}
              />
            ),
            handle: {
              breadcrumb: 'Parcel',
              trackname: 'parcel',
            },
          },
        ],
      },
    ],
  },
  {
    path: 'doi',
    children: [
      {
        path: 'new',
        element: <MintDOIPage />,
        handle: {
          breadcrumb: 'Mint DOI',
          trackname: 'mint-doi',
        },
      },
      {
        path: 'reserve',
        element: <ReserveDOIPage />,
        handle: {
          breadcrumb: 'Reserve DOI',
          trackname: 'reserve-doi',
        },
      },
      {
        path: ':doiPrefix/:doiSuffix',
        id: 'doi',
        element: <DOIPage />,
        handle: {
          breadcrumb: ({ doiPrefix, doiSuffix }: any) => (
            <span
              style={{
                fontSize: 12,
              }}
            >
              <DOIBadge doi={`${doiPrefix}/${doiSuffix}`} interactive={false} />
            </span>
          ),
          trackname: 'info',
        },
        children: [
          {
            index: true,
            element: <DOIInfo />,
          },
          {
            path: 'datasets',
            handle: {
              breadcrumb: 'Datasets',
              trackname: 'datasets',
            },
            element: (
              <Suspense fallback={<Loading />}>
                <DOIDatasets />
              </Suspense>
            ),
          },
        ],
      },
    ],
  },
  {
    path: 'public',
    children: [
      {
        index: true,
        element: <NotFound />,
      },
      {
        path: ':doiPrefix/:doiSuffix',
        id: 'publicdoi',
        element: <PublicDOIRedirect />,
      },
    ],
  },
  {
    path: '/addresses',
    element: (
      <RemoteComponent
        remote={{
          name: 'logistics',
          component: './MyAddresses',
        }}
      />
    ),
    handle: {
      breadcrumb: 'Addresses',
      trackname: 'addresses',
    },
  },
  {
    path: 'compare',
    handle: {
      breadcrumb: 'Compare',
    },
    children: [
      {
        path: 'datasets',
        handle: {
          breadcrumb: 'Datasets',
          trackname: 'datasets',
        },
        element: (
          <Suspense fallback={<Loading />}>
            <SampleMetadata />
          </Suspense>
        ),
      },
    ],
  },
  {
    path: '/parcels',
    element: (
      <RemoteComponent
        remote={{
          name: 'logistics',
          component: './MyParcels',
        }}
      />
    ),
    handle: {
      breadcrumb: 'Parcels',
      trackname: 'parcels',
    },
  },
  {
    path: '*',
    element: <NotFound />,
    handle: {
      breadcrumb: 'Not found',
      trackname: 'not-found',
    },
  },
  {
    path: 'admin',
    element: <AdminPage />,
    handle: {
      breadcrumb: 'Admin',
      trackname: false,
    },
    children: [
      {
        path: 'facility',
        element: <FacilityMonitoringPage />,
        handle: {
          breadcrumb: () => <FacilityBreadcrumbItem />,
        },
        children: [
          {
            path: 'logbook',
            element: <FacilityLogbook />,
            handle: {
              breadcrumb: 'Logbook',
            },
          },
          {
            path: 'statistics',
            element: <SiteStatistics />,
            handle: {
              breadcrumb: 'Statistics',
            },
          },
        ],
      },
      {
        path: 'users',
        element: (
          <Suspense fallback={<Loading />}>
            <UsersPage />
          </Suspense>
        ),
        handle: {
          breadcrumb: 'Users',
        },
      },
    ],
  },
];
