import {
  AuthenticatedAPIProvider,
  AuthenticatorProvider,
  Loading,
  OpenIDProvider,
  SideNavProvider,
  UnauthenticatedAPIProvider,
} from '@edata-portal/core';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { AppRouter } from 'components/routing/AppRouter';
import { Suspense } from 'react';
import { Navigation } from 'components/navigation/Navigation';
import { ViewersProvider } from 'components/viewers/ViewersProvider';
import {
  NotifyProvider,
  NotifyRenderer,
} from 'components/notify/NotifyProvider';
import { ReprocessContextProvider } from 'components/reprocessing/ReprocessContext';
import { TrackRoutes } from 'components/routing/TrackRoutes';
import { ConfigProvider } from 'ConfigProvider';
import { RemoteProvider } from 'remotes';

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <RemoteProvider>
        <ConfigProvider>
          <AppRouter>
            <AppContextProviders>
              <Suspense fallback={<Loading />}>
                <ViewersProvider>
                  <SideNavProvider>
                    <ReprocessContextProvider>
                      <Navigation />
                    </ReprocessContextProvider>
                  </SideNavProvider>
                </ViewersProvider>
              </Suspense>
            </AppContextProviders>
          </AppRouter>
        </ConfigProvider>
      </RemoteProvider>
    </QueryClientProvider>
  );
}

function AppContextProviders({ children }: { children: React.ReactNode }) {
  return (
    <TrackRoutes>
      <NotifyProvider>
        <UnauthenticatedAPIProvider>
          <AuthenticatorProvider>
            <OpenIDProvider>
              <AuthenticatedAPIProvider>
                <NotifyRenderer />
                {children}
              </AuthenticatedAPIProvider>
            </OpenIDProvider>
          </AuthenticatorProvider>
        </UnauthenticatedAPIProvider>
      </NotifyProvider>
    </TrackRoutes>
  );
}

export default App;
