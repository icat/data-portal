import React from 'react';
import ReactDOM from 'react-dom/client';
import App from 'App';
import 'scss/main.scss';
import dayjs from 'dayjs';
import en from 'dayjs/locale/en';

dayjs.locale({
  ...en,
  weekStart: 1,
});

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
