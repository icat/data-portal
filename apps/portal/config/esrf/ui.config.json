{
  "applicationTitle": "Data Portal",
  "facilityName": "ESRF",
  "homePage": {
    "policyMessage": "Public data is accessible to anyone. You need to be logged-in to visualize your data when it is under embargo. See <a href=\"https://www.esrf.fr/fr/home/UsersAndScience/UserGuide/esrf-data-policy.html\" target=\"_blank\" rel=\"noreferrer\">ESRF data policy</a> for more details."
  },
  "knowledgeBasePage": "https://confluence.esrf.fr/display/DATAPOLWK/Knowledge+Base",
  "loginForm": {
    "accountCreationLink": "https://smis.esrf.fr/misapps/SMISWebClient/accountManager/searchExistingAccount.do?action=search",
    "note": {
      "enabled": true,
      "title": "Important note",
      "notes": [
        {
          "text": "In order to login to the Data Portal of the ESRF to send samples or browse embargoed data you need to be declared a member of a proposal on the ESRF User Portal. Once this is done your account will be activated <b>45 days</b> before the first experiment starts. If you need access earlier please contact the <a rel=\"noopener noreferrer\" target=\"_blank\" href=\"http://www.esrf.eu/UsersAndScience/UserGuide/Contacts\" >ESRF User Office</a>.<br/>Anonymous login to browse open data is always possible."
        },
        {
          "text": "During 2019 and according to the General Data Protection Regulation, all portal users who did not consent to the <a href=\"http://www.esrf.fr/GDPR\" rel=\"noopener noreferrer\" target=\"_blank\" > User Portal Privacy Statement</a> have had their account deactivated. Please contact the <a rel=\"noopener noreferrer\" target=\"_blank\" href=\"http://www.esrf.eu/UsersAndScience/UserGuide/Contacts\" >User Office</a> if you wish to reactivate it."
        }
      ]
    }
  },
  "userPortal": {
    "investigationParameterPkName": "Id",
    "link": "https://smis.esrf.fr/misapps/SMISWebClient/protected/aform/manageAForm.do?action=view&expSessionVO.pk="
  },
  "doi": {
    "link": "https://doi.esrf.fr/",
    "minimalAbstractLength": 1000,
    "minimalTitleLength": 40,
    "facilityPrefix": "10.15151",
    "facilitySuffix": "ESRF-DC",
    "referenceDoi": "https://doi.esrf.fr/10.15151/ESRF-DC-2011729981"
  },
  "fileBrowser": {
    "maxFileNb": 1000
  },
  "imageViewer": {
    "fileExtensions": [".png", ".jpg", ".jpeg", ".tiff", ".gif"]
  },
  "h5Viewer": {
    "url": "https://hibou.esrf.fr",
    "fileExtensions": [".hdf5", ".h5", ".nexus", ".nx", ".nxs", ".cxi"]
  },
  "galleryViewer": {
    "fileExtensions": ["jpeg", "jpg", "gif", "png", "svg"]
  },
  "textViewer": {
    "maxFileSize": 5000000,
    "fileExtensions": [
      ".txt",
      ".log",
      ".json",
      ".xml",
      ".csv",
      ".dat",
      ".inp",
      ".xds",
      ".descr",
      ".lp",
      ".hkl",
      ".site",
      ".asc",
      ".ini"
    ]
  },
  "feedback": {
    "email": "dataportalrequests@esrf.fr",
    "subject": "Feedback",
    "body": "Hi,\n\n<< Please provide your feedback here. >>\n<< To report an issue, please include screenshots, reproduction steps, proposal number, beamline, etc. >>\n<< To suggest a new feature, please describe the needs this feature would fulfill. >>\n\nThanks"
  },
  "footer": {
    "images": [
      {
        "src": "/images/esrf.jpg",
        "alt": "ESRF",
        "href": "https://www.esrf.fr/"
      },
      {
        "src": "/images/CoreTrustSeal.png",
        "alt": "CoreTrustSeal",
        "href": "https://www.coretrustseal.org/"
      }
    ]
  },
  "handsonTableLicenseKey": "non-commercial-and-evaluation",
  "globus": {
    "enabled": true,
    "url": "https://app.globus.org/file-manager?",
    "collections": [
      {
        "root": "/data/visitor/",
        "origin": "/data",
        "originId": "bfc3eff4-f5ca-4b4d-8532-e9a155f3613f"
      },
      {
        "root": "/data/projects/hop",
        "origin": "/data/projects/hop",
        "originId": "340dc883-4b0d-476c-abb0-969e1ddd9dc0"
      },
      {
        "root": "/data/projects/open-datasets",
        "origin": "/data/projects/open-datasets/",
        "originId": "8cbe8cdc-048a-48cf-b8a0-046a6af3ba44"
      },
      {
        "root": "/data/projects/paleo",
        "origin": "/data/projects/paleo/public",
        "originId": "e8fb6c4b-9ab0-4a1c-a79d-d51cef0b8c3d"
      }
    ],
    "messageAlert": {
      "enabled": true,
      "message": "For users who want to download large volume of experimental data <strong>(&gt;2GB)</strong>, ESRF users can access the Globus service, please read the <a href=\"https://confluence.esrf.fr/display/SCKB/Globus\" target=\"_blank\">documentation</a> for proceeding."
    }
  },
  "sample": {
    "pageTemplateURL": "https://smis.esrf.fr/misapps/SMISWebClient/protected/samplesheet/view.do?pk=",
    "editable": true,
    "descriptionParameterName": "Sample_description",
    "nonEditableParameterName": "Id",
    "notesParameterName": "Sample_notes"
  },
  "logbook": {
    "help": "https://confluence.esrf.fr/display/DATAPOLWK/Electronic+Logbook",
    "defaultMonthPeriodForStaff": 1
  },
  "projects": [
    {
      "title": "The Human Organ Atlas",
      "key": "The Human Organ Atlas",
      "url": "https://human-organ-atlas.esrf.eu/datasets/{datasetId}",
      "homepage": "https://human-organ-atlas.esrf.eu"
    },
    {
      "title": "Paleontology database",
      "key": "paleo",
      "url": "http://paleo.esrf.fr/datasets/{datasetId}",
      "homepage": "http://paleo.esrf.fr"
    }
  ],
  "features": {
    "reprocessing": true,
    "logbook": true,
    "dmp": true,
    "logistics": true
  },
  "tracking": {
    "enabled": true,
    "url": "https://matomo-srv-1.esrf.fr/",
    "siteId": "14",
    "tracker": "piwik.php",
    "script": "piwik.js"
  },
  "mx": {
    "pdb_map_mtz_viewer_url": "https://moorhen.esrf.fr"
  },
  "logistics": {
    "transportOrganizationEnabled": false,
    "facilityReimbursmentEnabled": true,
    "facilityForwarderName": "FedEX",
    "facilityForwarderAccount": "388310561",
    "facilityForwarderNamePickup": ["FEDEX", "DHL express", "UPS"]
  },
  "reprocessing": {
    "defaultDayPeriodJobs": 30
  }
}
