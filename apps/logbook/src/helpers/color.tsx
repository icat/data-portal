import type { LogbookEventCategory } from '@edata-portal/icat-plus-api';

export function getTextColor(category: LogbookEventCategory) {
  if (category === 'comment') {
    return 'text-primary';
  }

  if (category === 'error') {
    return 'text-danger';
  }

  if (category === 'info') {
    return 'text-info';
  }

  if (category === 'debug') {
    return 'text-muted';
  }
  return 'text-success';
}
