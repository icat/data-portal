import { LogbookEvent } from '@edata-portal/icat-plus-api';
import type { CSSProperties } from 'react';

export function getEventStyle(event: LogbookEvent): {
  style: CSSProperties;
  className: string;
} {
  const isAnnotation =
    event.type === 'annotation' || eventIsAnnotationOnNotification(event);

  const size = event.type === 'broadcast' ? 'larger' : undefined;
  const className = isAnnotation ? '' : 'monospace';

  return {
    style: {
      fontSize: size,
      whiteSpace: 'pre-wrap',
    },
    className,
  };
}

export function eventIsAnnotationOnNotification(event: LogbookEvent) {
  //if event is notification and has previous version event then it is actually an annotation on a notification
  return event.type === 'notification' && event.previousVersionEvent;
}
