import DOMPurify from 'dompurify';

export function formatHtml(html: string, search?: string) {
  const sanitized = DOMPurify.sanitize(html);

  const withFullScreen = sanitized.replace(
    /<img/g,
    "<img onClick='makeFullScreen(this)' style='height:200px;width:auto;cursor:zoom-in;' ",
  );

  const withSearch = replaceSearch(withFullScreen, search);

  return withSearch;
}

function replaceSearch(content: string, search?: string) {
  if (!search) return content;
  return content.replace(
    search,
    `<span style="background-color: yellow;">${search}</span>`,
  );
}

// We need to define the function in the global scope so that it can be called from the HTML
(window as any).makeFullScreen = function makeFullScreen(img: any) {
  const isFullscreen = document.fullscreenElement === img;

  if (!isFullscreen) {
    try {
      if (img.requestFullscreen) {
        img.requestFullscreen();
      } else if (img.msRequestFullscreen) {
        img.msRequestFullscreen();
      } else if (img.mozRequestFullScreen) {
        img.mozRequestFullScreen();
      } else if (img.webkitRequestFullscreen) {
        img.webkitRequestFullscreen();
      } else {
        console.error('Fullscreen API is not supported');
      }
    } catch {
      console.error('Fullscreen API is not supported');
    }
  } else {
    try {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if ((document as any).msExitFullscreen) {
        (document as any).msExitFullscreen();
      } else if ((document as any).mozCancelFullScreen) {
        (document as any).mozCancelFullScreen();
      } else if ((document as any).webkitExitFullscreen) {
        (document as any).webkitExitFullscreen();
      } else {
        console.error('Fullscreen API is not supported');
      }
    } catch {
      console.error('Fullscreen API is not supported');
    }
  }
};
