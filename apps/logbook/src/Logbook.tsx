import {
  Loading,
  SideNavElement,
  usePagination,
  useParam,
  useSetScroll,
  WithSideNav,
  useUserPreferences,
} from '@edata-portal/core';
import {
  useAsyncFetchEndpoint,
  EventListParams,
  LogbookEvent,
  LOGBOOK_EVENT_PAGE_ENDPOINT,
  LogbookSettings,
} from '@edata-portal/icat-plus-api';
import { LogbookEvents } from 'components/event/LogbookEvents';
import { LogbookEventsPagination } from 'components/event/LogbookEventsPagination';
import { LogbookConfiguration } from 'components/configure/LogbookConfiguration';
import { ManageTagsButton } from 'components/tag/ManageTags';
import { Suspense, useMemo, useState } from 'react';
import LogbookEventEditor from 'components/event/edit/LogBookEventEditor';
import { TakePicture } from 'components/TakePicture';
import { NewComment } from 'components/event/NewComment';
import { DownloadGroupButton } from 'components/DownloadGroupButton';
import { LogbookHelp } from 'components/LogbookHelp';

const DEFAULT_FILTERS = {
  types: [
    'annotation',
    'notification-comment',
    'notification-info',
    'notification-debug',
    'notification-error',
    'notification-commandline',
  ].join(','),
  sortBy: '_id',
  sortOrder: -1,
};

const PERSISTED_FILTERS = ['types', 'sortBy', 'sortOrder'] as const;

export default function Logbook({
  instrumentName,
  investigationId,
  readOnly,
}: {
  instrumentName?: string;
  investigationId?: string;
  readOnly?: boolean;
}) {
  const pageParameters = useMemo(
    () => ({
      ...(instrumentName ? { instrumentName } : {}),
      ...(investigationId ? { investigationId } : {}),
    }),
    [instrumentName, investigationId],
  );

  const pagination = usePagination({
    defaultSize: 100,
    availableSizes: [100, 200, 500, 1000],
    paginationKey: 'logbook',
  });

  const setScroll = useSetScroll();

  const [logbookPreferences, setLogbookPreferences] = useUserPreferences(
    'logbookFilters',
    DEFAULT_FILTERS,
  );

  const [filters, setFilters] = useParam(
    'filters',
    JSON.stringify(logbookPreferences),
  );

  const [writingNewComment, setWritingNewComment] = useState(false);

  const setFilterObject = (newFilter: EventListParams) => {
    const newPersistedFilters = PERSISTED_FILTERS.reduce((acc, key) => {
      return {
        ...acc,
        [key]: newFilter[key],
      };
    }, {} as LogbookSettings);

    if (
      PERSISTED_FILTERS.some(
        (key) => logbookPreferences[key] !== newPersistedFilters[key],
      )
    ) {
      setLogbookPreferences(newPersistedFilters);
    }

    const newString = JSON.stringify(newFilter);
    if (newString !== filters) {
      setFilters(newString);
      pagination.handlePageChange(0);
      setScroll();
    }
  };

  const currentFiltersObject = useMemo(() => {
    try {
      return {
        ...JSON.parse(filters),
      };
    } catch (e) {
      return {};
    }
  }, [filters]);

  const getEventPage = useAsyncFetchEndpoint(LOGBOOK_EVENT_PAGE_ENDPOINT);

  const onShowEventContext = useMemo(() => {
    if (!currentFiltersObject.search) return undefined;
    return (event: LogbookEvent) => {
      getEventPage({
        ...pageParameters,
        ...currentFiltersObject,
        limit: pagination.queryParams.limit || 1,
        search: undefined,
        _id: event._id.toString(),
      }).then((page) => {
        setFilters(
          JSON.stringify({
            ...currentFiltersObject,
            search: undefined,
          }),
        );
        setScroll(event._id.toString());
        if (page?.page.index) {
          pagination.handlePageChange(page.page.index - 1);
        }
      });
    };
  }, [
    currentFiltersObject,
    getEventPage,
    pagination,
    setFilters,
    pageParameters,
    setScroll,
  ]);

  const toolbar = (
    <div
      className="d-flex flex-column bg-white mb-1 border-bottom border-primary"
      style={{
        position: 'sticky',
        top: 0,
        zIndex: 10,
      }}
    >
      <div className="d-flex flex-row gap-2 flex-wrap">
        {readOnly ? null : (
          <>
            <NewComment
              enabled={!writingNewComment}
              onClick={() => setWritingNewComment(true)}
              investigationId={investigationId}
            />
            <ManageTagsButton
              instrumentName={instrumentName}
              investigationId={investigationId}
            />
            <TakePicture
              instrumentName={instrumentName}
              investigationId={investigationId}
            />
          </>
        )}
        {investigationId || instrumentName ? (
          <DownloadGroupButton
            filters={{
              ...pageParameters,
              ...currentFiltersObject,
            }}
          />
        ) : null}
        <LogbookHelp />
      </div>

      <Suspense fallback={<Loading />}>
        <LogbookEventsPagination
          filters={{
            ...pageParameters,
            ...currentFiltersObject,
          }}
          pagination={pagination}
          showScrollToTop
          showScrollToBottom
        />
      </Suspense>
    </div>
  );

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label="Logbook">
          <LogbookConfiguration
            filters={currentFiltersObject}
            onUpdate={setFilterObject}
            showStaffSection={investigationId === undefined}
            investigationId={investigationId}
            instrumentName={instrumentName}
          />
        </SideNavElement>
      }
    >
      <div
        style={{
          zIndex: 0,
          position: 'relative',
        }}
      >
        {writingNewComment && (
          <div className="py-2">
            <Suspense fallback={<Loading />}>
              <LogbookEventEditor
                onCanceled={() => setWritingNewComment(false)}
                onClosed={() => setWritingNewComment(false)}
                onSaved={() => {
                  setWritingNewComment(false);
                }}
                createForInstrumentName={instrumentName}
                createForInvestigationId={
                  isNaN(Number(investigationId))
                    ? undefined
                    : Number(investigationId)
                }
              />
            </Suspense>
          </div>
        )}
        {toolbar}
        <Suspense fallback={<Loading />}>
          <LogbookEvents
            filters={{
              ...pageParameters,
              ...currentFiltersObject,
              ...pagination.queryParams,
            }}
            showInvestigation={investigationId === undefined}
            showInstrument={
              instrumentName === undefined && investigationId === undefined
            }
            canEditBeamlineEvents={instrumentName !== undefined}
            onShowEventContext={onShowEventContext}
          />
        </Suspense>

        <Suspense fallback={<Loading />}>
          <LogbookEventsPagination
            filters={{
              ...pageParameters,
              ...currentFiltersObject,
            }}
            pagination={pagination}
            showScrollToTop
          />
        </Suspense>
      </div>
    </WithSideNav>
  );
}
