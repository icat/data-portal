import { useConfig } from '@edata-portal/core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';

export function LogbookHelp() {
  const config = useConfig();
  return (
    <Button href={config.ui.logbook.help} size="sm" target="_blank">
      <FontAwesomeIcon
        icon={faQuestionCircle}
        style={{
          marginRight: 5,
        }}
      />
      Help
    </Button>
  );
}
