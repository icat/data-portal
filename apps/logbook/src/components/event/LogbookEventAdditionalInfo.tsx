import { MetadataTable, useUser } from '@edata-portal/core';
import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Button, OverlayTrigger, Popover, PopoverBody } from 'react-bootstrap';

export function LogbookEventAdditionalInfo({ event }: { event: LogbookEvent }) {
  const user = useUser();

  if (!user?.isAdministrator) return null;

  if (!event.software && !event.machine && !event.datasetName) return null;

  return (
    <OverlayTrigger
      trigger={['hover', 'focus']}
      placement="auto"
      rootClose
      overlay={
        <Popover key={'additional-info-popover'}>
          <PopoverBody>
            <AdditionalInfoContent event={event} />
          </PopoverBody>
        </Popover>
      }
    >
      <Button
        key={'additional-info-content'}
        variant="link"
        size="sm"
        className="p-0 d-flex"
      >
        <FontAwesomeIcon icon={faInfoCircle} />
      </Button>
    </OverlayTrigger>
  );
}

function AdditionalInfoContent({ event }: { event: LogbookEvent }) {
  return (
    <MetadataTable
      parameters={[
        ...(event.software
          ? [{ caption: 'Software', value: event.software }]
          : []),
        ...(event.machine
          ? [{ caption: 'Machine', value: event.machine }]
          : []),

        ...(event.datasetName
          ? [{ caption: 'Dataset name', value: event.datasetName }]
          : []),
      ]}
    />
  );
}
