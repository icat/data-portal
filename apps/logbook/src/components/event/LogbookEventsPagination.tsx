import { Pagination, PaginationMenu } from '@edata-portal/core';
import {
  useGetEndpoint,
  EventListParams,
  LOGBOOK_EVENT_COUNT_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export function LogbookEventsPagination({
  filters,
  pagination,
  showScrollToTop = false,
  showScrollToBottom = false,
}: {
  filters: EventListParams;
  pagination: Pagination;
  showScrollToTop?: boolean;
  showScrollToBottom?: boolean;
}) {
  const countResult = useGetEndpoint({
    endpoint: LOGBOOK_EVENT_COUNT_ENDPOINT,
    params: filters,
  });

  const totalElements = useMemo(() => {
    if (!countResult || !countResult.length) {
      return 0;
    }
    return countResult[0].totalNumber;
  }, [countResult]);

  const totalPages = useMemo(() => {
    return Math.ceil(totalElements / pagination.size);
  }, [totalElements, pagination.size]);

  if (totalElements === 0) {
    return null;
  }

  return (
    <div className="d-flex flex-row gap-2 pb-1 align-items-center">
      <PaginationMenu
        pagination={pagination}
        totalElements={totalElements}
        totalPages={totalPages}
        showScrollToBottom={showScrollToBottom}
        showScrollToTop={showScrollToTop}
      />
    </div>
  );
}
