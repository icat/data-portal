import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type { LogbookItemConfig } from 'components/event/LogbookEventGroup';
import {
  getEventUser,
  type LogbookEventGroupObject,
} from 'components/event/logbookEventGroupHelper';
import { Badge } from 'react-bootstrap';

export function LogbookEventGroupHeader({
  group,
  itemConfig,
}: {
  group: LogbookEventGroupObject;
  itemConfig: LogbookItemConfig;
}) {
  const event = group.events[0];
  const user = getEventUser(event);

  const showInvestigation =
    event.investigationName && itemConfig.showInvestigation;
  const showInstrument = event.instrumentName && itemConfig.showInstrument;

  return (
    <div className="d-flex flex-row bg-secondary w-100 flex-wrap gap-2 p-1">
      {showInvestigation && (
        <Badge bg={'info'}>
          <a
            target={'_blank'}
            href={`/investigation/${event.investigationId}/logbook`}
            rel="noreferrer"
            className="text-white"
          >
            {event.investigationName}
          </a>
        </Badge>
      )}
      {showInstrument && (
        <Badge bg={'info'}>
          <a
            target={'_blank'}
            href={`/instruments/${event.instrumentName}/logbook`}
            rel="noreferrer"
            className="text-white"
          >
            {event.instrumentName}
          </a>
        </Badge>
      )}

      {user && (
        <Badge bg="light">
          <i>
            <FontAwesomeIcon className="me-2" icon={faUser} /> {user}
          </i>
        </Badge>
      )}
    </div>
  );
}
