import { LogbookEventGroups } from 'components/event/LogbookEventGroups';
import { formatDateToDayAndTime } from '@edata-portal/core';
import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { useMemo, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';

export function LogbookEventHistory({ event }: { event: LogbookEvent }) {
  const [showModal, setShowModal] = useState(false);

  if (!event.previousVersionEvent) {
    return null;
  }

  const onShowModal = () => {
    setShowModal(true);
  };
  const onHideModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button
        className="p-0"
        variant="link"
        style={{
          fontSize: 'smaller',
        }}
        onClick={() => {
          onShowModal();
        }}
      >
        <i>edited on {formatDateToDayAndTime(event.updatedAt)}</i>
      </Button>
      <VersionListModal
        event={event}
        show={showModal}
        onShowModal={onShowModal}
        onHideModal={onHideModal}
      />
    </>
  );
}

function VersionListModal({
  event,
  show,
  onShowModal,
  onHideModal,
}: {
  event: LogbookEvent;
  show: boolean;
  onShowModal: () => void;
  onHideModal: () => void;
}) {
  const versions = useMemo(() => {
    const versions: LogbookEvent[] = [event];
    let currentEvent = event;
    while (currentEvent.previousVersionEvent) {
      versions.push(currentEvent.previousVersionEvent);
      currentEvent = currentEvent.previousVersionEvent;
    }
    return versions;
  }, [event]);

  return (
    <Modal show={show} size="lg" onShow={onShowModal} onHide={onHideModal}>
      <Modal.Header closeButton>
        <Modal.Title>Log history</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div
          className="border border-primary"
          style={{
            overflow: 'hidden',
          }}
        >
          <LogbookEventGroups
            events={versions}
            itemConfig={{
              showInvestigation: false,
              showInstrument: false,
              canEditBeamlineEvents: false,
              onShowEventContext: undefined,
              showEditButton: false,
              showHistory: false,
              showUpdateDate: true,
            }}
          />
        </div>
      </Modal.Body>
    </Modal>
  );
}
