import { LogbookEventGroups } from 'components/event/LogbookEventGroups';
import type { LogbookEvent } from '@edata-portal/icat-plus-api';

export function LogbookDaySection({
  day,
  events,
  showInvestigation,
  showInstrument,
  canEditBeamlineEvents,
  onShowEventContext = undefined,
  search,
}: {
  day: string;
  events: LogbookEvent[];
  showInvestigation: boolean;
  showInstrument: boolean;
  canEditBeamlineEvents: boolean;
  onShowEventContext?: (event: LogbookEvent) => void;
  search?: string;
}) {
  return (
    <>
      <div
        style={{
          width: '100%',
        }}
        className="text-center bg-primary text-white p-2"
      >
        {day}
      </div>
      <LogbookEventGroups
        events={events}
        itemConfig={{
          showInvestigation,
          showInstrument,
          canEditBeamlineEvents,
          onShowEventContext,
          search,
          showEditButton: true,
          showHistory: true,
          showUpdateDate: false,
        }}
      />
    </>
  );
}
