import { FullScreen, useLocalStorageValue } from '@edata-portal/core';
import {
  DOWNLOAD_FILE_ENDPOINT,
  LogbookEvent,
  LOGBOOK_EVENT_CREATE_ENDPOINT,
  LogbookTag,
  LOGBOOK_EVENT_UPDATE_ENDPOINT,
  UPLOAD_FILE_ENDPOINT,
  useEndpointURL,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { faCheck, faWarning } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Editor } from '@tinymce/tinymce-react';
import { EDITION_MODE_TINYMCE_CONFIG } from 'components/event/edit/config';
import { TagsSelection } from 'components/event/edit/TagSelection';

import { useCallback, useMemo, useRef, useState } from 'react';
import { Alert, Button, Col, Row } from 'react-bootstrap';

const LOCAL_STORAGE_SAVED_CONTENT_KEY = 'logbook-event-editor-saved-content-';

export default function LogbookEventEditor({
  event,
  onCanceled,
  onSaved,
  onClosed,
  createForInvestigationId,
  createForInstrumentName,
}: {
  event?: LogbookEvent;
  onCanceled?: () => void;
  onSaved?: (newEvent: LogbookEvent) => void;
  onClosed?: () => void;
  createForInvestigationId?: number;
  createForInstrumentName?: string;
}) {
  const updateEvent = useMutateEndpoint({
    endpoint: LOGBOOK_EVENT_UPDATE_ENDPOINT,
    params: {
      ...(event?.investigationId
        ? { investigationId: event?.investigationId }
        : {}),
      ...(event?.instrumentName
        ? { instrumentName: event?.instrumentName }
        : {}),
    },
  });

  const createEvent = useMutateEndpoint({
    endpoint: LOGBOOK_EVENT_CREATE_ENDPOINT,
    params: {
      ...(createForInvestigationId
        ? { investigationId: createForInvestigationId }
        : {}),
      ...(createForInstrumentName
        ? { instrumentName: createForInstrumentName }
        : {}),
    },
  });

  const [fullscreen, setFullscreen] = useState(false);

  const [error, setError] = useState<string | undefined>(undefined);

  const editorRef = useRef(null);

  const originalHtmlContent = useMemo(() => {
    return (event?.content || []).find((content) => content.format === 'html');
  }, [event?.content]);
  const originalTextContent = useMemo(() => {
    return (event?.content || []).find(
      (content) => content.format === 'plainText',
    );
  }, [event?.content]);
  const originalContent = useMemo(() => {
    return originalHtmlContent || originalTextContent;
  }, [originalHtmlContent, originalTextContent]);

  const localStorageKey = useMemo(
    () => LOCAL_STORAGE_SAVED_CONTENT_KEY + (event?._id || 'new'),
    [event],
  );

  const [savedHtmlContent, setSavedHtmlContent] =
    useLocalStorageValue(localStorageKey);

  //we do not want this to update after the editor has been initialized so no dependency
  const initialValue = useMemo(() => {
    const isOriginalNotificationEvent =
      event?.type === 'notification' && !event?.previousVersionEvent;
    return (
      savedHtmlContent ||
      (isOriginalNotificationEvent ? '' : originalContent?.text)
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const [tags, setTags] = useState<LogbookTag[]>(event?.tag || []);

  const [saving, setSaving] = useState(false);

  const onSave = useCallback(
    (close: boolean) => {
      if (!editorRef.current) {
        return;
      }

      const htmlContent = (editorRef.current as any).getContent();
      const textContent = (editorRef.current as any).getContent({
        format: 'text',
      });
      setSaving(true);

      if (!event) {
        const newEvent = buildNewEvent(htmlContent, textContent, tags);
        createEvent.mutate(
          { body: newEvent },
          {
            onSuccess: (data) => {
              setSaving(false);
              setSavedHtmlContent(undefined);
              if (data) onSaved && onSaved(data);
              if (close) onClosed && onClosed();
            },
            onError: (e) => {
              setError(e.message);
              setSaving(false);
            },
          },
        );
      } else {
        const updatedEvent = buildUpdatedEvent(
          event,
          htmlContent,
          textContent,
          tags,
        );
        updateEvent.mutate(
          { body: updatedEvent },
          {
            onSuccess: (data) => {
              setSaving(false);
              setSavedHtmlContent(undefined);
              if (data) onSaved && onSaved(data);
              if (close) onClosed && onClosed();
            },
            onError: (e) => {
              setError(e.message);
              setSaving(false);
            },
          },
        );
      }
    },
    [
      event,
      tags,
      createEvent,
      onSaved,
      setSavedHtmlContent,
      onClosed,
      updateEvent,
    ],
  );

  const uploadEndpoint = useMutateEndpoint({
    endpoint: UPLOAD_FILE_ENDPOINT,
    params: {
      ...(event?.investigationId
        ? { investigationId: event?.investigationId }
        : {}),
      ...(event?.instrumentName
        ? { instrumentName: event?.instrumentName }
        : {}),
      ...(createForInvestigationId
        ? { investigationId: createForInvestigationId }
        : {}),
      ...(createForInstrumentName
        ? { instrumentName: createForInstrumentName }
        : {}),
    },
  });

  const downloadEndpoint = useEndpointURL(DOWNLOAD_FILE_ENDPOINT);

  const imagesUploadHandler = useCallback(
    (blobInfo: any, success: any, failure: any) => {
      return new Promise((resolve, reject) => {
        const data = new FormData();
        data.append('file', blobInfo.blob(), blobInfo.filename());
        if (event?.investigationId)
          data.append('investigationId', event.investigationId.toString());
        if (!event && createForInvestigationId)
          data.append('investigationId', createForInvestigationId.toString());
        if (event?.instrumentName)
          data.append('instrumentName', event?.instrumentName);
        if (!event && createForInstrumentName)
          data.append('instrumentName', createForInstrumentName);
        data.append('creationDate', Date());
        data.append('type', 'attachment');
        data.append('category', 'file');

        uploadEndpoint.mutate(
          { body: data, isFormData: true },
          {
            onSuccess: (value) => {
              if (value?._id) {
                const url = downloadEndpoint({ resourceId: value._id });
                resolve(url);
              }
            },
            onError: (e) => {
              setError(e.message);
              reject(e);
            },
          },
        );
      });
    },
    [
      createForInstrumentName,
      createForInvestigationId,
      downloadEndpoint,
      event,
      uploadEndpoint,
    ],
  );

  const isSaved = useMemo(() => {
    return (
      savedHtmlContent === undefined &&
      !saving &&
      tags.length === event?.tag?.length &&
      tags.every((tag) => event.tag?.findIndex((t) => t._id === tag._id) !== -1)
    );
  }, [event?.tag, savedHtmlContent, saving, tags]);

  const onRefChange = useCallback((node: HTMLDivElement | null) => {
    if (node) {
      node.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      });
    }
  }, []);

  const editor = (
    <Col
      ref={onRefChange}
      style={{
        scrollMarginTop: 100,
      }}
    >
      <Row></Row>
      <Editor
        onInit={(evt: any, editor: any) => (editorRef.current = editor)}
        initialValue={initialValue}
        onEditorChange={(newValue) => {
          setSavedHtmlContent(newValue);
        }}
        init={{
          ...EDITION_MODE_TINYMCE_CONFIG,
          images_upload_handler: imagesUploadHandler as any,
          ...(fullscreen ? { height: 'calc(100vh - 150px)' } : {}),
        }}
      />
      <Row>
        <Col>
          {error && (
            <Alert variant="danger" onClose={() => setError(undefined)}>
              <FontAwesomeIcon icon={faWarning} />
              {error}
            </Alert>
          )}
        </Col>
      </Row>
      <Row>
        <Col
          style={{
            marginTop: '1rem',
          }}
        >
          <TagsSelection
            tags={tags}
            onChange={(tags) => {
              setTags(tags);
            }}
            investigationId={event?.investigationId || createForInvestigationId}
            instrumentName={event?.instrumentName || createForInstrumentName}
          />
        </Col>
      </Row>
      <Row
        className="d-flex justify-content-end g-2"
        style={{ marginTop: '1rem' }}
      >
        <Col xs={'auto'}>
          <span
            className={'p-2 ' + (isSaved ? 'text-success' : 'text-warning')}
          >
            <FontAwesomeIcon icon={isSaved ? faCheck : faWarning} />
            {isSaved ? 'Saved' : 'Unsaved content'}
          </span>
        </Col>
        <Col xs={'auto'}>
          <Button
            onClick={() => {
              setFullscreen(!fullscreen);
            }}
            size={'sm'}
          >
            {fullscreen ? 'Exit fullscreen' : 'Fullscreen'}
          </Button>
        </Col>
        <Col xs={'auto'}>
          <Button
            disabled={saving}
            onClick={() => {
              setFullscreen(false);
              setSavedHtmlContent(undefined);
              onCanceled && onCanceled();
            }}
            size={'sm'}
          >
            Close
          </Button>
        </Col>
        {event && (
          <Col xs={'auto'}>
            <Button
              variant="primary"
              disabled={saving || isSaved}
              onClick={() => onSave(false)}
              size={'sm'}
            >
              Save
            </Button>
          </Col>
        )}
        <Col xs={'auto'}>
          <Button
            variant="primary"
            disabled={saving || isSaved}
            onClick={() => {
              setFullscreen(false);
              onSave(true);
            }}
            size={'sm'}
          >
            Save and close
          </Button>
        </Col>
      </Row>
    </Col>
  );

  return fullscreen ? (
    <FullScreen
      onExitFullscreen={() => setFullscreen(false)}
      showCloseButton={false}
    >
      {editor}
    </FullScreen>
  ) : (
    editor
  );
}

function buildUpdatedEvent(
  src: LogbookEvent,
  htmlContent: string,
  textContent: string,
  tags: LogbookTag[],
) {
  return {
    _id: src._id,
    category: src.category,
    content: [
      {
        format: 'html',
        text: htmlContent,
      },
      {
        format: 'plainText',
        text: textContent,
      },
    ],
    creationDate: Date(),
    type: src.type,
    tag: tags,
    title: null,
    previousVersionEvent: src._id,
  };
}

function buildNewEvent(
  htmlContent: string,
  textContent: string,
  tags: LogbookTag[],
) {
  return {
    category: 'comment',
    content: [
      {
        format: 'html',
        text: htmlContent,
      },
      {
        format: 'plainText',
        text: textContent,
      },
    ],
    creationDate: Date(),
    type: 'annotation',
    tag: tags,
  };
}
