import { ShowIfInvestigationNotReleased } from '@edata-portal/core';
import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Button } from 'react-bootstrap';

export function LogbookEventEditButton({
  event,
  triggerEdit,
  canEditBeamlineEvents,
}: {
  event: LogbookEvent;
  triggerEdit?: () => void;
  canEditBeamlineEvents: boolean;
}) {
  if (event.type === 'broadcast') return null;
  if (!event.investigationId) {
    if (canEditBeamlineEvents)
      return <EditEventButton triggerEdit={triggerEdit} />;
    return null;
  }

  return (
    <ShowIfInvestigationNotReleased
      investigationId={event.investigationId.toString()}
    >
      <EditEventButton triggerEdit={triggerEdit} />
    </ShowIfInvestigationNotReleased>
  );
}

function EditEventButton({ triggerEdit }: { triggerEdit?: () => void }) {
  return (
    <Button
      variant="link"
      className="p-0 d-flex"
      onClick={triggerEdit}
      test-id="logbook-event-edit-button"
    >
      <FontAwesomeIcon icon={faEdit} />
    </Button>
  );
}
