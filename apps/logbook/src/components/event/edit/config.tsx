// TinyMCE so the global var exists
import tinymce from 'tinymce'; // eslint-disable-line @typescript-eslint/no-unused-vars
/* Default icons are required. After that, import custom icons if applicable */
import 'tinymce/icons/default/icons.min.js';

/* Required TinyMCE components */
import 'tinymce/themes/silver/theme.min.js';
import 'tinymce/models/dom/model.min.js';

/* Import a skin (can be a custom skin instead of the default) */
import 'tinymce/skins/ui/oxide/skin.js';

/* Import plugins */
import 'tinymce/plugins/link';
import 'tinymce/plugins/image';
import 'tinymce/plugins/table';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/media';
import 'tinymce/plugins/importcss';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/codesample';
import 'tinymce/plugins/emoticons';
import 'tinymce/plugins/emoticons/js/emojis';

/* content UI CSS is required */
import 'tinymce/skins/ui/oxide/content.js';

/* The default content CSS can be changed or replaced with appropriate CSS for the editor content. */
import 'tinymce/skins/content/default/content.js';

export const EDITION_MODE_TINYMCE_CONFIG = {
  height: '60vh',
  menubar: false,
  plugins: [
    'link',
    'image',
    'table',
    'lists',
    'fullscreen',
    'media',
    'importcss',
    'charmap',
    'codesample',
  ],
  toolbar:
    'undo redo fontsize bold italic charmap subscript superscript alignleft aligncenter alignright alignjustify bullist numlist outdent indent forecolor backcolor link image table codesample',

  content_style:
    'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',

  file_picker_types: 'file image media',
  codesample_languages: [
    { text: 'Python', value: 'python' },
    { text: 'HTML/XML', value: 'markup' },
    { text: 'JavaScript', value: 'javascript' },
    { text: 'Java', value: 'java' },
    { text: 'C', value: 'c' },
  ],
  skin_url: 'default',
  content_css: 'default',
};
