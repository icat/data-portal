import {
  LogbookTag,
  LOGBOOK_CREATE_TAG_ENDPOINT,
  LOGBOOK_TAG_LIST_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { Tag } from 'components/tag/Tag';

import ReactSelect from 'react-select/creatable';

export function TagsSelection({
  tags,
  onChange,
  investigationId,
  instrumentName,
}: {
  tags: LogbookTag[];
  onChange: (tags: LogbookTag[]) => void;
  investigationId?: number;
  instrumentName?: string;
}) {
  const tagList = useGetEndpoint({
    endpoint: LOGBOOK_TAG_LIST_ENDPOINT,
    params: {
      ...(investigationId
        ? { investigationId: investigationId.toString() }
        : {}),
      ...(!investigationId && instrumentName ? { instrumentName } : {}),
    },
  });

  const createTag = useMutateEndpoint({
    endpoint: LOGBOOK_CREATE_TAG_ENDPOINT,
    params: {
      ...(investigationId ? { investigationId: investigationId } : {}),
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
    },
  });

  return (
    <ReactSelect
      placeholder="Select tags..."
      options={(tagList || [])
        //disable global tags
        .filter((t) => t.investigationId || t.instrumentName)
        //keep only tags which name matches list of selected tags
        .filter((t) => !tags.find((tag) => tag.name === t.name))
        //Remove duplicates
        .filter((s, i, a) => a.findIndex((s2) => s2.name === s.name) === i)
        .map((tag) => ({
          value: tag,
          label: tag.name,
        }))}
      value={tags.map((tag) => ({
        value: tag,
        label: tag.name,
      }))}
      onCreateOption={(name) => {
        const newTag = {
          name,
          investigationId,
          instrumentName,
        };
        createTag
          .mutateAsync({
            body: newTag,
          })
          .then((c) => {
            if (!c) return;
            onChange([...tags, c]);
          });
      }}
      formatCreateLabel={(name) => `Create tag "${name}"`}
      isMulti
      isClearable
      isSearchable
      formatOptionLabel={(option) => {
        if ('__isNew__' in option) {
          return option.label;
        }
        return <Tag tag={option.value} />;
      }}
      menuPlacement="top"
      onChange={(selected) =>
        onChange(
          selected
            .filter(
              (s, i) =>
                selected.findIndex((s2) => s2.value.name === s.value.name) ===
                i,
            )
            .map((s) => s.value),
        )
      }
    />
  );
}
