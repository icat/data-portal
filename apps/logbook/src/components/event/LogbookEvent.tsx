import { useEffect, useMemo } from 'react';
import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { getTextColor } from 'helpers/color';
import { formatHtml } from 'helpers/html';
import { LogbookEventEditButton } from 'components/event/edit/LogbookEventEditButton';
import { LogbookEventHistory } from 'components/event/LogbookEventHistory';
import { Button } from 'react-bootstrap';
import { LogbookEventAdditionalInfo } from 'components/event/LogbookEventAdditionalInfo';
import { Tag } from 'components/tag/Tag';
import type { LogbookItemConfig } from 'components/event/LogbookEventGroup';

import Prism from 'prismjs';
import 'prismjs/themes/prism-okaidia.min.css';
import 'prismjs/components/prism-python';
import 'prismjs/components/prism-c';
import 'prismjs/components/prism-cshtml';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-javascript';
import { eventIsAnnotationOnNotification, getEventStyle } from 'helpers/event';

export function LogbookEventViewer({
  item,
  itemConfig,
  setEdit,
}: {
  item: LogbookEvent;
  itemConfig: LogbookItemConfig;
  setEdit?: (edit: boolean) => void;
}) {
  return (
    <div
      className="d-flex flex-row gap-1 align-items-start"
      test-id="logbook-event"
    >
      {itemConfig.showEditButton && setEdit ? (
        <LogbookEventEditButton
          canEditBeamlineEvents={itemConfig.canEditBeamlineEvents}
          event={item}
          triggerEdit={() => setEdit(true)}
        />
      ) : null}
      <div className="d-flex flex-column w-100 overflow-hidden">
        <LogbookNotificationAnnotationViewer event={item} />

        <div className="d-flex flex-column w-100 overflow-auto">
          <LogbookEventContentViewer event={item} search={itemConfig.search} />
        </div>
        <div className="d-flex flex-row gap-2">
          {itemConfig.onShowEventContext && (
            <Button
              variant="link"
              className="p-0"
              style={{
                fontSize: 'smaller',
              }}
              onClick={() => {
                itemConfig.onShowEventContext &&
                  itemConfig.onShowEventContext(item);
              }}
            >
              Show context
            </Button>
          )}
          {itemConfig.showHistory && <LogbookEventHistory event={item} />}
        </div>
      </div>
      <div className="d-none d-sm-flex flex-column">
        <TagsSection event={item} />
      </div>
      <LogbookEventAdditionalInfo event={item} />
    </div>
  );
}

function TagsSection({ event }: { event: LogbookEvent }) {
  return (
    <>
      {event.tag?.map((tag) => (
        <small
          key={tag._id}
          style={{
            fontSize: 10,
          }}
        >
          <Tag tag={tag} />
        </small>
      ))}
    </>
  );
}

function LogbookEventContentViewer({
  event,
  search,
}: {
  event: LogbookEvent;
  search?: string;
}) {
  const htmlContent = useMemo(() => {
    return (event.content || []).find((content) => content.format === 'html');
  }, [event.content]);
  const textContent = useMemo(() => {
    return (event.content || []).find(
      (content) => content.format === 'plainText',
    );
  }, [event.content]);

  const category = eventIsAnnotationOnNotification(event)
    ? 'comment'
    : event.category;

  const eventStyle = getEventStyle(event);

  return (
    <div
      className={getTextColor(category) + ' ' + eventStyle.className}
      style={{ ...eventStyle.style, overflowY: 'auto' }}
    >
      <LogbookHTMLEventContentViewer
        content={htmlContent?.text || textContent?.text || ''}
        search={search}
      />
    </div>
  );
}

function LogbookHTMLEventContentViewer({
  content,
  search,
}: {
  content: string;
  search?: string;
}) {
  const formatted = formatHtml(content, search);

  useEffect(() => {
    Prism.highlightAll();
  }, []);

  return (
    <div
      dangerouslySetInnerHTML={{
        __html: formatted,
      }}
    />
  );
}

function LogbookNotificationAnnotationViewer({
  event,
}: {
  event: LogbookEvent;
}) {
  if (!eventIsAnnotationOnNotification(event)) return null;

  //find source event
  let sourceEvent = event.previousVersionEvent!;
  while (sourceEvent.previousVersionEvent) {
    sourceEvent = sourceEvent.previousVersionEvent;
  }

  return (
    <blockquote
      style={{
        borderLeft: '5px solid black',
        paddingLeft: 5,
      }}
    >
      <LogbookEventContentViewer event={sourceEvent} />
    </blockquote>
  );
}
