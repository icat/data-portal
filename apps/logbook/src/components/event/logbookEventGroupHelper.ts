import type { LogbookEvent } from '@edata-portal/icat-plus-api';

export function groupEventBySource(
  events: LogbookEvent[],
  showInstrument: boolean,
  showInvestigation: boolean,
) {
  return events.reduce((acc, event) => {
    const lastGroup = acc[acc.length - 1];

    const eventSource = getEventSourceKey(
      event,
      showInstrument,
      showInvestigation,
    );

    if (!lastGroup) {
      acc.push({
        source: eventSource,
        events: [event],
      });
      return acc;
    }

    const lastEventSource = lastGroup.source;

    if (lastEventSource !== eventSource) {
      acc.push({
        source: eventSource,
        events: [event],
      });
    } else {
      lastGroup.events.push(event);
    }
    return acc;
  }, [] as LogbookEventGroupObject[]);
}

export type LogbookEventGroupObject = {
  source: string | undefined;
  events: LogbookEvent[];
};

function getEventSourceKey(
  event: LogbookEvent,
  showInstrument: boolean,
  showInvestigation: boolean,
) {
  const user = getEventUser(event);
  const investigation = showInvestigation ? event.investigationName : undefined;
  const instrument = showInstrument ? event.instrumentName : undefined;

  const sourceElements = [user, investigation, instrument]
    .filter((el): el is string => el !== undefined && el !== null)
    .filter((el) => {
      return el.toLocaleLowerCase().trim().length;
    });

  if (sourceElements.length === 0) {
    return undefined;
  }

  return sourceElements.join('-');
}

export function getEventUser(event: LogbookEvent) {
  return event.fullName &&
    event.fullName.toLocaleLowerCase().trim() !== 'unknown'
    ? event.fullName
    : event.username;
}
