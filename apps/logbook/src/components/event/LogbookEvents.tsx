import { formatDateToDay } from '@edata-portal/core';
import {
  LOGBOOK_EVENT_LIST_ENDPOINT,
  useGetEndpoint,
  EventListParams,
  LogbookEvent,
} from '@edata-portal/icat-plus-api';
import { LogbookDaySection } from 'components/event/LogbookDaySection';
import { useMemo } from 'react';
import { Alert } from 'react-bootstrap';

export function LogbookEvents({
  filters,
  showInvestigation,
  showInstrument,
  canEditBeamlineEvents,
  onShowEventContext = undefined,
}: {
  filters: EventListParams;
  showInvestigation: boolean;
  showInstrument: boolean;
  canEditBeamlineEvents: boolean;
  onShowEventContext?: (event: LogbookEvent) => void;
}) {
  const events = useGetEndpoint({
    endpoint: LOGBOOK_EVENT_LIST_ENDPOINT,
    params: filters,
    default: [] as LogbookEvent[],
  });

  const eventsByDay = useMemo(() => {
    const eventsByDay: {
      day: string;
      events: LogbookEvent[];
    }[] = [];
    events.forEach((event) => {
      const day = formatDateToDay(event.createdAt);
      if (!day) return;
      const existingDay = eventsByDay.find((d) => d.day === day);
      if (existingDay) {
        existingDay.events.push(event);
      } else {
        eventsByDay.push({
          day,
          events: [event],
        });
      }
    });
    return eventsByDay;
  }, [events]);

  if (events.length === 0) {
    return (
      <Alert variant="info" style={{ marginTop: '12px' }}>
        No logs have been found.
      </Alert>
    );
  }

  return (
    <div
      className="border border-primary rounded"
      style={{
        overflow: 'hidden',
      }}
    >
      <div
        style={{
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          overflow: 'hidden',
          gap: 2,
        }}
      >
        {eventsByDay.map((day) => {
          return (
            <LogbookDaySection
              key={day.day}
              day={day.day}
              events={day.events}
              showInvestigation={showInvestigation}
              showInstrument={showInstrument}
              canEditBeamlineEvents={canEditBeamlineEvents}
              onShowEventContext={onShowEventContext}
              search={filters.search}
            />
          );
        })}
      </div>
    </div>
  );
}
