import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getEventUser } from 'components/event/logbookEventGroupHelper';

export function EventSource({
  event,
  showInvestigation,
  showInstrument,
}: {
  event: LogbookEvent;
  showInvestigation: boolean;
  showInstrument: boolean;
}) {
  const user = getEventUser(event);

  return (
    <div
      className="pb-1"
      style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          gap: '5px',
        }}
      >
        {user && (
          <small
            className="border rounded bg-light"
            style={{
              padding: '0px 5px',
            }}
          >
            <i>
              <FontAwesomeIcon icon={faUser} /> {user}
            </i>
          </small>
        )}
      </div>
      <small>
        {event.investigationName && showInvestigation && (
          <a
            target={'_blank'}
            href={`/investigation/${event.investigationId}/logbook`}
            rel="noreferrer"
          >
            {event.investigationName}
          </a>
        )}
        {event.instrumentName && showInvestigation && showInstrument && (
          <>
            {event.investigationName && <span>{' - '}</span>}
            <a
              target={'_blank'}
              href={`/instruments/${event.instrumentName}/logbook`}
              rel="noreferrer"
            >
              {event.instrumentName}
            </a>
          </>
        )}
      </small>
    </div>
  );
}
