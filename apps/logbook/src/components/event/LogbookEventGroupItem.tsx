import {
  useScroll,
  LazyWrapper,
  Loading,
  formatDateToDayAndTime,
  formatDateToTime,
  formatDateToDay,
} from '@edata-portal/core';
import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { LogbookEventViewer } from 'components/event/LogbookEvent';
import type { LogbookItemConfig } from 'components/event/LogbookEventGroup';
import LogbookEventEditor from 'components/event/edit/LogBookEventEditor';
import { useState } from 'react';
import OverlayTrigger from 'react-bootstrap/esm/OverlayTrigger';
import Popover from 'react-bootstrap/esm/Popover';
import PopoverBody from 'react-bootstrap/esm/PopoverBody';

export function LogbookEventGroupItem({
  item,
  itemConfig,
}: {
  item: LogbookEvent;
  itemConfig: LogbookItemConfig;
}) {
  const [editing, setEditing] = useState(false);
  const scroll = useScroll<HTMLTableRowElement>(item._id);

  if (item.type === 'attachment') return null;

  const editComponent = (
    <LazyWrapper placeholder={<Loading />}>
      <LogbookEventEditor
        event={item}
        onCanceled={() => setEditing(false)}
        onClosed={() => {
          setEditing(false);
        }}
      />
    </LazyWrapper>
  );

  const content = (
    <LogbookEventViewer
      item={item}
      itemConfig={itemConfig}
      setEdit={setEditing}
    />
  );

  return (
    <tr
      ref={scroll.ref}
      className={`logbook-event-group-item ${
        scroll.isCurrent ? 'border border-info border-3' : ''
      }`}
    >
      <td
        className="text-center text-muted"
        style={{
          width: itemConfig.showUpdateDate ? 75 : 65,
          verticalAlign: 'top',
        }}
      >
        <div className={`d-flex flex-column text-center pointer`}>
          <OverlayTrigger
            trigger={['hover', 'focus']}
            placement="auto"
            rootClose
            overlay={
              <Popover key={'additional-info-popover'}>
                <PopoverBody>
                  Date: {formatDateToDay(item.updatedAt)}
                </PopoverBody>
              </Popover>
            }
          >
            <span className="monospace">
              {itemConfig.showUpdateDate
                ? formatDateToDayAndTime(item.updatedAt)
                : formatDateToTime(item.createdAt)}
            </span>
          </OverlayTrigger>
        </div>
      </td>
      <td>{editing ? editComponent : content}</td>
    </tr>
  );
}
