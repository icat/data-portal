import { ShowIfInvestigationNotReleased } from '@edata-portal/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';

export function NewComment({
  enabled,
  onClick,
  investigationId,
}: {
  enabled: boolean;
  onClick: () => void;
  investigationId?: string;
}) {
  if (investigationId) {
    return (
      <ShowIfInvestigationNotReleased investigationId={investigationId}>
        <NewCommentButton enabled={enabled} onClick={onClick} />
      </ShowIfInvestigationNotReleased>
    );
  }
  return <NewCommentButton enabled={enabled} onClick={onClick} />;
}

function NewCommentButton({
  enabled,
  onClick,
}: {
  enabled: boolean;
  onClick: () => void;
}) {
  return (
    <Button variant="primary" onClick={onClick} size={'sm'} disabled={!enabled}>
      <FontAwesomeIcon icon={faPlus} /> New comment
    </Button>
  );
}
