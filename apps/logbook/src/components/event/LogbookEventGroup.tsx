import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import { LogbookEventGroupHeader } from 'components/event/LogbookEventGroupHeader';
import type { LogbookEventGroupObject } from 'components/event/logbookEventGroupHelper';
import { LogbookEventGroupItem } from 'components/event/LogbookEventGroupItem';
import { Table } from 'react-bootstrap';

export type LogbookItemConfig = {
  showInvestigation: boolean;
  showInstrument: boolean;
  showHistory: boolean;
  showUpdateDate: boolean;
  showEditButton: boolean;
  canEditBeamlineEvents: boolean;
  onShowEventContext?: (event: LogbookEvent) => void;
  search?: string;
};

export function LogbookEventGroup({
  group,
  itemConfig,
}: {
  group: LogbookEventGroupObject;
  itemConfig: LogbookItemConfig;
}) {
  return (
    <div
      className={
        group.source ? 'border-start border-3 border-secondary' : undefined
      }
    >
      {group.source ? (
        <LogbookEventGroupHeader group={group} itemConfig={itemConfig} />
      ) : null}
      <div className="logbook-event-group-content">
        <Table
          hover
          className="w-100 m-0"
          size="sm"
          style={{
            tableLayout: 'fixed',
          }}
        >
          <tbody>
            {group.events.map((event) => {
              return (
                <LogbookEventGroupItem
                  key={event._id}
                  item={event}
                  itemConfig={itemConfig}
                />
              );
            })}
          </tbody>
        </Table>
      </div>
    </div>
  );
}
