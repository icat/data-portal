import type { LogbookEvent } from '@edata-portal/icat-plus-api';
import {
  LogbookEventGroup,
  LogbookItemConfig,
} from 'components/event/LogbookEventGroup';
import { groupEventBySource } from 'components/event/logbookEventGroupHelper';
import { useMemo } from 'react';

export function LogbookEventGroups({
  events,
  itemConfig,
}: {
  events: LogbookEvent[];
  itemConfig: LogbookItemConfig;
}) {
  const groups = useMemo(
    () =>
      groupEventBySource(
        events,
        itemConfig.showInstrument,
        itemConfig.showInvestigation,
      ),
    [events, itemConfig.showInstrument, itemConfig.showInvestigation],
  );

  return (
    <>
      {groups.map((group) => {
        const key = group.events[group.events.length - 1]._id;
        return (
          <LogbookEventGroup key={key} group={group} itemConfig={itemConfig} />
        );
      })}
    </>
  );
}
