import type { LogbookTag } from '@edata-portal/icat-plus-api';
import { faTag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const DEFAULT_TAG_COLOR = '#4f4f4f';

export function Tag({ tag }: { tag: LogbookTag }) {
  const hasColor =
    tag.color !== undefined &&
    tag.color !== null &&
    tag.color.trim().length > 0;

  return (
    <span
      className={'border rounded text-white'}
      style={{
        padding: '1px 5px',
        backgroundColor: hasColor ? tag.color : DEFAULT_TAG_COLOR,
        display: 'flex',
        alignItems: 'center',
        gap: '2px',
        flexDirection: 'row',
      }}
    >
      <FontAwesomeIcon icon={faTag} /> {tag.name.toLocaleUpperCase()}
    </span>
  );
}
