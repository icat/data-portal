import {
  LogbookTag,
  LOGBOOK_CREATE_TAG_ENDPOINT,
  LOGBOOK_UPDATE_TAG_ENDPOINT,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { faCancel, faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DEFAULT_TAG_COLOR } from 'components/tag/Tag';
import { useMemo, useState } from 'react';
import { Button, Form } from 'react-bootstrap';

export function TagEditor({
  tag,
  onEdited,
  onCanceled,
  investigationId,
  instrumentName,
}: {
  tag?: LogbookTag;
  onEdited: () => void;
  onCanceled: () => void;
  investigationId?: number;
  instrumentName?: string;
}) {
  const [name, setName] = useState(tag?.name || '');
  const [color, setColor] = useState(tag?.color || DEFAULT_TAG_COLOR);

  const canSave = useMemo(() => {
    return name.length > 0;
  }, [name]);

  const updateTag = useMutateEndpoint({
    endpoint: LOGBOOK_UPDATE_TAG_ENDPOINT,
    params: {
      ...(tag ? { id: tag?._id } : {}),
      ...(investigationId ? { investigationId: investigationId } : {}),
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
    },
  });

  const createTag = useMutateEndpoint({
    endpoint: tag ? LOGBOOK_UPDATE_TAG_ENDPOINT : LOGBOOK_CREATE_TAG_ENDPOINT,
    params: {
      ...(investigationId ? { investigationId: investigationId } : {}),
      ...(instrumentName ? { instrumentName: instrumentName } : {}),
    },
  });

  const save = () => {
    if (tag) {
      updateTag
        .mutateAsync({
          body: {
            _id: tag._id,
            name,
            color,
            investigationId: tag.investigationId,
            instrumentName: tag.instrumentName,
          },
        })
        .then(() => {
          onEdited();
        });
    } else {
      createTag
        .mutateAsync({
          body: {
            name,
            color,
            investigationId: investigationId,
            instrumentName: instrumentName,
          },
        })
        .then(() => {
          onEdited();
        });
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        gap: '5px',
        flexWrap: 'nowrap',
      }}
    >
      <Button
        variant="link"
        size="sm"
        className="p-0 text-success"
        onClick={save}
        disabled={!canSave}
      >
        <FontAwesomeIcon icon={faCheck} />
      </Button>
      <Button
        variant="link"
        size="sm"
        className="p-0 text-danger"
        onClick={onCanceled}
      >
        <FontAwesomeIcon icon={faCancel} />
      </Button>
      <Form.Control
        type="text"
        size="sm"
        value={name}
        onChange={(e) => setName(e.target.value)}
        style={{
          backgroundColor: color,
          color: 'white',
        }}
      />
      <Form.Control
        type="color"
        size="sm"
        value={color}
        onChange={(e) => setColor(e.target.value)}
      />
    </div>
  );
}
