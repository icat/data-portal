import {
  isReleasedInvestigation,
  LazyWrapper,
  Loading,
  immutableArray,
} from '@edata-portal/core';
import {
  INVESTIGATION_LIST_ENDPOINT,
  LogbookTag,
  LOGBOOK_TAG_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { faEdit, faTags } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Tag } from 'components/tag/Tag';
import { TagEditor } from 'components/tag/TagEditor';
import { useState } from 'react';
import { Button, Card, Col, Modal, Row } from 'react-bootstrap';

export function ManageTagsButton({
  investigationId,
  instrumentName,
}: {
  investigationId?: string;
  instrumentName?: string;
}) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const investigationList = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId || '',
    },
    skipFetch: !investigationId,
  });
  const investigation =
    investigationList && investigationList.length
      ? investigationList[0]
      : undefined;

  const isReleased = isReleasedInvestigation(investigation);

  const instrument = instrumentName || investigation?.instrument?.name;

  if (investigation && isReleased) return null;

  if (!instrument) return null;

  return (
    <>
      <Button variant="primary" size="sm" onClick={handleShow}>
        <FontAwesomeIcon icon={faTags} /> Edit Tags
      </Button>

      <Modal size="xl" show={show} onHide={handleClose} backdrop="static">
        <Modal.Header closeButton>
          <Modal.Title>Manage tags</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LazyWrapper placeholder={<Loading />}>
            <ManageTags
              investigationId={investigationId}
              instrumentName={instrument}
            />
          </LazyWrapper>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleClose}>Close</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

function ManageTags({
  investigationId,
  instrumentName,
}: {
  investigationId?: string;
  instrumentName: string;
}) {
  const tags = useGetEndpoint({
    endpoint: LOGBOOK_TAG_LIST_ENDPOINT,
    params: {
      ...(instrumentName ? { instrumentName } : {}),
      ...(investigationId ? { investigationId } : {}),
    },
  });

  const investigationTags = tags?.filter((tag) => tag.investigationId);
  const instrumentTags = tags?.filter(
    (tag) => tag.instrumentName && !tag.investigationId,
  );
  const systemTags = tags?.filter(
    (tag) => !tag.instrumentName && !tag.investigationId,
  );

  const numberInvestigationId = isNaN(Number(investigationId))
    ? undefined
    : Number(investigationId);

  return (
    <Row>
      {investigationId && (
        <Col>
          <TagManageList
            tags={investigationTags ?? []}
            title="Experiment session"
            description="Participants of an experiment session can create their own tags"
            editable
            investigationId={numberInvestigationId}
          />
        </Col>
      )}
      <Col>
        <TagManageList
          tags={instrumentTags ?? []}
          title="Beamline"
          description="These tags are only editable by beamline staff and are applicable to all experiments of a beamline"
          editable
          instrumentName={instrumentName}
        />
      </Col>
      <Col>
        <TagManageList
          tags={systemTags ?? []}
          title="System"
          description="These tags are only editable by administrator and applicable to all the experiments"
          editable={false}
        />
      </Col>
    </Row>
  );
}

function TagManageList({
  tags,
  title,
  description,
  editable,
  investigationId,
  instrumentName,
}: {
  tags: LogbookTag[];
  title: string;
  description: string;
  editable: boolean;
  investigationId?: number;
  instrumentName?: string;
}) {
  const sorted = immutableArray(tags)
    .sort((a, b) => a.name.localeCompare(b.name))
    .toArray();

  const [creating, setCreating] = useState(false);

  return (
    <Card>
      <Card.Header>
        <h5>{title}</h5>
        <sub>{description}</sub>
      </Card.Header>
      <Card.Body>
        {editable && (
          <>
            <Button size={'sm'} onClick={() => setCreating(true)}>
              Create new tag
            </Button>
            {creating && (
              <TagEditor
                onEdited={() => setCreating(false)}
                onCanceled={() => setCreating(false)}
                investigationId={investigationId}
                instrumentName={instrumentName}
              />
            )}
            <hr />
          </>
        )}
        {sorted.map((tag) =>
          editable && (investigationId || instrumentName) ? (
            <EditTagButton
              investigationId={investigationId}
              key={tag._id}
              tag={tag}
              instrumentName={instrumentName}
            />
          ) : (
            <div className="d-flex" key={tag._id}>
              <Tag tag={tag} />
            </div>
          ),
        )}
      </Card.Body>
    </Card>
  );
}

function EditTagButton({
  tag,
  investigationId,
  instrumentName,
}: {
  tag: LogbookTag;
  investigationId?: number;
  instrumentName?: string;
}) {
  const [editing, setEditing] = useState(false);

  if (editing)
    return (
      <TagEditor
        tag={tag}
        onEdited={() => setEditing(false)}
        onCanceled={() => setEditing(false)}
        investigationId={investigationId}
        instrumentName={instrumentName}
      />
    );

  return (
    <Col>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          gap: '5px',
          flexWrap: 'nowrap',
        }}
      >
        <Button
          variant="link"
          size="sm"
          className="p-0"
          onClick={() => setEditing(true)}
        >
          <FontAwesomeIcon icon={faEdit} />
        </Button>
        <Tag tag={tag} />
      </div>
    </Col>
  );
}
