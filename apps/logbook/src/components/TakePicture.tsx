import { ShowIfInvestigationNotReleased, useNotify } from '@edata-portal/core';
import {
  useMutateEndpoint,
  LOGBOOK_EVENT_CREATE_BASE64_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { faCamera, faClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useCallback, useRef, useState } from 'react';
import { Button } from 'react-bootstrap';
import Webcam from 'react-webcam';

export function TakePicture({
  instrumentName,
  investigationId,
}: {
  instrumentName?: string;
  investigationId?: string;
}) {
  if (investigationId) {
    return (
      <ShowIfInvestigationNotReleased investigationId={investigationId}>
        <TakePictureButton
          instrumentName={instrumentName}
          investigationId={investigationId}
        />
      </ShowIfInvestigationNotReleased>
    );
  }
  return <TakePictureButton instrumentName={instrumentName} />;
}

function TakePictureButton({
  instrumentName,
  investigationId,
}: {
  instrumentName?: string;
  investigationId?: string;
}) {
  const [takingPicture, setTakingPicture] = useState(false);

  const create = useMutateEndpoint({
    endpoint: LOGBOOK_EVENT_CREATE_BASE64_ENDPOINT,
    params: {
      ...(instrumentName ? { instrumentName } : {}),
      ...(investigationId ? { investigationId: Number(investigationId) } : {}),
    },
  });

  return (
    <>
      <Button
        variant="primary"
        onClick={() => setTakingPicture(true)}
        size="sm"
      >
        Take Picture
      </Button>
      {takingPicture && (
        <FullscreenCamera
          onPicture={(picture) => {
            create.mutateAsync({ body: { base64: picture } }).then(() => {
              setTakingPicture(false);
            });
          }}
          onCanceled={() => setTakingPicture(false)}
        />
      )}
    </>
  );
}

function FullscreenCamera({
  onPicture,
  onCanceled,
}: {
  onPicture?: (picture: string) => void;
  onCanceled?: () => void;
}) {
  const webcamRef = useRef<Webcam>(null);
  const [cameraError, setCameraError] = useState('');
  const notify = useNotify();

  const capture = useCallback(() => {
    if (!webcamRef.current) return;
    const imageSrc = webcamRef.current.getScreenshot();
    if (onPicture && imageSrc) onPicture(imageSrc);
  }, [onPicture]);

  const onMediaError = (error: string | DOMException) => {
    setCameraError(error.toString());
    notify({
      title: <strong>Failed to find the camera</strong>,
      message: (
        <>
          <h5>The camera has not been detected</h5>
          <p>{cameraError}</p>
        </>
      ),
      type: 'danger',
    });
    if (onCanceled) {
      onCanceled();
    }
  };

  if (cameraError && cameraError.length > 0) {
    return null;
  }

  return (
    <div
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        zIndex: 1000,
        padding: 10,
      }}
    >
      <div
        style={{
          width: '100%',
          maxHeight: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Webcam
          ref={webcamRef}
          audio={false}
          width={'100%'}
          height={'100%'}
          screenshotFormat="image/png"
          videoConstraints={{
            facingMode: 'environment',
          }}
          onUserMediaError={(error) => onMediaError(error)}
        />
      </div>
      <div
        style={{
          position: 'absolute',
          bottom: 50,
          left: 0,
          right: 0,
          height: 100,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: 5,
        }}
      >
        <Button size="lg" variant="info" onClick={capture}>
          <FontAwesomeIcon icon={faCamera} />
        </Button>
        <Button
          size="lg"
          variant="warning"
          onClick={onCanceled ? onCanceled : undefined}
        >
          <FontAwesomeIcon icon={faClose} />
        </Button>
      </div>
    </div>
  );
}
