import { Progress, TanstackBootstrapTable } from '@edata-portal/core';
import {
  useReactTable,
  getCoreRowModel,
  getFilteredRowModel,
  getSortedRowModel,
  getPaginationRowModel,
} from '@tanstack/react-table';
import { useLogbookInvestigationStats } from 'components/statistics/useLogbookInvestigationStats';
import { useMemo } from 'react';

export function SiteLogbookInstruments({
  startDate,
  endDate,
}: {
  startDate: string;
  endDate: string;
}) {
  const data = useLogbookInvestigationStats(startDate, endDate);

  const { total, investigationData } = data;

  const instrumentData = useMemo(
    () =>
      Array.from(
        investigationData
          .reduce(
            (acc, curr) => {
              const instrument = curr.instrument || 'unknown';
              const current = acc.get(instrument) || {
                count: 0,
                notifications: 0,
                annotations: 0,
                instrument,
              };
              current.count += curr.count || 0;
              current.notifications += curr.notifications || 0;
              current.annotations += curr.annotations || 0;
              acc.set(instrument, current);
              return acc;
            },
            new Map<
              string,
              {
                count: number;
                notifications: number;
                annotations: number;
              }
            >(),
          )
          .values(),
      ),
    [investigationData],
  );

  const table = useReactTable({
    data: instrumentData,
    columns: [
      {
        header: 'Beamline',
        accessorKey: 'instrument',
        enableSorting: false,
        enableColumnFilter: true,
      },
      {
        header: 'Events',
        accessorKey: 'count',
        enableSorting: true,
        sortUndefined: -1,
        enableColumnFilter: false,
      },
      {
        header: '%total',
        accessorKey: 'count',
        id: 'percent_count',
        enableSorting: false,
        enableColumnFilter: false,
        cell: (row) =>
          row.getValue() && (
            <Progress
              valuePercent={(row.getValue() / total.totalEvent) * 100}
            />
          ),
      },
      {
        header: 'Notifications',
        accessorKey: 'notifications',
        enableSorting: true,
        sortUndefined: -1,
        enableColumnFilter: false,
      },
      {
        header: '%total',
        accessorKey: 'notifications',
        id: 'percent_notifications',
        enableSorting: false,
        enableColumnFilter: false,
        cell: (row) =>
          row.getValue() && (
            <Progress
              valuePercent={(row.getValue() / total.totalNotification) * 100}
            />
          ),
      },
      {
        header: 'Annotations',
        accessorKey: 'annotations',
        enableSorting: true,
        sortUndefined: -1,
        enableColumnFilter: false,
      },
      {
        header: '%total',
        accessorKey: 'annotations',
        id: 'percent_annotations',
        enableSorting: false,
        enableColumnFilter: false,
        cell: (row) =>
          row.getValue() && (
            <Progress
              valuePercent={(row.getValue() / total.totalAnnotation) * 100}
            />
          ),
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      sorting: [{ id: 'count', desc: true }],
      pagination: {
        pageSize: 20,
      },
    },
  });

  return (
    <TanstackBootstrapTable
      table={table}
      dataLength={instrumentData.length}
      sorting
    />
  );
}
