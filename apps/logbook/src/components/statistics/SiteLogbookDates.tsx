import { PlotWidget } from '@edata-portal/core';
import {
  useGetEndpoint,
  LOGBOOK_COUNT_STATISTICS_ENDPOINT,
  LogbookInvestigationCount,
} from '@edata-portal/icat-plus-api';

export function SiteLogbookDates({
  startDate,
  endDate,
}: {
  startDate: string;
  endDate: string;
}) {
  const annotations = useGetEndpoint({
    endpoint: LOGBOOK_COUNT_STATISTICS_ENDPOINT,
    params: { startDate, endDate, type: 'annotation' },
    default: [] as LogbookInvestigationCount[],
  });

  const notification = useGetEndpoint({
    endpoint: LOGBOOK_COUNT_STATISTICS_ENDPOINT,
    params: { startDate, endDate, type: 'notification' },
    default: [] as LogbookInvestigationCount[],
  });

  return (
    <PlotWidget
      data={[
        {
          x: annotations.map((v) => v._id),
          y: annotations.map((v) => v.count),
          type: 'bar',
          name: 'Annotations',
          ...{ offsetgroup: 1 },
        },
        {
          x: notification.map((v) => v._id),
          y: notification.map((v) => v.count),
          type: 'bar',
          name: 'Notifications',
          yaxis: 'y2',
          ...{ offsetgroup: 2 },
        },
      ]}
      layout={{
        hovermode: 'x unified',
        xaxis: {
          type: 'date',
        },
        yaxis: {
          title: 'Annotations',
        },
        yaxis2: {
          title: 'Notifications',
          side: 'right',
          overlaying: 'y',
        },
      }}
    />
  );
}
