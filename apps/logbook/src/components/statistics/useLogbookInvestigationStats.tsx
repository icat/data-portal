import {
  useGetEndpoint,
  LOGBOOK_INVESTIGATION_STATISTICS_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

export function useLogbookInvestigationStats(
  startDate: string,
  endDate: string,
) {
  const investigationData = useGetEndpoint({
    endpoint: LOGBOOK_INVESTIGATION_STATISTICS_ENDPOINT,
    params: { startDate, endDate },
  });

  const total = useMemo(
    () =>
      investigationData?.reduce(
        (acc, curr) => {
          acc.totalEvent += curr.count || 0;
          acc.totalAnnotation += curr.annotations || 0;
          acc.totalNotification += curr.notifications || 0;
          return acc;
        },
        { totalEvent: 0, totalAnnotation: 0, totalNotification: 0 },
      ),
    [investigationData],
  );

  if (!investigationData || !total)
    return {
      investigationData: [],
      total: {
        totalEvent: 0,
        totalAnnotation: 0,
        totalNotification: 0,
      },
    };

  return { investigationData, total };
}
