import {
  parseDate,
  TanstackBootstrapTable,
  formatDateToDay,
  Progress,
} from '@edata-portal/core';
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { useLogbookInvestigationStats } from 'components/statistics/useLogbookInvestigationStats';

export function SiteLogbookInvestigations({
  startDate,
  endDate,
}: {
  startDate: string;
  endDate: string;
}) {
  const data = useLogbookInvestigationStats(startDate, endDate);

  const { total, investigationData } = data;

  const table = useReactTable({
    data: investigationData,
    columns: [
      {
        header: 'Experiment session',
        accessorKey: 'name',
        enableSorting: false,
        enableColumnFilter: true,
      },
      {
        header: 'Beamline',
        accessorKey: 'instrument',
        enableSorting: false,
        enableColumnFilter: true,
      },
      {
        header: 'Start date',
        accessorKey: 'startDate',
        cell: (row) => formatDateToDay(parseDate(row.getValue())),
        enableSorting: false,
        enableColumnFilter: false,
      },
      {
        header: 'End date',
        accessorKey: 'endDate',
        cell: (row) => formatDateToDay(parseDate(row.getValue())),
        enableSorting: false,
        enableColumnFilter: false,
      },
      {
        header: 'Events',
        accessorKey: 'count',
        enableSorting: true,
        sortUndefined: -1,
        enableColumnFilter: false,
      },
      {
        header: '%total',
        accessorKey: 'count',
        id: 'percent_count',
        enableSorting: false,
        enableColumnFilter: false,
        cell: (row) =>
          row.getValue() && (
            <Progress
              valuePercent={(row.getValue() / total.totalEvent) * 100}
            />
          ),
      },
      {
        header: 'Notifications',
        accessorKey: 'notifications',
        enableSorting: true,
        sortUndefined: -1,
        enableColumnFilter: false,
      },
      {
        header: '%total',
        accessorKey: 'notifications',
        id: 'percent_notifications',
        enableSorting: false,
        enableColumnFilter: false,
        cell: (row) =>
          row.getValue() && (
            <Progress
              valuePercent={(row.getValue() / total.totalNotification) * 100}
            />
          ),
      },
      {
        header: 'Annotations',
        accessorKey: 'annotations',
        enableSorting: true,
        sortUndefined: -1,
        enableColumnFilter: false,
      },
      {
        header: '%total',
        accessorKey: 'annotations',
        id: 'percent_annotations',
        enableSorting: false,
        enableColumnFilter: false,
        cell: (row) =>
          row.getValue() && (
            <Progress
              valuePercent={(row.getValue() / total.totalAnnotation) * 100}
            />
          ),
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      sorting: [{ id: 'count', desc: true }],
      pagination: {
        pageSize: 20,
      },
    },
  });

  return (
    <TanstackBootstrapTable
      table={table}
      dataLength={investigationData.length}
      sorting
    />
  );
}
