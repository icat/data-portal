import {
  formatDateToIcatDate,
  addToDate,
  useParam,
  WithSideNav,
  SideNavElement,
  SideNavFilter,
  parseDate,
  UI_DATE_DAY_FORMAT,
  Loading,
  prettyPrint,
} from '@edata-portal/core';
import { SiteLogbookDates } from 'components/statistics/SiteLogbookDates';
import { SiteLogbookInstruments } from 'components/statistics/SiteLogbookInstruments';
import { SiteLogbookInvestigations } from 'components/statistics/SiteLogbookInvestigations';
import { SiteLogbookMeta } from 'components/statistics/SiteLogbookMeta';
import { Suspense } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const DATE_PRESETS = [
  {
    label: 'Today',
    startDate: formatDateToIcatDate(new Date()),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'Past 7 days',
    startDate: formatDateToIcatDate(addToDate(new Date(), { days: -7 })),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'Past 30 days',
    startDate: formatDateToIcatDate(addToDate(new Date(), { days: -30 })),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'Past year',
    startDate: formatDateToIcatDate(addToDate(new Date(), { years: -1 })),
    endDate: formatDateToIcatDate(new Date()),
  },
  {
    label: 'All time',
    startDate: 'undefined',
    endDate: 'undefined',
  },
];

const DEFAULT_START_DATE =
  formatDateToIcatDate(addToDate(new Date(), { days: -30 })) || 'undefined';
const DEFAULT_END_DATE = formatDateToIcatDate(new Date()) || 'undefined';

const GROUPS = ['beamline', 'experiment', 'date'] as const;

export default function SiteLogbookStatistics() {
  const [startDate, setStartDate] = useParam<string>(
    'startDate',
    DEFAULT_START_DATE,
  );
  const [endDate, setEndDate] = useParam<string>('endDate', DEFAULT_END_DATE);

  const [group, setGroup] = useParam<(typeof GROUPS)[number]>(
    'group',
    GROUPS[0],
  );

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label={'Data Statistics'}>
          <>
            <SideNavFilter label={'Start Date'}>
              <DatePicker
                className="form-control"
                selected={
                  startDate !== 'undefined' ? parseDate(startDate) : undefined
                }
                onChange={(date: Date | null) => {
                  setStartDate(
                    formatDateToIcatDate(date ?? undefined) || 'undefined',
                  );
                }}
                dateFormat={UI_DATE_DAY_FORMAT}
                isClearable
                placeholderText="Start Date"
              />
            </SideNavFilter>
            <SideNavFilter label={'End Date'}>
              <DatePicker
                className="form-control"
                selected={
                  endDate !== 'undefined' ? parseDate(endDate) : undefined
                }
                onChange={(date: Date | null) => {
                  setEndDate(
                    formatDateToIcatDate(date ?? undefined) || 'undefined',
                  );
                }}
                dateFormat={UI_DATE_DAY_FORMAT}
                isClearable
                placeholderText="End Date"
              />
            </SideNavFilter>
            <SideNavFilter label={'Date Presets'}>
              <Row className="g-1">
                {DATE_PRESETS.map((v) => (
                  <Col key={v.label} xs="auto">
                    <Button
                      variant="outline-primary"
                      size="sm"
                      onClick={() => {
                        setStartDate(v.startDate);
                        setEndDate(v.endDate);
                      }}
                      active={
                        v.startDate === startDate && v.endDate === endDate
                      }
                    >
                      {v.label}
                    </Button>
                  </Col>
                ))}
              </Row>
            </SideNavFilter>
            <SideNavFilter label={'Group by'}>
              <Row className="g-1">
                {GROUPS.map((v) => (
                  <Col key={v} xs="auto">
                    <Button
                      variant="outline-primary"
                      size="sm"
                      onClick={() => {
                        setGroup(v);
                      }}
                      active={v === group}
                    >
                      {prettyPrint(v)}
                    </Button>
                  </Col>
                ))}
              </Row>
            </SideNavFilter>
          </>
        </SideNavElement>
      }
    >
      <Suspense fallback={<Loading />}>
        <SiteLogbookMeta startDate={startDate} endDate={endDate} />
      </Suspense>
      <Suspense fallback={<Loading />}>
        <LoadAndDisplay startDate={startDate} endDate={endDate} group={group} />
      </Suspense>
    </WithSideNav>
  );
}

function LoadAndDisplay({
  startDate,
  endDate,
  group,
}: {
  startDate: string;
  endDate: string;
  group: (typeof GROUPS)[number];
}) {
  if (group === 'beamline') {
    return <SiteLogbookInstruments startDate={startDate} endDate={endDate} />;
  }
  if (group === 'experiment') {
    return (
      <SiteLogbookInvestigations startDate={startDate} endDate={endDate} />
    );
  }
  if (group === 'date') {
    return <SiteLogbookDates startDate={startDate} endDate={endDate} />;
  }
  return null;
}
