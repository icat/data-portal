import { MetadataTable, convertToFixed } from '@edata-portal/core';
import { useLogbookInvestigationStats } from 'components/statistics/useLogbookInvestigationStats';

export function SiteLogbookMeta({
  startDate,
  endDate,
}: {
  startDate: string;
  endDate: string;
}) {
  const data = useLogbookInvestigationStats(startDate, endDate);

  const { total } = data;

  return (
    <MetadataTable
      parameters={[
        { caption: 'Events', value: total.totalEvent },
        {
          caption: 'Notifications',
          value: `${total.totalNotification} (${convertToFixed(
            (total.totalNotification / total.totalEvent) * 100,
            2,
          )}%)`,
        },
        {
          caption: 'Annotations',
          value: `${total.totalAnnotation} (${convertToFixed(
            (total.totalAnnotation / total.totalEvent) * 100,
            2,
          )}%)`,
        },
      ]}
    />
  );
}
