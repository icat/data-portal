import {
  EventListParams,
  LOGBOOK_EVENT_LIST_ENDPOINT,
  useEndpointURL,
  DownloadType,
} from '@edata-portal/icat-plus-api';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DropdownButton, Dropdown, ButtonGroup } from 'react-bootstrap';

/**
 * DropdownButton with the formats proposed to download the logbook
 * @param param0
 * @returns
 */
export function DownloadGroupButton({ filters }: { filters: EventListParams }) {
  const getUrl = useEndpointURL(LOGBOOK_EVENT_LIST_ENDPOINT);

  const downloadOptions = [
    { text: 'PDF', format: DownloadType.pdf },
    { text: 'Plain text', format: DownloadType.txt },
  ];

  return (
    <DropdownButton
      size="sm"
      as={ButtonGroup}
      title={
        <>
          <FontAwesomeIcon
            icon={faDownload}
            style={{
              marginRight: 5,
            }}
          ></FontAwesomeIcon>
          Download
        </>
      }
      id="LogbookDownloadGroupButton-nested-dropdown"
    >
      {downloadOptions.map((button, index) => {
        return (
          <Dropdown.Item
            key={index}
            eventKey={index}
            target="_blank"
            href={getUrl({
              ...filters,
              format: button.format,
            })}
          >
            {button.text}
          </Dropdown.Item>
        );
      })}
    </DropdownButton>
  );
}
