import type { EventListParams } from '@edata-portal/icat-plus-api';
import { useCallback, useMemo } from 'react';
import { Col, Form } from 'react-bootstrap';

export function NotFromInvestigationFilter({
  filters,
  onUpdate,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
}) {
  const active = useMemo(() => {
    return filters.filterInvestigation;
  }, [filters.filterInvestigation]);

  const toggle = useCallback(() => {
    onUpdate({
      ...filters,
      filterInvestigation: !filters.filterInvestigation,
    });
  }, [filters, onUpdate]);

  return (
    <Col>
      <Form.Check
        type="switch"
        label="Hide user's logs"
        name="Hide user's logs"
        checked={active}
        onChange={toggle}
      />
    </Col>
  );
}
