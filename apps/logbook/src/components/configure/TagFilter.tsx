import {
  EventListParams,
  LogbookTag,
  LOGBOOK_TAG_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { Tag } from 'components/tag/Tag';
import { useCallback, useMemo } from 'react';
import ReactSelect from 'react-select';

export function TagFilter({
  filters,
  onUpdate,
  investigationId,
  instrumentName,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
  investigationId?: string;
  instrumentName?: string;
}) {
  const setTags = useCallback(
    (newValue: string) => {
      onUpdate({
        ...filters,
        tags: newValue,
      });
    },
    [filters, onUpdate],
  );

  const selectedTags = useMemo(() => {
    if (!filters.tags) return [];
    return filters.tags.split(',');
  }, [filters.tags]);

  const tagList = useGetEndpoint({
    endpoint: LOGBOOK_TAG_LIST_ENDPOINT,
    params: {
      ...(investigationId
        ? { investigationId: investigationId.toString() }
        : {}),
      ...(!investigationId && instrumentName ? { instrumentName } : {}),
    },
    default: [] as LogbookTag[],
  });

  const tags = useMemo(() => {
    return (
      tagList
        .filter((t) => selectedTags.includes(t.name))
        //Remove duplicates
        .filter((s, i, a) => a.findIndex((s2) => s2.name === s.name) === i)
    );
  }, [tagList, selectedTags]);

  return (
    <ReactSelect
      placeholder="Select tags..."
      options={(tagList || [])
        //keep only tags which name matches list of selected tags
        .filter((t) => !tags.find((tag) => tag.name === t.name))
        //Remove duplicates
        .filter((s, i, a) => a.findIndex((s2) => s2.name === s.name) === i)
        .map((tag) => ({
          value: tag,
          label: tag.name,
        }))}
      value={tags.map((tag) => ({
        value: tag,
        label: tag.name,
      }))}
      isMulti
      isClearable
      isSearchable
      formatOptionLabel={(option) => {
        if ('__isNew__' in option) {
          return option.label;
        }
        return <Tag tag={option.value} />;
      }}
      menuPlacement="auto"
      onChange={(selected) =>
        setTags(selected.map((s) => s.value.name).join(','))
      }
    />
  );
}
