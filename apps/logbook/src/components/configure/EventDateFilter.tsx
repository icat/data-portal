import {
  UI_DATE_DAY_FORMAT,
  formatDateToIcatDate,
  parseDate,
} from '@edata-portal/core';
import {
  useGetEndpoint,
  EventListParams,
  LOGBOOK_EVENT_DATES_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useCallback } from 'react';
import { Col } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export function EventDateFilter({
  filters,
  onUpdate,
  investigationId,
  instrumentName,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
  investigationId?: string;
  instrumentName?: string;
}) {
  const availableDates = useGetEndpoint({
    endpoint: LOGBOOK_EVENT_DATES_ENDPOINT,
    params: {
      ...filters,
      ...(investigationId ? { investigationId } : {}),
      ...(instrumentName ? { instrumentName } : {}),
      date: undefined,
    },
    skipFetch: !investigationId && !instrumentName,
  });

  const applyDate = useCallback(
    (date?: Date) => {
      const strDate = formatDateToIcatDate(date);
      onUpdate({
        ...filters,
        date: strDate,
      });
    },
    [filters, onUpdate],
  );

  const includeDates: Date[] = availableDates
    ?.map((d) => parseDate(d._id))
    .filter((date: Date | undefined) => date !== undefined) as Date[];

  return (
    <Col>
      <DatePicker
        selected={parseDate(filters.date)}
        onChange={(date) => applyDate(date ? date : undefined)}
        dateFormat={UI_DATE_DAY_FORMAT}
        includeDates={includeDates}
        className="form-control form-control-sm"
        isClearable
        placeholderText="Select date"
      />
    </Col>
  );
}
