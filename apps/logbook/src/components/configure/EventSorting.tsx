import type { EventListParams } from '@edata-portal/icat-plus-api';
import { useCallback } from 'react';
import { Col, Form } from 'react-bootstrap';

export function EventSorting({
  filters,
  onUpdate,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
}) {
  const setSort = useCallback(
    (order: 1 | -1) => {
      onUpdate({
        ...filters,
        sortBy: '_id',
        sortOrder: order,
      });
    },
    [filters, onUpdate],
  );

  return (
    <Col>
      <Form.Check
        type="radio"
        label="Newest First"
        name="sort"
        id="sort-newest"
        checked={filters.sortOrder === -1 && filters.sortBy === '_id'}
        onChange={() => setSort(-1)}
      />
      <Form.Check
        type="radio"
        label="Oldest First"
        name="sort"
        id="sort-oldest"
        checked={filters.sortOrder === 1 && filters.sortBy === '_id'}
        onChange={() => setSort(1)}
      />
    </Col>
  );
}
