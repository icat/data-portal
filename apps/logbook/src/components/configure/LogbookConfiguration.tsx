import {
  formatDateTimeToIcatDate,
  Loading,
  parseDate,
  SideNavFilter,
  UI_DATE_DAY_FORMAT,
  useConfig,
} from '@edata-portal/core';
import { type EventListParams } from '@edata-portal/icat-plus-api';
import { EventDateFilter } from 'components/configure/EventDateFilter';
import { EventSearch } from 'components/configure/EventSearch';
import { EventSorting } from 'components/configure/EventSorting';
import { EventTypeFilter } from 'components/configure/EventTypeFilter';
import { NotFromInvestigationFilter } from 'components/configure/NotFromInvestigationFilter';
import { TagFilter } from 'components/configure/TagFilter';
import { Suspense, useCallback, useEffect } from 'react';
import { Col } from 'react-bootstrap';
import DatePicker from 'react-datepicker';

export function LogbookConfiguration({
  filters,
  onUpdate,
  showStaffSection,
  investigationId,
  instrumentName,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
  showStaffSection?: boolean;
  investigationId?: string;
  instrumentName?: string;
}) {
  const config = useConfig();
  const setStartTime = useCallback(
    (startTime?: string) => {
      onUpdate({
        ...filters,
        startTime: startTime,
      });
    },
    [filters, onUpdate],
  );
  const setEndTime = useCallback(
    (endTime?: string) => {
      onUpdate({
        ...filters,
        endTime: endTime,
      });
    },
    [filters, onUpdate],
  );
  useEffect(() => {
    if (showStaffSection && !filters.startTime) {
      const now = new Date();
      const monthsBefore = new Date(now);
      monthsBefore.setMonth(
        now.getMonth() - config.ui.logbook.defaultMonthPeriodForStaff,
      );
      monthsBefore.setHours(0, 0, 0, 0);
      const formattedDate = formatDateTimeToIcatDate(monthsBefore);
      setStartTime(formattedDate);
    }
  }, [
    setStartTime,
    filters,
    config.ui.logbook.defaultMonthPeriodForStaff,
    showStaffSection,
  ]);
  return (
    <Col>
      <SideNavFilter label="Search">
        <EventSearch filters={filters} onUpdate={onUpdate} />
      </SideNavFilter>
      {investigationId || instrumentName ? (
        <SideNavFilter label="Filter by tags">
          <Suspense fallback={<Loading />}>
            <TagFilter
              filters={filters}
              onUpdate={onUpdate}
              investigationId={investigationId}
              instrumentName={instrumentName}
            />
          </Suspense>
        </SideNavFilter>
      ) : null}
      <SideNavFilter label="Filter by type">
        <EventTypeFilter filters={filters} onUpdate={onUpdate} />
      </SideNavFilter>
      <SideNavFilter label="Order">
        <EventSorting filters={filters} onUpdate={onUpdate} />
      </SideNavFilter>
      {!showStaffSection && (
        <SideNavFilter label="Date">
          <Suspense fallback={<Loading />}>
            <EventDateFilter
              filters={filters}
              onUpdate={onUpdate}
              investigationId={investigationId}
              instrumentName={instrumentName}
            />
          </Suspense>
        </SideNavFilter>
      )}
      {showStaffSection && (
        <>
          <SideNavFilter label="Staff">
            <NotFromInvestigationFilter filters={filters} onUpdate={onUpdate} />
          </SideNavFilter>

          <SideNavFilter label={'Start date'}>
            <DatePicker
              className="form-control"
              dateFormat={UI_DATE_DAY_FORMAT}
              selected={parseDate(filters.startTime)}
              maxDate={parseDate(filters.endTime)}
              onChange={(date: any) => {
                if (date) {
                  const adjustedDate = new Date(date);
                  adjustedDate.setHours(0, 0, 0, 0);
                  const strDate =
                    formatDateTimeToIcatDate(adjustedDate) || 'undefined';
                  setStartTime(strDate);
                } else {
                  setStartTime('undefined');
                }
              }}
              isClearable
              placeholderText="Start date"
            />
          </SideNavFilter>
          <SideNavFilter label={'End date'}>
            <DatePicker
              className="form-control"
              dateFormat={UI_DATE_DAY_FORMAT}
              selected={parseDate(filters.endTime)}
              minDate={parseDate(filters.startTime)}
              onChange={(date: any) => {
                if (date) {
                  const adjustedDate = new Date(date);
                  adjustedDate.setHours(23, 59, 59, 999);
                  const strDate =
                    formatDateTimeToIcatDate(adjustedDate) || 'undefined';
                  setEndTime(strDate);
                } else {
                  setEndTime('undefined');
                }
              }}
              isClearable
              placeholderText="End date"
            />
          </SideNavFilter>
        </>
      )}
    </Col>
  );
}
