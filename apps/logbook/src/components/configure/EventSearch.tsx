import { SearchBar } from '@edata-portal/core';
import type { EventListParams } from '@edata-portal/icat-plus-api';
import { useCallback } from 'react';
import { Col } from 'react-bootstrap';

export function EventSearch({
  filters,
  onUpdate,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
}) {
  const applySearch = useCallback(
    (search?: string) => {
      const trimmed = search?.trim() || undefined;
      const newValue = trimmed?.length ? trimmed : undefined;
      onUpdate({
        ...filters,
        search: newValue,
      });
    },
    [filters, onUpdate],
  );

  return (
    <Col>
      <SearchBar value={filters.search} onUpdate={applySearch} />
    </Col>
  );
}
