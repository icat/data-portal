import type {
  EventListParams,
  LogbookEventCategory,
  LogbookEventType,
} from '@edata-portal/icat-plus-api';
import { useCallback, useMemo } from 'react';
import { Col, Form } from 'react-bootstrap';

type CheckboxDescription = {
  name: string;
  label: string;
  value: CheckBoxValue[];
};
type CheckBoxValue = {
  type: LogbookEventType;
  category?: LogbookEventCategory;
};

const typeCheckBoxes: CheckboxDescription[] = [
  {
    name: 'showComments',
    label: 'Comments',
    value: [
      { type: 'annotation' },
      { type: 'notification', category: 'comment' },
    ],
  },
  {
    name: 'showInformation',
    label: 'Information',
    value: [
      { type: 'notification', category: 'info' },
      { type: 'notification', category: 'debug' },
    ],
  },
  {
    name: 'showError',
    label: 'Errors',
    value: [{ type: 'notification', category: 'error' }],
  },
  {
    name: 'showCommandLine',
    label: 'Command Lines',
    value: [{ type: 'notification', category: 'commandline' }],
  },
  {
    name: 'showBroadcast',
    label: 'Machine',
    value: [{ type: 'broadcast' }],
  },
];

export function EventTypeFilter({
  filters,
  onUpdate,
}: {
  filters: EventListParams;
  onUpdate: (filters: EventListParams) => void;
}) {
  const activeTypeValues: CheckBoxValue[] = useMemo(() => {
    if (!filters.types) return [];
    return filters.types.split(',').map((filter) => {
      if (!filter.includes('-')) return { type: filter as LogbookEventType };
      const [type, category] = filter.split('-');
      return { type, category } as CheckBoxValue;
    });
  }, [filters]);

  const isActiveCheckbox = useCallback(
    (checkbox: CheckboxDescription) => {
      return !checkbox.value.some((value) => {
        return !activeTypeValues.some((activeValue) => {
          return (
            activeValue.type === value.type &&
            activeValue.category === value.category
          );
        });
      });
    },
    [activeTypeValues],
  );
  const getCheckboxString = useCallback((checkbox: CheckboxDescription) => {
    return checkbox.value
      .map((value) => {
        if (!value.category) return value.type;
        return `${value.type}-${value.category}`;
      })
      .join(',');
  }, []);

  const toggleCheckBox = useCallback(
    (checkbox: CheckboxDescription) => {
      let newTypes = filters.types;
      if (isActiveCheckbox(checkbox)) {
        newTypes = typeCheckBoxes
          .filter((c) => isActiveCheckbox(c))
          .filter((c) => c !== checkbox)
          .map((c) => getCheckboxString(c))
          .join(',');
      } else {
        const checkboxString = getCheckboxString(checkbox);
        newTypes = newTypes ? `${newTypes},${checkboxString}` : checkboxString;
      }
      onUpdate({
        ...filters,
        types: newTypes,
      });
    },
    [filters, onUpdate, isActiveCheckbox, getCheckboxString],
  );
  return (
    <Col>
      {typeCheckBoxes.map((checkbox) => {
        return (
          <Form.Check
            key={checkbox.name}
            type="checkbox"
            name={checkbox.name}
            label={checkbox.label}
            checked={isActiveCheckbox(checkbox)}
            onChange={(e) => {
              toggleCheckBox(checkbox);
            }}
          />
        );
      })}
    </Col>
  );
}
