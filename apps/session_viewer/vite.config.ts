import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import federation from '@originjs/vite-plugin-federation';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      react(),
      tsconfigPaths(),
      federation({
        name: 'session_viewer',
        filename: 'remoteEntry.js',
        exposes: {
          './HumanOrganAtlasInvestigation':
            './src/components/project/hoa/HumanOrganAtlasInvestigation',
          './XASDatasetTable': './src/components/technique/xas/XASDatasetTable',
          './PaleoInvestigation':
            './src/components/project/paleo/PaleoInvestigation',
        },
        shared: [
          'react',
          'react-dom',
          '@tanstack/react-query',
          'react-router-dom',
          '@edata-portal/core',
        ],
      }),
    ],
    server: {
      port: 3001,
      host: true,
    },
    build: {
      modulePreload: false,
      target: 'esnext',
      minify: mode === 'production',
      cssCodeSplit: false,
    },
  };
});
