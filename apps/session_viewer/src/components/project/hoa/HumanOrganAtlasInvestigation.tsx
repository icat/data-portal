import {
  DatasetList,
  Loading,
  WithSideNav,
  useParam,
  useUserPreferences,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { HumanOrganAtlasFilters } from 'components/project/hoa/HumanOrganAtlasFilters';
import { Suspense, useMemo } from 'react';

export default function HumanOrganAtlasInvestigation({
  investigation,
}: {
  investigation: Investigation;
}) {
  const [filters, setFilters] = useParam<string>('filters', '');
  const [sampleId, setSampleId] = useParam<string>('sampleId', '');
  const [isGroupedBySample, setIsGroupedBySample] = useUserPreferences(
    'isGroupedBySample',
    true,
  );
  const [sampleChecked, setSampleChecked] = useParam<string>(
    'groupBySample',
    isGroupedBySample.toString(),
  );

  const updateSampleChecked = (checked: string) => {
    setSampleChecked(checked);
    setIsGroupedBySample(checked === 'true');
  };

  const [search, setSearch] = useParam<string>('search', '');
  const groupBy = useMemo(() => {
    return sampleChecked === 'true' ? 'sample' : 'dataset';
  }, [sampleChecked]);

  return (
    <WithSideNav
      sideNav={
        <HumanOrganAtlasFilters
          investigation={investigation}
          filters={filters}
          setFilters={setFilters}
          setSampleId={setSampleId}
          selectedSampleId={Number(sampleId)}
          sampleChecked={sampleChecked}
          setSampleChecked={updateSampleChecked}
          datasetSearch={search}
          setDatasetSearch={setSearch}
        />
      }
    >
      <Suspense fallback={<Loading />}>
        <DatasetList
          groupBy={groupBy}
          investigationId={investigation.id.toString()}
          parameterFilter={filters?.length ? filters : undefined}
          sampleId={sampleId?.length ? sampleId : undefined}
          sampleIds={sampleId?.length ? [sampleId] : undefined}
          search={search?.length ? search : undefined}
        />
      </Suspense>
    </WithSideNav>
  );
}
