import {
  SideNavElement,
  MetadataFilterDefinition,
  SideNavMetadataFilters,
  GenericDatasetsFilter,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { useEffect, useState } from 'react';

const FILTERS: {
  category: string;
  definitions: MetadataFilterDefinition[];
}[] = [
  {
    category: 'Patient',
    definitions: [
      {
        label: 'Sex',
        parameter: 'SamplePatient_sex',
        type: 'select-single',
        doPrettyPrint: true,
      },
      {
        label: 'Age',
        parameter: 'SamplePatient_age',
        type: 'range',
        step: 1,
        doPrettyPrint: false,
      },
      {
        label: 'Height (cm)',
        parameter: 'SamplePatient_size',
        type: 'range',
        step: 1,
        doPrettyPrint: false,
      },
      {
        label: 'Weight (kg)',
        parameter: 'SamplePatient_weight',
        type: 'range',
        step: 1,
        doPrettyPrint: false,
      },
      {
        label: 'Medical info',
        parameter: 'SamplePatient_info',
        type: 'search',
        doPrettyPrint: false,
      },
    ],
  },
  {
    category: 'Sample',
    definitions: [
      {
        label: 'Organ',
        parameter: 'SamplePatient_organ_name',
        type: 'select-multi',
        doPrettyPrint: true,
      },
      {
        label: 'Description',
        parameter: 'Sample_description',
        type: 'search',
        doPrettyPrint: false,
      },
    ],
  },
  {
    category: 'Scan parameters',
    definitions: [
      {
        label: 'Pixel size (um)',
        parameter: 'TOMO_pixelSize',
        type: 'range',
        doPrettyPrint: false,
      },
      {
        label: 'Detected avg. energy (keV)',
        parameter: 'TOMO_energy',
        type: 'range',
        doPrettyPrint: false,
      },
      {
        label: 'Surface dose rate (Gy/s)',
        parameter: 'TOMO_surface_dose',
        type: 'range',
        doPrettyPrint: false,
      },
      {
        label: 'VOI integ. dose (kGy)',
        parameter: 'TOMO_total_voi_dose',
        type: 'range',
        doPrettyPrint: false,
      },
    ],
  },
];

export function HumanOrganAtlasFilters({
  investigation,
  filters,
  setFilters,
  setSampleId,
  selectedSampleId,
  sampleChecked,
  setSampleChecked,
  datasetSearch,
  setDatasetSearch,
}: {
  investigation: Investigation;
  filters?: string;
  setFilters: (newFilters?: string) => void;
  setSampleId: (sampleId?: string) => void;
  selectedSampleId?: number;
  sampleChecked: string;
  setSampleChecked: (sampleChecked: string) => void;
  datasetSearch?: string;
  setDatasetSearch: (search: string) => void;
}) {
  const [localFilters, updateLocalFilters] = useState<string | undefined>(
    filters,
  );

  //apply filters after 500ms without change
  useEffect(() => {
    const timeout = setTimeout(() => {
      setFilters(localFilters);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [localFilters]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    updateLocalFilters(filters);
  }, [filters]);

  return (
    <>
      <GenericDatasetsFilter
        investigation={investigation}
        setSampleId={setSampleId}
        selectedSampleId={Number(selectedSampleId)}
        sampleChecked={sampleChecked}
        setSampleChecked={setSampleChecked}
        datasetSearch={datasetSearch}
        setDatasetSearch={setDatasetSearch}
      ></GenericDatasetsFilter>
      {FILTERS.map(({ category, definitions }) => (
        <SideNavElement key={category} label={category}>
          <SideNavMetadataFilters
            filterDefinitions={definitions}
            state={{
              currentFilters: localFilters || '',
              setCurrentFilters: updateLocalFilters,
              investigationId: investigation.id.toString(),
            }}
          />
        </SideNavElement>
      ))}
    </>
  );
}
