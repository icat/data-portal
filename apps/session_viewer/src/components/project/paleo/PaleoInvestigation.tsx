import {
  DatasetList,
  Loading,
  useParam,
  useUserPreferences,
  WithSideNav,
} from '@edata-portal/core';
import { Investigation } from '@edata-portal/icat-plus-api';
import { PaleoFilters } from 'components/project/paleo/PaleoFilters';
import { Suspense, useMemo } from 'react';

export default function PaleoInvestigation({
  investigation,
}: {
  investigation: Investigation;
}) {
  const [filters, setFilters] = useParam<string>('filters', '');
  const [sampleId, setSampleId] = useParam<string>('sampleId', '');
  const [isGroupedBySample, setIsGroupedBySample] = useUserPreferences(
    'isGroupedBySample',
    true,
  );
  const [sampleChecked, setSampleChecked] = useParam<string>(
    'groupBySample',
    isGroupedBySample.toString(),
  );

  const updateSampleChecked = (checked: string) => {
    setSampleChecked(checked);
    setIsGroupedBySample(checked === 'true');
  };

  const [search, setSearch] = useParam<string>('search', '');
  const groupBy = useMemo(() => {
    return sampleChecked === 'true' ? 'sample' : 'dataset';
  }, [sampleChecked]);

  return (
    <WithSideNav
      sideNav={
        <PaleoFilters
          investigation={investigation}
          filters={filters}
          setFilters={setFilters}
          setSampleId={setSampleId}
          selectedSampleId={Number(sampleId)}
          sampleChecked={sampleChecked}
          setSampleChecked={updateSampleChecked}
          datasetSearch={search}
          setDatasetSearch={setSearch}
        />
      }
    >
      <Suspense fallback={<Loading />}>
        <DatasetList
          groupBy={groupBy}
          investigationId={investigation.id.toString()}
          parameterFilter={filters?.length ? filters : undefined}
          sampleId={sampleId?.length ? sampleId : undefined}
          sampleIds={sampleId?.length ? [sampleId] : undefined}
          search={search?.length ? search : undefined}
        />
      </Suspense>
    </WithSideNav>
  );
}
