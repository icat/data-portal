import {
  SideNavElement,
  GenericDatasetsFilter,
  SideNavMetadataFilter,
} from '@edata-portal/core';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { useEffect, useState } from 'react';

export function PaleoFilters({
  investigation,
  filters,
  setFilters,
  setSampleId,
  selectedSampleId,
  sampleChecked,
  setSampleChecked,
  datasetSearch,
  setDatasetSearch,
}: {
  investigation: Investigation;
  filters?: string;
  setFilters: (newFilters?: string) => void;
  setSampleId: (sampleId?: string) => void;
  selectedSampleId?: number;
  sampleChecked: string;
  setSampleChecked: (sampleChecked: string) => void;
  datasetSearch?: string;
  setDatasetSearch: (search: string) => void;
}) {
  const [localFilters, updateLocalFilters] = useState<string | undefined>(
    filters,
  );

  //apply filters after 500ms without change
  useEffect(() => {
    const timeout = setTimeout(() => {
      setFilters(localFilters);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [localFilters]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    updateLocalFilters(filters);
  }, [filters]);

  return (
    <>
      <GenericDatasetsFilter
        investigation={investigation}
        setSampleId={setSampleId}
        selectedSampleId={Number(selectedSampleId)}
        sampleChecked={sampleChecked}
        setSampleChecked={setSampleChecked}
        datasetSearch={datasetSearch}
        setDatasetSearch={setDatasetSearch}
      ></GenericDatasetsFilter>
      <SideNavElement label={'Sample'}>
        <SideNavMetadataFilter
          filterDefinition={{
            label: 'Scientific domain',
            parameter: 'SamplePaleo_scientific_domain',
            type: 'select-multi',
            doPrettyPrint: true,
          }}
          state={{
            currentFilters: localFilters || '',
            setCurrentFilters: updateLocalFilters,
            investigationId: investigation.id.toString(),
          }}
        />
      </SideNavElement>
    </>
  );
}
