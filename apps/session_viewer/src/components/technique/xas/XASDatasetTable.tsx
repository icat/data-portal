import type { ColumnDef } from '@tanstack/react-table';

import {
  getDatasetParamValue,
  getDatasetTableColumns,
  GenericDatasetTable,
} from '@edata-portal/core';
import type { ExpandDataset } from '@edata-portal/core';
import type { Dataset, Investigation } from '@edata-portal/icat-plus-api';

export const LASER_DELAY_PARAM = 'InstrumentLaser01_delay';
export const LASER_ENERGY_PARAM = 'InstrumentLaser01_energy';
export const NOTES_00 = 'Notes_note_00';

export default function XASDatasetTable({
  investigation,
  instrumentName,
  datasetIds,
  nested,
}: {
  investigation: Investigation;
  instrumentName?: string;
  datasetIds?: (string | number)[];
  nested?: boolean;
}) {
  const metadataColumns: ColumnDef<ExpandDataset>[] = [
    {
      accessorFn: (row) => row,
      header: 'Laser Delay (s)',
      footer: 'Laser Delay (s)',
      cell: (info) =>
        getDatasetParamValue(info.getValue() as Dataset, LASER_DELAY_PARAM),
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Laser Energy (J)',
      footer: 'Laser Energy (J)',
      cell: (info) =>
        getDatasetParamValue(info.getValue() as Dataset, LASER_ENERGY_PARAM),
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Notes',
      footer: 'Notes',
      cell: (info) =>
        getDatasetParamValue(info.getValue() as Dataset, NOTES_00),
      enableColumnFilter: false,
    },
  ];
  const columns: ColumnDef<ExpandDataset, any>[] = getDatasetTableColumns({
    metadataColumns,
  });

  return (
    <GenericDatasetTable
      investigation={investigation}
      instrumentName={instrumentName}
      datasetIds={datasetIds}
      nested={nested}
      columns={columns}
    />
  );
}
