import {
  useGetEndpoint,
  type Dataset,
  type WorkflowDescription,
  WORKFLOW_LIST_ENDPOINT,
  IcatUser,
} from '@edata-portal/icat-plus-api';
import { useConfig } from 'context';
import { getSharedContext } from 'context/shared';
import { useUser } from 'context/user';
import { getWorkflowsForDatasets } from 'helpers';
import React, { useMemo } from 'react';

export type Reprocessing = {
  datasetIds: (string | number)[];
  workflowId?: string;
};

export type ReprocessingContextType = {
  reprocess: (reprocessing: Reprocessing) => void;
};

export const ReprocessContext = getSharedContext<ReprocessingContextType>(
  'ReprocessingContext',
);

export function useReprocess() {
  const reprocessContext = React.useContext(ReprocessContext);
  return useMemo(
    () => reprocessContext.reprocess,
    [reprocessContext.reprocess],
  );
}

export function useAvailableWorkflows(dataset: Dataset | Dataset[]) {
  const user = useUser();
  const config = useConfig();

  const workflows = useGetEndpoint({
    endpoint: WORKFLOW_LIST_ENDPOINT,
    default: [] as WorkflowDescription[],
    skipFetch: !user?.isAuthenticated || !config.ui.features.reprocessing,
  });

  return useMemo(() => {
    if (!config.ui.features.reprocessing) return [];
    const datasets = Array.isArray(dataset) ? dataset : [dataset];
    return getWorkflowsForDatasets(workflows, datasets, user as IcatUser);
  }, [dataset, workflows, config, user]);
}
