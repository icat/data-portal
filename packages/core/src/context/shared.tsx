import { createContext } from 'react';

declare global {
  interface Window {
    dataPortalContextRegistry: Record<string, React.Context<any>>;
  }
}
window.dataPortalContextRegistry = window.dataPortalContextRegistry || {};

export function getSharedContext<T>(
  name: string,
  defaultValue?: T,
): React.Context<T> {
  if (!window.dataPortalContextRegistry[name]) {
    window.dataPortalContextRegistry[name] = createContext(
      defaultValue ? defaultValue : ({} as T),
    );
  }
  return window.dataPortalContextRegistry[name];
}
