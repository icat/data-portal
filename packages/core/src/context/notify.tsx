import { getSharedContext } from 'context/shared';
import React, { useMemo } from 'react';

export type Notification = {
  message: string | React.ReactNode;
  type?: 'danger' | 'success' | 'warning' | 'info';
  title?: string | React.ReactNode;
};

export type NotifyContextType = {
  notify: (notification: Notification) => void;
};

export const NotifyContext =
  getSharedContext<NotifyContextType>('NotifyContext');

export function useNotify() {
  const notifyContext = React.useContext(NotifyContext);
  return useMemo(() => notifyContext.notify, [notifyContext.notify]);
}
