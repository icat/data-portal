export interface UIConfig {
  applicationTitle: string;
  facilityName: string;
  homePage: {
    policyMessage: string;
  };
  loginForm: LoginForm;
  userPortal: {
    investigationParameterPkName: string;
    link: string;
  };
  doi: {
    link: string;
    minimalAbstractLength: number;
    minimalTitleLength: number;
    facilityPrefix: string;
    facilitySuffix: string;
    referenceDoi: string;
  };
  fileBrowser: {
    maxFileNb: number;
  };
  imageViewer: {
    fileExtensions: string[];
  };
  h5Viewer: {
    url: string;
    fileExtensions: string[];
  };
  textViewer: {
    maxFileSize: number;
    fileExtensions: string[];
  };
  galleryViewer: {
    fileExtensions: string[];
  };
  feedback: {
    email: string;
    subject: string;
    body: string;
  };
  knowledgeBasePage: string;
  footer: {
    images: {
      src: string;
      alt: string;
      href: string;
    }[];
  };
  logbook: {
    help: string;
    defaultMonthPeriodForStaff: number;
  };
  handsonTableLicenseKey: string;
  globus: {
    enabled: boolean;
    url: string;
    collections: {
      root: string;
      origin: string;
      originId: string;
    }[];
    messageAlert: {
      enabled: boolean;
      message: string;
    };
  };
  sample: {
    pageTemplateURL: string;
    editable: boolean;
    descriptionParameterName: string;
    nonEditableParameterName: string;
    notesParameterName: string;
  };
  projects: { key: string; title: string; url: string; homepage: string }[];
  features: UIFeaturesConfig;
  tracking: TrackingConfig;
  mx: MXConfig;
  logistics: LogisticsConfig;
  reprocessing: {
    defaultDayPeriodJobs: number;
  };
}

export interface UIFeaturesConfig {
  reprocessing: boolean;
  logbook: boolean;
  logistics: boolean;
  dmp: boolean;
}

export interface LoginForm {
  accountCreationLink: string;
  note: Note;
}

export interface Note {
  title: string;
  enabled: boolean;
  notes: TextNote[];
}

export interface TextNote {
  text: string;
}

export interface TrackingConfig {
  enabled: boolean;
  url: string;
  siteId: string;
  tracker: string;
  script: string;
}

export interface MXConfig {
  pdb_map_mtz_viewer_url: string;
}

export interface LogisticsConfig {
  transportOrganizationEnabled: boolean;
  facilityReimbursmentEnabled: boolean;
  facilityForwarderName: string;
  facilityForwarderAccount: string;
  facilityForwarderNamePickup: string[];
}
