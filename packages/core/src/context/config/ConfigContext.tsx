import { APIConfig } from 'context/config/APIConfig';
import { TechniquesConfig } from 'context/config/TechniquesConfig';
import { UIConfig } from 'context/config/UIConfig';
import { getSharedContext } from 'context/shared';
import React from 'react';

export interface Config {
  ui: UIConfig;
  api: APIConfig;
  techniques: TechniquesConfig;
}

export const ConfigContext = getSharedContext<Config>('ConfigContext');

export function useConfig() {
  return React.useContext(ConfigContext);
}
