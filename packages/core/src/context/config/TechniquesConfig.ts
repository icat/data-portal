export type TechniquesConfig = TechniqueConfig[];

export type TechniqueConfig = {
  name: string;
  shortname: string;
};
