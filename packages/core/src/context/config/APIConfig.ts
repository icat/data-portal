export interface APIConfig {
  icat_url: string | undefined;
  authentication: Authentication;
}

export interface GenericAuthenticator {
  enabled: boolean;
  name: 'Database' | 'OpenID';
  message?: string;
  plugin: string;
  preferred?: boolean;
  title: string;
}

export interface Authentication {
  autoRefresh: boolean;
  autoRefreshThresholdMinutes?: number;
  authenticators: (GenericAuthenticator | OpenIDAuthenticator)[];
  anonymous: {
    plugin: string;
    username: string;
    password: string;
  };
}

export interface OpenIDAuthenticator extends GenericAuthenticator {
  openidLogoutTransferToApp: boolean;
  minValidity: number;
  refreshToken: boolean;
  configuration: {
    authority: string;
    clientId: string;
  };
}
