import type {
  Dataset,
  Investigation,
  Sample,
} from '@edata-portal/icat-plus-api';
import { getSharedContext } from 'context/shared';
import React from 'react';

export const DATASET_VIEWER_TYPES = [
  'details',
  'snapshot',
  'tableCell',
  'generic',
] as const;
export type DatasetViewerType = (typeof DATASET_VIEWER_TYPES)[number];

export type ViewersContextType = {
  viewInvestigation: (investigation: Investigation, props?: any) => JSX.Element;
  viewDataset: (
    dataset: Dataset,
    type: DatasetViewerType,
    props?: any,
  ) => JSX.Element;
  viewSample: (sample: Sample, props?: any) => JSX.Element;
};

export const ViewersContext =
  getSharedContext<ViewersContextType>('ViewersContext');

export function useViewers() {
  return React.useContext(ViewersContext);
}
