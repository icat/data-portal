import { getSharedContext } from 'context/shared';
import { useMemo, useState, useContext, useEffect } from 'react';
import { createPortal } from 'react-dom';

export type SideNavNode = {
  id: string;
  position: number;
  onRef?: (ref: HTMLElement) => void;
};

export type SideNavContextType = {
  nodes: SideNavNode[];
} & UpdateSideNavContextType;
export const SideNavContext =
  getSharedContext<SideNavContextType>('SideNavContext');

export type UpdateSideNavContextType = {
  addNode: (node: SideNavNode) => void;
  removeNode: (node: SideNavNode) => void;
};
export const UpdateSideNavContext = getSharedContext<UpdateSideNavContextType>(
  'UpdateSideNavContext',
);

function useSideNavPortal(node: React.ReactNode, position: number = 0) {
  const { addNode, removeNode } = useContext(UpdateSideNavContext);

  const [refState, setRefState] = useState<Element | null>(null);

  const id = useMemo(() => generateNodeId(), []);

  const nodeObject: SideNavNode = useMemo(
    () => ({
      id: id,
      position,
      onRef: (ref) => setRefState(ref),
    }),
    [id, position, setRefState],
  );

  useEffect(() => {
    addNode(nodeObject);
    return () => {
      removeNode(nodeObject);
    };
  }, [addNode, removeNode, nodeObject]);

  if (refState === null) {
    return null;
  }

  return createPortal(node, refState);
}

function generateNodeId() {
  return Math.random().toString(36);
}

/**
 * This allows your component to insert something into the side navigation bar.
 * The position relative to the other elements in the navigation bar is determined by the `position` parameter.
 * If you don't specify a position, the position is automatically computed based on the depth of the component in the DOM (the deeper, the higher the position).
 */
export function WithSideNav({
  children,
  sideNav,
  position,
  hide,
}: {
  children: React.ReactNode;
  sideNav: React.ReactNode;
  position?: number;
  hide?: boolean;
}) {
  if (hide) {
    return <>{children}</>;
  }
  if (position === undefined) {
    return (
      <WithAutoComputePosition sideNav={sideNav}>
        {children}
      </WithAutoComputePosition>
    );
  }
  return (
    <SideNavPortal sideNav={sideNav} position={position}>
      {children}
    </SideNavPortal>
  );
}

function WithAutoComputePosition({
  children,
  sideNav,
}: {
  children: React.ReactNode;
  sideNav: React.ReactNode;
}) {
  const [divElement, setDivElement] = useState<Element | null>(null);

  const divDepth = useMemo(() => {
    if (divElement === null) {
      return null;
    }
    let depth = 0;
    let current: Element = divElement;
    while (current.parentElement) {
      current = current.parentElement;
      depth++;
    }
    return depth;
  }, [divElement]);

  return (
    <div ref={(ref) => setDivElement(ref)}>
      <SideNavPortal sideNav={sideNav} position={divDepth || 0}>
        {children}
      </SideNavPortal>
    </div>
  );
}

function SideNavPortal({
  children,
  sideNav,
  position,
}: {
  children: React.ReactNode;
  sideNav: React.ReactNode;
  position: number;
}) {
  const sideNavPortal = useSideNavPortal(sideNav, position);

  return (
    <>
      {sideNavPortal}
      {children}
    </>
  );
}
