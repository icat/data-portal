import type { IcatUser } from '@edata-portal/icat-plus-api';
import { getSharedContext } from 'context/shared';
import React from 'react';

export const UserContext = getSharedContext<
  (IcatUser & { logout: () => void; isAuthenticated: boolean }) | undefined
>('UserContext');

export function useUser() {
  return React.useContext(UserContext);
}
