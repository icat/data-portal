import {
  useMutateEndpoint,
  type Sample,
  CREATE_SAMPLE_PARAMETER,
  UPDATE_SAMPLE_PARAMETER,
} from '@edata-portal/icat-plus-api';
import { useConfig } from 'context';
import { useInvestigationAccess } from 'hooks/investigationAccess';
import { useCallback, useMemo, useState } from 'react';
import { getParameter } from 'helpers/entity';

export function useHasComments(sample: Sample) {
  const { value } = useCommentParameter(sample);

  return useMemo(() => {
    return Boolean(value.trim().length);
  }, [value]);
}

export function useCommentParameter(sample: Sample) {
  const config = useConfig();

  const param = useMemo(() => {
    return getParameter(sample, config.ui.sample.notesParameterName);
  }, [sample, config]);

  const createParam = useMutateEndpoint({
    endpoint: CREATE_SAMPLE_PARAMETER,
    params: {
      sampleId: String(sample.id),
      investigationId: String(sample.investigation.id),
    },
  });

  const updateParam = useMutateEndpoint({
    endpoint: UPDATE_SAMPLE_PARAMETER,
    params: {
      id: String(param?.id),
      investigationId: String(sample.investigation.id),
    },
  });

  const [saving, setSaving] = useState(false);

  const update = useCallback(
    (value: string) => {
      const newValue = value.trim() || ' ';
      setSaving(true);
      if (param) {
        updateParam
          .mutateAsync({
            body: {
              ...param,
              value: newValue,
            },
          })
          .then(() => {
            setSaving(false);
          });
      } else {
        createParam
          .mutateAsync({
            body: {
              name: config.ui.sample.notesParameterName,
              value: newValue,
            },
          })
          .then(() => {
            setSaving(false);
          });
      }
    },
    [param, createParam, updateParam, config.ui.sample.notesParameterName],
  );

  return useMemo(() => {
    return {
      value: param?.value.trim() || '',
      update,
      saving,
    };
  }, [param, update, saving]);
}

export function useCanEditComments(sample: Sample) {
  return useInvestigationAccess({
    investigation: sample.investigation,
    roles: {
      participant: true,
      instrumentScientist: true,
      administrator: true,
    },
  });
}
