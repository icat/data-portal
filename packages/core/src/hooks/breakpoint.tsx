import { useState, useEffect, useCallback, useMemo } from 'react';

export const BREAKPOINTS = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'] as const;
export type Breakpoint = (typeof BREAKPOINTS)[number];

export function getBreakpoint(width: number): Breakpoint {
  if (width < 576) {
    return 'xs';
  } else if (width < 768) {
    return 'sm';
  } else if (width < 992) {
    return 'md';
  } else if (width < 1200) {
    return 'lg';
  } else if (width < 1400) {
    return 'xl';
  } else {
    return 'xxl';
  }
}

export function useBreakpoint(): Breakpoint {
  const [breakPoint, setBreakPoint] = useState(() =>
    getBreakpoint(window.innerWidth),
  );

  useEffect(() => {
    const updateValue = function () {
      const newValue = getBreakpoint(window.innerWidth);
      if (breakPoint !== newValue) setBreakPoint(newValue);
    };
    window.addEventListener('resize', updateValue);
    return () => window.removeEventListener('resize', updateValue);
  }, [breakPoint]);

  return breakPoint;
}

/**
 * Allows to change the value of a variable depending on the current breakpoint
 * @param values values for each breakpoint (xs is required and used as default)
 * @returns the value for the current breakpoint, or the one bellow if no defined value for the current breakpoint
 */
export function useBreakpointValue<T>(
  values: {
    xs: T;
  } & {
    [P in Breakpoint]?: T;
  },
): T {
  const breakPoint = useBreakpoint();

  const findBreakpointValue = useCallback(
    function (breakPoint: Breakpoint): T {
      const v = values[breakPoint];
      if (v !== undefined) return v;
      const breakPointIndex = BREAKPOINTS.indexOf(breakPoint);
      if (breakPointIndex === 0) return values.xs;
      return findBreakpointValue(BREAKPOINTS[breakPointIndex - 1]);
    },
    [values],
  );

  const value = useMemo(
    () => findBreakpointValue(breakPoint),
    [breakPoint, findBreakpointValue],
  );

  return value;
}
