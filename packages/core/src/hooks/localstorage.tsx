import { useCallback, useEffect, useMemo, useState } from 'react';

export function useLocalStorageValue(key: string) {
  const getValue = useCallback(() => {
    const value = localStorage.getItem(key);
    return value ?? undefined;
  }, [key]);

  const [stateValue, setStateValue] = useState(getValue());

  const setValue = useCallback(
    (value: string | undefined) => {
      if (value !== undefined) {
        localStorage.setItem(key, value);
      } else {
        localStorage.removeItem(key);
      }

      setTimeout(() => {
        window.dispatchEvent(new Event(key));
      });
    },
    [key],
  );

  useEffect(() => {
    const handleStorageChange = () => {
      setStateValue((prev) => {
        const value = getValue();
        if (prev === value) return prev;
        return value;
      });
    };

    window.addEventListener(key, handleStorageChange);

    return () => {
      window.removeEventListener(key, handleStorageChange);
    };
  });

  return [stateValue, setValue] as const;
}

export function useLocalStorageObject(key: string) {
  const [value, setValue] = useLocalStorageValue(key);

  const parsedValue = useMemo(() => {
    try {
      return value ? JSON.parse(value) : undefined;
    } catch (e) {
      return undefined;
    }
  }, [value]);

  const updateValue = useCallback(
    (newValue: Record<string, any>) => {
      setValue(JSON.stringify(newValue));
    },
    [setValue],
  );

  return [parsedValue, updateValue] as const;
}
