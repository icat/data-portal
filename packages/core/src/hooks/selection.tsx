import type { SelectionSetting } from '@edata-portal/icat-plus-api';
import { removeDuplicates } from 'helpers';
import { useUserPreferences } from 'hooks/preferences';
import { useCallback, useMemo } from 'react';

export type SelectEntityType = 'dataset';

export function useSelection(type: SelectEntityType) {
  const key: keyof SelectionSetting = useMemo(() => {
    return type === 'dataset' ? 'datasetIds' : 'sampleIds';
  }, [type]);

  const [selection, setSelection] = useUserPreferences('selection', {
    datasetIds: [],
    sampleIds: [],
  });

  const value = useMemo(
    () => (type === 'dataset' ? selection.datasetIds : selection.sampleIds),
    [selection, type],
  );

  const selectEntity = useCallback(
    (entityIds: number[]) => {
      const idsToSelect = entityIds || [];
      const currentSelection = selection[key];
      const newSelection = [...currentSelection, ...idsToSelect];
      setSelection({
        ...selection,
        [key]: removeDuplicates(newSelection),
      });
    },
    [key, selection, setSelection],
  );

  const unselectEntity = useCallback(
    (entityIds: number[]) => {
      const currentSelection = selection[key];
      const newSelection = currentSelection.filter(
        (id) => !entityIds.includes(id),
      );
      setSelection({
        ...selection,
        [key]: newSelection,
      });
    },
    [key, selection, setSelection],
  );

  const clearSelection = useCallback(() => {
    setSelection({
      ...selection,
      [key]: [],
    });
  }, [key, selection, setSelection]);

  const res = useMemo(
    () => ({
      value,
      selectEntity,
      unselectEntity,
      clearSelection,
    }),
    [value, selectEntity, unselectEntity, clearSelection],
  );

  return res;
}

export function useItemSelection(type: SelectEntityType, ids: number[]) {
  const selection = useSelection(type);

  const isSelected = useMemo(() => {
    return ids ? ids.some((id) => selection.value.includes(id)) : false;
  }, [ids, selection.value]);

  const toggleSelected = useCallback(() => {
    if (isSelected) {
      selection.unselectEntity(ids);
    } else {
      selection.selectEntity(ids);
    }
  }, [ids, isSelected, selection]);

  return useMemo(
    () => ({
      isSelected,
      toggleSelected,
    }),
    [isSelected, toggleSelected],
  );
}
