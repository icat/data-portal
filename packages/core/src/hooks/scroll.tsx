import { faLink } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useParam } from 'hooks/param';
import React, { useEffect, useMemo, useRef } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';

export function useSetScroll() {
  const [, setScroll] = useParam<string>('scroll', '');

  return setScroll;
}

export function useScroll<
  T extends {
    scrollIntoView(options?: ScrollIntoViewOptions): void;
  } = HTMLDivElement,
>(value: string | number) {
  const [scroll] = useParam<string>('scroll', '');

  const ref = useRef<T>(null);

  const isCurrent = useMemo(() => {
    return scroll === value.toString();
  }, [scroll, value]);

  useEffect(() => {
    if (isCurrent) {
      ref.current?.scrollIntoView({
        behavior: 'instant' as any,
        block: 'center',
        inline: 'center',
      });
    }
  }, [isCurrent]);

  return {
    ref,
    scrollValue: value.toString(),
    isCurrent,
  };
}

export function HashScrollButton({
  hash,
  isCurrent,
}: {
  hash: string;
  isCurrent?: boolean;
}) {
  const [copied, setCopied] = React.useState(false);
  React.useEffect(() => {
    if (copied) {
      const timeout = setTimeout(() => setCopied(false), 5000);
      return () => clearTimeout(timeout);
    }
    return undefined;
  }, [copied]);

  const setScroll = useSetScroll();

  return (
    <OverlayTrigger
      placement="auto"
      overlay={<Tooltip>{copied ? 'Copied' : 'Copy to clipboard'}</Tooltip>}
    >
      <Button
        type="button"
        variant={isCurrent ? 'success' : 'info'}
        size="sm"
        onClick={(e) => {
          e.stopPropagation();
          setScroll(hash);
          navigator.clipboard.writeText(window.location.href);
          setCopied(true);
        }}
      >
        <FontAwesomeIcon size={'lg'} icon={faLink} />
      </Button>
    </OverlayTrigger>
  );
}
