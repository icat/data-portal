import { Config, useConfig } from 'context';
import { useEffect } from 'react';

function getTracker() {
  try {
    const tracker = (window as any)._paq || [];
    if (!(window as any)._paq) {
      (window as any)._paq = tracker;
    }
    return tracker;
  } catch (e) {
    console.error(e);
    return [];
  }
}

function pushTrackingEvent(event: string[]) {
  try {
    const tracker = getTracker();
    tracker.push([
      'setCustomUrl',
      window.location.href.replace(window.location.search, ''),
    ]);
    tracker.push(event);
  } catch (e) {
    console.error(e);
  }
}

function configureTracking(config: Config) {
  if (!config.ui.tracking.enabled) return;

  if ((window as any).TRACKING_CONFIGURED) {
    return;
  }
  (window as any).TRACKING_CONFIGURED = true;

  pushTrackingEvent([
    'setTrackerUrl',
    config.ui.tracking.url + config.ui.tracking.tracker,
  ]);
  pushTrackingEvent(['setSiteId', config.ui.tracking.siteId]);

  const newScript = document.createElement('script');
  const scripts = document.getElementsByTagName('script')[0];
  newScript.type = 'text/javascript';
  newScript.async = true;
  newScript.src = config.ui.tracking.url + config.ui.tracking.script;

  scripts?.parentNode?.insertBefore(newScript, scripts);

  try {
    pushTrackingEvent(['enableLinkTracking']);
  } catch (e) {
    console.error(e);
  }
}

export function usePageTracking(name: string, skip = false) {
  const config = useConfig();
  configureTracking(config);

  useEffect(() => {
    if (skip) return;
    pushTrackingEvent(['trackPageView', name]);
  }, [name, skip]);
}

export function useTrackingCallback(): (
  category: string,
  action: string,
  name: string,
) => void {
  const config = useConfig();
  configureTracking(config);

  return (category: string, action: string, name: string) => {
    pushTrackingEvent(['trackEvent', category, action, name]);
  };
}

export function useTracking(category: string, action: string, name: string) {
  const track = useTrackingCallback();
  useEffect(() => {
    track(category, action, name);
  }, [category, action, name, track]);
}
