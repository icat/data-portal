import {
  GET_PATH_ENDPOINT,
  useGetEndpoint,
  type Dataset,
} from '@edata-portal/icat-plus-api';
import { flattenOutputs } from 'helpers';
import { useMemo } from 'react';

export function useDatasetPath(dataset: Dataset) {
  const datasetIds = useMemo(() => {
    return flattenOutputs([dataset])
      .map((d) => d.id)
      .join(',');
  }, [dataset]);

  const paths = useGetEndpoint({
    endpoint: GET_PATH_ENDPOINT,
    params: {
      datasetIds: datasetIds,
      isonline: true,
      investigationId: dataset.investigation.id,
    },
    notifyOnError: false,
  });

  return useMemo(() => {
    if (!paths?.length) return undefined;
    return paths;
  }, [paths]);
}
