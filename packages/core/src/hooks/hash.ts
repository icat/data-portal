import { hashValue } from 'helpers/hash';
import { useEffect, useState } from 'react';

export function useHashEffect(callback: () => void, dep: any) {
  const [hash, setHash] = useState<string>(hashValue(dep));

  const [trigger, setTrigger] = useState<boolean>(false);

  if (hashValue(dep) !== hash) {
    setHash(hashValue(dep));
    setTrigger(true);
  }

  useEffect(() => {
    if (!trigger) return;
    callback();
    setTrigger(false);
  }, [callback, trigger]);

  return trigger;
}
