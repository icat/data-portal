import {
  DATASET_STATUS_ARCHIVED,
  DATASET_STATUS_ENDPOINT,
  DATASET_STATUS_ONLINE,
  DATASET_STATUS_RESTORING,
  useGetEndpoint,
  type Dataset,
} from '@edata-portal/icat-plus-api';
import { flattenOutputs } from 'helpers';
import { useMemo } from 'react';

export function useDatasetStatus(dataset: Dataset) {
  const ids = useMemo(() => {
    return flattenOutputs([dataset])
      .map((d) => d.id)
      .join(',');
  }, [dataset]);

  const allStatus = useGetEndpoint({
    endpoint: DATASET_STATUS_ENDPOINT,
    params: {
      datasetIds: ids,
    },
    notifyOnError: false,
  });

  return useMemo(() => {
    if (!allStatus?.length) return undefined;
    if (allStatus.some((s) => s === DATASET_STATUS_ARCHIVED))
      return DATASET_STATUS_ARCHIVED;
    if (allStatus.some((s) => s === DATASET_STATUS_RESTORING))
      return DATASET_STATUS_RESTORING;
    return DATASET_STATUS_ONLINE;
  }, [allStatus]);
}
