import {
  useGetEndpoint,
  type Investigation,
  USER_INSTRUMENT_LIST_ENDPOINT,
  Instrument,
} from '@edata-portal/icat-plus-api';
import { useUser } from 'context';
import { useMemo } from 'react';

export function useInvestigationAccess(props: {
  investigation?: Investigation;
  roles: {
    participant: boolean;
    instrumentScientist: boolean;
    administrator: boolean;
  };
}) {
  const user = useUser();

  const isAuthenticated = useMemo(
    () => user !== undefined && user.isAuthenticated,
    [user],
  );

  const isAdministrator = useMemo(() => user?.isAdministrator, [user]);

  const needsInstruments = useMemo(
    () =>
      props.investigation &&
      isAuthenticated &&
      !isAdministrator &&
      user?.isInstrumentScientist &&
      props.roles.instrumentScientist,
    [
      props.investigation,
      props.roles.instrumentScientist,
      isAuthenticated,
      isAdministrator,
      user?.isInstrumentScientist,
    ],
  );

  const instruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    params: {
      filter: 'instrumentscientist',
    },
    default: [] as Instrument[],
    skipFetch: !needsInstruments,
  });

  const isInstrumentScientist = useMemo(
    () =>
      props.investigation &&
      instruments?.some(
        (instrument) =>
          instrument.name === props.investigation?.instrument?.name,
      ),
    [instruments, props.investigation],
  );

  const isParticipant = useMemo(
    () =>
      props.investigation &&
      props.investigation.investigationUsers?.some(
        (participant) => participant.user.name === user?.name,
      ),
    [props.investigation, user?.name],
  );

  return useMemo(() => {
    return (
      (props.roles.participant && isParticipant) ||
      (props.roles.instrumentScientist && isInstrumentScientist) ||
      (props.roles.administrator && isAdministrator)
    );
  }, [
    isAdministrator,
    isInstrumentScientist,
    isParticipant,
    props.roles.administrator,
    props.roles.instrumentScientist,
    props.roles.participant,
  ]);
}
