import {
  GET_USER_PREFERENCES_ENDPOINT,
  PUT_USER_PREFERENCES_ENDPOINT,
  useGetEndpoint,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import type { UserPreferences } from '@edata-portal/icat-plus-api';
import { useUser } from 'context';
import { useLocalStorageObject } from 'hooks/localstorage';
import { useCallback, useMemo } from 'react';

export function useUserPreferences<
  T extends keyof UserPreferences = keyof UserPreferences,
>(
  key: T,
  defaultValue: UserPreferences[T],
): readonly [UserPreferences[T], (value: UserPreferences[T]) => void] {
  const preferences = useGetEndpoint({
    endpoint: GET_USER_PREFERENCES_ENDPOINT,
  });

  const updatePreferences = useMutateEndpoint({
    endpoint: PUT_USER_PREFERENCES_ENDPOINT,
  });

  const user = useUser();

  const [localPreferences, updateLocalPreferences] =
    useLocalStorageUserPreferences();

  const value = useMemo(() => {
    if (user?.isAuthenticated) {
      return preferences && preferences[key] !== undefined
        ? preferences[key]
        : defaultValue;
    } else {
      return localPreferences && localPreferences[key] !== undefined
        ? (localPreferences[key] as UserPreferences[T])
        : defaultValue;
    }
  }, [user?.isAuthenticated, preferences, key, defaultValue, localPreferences]);

  const setValue = useCallback(
    (value: UserPreferences[T]) => {
      if (user?.isAuthenticated) {
        updatePreferences.mutate({
          body: {
            [key]: value,
          },
        });
      } else {
        updateLocalPreferences({
          [key]: value,
        });
      }
    },
    [key, updateLocalPreferences, updatePreferences, user?.isAuthenticated],
  );

  const res = useMemo(() => [value, setValue] as const, [value, setValue]);

  return res;
}

const LOCAL_STORAGE_KEY = 'LOCAL_USER_PREFERENCE_ANONYMOUS';

export function useLocalStorageUserPreferences() {
  const [value, setValue] = useLocalStorageObject(LOCAL_STORAGE_KEY);

  const updatePreferences = useCallback(
    (newPreferences: Partial<UserPreferences>) => {
      setValue({
        ...value,
        ...newPreferences,
      });
    },
    [value, setValue],
  );

  return [value, updatePreferences] as const;
}
