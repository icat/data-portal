import {
  useGetEndpoint,
  type Investigation,
  USER_INSTRUMENT_LIST_ENDPOINT,
  Instrument,
  INVESTIGATION_USERS_LIST_ENDPOINT,
  InvestigationUser,
  PRINCIPAL_INVESTIGATOR_ROLE,
  LOCAL_CONTACT_ROLE,
} from '@edata-portal/icat-plus-api';
import { useUser } from 'context';
import { useMemo } from 'react';

export function useIsInvestigationUsersManager(investigation: Investigation) {
  const user = useUser();

  const isAuthenticated = useMemo(
    () => user !== undefined && user.isAuthenticated,
    [user],
  );

  const needsInstruments = useMemo(
    () => isAuthenticated && user?.isInstrumentScientist,
    [isAuthenticated, user],
  );

  const needsInvestigationUsers = useMemo(
    () => isAuthenticated,
    [isAuthenticated],
  );

  const instruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    params: {
      filter: 'instrumentscientist',
    },
    default: [] as Instrument[],
    skipFetch: !needsInstruments,
  });

  const investigationParticipants = useGetEndpoint({
    endpoint: INVESTIGATION_USERS_LIST_ENDPOINT,
    params: {
      investigationId: investigation.id.toString(),
    },
    default: [] as InvestigationUser[],
    skipFetch: !needsInvestigationUsers,
  });

  const isInstrumentScientist = useMemo(
    () =>
      instruments?.some(
        (instrument) => instrument.name === investigation.instrument?.name,
      ),
    [instruments, investigation.instrument],
  );

  const roles = [PRINCIPAL_INVESTIGATOR_ROLE, LOCAL_CONTACT_ROLE];
  const hasRole = investigationParticipants.some((investigationUser) => {
    return (
      investigationUser.name === user?.name &&
      roles.some((role) => role === investigationUser.role)
    );
  });

  return useMemo(
    () => isInstrumentScientist || hasRole,
    [isInstrumentScientist, hasRole],
  );
}
