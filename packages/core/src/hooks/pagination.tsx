import type { OnChangeFn, PaginationState } from '@tanstack/react-table';
import { GetEndpointArgs, useGetEndpoint } from '@edata-portal/icat-plus-api';
import { useHashEffect } from 'hooks/hash';
import { useParam } from 'hooks/param';
import React, { useCallback, useMemo, useState } from 'react';
import { Button, ButtonGroup, Col, FormSelect, Row } from 'react-bootstrap';
import ReactSelect from 'react-select';
import { useUserPreferences } from 'hooks/preferences';
import {
  REACT_SELECT_COMPACT_STYLES,
  scrollPageFromElement,
  useIsScrolledTo,
} from 'helpers';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleDoubleDown,
  faAngleDoubleUp,
} from '@fortawesome/free-solid-svg-icons';
import { LogoHeader, LogoHeaderType } from 'components/utils/LogoHeader';

export interface Pagination {
  page: number;
  size: number;
  queryParams: {
    limit?: number;
    skip?: number;
  };
  handlePageChange: (newPage: number) => void;
  handlePageSizeChange: (newSize: number) => void;
  availableSizes: number[];
  logo?: LogoHeaderType;
}

export function PaginationMenu({
  pagination,
  totalPages,
  totalElements,
  btnVariant = 'primary',
  margin,
  showScrollToTop = false,
  showScrollToBottom = false,
  logo,
}: {
  pagination: Pagination;
  totalPages: number;
  totalElements: number;
  btnVariant?: string;
  margin?: string;
  showScrollToTop?: boolean;
  showScrollToBottom?: boolean;
  logo?: LogoHeaderType;
}) {
  const canPrevious = pagination.page > 0;
  const canNext = pagination.page < totalPages - 1;
  const hasMultiplePages = totalPages > 1;

  const displayTotalElements = totalElements;

  const [ref, setRef] = useState<HTMLDivElement>();

  const isScrolledTo = useIsScrolledTo(ref);

  if (displayTotalElements === 0 || totalPages === 0) return null;

  return (
    <Row
      style={{
        margin: margin || '0.5rem 0',
        alignItems: 'center',
      }}
      className="g-2 small"
      onClick={(e) => {
        e.stopPropagation();
      }}
      ref={setRef}
    >
      {hasMultiplePages && (
        <Col
          xs="auto"
          style={{
            paddingLeft: 0,
          }}
        >
          <ButtonGroup>
            <Button
              variant={btnVariant}
              size="sm"
              onClick={() => pagination.handlePageChange(0)}
              disabled={!canPrevious}
              className="p-1"
            >
              {'<<'}
            </Button>
            <Button
              variant={btnVariant}
              size="sm"
              onClick={() => pagination.handlePageChange(pagination.page - 1)}
              disabled={!canPrevious}
            >
              {'<'}
            </Button>
            <Button
              variant={btnVariant}
              size="sm"
              onClick={() => pagination.handlePageChange(pagination.page + 1)}
              disabled={!canNext}
            >
              {'>'}
            </Button>
            <Button
              variant={btnVariant}
              size="sm"
              onClick={() => pagination.handlePageChange(totalPages - 1)}
              disabled={!canNext}
              className="p-1"
            >
              {'>>'}
            </Button>
          </ButtonGroup>
        </Col>
      )}
      {hasMultiplePages && (
        <Col xs="auto" style={{ display: 'flex', alignItems: 'center' }}>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              gap: 5,
            }}
          >
            Page
            <ReactSelect
              className="text-black"
              value={{
                value: pagination.page,
                label: pagination.page + 1,
              }}
              onChange={(e) => {
                pagination.handlePageChange(Number(e?.value));
              }}
              options={Array.from({ length: totalPages }).map((_, i) => ({
                value: i,
                label: i + 1,
              }))}
              menuPortalTarget={document.body}
              styles={{
                ...REACT_SELECT_COMPACT_STYLES,
              }}
            />
            of {totalPages}
          </div>
        </Col>
      )}
      <Col xs="auto" style={{ display: 'flex', alignItems: 'center' }}>
        <span>
          Items{' '}
          <strong>
            {pagination.page * pagination.size + 1}-
            {Math.min(
              (pagination.page + 1) * pagination.size,
              displayTotalElements,
            )}{' '}
            of {displayTotalElements}
          </strong>
        </span>
      </Col>

      <Col xs="auto">
        <FormSelect
          size="sm"
          value={pagination.size}
          onChange={(e) => {
            pagination.handlePageSizeChange(Number(e.target.value));
          }}
        >
          {pagination.availableSizes.map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </FormSelect>
      </Col>

      {isScrolledTo && showScrollToTop && (
        <Col xs="auto">
          <Button
            size="sm"
            onClick={() => {
              scrollPageFromElement(ref, 'top');
            }}
            disabled={isScrolledTo === 'top'}
          >
            <FontAwesomeIcon icon={faAngleDoubleUp} className="me-1" />
            Top
          </Button>
        </Col>
      )}
      {isScrolledTo && showScrollToBottom && (
        <Col xs="auto">
          <Button
            size="sm"
            onClick={() => {
              scrollPageFromElement(ref, 'bottom');
            }}
            disabled={isScrolledTo === 'bottom'}
          >
            <FontAwesomeIcon icon={faAngleDoubleDown} className="me-1" />
            Bottom
          </Button>
        </Col>
      )}
      {logo && (
        <Col xs="auto" className="ms-auto">
          <LogoHeader header={logo}></LogoHeader>
        </Col>
      )}
    </Row>
  );
}

export type PaginationParams = {
  defaultSize?: number;
  availableSizes?: number[];
  paginationKey: string;
  excludeFromPath?: boolean;
  excludeFromPreferences?: boolean;
};

export function usePagination({
  defaultSize = 20,
  availableSizes = [10, 20, 50, 100, 200],
  paginationKey,
  excludeFromPath = false,
  excludeFromPreferences = false,
}: PaginationParams): Pagination {
  const [pageSizePreference, setPageSizePreference] = usePageSizePreference(
    paginationKey,
    defaultSize,
  );

  const [pagePath, setPagePath] = useParam<string>(
    paginationKey + '-page',
    '0',
  );
  const [sizePath, setSizePath] = useParam<string>(
    paginationKey + '-size',
    pageSizePreference.toString(),
  );
  const [pageLocal, setPageLocal] = useState('0');
  const [sizeLocal, setSizeLocal] = useState(pageSizePreference.toString());

  const page = useMemo(() => {
    return excludeFromPath ? pageLocal : pagePath;
  }, [excludeFromPath, pageLocal, pagePath]);
  const size = useMemo(() => {
    return excludeFromPath ? sizeLocal : sizePath;
  }, [excludeFromPath, sizeLocal, sizePath]);
  const setPage = useCallback(
    (newPage: string) => {
      if (excludeFromPath) {
        setPageLocal(newPage);
      } else {
        setPagePath(newPage);
      }
    },
    [excludeFromPath, setPageLocal, setPagePath],
  );
  const setSize = useCallback(
    (newSize: string) => {
      if (excludeFromPath) {
        setSizeLocal(newSize);
      } else {
        setSizePath(newSize);
      }
    },
    [excludeFromPath, setSizeLocal, setSizePath],
  );

  const handlePageChange = (newPage?: number) => {
    if (newPage !== undefined && newPage !== Number(page)) {
      setPage(newPage.toString());
    }
  };

  const handlePageSizeChange = (newSize?: number) => {
    if (newSize !== undefined && newSize !== Number(size)) {
      setSize(newSize.toString());
      !excludeFromPreferences && setPageSizePreference(newSize);
      handlePageChange(0);
    }
  };

  const queryParams = React.useMemo(
    () => ({
      limit: Number(size),
      skip: Number(page) * Number(size),
    }),
    [page, size],
  );

  return {
    page: Number(page),
    size: Number(size),
    queryParams,
    handlePageChange,
    handlePageSizeChange,
    availableSizes,
  };
}

function usePageSizePreference(paginationKey: string, defaultSize: number) {
  const [preferencePageSize, setPreferencePageSize] = useUserPreferences(
    'pagination',
    [],
  );

  const valueForKey = useMemo(() => {
    const entry = preferencePageSize.find((p) => p.key === paginationKey);
    return entry?.value || defaultSize;
  }, [preferencePageSize, paginationKey, defaultSize]);

  const setValue = useCallback(
    (value: number) => {
      const otherValues = preferencePageSize.filter(
        (p) => p.key !== paginationKey,
      );
      if (value === defaultSize) {
        setPreferencePageSize(otherValues);
      } else {
        setPreferencePageSize([...otherValues, { key: paginationKey, value }]);
      }
    },
    [defaultSize, paginationKey, preferencePageSize, setPreferencePageSize],
  );

  return [valueForKey, setValue] as const;
}

export function useEndpointPagination<
  DATA extends any,
  P extends {
    limit?: number | undefined;
    skip?: number | undefined;
  },
>({
  paginationParams,
  ...args
}: GetEndpointArgs<DATA[], P, undefined> & {
  paginationParams: PaginationParams;
}): {
  data: DATA[];
  pagination: Pagination;
  totalPages: number;
  totalElements: number;
  tablePagination: {
    manualPagination: true;
    state: {
      pagination: {
        pageIndex: number;
        pageSize: number;
      };
    };
    onPaginationChange: OnChangeFn<PaginationState>;
    pageCount: number;
  };
  goToPage: (page: number) => void;
} {
  const pagination = usePagination(paginationParams);

  useHashEffect(() => {
    pagination.handlePageChange(0);
  }, args.params);

  const data = useGetEndpoint({
    ...args,
    params: {
      ...args.params,
      ...pagination.queryParams,
    } as P,
    default: [] as any[] as DATA[],
  });

  const tablePagination = React.useMemo(
    () => ({
      pageIndex: pagination.page,
      pageSize: pagination.size,
    }),
    [pagination.page, pagination.size],
  );

  const onPaginationChange: OnChangeFn<PaginationState> = (updater) => {
    if (typeof updater === 'function') {
      const state = updater(tablePagination);
      pagination.handlePageChange(state.pageIndex);
      pagination.handlePageSizeChange(state.pageSize);
    } else {
      const state = updater;
      pagination.handlePageChange(state.pageIndex);
      pagination.handlePageSizeChange(state.pageSize);
    }
  };

  const firstElement = useMemo(
    () => (Array.isArray(data) && data.length ? data[0] : undefined) as any,
    [data],
  );
  const totalElements = useMemo(() => {
    const res = firstElement?.meta?.page?.total;
    return res ? res : 0;
  }, [firstElement]);

  const totalPages = useMemo(() => {
    const res = firstElement?.meta?.page?.totalPages;
    return res ? res : 0;
  }, [firstElement]);

  const goToPage = useCallback(
    (page: number) => {
      pagination.handlePageChange(page);
    },
    [pagination],
  );

  return {
    data,
    pagination,
    totalPages,
    totalElements,
    tablePagination: {
      manualPagination: true,
      state: {
        pagination: tablePagination,
      },
      onPaginationChange,
      pageCount: totalPages,
    },
    goToPage,
  };
}
