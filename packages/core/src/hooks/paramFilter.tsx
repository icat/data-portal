import { useCallback, useMemo } from 'react';

export type DatasetParameterFilter = {
  name: string;
  op: string;
  value: string;
};

export function useDatasetParameterFilter({
  filters,
  setFilters,
  name,
  operator,
}: {
  filters: string;
  setFilters: (filters: string) => void;
  name: string;
  operator: string;
}) {
  const parseFilters = useCallback((filters: string) => {
    return filters
      .trim()
      .split(',')
      .map((v) => v.trim())
      .filter((v) => v.length > 0)
      .map((v) => {
        const [name, op, value] = v.split('~');
        return { name, op, value };
      });
  }, []);
  const paramFilterValues: DatasetParameterFilter[] = useMemo(() => {
    return parseFilters(filters);
  }, [filters, parseFilters]);

  const value = useMemo(() => {
    const v = paramFilterValues.find(
      (v) => v.name === name && v.op === operator,
    );
    return v ? v.value : undefined;
  }, [paramFilterValues, name, operator]);

  const setValue = useCallback(
    (value?: number) => {
      const newValues = parseFilters(filters);
      const v = newValues.find((v) => v.name === name && v.op === operator);
      if (v) {
        if (value === undefined) {
          newValues.splice(newValues.indexOf(v), 1);
        } else if (v.value === value.toString()) {
          setFilters(filters);
          return;
        } else {
          v.value = value?.toString() || '';
        }
      } else {
        if (!value) {
          setFilters(filters);
          return;
        }
        newValues.push({
          name,
          op: operator,
          value: value.toString(),
        });
      }
      setFilters(
        newValues.map((v) => `${v.name}~${v.op}~${v.value}`).join(','),
      );
    },
    [filters, name, operator, parseFilters, setFilters],
  );

  return [value, setValue] as const;
}
