import { useCallback, useMemo } from 'react';
import qs from 'qs';
import { useSearchParams } from 'react-router-dom';

export function useParams(): [
  { [k: string]: string },
  (
    key: string,
    value?: string,
    isNavigation?: boolean,
    resetOthers?: boolean,
  ) => void,
] {
  const [searchParams, setSearchParams] = useSearchParams();

  const paramsObject = useMemo(() => {
    return Object.fromEntries(searchParams);
  }, [searchParams]);

  const setParam = useCallback(
    (
      key: string,
      value?: string,
      isNavigation?: boolean,
      resetOthers?: boolean,
    ) => {
      // searchParams from the hook is not updated immediately when
      // setSearchParams is called multiple times in a row,
      // so we need to use the value from the window.location.search
      // to be sure it is fully up to date.
      const newValue = resetOthers
        ? {}
        : (qs.parse(window.location.search, {
            ignoreQueryPrefix: true,
          }) as any);

      if (value === undefined) {
        delete newValue[key];
      } else {
        newValue[key] = value;
      }

      setSearchParams(newValue, {
        replace: !isNavigation,
        preventScrollReset: true,
      });
    },
    [setSearchParams],
  );

  return [paramsObject, setParam];
}
export function useParam<T extends string = string>(
  paramName: string,
  defaultValue: T,
  isNavigation?: boolean,
  resetOthers?: boolean,
): [T, (value?: T) => void] {
  const [params, setParam] = useParams();

  const setValue = (value?: T) => {
    setParam(paramName, value, isNavigation, resetOthers);
  };

  const value = useMemo(() => {
    const v = params[paramName];
    if (v === undefined) return defaultValue;
    return v as T;
  }, [params, paramName, defaultValue]);

  return [value as T, setValue];
}
