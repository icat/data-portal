import type { Dataset, Parameter } from '@edata-portal/icat-plus-api';
import { immutableArray } from 'helpers/array';
import {
  DATASET_TYPE_ACQUISITION,
  DATASET_TYPE_PROCESSED,
  DATASET_TYPE_REPROCESSED,
} from '@edata-portal/icat-plus-api';
import { prettyPrint } from 'helpers/string';
import { unit } from 'mathjs';
import { convertToFixed } from 'helpers/numeric';
import { sortParameters } from 'helpers/metadata';

export const DATASET_NAME_PARAM = 'datasetName';

export const SCAN_TYPE_PARAM = 'scanType';
export const DEFINITION_PARAM = 'definition';

export const PROJECT_NAME_PARAM = 'Project_name';

export function isSameScanType(a: Dataset, b: Dataset) {
  const typeA = getScanType(a)?.toLowerCase();
  const typeB = getScanType(b)?.toLowerCase();
  return typeA === typeB && typeA !== undefined;
}

export function getScanType(dataset: Dataset): string | undefined {
  return getDatasetParamValue(dataset, SCAN_TYPE_PARAM);
}

export function prettyPrintScanType(dataset: Dataset | string): string {
  const scanType = typeof dataset === 'string' ? dataset : getScanType(dataset);
  if (scanType?.trim().toLowerCase() === 'datacollection') {
    return 'Data collection';
  }
  if (scanType !== undefined && scanType !== null) {
    return prettyPrint(scanType);
  }
  return typeof dataset === 'string' ? 'Unknown' : getDatasetName(dataset);
}

export function getDefinition(dataset: Dataset): string | undefined {
  return getDatasetParamValue(dataset, DEFINITION_PARAM);
}

export function getDatasetName(dataset: Dataset): string {
  const nameParam = getDatasetParamValue(dataset, DATASET_NAME_PARAM);

  if (nameParam) {
    return nameParam;
  }
  // The type `Dataset` actually has a `name` property which is an internal ICAT identifier and is not guaranteed to be user-friendly.
  // It was thus removed from the type declaration so that developers do not use it (the DATASET_NAME_PARAM should be used instead).
  // It is only used as fallback here if the param is not found.

  return dataset.name;
}

export function flattenOutputs(datasets: Dataset[] | Dataset): Dataset[] {
  if (!Array.isArray(datasets)) return flattenOutputs([datasets]);
  return [
    ...datasets,
    ...datasets.flatMap((dataset) =>
      flattenOutputs(dataset.outputDatasets || []),
    ),
  ].filter(
    (ds, index, self) => self.findIndex((d) => d.id === ds.id) === index,
  );
}

export function sortDatasetsByDate<T extends { startDate?: string }>(
  datasets: T[],
): T[] {
  return immutableArray(datasets).sort(compareDatasetsByDate).toArray();
}

export function compareDatasetsByDate<T extends { startDate?: string }>(
  a: T,
  b: T,
): number {
  if (!a.startDate) return 1;
  if (!b.startDate) return -1;
  return a.startDate.localeCompare(b.startDate);
}

export function isAcquisition(dataset: Dataset): boolean {
  return dataset.type === DATASET_TYPE_ACQUISITION;
}

export function isProcessed(dataset: Dataset): boolean {
  return dataset.type === DATASET_TYPE_PROCESSED;
}

export function isReprocessed(dataset: Dataset): boolean {
  return dataset.type === DATASET_TYPE_REPROCESSED;
}

export function getDatasetURL(datasetId: number, investigationId: number) {
  return `/investigation/${investigationId}/datasets/${datasetId}`;
}

export function getDatasetParam(dataset: Dataset, name: string) {
  return dataset.parameters.find((p) => p.name === name);
}

export function getDatasetParamValue(
  dataset: Dataset,
  name: string,
  datasetNameRegExp?: string,
) {
  if (datasetNameRegExp) {
    const target = getOutputDatasetByNameRegExp(dataset, datasetNameRegExp);
    if (!target) return undefined;
    const param = getDatasetParam(target, name);
    return param ? param.value : undefined;
  }

  const param = getDatasetParam(dataset, name);
  return param ? param.value : undefined;
}

export function getOutputDatasetByNameRegExp(
  dataset: Dataset,
  datasetNameRegExp: string,
): Dataset | undefined {
  const found = dataset?.outputDatasets?.find(
    (ds: Dataset) => getDatasetName(ds).indexOf(datasetNameRegExp) !== -1,
  );
  return found;
}

export const INPUT_DATASET_ID_PARAM = 'input_datasetIds';

export function getInputDatasetIds(dataset: Dataset): string[] {
  const input = getDatasetParam(dataset, INPUT_DATASET_ID_PARAM);
  return input?.value ? input?.value.split(' ') : [];
}

export function convertUnitDatasetParameter(
  dataset: Dataset,
  parameterName: string,
  newUnit: string,
  digit?: number,
) {
  const value = getDatasetParamValue(dataset, parameterName);
  const units = getDatasetParam(dataset, parameterName)?.units;
  if (value && !isNaN(Number(value)) && units) {
    const convertedValue = unit(Number(value), units).toNumber(newUnit);
    return digit ? convertToFixed(convertedValue, digit) : convertedValue;
  }
  return value;
}

/**
 * Returns the sorted list of Instrument metadata for a given dataset
 * Empty if not Instrument metadata are found
 * @param dataset
 * @returns
 */
export function getInstrumentParameters(dataset: Dataset): Parameter[] {
  const instrumentParams = sortParameters(
    dataset.parameters.filter((parameter) =>
      parameter.name.startsWith('Instrument'),
    ),
  );
  return instrumentParams;
}
