import type { Dataset } from '@edata-portal/icat-plus-api';
import {
  flattenOutputs,
  getDatasetName,
  getDatasetParamValue,
  sortDatasetsByDate,
} from 'helpers/dataset';

export type GalleryItem = {
  id: string;
  name: string;
  path: string;
  dataset: Dataset;
};

/**
 * This function splits the filepath of the file and returns the name of the file with the extension
 * @param filepath
 * @returns
 */
function getFileName(filePath: string): string {
  try {
    const segments = filePath.split('/');
    return segments[segments.length - 1];
  } catch {
    return '';
  }
}

export function getGalleryItems(dataset: Dataset): GalleryItem[] {
  const paths =
    getDatasetParamValue(dataset, 'ResourcesGalleryFilePaths')?.split(',') ||
    [];

  const getResourceIdByIndex = (index: number) => {
    return (
      getDatasetParamValue(dataset, 'ResourcesGallery')?.split(' ')[index] || ''
    );
  };

  const items = paths.map((path, index) => {
    const resource: GalleryItem = {
      id: getResourceIdByIndex(index),
      path: path,
      name: getFileName(path),
      dataset: dataset,
    };
    return resource;
  });

  return items;
}

/**
 * This will return a single gallery item if name or regExp matches, undefined otherwise
 * @param dataset
 * @param imageNameRegExp
 * @returns
 */
export function getGalleryItemByName(
  dataset: Dataset,
  imageNameRegExp: string | RegExp,
) {
  const items = getGalleryItems(dataset).filter((item) => {
    if (typeof imageNameRegExp === 'string') {
      return (
        item.name.toLowerCase().indexOf(imageNameRegExp.toLowerCase()) !== -1
      );
    } else {
      return imageNameRegExp.test(item.name);
    }
  });

  if (items.length > 0) {
    return items[0];
  }
  return undefined;
}

export function getGalleryItemRecursiveByName(
  dataset: Dataset,
  imageNameRegExp: string | RegExp,
  datasetNameRegExp: string | RegExp,
) {
  const target = sortDatasetsByDate(flattenOutputs([dataset]))
    .reverse()
    .find((ds) => {
      const name = getDatasetName(ds);
      if (typeof datasetNameRegExp === 'string') {
        return name.indexOf(datasetNameRegExp) !== -1;
      } else {
        return datasetNameRegExp.test(name);
      }
    });
  if (!target) return undefined;
  return getGalleryItemByName(target, imageNameRegExp);
}
