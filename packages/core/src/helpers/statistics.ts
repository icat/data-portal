export const ELAPSED_TIME = '__elapsedTime';

export const DATASET_VOLUME = '__volume';
export const ACQUISITION_DATASET_VOLUME = '__acquisitionVolume';
export const PROCESSED_DATASET_VOLUME = '__processedVolume';

export const DATASET_COUNT = '__datasetCount';
export const ACQUISITION_DATASET_COUNT = '__acquisitionDatasetCount';
export const PROCESSED_DATASET_COUNT = '__processedDatasetCount';

export const FILE_COUNT = '__fileCount';
export const ACQUISITION_FILE_COUNT = '__acquisitionFileCount';
export const PROCESSED_FILE_COUNT = '__processedFileCount';

export const SAMPLE_COUNT = '__sampleCount';

export const ARCHIVED_DATASET_COUNT = '__archivedDatasetCount';
export const ACQUISITION_ARCHIVED_DATASET_COUNT = '__archivedRawDatasetCount';
export const PROCESSED_ARCHIVED_DATASET_COUNT =
  '__archivedProcessedDatasetCount';
