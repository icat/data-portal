export function parseDOI(doi: string): {
  type: 'DC' | 'ES';
  id: string;
} {
  const [, suffix] = doi.split('/').map((s) => s.trim());

  const [, type, id] = suffix.split('-').map((s) => s.trim());

  if (type.toLocaleLowerCase().includes('es')) {
    return {
      type: 'ES',
      id: id,
    };
  } else {
    return {
      type: 'DC',
      id: id,
    };
  }
}

export function buildDOIFromDataCollectionId(
  dataCollectionId: number,
  facilityPrefix: string,
  facilitySuffix: string,
) {
  return `${facilityPrefix}/${facilitySuffix}-${dataCollectionId}`;
}
