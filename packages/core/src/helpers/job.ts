import type { Job } from '@edata-portal/icat-plus-api';
import { immutableArray } from 'helpers/array';

export function getJobStepNames(job: Job) {
  return job.steps
    .map((step) => step.name)
    .filter((step, index, self) => self.indexOf(step) === index);
}

export function getLastStepByName(job: Job, stepName: string) {
  const step = immutableArray(job.steps)
    .sort(sortByCreatedAt)
    .reverse()
    .find((step) => step.name === stepName);
  return step;
}

/**
 * This returns the last step's status for each step name.
 */
export function getLastSteps(job: Job) {
  const stepsName = getJobStepNames(job);
  return stepsName.map(
    (step) => getLastStepByName(job, step) || { name: step, status: 'UNKNOWN' },
  );
}

export function sortByCreatedAt<T extends { createdAt: string }>(a: T, b: T) {
  return a.createdAt.localeCompare(b.createdAt);
}
