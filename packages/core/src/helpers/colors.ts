export interface Color {
  red: number;
  blue: number;
  green: number;
}

export type ColorGradient = Record<number, Color>;

export const COLORS = {
  red: { red: 255, green: 0, blue: 0 },
  green: { red: 0, green: 255, blue: 0 },
  blue: { red: 0, green: 0, blue: 255 },
  yellow: { red: 255, green: 255, blue: 0 },
  orange: { red: 255, green: 165, blue: 20 },
  light_green: { red: 144, green: 238, blue: 144 },
};

/**
 * Calculates the color value for a given gradient and value
 * @param gradient The gradient to use: a set of colors defined for specific values. The colors in between these values will be interpolated.
 * @param value The value to calculate the color for
 * @returns The calculated color for the value
 * @throws If the gradient is invalid (no colors)
 * @example getGradientValue({ 0: { red: 255, green: 0, blue: 0 }, 10: { red: 0, green: 255, blue: 0 } }, 5) will return the color for 5 in a 0 to 10 red to green gradient
 */
export function getGradientValue(
  gradient: ColorGradient,
  value: number,
): Color {
  const keys = Object.keys(gradient)
    .map(Number)
    .filter((key) => !isNaN(key))
    .sort((a, b) => {
      return a - b;
    });
  if (!keys?.length) throw new Error('Invalid gradient');
  const firstKey = keys[0];
  if (value <= firstKey) return gradient[firstKey];
  const lastKey = keys[keys.length - 1];
  if (value >= lastKey) return gradient[lastKey];
  for (let i = 0; i < keys.length - 1; i++) {
    if (value >= keys[i] && value <= keys[i + 1]) {
      const color1 = gradient[keys[i]];
      const color2 = gradient[keys[i + 1]];
      const ratio = (value - keys[i]) / (keys[i + 1] - keys[i]);
      return {
        red: safeColorValue(color1.red + (color2.red - color1.red) * ratio),
        green: safeColorValue(
          color1.green + (color2.green - color1.green) * ratio,
        ),
        blue: safeColorValue(color1.blue + (color2.blue - color1.blue) * ratio),
      };
    }
  }

  return { red: 0, green: 0, blue: 0 };
}

function safeColorValue(value: number) {
  const rounded = Math.round(value);
  if (rounded > 255) return 255;
  if (rounded < 0) return 0;
  return rounded;
}

export function toCSSColor(color: Color, a: number = 1): string {
  return `rgba(${color.red},${color.green},${color.blue},${a})`;
}

export function toCSSGradient(
  gradient: ColorGradient,
  start: number,
  end: number,
  angle: number = 90,
  a?: number,
): string {
  return `linear-gradient(${angle}deg, ${Object.entries(gradient)
    .map(([key, color]) => {
      const percent = ((Number(key) - start) / (end - start)) * 100;
      return {
        color: toCSSColor(color, a),
        percent,
      };
    })
    .sort((a, b) => a.percent - b.percent)
    .map(({ color, percent }) => `${color} ${percent}%`)
    .join(',')})`;
}
