export const REACT_SELECT_COMPACT_STYLES = {
  //These make the select more compact
  dropdownIndicator: (base: any) => ({
    ...base,
    padding: 2,
  }),
  indicatorSeparator: () => ({
    display: 'none',
  }),
  valueContainer: (base: any) => ({
    ...base,
    padding: 2,
  }),
  menu: (base: any) => ({
    ...base,
    width: 'max-content',
    minWidth: '100%',
  }),
};
