export type MetadataFilterValue = {
  parameter: string;
  value?: string;
  operator?: MetadataFilterOperator;
};

const METADATA_FILTER_OPERATORS = [
  'eq',
  'like',
  'gt',
  'gteq',
  'lt',
  'lteq',
  'in',
] as const;

type MetadataFilterOperator = (typeof METADATA_FILTER_OPERATORS)[number];

export function parseMetadataFilters(filters: string): MetadataFilterValue[] {
  return filters
    .split(',')
    .map((filter) => {
      const [parameter, operatorString, value] = filter.split('~');
      if (!parameter) {
        return undefined;
      }
      return {
        parameter,
        value,
        operator: parseOperator(operatorString),
      };
    })
    .filter((filter) => filter !== undefined) as MetadataFilterValue[];
}

function parseOperator(
  operatorString: string,
): MetadataFilterOperator | undefined {
  if (!METADATA_FILTER_OPERATORS.includes(operatorString as any)) {
    return undefined;
  }
  return operatorString as MetadataFilterOperator;
}

export function formatMetadataFilters(filters: MetadataFilterValue[]): string {
  return filters
    .map((filter) => {
      if (!filter.operator || !filter.value) return filter.parameter;
      return [filter.parameter, filter.operator, filter.value].join('~');
    })
    .join(',');
}

export type MetadataFilterDefinition = {
  label: string;
  parameter: string;
  type: 'select-single' | 'select-multi' | 'range' | 'search';
  step?: number;
  doPrettyPrint: boolean;
};

export function clearMetadataFiltersForDefinition(
  filters: MetadataFilterValue[],
  definition: MetadataFilterDefinition,
) {
  const filtersForDefinition = getMetadataFiltersForDefinition(
    filters,
    definition,
  );
  return filters.slice().filter((f) => {
    return !filtersForDefinition.includes(f);
  });
}

export function getMetadataFiltersForDefinition(
  filters: MetadataFilterValue[],
  definition: MetadataFilterDefinition,
) {
  const operators = getMetadataFilterOperatorsForDefinition(definition);
  return filters.slice().filter((f) => {
    if (f.parameter !== definition.parameter) return false;
    if (!f.operator) return false;
    return operators.includes(f.operator);
  });
}

export function getMetadataFilterOperatorsForDefinition(
  definition: MetadataFilterDefinition,
): MetadataFilterOperator[] {
  if (definition.type === 'range') {
    return ['gteq', 'lteq'];
  }
  if (definition.type === 'select-single') {
    return ['eq'];
  }
  if (definition.type === 'select-multi') {
    return ['in'];
  }
  if (definition.type === 'search') {
    return ['like'];
  }
  return [];
}
