import type {
  Dataset,
  IcatUser,
  WorkflowDescription,
  WorkflowDescriptionKeywords,
} from '@edata-portal/icat-plus-api';
import { getDefinition, getScanType } from 'helpers/dataset';

const REPROCESSING_AVAILABILITY_DAYS = 3 * 30;

export function isValidWorkflow(
  datasets: Dataset[] | undefined,
  user: IcatUser,
  keywords?: WorkflowDescriptionKeywords,
) {
  if (!keywords) {
    return false;
  }
  let {
    instrumentName,
    cardinality,
    definition,
    scanType,
    roles,
    validityDays,
  } = keywords;
  if (datasets === undefined || !datasets || !datasets.length) {
    return false;
  }

  const days = validityDays ? validityDays : REPROCESSING_AVAILABILITY_DAYS;

  // Check default X days of the startDate of the dataset or the investigation.endDate
  try {
    if (datasets.length > 0) {
      const startDate = new Date(
        datasets[0].investigation.endDate || datasets[0].startDate,
      );
      const diffTime = Math.abs(new Date().getTime() - startDate.getTime());
      // Convert the difference from milliseconds to days
      const diffDays = diffTime / (1000 * 3600 * 24);
      if (diffDays > days) {
        return false;
      }
    }
  } catch {
    return false;
  }

  if (
    roles &&
    !(
      (user.isAdministrator && roles.includes('manager')) ||
      (user.isInstrumentScientist && roles.includes('instrumentScientist'))
    )
  ) {
    return false;
  }
  if (datasets && datasets.length === 0) {
    // if no datasets no workflows will be available
    return false;
  }
  // incorrect cardinality
  if (cardinality !== '*' && datasets.length.toString() !== cardinality) {
    return false;
  }

  return datasets.every((dataset: Dataset) => {
    // incorrect instrument
    if (instrumentName !== '*' && dataset.instrumentName !== instrumentName) {
      return false;
    }
    // incorrect scanType
    if (scanType !== '*') {
      const datasetScanType = getScanType(dataset);
      if (!datasetScanType) return false;
      if (Array.isArray(scanType)) {
        if (!scanType.includes(datasetScanType)) {
          return false;
        }
      } else if (datasetScanType !== scanType) {
        return false;
      }
    }
    // incorrect definition
    if (definition !== '*' && getDefinition(dataset) !== definition) {
      return false;
    }
    return true;
  });
}

export function getWorkflowsForDatasets(
  workflows: WorkflowDescription[] | undefined,
  datasets: Dataset[] | undefined,
  user: IcatUser,
) {
  if (workflows?.length && datasets?.length) {
    return workflows.filter((workflow: WorkflowDescription) =>
      isValidWorkflow(datasets, user, workflow.keywords),
    );
  }
  return [];
}
