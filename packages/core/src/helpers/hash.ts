export function hashValue(input: any): string {
  if (Array.isArray(input)) {
    return hashArray(input);
  } else if (typeof input === 'string') {
    return hashString(input);
  } else if (typeof input === 'object') {
    return hashObject(input);
  } else {
    // For other types, convert to string and hash
    return hashString(String(input));
  }
}

function hashString(input: string): string {
  let hash = 0;
  if (input.length === 0) return hash.toString();

  for (let i = 0; i < input.length; i++) {
    const char = input.charCodeAt(i);
    hash = (hash << 5) - hash + char;
    hash = hash & hash; // Convert to 32-bit integer
  }

  return hash.toString();
}

function hashArray(input: any[]): string {
  const arrayString = input.map(hashValue).join();
  return hashString(arrayString);
}

function hashObject(input: object): string {
  if (input === null) {
    return hashString('null');
  }
  const keys = Object.keys(input).sort();
  const keyValues = keys.map(
    (key) => `${key}:${hashValue((input as any)[key])}`,
  );
  return hashValue(keyValues);
}
