export function capitalize<T>(str: string | T) {
  if (typeof str === 'string') {
    return str.charAt(0).toLocaleUpperCase() + str.slice(1);
  }
  return str;
}

export function removeAccents(str: string) {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

/**
 * This function will transform snake case to words with spaces.
 */
export function addSpacesToSnakeCase(str: string) {
  return str.replace(/_/g, ' ');
}

/**
 * This function will transform camel case to words with spaces.
 */
export function addSpacesToCamelCase(str: string) {
  return str.replace(/([A-Z])/g, ' $1').trim();
}

/**
 * This function will capitalize the first word of each sentence and lower everything else.
 */
export function capitalizeFirstWords(str: string) {
  return str
    .split('.')
    .map((v) => v.toLocaleLowerCase())
    .map(capitalize)
    .join('.');
}

/**
 * This function will transform snake case or camel case to words with spaces and capitalize the first word of each sentence.
 */
export function prettyPrint(str: string) {
  return capitalizeFirstWords(addSpacesToCamelCase(addSpacesToSnakeCase(str)));
}
