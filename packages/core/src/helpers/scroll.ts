import { useCallback, useEffect, useState } from 'react';

const SCROLLABLE_DETECTION_MARGIN = 100;

export function getScrollRootFromElementRef(
  ref: React.RefObject<HTMLElement> | HTMLElement,
) {
  //Find the scroll root : closest parent that can be scrolled

  let element = ref instanceof HTMLElement ? ref : ref.current;
  while (element) {
    if (
      element.scrollHeight >
      element.clientHeight + SCROLLABLE_DETECTION_MARGIN
    ) {
      return element;
    }
    element = element.parentElement;
  }
  return undefined;
}

export function scrollPageFromElement(
  ref: React.RefObject<HTMLElement> | HTMLElement | undefined,
  to: 'top' | 'bottom',
) {
  const root = ref ? getScrollRootFromElementRef(ref) : undefined;

  if (root) {
    root.scrollTo({
      top: to === 'top' ? 0 : root.scrollHeight,
      behavior: 'smooth',
    });
  }
}

export function useIsScrolledTo(
  element: React.RefObject<HTMLElement> | HTMLElement | undefined,
): 'top' | 'bottom' | 'middle' | undefined {
  const [value, setValue] = useState<'top' | 'bottom' | 'middle' | undefined>(
    undefined,
  );

  const [root, setRoot] = useState<HTMLElement | undefined>(undefined);

  const findRoot = useCallback(() => {
    if (element) {
      const newRoot = getScrollRootFromElementRef(element);
      setRoot(newRoot);
    } else {
      setRoot(undefined);
    }
  }, [element]);

  useEffect(() => {
    findRoot();
  }, [findRoot]);

  useEffect(() => {
    const observer = new window.MutationObserver(() => {
      findRoot();
    });
    observer.observe(document.body, { childList: true, subtree: true });

    return () => {
      observer.disconnect();
    };
  }, [findRoot]);

  useEffect(() => {
    if (!root) {
      setValue(undefined);
      return;
    }
    const update = () => {
      if (root.scrollTop === 0) {
        setValue('top');
      } else if (root.scrollHeight - root.scrollTop === root.clientHeight) {
        setValue('bottom');
      } else {
        setValue('middle');
      }
    };

    update();

    root.addEventListener('scroll', update);
    root.addEventListener('resize', update);

    return () => {
      root.removeEventListener('scroll', update);
      root.removeEventListener('resize', update);
    };
  }, [root]);

  return value;
}
