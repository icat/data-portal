import {
  Investigation,
  PRINCIPAL_INVESTIGATOR_ROLE,
  COLLABORATOR_ROLE,
  InvestigationUser,
  InvestigationUserRole,
} from '@edata-portal/icat-plus-api';
import { addToDate, parseDate } from 'helpers/date';

export function isReleasedInvestigation(
  investigation?: Investigation,
): boolean {
  if (investigation) {
    if (!investigation.doi) {
      return false;
    }
    const releaseDate = parseDate(investigation.releaseDate);
    if (!releaseDate) {
      return false;
    }
    return releaseDate.getTime() < Date.now();
  }
  return false;
}

export function isFinishedInvestigation(
  investigation?: Investigation,
): boolean {
  if (investigation) {
    const endDate = parseDate(investigation.endDate);
    if (!endDate) {
      return false;
    }
    // investigation is finished for sure 48 hours after the end date
    const finishedDate = addToDate(endDate, { hours: 48 });
    if (!finishedDate) {
      return false;
    }
    return finishedDate.getTime() <= Date.now();
  }
  return false;
}

export function sortInvestigationByDistanceFromNow(
  a: Investigation,
  b: Investigation,
) {
  const aDate = parseDate(a.startDate)?.getTime() || 0;
  const bDate = parseDate(b.startDate)?.getTime() || 0;
  const now = new Date().getTime();

  const aDistance = Math.abs(now - aDate);
  const bDistance = Math.abs(now - bDate);

  return aDistance - bDistance;
}

/**
 * Returns true if the investigation is considered as scheduled. A investigation is considered as scheduled if there is the parameter Id within the investigation.
 * The parameter id is the id of the investigation in the user portal.
 * Old investigations has not Id because it was not exported so we check that the visitId < 15 characters (old investigations had the beamline name in the field visit Id which is < 15 characters for the non-scheduled
 * and the title of the proposal for the scheduled ones)
 *
 * TODO: Facility especific
 * @param investigation
 * @returns
 */
export function isScheduled(investigation: Investigation): boolean {
  const idParameter = getInvestigationParam(investigation, 'Id');
  if (!idParameter) {
    if (investigation.visitId.trim().length < 15) return false;
  }
  return true;
}

/**
 * Given a investigation it will retrieve the innstrument name
 * @param investigation
 * @returns
 */
export function getInstrumentNameByInvestigation(investigation: Investigation) {
  if (investigation.instrument?.name) {
    return investigation.instrument.name;
  }
  const investigationInstruments = investigation.investigationInstruments || [];
  for (const instrument of investigationInstruments) {
    if (instrument.instrument.name) {
      return instrument.instrument.name;
    }
  }
  return 'unknown';
}

export function getInvestigationParam(
  investigation: Investigation,
  name: string,
): string | undefined {
  return investigation.parameters[name];
}

export function isPrincipalInvestigator<T extends { name: string }>(
  icatUser: T | undefined,
  users: InvestigationUser[],
) {
  return isRole(icatUser, users, PRINCIPAL_INVESTIGATOR_ROLE);
}

export function isCollaborator<T extends { name: string }>(
  icatUser: T | undefined,
  users: InvestigationUser[],
) {
  return isRole(icatUser, users, COLLABORATOR_ROLE);
}

export function isRole<T extends { name: string }>(
  icatUser: T | undefined,
  users: InvestigationUser[],
  role: InvestigationUserRole,
) {
  if (!icatUser) return false;
  return users
    .filter((u) => u.role === role)
    .some((u) => u.name === icatUser.name);
}
