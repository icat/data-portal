export const UNIT_PREFIXES = [
  '',
  'K',
  'M',
  'G',
  'T',
  'P',
  'E',
  'Z',
  'Y',
] as const;

export type UnitPrefix = (typeof UNIT_PREFIXES)[number];

const MULTIPLIER = 1000;

type ValueType = number | undefined | null | string;

function convertValue(value: ValueType) {
  if (typeof value === 'string') {
    return parseFloat(value);
  }
  if (typeof value === 'number') {
    return value;
  }
  return 0;
}

/**
 * Returns a friendly readable string
 */
export function stringifyWithUnitPrefix(
  value: ValueType,
  fixedPoint: number,
  unit: string = '',
  from: UnitPrefix = '',
) {
  const asNumber = convertValue(value);
  if (asNumber === 0) {
    return '0' + unit;
  }
  const bestPrefix = getBestPrefix(asNumber, from);
  const res = toUnitPrefix(asNumber, bestPrefix, from);
  const fixed = res.toFixed(fixedPoint);
  if (unit?.length) return `${fixed} ${bestPrefix}${unit}`;
  return `${fixed}${bestPrefix}`;
}

export function toUnitPrefix(
  value: number,
  to: UnitPrefix,
  from: UnitPrefix = '',
) {
  const asBytes = fromUnitPrefix(value, from);
  const index = UNIT_PREFIXES.indexOf(to);
  return asBytes / MULTIPLIER ** index;
}

export function fromUnitPrefix(value: number, from: UnitPrefix) {
  const index = UNIT_PREFIXES.indexOf(from);
  return value * MULTIPLIER ** index;
}

export function getBestPrefix(
  value: ValueType | ValueType[],
  from: UnitPrefix = '',
): UnitPrefix {
  const asValue = Array.isArray(value)
    ? Math.max(...value.map(convertValue))
    : convertValue(value);
  if (asValue === 0) return '';
  const asWithoutPrefix = fromUnitPrefix(asValue, from);
  if (asWithoutPrefix < MULTIPLIER) return '';
  const unitIndex = Math.floor(
    Math.log(asWithoutPrefix) / Math.log(MULTIPLIER),
  );
  const maxUnitIndex = UNIT_PREFIXES.length - 1;
  const usedIndex = Math.min(unitIndex, maxUnitIndex);
  return UNIT_PREFIXES[usedIndex];
}

export function stringUnitPrefix(unit: string) {
  const res = unit as UnitPrefix;
  if (!UNIT_PREFIXES.includes(res)) {
    throw new Error(`Invalid byte unit: ${unit}`);
  }
  return res;
}
