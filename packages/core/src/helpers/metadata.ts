import type { Parameter } from '@edata-portal/icat-plus-api';
import { immutableArray } from 'helpers/array';

export function sortParameters(parameters: Parameter[]) {
  return immutableArray(parameters)
    .sort((a, b) =>
      a.name.localeCompare(b.name, undefined, { sensitivity: 'base' }),
    )
    .toArray();
}

/**
 * returns the name after the first underscore and capitalize the first letter
 * eg. shortenParameterName('InstrumentMonochromator_energy') returns 'Energy'
 * @param name
 * @returns
 */
export function shortenParameterName(name: string): string {
  return renderParameterNameAsTitle(name.slice(name.indexOf('_') + 1));
}

/**
 * returns the name in which the underscore is replaced by a space and each first letter is capitalized
 * eg. renderParameterNameAsTitle('blade_front') returns 'Blade Front'
 * @param name
 * @returns
 */
export function renderParameterNameAsTitle(name: string): string {
  return name
    .replaceAll('_', ' ')
    .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());
}

export function getParameterByName(
  name: string,
  parameters?: Parameter[],
): Parameter | undefined {
  if (parameters) {
    return parameters.find((parameter) => parameter.name === name);
  }
  return undefined;
}
