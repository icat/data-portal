import type { Datafile } from '@edata-portal/icat-plus-api';
import type { Config } from 'context';

export function isH5Viewer(datafile: Datafile | string, config: Config) {
  return anyExtensionMatch(datafile, config.ui.h5Viewer.fileExtensions);
}

export function isImageViewer(datafile: Datafile | string, config: Config) {
  return anyExtensionMatch(datafile, config.ui.imageViewer.fileExtensions);
}

export function isTextViewer(datafile: Datafile | string, config: Config) {
  return anyExtensionMatch(datafile, config.ui.textViewer.fileExtensions);
}

function anyExtensionMatch(datafile: Datafile | string, extensions: string[]) {
  const datafileName = typeof datafile === 'string' ? datafile : datafile.name;
  return extensions.some((extension) =>
    datafileName.toLowerCase().endsWith(extension.toLowerCase()),
  );
}

export function isViewerAvailable(datafile: Datafile, config: Config) {
  return (
    isH5Viewer(datafile, config) ||
    isImageViewer(datafile, config) ||
    isTextViewer(datafile, config)
  );
}

export function isZeroBytesFile(datafile: Datafile) {
  return datafile && datafile.fileSize === 0;
}

export type DatafileDirectory = {
  name: string;
  content: DatafileDirectoryContent[];
  type: 'directory';
};

export type DatafileFileItem = {
  name: string;
  type: 'file';
  datafile: Datafile;
};

export type DatafileDirectoryContent = DatafileFileItem | DatafileDirectory;

export function getDatafileDirectoryContent(
  files: Datafile[],
  prefix: string = '/',
): DatafileDirectoryContent[] {
  const items = files.map((file) => {
    const path = file.location.startsWith(prefix)
      ? file.location.slice(prefix.length)
      : file.location;

    const pathItems = path.split('/');
    const name = pathItems[0];
    const res = {
      name,
      isEnd: pathItems.length === 1,
      datafile: file,
    };
    return res;
  });

  const itemsGroupedByName = items.reduce(
    (acc, item) => {
      if (!acc[item.name]) {
        acc[item.name] = [];
      }
      acc[item.name].push(item);
      return acc;
    },
    {} as Record<
      string,
      {
        name: string;
        isEnd: boolean;
        datafile: Datafile;
      }[]
    >,
  );

  return Object.entries(itemsGroupedByName).map(([name, items]) => {
    if (items.length === 1 && items[0].isEnd) {
      const res: DatafileFileItem = {
        name,
        type: 'file',
        datafile: items[0].datafile,
      };
      return res;
    }

    const res: DatafileDirectory = {
      name,
      type: 'directory',
      content: getDatafileDirectoryContent(
        items.map((i) => i.datafile),
        prefix + name + '/',
      ),
    };
    return res;
  });
}

export function getNbItemsDirectory(directory: DatafileDirectory): number {
  return directory.content.reduce((acc, item) => {
    if (item.type === 'directory') {
      return acc + getNbItemsDirectory(item);
    }
    return acc + 1;
  }, 0);
}
