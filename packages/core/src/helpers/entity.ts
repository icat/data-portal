import type { Entity } from '@edata-portal/icat-plus-api';

/**
 * Given an entity that can be a dataset, sample or investigation and a parameter name, it returns the parameter
 * @param entity it might be a sample, investigation or dataset
 * @param name parameter name in ICAT
 * @returns the value of the parameter or undefined if not found
 */
export function getParameterValue(entity: Entity, name: string) {
  const param = getParameter(entity, name);
  return param ? param.value : undefined;
}

/**
 * Given an entity that can be a dataset, sample or investigation and a parameter name, it returns the parameter
 * @param entity it might be a sample, investigation or dataset
 * @param name parameter name in ICAT
 * @returns the value of the parameter or undefined if not found
 */
export function getParameter(entity: Entity, name: string) {
  return entity.parameters.find((p) => p.name === name);
}
