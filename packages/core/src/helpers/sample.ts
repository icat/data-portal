import { getParameter } from 'helpers/entity';
import type { Sample } from '@edata-portal/icat-plus-api';

/**
 * Given an entity that can be a dataset, sample or investigation and a parameter name, it returns the parameter
 * @param entity it might be a sample, investigation or dataset
 * @param name parameter name in ICAT
 * @returns the value of the parameter or undefined if not found
 */
export function getSampleParameterValue(sample: Sample, name: string) {
  const param = getParameter(sample, name);
  return param ? param.value : undefined;
}
