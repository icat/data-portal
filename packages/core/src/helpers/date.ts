import {
  intervalToDuration,
  format,
  parse,
  parseISO,
  isMatch,
  add,
  formatDistanceToNow as formatDistanceToNowDateFns,
  formatDuration as formatDurationText,
  differenceInHours,
  startOfMinute,
  startOfHour,
  startOfDay,
  type Duration,
} from 'date-fns';
import { useMemo } from 'react';
import { useBreakpointValue } from 'hooks';

//All our functions will accept a date in the format of a string, number or Date object
export type DateType = Date | string | number;

// We support the following formats:
export const ICAT_DATE_FORMAT = 'yyyy-MM-dd';
export const UI_DATE_DAY_FORMAT = 'dd/MM/yyyy';
export const UI_DATE_DAYTIME_FORMAT = 'dd/MM/yyyy HH:mm:ss';
export const UI_DATE_TIME_FORMAT = 'HH:mm:ss';
export const UI_DATE_HOUR_FORMAT = 'HH:mm';
export const UI_DATE_DAYTIME_PRECISE_FORMAT = 'dd/MM/yyyy HH:mm:ss.SSS';
export const UI_DATE_TIME_PRECISE_FORMAT = 'HH:mm:ss.SSS';
export const UI_DATE_DAYTIME_SHORT_FORMAT = 'dd/MM/yyyy HH:mm';
export const ICAT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

export const FORMATS = [
  ICAT_DATE_FORMAT,
  UI_DATE_DAY_FORMAT,
  UI_DATE_DAYTIME_FORMAT,
  UI_DATE_TIME_FORMAT,
  UI_DATE_HOUR_FORMAT,
  UI_DATE_DAYTIME_PRECISE_FORMAT,
  UI_DATE_TIME_PRECISE_FORMAT,
  UI_DATE_DAYTIME_SHORT_FORMAT,
  ICAT_DATE_TIME_FORMAT,
];

/**
 * This will format a date to a string using the given format.
 * @param date Date to be formatted (can be a string, number or Date object)
 * @param toFormat Format to be used
 * @returns  The formatted date as a string or undefined if the date is invalid
 */
export function formatDate(
  date: DateType | undefined,
  toFormat: string,
): string | undefined {
  if (!date) return undefined;
  if (typeof date === 'string') {
    const parsed = parseDate(date);
    return parsed ? formatDate(parsed, toFormat) : undefined;
  }
  try {
    return format(date, toFormat);
  } catch (e) {
    console.error(e);
    return undefined;
  }
}

function formatterWith(format: string) {
  return (date: DateType | undefined) => formatDate(date, format);
}
//These are shortcuts to the predefined formats
export const formatDateToIcatDate = formatterWith(ICAT_DATE_FORMAT);
export const formatDateToDay = formatterWith(UI_DATE_DAY_FORMAT);
export const formatDateToDayAndTime = formatterWith(UI_DATE_DAYTIME_FORMAT);
export const formatDateToTime = formatterWith(UI_DATE_TIME_FORMAT);
export const formatDateToHour = formatterWith(UI_DATE_HOUR_FORMAT);
export const formatDateToDayAndPreciseTime = formatterWith(
  UI_DATE_DAYTIME_PRECISE_FORMAT,
);
export const formatDateToPreciseTime = formatterWith(
  UI_DATE_TIME_PRECISE_FORMAT,
);
export const formatDateToDayAndShortTime = formatterWith(
  UI_DATE_DAYTIME_SHORT_FORMAT,
);

export const formatDateTimeToIcatDate = formatterWith(ICAT_DATE_TIME_FORMAT);
/**
 *  This will format a duration to a string in the format HH:mm:ss
 * @param duration Duration to be formatted (in ms) (should be a number or a string representing a number)
 * @param defaultValue Value to be returned if the duration is not valid
 * @returns The formatted duration as a string or the defaultValue if the duration is not valid
 */
export function formatDuration(
  duration: number | string | undefined,
  format: 'clock' | 'text',
  defaultValue: string = 'unknown',
): string {
  if (duration === undefined) return defaultValue;
  const numberDuration =
    typeof duration === 'number' ? duration : Number.parseInt(duration);
  if (Number.isNaN(numberDuration)) return defaultValue;
  const durationObject = intervalToDuration({
    start: 0,
    end: numberDuration,
  });

  if (format === 'text') {
    if (numberDuration < 1000) return defaultValue;
    return formatDurationText(durationObject);
  }

  const zeroPad = (num?: number) => String(num).padStart(2, '0');

  return (
    [durationObject.hours, durationObject.minutes, durationObject.seconds]
      .map((v) => v || 0)
      .map(zeroPad)
      .join(':') || defaultValue
  );
}

function safeReturnDate(date: Date | undefined): Date | undefined {
  //Failed parse can return a date with NaN as timestamp. We replace it by undefined to make it easier to handle.
  return date && !Number.isNaN(date.getTime()) ? date : undefined;
}

/**
 * This will parse a date from a string using one of the known formats.
 * @param date Date to be parsed (can be a string, number or Date object)
 * @param forceFormat Allows to force a format to be used to parse the date (if omitted, the function will try with the known formats)
 * @returns The parsed date as a Date object or undefined if the date is invalid
 */
export function parseDate(
  date: DateType | undefined,
  forceFormat?: string,
): Date | undefined {
  if (date === undefined) return undefined;
  if (date instanceof Date) return date;
  if (typeof date === 'number') return safeReturnDate(new Date(date));
  if (forceFormat) {
    return safeReturnDate(parse(date, forceFormat, new Date()));
  }
  for (const format of FORMATS) {
    if (isMatch(date, format)) {
      return safeReturnDate(parse(date, format, new Date()));
    }
  }
  try {
    return safeReturnDate(parseISO(date));
  } catch (e) {
    console.error(e);
    return undefined;
  }
}

/**
 * This will add a duration to a date.
 * @param date Date to which the duration will be added (can be a string, number or Date object)
 * @param duration Duration to be added as an object: years, months, weeks, days, hours, minutes, seconds
 * @returns The new date as a Date object or undefined if the date is invalid
 */
export function addToDate(
  date: DateType | undefined,
  duration: Duration,
): Date | undefined {
  const parsed = parseDate(date);
  if (!parsed) return undefined;
  return add(parsed, duration);
}

/**
 * This will format a date to a string representing how long ago/in the future the date is in a user friendly format.
 * @param date Date to be formatted (can be a string, number or Date object)
 * @param defaultValue Value to be returned if the date is not valid
 * @returns The formatted date as a string or the defaultValue if the date is not valid
 */
export function formatDistanceToNow(
  date: DateType,
  defaultValue: string = 'unknown',
): string {
  const parsed = parseDate(date);
  if (!parsed) return defaultValue;
  return formatDistanceToNowDateFns(parsed);
}

/**
 * This hook will return a list of timestamps to be used as ticks in a timeline, and a formatter to allow displaying the ticks dates.
 */
export function useTimelineFormatter(
  startTimestamp: number,
  endTimestamp: number,
) {
  const nbTicks = useBreakpointValue({
    xs: 2,
    sm: 4,
    md: 6,
    xl: 8,
  });

  return useMemo(() => {
    const duration = endTimestamp - startTimestamp;
    const durationHour = differenceInHours(startTimestamp, endTimestamp);

    const formatter =
      durationHour < 24
        ? 'HH:mm'
        : durationHour < 4 * 24
          ? ' dd/MM HH:mm'
          : durationHour < 24 * 365
            ? 'dd/MM'
            : 'dd/MM/yyyy';
    const rounding =
      durationHour < 6
        ? startOfMinute
        : durationHour < 6 * 24
          ? startOfHour
          : startOfDay;

    const tickInterval = duration / nbTicks;

    const times = [];
    for (
      let i = startTimestamp + tickInterval;
      i <= endTimestamp - tickInterval;
      i = i + tickInterval
    ) {
      times.push(rounding(i).getTime());
    }

    return {
      formatter: (date: DateType) => formatDate(date, formatter) || 'NA',
      times,
    };
  }, [startTimestamp, endTimestamp, nbTicks]);
}
