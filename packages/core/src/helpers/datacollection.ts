import type { DataCollection } from '@edata-portal/icat-plus-api';

export function getDataCollectionParam(
  dataCollection: DataCollection,
  name: string,
): string | undefined {
  return dataCollection.parameters.find((p) => p.name === name)?.value;
}

export const DATA_COLLECTION_PARAM_TITLE = 'title';
export const DATA_COLLECTION_PARAM_ABSTRACT = 'abstract';
export const DATA_COLLECTION_PARAM_INVESTIGATION_NAMES = 'investigationNames';
export const DATA_COLLECTION_PARAM_INSTRUMENT_NAMES = 'instrumentNames';
export const DATA_COLLECTION_PARAM_MINTED_BY_NAME = 'mintedByName';
export const DATA_COLLECTION_PARAM_MINTED_BY_FULL_NAME = 'mintedByFullName';

export const PUBLISHER_INSTRUMENT = 'PUBLISHER';
