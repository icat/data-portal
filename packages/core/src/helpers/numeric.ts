/**
 * formats a number using fixed-point notation.
 * @param {string} value
 * @param {number} fixed The number of digits to appear after the decimal point
 */
export function convertToFixed(
  value: string | undefined | number,
  fixed: number,
  placeholder = 'NA',
): string {
  try {
    if (value) {
      if (!isNaN(Number(value))) {
        return parseFloat(value as string).toFixed(fixed);
      }
      return (value as number).toFixed(fixed);
    }
  } catch (e) {
    return placeholder;
  }
  return placeholder;
}

export function toExponential(value: string | undefined, digits: number) {
  try {
    if (value) {
      return parseFloat(value).toExponential(digits);
    }
  } catch {
    return '';
  }
  return '';
}

export function toMinutes(seconds: number) {
  return convertToFixed((seconds / 60).toString(), 2);
}

export function toMiliseconds(seconds: number) {
  return convertToFixed((seconds * 1000).toString(), 2);
}

export function toFixedUpperLower(
  value: number | undefined,
  nbDecimals: number,
  upperLower: 'upper' | 'lower',
) {
  if (value === undefined) return undefined;

  const factor = Math.pow(10, nbDecimals);
  const rounded =
    upperLower === 'upper'
      ? Math.ceil(value * factor)
      : Math.floor(value * factor);
  return rounded / factor;
}
