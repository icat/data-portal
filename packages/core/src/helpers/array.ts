import { List } from 'immutable';

//get first element of array
export function first<T>(array?: T[]): T | undefined {
  if (!array || array.length === 0) return undefined;
  return array[0];
}

//get last element of array
export function last<T>(array?: T[]): T | undefined {
  if (!array || array.length === 0) return undefined;
  return array[array.length - 1];
}

export function immutableArray<T>(array?: T[]) {
  return List(array || []);
}

export function removeDuplicates<T>(array: T[]): T[] {
  return array.filter((item, index, self) => {
    return self.indexOf(item) === index;
  });
}
