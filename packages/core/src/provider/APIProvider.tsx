import { IcatPlusAPIContext } from '@edata-portal/icat-plus-api';
import { useConfig, useNotify, useUser } from 'context';
import { useMemo, type PropsWithChildren } from 'react';

export function UnauthenticatedAPIProvider({
  children,
}: PropsWithChildren<{}>) {
  const config = useConfig();
  const notify = useNotify();

  const value = useMemo(
    () =>
      ({
        baseUrl: config.api.icat_url || '',
        onError: (message: string) =>
          notify({
            type: 'danger',
            message,
          }),
      }) as const,
    [config.api.icat_url, notify],
  );

  return (
    <IcatPlusAPIContext.Provider value={value}>
      {children}
    </IcatPlusAPIContext.Provider>
  );
}

export function AuthenticatedAPIProvider({ children }: PropsWithChildren<{}>) {
  const user = useUser();
  const config = useConfig();
  const notify = useNotify();

  const value = useMemo(
    () =>
      ({
        baseUrl: config.api.icat_url || '',
        sessionId: user?.sessionId,
        sessionIdExpirationTime: user?.expirationTime,
        onExpiredSessionId: user?.logout,
        onError: (message: string) =>
          notify({
            type: 'danger',
            message,
          }),
      }) as const,
    [
      config.api.icat_url,
      user?.sessionId,
      user?.expirationTime,
      user?.logout,
      notify,
    ],
  );

  return (
    <IcatPlusAPIContext.Provider value={value}>
      {children}
    </IcatPlusAPIContext.Provider>
  );
}
