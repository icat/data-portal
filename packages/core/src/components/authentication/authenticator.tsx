import React, { useCallback, useMemo, useState } from 'react';
import {
  SESSION_CREATE_ENDPOINT,
  IcatUser,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { GenericAuthenticator, UserContext } from 'context';
import { usePersistedUserState } from 'components/authentication/persist';
import { useUserState } from 'components/authentication/userState';
import { useAnonymous } from 'components/authentication/anonymous';
import { Loading } from 'components/utils';

export interface AuthenticationState {
  login: (
    authenticator: GenericAuthenticator,
    username?: string,
    password?: string,
    persist?: boolean,
    suffix?: string,
  ) => Promise<IcatUser>;
  logout: () => void;
  user?: IcatUser;
  isAuthenticated: boolean;
  showLoginPopup: boolean;
  setShowLoginPopup: (value: boolean) => void;
}

const PERSISTED_USER_KEY = 'persisted-user';

function useAuthenticatorState(): AuthenticationState {
  const [persistedUser, setPersistedUser] =
    usePersistedUserState(PERSISTED_USER_KEY);
  const [nonPersistedUser, setNonPersistedUser] = useUserState();
  const anonymous = useAnonymous();

  const user = useMemo(() => {
    // priority is : persistedUser > nonPersistedUser (from openid for instance) > anonymous
    if (persistedUser) return persistedUser;
    if (nonPersistedUser) return nonPersistedUser;
    return anonymous;
  }, [nonPersistedUser, persistedUser, anonymous]);

  const setUser = useCallback(
    (newUser?: IcatUser, persist?: boolean) => {
      if (newUser) {
        if (persist) {
          setPersistedUser(newUser);
        } else {
          setNonPersistedUser(newUser);
        }
      } else {
        setPersistedUser(undefined);
        setNonPersistedUser(undefined);
      }
    },
    [setPersistedUser, setNonPersistedUser],
  );

  const isAuthenticated = useMemo(
    () => user !== undefined && user !== anonymous,
    [user, anonymous],
  );

  const [showLoginPopup, setShowLoginPopup] = useState<boolean>(false);

  const createSession = useMutateEndpoint({
    endpoint: SESSION_CREATE_ENDPOINT,
  });

  const login = useCallback(
    (
      authenticator: GenericAuthenticator,
      username?: string,
      password?: string,
      persist: boolean = true,
      suffix?: string,
    ) => {
      return createSession
        .mutateAsync({
          body: {
            plugin: authenticator.plugin,
            username,
            password,
            usernameSuffix: suffix,
          },
        })
        .then((newUser) => {
          if (newUser) {
            setUser(newUser, persist);
            setShowLoginPopup(false);
          } else {
            setUser(undefined);
            throw new Error('Unexpected error');
          }
          return newUser;
        })
        .catch(() => {
          setUser(undefined);
          throw new Error('Wrong username or password');
        });
    },
    [createSession, setUser],
  );

  const logout = useCallback(() => {
    setUser(undefined);
  }, [setUser]);

  return React.useMemo(
    () => ({
      login,
      logout,
      user,
      isAuthenticated,
      showLoginPopup,
      setShowLoginPopup,
    }),
    [user, login, logout, isAuthenticated, showLoginPopup, setShowLoginPopup],
  );
}

export const AuthenticationContext = React.createContext<AuthenticationState>(
  {} as AuthenticationState,
);

export function AuthenticatorProvider({
  children,
}: {
  children?: React.ReactNode;
}) {
  const authenticator = useAuthenticatorState();

  const value = React.useMemo(() => {
    if (!authenticator.user) return undefined;
    return {
      ...authenticator.user,
      logout: authenticator.logout,
      isAuthenticated: authenticator.isAuthenticated,
    };
  }, [authenticator]);

  if (!value) return <Loading />;

  return (
    <AuthenticationContext.Provider value={authenticator}>
      <UserContext.Provider value={value}>{children}</UserContext.Provider>
    </AuthenticationContext.Provider>
  );
}

export function useAuthenticator() {
  return React.useContext(AuthenticationContext);
}
