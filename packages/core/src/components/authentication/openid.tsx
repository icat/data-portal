import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { AuthProvider, useAuth, hasAuthParams } from 'react-oidc-context';
import { Spinner } from 'react-bootstrap';
import { OpenIDAuthenticator, useConfig } from 'context';
import { useAuthenticator } from 'components/authentication/authenticator';

export function useOpenIDConfig() {
  const config = useConfig();
  return useMemo(() => {
    return config.api.authentication.authenticators.find(
      (auth) => auth.name === 'OpenID',
    ) as OpenIDAuthenticator | undefined;
  }, [config]);
}

function useOpenIDAuthenticatorOpenidConfig() {
  const openidConfig = useOpenIDConfig();

  return useMemo(() => {
    if (!openidConfig) return undefined;
    return {
      authority: openidConfig.configuration.authority,
      client_id: openidConfig.configuration.clientId,
      redirect_uri: window.location.href,
      automaticSilentRenew: true,
      monitorSession: true,
    };
  }, [openidConfig]);
}

export interface OpenIDState {
  login: () => void;
  logout: () => void;
  switchAffiliation: (suffix: string) => void;
  authenticated: boolean;
  status: 'ready' | 'unavailable';
}

const AppOpenIDContext = createContext<OpenIDState>({
  authenticated: false,
  login: () => undefined,
  logout: () => undefined,
  switchAffiliation: () => undefined,
  status: 'unavailable',
});

export function OpenIDProvider({ children }: { children: React.ReactNode }) {
  const openidConfig = useOpenIDAuthenticatorOpenidConfig();

  if (!openidConfig) return <>{children}</>;

  return (
    <AuthProvider {...openidConfig}>
      <AppOpenIDProvider>{children}</AppOpenIDProvider>
    </AuthProvider>
  );
}

function AppOpenIDProvider({ children }: { children: React.ReactNode }) {
  const clearParams = useClearParams();

  const client = useAuth();

  const openidConfig = useOpenIDConfig();

  const icatAuthenticator = useAuthenticator();
  const [icatAuthenticated, setIcatAuthenticated] = useState(false);
  const [handledOpenIdAuthenticated, setHandledOpenIdAuthenticated] =
    useState(false);

  const [hasTriedSignin, setHasTriedSignin] = useState(false);

  useEffect(() => {
    //this handles auto refresh of id token
    return client.events.addAccessTokenExpiring(() => {
      client.signinSilent();
    });
  }, [client]);

  useEffect(() => {
    //this handles silent autoconnect
    if (
      !hasAuthParams() &&
      !client.isAuthenticated &&
      !client.activeNavigator &&
      !client.isLoading &&
      !hasTriedSignin
    ) {
      setHasTriedSignin(true);
      client.signinSilent({
        redirect_uri: window.location.href,
      });
    }
  }, [client, hasTriedSignin]);

  const handleOpenIdAuthenticated = useCallback(
    (token: string) => {
      clearParams();
      if (handledOpenIdAuthenticated) return;
      if (!openidConfig) return;
      if (icatAuthenticated) return;
      setHandledOpenIdAuthenticated(true);
      icatAuthenticator
        .login(openidConfig, undefined, token, false)
        .then(() => {
          setIcatAuthenticated(true);
        })
        .catch(() => {
          setIcatAuthenticated(false);
        });
    },
    [
      openidConfig,
      handledOpenIdAuthenticated,
      icatAuthenticated,
      icatAuthenticator,
      clearParams,
    ],
  );

  const handleOpenIdLogout = useCallback(() => {
    if (!handledOpenIdAuthenticated) return;
    if (!openidConfig) return;
    if (!icatAuthenticated) return;
    setHandledOpenIdAuthenticated(false);
    icatAuthenticator.logout();
    client.signoutRedirect({
      post_logout_redirect_uri: window.location.href,
    });
  }, [
    openidConfig,
    client,
    handledOpenIdAuthenticated,
    icatAuthenticated,
    icatAuthenticator,
  ]);

  useEffect(() => {
    //this transfers any successful connection of OpenID to icat
    if (client.user?.id_token) {
      handleOpenIdAuthenticated(client.user?.id_token);
    } else {
      setHandledOpenIdAuthenticated(false);
    }
  }, [client.user?.id_token, handleOpenIdAuthenticated]);

  const login = useCallback(() => {
    client.signinRedirect({
      redirect_uri: window.location.href,
    });
  }, [client]);

  useEffect(() => {
    return client.events.addUserSignedOut(() => {
      if (openidConfig && openidConfig.openidLogoutTransferToApp) {
        handleOpenIdLogout();
      }
    });
  }, [openidConfig, client, handleOpenIdLogout]);

  const onSwitchAffiliation = useCallback(
    (suffix: string) => {
      const currentToken = client.user?.id_token;
      if (!currentToken) return;
      if (!openidConfig) return;
      icatAuthenticator.login(
        openidConfig,
        undefined,
        currentToken,
        true,
        suffix,
      );
    },
    [openidConfig, client.user?.id_token, icatAuthenticator],
  );

  const isInitialized = useMemo(() => {
    if (!client) return true;
    if (client.isAuthenticated) return true;
    if (client.isLoading) return false;
    if (hasTriedSignin) return true;
    return false;
  }, [client, hasTriedSignin]);

  const status = useMemo(() => {
    if (!openidConfig?.enabled || client.isLoading) return 'unavailable';
    return 'ready';
  }, [openidConfig, client.isLoading]);

  const value: OpenIDState = useMemo(
    () => ({
      authenticated: icatAuthenticated,
      login,
      logout: handleOpenIdLogout,
      switchAffiliation: onSwitchAffiliation,
      status: status,
    }),
    [icatAuthenticated, login, handleOpenIdLogout, onSwitchAffiliation, status],
  );

  if (isInitialized)
    return (
      <AppOpenIDContext.Provider value={value}>
        {children}
      </AppOpenIDContext.Provider>
    );

  return (
    <i
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '2rem',
      }}
    >
      Initializing SSO...
      <Spinner />
    </i>
  );
}

export function useAppOpenID() {
  return useContext(AppOpenIDContext);
}

const PARAMS = ['state', 'code', 'session_state', 'error'] as const;

function useClearParams() {
  return useCallback(() => {
    if (!hasAuthParams()) return;

    const params = new URLSearchParams(window.location.search);
    if (!PARAMS.some((p) => params.has(p))) return;

    PARAMS.forEach((p) => params.delete(p));

    window.history.replaceState(
      null,
      '',
      `${window.location.pathname}?${params.toString()}`,
    );
  }, []);
}
