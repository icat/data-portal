import type { IcatUser } from '@edata-portal/icat-plus-api';
import { useUserRefresher } from 'components/authentication/refresh';
import { useCallback, useMemo, useState } from 'react';

/**
 * This manages a user state and synchronizes it with local storage.
 * @param key This is the key to use for the local storage, will be prefixed with 'icat.user.'
 */
export function usePersistedUserState(key: string) {
  const storageKey = useMemo(() => 'icat.user.' + key, [key]);

  const getUser = useCallback(() => {
    const value = localStorage.getItem(storageKey);
    if (value) {
      try {
        const user = JSON.parse(value) as IcatUser;
        if (!user.expirationTime) return undefined;
        return user;
      } catch {}
    }

    return undefined;
  }, [storageKey]);

  const [user, setUser] = useState<IcatUser | undefined>(getUser());

  const updateUser = useCallback(
    (newUser?: IcatUser) => {
      setUser(newUser);
      localStorage.setItem(storageKey, JSON.stringify(newUser));
      if (!newUser) {
        localStorage.removeItem(storageKey);
      }
    },
    [setUser, storageKey],
  );

  useUserRefresher(user, () => updateUser(undefined), updateUser);

  return [user, updateUser] as const;
}
