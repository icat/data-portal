import type { IcatUser } from '@edata-portal/icat-plus-api';
import { useUserRefresher } from 'components/authentication/refresh';
import { useState } from 'react';

export function useUserState() {
  const [user, setUser] = useState<IcatUser | undefined>(undefined);
  useUserRefresher(user, () => setUser(undefined), setUser);
  return [user, setUser] as const;
}
