import {
  SESSION_BY_ID_ENDPOINT,
  SESSION_REFRESH_ENDPOINT,
  IcatUser,
  useMutateEndpoint,
  useAsyncFetchEndpoint,
} from '@edata-portal/icat-plus-api';
import { useConfig } from 'context';
import { addToDate, parseDate } from 'helpers';
import { useCallback, useEffect } from 'react';

export function useUserRefresher(
  user: IcatUser | undefined,
  logout: () => void,
  updateRefreshedUser: (user: IcatUser) => void,
) {
  const config = useConfig();
  const refresh = useRefresh({ user, updateRefreshedUser, logout });

  useEffect(() => {
    if (user && config.api.authentication.autoRefresh) {
      // if a user is logged in and auto refresh is enabled
      // periodically check if the session is about to expire and refresh it if necessary
      const threshold =
        config.api.authentication.autoRefreshThresholdMinutes || 10;

      const expiration = parseDate(user.expirationTime);
      if (!expiration || isNaN(expiration.getTime())) {
        console.error('Invalid expiration time', user.expirationTime);
        return;
      }

      const nextRefresh =
        addToDate(expiration, {
          minutes: -threshold,
        }) || expiration;

      const shouldRefresh = () => {
        const now = new Date();
        const diff = nextRefresh.getTime() - now.getTime();
        return diff <= 0;
      };

      if (shouldRefresh()) {
        //if should refresh immediately, do it
        refresh();
      } else {
        //otherwise, set a timeout to check again when `nextRefresh` is reached
        const timeoutValue = nextRefresh.getTime() - new Date().getTime();
        const timeout = setTimeout(() => {
          refresh();
        }, timeoutValue);
        return () => {
          clearTimeout(timeout);
        };
      }
    }
    return undefined;
  }, [user?.expirationTime]); // eslint-disable-line react-hooks/exhaustive-deps
}

function useRefresh({
  user,
  updateRefreshedUser,
  logout,
}: {
  user?: IcatUser;
  updateRefreshedUser: (user: IcatUser) => void;
  logout: () => void;
}) {
  const refreshQuery = useMutateEndpoint({
    endpoint: SESSION_REFRESH_ENDPOINT,
  });
  const getRefreshedUser = useAsyncFetchEndpoint(SESSION_BY_ID_ENDPOINT);

  return useCallback(() => {
    if (!user?.sessionId) return;

    refreshQuery
      .mutateAsync({
        body: {},
        params: { sessionId: user.sessionId },
      })
      .then(() => {
        getRefreshedUser({ sessionId: user.sessionId }).then((session) => {
          if (!session?.remainingMinutes || session.remainingMinutes <= 0) {
            console.error('Failed to refresh session - logging out.', session);
            logout();
            return;
          } else {
            updateRefreshedUser(session.user);
          }
        });
      });
  }, [
    getRefreshedUser,
    logout,
    refreshQuery,
    updateRefreshedUser,
    user?.sessionId,
  ]);
}
