import {
  SESSION_CREATE_ENDPOINT,
  useMutateEndpoint,
  IcatUser,
} from '@edata-portal/icat-plus-api';
import { CancelledError } from '@tanstack/react-query';
import { usePersistedUserState } from 'components/authentication/persist';
import { useConfig } from 'context';
import { useCallback, useEffect } from 'react';

const PERSISTED_ANONYMOUS_KEY = 'anonymous-user';

export function useAnonymous(): IcatUser | undefined {
  const config = useConfig();
  const [anonymousUser, setAnonymousUser] = usePersistedUserState(
    PERSISTED_ANONYMOUS_KEY,
  );

  const createSession = useMutateEndpoint({
    endpoint: SESSION_CREATE_ENDPOINT,
  });

  const login = useCallback(() => {
    createSession
      .mutateAsync({
        body: {
          plugin: config.api.authentication.anonymous.plugin,
          username: config.api.authentication.anonymous.username,
          password: config.api.authentication.anonymous.password,
        },
      })
      .then((newUser) => {
        if (newUser) {
          setAnonymousUser(newUser);
        } else {
          setAnonymousUser(undefined);
          throw new Error('Unexpected error');
        }
        return newUser;
      })
      .catch((e) => {
        if (e instanceof CancelledError) return;
        setAnonymousUser(undefined);
        throw new Error('Wrong username or password');
      });
  }, [
    config.api.authentication.anonymous.password,
    config.api.authentication.anonymous.plugin,
    config.api.authentication.anonymous.username,
    createSession,
    setAnonymousUser,
  ]);

  useEffect(() => {
    if (!anonymousUser) login();
  }, [anonymousUser]); // eslint-disable-line react-hooks/exhaustive-deps

  return anonymousUser;
}
