import {
  Sample,
  Dataset,
  DATASET_LIST_ENDPOINT,
  useGetEndpoint,
  DATASET_TYPE_ACQUISITION,
} from '@edata-portal/icat-plus-api';
import { useViewers } from 'context/viewers';
import { PlaceHolderDatasetCard } from 'components/dataset';
import { Button, LazyWrapper } from 'components/utils';
import { Card, Col, Placeholder, Row } from 'react-bootstrap';
import { SampleActionButton } from 'components/sample/SampleActions';
import { Tree } from 'components/layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronDown,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { useCallback, useMemo, useState } from 'react';
import { formatDateToDay, formatDateToHour, parseDate } from 'helpers';
import { getParameterValue } from 'helpers/entity';
import { DATASET_COUNT } from 'helpers/statistics';
import { PaginationMenu, usePagination } from 'hooks';

export function SampleHeader({
  sample,
  expanded,
  onExpand,
  secondRow,
}: {
  sample: Sample;
  expanded?: boolean;
  onExpand?: (v: boolean) => void;
  secondRow?: React.ReactNode;
}) {
  return (
    <Card
      className="bg-sample d-flex flex-row justify-content-between align-items-center px-2 py-1"
      style={{
        flexWrap: 'wrap',
        cursor: onExpand ? 'pointer' : undefined,
      }}
      onClick={
        onExpand
          ? (e) => {
              e.stopPropagation();
              onExpand(!expanded);
            }
          : undefined
      }
    >
      <Row className="justify-content-between align-items-center g-0 w-100">
        <Col xs={'auto'}>
          <Row className="align-items-center g-2">
            {onExpand ? (
              <Col xs={'auto'}>
                <FontAwesomeIcon
                  icon={expanded ? faChevronDown : faChevronRight}
                />
              </Col>
            ) : null}
            <Col xs={'auto'}>
              <LazyWrapper placeholder={<Placeholder />}>
                <SampleHeaderDate sample={sample} />
              </LazyWrapper>
            </Col>
            <Col xs={'auto'}>
              <strong
                style={{
                  fontSize: '1.3rem',
                }}
              >
                {sample.name}
              </strong>
            </Col>
          </Row>
        </Col>
        <Col xs={'auto'}>
          <Row className="align-items-center g-2 ">
            <Col xs={'auto'}>
              <SampleActionButton
                sampleId={sample.id}
                investigationId={sample.investigation.id}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      {secondRow ? secondRow : null}
    </Card>
  );
}

export function SampleHeaderDate({ sample }: { sample: Sample }) {
  const datasets: Dataset[] = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  const findDate = useCallback(
    (firstOrLast: 'first' | 'last') => {
      return datasets.reduce(
        (acc, dataset) => {
          const datasetDate = parseDate(
            firstOrLast === 'first' ? dataset.startDate : dataset.endDate,
          );
          if (!acc) return datasetDate;
          if (!datasetDate) return acc;
          return firstOrLast === 'first'
            ? datasetDate?.getTime() < acc?.getTime()
              ? datasetDate
              : acc
            : datasetDate?.getTime() > acc?.getTime()
              ? datasetDate
              : acc;
        },
        undefined as Date | undefined,
      );
    },
    [datasets],
  );

  const { firstDate, lastDate } = useMemo(() => {
    const firstDate = findDate('first');
    const lastDate = findDate('last');
    return { firstDate, lastDate };
  }, [findDate]);

  const date = useMemo(() => {
    if (!firstDate) {
      return '';
    }
    const firstDateDay = formatDateToDay(firstDate);
    const lastDateDay = formatDateToDay(lastDate);

    const firstDateTime = formatDateToHour(firstDate);
    const lastDateTime = formatDateToHour(lastDate);

    if (firstDateDay === lastDateDay) {
      if (firstDateTime === lastDateTime) {
        return `${firstDateDay} ${firstDateTime}`;
      } else {
        return `${firstDateDay} ${firstDateTime} to ${lastDateTime}`;
      }
    } else {
      return `${firstDateDay} ${firstDateTime} to ${lastDateDay} ${lastDateTime}`;
    }
  }, [firstDate, lastDate]);

  return <span className="monospace">{date}</span>;
}

/**
 * This component displays a sample header with a pagination of the datasets inside
 * @param sample
 * @param maxOutputs it is the page size of the datasets
 * @returns
 */
export function SampleHeaderWithOutputs({
  sample,
  maxOutputs = 10,
}: {
  sample: Sample;
  maxOutputs?: number;
}) {
  return (
    <LazyWrapper
      placeholder={
        <SampleHeaderWithOutputsPlaceholder
          sample={sample}
          maxOutputs={maxOutputs}
        />
      }
    >
      <SampleHeaderWithOutputsContent
        key={sample.id}
        sample={sample}
        maxOutputs={maxOutputs}
      />
    </LazyWrapper>
  );
}

function SampleHeaderWithOutputsPlaceholder({
  sample,
  maxOutputs,
}: {
  sample: Sample;
  maxOutputs: number;
}) {
  const nbDatasets = Number(getParameterValue(sample, DATASET_COUNT));

  return (
    <Tree
      items={[
        {
          key: sample.id,
          item: <SampleHeader sample={sample} />,
          sticky: true,
          children: !Number.isNaN(nbDatasets)
            ? Array.from(Array(Math.min(nbDatasets, maxOutputs)).keys()).map(
                (i) => ({
                  key: i,
                  item: <PlaceHolderDatasetCard />,
                }),
              )
            : [
                {
                  key: 0,
                  item: <PlaceHolderDatasetCard />,
                },
              ],
        },
      ]}
    />
  );
}

/**
 * This component shows the datasets of a sample as a tree (with lines on the right)
 * @param param0
 * @returns
 */
function SampleHeaderWithOutputsContent({
  sample,
  maxOutputs,
}: {
  sample: Sample;
  maxOutputs: number;
}) {
  const viewers = useViewers();

  const datasets: Dataset[] = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  const pagination = usePagination({
    defaultSize: maxOutputs,
    availableSizes: [10, 20, 50],
    paginationKey: 'sampleDatasets',
    excludeFromPath: true,
  });

  const datasetsToDisplay = useMemo(() => {
    const offset = pagination.page * pagination.size;
    return datasets.slice(offset, offset + pagination.size);
  }, [datasets, pagination.page, pagination.size]);

  const hasPagination = useMemo(
    () => datasets.length !== datasetsToDisplay.length,
    [datasets, datasetsToDisplay],
  );

  const datasetChildren = useMemo(
    () =>
      datasetsToDisplay.map((dataset) => ({
        key: dataset.id,
        item: viewers.viewDataset(dataset, 'details'),
      })),
    [datasetsToDisplay, viewers],
  );

  const paginationItem = useMemo(() => {
    if (!hasPagination) {
      return undefined;
    }
    return (
      <small>
        <PaginationMenu
          pagination={pagination}
          totalPages={Math.ceil(datasets.length / pagination.size)}
          totalElements={datasets.length}
          btnVariant="light"
        />
      </small>
    );
  }, [datasets.length, hasPagination, pagination]);

  const [expanded, setExpanded] = useState(true);

  const sampleHeader = (
    <SampleHeader
      sample={sample}
      secondRow={paginationItem}
      expanded={expanded}
      onExpand={setExpanded}
    />
  );

  const children = expanded
    ? datasetChildren
    : [
        {
          key: 'show-more',
          item: (
            <div
              className="p-2 border rounded"
              onClick={() => setExpanded(true)}
              style={{ cursor: 'pointer' }}
            >
              <Button
                variant="link"
                className="p-0"
                onClick={() => setExpanded(true)}
                size="sm"
              >
                Show {datasetChildren.length} item
                {datasetChildren.length > 1 ? 's' : ''}...
              </Button>
            </div>
          ),
        },
      ];

  return (
    <Tree
      items={[
        {
          key: sample.id,
          item: sampleHeader,
          sticky: true,
          children: children,
        },
      ]}
    />
  );
}
