import { faLink } from '@fortawesome/free-solid-svg-icons';
import { ActionMenu } from 'components/utils/ActionMenu';
import { usePath, useParam } from 'hooks';

function paramIsDefined(param: string | undefined) {
  return param?.length && param !== 'undefined';
}

export function SampleActionButton({
  sampleId,
  investigationId,
}: {
  sampleId: number;
  investigationId: number;
}) {
  const sampleIdPath = usePath('sampleId');
  const [sampleIdSearch] = useParam('sampleId', '');
  const sampleIdFilter = [sampleIdPath, sampleIdSearch].find(paramIsDefined);
  const investigationIdPath = usePath('investigationId');
  const [investigationIdSearch] = useParam('investigationId', '');
  const investigationIdFilter = [
    investigationIdPath,
    investigationIdSearch,
  ].find(paramIsDefined);

  const sampleWithScrollLink = `/investigation/${investigationId}/datasets?scrollToSample=${sampleId}`;
  const sampleLink = `/investigation/${investigationId}/datasets?sampleId=${sampleId}`;

  const isOnInvestigation =
    investigationIdFilter === investigationId.toString();
  const isOnSample = sampleIdFilter === sampleId.toString();

  return (
    <ActionMenu
      items={[
        {
          link: `/compare/datasets?sampleId=${sampleId}`,
          label: 'Open metadata list',
        },
        {
          hide: isOnInvestigation,
          link: `/investigation/${investigationId}/datasets`,
          label: 'Open experiment page',
        },
        {
          hide: isOnSample,
          link: sampleLink,
          label: 'Open sample page',
        },
        {
          hide: isOnInvestigation && !isOnSample,
          link: sampleWithScrollLink,
          label: 'Show in experiment page',
        },
        {
          label: 'Copy sample link',
          icon: faLink,
          onClick: () => {
            const link = `${window.location.origin}${sampleLink}`;
            navigator.clipboard.writeText(link);
          },
        },
      ]}
    />
  );
}
