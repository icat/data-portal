import type { Sample } from '@edata-portal/icat-plus-api';
import { EditableText } from 'components/inputs';
import { useCanEditComments, useCommentParameter } from 'hooks/sampleComments';
import { Card } from 'react-bootstrap';

export function SampleComments({ sample }: { sample: Sample }) {
  const canEdit = useCanEditComments(sample);

  if (canEdit) {
    return <SampleCommentsEditor sample={sample} />;
  }
  return <SampleCommentsReader sample={sample} />;
}

function SampleCommentsEditor({ sample }: { sample: Sample }) {
  const param = useCommentParameter(sample);

  return (
    <small>
      <EditableText
        showLabel={false}
        label={'Comments'}
        value={param.value}
        onSave={param.update}
        type="textarea"
        isSaving={param.saving}
        editable={true}
        size="sm"
      />
    </small>
  );
}

function SampleCommentsReader({ sample }: { sample: Sample }) {
  const param = useCommentParameter(sample);

  return (
    <Card>
      <Card.Body className="p-1">
        <small>
          <pre className="m-0">{param.value}</pre>
        </small>
      </Card.Body>
    </Card>
  );
}
