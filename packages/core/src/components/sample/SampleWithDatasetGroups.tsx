import {
  DATASET_LIST_ENDPOINT,
  DATASET_TYPE_ACQUISITION,
  useGetEndpoint,
  type Dataset,
  type Sample,
} from '@edata-portal/icat-plus-api';
import { PlaceHolderDatasetCard } from 'components/dataset';
import { Tree } from 'components/layout';
import { SampleComments } from 'components/sample/SampleComments';
import { SampleHeader } from 'components/sample/SampleHeader';
import { Button, LazyWrapper } from 'components/utils';
import { first, immutableArray, sortDatasetsByDate } from 'helpers';
import { PaginationMenu, usePagination } from 'hooks';
import { useCanEditComments, useHasComments } from 'hooks/sampleComments';
import { useMemo, useState } from 'react';
import { Card } from 'react-bootstrap';

export type SampleSummaryDefinition = {
  title: string;
  content: (groups: Dataset[][]) => JSX.Element | undefined;
};

export function SampleWithDatasetGroups({
  sample,
  computeGroups,
  summary,
  renderGroup,
  paginationSize,
}: {
  sample: Sample;
  computeGroups: (datasets: Dataset[]) => Dataset[][];
  summary?: SampleSummaryDefinition;
  renderGroup: (group: Dataset[]) => JSX.Element;
  paginationSize: number;
}) {
  return (
    <LazyWrapper placeholder={<LoadingPlaceholder sample={sample} />}>
      <LoadAndDisplay
        sample={sample}
        computeGroups={computeGroups}
        summary={summary}
        renderGroup={renderGroup}
        paginationSize={paginationSize}
      />
    </LazyWrapper>
  );
}

function LoadingPlaceholder({ sample }: { sample: Sample }) {
  return (
    <Tree
      items={[
        {
          key: sample.id,
          item: <SampleHeader sample={sample} />,
          sticky: true,
          children: [
            {
              key: 0,
              item: <PlaceHolderDatasetCard />,
            },
          ],
        },
      ]}
    />
  );
}

function LoadAndDisplay({
  sample,
  computeGroups,
  summary,
  renderGroup,
  paginationSize,
}: {
  sample: Sample;
  computeGroups: (datasets: Dataset[]) => Dataset[][];
  summary?: SampleSummaryDefinition;
  renderGroup: (group: Dataset[]) => JSX.Element;
  paginationSize: number;
}) {
  const datasets = useGetEndpoint({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      investigationIds: sample.investigation.id.toString(),
      nested: true,
      sampleId: sample.id,
      datasetType: DATASET_TYPE_ACQUISITION,
    },
    default: [] as Dataset[],
  });

  const pagination = usePagination({
    defaultSize: paginationSize,
    availableSizes: [10, 20, 50],
    excludeFromPath: true,
    paginationKey: 'sampleGroups',
  });

  const groups = useMemo(
    () => computeGroups(datasets),
    [datasets, computeGroups],
  );

  const groupItems = useMemo(() => {
    const offset = pagination.page * pagination.size;
    return immutableArray(groups)
      .reverse()
      .slice(offset, offset + pagination.size)
      .map((group, i) => {
        const firstDataset = first(sortDatasetsByDate(group));
        return {
          key: firstDataset?.id || 'no-id',
          item: renderGroup(group),
        };
      })
      .toArray();
  }, [groups, pagination.page, pagination.size, renderGroup]);

  const hasPagination = useMemo(
    () => groupItems.length !== groups.length,
    [groupItems, groups],
  );

  const paginationItem = useMemo(() => {
    if (!hasPagination) {
      return undefined;
    }
    return (
      <small>
        <PaginationMenu
          pagination={pagination}
          totalPages={Math.ceil(groups.length / pagination.size)}
          totalElements={groups.length}
          btnVariant="light"
        />
      </small>
    );
  }, [groups.length, hasPagination, pagination]);

  const summaryContent = summary ? summary.content(groups) : undefined;
  const summaryItem =
    summary && summaryContent
      ? {
          key: summary.title,
          item: (
            <SampleSummaryRender
              summary={summaryContent}
              title={summary.title}
            />
          ),
        }
      : undefined;

  const canEditComments = useCanEditComments(sample);
  const hasComments = useHasComments(sample);

  const [expanded, setExpanded] = useState(true);

  const sampleHeader = (
    <SampleHeader
      sample={sample}
      onExpand={setExpanded}
      expanded={expanded}
      secondRow={paginationItem}
    />
  );

  const children = expanded
    ? summaryItem
      ? [summaryItem, ...groupItems]
      : groupItems
    : [
        {
          key: 'show-more',
          item: (
            <div
              className="p-2 border rounded"
              onClick={() => setExpanded(true)}
              style={{ cursor: 'pointer' }}
            >
              <Button
                variant="link"
                className="p-0"
                onClick={() => setExpanded(true)}
                size="sm"
              >
                Show {groupItems.length} item
                {groupItems.length > 1 ? 's' : ''}...
              </Button>
            </div>
          ),
        },
      ];

  return (
    <div>
      <Tree
        items={[
          {
            key: sample.id,
            item: sampleHeader,
            children: children,
            sticky: true,
          },
          ...(canEditComments || hasComments
            ? [
                {
                  key: sample.id + 'comment',
                  item: <SampleComments sample={sample} />,
                },
              ]
            : []),
        ]}
      />
    </div>
  );
}

function SampleSummaryRender({
  summary,
  title,
}: {
  summary: React.ReactNode;
  title: string;
}) {
  return (
    <Card>
      <Card.Header className="bg-light text-info p-1">
        <small>
          <strong>{title}</strong>
        </small>
      </Card.Header>
      <Card.Body className="p-1">{summary}</Card.Body>
    </Card>
  );
}
