import {
  useGetEndpoint,
  type Investigation,
  type Sample,
  SAMPLE_LIST_ENDPOINT,
  useAsyncFetchEndpoint,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';
import ReactSelect from 'react-select/async';

export function SampleSelector({
  investigation,
  selectedSampleId,
  updateSelectedSample,
  updateSelectedSampleObject,
}: {
  investigation: Investigation;
  selectedSampleId?: number;
  updateSelectedSample?: (sampleId?: string) => void;
  updateSelectedSampleObject?: (sample?: Sample) => void;
}) {
  const selectedSampleList = useGetEndpoint({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: {
      investigationId: investigation.id,
      sampleIds: String(selectedSampleId),
    },
    default: [] as Sample[],
    skipFetch: !selectedSampleId,
  });

  const value = useMemo(() => {
    const selectedSample = selectedSampleList?.length
      ? selectedSampleList[0]
      : undefined;
    return selectedSample
      ? { label: selectedSample.name, value: selectedSample }
      : undefined;
  }, [selectedSampleList]);

  const fetchSamples = useAsyncFetchEndpoint(SAMPLE_LIST_ENDPOINT);

  const loadOptions = useMemo(() => {
    return (inputValue: string) => {
      const limit = inputValue?.length ? 100 : 20;
      return fetchSamples({
        investigationId: investigation.id,
        search: inputValue,
        limit: limit,
      }).then((samples) => {
        if (!samples) return [];
        return samples.map((sample) => ({
          value: sample,
          label: sample.name,
        }));
      });
    };
  }, [fetchSamples, investigation.id]);

  return (
    <ReactSelect
      key={value === undefined ? 'undefined' : 'value'}
      value={value}
      loadOptions={loadOptions}
      defaultOptions
      isClearable
      placeholder="Type sample name"
      onChange={(option) => {
        const value = option?.value;
        const sampleId = value?.id;
        const sampleIdString = sampleId ? String(sampleId) : undefined;
        if (updateSelectedSample) updateSelectedSample(sampleIdString);
        if (updateSelectedSampleObject) updateSelectedSampleObject(value);
      }}
    />
  );
}
