import {
  useGetEndpoint,
  INSTRUMENT_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';

export function Instrument({
  instrumentName,
}: {
  instrumentName: string | undefined;
}) {
  const instruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
  });

  const instrument = instruments?.filter(
    (instrument) => instrument.name === instrumentName,
  )[0];

  if (!instrument || !instrument.url) {
    return <>{instrumentName}</>;
  }

  return (
    <a target={'_blank'} href={instrument.url} rel="noreferrer">
      {instrumentName}
    </a>
  );
}
