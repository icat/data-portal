import type { Dataset } from '@edata-portal/icat-plus-api';
import { MetadataCard } from 'components/metadata/MetadataCard';
import { MetadataTableParameter } from 'components/models';
import { getDatasetParamValue } from 'helpers';
import { Col } from 'react-bootstrap';

export function InstrumentPositionersWidget({ dataset }: { dataset: Dataset }) {
  const prefix = 'InstrumentPositioners';
  const presetPositioners: MetadataTableParameter[] = [];
  const positionersValues =
    getDatasetParamValue(dataset, `${prefix}_value`)?.trim().split(' ') || [];
  const positionersNames =
    getDatasetParamValue(dataset, `${prefix}_name`)?.trim().split(' ') || [];
  for (let i = 0; i < positionersNames.length; i++) {
    presetPositioners.push({
      caption: positionersNames[i],
      value: positionersValues[i],
    });
  }

  if (presetPositioners.length === 0) {
    return null;
  }
  return (
    <Col>
      <MetadataCard
        title="Positioners"
        entity={dataset}
        content={presetPositioners}
      />
    </Col>
  );
}
