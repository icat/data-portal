import type { Dataset } from '@edata-portal/icat-plus-api';
import { MetadataCard } from 'components/metadata/MetadataCard';
import { MetadataTableParameter } from 'components/models';
import { getDatasetParamValue } from 'helpers';
import { Col } from 'react-bootstrap';

export function InstrumentOpticsWidget({ dataset }: { dataset: Dataset }) {
  const prefix = 'InstrumentOpticsPositioners';
  const presetOptics: MetadataTableParameter[] = [];
  const opticsValues =
    getDatasetParamValue(dataset, `${prefix}_value`)?.trim().split(' ') || [];
  const opticsNames =
    getDatasetParamValue(dataset, `${prefix}_name`)?.trim().split(' ') || [];
  for (let i = 0; i < opticsNames.length; i++) {
    presetOptics.push({
      caption: opticsNames[i],
      value: opticsValues[i],
    });
  }

  if (presetOptics.length === 0) {
    return null;
  }
  return (
    <Col xs="auto">
      <MetadataCard title="Optics" entity={dataset} content={presetOptics} />
    </Col>
  );
}
