import { Table } from 'react-bootstrap';
import type {
  MetadataTableParameter,
  MetadataTableParameters,
} from 'components/models';
import { convertToFixed } from 'helpers/numeric';
import type { Entity } from '@edata-portal/icat-plus-api';
import { getParameter } from 'helpers/entity';
import MetadataName from 'components/metadata/MetadataName';

function getValue(
  parameter: MetadataTableParameter,
  entity?: Entity,
): {
  value: string | number | undefined;
  unit: string | undefined;
} {
  if (parameter.value) {
    return {
      value: parameter.value,
      unit: parameter.units,
    };
  }
  if (entity) {
    const param = getParameter(
      entity,
      parameter.parameterName ? parameter.parameterName : '',
    );
    if (param) {
      return { value: param.value, unit: param.units };
    }
  }

  return {
    value: undefined,
    unit: undefined,
  };
}

const defaultFormatter: Required<MetadataTableParameter>['formatter'] = (
  value,
  unit,
) => {
  return <>{unit && value && unit !== 'NA' ? `${value} ${unit}` : value}</>;
};

/**
 *
 * @param dataset This components renders a list of dataset parameters as a boostrap Table
 * @param parameters a list with the parameters that will be displayed
 * @returns
 */
export function MetadataTable({
  entity,
  parameters,
  size = 'sm',
  valuePlaceholder,
}: {
  entity?: Entity;
  parameters: MetadataTableParameters;
  size?: string;
  valuePlaceholder?: string;
}) {
  const values = parameters.map((p) => {
    if (p.component)
      return {
        caption: p.caption,
        value: p.component,
        parameterName: p.parameterName,
      };

    const { value, unit } = getValue(p, entity);
    if (value === undefined)
      return {
        caption: p.caption,
        value: valuePlaceholder,
        parameterName: p.parameterName,
      };
    const fixedValue =
      p.digits && !Number.isNaN(Number(value))
        ? convertToFixed(value, p.digits)
        : value;

    const formater = p.formatter ? p.formatter : defaultFormatter;
    return {
      caption: p.caption,
      value: formater(fixedValue, unit),
      parameterName: p.parameterName,
    };
  });

  return (
    <Table size={size} responsive className="m-0">
      <tbody>
        {values.map((v, i) => (
          <tr key={i}>
            <td className="text-start">
              <MetadataName
                caption={v.caption}
                parameterKey={v.parameterName}
              />
            </td>
            <td className="text-start">
              <strong>
                <small>{v.value}</small>
              </strong>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}
