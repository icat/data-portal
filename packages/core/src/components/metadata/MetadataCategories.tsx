import type { Entity } from '@edata-portal/icat-plus-api';
import { MetadataCard } from 'components/metadata/MetadataCard';
import type { MetadataTableParameters } from 'components/models';

export type MetadataCategory = {
  id: string;
  title: string | JSX.Element;
  content: MetadataTableParameters;
};

export function MetadataCategories({
  categories,
  entity,
}: {
  categories: MetadataCategory[];
  entity: Entity;
}) {
  return (
    <div className="d-flex flex-wrap gap-2 justify-content-center">
      {categories.map((category) => (
        <MetadataCard
          key={category.id}
          title={category.title}
          content={category.content}
          entity={entity}
        />
      ))}
    </div>
  );
}
