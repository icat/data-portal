import type { Entity } from '@edata-portal/icat-plus-api';
import { MetadataTable } from 'components/metadata/MetadataTable';
import type { MetadataTableParameters } from 'components/models';
import { Card } from 'react-bootstrap';

export function MetadataCard({
  title,
  content,
  entity,
}: {
  title: string | JSX.Element;
  content: MetadataTableParameters;
  entity: Entity;
}) {
  return (
    <Card
      style={{
        maxWidth: 700,
        flexGrow: 1,
      }}
    >
      <Card.Header className="text-center">{title}</Card.Header>
      <Card.Body style={{ padding: 0 }}>
        <MetadataTable entity={entity} parameters={content} />
      </Card.Body>
    </Card>
  );
}
