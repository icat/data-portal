import type { Dataset } from '@edata-portal/icat-plus-api';
import { MetadataCategories } from 'components/metadata/MetadataCategories';
import { MetadataTableParameter } from 'components/models';
import { getDatasetParamValue } from 'helpers';
import { Col, Card } from 'react-bootstrap';

export function InstrumentDetectorWidget({ dataset }: { dataset: Dataset }) {
  const detectors = [];

  for (let index = 1; index < 10; index++) {
    const prefix = `InstrumentDetector0${index}`;
    const presetDetector: MetadataTableParameter[] = [];
    const positionersValues =
      getDatasetParamValue(dataset, `${prefix}Positioners_value`)
        ?.trim()
        .split(' ') || [];
    const positionersNames =
      getDatasetParamValue(dataset, `${prefix}Positioners_name`)
        ?.trim()
        .split(' ') || [];
    for (let i = 0; i < positionersNames.length; i++) {
      presetDetector.push({
        caption: positionersNames[i],
        value: positionersValues[i],
      });
    }

    if (presetDetector.length > 0) {
      const name = `${prefix}_name`;
      detectors.push({
        title: `#${index} ${getDatasetParamValue(dataset, name)}`,
        id: name,
        content: presetDetector,
      });
    }
  }

  if (detectors.length === 0) {
    return null;
  }
  return (
    <Col>
      <Card>
        <Card.Header className="text-center">Detector</Card.Header>
        <Card.Body>
          <MetadataCategories categories={detectors} entity={dataset} />
        </Card.Body>
      </Card>
    </Col>
  );
}
