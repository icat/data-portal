import type { Dataset } from '@edata-portal/icat-plus-api';
import { InstrumentMonochromatorWidget } from 'components/metadata/InstrumentMonochromatorWidget';
import { InstrumentDetectorWidget } from 'components/metadata/InstrumentDetectorWidget';
import { InstrumentSlitWidget } from 'components/metadata/InstrumentSlitWidget';
import { InstrumentOpticsWidget } from 'components/metadata/InstrumentOpticsWidget';
import { InstrumentPositionersWidget } from 'components/metadata/InstrumentPositionersWidget';
import { Col, Row } from 'react-bootstrap';
import { getInstrumentParameters } from 'helpers';

export function InstrumentWidget({ dataset }: { dataset: Dataset }) {
  const instrumentParams = getInstrumentParameters(dataset);
  return (
    <Row className="g-2 m-2">
      <Col xs={'auto'}>
        <InstrumentMonochromatorWidget
          dataset={dataset}
          parameters={instrumentParams}
        />
      </Col>
      <Col xs={'auto'}>
        <InstrumentDetectorWidget dataset={dataset} />
      </Col>
      <Col xs={'auto'}>
        <InstrumentSlitWidget dataset={dataset} />
      </Col>
      <Col xs={'auto'}>
        <InstrumentOpticsWidget dataset={dataset} />
      </Col>
      <Col xs={'auto'}>
        <InstrumentPositionersWidget dataset={dataset} />
      </Col>
    </Row>
  );
}
