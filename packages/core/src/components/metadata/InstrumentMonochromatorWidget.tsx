import type { Dataset, Parameter } from '@edata-portal/icat-plus-api';
import { MetadataCard } from 'components/metadata/MetadataCard';
import { MetadataTableParameter } from 'components/models';
import { shortenParameterName } from 'helpers';
import { Col } from 'react-bootstrap';

export function InstrumentMonochromatorWidget({
  dataset,
  parameters,
}: {
  dataset: Dataset;
  parameters: Parameter[];
}) {
  const monochromatorParams = parameters.filter((parameter) =>
    parameter.name.startsWith('InstrumentMonochromator'),
  );
  const presetMonochromator: MetadataTableParameter[] = [];
  monochromatorParams.forEach((param) => {
    presetMonochromator.push({
      caption: shortenParameterName(param.name),
      parameterName: param.name,
    });
  });

  if (monochromatorParams.length === 0) {
    return null;
  }
  return (
    <Col xs={12} md="auto">
      <MetadataCard
        title="Monochromator"
        entity={dataset}
        content={presetMonochromator}
      />
    </Col>
  );
}
