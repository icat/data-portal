import type { Dataset } from '@edata-portal/icat-plus-api';
import { MetadataCard } from 'components/metadata/MetadataCard';
import { MetadataTableParameter } from 'components/models';
import { getDatasetParamValue } from 'helpers';
import { Col } from 'react-bootstrap';

export function AttenuatorsWidget({ dataset }: { dataset: Dataset }) {
  const presetAttenuators = (dataset: Dataset): MetadataTableParameter[] => {
    const params: MetadataTableParameter[] = [];
    for (let i = 0; i < 15; i++) {
      const index = (i + 1).toString().padStart(2, '0');
      const type = getDatasetParamValue(
        dataset,
        `InstrumentAttenuator${index}_type`,
      );
      const thickness = getDatasetParamValue(
        dataset,
        `InstrumentAttenuator${index}_thickness`,
      );

      if (type && type.toLowerCase() !== 'empty') {
        params.push({ caption: `Att${i + 1}`, value: `${type}, ${thickness}` });
      }
    }
    return params;
  };

  if (presetAttenuators.length === 0) {
    return null;
  }
  return (
    <Col xs="auto">
      <MetadataCard
        title="Attenuators"
        entity={dataset}
        content={presetAttenuators(dataset)}
      />
    </Col>
  );
}
