import type { Dataset } from '@edata-portal/icat-plus-api';
import { MetadataCard } from 'components/metadata/MetadataCard';
import { MetadataTableParameter } from 'components/models';
import { getDatasetParamValue } from 'helpers';
import { Col, Card, Row } from 'react-bootstrap';

export function InstrumentSlitWidget({ dataset }: { dataset: Dataset }) {
  const addParam = (
    params: MetadataTableParameter[],
    dataset: Dataset,
    key: string,
    caption: string,
    digits?: number,
  ) => {
    const value = getDatasetParamValue(dataset, key) || undefined;
    if (value) {
      params.push({ caption, value, digits });
    }
  };

  const primarySlitsParameters = (
    dataset: Dataset,
  ): MetadataTableParameter[] => {
    const params: MetadataTableParameter[] = [];
    addParam(
      params,
      dataset,
      'InstrumentSlitPrimary_horizontal_gap',
      'Horizontal gap',
      2,
    );
    addParam(
      params,
      dataset,
      'InstrumentSlitPrimary_horizontal_offset',
      'Horizontal offset',
      2,
    );
    addParam(
      params,
      dataset,
      'InstrumentSlitPrimary_vertical_gap',
      'Vertical gap',
      2,
    );
    addParam(
      params,
      dataset,
      'InstrumentSlitPrimary_vertical_offset',
      'Vertical offset',
      2,
    );

    return params;
  };

  const secondarySlitsParameters = (
    dataset: Dataset,
  ): MetadataTableParameter[] => {
    const params: MetadataTableParameter[] = [];
    addParam(
      params,
      dataset,
      'InstrumentSlitSecondary_horizontal_gap',
      'Horizontal gap',
      2,
    );
    addParam(
      params,
      dataset,
      'InstrumentSlitSecondary_horizontal_offset',
      'Horizontal offset',
      2,
    );
    addParam(
      params,
      dataset,
      'InstrumentSlitSecondary_vertical_gap',
      'Vertical gap',
      2,
    );
    addParam(
      params,
      dataset,
      'InstrumentSlitSecondary_vertical_offset',
      'Vertical offset',
      2,
    );

    return params;
  };

  const otherSlits = (
    dataset: Dataset,
  ): { slitsTitles: string[]; otherSlitsData: MetadataTableParameter[][] } => {
    const paramKeys = [
      { key: 'InstrumentSlits_horizontal_gap', caption: 'Horizontal gap' },
      {
        key: 'InstrumentSlits_horizontal_offset',
        caption: 'Horizontal offset',
      },
      { key: 'InstrumentSlits_vertical_gap', caption: 'Vertical gap' },
      { key: 'InstrumentSlits_vertical_offset', caption: 'Vertical offset' },
    ];

    const values = paramKeys.map(({ key }) =>
      getDatasetParamValue(dataset, key),
    );
    if (values.every((val) => val === null || val === undefined)) {
      return { slitsTitles: ['Slits'], otherSlitsData: [] };
    }
    const splitArrays = values.map((val) =>
      typeof val === 'string'
        ? val.split(/\s+/).filter(Boolean) // Split on spaces and remove empty entries
        : val !== null && val !== undefined
          ? [String(val)]
          : [],
    );

    // Ensure all split arrays have the same length
    const slitsNumber = Math.max(...splitArrays.map((arr) => arr.length));
    const allHaveSameLength = splitArrays.every(
      (arr) => arr.length === slitsNumber,
    );

    if (allHaveSameLength) {
      // Slit names if available, otherwise "Slits"
      const slitNames = getDatasetParamValue(dataset, 'InstrumentSlits_name');
      const slitTitles = slitNames
        ? slitNames
            .split(/\s+/)
            .slice(0, slitsNumber)
            .map((name) => name.trim())
        : Array(slitsNumber).fill('Slits');

      // Data into separate rows per slit configuration
      const slitData = Array.from({ length: slitsNumber }, (_, i) =>
        paramKeys.map(({ caption }, j) => ({
          caption,
          value: splitArrays[j][i] || '',
          digits: 2,
        })),
      );

      return {
        slitsTitles: slitTitles,
        otherSlitsData: slitData,
      };
    }

    return {
      slitsTitles: ['Slits'],
      otherSlitsData: [
        paramKeys
          .map(({ caption }, i) =>
            values[i] !== null && values[i] !== undefined
              ? { caption, value: String(values[i]).trim(), digits: 2 }
              : null,
          )
          .filter(Boolean) as MetadataTableParameter[],
      ],
    };
  };

  const { slitsTitles, otherSlitsData } = otherSlits(dataset);

  return primarySlitsParameters(dataset).length > 0 ||
    secondarySlitsParameters(dataset).length > 0 ||
    otherSlitsData.length > 0 ? (
    <Col xs="auto">
      <Card>
        <Card.Header className="text-center">Slits</Card.Header>
        <Card.Body>
          <Row>
            {primarySlitsParameters(dataset).length > 0 && (
              <Col xs="auto">
                <MetadataCard
                  title="Primary Slits"
                  entity={dataset}
                  content={primarySlitsParameters(dataset)}
                />
              </Col>
            )}
            {secondarySlitsParameters(dataset).length > 0 && (
              <Col xs="auto">
                <MetadataCard
                  title="Secondary Slits"
                  entity={dataset}
                  content={secondarySlitsParameters(dataset)}
                />
              </Col>
            )}
            {otherSlitsData.map((params, index) => (
              <Col xs="auto" key={index}>
                <MetadataCard
                  title={slitsTitles[index] || 'Slits'}
                  entity={dataset}
                  content={params}
                />
              </Col>
            ))}
          </Row>
        </Card.Body>
      </Card>
    </Col>
  ) : null;
}
