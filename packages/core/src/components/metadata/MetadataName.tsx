import {
  useAsyncFetchEndpoint,
  PARAMETERS_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useCallback, useState } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

/**
 * Display the metadata name. On mouse over the description is loaded and displayed if exists.
 */
export default function MetadataName({
  caption,
  parameterKey,
}: {
  caption: string | JSX.Element;
  parameterKey?: string;
}) {
  const [description, setDescription] = useState('');
  const get_parameters_endpoint = useAsyncFetchEndpoint(
    PARAMETERS_LIST_ENDPOINT,
  );

  const onHandleOver = useCallback(() => {
    if (!parameterKey || description) return;
    get_parameters_endpoint({ name: parameterKey }).then((loadedParameters) => {
      setDescription(
        (loadedParameters && loadedParameters[0]?.description) || '',
      );
    });
  }, [description, get_parameters_endpoint, parameterKey]);

  return (
    <OverlayTrigger
      trigger={['hover', 'focus']}
      onToggle={onHandleOver}
      overlay={
        description ? (
          <Tooltip id={`tooltip-${parameterKey}`}>{description}</Tooltip>
        ) : (
          <></>
        )
      }
    >
      <span className={'text-muted'}>{caption}</span>
    </OverlayTrigger>
  );
}
