import { useCallback, useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';

export function SearchBar({
  value,
  onUpdate,
  placeholder = 'Search',
  testId,
  size,
}: {
  value?: string;
  onUpdate: (value?: string) => void;
  placeholder?: string;
  testId?: string;
  size?: 'sm' | 'lg';
}) {
  const [search, setSearch] = useState<string | undefined>(value);

  const applySearch = useCallback(
    (search?: string) => {
      const trimmed = search?.trim() || undefined;
      setSearch(trimmed);
      onUpdate(trimmed);
    },
    [onUpdate],
  );

  //apply search after 1000ms without typing
  useEffect(() => {
    const timeout = setTimeout(() => {
      applySearch(search);
    }, 1000);
    return () => {
      clearTimeout(timeout);
    };
  }, [search]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setSearch(value);
  }, [value]);
  return (
    <Form.Control
      type="text"
      placeholder={placeholder}
      value={search || ''}
      onChange={(e) => setSearch(e.target.value)}
      test-id={testId}
      size={size}
    />
  );
}
