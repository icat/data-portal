import type { ColProps } from 'react-bootstrap';

export type FormObject = {
  [key: string]: FormObject | FormValue | FormArray;
};

export type FormValue = string | number | boolean | undefined;
export type FormArray = FormObject[];

export type ConditionParams = {
  rootValue: any;
  fieldValue: any;
  parentValue: any;
};

export type FormObjectDescription<T extends FormObject> = {
  [key in keyof T]?: FormFieldConditionalDescription<T[key]>;
};

export type FormFieldConditionalDescription<
  T extends FormObject[keyof FormObject],
> = FormFieldDescription<T> | ((v: ConditionParams) => FormFieldDescription<T>);

export type FormFieldDescription<T extends FormObject[keyof FormObject]> =
  T extends FormObject
    ? FormObjectDescription<T>
    : T extends FormArray
      ? FormArrayDescription<T>
      : T extends FormValue
        ? FormValueDescription<T>
        : never;

export type FormFieldDescriptionGenericParams<T> = {
  label: string;
  required?: boolean;
  hidden?: boolean;
  readonly?: boolean;
  col?: ColProps;
  validate?: (v: T) => string | undefined | void;
  minLength?: number;
};

export type FormValueDescription<T extends FormValue> =
  FormFieldDescriptionGenericParams<T> &
    (
      | {
          type:
            | 'checkbox'
            | 'radio'
            | 'email'
            | 'password'
            | 'tel'
            | 'color'
            | 'date'
            | 'week'
            | 'month'
            | 'time'
            | 'file'
            | 'hidden'
            | 'number'
            | 'range'
            | 'text'
            | 'url'
            | 'textarea';
        }
      | {
          type: 'select';
          options: {
            value: T;
            label: string | number | React.ReactElement;
            searchValue?: string;
          }[];
        }
    );

export type FormArrayDescription<T extends FormArray> =
  FormFieldDescriptionGenericParams<T> & {
    type: 'array';
    addFromPreset?: {
      value: T[number];
      label: string | number | React.ReactElement;
    }[];
    item: FormFieldDescription<T[number]>;
    newItem?: () => T[number];
  };
