export function MinimalLength({
  minCharacterCount,
}: {
  minCharacterCount: number;
}) {
  if (minCharacterCount === 0) {
    return null;
  }

  return (
    <div className="fst-italic">
      A minimum of {minCharacterCount} characters is required.
    </div>
  );
}
