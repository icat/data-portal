export function RequiredStar() {
  return <span style={{ color: 'red' }}>*</span>;
}
