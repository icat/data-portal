import type {
  FormObjectDescription,
  FormObject,
} from 'components/inputs/forms/FormDescription';
import { RenderFormObject } from 'components/inputs/forms/render/RenderFormObject';
import { Button, Col, Row, Spinner } from 'react-bootstrap';
import { Form } from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import { useState } from 'react';

export function RenderForm<T extends FormObject>({
  description,
  initialValue,
  onSubmit,
  submitLabel = 'Submit',
  onCancel,
  cancelLabel = 'Cancel',
}: {
  description: FormObjectDescription<T>;
  initialValue?: T;
  onSubmit?: (value: T, clearForm: () => void, onError: () => void) => void;
  submitLabel?: string;
  onCancel?: () => void;
  cancelLabel?: string;
}) {
  const [submitting, setSubmitting] = useState(false);

  return (
    <Form<T>
      initialValues={initialValue || ({} as any)}
      onSubmit={(values, form) => {
        setSubmitting(true);
        const clearFormCallback = () => {
          form.restart();
          setSubmitting(false);
        };
        const onErrorCallBack = () => {
          setSubmitting(false);
        };
        onSubmit && onSubmit(values, clearFormCallback, onErrorCallBack);
      }}
      validateOnChange={true}
      mutators={{
        ...arrayMutators,
      }}
    >
      {({ handleSubmit, invalid, form }) => {
        return (
          <Row className="g-2">
            <RenderFormObject description={description} />
            <Col xs={12}>
              <Button
                type="submit"
                disabled={invalid || submitting}
                onClick={() => {
                  handleSubmit();
                }}
              >
                {submitting ? <Spinner size="sm" /> : submitLabel}
              </Button>
              {onCancel && (
                <Button
                  className="ms-2"
                  variant="danger"
                  onClick={() => {
                    form.restart();
                    onCancel();
                  }}
                >
                  {cancelLabel}
                </Button>
              )}
            </Col>
          </Row>
        );
      }}
    </Form>
  );
}
