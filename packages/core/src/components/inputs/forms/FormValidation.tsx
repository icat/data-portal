import type {
  FormArrayDescription,
  FormValueDescription,
} from 'components/inputs/forms/FormDescription';

export function useFormValueValidator(
  description: FormArrayDescription<any> | FormValueDescription<any>,
) {
  return (value: any) => {
    if (description.required) {
      const requiredMessage = `${description.label} is required`;
      const lengthMessage = `${description.minLength} characters are needed`;

      if (value === undefined || value === null || value === '') {
        return requiredMessage;
      }
      if (Array.isArray(value) && value.length === 0) {
        return requiredMessage;
      }
      if (description.minLength && value.length < description.minLength)
        return lengthMessage;
    }

    return description.validate ? description.validate(value) : undefined;
  };
}
