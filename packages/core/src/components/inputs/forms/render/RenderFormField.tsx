import { RequiredStar } from 'components/inputs/forms/RequiredStar';
import { MinimalLength } from 'components/inputs/forms/MinimalLength';

import type {
  FormArray,
  FormFieldConditionalDescription,
  FormObject,
  FormValue,
  FormFieldDescription,
} from 'components/inputs/forms/FormDescription';
import { useResolvedConditionalDescription } from 'components/inputs/forms/render/ConditionalDescriptionRender';
import { RenderFormArray } from 'components/inputs/forms/render/RenderFormArray';
import { RenderFormObject } from 'components/inputs/forms/render/RenderFormObject';
import { RenderFormValue } from 'components/inputs/forms/render/RenderFormValue';
import { Col, Form } from 'react-bootstrap';
import { useField } from 'react-final-form';

export function RenderFormField({
  description,
  path,
}: {
  description: FormFieldConditionalDescription<
    FormObject | FormArray | FormValue
  >;
  path: string;
}) {
  const resolvedDescription = useResolvedConditionalDescription({
    description,
    path,
  });

  if (resolvedDescription.hidden) {
    return null;
  }

  //values and arrays have label and type, so if it is not there it must be a nested object
  if (!('label' in resolvedDescription) || !('type' in resolvedDescription)) {
    return (
      <RenderFormObject description={resolvedDescription as any} path={path} />
    );
  }

  if (resolvedDescription.type === 'array') {
    return (
      <FormFieldDecorator description={resolvedDescription as any} path={path}>
        <RenderFormArray description={resolvedDescription as any} path={path} />
      </FormFieldDecorator>
    );
  }
  return (
    <FormFieldDecorator description={resolvedDescription as any} path={path}>
      <RenderFormValue description={resolvedDescription as any} path={path} />
    </FormFieldDecorator>
  );
}

function FormFieldDecorator({
  children,
  description,
  path,
}: {
  children: React.ReactNode;
  description: FormFieldDescription<FormArray | FormValue>;
  path: string;
}) {
  const colProps = description.col || { xs: 12 };

  const field = useField(path);

  const meta = field.meta;

  const showError = meta.touched && meta.error;

  const errorMessage =
    typeof meta.error === 'string' ? meta.error : 'Invalid value';

  return (
    <Col {...colProps}>
      <Form.Group controlId={path}>
        <Form.Label className="mb-0">
          {description.label}
          {description.required ? <RequiredStar /> : null}
          {description.minLength ? (
            <MinimalLength minCharacterCount={description.minLength} />
          ) : null}
        </Form.Label>
        {children}
        {showError ? (
          <Form.Text className="text-danger">{errorMessage}</Form.Text>
        ) : null}
      </Form.Group>
    </Col>
  );
}
