import type {
  FormFieldConditionalDescription,
  FormValue,
} from 'components/inputs/forms/FormDescription';
import { useFormValueValidator } from 'components/inputs/forms/FormValidation';
import { useResolvedConditionalDescription } from 'components/inputs/forms/render/ConditionalDescriptionRender';
import { hashValue, parseDate, UI_DATE_DAY_FORMAT } from 'helpers';
import { Form } from 'react-bootstrap';
import { Field } from 'react-final-form';
import Select from 'react-select';
import DatePicker from 'react-datepicker';

export function RenderFormValue({
  description,
  path,
}: {
  description: FormFieldConditionalDescription<FormValue>;
  path: string;
}) {
  const resolvedDescription = useResolvedConditionalDescription({
    description,
    path,
  });

  const validator = useFormValueValidator(resolvedDescription);

  return (
    <Field
      key={hashValue(resolvedDescription)} //this is to force re-validation when the description changes
      name={path}
      validate={validator}
      allowNull={resolvedDescription.required !== true}
      render={({ input, meta }) => {
        const isInvalid = !!meta.touched && !!meta.error;

        if (resolvedDescription.type === 'select') {
          return (
            <Select
              key={path}
              isDisabled={resolvedDescription.readonly}
              value={resolvedDescription.options?.find(
                (option) => option.value === input.value,
              )}
              options={resolvedDescription.options}
              onChange={(option) => input.onChange(option?.value)}
              onBlur={input.onBlur}
              onFocus={input.onFocus}
              filterOption={(option, rawInput) => {
                if (!rawInput) return true;
                const data = option.data;

                return rawInput
                  .toLowerCase()
                  .split(' ')
                  .every((word) => {
                    if (
                      'searchValue' in data &&
                      typeof data.searchValue === 'string'
                    ) {
                      return data.searchValue.toLowerCase().includes(word);
                    }
                    if (typeof data.label === 'string') {
                      return data.label.toLowerCase().includes(word);
                    }
                    return String(data.value).toLowerCase().includes(word);
                  });
              }}
            />
          );
        }
        if (resolvedDescription.type === 'date') {
          return (
            <div>
              <DatePicker
                disabled={resolvedDescription.readonly}
                className="form-control"
                dateFormat={UI_DATE_DAY_FORMAT}
                selected={parseDate(input.value)}
                onChange={(value) => input.onChange(value)}
                isClearable
                placeholderText="Date"
              />
            </div>
          );
        }
        return (
          <Form.Control
            key={path}
            as={
              resolvedDescription.type === 'textarea' ? 'textarea' : undefined
            }
            disabled={resolvedDescription.readonly}
            isInvalid={isInvalid}
            minLength={resolvedDescription.minLength}
            {...input}
          />
        );
      }}
    />
  );
}
