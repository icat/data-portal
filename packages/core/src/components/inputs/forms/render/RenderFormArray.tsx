import {
  faArrowDown,
  faArrowUp,
  faPlus,
  faRemove,
  faTrash,
  faEllipsis,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type {
  FormArray,
  FormFieldConditionalDescription,
} from 'components/inputs/forms/FormDescription';
import { useFormValueValidator } from 'components/inputs/forms/FormValidation';
import { useResolvedConditionalDescription } from 'components/inputs/forms/render/ConditionalDescriptionRender';
import { RenderFormObject } from 'components/inputs/forms/render/RenderFormObject';
import { useBreakpointValue } from 'hooks';
import { useState } from 'react';
import { Button, Card, Col, Row, Table } from 'react-bootstrap';
import { FieldArray } from 'react-final-form-arrays';
import ReactSelect from 'react-select';

export function RenderFormArray({
  description,
  path,
}: {
  description: FormFieldConditionalDescription<FormArray>;
  path: string;
}) {
  const resolvedDescription = useResolvedConditionalDescription({
    description,
    path,
  });

  const validator = useFormValueValidator(resolvedDescription);

  const [dragging, setDragging] = useState<number | undefined>(undefined);
  const [draggingOver, setDraggingOver] = useState<number | undefined>(
    undefined,
  );

  const enableDragNDrop = useBreakpointValue({ xs: false, lg: true });

  const labelSingular = resolvedDescription.label
    .toLowerCase()
    .replace(/s$/, '');
  const labelPlural = labelSingular + 's';

  return (
    <FieldArray name={path} validate={validator}>
      {({ fields, meta }) => {
        const error = meta.touched && meta.error;

        const reorderedIndexesWithDragState = reorderWithDragging(
          fields.map((_, i) => i),
          dragging,
          draggingOver,
        );

        return (
          <Card className={error ? 'border-danger' : ''}>
            <Card.Body className="p-2">
              <Row className="g-2">
                {resolvedDescription.addFromPreset?.length ? (
                  <Col xs={'auto'}>
                    <ReactSelect
                      menuPortalTarget={document.body}
                      placeholder={`Select an ${labelSingular} to add...`}
                      isClearable={false}
                      value={null}
                      options={resolvedDescription.addFromPreset}
                      onChange={(v) => {
                        if (v?.value) {
                          fields.push(v.value);
                        }
                      }}
                    />
                  </Col>
                ) : null}
                {fields?.length ? (
                  <small>
                    <Table
                      size="sm"
                      responsive
                      hover
                      borderless
                      className="mt-2"
                    >
                      <tbody>
                        {reorderedIndexesWithDragState.map(
                          (originalIndex, currentIndex) => {
                            const newPath = `${path}.${originalIndex}`;
                            return (
                              <tr
                                key={originalIndex}
                                onDragOver={(e) => {
                                  setDraggingOver(currentIndex);
                                }}
                              >
                                {enableDragNDrop ? (
                                  <td
                                    style={{
                                      width: 1,
                                      verticalAlign: 'middle',
                                      cursor: 'move',
                                      fontSize: '1.5em',
                                    }}
                                    draggable={enableDragNDrop}
                                    //drag for desktops
                                    onDragStart={(e) => {
                                      setDragging(originalIndex);
                                      //no image for drag
                                      e.dataTransfer.setDragImage(
                                        document.createElement('div'),
                                        0,
                                        0,
                                      );
                                    }}
                                    onDragEnd={(e) => {
                                      if (dragging !== undefined) {
                                        fields.move(dragging, currentIndex);
                                      }
                                      setDragging(undefined);
                                      setDraggingOver(undefined);
                                    }}
                                  >
                                    <FontAwesomeIcon
                                      icon={faEllipsis}
                                      className="me-1 mt-3  "
                                    />
                                  </td>
                                ) : (
                                  <>
                                    <td
                                      style={{
                                        width: 1,
                                        verticalAlign: 'top',
                                      }}
                                    >
                                      {originalIndex === 0 ? null : (
                                        <Button
                                          className="p-0 text-info"
                                          variant="link"
                                          onClick={() => {
                                            fields.move(
                                              originalIndex,
                                              originalIndex - 1,
                                            );
                                          }}
                                          disabled={
                                            resolvedDescription.readonly
                                          }
                                          style={{
                                            fontSize: '1.5em',
                                          }}
                                        >
                                          <FontAwesomeIcon icon={faArrowUp} />
                                        </Button>
                                      )}
                                    </td>
                                    <td
                                      style={{
                                        width: 1,
                                        verticalAlign: 'top',
                                      }}
                                    >
                                      {fields.length &&
                                      originalIndex ===
                                        fields.length - 1 ? null : (
                                        <Button
                                          className="p-0 text-info"
                                          variant="link"
                                          onClick={() => {
                                            fields.move(
                                              originalIndex,
                                              originalIndex + 1,
                                            );
                                          }}
                                          disabled={
                                            resolvedDescription.readonly
                                          }
                                          style={{
                                            fontSize: '1.5em',
                                          }}
                                        >
                                          <FontAwesomeIcon icon={faArrowDown} />
                                        </Button>
                                      )}
                                    </td>
                                  </>
                                )}
                                <td>
                                  <Row className="g-2">
                                    <RenderFormObject
                                      description={resolvedDescription.item}
                                      path={newPath}
                                    />
                                  </Row>
                                </td>
                                <td
                                  style={{
                                    width: 1,
                                    verticalAlign: enableDragNDrop
                                      ? 'middle'
                                      : 'top',
                                  }}
                                >
                                  <Button
                                    className="p-0 text-danger"
                                    variant="link"
                                    onClick={() => {
                                      fields.remove(originalIndex);
                                    }}
                                    disabled={resolvedDescription.readonly}
                                    style={{
                                      fontSize: '1.5em',
                                    }}
                                  >
                                    <FontAwesomeIcon icon={faRemove} />
                                  </Button>
                                </td>
                              </tr>
                            );
                          },
                        )}
                      </tbody>
                    </Table>
                  </small>
                ) : null}
                {resolvedDescription.newItem ? (
                  <Col xs={'auto'}>
                    <Button
                      disabled={resolvedDescription.readonly}
                      variant="primary"
                      onClick={() => {
                        if (resolvedDescription.newItem)
                          fields.push(resolvedDescription.newItem());
                      }}
                    >
                      <FontAwesomeIcon icon={faPlus} className="me-1" />
                      New {labelSingular}
                    </Button>
                  </Col>
                ) : null}
                <Col xs={'auto'}>
                  <Button
                    disabled={resolvedDescription.readonly || !fields.length}
                    variant="danger"
                    onClick={() => {
                      const indexes = fields.map((_, i) => i);
                      indexes.reverse().forEach((i) => {
                        fields.remove(i);
                      });
                    }}
                  >
                    <FontAwesomeIcon icon={faTrash} className="me-1" />
                    Clear all {labelPlural}
                  </Button>
                </Col>
              </Row>
            </Card.Body>
          </Card>
        );
      }}
    </FieldArray>
  );
}

function reorderWithDragging<T>(
  list: T[],
  moveIndex?: number,
  toIndex?: number,
) {
  if (moveIndex === undefined || toIndex === undefined) {
    return list;
  }
  if (moveIndex === toIndex) {
    return list;
  }
  const copy = [...list];
  const [removed] = copy.splice(moveIndex, 1);
  copy.splice(toIndex, 0, removed);
  return copy;
}
