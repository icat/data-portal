import type {
  FormArray,
  FormFieldConditionalDescription,
  FormFieldDescription,
  FormObject,
  FormValue,
} from 'components/inputs/forms/FormDescription';
import get from 'lodash/get';
import { useMemo } from 'react';
import { useForm } from 'react-final-form';

export function useResolvedConditionalDescription<
  T extends FormObject | FormArray | FormValue,
>({
  description,
  path,
}: {
  description: FormFieldConditionalDescription<T>;
  path?: string;
}): FormFieldDescription<T> {
  const form = useForm(path);

  const state = form.getState();

  return useMemo(() => {
    if (typeof description === 'function') {
      const rootValue = state.values as any;
      const fieldValue = path ? get(rootValue, path) : rootValue;
      const parentPath = path
        ? path.split('.').slice(0, -1).join('.')
        : undefined;
      const parentValue = parentPath ? get(rootValue, parentPath) : rootValue;

      return description({
        fieldValue,
        parentValue,
        rootValue,
      });
    }
    return description;
  }, [description, state.values, path]);
}
