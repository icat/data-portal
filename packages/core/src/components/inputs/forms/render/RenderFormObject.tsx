import type {
  FormObject,
  FormFieldConditionalDescription,
} from 'components/inputs/forms/FormDescription';
import { useResolvedConditionalDescription } from 'components/inputs/forms/render/ConditionalDescriptionRender';
import { RenderFormField } from 'components/inputs/forms/render/RenderFormField';

export function RenderFormObject({
  description,
  path,
}: {
  description: FormFieldConditionalDescription<FormObject>;
  path?: string;
}) {
  const resolvedDescription = useResolvedConditionalDescription({
    description,
    path,
  });

  return (
    <>
      {Object.keys(resolvedDescription).map((key) => {
        const newPath = path ? `${path}.${key}` : key;
        const fieldDescription = resolvedDescription[key];
        if (!fieldDescription) return null;
        return (
          <RenderFormField
            key={newPath}
            description={fieldDescription}
            path={newPath}
          />
        );
      })}
    </>
  );
}
