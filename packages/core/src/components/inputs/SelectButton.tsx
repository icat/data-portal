import { type SelectEntityType, useItemSelection } from 'hooks';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

export function SelectButton({
  type,
  ids,
  light = false,
  onClick = () => {},
  withText = false,
}: {
  type: SelectEntityType;
  ids: number[];
  light?: boolean;
  onClick?: () => void;
  withText?: boolean;
}) {
  const selection = useItemSelection(type, ids);

  const message = selection.isSelected
    ? 'Remove from selection'
    : 'Add to selection';

  return (
    <OverlayTrigger
      placement="auto"
      overlay={<Tooltip key={'MessageContent'}>{message}</Tooltip>}
    >
      <Form.Check
        type="checkbox"
        onChange={(e) => {
          e.stopPropagation();
          selection.toggleSelected();
          if (onClick) {
            onClick();
          }
        }}
        checked={selection.isSelected}
        key={'SelectButton'}
      >
        {withText && message}
      </Form.Check>
    </OverlayTrigger>
  );
}
