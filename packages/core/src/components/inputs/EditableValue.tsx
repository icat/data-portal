import React, { KeyboardEventHandler, useCallback, useEffect } from 'react';
import {
  Button,
  Container,
  Form,
  InputGroup,
  Row,
  Spinner,
} from 'react-bootstrap';

export function EditableText({
  label,
  value,
  onSave,
  type = 'input',
  isSaving = false,
  required = false,
  disabled = false,
  editable = true,
  showLabel = true,
  size,
  placeholder,
  saveOnBlur = false,
}: {
  showLabel?: boolean;
  label: string;
  value: string;
  onSave: (value: string) => void;
  type?: 'input' | 'textarea';
  isSaving?: boolean;
  required?: boolean;
  disabled?: boolean;
  editable?: boolean;
  size?: 'sm' | 'lg';
  placeholder?: string;
  saveOnBlur?: boolean;
}) {
  const [currentValue, setCurrentValue] = React.useState(value);

  useEffect(() => {
    if (isSaving) return;
    setCurrentValue(value);
  }, [value, isSaving]);

  const touched = React.useMemo(
    () => currentValue !== value,
    [value, currentValue],
  );

  const canBeSaved = React.useMemo(() => {
    if (isSaving) {
      return false;
    }
    if (required) {
      return touched && currentValue.length > 0;
    }
    return touched;
  }, [touched, currentValue, required, isSaving]);

  const textAreaRef = React.useRef<HTMLTextAreaElement>(null);

  //autogrow textarea
  React.useEffect(() => {
    if (type === 'textarea' && textAreaRef.current) {
      textAreaRef.current.style.height = '0px';
      textAreaRef.current.style.height = `${
        textAreaRef.current.scrollHeight + 5
      }px`;
    }
  }, [currentValue, type]);

  const placeholderText = React.useMemo(() => {
    if (placeholder) {
      return placeholder;
    }
    return `Enter ${label.toLowerCase()} here...`;
  }, [label, placeholder]);

  const onBlur = React.useCallback(() => {
    if (saveOnBlur) {
      onSave(currentValue);
    }
  }, [saveOnBlur, onSave, currentValue]);

  const onKeyDown = useCallback<KeyboardEventHandler<HTMLInputElement>>(
    (e) => {
      if (e.key === 'Enter') {
        onSave(currentValue);
        e.preventDefault();
        e.stopPropagation();
      }
    },
    [currentValue, onSave],
  );

  const controlProps = React.useMemo(() => {
    return {
      disabled,
      as: type,
      placeholder: placeholderText,
      value: currentValue,
      onChange: (e: any) => {
        setCurrentValue(e.target.value);
      },
      size,
      onBlur,
      onKeyDown,
    } as const;
  }, [disabled, type, placeholderText, currentValue, size, onBlur, onKeyDown]);

  return (
    <Container fluid className="p-0">
      {showLabel ? (
        <Row>
          <strong>{label}</strong>
        </Row>
      ) : null}
      {editable ? (
        <InputGroup>
          <Form.Control
            {...(type === 'textarea'
              ? {
                  ref: textAreaRef,
                  style: { resize: 'none' },
                }
              : {})}
            {...controlProps}
          />
          {touched && (
            <Button
              variant="primary"
              disabled={!canBeSaved}
              onClick={async () => {
                onSave(currentValue);
              }}
            >
              {isSaving ? <Spinner size="sm" /> : 'Save'}
            </Button>
          )}
        </InputGroup>
      ) : (
        <Row>
          <span
            style={{
              marginBottom: '0.5rem',
            }}
          >
            <i>{value ? value : `no ${label.toLowerCase()} `}</i>
          </span>
        </Row>
      )}
    </Container>
  );
}
