import {
  Dataset,
  DATASET_FILE_LIST_ENDPOINT,
  useGetEndpoint,
  DatafileResponse,
  Datafile,
} from '@edata-portal/icat-plus-api';
import { DataFilesViewer } from 'components/datafile/viewer/files/DataFilesViewer';
import { DataFilesViewerListSelector } from 'components/datafile/viewer/files/selectors/DataFilesViewerListSelector';
import { DataFilesViewerTreeSelector } from 'components/datafile/viewer/files/selectors/DataFilesViewerTreeSelector';
import { DatasetDownloadButton } from 'components/dataset';
import { SearchBar } from 'components/inputs';
import { CopyValue, LazyWrapper, Loading, NoData } from 'components/utils';
import { useConfig } from 'context';
import { first } from 'helpers';
import { PaginationMenu, useEndpointPagination } from 'hooks';
import { useMemo } from 'react';
import { Suspense } from 'react';
import { useState } from 'react';
import { Spinner } from 'react-bootstrap';

export function DatasetFiles({
  dataset,
  showPath,
  showDatasetDownloadButton,
}: {
  dataset: Dataset;
  showPath: boolean;
  showDatasetDownloadButton?: boolean;
}) {
  return (
    <div className="d-flex flex-column gap-2 w-100 h-100 overflow-hidden">
      <div className="d-flex flex-row flex-wrap align-items-center justify-content-between">
        {showPath && (
          <div>
            <small>
              <CopyValue type="value" value={dataset.location} />
            </small>
          </div>
        )}
        {showDatasetDownloadButton && (
          <small>
            <LazyWrapper placeholder={<Spinner size="sm" />}>
              <DatasetDownloadButton dataset={dataset} />
            </LazyWrapper>
          </small>
        )}
      </div>
      <Suspense fallback={<Loading />}>
        <LoadDisplayDatasetFiles dataset={dataset} />
      </Suspense>
    </div>
  );
}

function LoadDisplayDatasetFiles({ dataset }: { dataset: Dataset }) {
  const filesLimit1 = useGetEndpoint({
    endpoint: DATASET_FILE_LIST_ENDPOINT,
    params: {
      datasetId: dataset.id.toString(),
      limit: 1,
    },
    default: [] as DatafileResponse[],
  });

  const nbTotal = first(filesLimit1)?.meta?.page?.total ?? 0;

  const [currentFile, setCurrentFile] = useState<Datafile | undefined>(
    undefined,
  );

  if (nbTotal === 0) return <NoData />;

  return (
    <DataFilesViewer
      selectorComponent={
        <Selector
          dataset={dataset}
          nbTotal={nbTotal}
          currentFile={currentFile}
          setCurrentFile={setCurrentFile}
        />
      }
      currentFile={currentFile}
    />
  );
}

function Selector({
  dataset,
  nbTotal,
  currentFile,
  setCurrentFile,
}: {
  dataset: Dataset;
  nbTotal: number;
  currentFile: Datafile | undefined;
  setCurrentFile: (file: Datafile) => void;
}) {
  const [search, setSearch] = useState<string>('');

  const searchCleaned = useMemo(() => search?.trim(), [search]);

  const config = useConfig();

  return (
    <div className="d-flex flex-column h-100 overflow-hidden gap-1">
      <SearchBar
        placeholder="Search for a file"
        value={search}
        onUpdate={(value) => setSearch(value || '')}
        size="sm"
      />
      <Suspense fallback={<i>Loading...</i>}>
        {nbTotal > config.ui.fileBrowser.maxFileNb ? (
          <LoadDataPaginatedList
            dataset={dataset}
            search={searchCleaned}
            currentFile={currentFile}
            setCurrentFile={setCurrentFile}
          />
        ) : (
          <LoadDataTree
            dataset={dataset}
            search={searchCleaned}
            currentFile={currentFile}
            setCurrentFile={setCurrentFile}
          />
        )}
      </Suspense>
    </div>
  );
}

function LoadDataTree({
  dataset,
  search,
  currentFile,
  setCurrentFile,
}: {
  dataset: Dataset;
  search: string;
  currentFile: Datafile | undefined;
  setCurrentFile: (file: Datafile) => void;
}) {
  const config = useConfig();

  const files = useGetEndpoint({
    endpoint: DATASET_FILE_LIST_ENDPOINT,
    params: {
      datasetId: dataset.id.toString(),
      ...(search?.length ? { search } : {}),
      limit: config.ui.fileBrowser.maxFileNb,
    },
    default: [] as DatafileResponse[],
  });

  return (
    <DataFilesViewerTreeSelector
      onSelected={setCurrentFile}
      selected={currentFile}
      files={files.map((f) => f.Datafile)}
    />
  );
}

function LoadDataPaginatedList({
  dataset,
  search,
  currentFile,
  setCurrentFile,
}: {
  dataset: Dataset;
  search: string;
  currentFile: Datafile | undefined;
  setCurrentFile: (file: Datafile) => void;
}) {
  const config = useConfig();

  const files = useEndpointPagination({
    endpoint: DATASET_FILE_LIST_ENDPOINT,
    params: {
      datasetId: dataset.id.toString(),
      ...(search?.length ? { search } : {}),
    },
    paginationParams: {
      defaultSize: 20,
      excludeFromPath: true,
      paginationKey: 'datasetFiles',
    },
  });

  return (
    <>
      <i>
        <small>
          File tree could not be shown because this dataset has more than{' '}
          {config.ui.fileBrowser.maxFileNb} files.
        </small>
      </i>
      <DataFilesViewerListSelector
        files={files.data.map((f) => f.Datafile)}
        onSelected={setCurrentFile}
        selected={currentFile}
      />

      <PaginationMenu {...files} margin="0px" />
    </>
  );
}
