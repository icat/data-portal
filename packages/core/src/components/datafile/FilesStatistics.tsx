import {
  FILE_COUNT,
  ACQUISITION_FILE_COUNT,
  PROCESSED_FILE_COUNT,
} from 'helpers/statistics';
import { OverlayTrigger, Popover, Row, Col } from 'react-bootstrap';

export function FilesStatistics({
  parameters,
  showLabel,
}: {
  parameters: Record<string, string>;
  showLabel?: boolean;
}) {
  const nbFiles = Number(parameters[FILE_COUNT] || 0) || 0;
  const nbFilesAcquisition =
    Number(parameters[ACQUISITION_FILE_COUNT] || 0) || 0;
  const nbFilesProcessed = Number(parameters[PROCESSED_FILE_COUNT] || 0) || 0;

  if (!nbFilesAcquisition && !nbFilesProcessed)
    return nbFiles ? (
      <div>
        {showLabel ? <strong>Files: </strong> : null}
        <span className="filecount">{nbFiles.toLocaleString()}</span>
      </div>
    ) : null;

  const overlay = (
    <Popover>
      <Popover.Header>Files</Popover.Header>
      <Popover.Body>
        <span>
          <strong>Total: </strong>
          {nbFiles.toLocaleString()}
        </span>
        <br />
        <span>
          <strong>Acquisition: </strong>
          {nbFilesAcquisition.toLocaleString()}
        </span>
        <br />
        <span>
          <strong>Processed: </strong>
          {nbFilesProcessed.toLocaleString()}
        </span>
        <br />
      </Popover.Body>
    </Popover>
  );

  return (
    <Row>
      {showLabel ? <Col>Files: </Col> : null}
      <Col>
        <OverlayTrigger placement="auto" overlay={overlay}>
          <span className="text-info tablenumber">
            {nbFiles.toLocaleString()}
          </span>
        </OverlayTrigger>
      </Col>
    </Row>
  );
}
