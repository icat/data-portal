import {
  DATASET_STATUS_ARCHIVED,
  DATASET_STATUS_ENDPOINT,
  DATASET_STATUS_ONLINE,
  DATASET_STATUS_RESTORING,
  useGetEndpoint,
  type Datafile,
} from '@edata-portal/icat-plus-api';
import {
  faWarning,
  faSpinner,
  faCheck,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useMemo } from 'react';
import { OverlayTrigger, Tooltip, Popover } from 'react-bootstrap';

export function DataFileStatus({
  datafile,
  showOnline,
}: {
  datafile: Datafile;
  showOnline?: boolean;
}) {
  const statusList = useGetEndpoint({
    endpoint: DATASET_STATUS_ENDPOINT,
    params: {
      datasetIds: datafile.dataset.id.toString(),
    },
    notifyOnError: false,
  });

  const status = useMemo(() => {
    if (!statusList?.length) {
      return null;
    }
    return statusList[0];
  }, [statusList]);

  const { icon, color, message } = useMemo(() => {
    if (status === DATASET_STATUS_ARCHIVED) {
      return {
        icon: faWarning,
        color: 'text-warning',
        message: 'Archived',
      };
    }
    if (status === DATASET_STATUS_RESTORING) {
      return {
        icon: faSpinner,
        color: 'text-info',
        message: 'Restoring',
      };
    }
    if (status === DATASET_STATUS_ONLINE)
      return {
        icon: showOnline ? faCheck : null,
        color: 'text-success',
        message: 'Online',
      };
    return {
      icon: faWarning,
      color: 'text-danger',
      message: 'Unknown status',
    };
  }, [showOnline, status]);

  if (icon === null || color === null || message === null) {
    return null;
  }

  return (
    <OverlayTrigger
      overlay={
        <Tooltip id="popover-basic">
          <Popover.Body>{message}</Popover.Body>
        </Tooltip>
      }
    >
      <div className={`d-flex flex-row gap-1 ${color}`}>
        <FontAwesomeIcon icon={icon} />
      </div>
    </OverlayTrigger>
  );
}
