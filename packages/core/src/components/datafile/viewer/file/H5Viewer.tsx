import { faShareAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type { Datafile } from '@edata-portal/icat-plus-api';
import { useConfig, useUser } from 'context';
import { Button } from 'react-bootstrap';
import { useTracking } from 'hooks';

export function H5Viewer({ datafile }: { datafile: Datafile }) {
  const config = useConfig();
  const user = useUser();

  const url = `${config.ui.h5Viewer.url}?sessionId=${user?.sessionId}&datafileId=${datafile.id}&name=${datafile.name}`;

  useTracking('H5Viewer', 'View', datafile.location);

  return (
    <div className="d-flex flex-column align-items-center gap-1 h-100 w-100 justify-content-center">
      <iframe
        title="HDF5 Viewer"
        className="h-100 w-100"
        src={`${url}&wide`}
        allowFullScreen
      />
      <Button target="_blank" href={url}>
        <FontAwesomeIcon icon={faShareAlt} style={{ marginRight: 5 }} />
        Open viewer
      </Button>
    </div>
  );
}
