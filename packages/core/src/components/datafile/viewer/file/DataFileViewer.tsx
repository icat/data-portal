import {
  DATASET_STATUS_ENDPOINT,
  DATASET_STATUS_ONLINE,
  useGetEndpoint,
  type Datafile,
} from '@edata-portal/icat-plus-api';
import { DataFileStatus } from 'components/datafile/viewer/file/DataFileStatus';
import { DownloadFileButton } from 'components/datafile/viewer/file/DownloadFileButton';
import { H5Viewer } from 'components/datafile/viewer/file/H5Viewer';
import { ImageViewer } from 'components/datafile/viewer/file/ImageViewer';
import {
  TextViewer,
  textFileIsAboveMaxSize,
} from 'components/datafile/viewer/file/TextViewer';
import { Button, EllipsisName, Loading } from 'components/utils';

import { useConfig } from 'context';
import {
  isH5Viewer,
  isImageViewer,
  isTextViewer,
  stringifyWithUnitPrefix,
} from 'helpers';
import { Suspense, useEffect, useMemo, useState } from 'react';

export function DataFileViewer({ datafile }: { datafile: Datafile }) {
  const config = useConfig();

  const [forceAsText, setForceAsText] = useState(false);

  useEffect(() => {
    setForceAsText(false);
  }, [datafile]);

  return isH5Viewer(datafile, config) ? (
    <H5Viewer datafile={datafile} />
  ) : isImageViewer(datafile, config) ? (
    <ImageViewer datafile={datafile} />
  ) : isTextViewer(datafile, config) || forceAsText ? (
    <TextViewer datafile={datafile} />
  ) : (
    <div className="d-flex flex-column align-items-center gap-2 h-100 justify-content-center">
      <strong className="d-flex align-items-center">
        Visualization is not available for this type of file
        <DownloadFileButton file={datafile} />
      </strong>
      {!textFileIsAboveMaxSize(datafile, config) && (
        <Button onClick={() => setForceAsText((prev) => !prev)}>
          Show as text
        </Button>
      )}
    </div>
  );
}

export function DataFileViewerWithInfo({ datafile }: { datafile: Datafile }) {
  const statusList = useGetEndpoint({
    endpoint: DATASET_STATUS_ENDPOINT,
    params: {
      datasetIds: datafile.dataset.id.toString(),
    },
    notifyOnError: false,
  });

  const status = useMemo(() => {
    if (!statusList?.length) {
      return null;
    }
    return statusList[0];
  }, [statusList]);

  const isOnline = useMemo(() => status === DATASET_STATUS_ONLINE, [status]);

  return (
    <div className="d-flex flex-column h-100 w-100 overflow-hidden">
      <div className="bg-light border-bottom  d-flex flex-row align-items-center gap-2 px-2">
        <EllipsisName
          displayName={datafile.name}
          fullName={datafile.location}
          side="start"
          className="monospace"
        />
        <span className="text-nowrap">
          {stringifyWithUnitPrefix(datafile.fileSize, 2, 'B')}
        </span>
        <DataFileStatus datafile={datafile} showOnline />
        {isOnline && <DownloadFileButton file={datafile} />}
      </div>
      <div className="h-100 w-100 overflow-hidden">
        <Suspense fallback={<Loading />}>
          {isOnline ? (
            <DataFileViewer datafile={datafile} />
          ) : (
            <div>File is not online</div>
          )}
        </Suspense>
      </div>
    </div>
  );
}
