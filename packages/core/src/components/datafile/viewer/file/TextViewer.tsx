import {
  type Datafile,
  DATA_DOWNLOAD_ENDPOINT,
  useEndpointURL,
} from '@edata-portal/icat-plus-api';
import { DownloadFileButton } from 'components/datafile/viewer/file/DownloadFileButton';
import { Loading, NoData } from 'components/utils';
import { Config, useConfig } from 'context';
import { stringifyWithUnitPrefix } from 'helpers';
import { useEffect, useState } from 'react';

export function TextViewer({ datafile }: { datafile: Datafile }) {
  const config = useConfig();

  if (textFileIsAboveMaxSize(datafile, config)) {
    return (
      <strong className="d-flex h-100 w-100 justify-content-center align-items-center">
        File is too large to display (max{' '}
        {stringifyWithUnitPrefix(config.ui.textViewer.maxFileSize, 0, 'B')})
        <DownloadFileButton file={datafile} />
      </strong>
    );
  }

  return <LoadAndDisplay datafile={datafile} />;
}

export function textFileIsAboveMaxSize(datafile: Datafile, config: Config) {
  return datafile.fileSize > config.ui.textViewer.maxFileSize;
}

function LoadAndDisplay({ datafile }: { datafile: Datafile }) {
  const getUrl = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  const url = getUrl({
    datafileIds: datafile.id.toString(),
  });

  const [fetching, setFetching] = useState(true);
  const [data, setData] = useState<string | null>(null);

  useEffect(() => {
    fetch(url)
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setData(data);
        setFetching(false);
      })
      .catch((error) => {
        console.error('Error:', error);
        setData(null);
        setFetching(false);
      });
  }, [url]);

  if (fetching) {
    return <Loading />;
  }

  if (!data) {
    return <NoData />;
  }

  return (
    <div className="h-100 overflow-auto bg-white p-2">
      <pre
        style={{
          overflow: 'visible',
        }}
        className="monospace"
      >
        {data}
      </pre>
    </div>
  );
}
