import {
  DATA_DOWNLOAD_ENDPOINT,
  Datafile,
  useEndpointURL,
} from '@edata-portal/icat-plus-api';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'components/utils';
import { useTrackingCallback } from 'hooks';

export function DownloadFileButton({ file }: { file: Datafile }) {
  const downloadUrl = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  const url = downloadUrl({
    datafileIds: file.id.toString(),
  });

  const tracking = useTrackingCallback();

  return (
    <Button
      size="sm"
      variant="link"
      target="_blank"
      href={url}
      onClick={() => {
        tracking('Download', 'Datafile', file.location);
        window.open(url, '_blank');
      }}
    >
      <FontAwesomeIcon icon={faDownload} />
    </Button>
  );
}
