import {
  type Datafile,
  useEndpointURL,
  DATA_DOWNLOAD_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { ZoomableImage } from 'components/image';

export function ImageViewer({ datafile }: { datafile: Datafile }) {
  const url = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  return (
    <ZoomableImage
      src={url({
        datafileIds: datafile.id.toString(),
      })}
      alt={datafile.name}
      zoomable
    />
  );
}
