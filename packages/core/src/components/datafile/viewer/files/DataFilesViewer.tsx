import type { Datafile } from '@edata-portal/icat-plus-api';
import { DataFileViewerWithInfo } from 'components/datafile/viewer/file/DataFileViewer';
import { Panel, PanelGroup, PanelResizeHandle } from 'react-resizable-panels';

export function DataFilesViewer({
  currentFile,
  selectorComponent,
}: {
  currentFile: Datafile | undefined;
  selectorComponent: JSX.Element;
}) {
  return (
    <div className="w-100 h-100 overflow-auto">
      <PanelGroup direction="horizontal" className="w-100 h-100">
        <Panel defaultSize={30} minSize={20} className="pe-1">
          {selectorComponent}
        </Panel>
        <PanelResizeHandle
          className="bg-info"
          style={{
            width: 5,
          }}
        />
        <Panel minSize={30}>
          {currentFile ? (
            <DataFileViewerWithInfo datafile={currentFile} />
          ) : (
            <NoSelectedFile />
          )}
        </Panel>
      </PanelGroup>
    </div>
  );
}

function NoSelectedFile() {
  return (
    <div className="d-flex h-100 align-items-center justify-content-center flex-column">
      <strong> No file selected.</strong>
      <i>Browse and select a file on the left to view it.</i>
    </div>
  );
}
