import type { Datafile } from '@edata-portal/icat-plus-api';
import { DataFilesViewerSelectorItem } from 'components/datafile/viewer/files/selectors/DataFilesViewerSelectorItem';
import type { DatafileFileItem } from 'helpers';

export function DataFilesViewerListSelector({
  files,
  onSelected,
  selected,
}: {
  files: Datafile[];
  onSelected: (datafile: Datafile) => void;
  selected?: Datafile;
}) {
  const items: DatafileFileItem[] = files.map((file) => {
    return {
      type: 'file',
      name: file.name,
      datafile: file,
    };
  });

  if (items.length === 0) {
    return <i>No files found</i>;
  }

  return (
    <div className="d-flex flex-column h-100 overflow-auto">
      {items.map((item) => (
        <DataFilesViewerSelectorItem
          key={item.datafile.id}
          item={item}
          onClick={() => onSelected(item.datafile)}
          selected={selected?.id === item.datafile.id}
          displayName={item.name}
          fullName={item.datafile.location}
        />
      ))}
    </div>
  );
}
