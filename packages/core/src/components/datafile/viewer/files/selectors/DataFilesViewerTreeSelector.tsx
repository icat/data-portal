import type { Datafile } from '@edata-portal/icat-plus-api';
import {
  faAngleDown,
  faAngleRight,
  faFolder,
  faFile,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DataFileStatus } from 'components/datafile/viewer/file/DataFileStatus';
import { EllipsisName } from 'components/utils';
import {
  getDatafileDirectoryContent,
  type DatafileDirectoryContent,
  type DatafileDirectory,
  getNbItemsDirectory,
  type DatafileFileItem,
} from 'helpers';
import { useState, useMemo, Suspense } from 'react';

export function DataFilesViewerTreeSelector({
  files,
  onSelected,
  selected,
}: {
  files: Datafile[];
  onSelected: (datafile: Datafile) => void;
  selected?: Datafile;
}) {
  const content = getDatafileDirectoryContent(files);

  if (content.length === 0) {
    return <i>No files found</i>;
  }

  return (
    <div className="overflow-auto h-100">
      <ul className="p-0 m-0 ">
        <DirectoryContentSelector
          content={content}
          onSelected={onSelected}
          selected={selected}
        />
      </ul>
    </div>
  );
}

function DirectoryContentSelector({
  content,
  onSelected,
  selected,
}: {
  content: DatafileDirectoryContent[];
  onSelected: (datafile: Datafile) => void;
  selected?: Datafile;
}) {
  return content
    .sort((a, b) => {
      //files first
      if (a.type === 'file' && b.type === 'directory') {
        return -1;
      }
      if (a.type === 'directory' && b.type === 'file') {
        return 1;
      }
      //sort by name
      return a.name.localeCompare(b.name);
    })
    .map((item) => {
      if (item.type === 'directory') {
        return (
          <DirectorySelector
            key={item.name}
            directory={item}
            onSelected={onSelected}
            selected={selected}
          />
        );
      }
      return (
        <FileSelector
          key={item.name}
          file={item}
          onSelected={onSelected}
          selected={selected}
        />
      );
    });
}

function DirectorySelector({
  directory,
  prefix,
  onSelected,
  selected,
}: {
  directory: DatafileDirectory;
  prefix?: string;
  onSelected: (datafile: Datafile) => void;
  selected?: Datafile;
}) {
  const initialExpanded = directory.content.length <= 20;

  const [expanded, setExpanded] = useState<boolean>(initialExpanded);

  const nbItems = useMemo(() => getNbItemsDirectory(directory), [directory]);

  const withPrefix = useMemo(() => {
    if (prefix) {
      return prefix + '/' + directory.name;
    }
    return directory.name;
  }, [prefix, directory]);

  const displayName = useMemo(() => {
    if (expanded) {
      return withPrefix;
    }
    return `${withPrefix} [${nbItems}]`;
  }, [expanded, withPrefix, nbItems]);

  if (directory.content.length === 1) {
    const item = directory.content[0];
    if (item.type === 'directory') {
      return (
        <DirectorySelector
          directory={item}
          prefix={withPrefix}
          onSelected={onSelected}
          selected={selected}
        />
      );
    }
  }

  return (
    <li
      style={{
        listStyle: 'none',
        paddingLeft: 0,
      }}
    >
      <RenderItem
        item={directory}
        displayName={displayName}
        fullName={withPrefix}
        expanded={expanded}
        onClick={() => setExpanded(!expanded)}
      />
      {expanded && (
        <ul className="ps-3 m-0">
          <DirectoryContentSelector
            content={directory.content}
            onSelected={onSelected}
            selected={selected}
          />
        </ul>
      )}
    </li>
  );
}

function FileSelector({
  file,
  onSelected,
  selected,
}: {
  file: DatafileFileItem;
  onSelected: (datafile: Datafile) => void;
  selected?: Datafile;
}) {
  return (
    <li
      style={{
        listStyle: 'none',
        paddingLeft: 0,
      }}
    >
      <RenderItem
        item={file}
        displayName={file.name}
        fullName={file.name}
        onClick={() => onSelected(file.datafile)}
        selected={selected === file.datafile}
      />
    </li>
  );
}

function RenderItem({
  item,
  displayName,
  fullName,
  expanded,
  onClick,
  selected,
}: {
  item: DatafileDirectoryContent;
  displayName: string;
  fullName: string;
  expanded?: boolean;
  onClick: () => void;
  selected?: boolean;
}) {
  const [over, setOver] = useState(false);

  const icon = useMemo(() => {
    if (expanded === true) {
      return faAngleDown;
    }
    if (expanded === false) {
      return faAngleRight;
    }
    if (item.type === 'directory') {
      return faFolder;
    }
    return faFile;
  }, [expanded, item.type]);

  const textColor = useMemo(() => {
    if (item.type === 'directory') {
      return 'text-dark';
    }
    return 'text-info';
  }, [item]);

  return (
    <div
      onMouseEnter={() => setOver(true)}
      onMouseLeave={() => setOver(false)}
      onClick={onClick}
      className={`${textColor} px-1 d-flex flex-row align-items-center gap-1`}
      style={{
        cursor: 'pointer',
        backgroundColor: over ? 'lightgray' : 'transparent',
        textDecoration: over || selected ? 'underline' : 'none',
      }}
    >
      <FontAwesomeIcon icon={icon} className="mr-1" />

      <EllipsisName
        displayName={displayName}
        fullName={fullName}
        side="start"
        className="monospace"
      />

      {item.type === 'file' && (
        <Suspense fallback={'...'}>
          <DataFileStatus datafile={item.datafile} />
        </Suspense>
      )}
    </div>
  );
}
