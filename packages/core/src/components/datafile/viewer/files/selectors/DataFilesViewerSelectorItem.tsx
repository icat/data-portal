import {
  faAngleDown,
  faAngleRight,
  faFolder,
  faFile,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DataFileStatus } from 'components/datafile/viewer/file/DataFileStatus';
import { EllipsisName } from 'components/utils';
import type { DatafileDirectoryContent } from 'helpers';
import { useState, useMemo, Suspense } from 'react';

export function DataFilesViewerSelectorItem({
  item,
  displayName,
  fullName,
  expanded,
  onClick,
  selected,
}: {
  item: DatafileDirectoryContent;
  displayName: string;
  fullName: string;
  expanded?: boolean;
  onClick: () => void;
  selected?: boolean;
}) {
  const [over, setOver] = useState(false);

  const icon = useMemo(() => {
    if (expanded === true) {
      return faAngleDown;
    }
    if (expanded === false) {
      return faAngleRight;
    }
    if (item.type === 'directory') {
      return faFolder;
    }
    return faFile;
  }, [expanded, item.type]);

  const textColor = useMemo(() => {
    if (item.type === 'directory') {
      return 'text-dark';
    }
    return 'text-info';
  }, [item]);

  return (
    <div
      onMouseEnter={() => setOver(true)}
      onMouseLeave={() => setOver(false)}
      onClick={onClick}
      className={`${textColor} px-1 d-flex flex-row align-items-center gap-1`}
      style={{
        cursor: 'pointer',
        backgroundColor: over ? 'lightgray' : 'transparent',
        textDecoration: over || selected ? 'underline' : 'none',
      }}
    >
      <FontAwesomeIcon icon={icon} className="mr-1" />
      <EllipsisName
        displayName={displayName}
        fullName={fullName}
        side="start"
        className="monospace"
      />

      {item.type === 'file' && (
        <Suspense fallback={'...'}>
          <DataFileStatus datafile={item.datafile} />
        </Suspense>
      )}
    </div>
  );
}
