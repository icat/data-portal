export type ImageDefinition = {
  src: string;
  alt?: string;
  legend?: string;
  showCenter?: boolean;
};
