import { Image } from 'components/image/Image';
import type { ImageDefinition } from 'components/image/ImageDefinition';
import { LoadingImages, useLoadImages } from 'components/image/useLoadImages';
import { Zoomable } from 'components/layout';
import { useEffect, useState } from 'react';

/**
 * This component will load the image, and make it zoomable
 */
export function ZoomableImage(
  props: ImageDefinition & {
    zoomable?: boolean;
  },
) {
  const { loaded, imagesData, progress } = useLoadImages([props.src]);

  if (!loaded) return <LoadingImages progress={progress} />;

  const data = imagesData[0];

  if (!data) return null;

  const image = <Image {...props} src={data} />;

  return props.zoomable ? <Zoomable>{image}</Zoomable> : image;
}

export type ImagesDefinition = Omit<ImageDefinition, 'src'> & {
  src: string[];
};

const EMPTY_IMAGE =
  'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

/**
 * This component will load the images, alternate between them, and make it zoomable
 */
export function ZoomableImages(props: ImagesDefinition) {
  const { loaded, imagesData, nbTotal, progress } = useLoadImages(props.src);

  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentImageIndex((c) => (c + 1) % nbTotal);
    }, 1000);
    return () => clearInterval(interval);
  }, [nbTotal]);

  if (!loaded) return <LoadingImages progress={progress} />;

  const currentImageData = imagesData[currentImageIndex] || EMPTY_IMAGE;

  return (
    <Zoomable>
      <Image {...props} src={currentImageData} />
    </Zoomable>
  );
}
