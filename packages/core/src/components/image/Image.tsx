import { ImageCenterMark } from 'components/image/ImageCenterMark';
import type { ImageDefinition } from 'components/image/ImageDefinition';
import { Image as ImageBootstrap } from 'react-bootstrap';

/**
 * This component displays an image and allows it to extend to any size
 * @param props.src the source of the image
 * @param props.alt the alt text of the image
 * @param props.legend Optional legend to be displayed under the image
 * @param props.showCenter if true, a cross is displayed at the center of the image
 */
export function Image(props: ImageDefinition) {
  const imageElement = (
    <ImageBootstrap
      fluid
      style={{
        //bootstrap fluid not enough here as it does not extend beyond source image size
        objectFit: 'contain',
        width: '100%',
        height: '100%',
      }}
      src={props.src}
      alt={props.alt}
    />
  );
  return (
    <div className="d-flex flex-column h-100 w-100">
      {props.showCenter ? (
        <ImageCenterMark>{imageElement}</ImageCenterMark>
      ) : (
        imageElement
      )}
      {props.legend && (
        <figcaption
          style={{
            fontSize: 'small',
            color: 'gray',
            textAlign: 'center',
          }}
        >
          {props.legend}
        </figcaption>
      )}
    </div>
  );
}
