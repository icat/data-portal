import { Image } from 'components/image/Image';
import type { ImageDefinition } from 'components/image/ImageDefinition';
import { LoadingImages, useLoadImages } from 'components/image/useLoadImages';
import { LazyWrapper } from 'components/utils';
import { useState } from 'react';
import { Modal } from 'react-bootstrap';

export function ImageWithModal({
  children,
  title,
  ...image
}: ImageDefinition & {
  title: string;
  children: React.ReactNode;
}) {
  const [show, setShow] = useState(false);

  const { loaded, imagesData, progress } = useLoadImages([image.src]);
  if (!loaded) return <LoadingImages progress={progress} />;
  const data = imagesData[0];

  if (!data) return null;

  return (
    <>
      <div
        style={{
          cursor: 'pointer',
        }}
        onClick={() => setShow(true)}
      >
        <Image {...image} src={data} />
      </div>
      <Modal show={show} onHide={() => setShow(false)} size="xl">
        <Modal.Header closeButton>
          <h5>{title}</h5>
        </Modal.Header>
        <Modal.Body>
          <LazyWrapper>{children}</LazyWrapper>
        </Modal.Body>
      </Modal>
    </>
  );
}
