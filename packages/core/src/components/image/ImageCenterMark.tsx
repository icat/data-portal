/**
 * This component displays an image and adds a pointer at the center of the image
 * Can't do that with bootstrap.
 */
export function ImageCenterMark({ children }: { children: JSX.Element }) {
  return (
    <div
      style={{
        position: 'relative',
      }}
    >
      {children}
      <div
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          width: '5%',
          aspectRatio: '1/1',
          transform: 'translate(-50%, -50%)',
        }}
      >
        <div
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: '50%',
            width: 1,
            backgroundColor: 'red',
            transform: 'translate(-50%, 0%)',
          }}
        />
        <div
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: '50%',
            height: 1,
            backgroundColor: 'red',
            transform: 'translate(0%, -50%)',
          }}
        />
      </div>
    </div>
  );
}
