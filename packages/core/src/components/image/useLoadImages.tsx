import { useFetchBase64 } from '@edata-portal/icat-plus-api';
import { useEffect, useMemo, useState } from 'react';
import { ProgressBar } from 'react-bootstrap';

/**
 * This will load images from a list of urls and return them as base64 strings to allow for caching with react-query and switching between them rapidly
 * @returns {loaded: boolean, progress: number, imagesData: string[], nbTotal: number, nbLoaded: number}
 * loaded: true if all images have been loaded
 * progress: a number between 0 and 1 indicating the progress of the loading
 * imagesData: an array of base64 strings representing the images
 * nbTotal: the total number of images to load
 * nbLoaded: the number of images that have been loaded
 */
export function useLoadImages(src: string[]) {
  const fetchBase64 = useFetchBase64();

  const [loaded, setLoaded] = useState<boolean[]>(
    Array(src.length || 0).fill(false),
  );

  const [progress, setProgress] = useState<number[]>(
    Array(src.length || 0).fill(0),
  );

  const [loadedKey, setLoadedKey] = useState<string | undefined>(undefined);

  const [imagesData, setImagesData] = useState<(string | undefined)[]>(
    Array(src.length || 0).fill(undefined),
  );

  const srcLoadingKey = src.slice().sort().join(',');

  const [startLoad, setStartLoad] = useState(false);
  useEffect(() => {
    setStartLoad(true);
    return () => setStartLoad(false);
  }, []);

  if (srcLoadingKey !== loadedKey && startLoad) {
    setLoadedKey(srcLoadingKey);
    setLoaded(Array(src.length || 0).fill(false));
    setImagesData(Array(src.length || 0).fill(undefined));
    src.forEach((image, i) => {
      const onProgress = (progress: number) => {
        setProgress((previousProgress) => {
          const newProgress = previousProgress.slice();
          newProgress[i] = progress;
          return newProgress;
        });
      };
      fetchBase64(image, onProgress).then((imageData) => {
        setLoaded((previousLoaded) => {
          if (previousLoaded[i]) {
            return previousLoaded;
          }
          const newLoaded = previousLoaded.slice();
          newLoaded[i] = true;
          return newLoaded;
        });
        setImagesData((previousImagesData) => {
          const newDataUrls = previousImagesData.slice();
          newDataUrls[i] = imageData;
          return newDataUrls;
        });
      });
    });
  }

  const nbTotal = useMemo(() => src.length, [src.length]);
  const nbLoaded = useMemo(
    () => loaded.reduce((acc, cur) => acc + (cur ? 1 : 0), 0),
    [loaded],
  );
  const allLoaded = useMemo(() => nbLoaded === nbTotal, [nbLoaded, nbTotal]);

  const averageProgress = useMemo(
    () => progress.reduce((acc, cur) => acc + cur, 0) / progress.length,
    [progress],
  );

  return useMemo(
    () => ({
      loaded: allLoaded,
      progress: averageProgress,
      imagesData,
      nbTotal: nbTotal,
      nbLoaded,
    }),
    [allLoaded, averageProgress, imagesData, nbTotal, nbLoaded],
  );
}

export function LoadingImages({ progress }: { progress: number }) {
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center p-1 w-100"
      style={{
        gap: '1rem',
      }}
    >
      <ProgressBar
        className="w-100"
        animated
        variant="info"
        now={progress * 100}
      />
    </div>
  );
}
