import type { Investigation } from '@edata-portal/icat-plus-api';
import { formatDateToDay, getInstrumentNameByInvestigation } from 'helpers';
import { Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function InvestigationLabel({
  investigation,
  bg = 'secondary',
}: {
  investigation: Investigation;
  bg?: string;
}) {
  const endDate = investigation.endDate
    ? ` - ${formatDateToDay(investigation.endDate)}`
    : '';
  const investigationDates = `${formatDateToDay(
    investigation.startDate,
  )}${endDate}`;

  return (
    <>
      <InvestigationLabelBadge
        bg={bg}
        text={investigation.name}
        link={`/experiments?search=${investigation.name}`}
      />
      <InvestigationLabelBadge
        bg={bg}
        text={investigation.instrument?.name}
        link={`/experiments?instrument=${getInstrumentNameByInvestigation(
          investigation,
        )}`}
      />
      {investigationDates}
    </>
  );
}

function InvestigationLabelBadge({
  text,
  bg,
  link,
}: {
  text: string | undefined;
  bg: string;
  link: string;
}) {
  if (text) {
    return (
      <Link to={link}>
        <Badge
          bg={bg}
          style={{
            marginRight: '4px',
            verticalAlign: 'text-top',
          }}
        >
          {text}
        </Badge>
      </Link>
    );
  }
  return null;
}
