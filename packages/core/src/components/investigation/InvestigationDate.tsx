import {
  useGetEndpoint,
  type Investigation,
  INVESTIGATION_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { formatDateToDayAndTime, formatDateToDay } from 'helpers';
import { useMemo } from 'react';
import { Popover, OverlayTrigger } from 'react-bootstrap';

export function InvestigationDate({
  investigationId,
  investigation,
}: {
  investigation?: Investigation;
  investigationId?: number | string;
}) {
  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: String(investigationId),
    },
    skipFetch: investigation !== undefined,
  });

  const investigationObject = useMemo(() => {
    if (investigation !== undefined) return investigation;

    if (investigations?.length) return investigations[0];
    return undefined;
  }, [investigation, investigations]);

  if (!investigationObject?.startDate) return null;

  const overlay = (
    <Popover key={'popover-date'}>
      <Popover.Header>Investigation schedule</Popover.Header>
      <Popover.Body>
        <span>
          <strong>Start: </strong>
          {formatDateToDayAndTime(investigationObject.startDate)}
        </span>
        <br />
        <span>
          <strong>End: </strong>
          {formatDateToDayAndTime(investigationObject.endDate)}
        </span>
        <br />
      </Popover.Body>
    </Popover>
  );

  return (
    <OverlayTrigger placement="auto" overlay={overlay}>
      <div key={'content-date'} className="text-info">
        {formatDateToDay(investigationObject.startDate)}
      </div>
    </OverlayTrigger>
  );
}
