import type { Investigation } from '@edata-portal/icat-plus-api';
import { InvestigationNameButton } from 'components/investigation/InvestigationNameButton';

export function InvestigationOpenBtn({
  investigation,
  sampleId,
  title,
}: {
  investigation: Investigation;
  sampleId?: number;
  title?: string;
}) {
  if (investigation.canAccessDatasets === false) {
    return <span className="text-nowrap">{title || investigation.name}</span>;
  }

  return (
    <InvestigationNameButton
      title={title || investigation.name}
      onClick={(e) => {
        e.stopPropagation();
      }}
      link={`/investigation/${investigation.id}/datasets${sampleId ? `?sampleId=${sampleId}` : ''}`}
    />
  );
}
