import { faSquareArrowUpRight } from '@fortawesome/free-solid-svg-icons';
import {
  INVESTIGATION_LIST_ENDPOINT,
  Investigation,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { OverlayLink } from 'components/browse/OverlayLink';
import { useConfig } from 'context/config/ConfigContext';
import { useMemo } from 'react';
import { useInvestigationAccess } from 'hooks/investigationAccess';

export function UserPortalButton({
  investigationId,
  investigation,
}: {
  investigation?: Investigation;
  investigationId?: number | string;
}) {
  const config = useConfig();

  const investigations = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: String(investigationId),
    },
    skipFetch: investigation !== undefined,
  });

  const investigationObject = useMemo(() => {
    if (investigation !== undefined) return investigation;

    if (investigations?.length) return investigations[0];
    return undefined;
  }, [investigation, investigations]);

  const access = useInvestigationAccess({
    investigation: investigationObject,
    roles: {
      participant: true,
      instrumentScientist: true,
      administrator: true,
    },
  });

  /** Button will not shown if user has no access to the A-Form */
  if (!access) {
    return null;
  }

  const pk =
    investigationObject?.parameters?.[
      config.ui.userPortal.investigationParameterPkName
    ];

  if (!pk) return null;
  return (
    <OverlayLink
      link={config.ui.userPortal.link + pk}
      icon={faSquareArrowUpRight}
      tooltipText={'Open A-Form'}
      openOnNewTab={true}
    ></OverlayLink>
  );
}
