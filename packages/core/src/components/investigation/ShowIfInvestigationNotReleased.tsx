import {
  INVESTIGATION_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { isReleasedInvestigation } from 'helpers';

export function ShowIfInvestigationNotReleased({
  investigationId,
  children,
}: {
  investigationId: string;
  children: React.ReactNode;
}) {
  const investigationList = useGetEndpoint({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    params: {
      ids: investigationId,
    },
  });
  const investigation =
    investigationList && investigationList.length
      ? investigationList[0]
      : undefined;

  if (investigation && !isReleasedInvestigation(investigation)) {
    return <>{children}</>;
  }
  return null;
}
