import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function InvestigationNameButton({
  title,
  onClick,
  link,
}: {
  title: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  link?: string;
}) {
  const btn = (
    <Button
      test-id="investigation-name-button"
      size="sm"
      style={{
        width: '100%',
        textAlign: 'left',
        whiteSpace: 'nowrap',
        padding: '1px 0.5rem',
      }}
      variant="primary"
      onClick={onClick}
    >
      <FontAwesomeIcon icon={faArrowCircleRight} />
      <span style={{ marginLeft: 10 }}>{title}</span>
    </Button>
  );
  if (link) {
    return <Link to={link}>{btn}</Link>;
  }
  return btn;
}
