import { faCircleMinus, faCirclePlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type { GridGraphColumn } from 'components/gridgraph/gridGraphDefinition';
import { prettyPrint } from 'helpers';

export function GridGraphCollapsedColumnNode({
  column,
  onClick,
  isCollapsed,
}: {
  column: GridGraphColumn;
  onClick: () => void;
  isCollapsed: boolean;
}) {
  if (!column.collapsible) return null;

  return (
    <div
      className="p-2 bg-light border border-info rounded d-flex flex-column align-items-center justify-content-between"
      style={{ cursor: 'pointer' }}
      onClick={onClick}
    >
      <span className="text-nowrap">
        <strong>{prettyPrint(column.collapsedLabel)}</strong> :{' '}
        {column.items.length}
      </span>
      <FontAwesomeIcon
        className="text-info"
        icon={isCollapsed ? faCirclePlus : faCircleMinus}
        size="xl"
      />
    </div>
  );
}
