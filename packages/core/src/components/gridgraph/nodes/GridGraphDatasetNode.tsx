import type { Dataset } from '@edata-portal/icat-plus-api';
import { useViewers } from 'context';

export function GridGraphDatasetNode({
  dataset,
  renderProps,
}: {
  dataset: Dataset;
  renderProps?: any;
}) {
  const viewers = useViewers();

  return (
    <div className="d-flex h-100 w-100 justify-content-center">
      {viewers.viewDataset(dataset, 'snapshot', renderProps)}
    </div>
  );
}
