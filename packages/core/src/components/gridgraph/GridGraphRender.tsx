import {
  GridGraphDefinition,
  gridGraphDefinitionToRenderItems,
  type GridGraphCollapsedManager,
} from 'components/gridgraph/gridGraphDefinition';
import { useMemo, useState } from 'react';

export type GridGraphPosition = {
  row: number;
  column: number;
};

export type GridGraphRenderNode = {
  type: 'node';
  node: React.ReactNode;
  position: GridGraphPosition;
};

export type GridGraphRenderLink = {
  type: 'link';
  from: GridGraphPosition;
  to: GridGraphPosition;
};

export type GridGraphRenderItem = GridGraphRenderNode | GridGraphRenderLink;

const LINK_Z_INDEX = 1;
const ITEM_Z_INDEX = 2;

export function GridGraphRender({
  grid,
  defaultCollapsed = true,
}: {
  grid: GridGraphDefinition;
  defaultCollapsed?: boolean;
}) {
  const [toggledColumns, setToggledColumns] = useState<string[]>([]);

  const collapsedManager: GridGraphCollapsedManager = useMemo(
    () => ({
      isCollapsedColumn: (columnId) => {
        const isToggled = toggledColumns.includes(columnId);
        if (defaultCollapsed) return !isToggled;
        return isToggled;
      },
      toggleCollapsedColumn: (columnId) => {
        setToggledColumns((v) => {
          if (v?.includes(columnId)) {
            return v?.filter((c) => c !== columnId);
          }
          return [...(v ?? []), columnId];
        });
      },
    }),
    [toggledColumns, defaultCollapsed],
  );

  const items = useMemo(() => {
    return gridGraphDefinitionToRenderItems(grid, collapsedManager);
  }, [grid, collapsedManager]);

  const maxColumn = useMemo(
    () =>
      Math.max(
        ...items.map((item) =>
          item.type === 'link' ? 0 : item.position.column,
        ),
      ),
    [items],
  );

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: `repeat(${maxColumn},minmax(min-content,max-content))`,
        zIndex: 0,
      }}
    >
      {items.map((item, index) => {
        if (item.type === 'node') {
          return <RenderNode key={index} node={item} />;
        }
        if (item.type === 'link') {
          return <RenderLink key={index} link={item} />;
        }
        return null;
      })}
    </div>
  );
}

function RenderNode({ node }: { node: GridGraphRenderNode }) {
  return (
    <div
      className="m-1"
      style={{
        zIndex: ITEM_Z_INDEX,
        gridRow: node.position.row,
        gridColumn: node.position.column,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {node.node}
    </div>
  );
}

function RenderLink({ link }: { link: GridGraphRenderLink }) {
  const isVertical = link.from.column === link.to.column;
  const isHorizontal = link.from.row === link.to.row;

  if (isVertical && isHorizontal) return null;
  if (!isVertical && !isHorizontal)
    throw new Error('Can only render vertical or horizontal links');

  const start = isVertical
    ? Math.min(link.from.row, link.to.row)
    : Math.min(link.from.column, link.to.column);
  const end = isVertical
    ? Math.max(link.from.row, link.to.row)
    : Math.max(link.from.column, link.to.column);

  const hasMiddle = end - start > 1;

  const startPosition = isVertical
    ? {
        row: start,
        column: link.from.column,
      }
    : {
        row: link.from.row,
        column: start,
      };
  const endPosition = isVertical
    ? {
        row: end,
        column: link.from.column,
      }
    : {
        row: link.from.row,
        column: end,
      };

  const middleStart = isVertical
    ? {
        row: start + 1,
        column: link.from.column,
      }
    : {
        row: link.from.row,
        column: start + 1,
      };
  const middleSpan = isVertical
    ? {
        rowSpan: end - start - 1,
      }
    : {
        colSpan: end - start - 1,
      };

  return (
    <>
      <LinkCell {...startPosition} bottom={isVertical} right={isHorizontal} />
      {hasMiddle && (
        <LinkCell
          {...middleStart}
          {...middleSpan}
          top={isVertical}
          left={isHorizontal}
          bottom={isVertical}
          right={isHorizontal}
        />
      )}
      <LinkCell {...endPosition} top={isVertical} left={isHorizontal} />
    </>
  );
}

function LinkCell({
  top,
  left,
  bottom,
  right,
  horizontal,
  vertical,
  column,
  row,
  colSpan,
  rowSpan,
  minWidth = 10,
}: {
  top?: boolean;
  left?: boolean;
  bottom?: boolean;
  right?: boolean;
  horizontal?: boolean;
  vertical?: boolean;
  column: number;
  row: number;
  colSpan?: number;
  rowSpan?: number;
  minWidth?: number;
}) {
  return (
    <div
      className="link-cell"
      style={{
        position: 'relative',
        width: '100%',
        minWidth: minWidth,
        minHeight: 10,
        flexBasis: 0,
        flexGrow: 1,
        gridColumnStart: column,
        gridRowStart: row,
        gridColumnEnd: colSpan && column + colSpan,
        gridRowEnd: rowSpan && row + rowSpan,
        zIndex: LINK_Z_INDEX,
      }}
    >
      {top && (
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: '50%',

            height: '50%',
            width: 2,
            backgroundColor: 'black',
            transform: 'translateX(-50%)',
          }}
        />
      )}
      {bottom && (
        <div
          style={{
            position: 'absolute',
            bottom: 0,
            left: '50%',
            height: '50%',
            width: 2,
            backgroundColor: 'black',
            transform: 'translateX(-50%)',
          }}
        />
      )}
      {left && (
        <div
          style={{
            position: 'absolute',
            top: '50%',
            left: 0,
            width: '50%',
            height: 2,
            backgroundColor: 'black',
            transform: 'translateY(-50%)',
          }}
        />
      )}
      {right && (
        <div
          style={{
            position: 'absolute',
            top: '50%',
            right: 0,
            width: '50%',
            height: 2,
            backgroundColor: 'black',
            transform: 'translateY(-50%)',
          }}
        />
      )}
      {horizontal && (
        <div
          className="mx-1"
          style={{
            position: 'absolute',
            top: '50%',
            left: 0,
            right: 0,
            height: 2,
            backgroundColor: 'black',
            transform: 'translateY(-50%)',
          }}
        />
      )}
      {vertical && (
        <div
          className="my-1"
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: '50%',
            width: 2,
            backgroundColor: 'black',
            transform: 'translateX(-50%)',
          }}
        />
      )}
    </div>
  );
}
