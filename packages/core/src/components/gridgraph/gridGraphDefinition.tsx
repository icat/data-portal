import type { Dataset } from '@edata-portal/icat-plus-api';
import type {
  GridGraphRenderItem,
  GridGraphRenderNode,
  GridGraphPosition,
} from 'components/gridgraph/GridGraphRender';
import { GridGraphCollapsedColumnNode } from 'components/gridgraph/nodes/GridGraphCollapsedColumnNode';
import { GridGraphDatasetNode } from 'components/gridgraph/nodes/GridGraphDatasetNode';

export type GridGraphDefinition = {
  root: GridGraphItem;
  mergedItem?: MergedItem;
};

export type GridGraphCollapsedManager = {
  isCollapsedColumn: (columnId: string) => boolean;
  toggleCollapsedColumn: (columnId: string) => void;
};

export type MergedItem = {
  type: 'merged';
  toIds: string[];
  content: GridGraphItem;
};

export type GridGraphItem = GridGraphColumn | GridGraphRow | GridGraphCell;

export type GridGraphColumn = {
  id: string;
  type: 'column';
  items: GridGraphItem[];
  collapsible?: boolean;
} & (
  | {
      collapsible: false;
    }
  | {
      collapsible: true;
      collapsedLabel: string;
    }
);

export type GridGraphRow = {
  id: string;
  type: 'row';
  items: GridGraphItem[];
};

export type GridGraphCell = {
  id: string;
  type: 'cell';
  content: GridGraphCellContent;
};

export type GridGraphCellContent =
  | { type: 'dataset'; dataset: Dataset; renderProps?: any }
  | { type: 'element'; element: JSX.Element };

export function gridGraphDefinitionToRenderItems(
  definition: GridGraphDefinition,
  collapsedManager: GridGraphCollapsedManager,
): GridGraphRenderItem[] {
  const startPosition = { row: 1, column: 1 };

  const items = getRenderGridGraphItem(
    definition.root,
    collapsedManager,
    startPosition,
  );

  return [
    ...items,
    ...getRenderMergedItem(items, definition, collapsedManager),
  ];
}

function getRenderMergedItem(
  items: GridGraphRenderItemWithIds[],
  definition: GridGraphDefinition,
  collapsedManager: GridGraphCollapsedManager,
): GridGraphRenderItem[] {
  const mergedItem = definition.mergedItem;
  if (!mergedItem) {
    return [];
  }

  const nodes = items.filter(
    (item) => item.type === 'node' && item.ids?.length,
  );

  const maxColumn = Math.max(
    ...nodes
      .filter((i): i is GridGraphRenderNode => i.type === 'node')
      .map((i) => i.position.column),
  );

  const mergedItemPosition = {
    row: 1,
    column: maxColumn + 1,
  };

  const mergedItems = getRenderGridGraphItem(
    mergedItem.content,
    collapsedManager,
    mergedItemPosition,
  );

  const sourcePositions = nodes
    .filter(
      (node) =>
        node.ids && node.ids.some((id) => mergedItem.toIds?.includes(id)),
    )
    .filter((i): i is GridGraphRenderNode => i.type === 'node')

    .map((item) => item.position)
    .map(
      (position) =>
        ({
          type: 'link',
          from: position,
          to: {
            row: position.row,
            column: mergedItemPosition.column,
          },
        }) as const,
    );

  const maxSourceRow = Math.max(...sourcePositions.map((i) => i.from.row));

  return [
    ...mergedItems,
    ...sourcePositions,
    ...(sourcePositions.length
      ? [
          {
            type: 'link',
            from: {
              row: maxSourceRow,
              column: mergedItemPosition.column,
            },
            to: {
              row: mergedItemPosition.row,
              column: mergedItemPosition.column,
            },
          } as const,
        ]
      : []),
  ];
}

type GridGraphRenderItemWithIds = GridGraphRenderItem & { ids?: string[] };

function getRenderGridGraphItem(
  item: GridGraphItem,
  collapsedManager: GridGraphCollapsedManager,
  position: GridGraphPosition,
  columnOffset = 0,
): GridGraphRenderItemWithIds[] {
  if (item.type === 'cell') {
    return getRenderGridGraphCell(item, position);
  }
  if (item.type === 'row') {
    return getRenderGridGraphRow(item, collapsedManager, position);
  }
  if (item.type === 'column') {
    return getRenderGridGraphColumn(
      item,
      collapsedManager,
      position,
      columnOffset,
    );
  }
  throw new Error('Invalid GridGraphItem type');
}

function getRenderGridGraphRow(
  row: GridGraphRow,
  collapsedManager: GridGraphCollapsedManager,
  position: GridGraphPosition,
): GridGraphRenderItemWithIds[] {
  const rowPosition = position.row;
  const res: GridGraphRenderItemWithIds[] = [];
  let nextColumn = position.column;
  let lastColumn = nextColumn;

  let columnOffset = 1;

  row.items.forEach((item) => {
    const column = nextColumn;

    const itemPosition = {
      row: rowPosition,
      column: column,
    };
    const itemRenderItems = getRenderGridGraphItem(
      item,
      collapsedManager,
      itemPosition,
      columnOffset,
    );
    res.push(...itemRenderItems);

    const itemSize = getGridGraphItemSize(item, collapsedManager, 0);

    lastColumn = column;
    nextColumn = column + itemSize.column;
    columnOffset = Math.max(columnOffset, itemSize.row);
  });

  res.push({
    type: 'link',
    from: position,
    to: {
      row: rowPosition,
      column: lastColumn,
    },
  });

  return res;
}

function getRenderGridGraphColumn(
  column: GridGraphColumn,
  collapsedManager: GridGraphCollapsedManager,
  position: GridGraphPosition,
  columnOffset: number,
): GridGraphRenderItemWithIds[] {
  //column with a single item is ignored
  if (column.items.length === 1) {
    return getRenderGridGraphItem(
      column.items[0],
      collapsedManager,
      position,
      columnOffset,
    );
  }
  const isCollapsed = collapsedManager.isCollapsedColumn(column.id);
  const toggleCollapseItem = getRenderGridGraphCell(
    {
      id: column.id,
      type: 'cell',
      content: {
        type: 'element',
        element: (
          <div className="d-flex align-items-center justify-content-center h-100 w-100">
            <GridGraphCollapsedColumnNode
              onClick={() => collapsedManager.toggleCollapsedColumn(column.id)}
              column={column}
              isCollapsed={isCollapsed}
            />
          </div>
        ),
      },
    },
    position,
    isCollapsed ? column.items : undefined,
  );

  const res: GridGraphRenderItemWithIds[] = column.collapsible
    ? [...toggleCollapseItem]
    : [];

  if (column.collapsible && isCollapsed) {
    return res;
  }

  const columnPosition = position.column;
  let nextRow = position.row + columnOffset;
  let lastRow = nextRow;

  column.items.forEach((item) => {
    const row = nextRow;

    const itemPosition = {
      row: row,
      column: columnPosition,
    };
    const itemRenderItems = getRenderGridGraphItem(
      item,
      collapsedManager,
      itemPosition,
    );
    res.push(...itemRenderItems);

    const itemSize = getGridGraphItemSize(item, collapsedManager, 0);

    nextRow = row + itemSize.row;
    lastRow = row;
  });

  res.push({
    type: 'link',
    from: position,
    to: {
      row: lastRow,
      column: columnPosition,
    },
  });

  return res;
}

function getCellIds(item: GridGraphItem): string[] {
  if (item.type === 'cell') {
    return [item.id];
  }
  if (item.type === 'row') {
    return item.items.flatMap(getCellIds);
  }
  if (item.type === 'column') {
    return item.items.flatMap(getCellIds);
  }
  throw new Error('Invalid GridGraphItem type');
}

function getRenderGridGraphCell(
  cell: GridGraphCell,
  position: GridGraphPosition,
  hiddenItems: GridGraphItem[] = [],
): GridGraphRenderItemWithIds[] {
  return [
    {
      type: 'node',
      node: <CellContent content={cell.content} />,
      position,
      ids: [...getCellIds(cell), ...hiddenItems.flatMap(getCellIds)],
    },
  ];
}

function CellContent({ content }: { content: GridGraphCellContent }) {
  if (content.type === 'dataset') {
    return <GridGraphDatasetNode {...content} />;
  }
  return content.element;
}

function getGridGraphItemSize(
  item: GridGraphItem,
  collapsedManager: GridGraphCollapsedManager,
  columnOffset: number,
): GridGraphPosition {
  if (item.type === 'cell') {
    return { row: 1, column: 1 };
  }
  if (item.type === 'row') {
    return item.items.reduce(
      (acc, column) => {
        const columnSize = getGridGraphItemSize(
          column,
          collapsedManager,
          acc.row,
        );
        return {
          row: Math.max(acc.row, columnSize.row),
          column: acc.column + columnSize.column,
        };
      },
      { row: 1, column: 0 },
    );
  }
  if (item.type === 'column') {
    if (collapsedManager.isCollapsedColumn(item.id)) {
      return { row: 1, column: 1 };
    }
    //column with a single item is ignored
    if (item.items.length === 1) {
      return getGridGraphItemSize(
        item.items[0],
        collapsedManager,
        columnOffset,
      );
    }
    return item.items.reduce(
      (acc, row) => {
        const rowSize = getGridGraphItemSize(row, collapsedManager, 0);
        return {
          row: acc.row + rowSize.row,
          column: Math.max(acc.column, rowSize.column),
        };
      },
      { row: columnOffset, column: 0 },
    );
  }
  throw new Error('Invalid GridGraphItem type');
}
