import React, { useEffect } from 'react';
import { faSortAsc, faSortDesc } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type { Table, RowData, Column } from '@tanstack/react-table';
import { flexRender } from '@tanstack/react-table';
import { TablePaginationElement } from 'components/table/PaginationElement';
import { Col, Form, Row, Table as BootstrapTable } from 'react-bootstrap';

export function TanstackBootstrapTable<TData extends RowData>({
  table,
  pagination = true,
  bordered = false,
  responsive = true,
  sorting = false,
  striped = true,
  onRowClick,
  getTrStyle,
  dataLength,
  expandRender,
  testId,
}: {
  table: Table<TData>;
  pagination?: boolean;
  bordered?: boolean;
  responsive?: boolean;
  sorting?: boolean;
  striped?: boolean;
  onRowClick?: (row: TData) => void;
  getTrStyle?: (row: TData) => React.CSSProperties;
  dataLength: number;
  expandRender?: (row: TData) => React.ReactNode;
  testId?: string;
}) {
  useEffect(() => {
    table.setExpanded({});
  }, [table.getState().pagination.pageIndex]); // eslint-disable-line react-hooks/exhaustive-deps

  const displayDuplicatePaginationAndFooter =
    table.getState().pagination.pageSize > 10 && dataLength > 10;
  return (
    <Col style={{ marginTop: '1rem' }}>
      {pagination && displayDuplicatePaginationAndFooter && (
        <div
          style={{
            paddingBottom: '1rem',
          }}
        >
          <TablePaginationElement table={table} dataLength={dataLength} />
        </div>
      )}
      <Row>
        <BootstrapTable
          striped={striped}
          hover
          responsive={responsive}
          bordered={bordered}
          size="sm"
          style={{
            backgroundColor: 'white',
            borderRadius: '0.25rem',
          }}
          test-id={testId}
        >
          <thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => (
                  <th
                    key={header.id}
                    {...(sorting
                      ? {
                          onClick: header.column.getToggleSortingHandler(),
                          style: {
                            cursor: header.column.getCanSort()
                              ? 'pointer'
                              : undefined,
                          },
                        }
                      : {})}
                  >
                    {header.isPlaceholder ? null : (
                      <div>
                        {sorting && <Sort column={header.column} />}
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                        {header.column.getCanFilter() ? (
                          <div onClick={(e) => e.stopPropagation()}>
                            <Filter column={header.column} table={table} />
                          </div>
                        ) : null}
                      </div>
                    )}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row) => (
              <React.Fragment key={row.id}>
                <tr
                  onClick={
                    onRowClick ? () => onRowClick(row.original) : undefined
                  }
                  style={{
                    cursor: onRowClick ? 'pointer' : undefined,
                    ...getTrStyle?.(row.original),
                  }}
                >
                  {row.getVisibleCells().map((cell) => {
                    return (
                      <td
                        key={cell.id}
                        style={{
                          verticalAlign: 'middle',
                        }}
                      >
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )}
                      </td>
                    );
                  })}
                </tr>
                {row.getIsExpanded() && expandRender ? (
                  <tr>
                    <td colSpan={table.getAllColumns().length}>
                      {expandRender(row.original)}
                    </td>
                  </tr>
                ) : null}
              </React.Fragment>
            ))}
          </tbody>
          <tfoot>
            {displayDuplicatePaginationAndFooter &&
              table.getFooterGroups().map((footerGroup) => (
                <tr key={footerGroup.id}>
                  {footerGroup.headers.map((header) => (
                    <th key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.footer,
                            header.getContext(),
                          )}
                    </th>
                  ))}
                </tr>
              ))}
          </tfoot>
        </BootstrapTable>
      </Row>
      {pagination && (
        <TablePaginationElement table={table} dataLength={dataLength} />
      )}
    </Col>
  );
}

function Filter({
  column,
  table,
}: {
  column: Column<any, any>;
  table: Table<any>;
}) {
  if (!column.getCanFilter()) return null;

  const firstValue = table
    .getPreFilteredRowModel()
    .flatRows[0]?.getValue(column.id);

  const columnFilterValue = column.getFilterValue();

  return typeof firstValue === 'number' ? (
    <div className="flex space-x-2">
      <Form.Control
        type="number"
        value={(columnFilterValue as [number, number])?.[0] ?? ''}
        onChange={(e) =>
          column.setFilterValue((old: [number, number]) => [
            e.target.value,
            old?.[1],
          ])
        }
        placeholder="Min"
        size="sm"
      />
      <Form.Control
        type="number"
        value={(columnFilterValue as [number, number])?.[1] ?? ''}
        onChange={(e) =>
          column.setFilterValue((old: [number, number]) => [
            old?.[0],
            e.target.value,
          ])
        }
        placeholder="Max"
        size="sm"
      />
    </div>
  ) : (
    <Form.Control
      value={(columnFilterValue ?? '') as string}
      onChange={(e) => column.setFilterValue(e.target.value)}
      placeholder="Search..."
      size="sm"
    />
  );
}

export function Sort({ column }: { column: Column<any, any> }) {
  if (!column.getCanSort()) return null;

  const isSorted = column.getIsSorted();

  return (
    <span className="me-2">
      <FontAwesomeIcon
        icon={faSortAsc}
        className={isSorted === 'asc' ? 'text-primary' : 'text-light'}
      />
      <FontAwesomeIcon
        icon={faSortDesc}
        className={isSorted === 'desc' ? 'text-primary' : 'text-light'}
      />
    </span>
  );
}
