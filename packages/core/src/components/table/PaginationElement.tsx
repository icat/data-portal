import type { Table, RowData } from '@tanstack/react-table';
import { Pagination, PaginationMenu } from 'hooks';

export function TablePaginationElement<TData extends RowData>({
  table,
  dataLength,
}: {
  table: Table<TData>;
  dataLength: number;
}) {
  const state = table.getState();

  const pagination: Pagination = {
    page: state.pagination.pageIndex,
    size: state.pagination.pageSize,
    queryParams: {
      limit: state.pagination.pageSize,
      skip: state.pagination.pageIndex * state.pagination.pageSize,
    },
    handlePageChange: (newPage) => {
      table.setPageIndex(newPage);
    },
    handlePageSizeChange: (newSize) => {
      table.setPageSize(newSize);
    },
    availableSizes: [10, 20, 30, 40, 50],
  };

  return (
    <PaginationMenu
      pagination={pagination}
      totalPages={table.getPageCount()}
      totalElements={dataLength}
    />
  );
}
