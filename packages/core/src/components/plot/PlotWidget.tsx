import {
  IconDefinition,
  faMaximize,
  faMinimize,
} from '@fortawesome/free-solid-svg-icons';
import { FullScreen } from 'components/layout';
import type { PlotSelectionEvent } from 'plotly.js-dist-min';
import { lazy, useState } from 'react';
import type { PlotParams } from 'react-plotly.js';
import createPlotlyComponent from 'react-plotly.js/factory';
export type { PlotSelectionEvent };

// https://reactjs.org/docs/code-splitting.html
// https://github.com/plotly/react-plotly.js#customizing-the-plotlyjs-bundle
const Plot = lazy(() =>
  import('plotly.js-dist-min').then((Plotly) => ({
    default: createPlotlyComponent(Plotly),
  })),
);

export interface PlotWidgetProps extends PlotParams {
  compact?: boolean;
  getTooltip?: (point: any) => JSX.Element;
  onSelect?: (e: PlotSelectionEvent) => void;
}

export function PlotWidget(props: PlotWidgetProps) {
  const [hovered, setHovered] = useState<any | undefined>(undefined);

  const [fullscreen, setFullscreen] = useState(false);

  const margin = props.layout.margin;
  props.layout.margin = props.compact
    ? {
        b: props.layout?.xaxis?.title ? 35 : 20,
        l: props.layout?.yaxis?.title ? 50 : 35,
        t: 60,
        r: 5,
        ...margin,
      }
    : props.layout.margin;
  const title = props.layout.title;
  props.layout.title = props.compact
    ? {
        yref: 'container',
        y: 40,
        yanchor: 'top',
        ...(typeof title === 'string' ? { text: title } : title),
      }
    : title;

  const plot = (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height: fullscreen ? '100%' : undefined,
      }}
    >
      <Plot
        style={{
          width: '100%',
          height: '100%',
          overflow: 'hidden',
        }}
        useResizeHandler
        onHover={
          props.getTooltip
            ? (e) => {
                setHovered(e);
              }
            : undefined
        }
        onUnhover={props.getTooltip ? () => setHovered(undefined) : undefined}
        {...props}
        layout={{
          modebar: {
            bgcolor: 'transparent',
            color: 'black',
            activecolor: 'black',
          },
          uirevision: 'true',
          ...props.layout,
        }}
        config={{
          displayModeBar: true,
          displaylogo: false,
          modeBarButtons: [
            props.onSelect ? ['select2d'] : [],
            ['toImage'],
            ['pan2d', 'zoom2d'],
            ['zoomIn2d', 'zoomOut2d', 'resetScale2d'],
            [
              {
                name: fullscreen ? 'Exit full screen' : 'Full screen',
                title: fullscreen ? 'Exit full screen' : 'Full screen',
                icon: faIconToPlotlyIcon(fullscreen ? faMinimize : faMaximize),
                click: function () {
                  setFullscreen((v) => !v);
                },
              },
            ],
          ],
          ...props.config,
        }}
        onSelected={props.onSelect}
      />
      {props.getTooltip && hovered !== undefined && (
        <div
          style={{
            position: 'fixed',
            left: `${hovered?.event.clientX + 5}px`,
            top: `${hovered?.event.clientY + 5}px`,
            backgroundColor: 'white',
            padding: 5,
            border: '1px solid black',
            zIndex: 100,
          }}
        >
          {props.getTooltip(hovered?.points[0])}
        </div>
      )}
    </div>
  );

  if (fullscreen)
    return (
      <FullScreen
        onExitFullscreen={() => setFullscreen(false)}
        showCloseButton={false}
      >
        {plot}
      </FullScreen>
    );

  return plot;
}

function faIconToPlotlyIcon(icon: IconDefinition) {
  const [width, height, , , path] = icon.icon;

  return {
    width,
    height,
    path: Array.isArray(path) ? path.join(' ') : path,
  };
}
