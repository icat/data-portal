import type { MetadataFiltersState } from 'components/filters/metadata/MetadataFilter';
import { SideNavMetadataFilter } from 'components/filters/metadata/SideNavMetadataFilter';
import type { MetadataFilterDefinition } from 'helpers/metadatafilter';

export function SideNavMetadataFilters({
  filterDefinitions,
  state,
}: {
  filterDefinitions: MetadataFilterDefinition[];
  state: MetadataFiltersState;
}) {
  return (
    <>
      {filterDefinitions.map((definition) => (
        <SideNavMetadataFilter
          key={definition.parameter}
          filterDefinition={definition}
          state={state}
        />
      ))}
    </>
  );
}
