import type { MetadataFiltersState } from 'components/filters/metadata/MetadataFilter';
import {
  MetadataFilterDefinition,
  clearMetadataFiltersForDefinition,
  formatMetadataFilters,
  getMetadataFiltersForDefinition,
  parseMetadataFilters,
} from 'helpers/metadatafilter';
import { Form } from 'react-bootstrap';

export function MetadataFilterSearch({
  filterDefinition,
  state,
}: {
  filterDefinition: MetadataFilterDefinition;
  state: MetadataFiltersState;
}) {
  const filters = parseMetadataFilters(state.currentFilters);
  const currentFilters = getMetadataFiltersForDefinition(
    filters,
    filterDefinition,
  );

  const value = currentFilters[0]?.value?.replace(/%/g, '');

  return (
    <Form.Control
      type="text"
      value={value || ''}
      placeholder={`Search in ${filterDefinition.label.toLocaleLowerCase()}...`}
      onChange={(e) => {
        const newFilters = clearMetadataFiltersForDefinition(
          filters,
          filterDefinition,
        );
        if (e.target.value?.length) {
          newFilters.push({
            parameter: filterDefinition.parameter,
            value: `%${e.target.value}%`,
            operator: 'like',
          });
        }
        state.setCurrentFilters(formatMetadataFilters(newFilters));
      }}
    />
  );
}
