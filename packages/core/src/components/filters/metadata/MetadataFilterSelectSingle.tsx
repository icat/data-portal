import type { MetadataFiltersState } from 'components/filters/metadata/MetadataFilter';
import { ParameterValueSelect } from 'components/filters/parametervalue';
import {
  MetadataFilterDefinition,
  clearMetadataFiltersForDefinition,
  formatMetadataFilters,
  getMetadataFiltersForDefinition,
  parseMetadataFilters,
} from 'helpers/metadatafilter';

export function MetadataFilterSelectSingle({
  filterDefinition,
  state,
}: {
  filterDefinition: MetadataFilterDefinition;
  state: MetadataFiltersState;
}) {
  const filters = parseMetadataFilters(state.currentFilters);
  const currentFilters = getMetadataFiltersForDefinition(
    filters,
    filterDefinition,
  );

  const value = currentFilters[0]?.value;

  return (
    <ParameterValueSelect
      type="list"
      isMulti={false}
      investigationId={state.investigationId}
      parameter={filterDefinition.parameter}
      value={value}
      onChange={(value) => {
        const newFilters = clearMetadataFiltersForDefinition(
          filters,
          filterDefinition,
        );
        if (value) {
          newFilters.push({
            parameter: filterDefinition.parameter,
            value,
            operator: 'eq',
          });
        }
        state.setCurrentFilters(formatMetadataFilters(newFilters));
      }}
      doPrettyPrint={filterDefinition.doPrettyPrint}
    />
  );
}
