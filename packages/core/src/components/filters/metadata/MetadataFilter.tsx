import { MetadataFilterRange } from 'components/filters/metadata/MetadataFilterRange';
import { MetadataFilterSearch } from 'components/filters/metadata/MetadataFilterSearch';
import { MetadataFilterSelectMulti } from 'components/filters/metadata/MetadataFilterSelectMulti';
import { MetadataFilterSelectSingle } from 'components/filters/metadata/MetadataFilterSelectSingle';
import type { MetadataFilterDefinition } from 'helpers/metadatafilter';

export type MetadataFiltersState = {
  currentFilters: string;
  setCurrentFilters: (newFilters: string) => void;
  investigationId: string;
};

/**
 * Allows to filter datasets or samples based on metadata parameters (see ICAT+'s `parameters` filter)
 * @param param0
 * @returns
 */
export function MetadataFilter({
  filterDefinition,
  state,
}: {
  filterDefinition: MetadataFilterDefinition;
  state: MetadataFiltersState;
}) {
  if (filterDefinition.type === 'select-single')
    return (
      <MetadataFilterSelectSingle
        filterDefinition={filterDefinition}
        state={state}
      />
    );
  if (filterDefinition.type === 'select-multi')
    return (
      <MetadataFilterSelectMulti
        filterDefinition={filterDefinition}
        state={state}
      />
    );
  if (filterDefinition.type === 'range')
    return (
      <MetadataFilterRange filterDefinition={filterDefinition} state={state} />
    );
  if (filterDefinition.type === 'search')
    return (
      <MetadataFilterSearch filterDefinition={filterDefinition} state={state} />
    );
  return null;
}
