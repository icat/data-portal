import {
  MetadataFilter,
  type MetadataFiltersState,
} from 'components/filters/metadata/MetadataFilter';
import { SideNavFilter } from 'components/utils';
import {
  MetadataFilterDefinition,
  clearMetadataFiltersForDefinition,
  formatMetadataFilters,
  getMetadataFiltersForDefinition,
  parseMetadataFilters,
} from 'helpers/metadatafilter';
import { useMemo } from 'react';

export function SideNavMetadataFilter({
  filterDefinition,
  state,
}: {
  filterDefinition: MetadataFilterDefinition;
  state: MetadataFiltersState;
}) {
  const allFilters = useMemo(
    () => parseMetadataFilters(state.currentFilters),
    [state.currentFilters],
  );

  const hasValue = useMemo(() => {
    const filters = getMetadataFiltersForDefinition(
      allFilters,
      filterDefinition,
    );
    return filters.length > 0;
  }, [allFilters, filterDefinition]);

  const onClear = () => {
    const newFilters = clearMetadataFiltersForDefinition(
      allFilters,
      filterDefinition,
    );
    state.setCurrentFilters(formatMetadataFilters(newFilters));
  };

  return (
    <SideNavFilter
      label={filterDefinition.label}
      onClear={onClear}
      hasValue={hasValue}
    >
      <MetadataFilter filterDefinition={filterDefinition} state={state} />
    </SideNavFilter>
  );
}
