import type { MetadataFiltersState } from 'components/filters/metadata/MetadataFilter';
import { ParameterValueSelect } from 'components/filters/parametervalue';
import {
  MetadataFilterDefinition,
  clearMetadataFiltersForDefinition,
  formatMetadataFilters,
  getMetadataFiltersForDefinition,
  parseMetadataFilters,
} from 'helpers/metadatafilter';

export function MetadataFilterRange({
  filterDefinition,
  state,
}: {
  filterDefinition: MetadataFilterDefinition;
  state: MetadataFiltersState;
}) {
  const filters = parseMetadataFilters(state.currentFilters);
  const currentFilters = getMetadataFiltersForDefinition(
    filters,
    filterDefinition,
  );

  const min = currentFilters?.find((f) => f.operator === 'gteq')?.value;
  const max = currentFilters?.find((f) => f.operator === 'lteq')?.value;

  const value =
    min && max
      ? {
          min: parseFloat(min),
          max: parseFloat(max),
          step: filterDefinition.step,
        }
      : undefined;

  return (
    <ParameterValueSelect
      type="range"
      investigationId={state.investigationId}
      parameter={filterDefinition.parameter}
      value={value}
      onChange={(value) => {
        const newFilters = clearMetadataFiltersForDefinition(
          filters,
          filterDefinition,
        );
        if (value) {
          newFilters.push({
            parameter: filterDefinition.parameter,
            value: value.min.toString(),
            operator: 'gteq',
          });
          newFilters.push({
            parameter: filterDefinition.parameter,
            value: value.max.toString(),
            operator: 'lteq',
          });
        }
        state.setCurrentFilters(formatMetadataFilters(newFilters));
      }}
      doPrettyPrint={filterDefinition.doPrettyPrint}
    />
  );
}
