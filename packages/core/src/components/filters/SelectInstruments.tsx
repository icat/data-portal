import {
  USER_INSTRUMENT_LIST_ENDPOINT,
  type Instrument,
  useGetEndpoint,
  INSTRUMENT_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { useMemo } from 'react';

import ReactSelect from 'react-select';

export function SelectInstruments({
  selectedInstruments = [],
  onChange,
  filter,
  forUser,
}: {
  selectedInstruments: (string | Instrument)[];
  onChange: (selectedInstruments: Instrument[]) => void;
  filter?: string;
  forUser: boolean;
}) {
  const userInstruments = useGetEndpoint({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    params: {
      ...(filter ? { filter: filter } : {}),
    },
    default: [] as Instrument[],
    skipFetch: !forUser,
  });
  const allInstruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
    default: [] as Instrument[],
    skipFetch: forUser,
  });

  const options = useMemo(() => {
    const items = [...userInstruments, ...allInstruments];
    return items.map((instrument) => ({
      label: instrument.name,
      value: instrument,
    }));
  }, [allInstruments, userInstruments]);

  const selected = options.filter((option) => {
    return selectedInstruments.some((selectedInstrument) => {
      if (typeof selectedInstrument === 'string') {
        return selectedInstrument === option.value.name;
      }
      return selectedInstrument.name === option.value.name;
    });
  });

  return (
    <ReactSelect
      options={options}
      isMulti
      value={selected}
      isClearable
      onChange={(selected) => {
        if (selected) {
          onChange(selected.map((option) => option.value));
        } else {
          onChange([]);
        }
      }}
      placeholder="Select beamlines..."
    />
  );
}
