import { SideNavMetadataFilter } from 'components/filters/metadata';
import { SearchBar } from 'components/inputs/SearchBar';
import { SideNavFilter } from 'components/utils/SideNavElement';
import { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';

export function GroupBySampleFilter({
  investigationId,
  sampleChecked,
  setSampleChecked,
  datasetSearch,
  setDatasetSearch,
  filters,
  setFilters,
}: {
  investigationId: number;
  sampleChecked: string;
  setSampleChecked: (sampleChecked: string) => void;
  datasetSearch?: string;
  setDatasetSearch: (search: string) => void;
  filters?: string;
  setFilters?: (newFilters?: string) => void;
}) {
  const [localFilters, updateLocalFilters] = useState<string | undefined>(
    filters,
  );

  //apply filters after 500ms without change
  useEffect(() => {
    const timeout = setTimeout(() => {
      if (setFilters) {
        setFilters(localFilters);
      }
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [localFilters]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    updateLocalFilters(filters);
  }, [filters]);

  return (
    <>
      <SideNavFilter label={'Group by'}>
        <Form.Check
          type="switch"
          id="custom-switch"
          checked={sampleChecked === 'true'}
          label={'Samples'}
          onChange={(e) => {
            setSampleChecked(e.currentTarget.checked.toString());
          }}
        />
      </SideNavFilter>
      {sampleChecked === 'false' && (
        <>
          <SideNavFilter label={'Search dataset'}>
            <SearchBar
              onUpdate={(value) => setDatasetSearch(value || '')}
              value={datasetSearch}
              placeholder="name, ..."
            />
          </SideNavFilter>
          {setFilters && (
            <SideNavMetadataFilter
              filterDefinition={{
                label: 'Technique',
                parameter: 'definition',
                type: 'select-multi',
                doPrettyPrint: true,
              }}
              state={{
                currentFilters: localFilters || '',
                setCurrentFilters: updateLocalFilters,
                investigationId: investigationId.toString(),
              }}
            />
          )}
        </>
      )}
    </>
  );
}
