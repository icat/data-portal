import { SideNavElement, SideNavFilter } from 'components/utils/SideNavElement';
import { SampleSelector } from 'components/sample/SampleSelector';
import type { Investigation } from '@edata-portal/icat-plus-api';
import { Suspense } from 'react';
import { Col } from 'react-bootstrap';
import { GroupBySampleFilter } from 'components/filters/GroupBySampleFilter';

export function GenericDatasetsFilter({
  investigation,
  setSampleId,
  selectedSampleId,
  sampleChecked,
  setSampleChecked,
  datasetSearch,
  setDatasetSearch,
  filters,
  setFilters,
}: {
  investigation: Investigation;
  setSampleId: (sampleId?: string) => void;
  selectedSampleId?: number;
  sampleChecked: string;
  setSampleChecked: (sampleChecked: string) => void;
  datasetSearch?: string;
  setDatasetSearch: (search: string) => void;
  filters?: string;
  setFilters?: (newFilters?: string) => void;
}) {
  return (
    <SideNavElement label="Datasets">
      <Col>
        <SideNavFilter label={'Filter by sample'}>
          <Suspense fallback={<small>Loading samples...</small>}>
            <SampleSelector
              investigation={investigation}
              selectedSampleId={selectedSampleId}
              updateSelectedSample={setSampleId}
            />
          </Suspense>
        </SideNavFilter>
        <GroupBySampleFilter
          filters={filters}
          setFilters={setFilters}
          investigationId={investigation.id}
          sampleChecked={sampleChecked}
          setSampleChecked={setSampleChecked}
          datasetSearch={datasetSearch}
          setDatasetSearch={setDatasetSearch}
        ></GroupBySampleFilter>
      </Col>
    </SideNavElement>
  );
}
