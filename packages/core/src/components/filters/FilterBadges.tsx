import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  SAMPLE_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import { prettyPrint } from 'helpers';
import { Suspense } from 'react';
import { Badge, Col, Row } from 'react-bootstrap';

export type FilterBadgeItem = {
  name: string;
  value?: string;
  onRemove: () => void;
  hide?: boolean;
};

export function FilterBadges({
  filters,
  centered = false,
  gap = 2,
}: {
  filters: FilterBadgeItem[];
  centered?: boolean;
  gap?: number;
}) {
  return (
    <Row className={'g-' + gap + (centered ? ' justify-content-center' : '')}>
      {filters
        .filter((filter) => !filter.hide)
        .map((filter) => {
          return (
            <Col key={JSON.stringify(filter)} xs={'auto'}>
              <FilterBadge
                param={filter.name}
                value={filter.value}
                onRemove={filter.onRemove}
              />
            </Col>
          );
        })}
    </Row>
  );
}

function FilterBadge({
  param,
  value,
  onRemove,
}: {
  param: string;
  value?: string;
  onRemove: () => void;
}) {
  return (
    <Badge
      bg={'info'}
      onClick={(e) => {
        onRemove();
        e.stopPropagation();
      }}
      style={{
        cursor: 'pointer',
        fontSize: '0.9em',
      }}
    >
      {formatLabel(param, value)}

      <FontAwesomeIcon className="ms-2" icon={faTimes} />
    </Badge>
  );
}

function formatLabel(param: string, value?: string): string | JSX.Element {
  const formatedParam = formatParam(param);

  if (!value) {
    return formatedParam;
  }

  if (param === 'sampleId') {
    return (
      <Suspense fallback="...">
        <SampleFilter id={value} />
      </Suspense>
    );
  }

  return `${formatedParam}: ${value}`;
}

function formatParam(param: string): string {
  return prettyPrint(param);
}

function SampleFilter({ id }: { id: string }) {
  const samples = useGetEndpoint({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: {
      sampleIds: id,
    },
    default: [],
  });

  const name = samples[0]?.name || 'unknown';

  return <>Sample: {name}</>;
}
