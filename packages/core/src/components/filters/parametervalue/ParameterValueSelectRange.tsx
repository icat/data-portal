import {
  DATASET_PARAMETER_VALUE_ENDPOINT,
  DatasetParameterValuesResult,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import type { ParameterValueSelectRangeProps } from 'components/filters/parametervalue/ParameterValueSelect';
import { RangeSelector } from 'components/utils';
import { useMemo } from 'react';

export function ParameterValueSelectRange(
  props: ParameterValueSelectRangeProps,
) {
  const options = useGetEndpoint({
    endpoint: DATASET_PARAMETER_VALUE_ENDPOINT,
    params: {
      investigationId: props.investigationId,
      name: props.parameter,
      ...(props.datasetType && props.datasetType !== 'any'
        ? { datasetType: props.datasetType }
        : {}),
    },
    default: [] as DatasetParameterValuesResult[],
  });

  const { min, max } = useMemo(() => {
    const values = options
      .flatMap((o) => o.values)
      .map((v) => parseFloat(v))
      .filter((v) => !isNaN(v));
    const min = Math.min(
      ...values,
      ...(props.value?.min !== undefined ? [props.value.min] : []),
    );
    const max = Math.max(
      ...values,
      ...(props.value?.max !== undefined ? [props.value.max] : []),
    );
    return { min: min, max };
  }, [options, props.value]);

  if (isNaN(min) || isNaN(max)) return null;

  return (
    <RangeSelector
      min={min}
      max={max}
      step={props.value?.step}
      selectedMax={props.value?.max || max}
      selectedMin={props.value?.min || min}
      setSelectedMax={(max) => {
        const newValues = { min: props.value?.min || min, max };
        props.onChange(newValues);
      }}
      setSelectedMin={(min) => {
        const newValues = { min, max: props.value?.max || max };
        props.onChange(newValues);
      }}
    />
  );
}
