import {
  DATASET_PARAMETER_VALUE_ENDPOINT,
  DatasetParameterValuesResult,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import type { ParameterValueSelectListProps } from 'components/filters/parametervalue/ParameterValueSelect';
import { prettyPrint } from 'helpers';
import { useMemo } from 'react';
import { Form } from 'react-bootstrap';
import ReactSelect from 'react-select';

const MAX_LENGTH_INLINE = 5;

export function ParameterValueSelectList(props: ParameterValueSelectListProps) {
  const options = useGetEndpoint({
    endpoint: DATASET_PARAMETER_VALUE_ENDPOINT,
    params: {
      investigationId: props.investigationId,
      name: props.parameter,
      ...(props.datasetType && props.datasetType !== 'any'
        ? { datasetType: props.datasetType }
        : {}),
    },
    default: [] as DatasetParameterValuesResult[],
  });

  const optionsValues = useMemo(() => {
    const mappedOptions = options.flatMap((o) => o.values);
    if (!props.value) return mappedOptions;

    return props.isMulti
      ? [...props.value, ...mappedOptions]
      : [props.value, ...mappedOptions];
  }, [options, props.isMulti, props.value]);

  const distinctOptions = optionsValues
    .filter((v): v is string => v !== undefined)
    .filter((v): v is string => v !== null)
    .filter((v, i, a) => a.indexOf(v) === i)
    .sort();

  if (!distinctOptions.length) return null;

  if (distinctOptions.length <= MAX_LENGTH_INLINE)
    return (
      <>
        {distinctOptions.map((option) => (
          <Form.Check
            key={option}
            type={props.isMulti ? 'checkbox' : 'radio'}
            label={props.doPrettyPrint ? prettyPrint(option) : option}
            checked={
              !!props.value &&
              (props.isMulti
                ? props.value.includes(option)
                : props.value === option)
            }
            onChange={(e) => {
              if (props.isMulti) {
                const current = props.value || [];
                if (e.target.checked) {
                  props.onChange([...current, option]);
                } else {
                  props.onChange(current.filter((v2) => v2 !== option));
                }
              } else {
                if (e.target.checked) {
                  props.onChange(option);
                }
              }
            }}
          />
        ))}
      </>
    );

  return (
    <ReactSelect
      isMulti={props.isMulti}
      options={distinctOptions.map((v) => ({
        value: v,
        label: props.doPrettyPrint ? prettyPrint(v) : v,
      }))}
      value={
        props.isMulti
          ? (props.value || []).map((v) => ({
              value: v,
              label: props.doPrettyPrint ? prettyPrint(v) : v,
            }))
          : props.value
            ? {
                value: props.value,
                label: props.doPrettyPrint
                  ? prettyPrint(props.value)
                  : props.value,
              }
            : null
      }
      onChange={(selected) => {
        if (selected === null || selected === undefined) {
          props.onChange(undefined);
          return;
        }
        if (props.isMulti && selected instanceof Array) {
          props.onChange(selected.map((v) => v.value));
        } else if (!props.isMulti && !(selected instanceof Array)) {
          props.onChange(selected.value);
        } else {
          props.onChange(undefined);
        }
      }}
    />
  );
}
