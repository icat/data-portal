import { ParameterValueSelectList } from 'components/filters/parametervalue/ParameterValueSelectList';
import { ParameterValueSelectRange } from 'components/filters/parametervalue/ParameterValueSelectRange';
import { Suspense } from 'react';

export interface AbstractParameterValueSelectProps<VALUETYPE> {
  parameter: string;
  investigationId: string;
  datasetType?: 'acquisition' | 'processed' | 'any';
  value: VALUETYPE | undefined;
  onChange: (value: VALUETYPE | undefined) => void;
  doPrettyPrint: boolean;
}

export interface ParameterValueSelectListSingleProps
  extends AbstractParameterValueSelectProps<string> {
  isMulti: false;
  type: 'list';
}

export interface ParameterValueSelectListMultiProps
  extends AbstractParameterValueSelectProps<string[]> {
  isMulti: true;
  type: 'list';
}

export type ParameterValueSelectListProps =
  | ParameterValueSelectListSingleProps
  | ParameterValueSelectListMultiProps;

export interface RangeValue {
  min: number;
  max: number;
  step?: number;
}

export interface ParameterValueSelectRangeProps
  extends AbstractParameterValueSelectProps<RangeValue> {
  type: 'range';
}

export type ParameterValueSelectProps =
  | ParameterValueSelectListProps
  | ParameterValueSelectRangeProps;

/**
 * Allow selection of parameter values from the list of available values.
 * @param parameter The name of the dataset parameter to select values for
 * @param investigationId The investigation to search for values in
 * @param isMulti Whether to allow multiple values to be selected
 * @param value The current value(s) of the parameter
 * @param onChange The function to call when the value(s) change
 * @param datasetType Whether to allow selection of raw, processed or any dataset
 */
export function ParameterValueSelect(props: ParameterValueSelectProps) {
  return (
    <div className="w-100">
      <Suspense fallback={'Loading ...'}>
        {props.type === 'list' ? <ParameterValueSelectList {...props} /> : null}
        {props.type === 'range' ? (
          <ParameterValueSelectRange {...props} />
        ) : null}
      </Suspense>
    </div>
  );
}
