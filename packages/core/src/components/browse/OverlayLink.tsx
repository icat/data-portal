import type { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Link } from 'react-router-dom';

/**
 * Link component with an overlay that will show a tooltip. This component can open the ref in a external tab with openOnNewTab = true
 * @param param0
 * @returns
 */
export function OverlayLink({
  link,
  tooltipText,
  icon,
  openOnNewTab = false,
}: {
  link: string;
  tooltipText: string;
  icon: IconDefinition;
  openOnNewTab?: boolean;
}) {
  return (
    <OverlayTrigger overlay={<Tooltip>{tooltipText}</Tooltip>}>
      <Link
        to={link}
        onClick={(e) => {
          e.stopPropagation();
        }}
        target={openOnNewTab ? '_blank' : undefined}
        rel={openOnNewTab ? 'noopener noreferrer' : undefined}
      >
        <Button style={{ border: 'none' }} size="sm" variant="outline-info">
          <FontAwesomeIcon icon={icon} />
        </Button>
      </Link>
    </OverlayTrigger>
  );
}
