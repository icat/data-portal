import { faAnglesLeft, faAnglesRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { LoadingIndicator } from 'components/LoadingIndicator';
import { SideNavContext, SideNavNode, UpdateSideNavContext } from 'context';
import { immutableArray } from 'helpers';
import { useBreakpointValue } from 'hooks';

import React from 'react';
import { Button, Container } from 'react-bootstrap';

function SideNav({
  navContent,
  panelContent,
}: {
  navContent: React.ReactNode;
  panelContent: React.ReactNode;
}) {
  const mobileView = useBreakpointValue({ xs: true, lg: false });

  const [show, setShow] = React.useState(mobileView ? false : true);

  const expandItem = (
    <Container
      className="bg-light"
      style={{
        display: mobileView || !show ? 'flex' : 'none',
        position: 'relative',
        width: 30,
        height: '100%',
        borderRight: '1px solid #ccc',
        alignItems: 'center',
        justifyContent: 'center',
        cursor: 'pointer',
        overflow: 'hidden',
      }}
      fluid
      onClick={() => {
        setShow(!show);
      }}
    >
      <FontAwesomeIcon icon={faAnglesRight} />
    </Container>
  );

  const navItem = (
    <Container
      className="bg-light pt-2 flex-column justify-content-between"
      style={{
        display: show ? 'flex' : 'none',
        position: mobileView ? 'absolute' : 'relative',
        width: '20rem',
        height: '100%',
        overflow: 'auto',
        borderRight: '1px solid #ccc',
        left: 0,
        zIndex: 2,
      }}
      fluid
    >
      <div>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Button
            variant="icon"
            style={{
              width: '100%',
            }}
            onClick={() => {
              setShow(!show);
            }}
            size="sm"
          >
            <FontAwesomeIcon icon={faAnglesLeft} />
          </Button>
          <LoadingIndicator />
        </div>
        {navContent}
      </div>
    </Container>
  );

  return (
    <div
      className="bg-white"
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        display: 'flex',
        flexDirection: 'row-reverse',
        overflow: 'hidden',
      }}
    >
      <Container
        style={{
          position: 'relative',
          width: '100%',
          marginTop: 20,
          overflow: 'auto',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
        fluid
      >
        {panelContent}
      </Container>
      {expandItem}
      {navItem}
    </div>
  );
}

export function SideNavRenderer({ children }: { children: React.ReactNode }) {
  const { nodes } = React.useContext(SideNavContext);
  if (nodes.length === 0) {
    return <>{children}</>;
  }

  return (
    <SideNav
      navContent={
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            gap: 5,
          }}
        >
          {immutableArray(nodes)
            .sort((a, b) => a.position - b.position)
            .map((node) => (
              <div
                key={node.id}
                ref={(ref) => {
                  if (ref && node.onRef) {
                    node.onRef(ref);
                  }
                }}
              />
            ))
            .toArray()}
        </div>
      }
      panelContent={children}
    />
  );
}

function SideNavContextProvider({ children }: { children: React.ReactNode }) {
  const [nodes, setNodes] = React.useState<SideNavNode[]>([]);
  const addNode = React.useCallback(
    (node: SideNavNode) => {
      setNodes((nodes) => [...nodes, node]);
    },
    [setNodes],
  );
  const removeNode = React.useCallback(
    (node: SideNavNode) => {
      setNodes((nodes) => nodes.filter((n) => n.id !== node.id));
    },
    [setNodes],
  );
  const value = React.useMemo(() => {
    return {
      nodes,
      addNode,
      removeNode,
    };
  }, [addNode, nodes, removeNode]);

  return (
    <SideNavContext.Provider value={value}>{children}</SideNavContext.Provider>
  );
}

function UpdateSideNavContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const { addNode, removeNode } = React.useContext(SideNavContext);

  const value = React.useMemo(() => {
    return {
      addNode,
      removeNode,
    };
  }, [addNode, removeNode]);

  return (
    <UpdateSideNavContext.Provider value={value}>
      {children}
    </UpdateSideNavContext.Provider>
  );
}

export function SideNavProvider({ children }: { children: React.ReactNode }) {
  return (
    <SideNavContextProvider>
      <UpdateSideNavContextProvider>{children}</UpdateSideNavContextProvider>
    </SideNavContextProvider>
  );
}
