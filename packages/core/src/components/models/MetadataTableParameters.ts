export type MetadataTableParameter = {
  caption: string | JSX.Element;
  parameterName?: string;
  value?: string | number;
  digits?: number;
  units?: string;
  component?: JSX.Element;
  formatter?: (
    value: string | number,
    unit: string | undefined,
  ) => string | JSX.Element;
};

export type MetadataTableParameters = Array<MetadataTableParameter>;
