import {
  faClipboard,
  faClipboardCheck,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'components/utils/Button';
import React, { useCallback, useMemo } from 'react';
import { OverlayTrigger, Tooltip, Card } from 'react-bootstrap';

export type CopyValueProps = {
  value: string;
} & (
  | {
      type: 'value' | 'icon';
      label?: string;
    }
  | {
      type: 'button';
      label: string;
    }
);

export function CopyValue({ value, ...props }: CopyValueProps) {
  const [copied, setCopied] = React.useState(false);
  React.useEffect(() => {
    if (copied) {
      const timeout = setTimeout(() => setCopied(false), 5000);
      return () => clearTimeout(timeout);
    }
    return undefined;
  }, [copied]);

  const icon = useMemo(
    () => (copied ? faClipboardCheck : faClipboard),
    [copied],
  );

  const onClick = useCallback(
    (e: React.MouseEvent) => {
      navigator.clipboard.writeText(value);
      setCopied(true);
    },
    [value],
  );

  const content = useMemo(() => {
    if (props.type === 'button') {
      return (
        <Button size="sm" variant={'primary'} onClick={onClick}>
          <FontAwesomeIcon icon={icon} className={'me-2'} />
          Copy {props.label}
        </Button>
      );
    }

    if (props.type === 'icon') {
      return (
        <Button size="sm" variant={'link'} onClick={onClick}>
          <FontAwesomeIcon icon={copied ? faClipboardCheck : faClipboard} />
        </Button>
      );
    }

    return (
      <Card
        onClick={onClick}
        style={{
          padding: 5,
          display: 'flex',
          alignItems: 'center',
          gap: '0.5rem',
          flexDirection: 'row',
          cursor: 'pointer',
        }}
      >
        <FontAwesomeIcon size="lg" icon={icon} />
        <span
          style={{
            wordBreak: 'break-all',
          }}
        >
          {value}
        </span>
      </Card>
    );
  }, [copied, icon, onClick, props, value]);

  if (props.type === 'button') return content;

  return (
    <OverlayTrigger
      placement="auto"
      overlay={
        <Tooltip key={'CopyValueTooltip'}>
          {copied
            ? 'Copied'
            : `Copy ${props.label ? props.label + ' ' : ''}to clipboard`}
        </Tooltip>
      }
    >
      <div key={'CopyValueContent'}>{content}</div>
    </OverlayTrigger>
  );
}
