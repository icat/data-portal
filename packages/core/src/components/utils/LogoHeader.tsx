/**
 * This type defines an object used to display the logo in the pagination bar
 * src will be the href of the image to be displayed
 * alt is the alternative text
 * url when it exists will be a link
 */
export type LogoHeaderType = {
  src: string;
  alt: string;
  url?: string;
};

/**
 * Component: LogoHeader
 * Renders a logo image with an optional hyperlink.
 *
 * @param {string} props.header.src - Image source URL.
 * @param {string} props.header.alt - Alternative text for the image.
 * @param {string} [props.header.url] - Optional hyperlink URL.
 */
export function LogoHeader({ header }: { header: LogoHeaderType }) {
  const { src, alt, url } = header;

  // Create an image element
  const img = <img src={src} alt={alt} />;

  return (
    <>
      {/* If URL is provided, wrap image in an anchor tag */}
      {url && (
        <a href={url} target="_blank" rel="noreferrer">
          {img}
        </a>
      )}
      {/* If no URL, render only the image */}
      {!url && { img }}
    </>
  );
}
