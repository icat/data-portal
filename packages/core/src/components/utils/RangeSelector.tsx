import { toCSSGradient, type ColorGradient } from 'helpers';
import { useCallback, useEffect, useRef, useState } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

const dragImg = document.createElement('img');
dragImg.src =
  'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

export function RangeSelector({
  min,
  max,
  step,
  selectedMin,
  selectedMax,
  setSelectedMin,
  setSelectedMax,
  backgroundGradient,
  height = 10,
}: {
  min: number;
  max: number;
  step?: number;
  selectedMin: number;
  selectedMax: number;
  setSelectedMin: (value: number) => void;
  setSelectedMax: (value: number) => void;
  backgroundGradient?: ColorGradient;
  height?: number;
}) {
  const range = max - min;
  const minPercent = ((selectedMin - min) / range) * 100;
  const maxPercent = ((selectedMax - min) / range) * 100;

  const [moving, setMoving] = useState<'min' | 'max' | null>(null);

  const [minText, setMinText] = useState(selectedMin.toString());
  const [maxText, setMaxText] = useState(selectedMax.toString());

  const sliderRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (selectedMin === min) {
      setMinText(selectedMin.toString());
    }
  }, [selectedMin, min]);
  useEffect(() => {
    if (selectedMax === max) {
      setMaxText(selectedMax.toString());
    }
  }, [selectedMax, max]);

  const isValid = useCallback(
    (value: number, type: 'min' | 'max') => {
      return (
        !isNaN(value) &&
        value >= min &&
        value <= max &&
        (type === 'min' ? value <= selectedMax : value >= selectedMin)
      );
    },
    [max, min, selectedMax, selectedMin],
  );

  const handleMove = useCallback(
    (e: MouseEvent | TouchEvent) => {
      e.preventDefault();
      if (!moving) return;
      const rect = sliderRef.current?.getBoundingClientRect();
      if (!rect) return;
      const clientX = 'clientX' in e ? e.clientX : e.touches[0].clientX;
      const x = clientX - rect.left;
      const percent = (x / rect.width) * 100;
      const correctedPercent = percent < 0 ? 0 : percent > 100 ? 100 : percent;

      function getValue() {
        const v = min + (range * correctedPercent) / 100;
        if (v === min || v === max) return v;
        const steppedV = step ? Math.round(v / step) * step : v;
        const fixedV = parseFloat(steppedV.toFixed(2));
        return fixedV;
      }

      const value = getValue();

      if (moving === 'min') {
        if (isValid(value, 'min') && value !== selectedMin) {
          setMinText(value.toString());
          setSelectedMin(value);
        }
      }
      if (moving === 'max') {
        if (isValid(value, 'max') && value !== selectedMax) {
          setMaxText(value.toString());
          setSelectedMax(value);
        }
      }
    },
    [
      isValid,
      max,
      min,
      moving,
      range,
      selectedMax,
      selectedMin,
      setSelectedMax,
      setSelectedMin,
      step,
    ],
  );

  const isMinTextValid = isValid(parseFloat(minText), 'min');
  const isMaxTextValid = isValid(parseFloat(maxText), 'max');

  const handleSetFromText = (type: 'min' | 'max') => {
    const value = type === 'min' ? parseFloat(minText) : parseFloat(maxText);
    if (isValid(value, type)) {
      if (type === 'min') {
        setSelectedMin(value);
      } else {
        setSelectedMax(value);
      }
    }
  };

  useEffect(() => {
    const mouseUpListener = () => {
      setMoving(null);
    };
    const mouseMoveListener = (e: MouseEvent | TouchEvent) => {
      handleMove(e);
    };

    const setupListeners = () => {
      //desktop
      document.body.addEventListener('dragend', mouseUpListener);
      document.body.addEventListener('dragover', mouseMoveListener);
      //mobile
      document.body.addEventListener('touchmove', mouseMoveListener);
      document.body.addEventListener('touchend', mouseUpListener);
    };
    const clearListeners = () => {
      //desktop
      document.body.removeEventListener('dragend', mouseUpListener);
      document.body.removeEventListener('dragover', mouseMoveListener);
      //mobile
      document.body.removeEventListener('touchmove', mouseMoveListener);
      document.body.removeEventListener('touchend', mouseUpListener);
    };

    if (moving) {
      setupListeners();
    } else {
      clearListeners();
    }
    return clearListeners;
  }, [handleMove, moving]);

  return (
    <div test-id="range-selector">
      <Container fluid>
        <Row>
          <Col
            xs={'auto'}
            style={{
              padding: 0,
              height: height,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            {min}
          </Col>
          <Col>
            <div
              style={{
                position: 'relative',
                width: '100%',
                height: height,
                borderRadius: 5,
                backgroundColor: '#a9a9a9',
              }}
              ref={sliderRef}
            >
              {backgroundGradient && (
                <BackgroundColor
                  min={min}
                  max={max}
                  gradient={backgroundGradient}
                />
              )}
              {/* cursors */}
              <div
                test-id="range-selector-cursor-min"
                style={{
                  position: 'absolute',
                  top: '50%',
                  left: `${minPercent}%`,
                  height: height + 25,
                  width: 35,
                  padding: 10,
                  transform: 'translate(-50%, -50%)',
                  cursor: 'grab',
                }}
                draggable="true"
                onDragStart={(e) => {
                  //desktop
                  e.dataTransfer.setDragImage(dragImg, 0, 0);
                  setMoving('min');
                }}
                onTouchStart={() => {
                  //mobile
                  setMoving('min');
                }}
              >
                <div
                  style={{
                    borderRadius: 15,
                    height: '100%',
                    width: '100%',
                    backgroundColor: '#007bff',
                  }}
                />
              </div>
              <div
                test-id="range-selector-cursor-max"
                style={{
                  position: 'absolute',
                  top: '50%',
                  left: `${maxPercent}%`,
                  height: height + 25,
                  width: 35,
                  padding: 10,
                  transform: 'translate(-50%, -50%)',
                  cursor: 'grab',
                }}
                draggable
                onDragStart={(e) => {
                  //desktop
                  e.dataTransfer.setDragImage(dragImg, 0, 0);
                  setMoving('max');
                }}
                onTouchStart={() => {
                  //mobile
                  setMoving('max');
                }}
              >
                <div
                  style={{
                    borderRadius: 15,
                    height: '100%',
                    width: '100%',
                    backgroundColor: '#007bff',
                  }}
                />
              </div>
            </div>

            <div
              // values
              style={{
                position: 'relative',
                width: '100%',
                height: 35,
              }}
            >
              <div
                style={{
                  position: 'absolute',
                  bottom: 0,
                  left: `${minPercent}%`,
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  transform:
                    maxPercent - minPercent <= 10
                      ? 'translate(-100%, 0%)'
                      : 'translate(-50%, 0%)',
                }}
              >
                <Form.Control
                  size="sm"
                  className="text-center"
                  style={{
                    width: `${minText.toString().length + 3}ch`,
                  }}
                  value={minText}
                  onInput={(e) => {
                    setMinText(
                      e.currentTarget.value.replaceAll(/[^0-9.-]/g, ''),
                    );
                  }}
                />
                {minText !== selectedMin.toString() && (
                  <>
                    {!isMinTextValid ? (
                      <span style={{ color: 'red' }}>Invalid</span>
                    ) : (
                      <Button
                        style={{ padding: 0 }}
                        variant="link"
                        onClick={() => {
                          handleSetFromText('min');
                        }}
                      >
                        Set
                      </Button>
                    )}
                  </>
                )}
              </div>
              <div
                style={{
                  position: 'absolute',
                  bottom: 0,
                  left: `${maxPercent}%`,
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  transform:
                    maxPercent - minPercent <= 10
                      ? 'translate(0%, 0%)'
                      : 'translate(-50%, 0%)',
                }}
              >
                <Form.Control
                  size="sm"
                  className="text-center"
                  style={{
                    width: `${maxText.toString().length + 3}ch`,
                  }}
                  value={maxText}
                  onInput={(e) => {
                    setMaxText(
                      e.currentTarget.value.replaceAll(/[^0-9.-]/g, ''),
                    );
                  }}
                />
                {maxText !== selectedMax.toString() && (
                  <>
                    {!isMaxTextValid ? (
                      <span style={{ color: 'red' }}>Invalid</span>
                    ) : (
                      <Button
                        style={{ padding: 0 }}
                        variant="link"
                        onClick={() => {
                          handleSetFromText('max');
                        }}
                      >
                        Set
                      </Button>
                    )}
                  </>
                )}
              </div>
            </div>
          </Col>
          <Col
            xs={'auto'}
            style={{
              padding: 0,
              height: height,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            {max}
          </Col>
        </Row>
      </Container>
    </div>
  );
}

function BackgroundColor({
  min,
  max,
  gradient,
}: {
  min: number;
  max: number;
  gradient: ColorGradient;
}) {
  return (
    <div
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        height: '100%',
        width: '100%',
        overflow: 'hidden',
        border: '1px solid black',
        borderRadius: 5,
        backgroundImage: toCSSGradient(gradient, min, max),
      }}
    />
  );
}
