import { getScrollRootFromElementRef } from 'helpers';
import { PropsWithChildren, ReactNode, Suspense, useRef } from 'react';
import { useInView } from 'react-intersection-observer';

export type LazyWrapperType = PropsWithChildren<{
  placeholder?: ReactNode;
  rootMarginTop?: number;
  fullWidth?: boolean;
}> &
  ({ height?: number | string } | { aspectRatio?: string });

export function LazyWrapper({
  children,
  placeholder,
  rootMarginTop = 500, // This is the margin at which the lazy loading is triggered
  fullWidth = true,
  ...props
}: LazyWrapperType) {
  const refForRootSearch = useRef<HTMLDivElement>(null);

  //This allows root margin to work properly when scroll is not on the whole page
  const root = getScrollRootFromElementRef(refForRootSearch);

  const { ref, inView } = useInView({
    rootMargin: `${rootMarginTop}px 0px`,
    triggerOnce: true,
    threshold: 0,
    root: root,
  });

  // Lazy elements need to have a fixed height or an aspect ratio to have
  // consistent height when scrolling list of lazy elements
  const height = 'height' in props ? props.height : undefined;
  const aspectRatio = 'aspectRatio' in props ? props.aspectRatio : undefined;

  return (
    <>
      <div
        ref={refForRootSearch}
        style={{
          display: 'hidden',
        }}
      />
      <div
        ref={ref}
        style={{
          height: height,
          aspectRatio: aspectRatio,
          width: fullWidth ? '100%' : undefined,
        }}
      >
        {inView ? (
          <Suspense fallback={placeholder}>{children}</Suspense>
        ) : (
          placeholder
        )}
      </div>
    </>
  );
}
