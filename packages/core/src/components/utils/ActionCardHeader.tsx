import {
  faChevronDown,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DateType, formatDateToDayAndTime } from 'helpers';
import React from 'react';
import { Badge, Card, Col, Row } from 'react-bootstrap';

const paddingClass = 'p-1 ps-2 pe-2 ';
const rawClassNameHeader = paddingClass + 'bg-dataset-raw';
const processedClassNameHeader = paddingClass + 'bg-dataset-processed';

export function ActionCardHeader({
  expanded,
  setExpanded,
  date,
  isProcessed,
  title,
  leftAlignedItems,
  rightAlignedItems,
  className,
  hideDatasetTitle,
}: {
  expanded?: boolean;
  setExpanded?: (expanded: boolean) => void;
  date: DateType | undefined;
  isProcessed: boolean;
  title: string;
  leftAlignedItems: React.ReactNode | React.ReactNode[];
  rightAlignedItems: React.ReactNode | React.ReactNode[];
  className?: string;
  hideDatasetTitle?: boolean;
}) {
  const left = (
    <Row className="align-items-center g-2">
      {setExpanded && (
        <Col xs={'auto'}>
          <FontAwesomeIcon icon={expanded ? faChevronDown : faChevronRight} />
        </Col>
      )}
      {!hideDatasetTitle && (
        <React.Fragment>
          <Col xs={'auto'}>
            <span className="monospace">{formatDateToDayAndTime(date)}</span>
          </Col>
          {isProcessed && (
            <Col xs={'auto'}>
              <Badge bg="success">Processed</Badge>
            </Col>
          )}
          <Col xs={'auto'}>
            <strong>{title}</strong>
          </Col>
        </React.Fragment>
      )}
      {Array.isArray(leftAlignedItems) ? (
        leftAlignedItems.map((item, i) => (
          <Col key={`col-${i}`} xs={'auto'}>
            {item}
          </Col>
        ))
      ) : (
        <Col xs={'auto'}>{leftAlignedItems}</Col>
      )}
    </Row>
  );

  const right = (
    <Row className="align-items-center g-2 ">
      {Array.isArray(rightAlignedItems) ? (
        rightAlignedItems.map((item, i) => (
          <Col key={i} xs={'auto'}>
            {item}
          </Col>
        ))
      ) : (
        <Col xs={'auto'}>{rightAlignedItems}</Col>
      )}
    </Row>
  );

  return (
    <Card.Header
      className={
        (isProcessed ? processedClassNameHeader : rawClassNameHeader) +
        ' ' +
        (className || '')
      }
      onClick={(e) => {
        if (!setExpanded) return;
        setExpanded(!expanded);
      }}
      style={{
        cursor: setExpanded ? 'pointer' : 'default',
      }}
    >
      <Row className="justify-content-between align-items-center g-0">
        <Col xs={'auto'}> {left}</Col>
        <Col xs={'auto'}> {right}</Col>
      </Row>
    </Card.Header>
  );
}
