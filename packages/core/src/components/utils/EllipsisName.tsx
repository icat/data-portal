import { EllipsisView } from 'components/utils/EllipsisView';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

export function EllipsisName({
  displayName,
  fullName,
  side,
  style,
  className,
}: {
  displayName: string;
  fullName?: string;
  side: 'start' | 'end';
  style?: React.CSSProperties;
  className?: string;
}) {
  return (
    <OverlayTrigger
      delay={{
        show: 500,
        hide: 0,
      }}
      overlay={
        <Tooltip
          id="popover-basic"
          style={{
            whiteSpace: 'normal',
            wordWrap: 'break-word',
          }}
        >
          {fullName || displayName}
        </Tooltip>
      }
      placement="auto"
    >
      <EllipsisView side={side} style={style} className={className}>
        {displayName}
      </EllipsisView>
    </OverlayTrigger>
  );
}
