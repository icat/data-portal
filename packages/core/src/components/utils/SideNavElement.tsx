import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'components/utils/Button';
import { Col, OverlayTrigger, Row, Tooltip } from 'react-bootstrap';

export function SideNavElement({
  label,
  children,
}: {
  label?: string | React.ReactElement;
  children: React.ReactElement | React.ReactElement[];
}) {
  return (
    <div>
      {label && (
        <strong
          className="text-primary"
          style={{
            fontSize: 17,
          }}
        >
          <i>{label}</i>
        </strong>
      )}
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          gap: 5,
        }}
      >
        <div
          style={{
            borderRight: '1px solid grey',
            margin: 4,
          }}
        />
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            gap: 5,
            width: '100%',
          }}
        >
          {children}
        </div>
      </div>
    </div>
  );
}

export function SideNavFilter({
  label,
  children,
  onClear,
  hasValue,
}: {
  label: string | React.ReactElement;
  children: React.ReactElement;
  onClear?: () => void;
  hasValue?: boolean;
}) {
  return (
    <>
      <Row>
        <Col xs={'auto'}>
          {onClear && (
            <Button
              disabled={!hasValue}
              variant={'link'}
              onClick={onClear}
              className="m-0 me-1 p-0"
            >
              <OverlayTrigger
                overlay={
                  <Tooltip id="tooltip-clear-filter">Clear this filter</Tooltip>
                }
                container={document.body}
              >
                <FontAwesomeIcon id="clear-filter-icon" icon={faTimesCircle} />
              </OverlayTrigger>
            </Button>
          )}
          <strong style={{ verticalAlign: 'middle' }}>{label}:</strong>
        </Col>
      </Row>
      <Row className="mb-2">
        <Col>{children}</Col>
      </Row>
    </>
  );
}
