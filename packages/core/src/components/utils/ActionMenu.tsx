import { IconDefinition, faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { Button, Nav, OverlayTrigger, Popover, Tooltip } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export type ActionMenuItem =
  | {
      link?: string;
      label: string;
      icon?: IconDefinition;
      onClick?: () => void;
      hide?: boolean;
      disabled?: boolean;
      active?: boolean;
      tooltip?: string;
      align?: 'left' | 'right' | 'center';
    }
  | {
      node: React.ReactNode;
      hideOnClick?: boolean;
    };

export function ActionMenu({ items }: { items: ActionMenuItem[] }) {
  const [show, setShow] = useState(false);

  return (
    <OverlayTrigger
      trigger={['click']}
      rootClose
      placement="auto"
      show={show}
      onToggle={() => setShow(!show)}
      overlay={
        <Popover
          key={'ActionContextMenu'}
          style={{
            maxWidth: '50vw',
            minWidth: 'fit-content',
            zIndex: Number.MAX_SAFE_INTEGER,
          }}
        >
          <Popover.Body>
            <Nav
              variant="underline"
              className="col"
              style={{
                display: 'flex',
                flexDirection: 'column',
                gap: 5,
              }}
            >
              {items.map((item, i) => {
                if ('node' in item)
                  return (
                    <div
                      key={i}
                      onClick={() => {
                        if (item.hideOnClick) {
                          setShow(false);
                        }
                      }}
                      style={{
                        display: 'contents',
                      }}
                    >
                      {item.node}
                    </div>
                  );

                if (item.hide) {
                  return null;
                }

                const button = (
                  <Button
                    className="w-100"
                    variant="outline-primary"
                    size="sm"
                    disabled={item.disabled}
                    active={item.active}
                    onClick={(e) => {
                      setShow(false);
                      if (item.onClick) {
                        item.onClick();
                      }
                      e.stopPropagation();
                    }}
                    style={{
                      textAlign: item.align,
                    }}
                  >
                    {item.icon ? (
                      <FontAwesomeIcon icon={item.icon} className="me-1" />
                    ) : null}
                    {item.label}
                  </Button>
                );

                const content = (
                  <Nav.Item
                    key={item.label}
                    onClick={() => {
                      setShow(false);
                      if (item.onClick) {
                        item.onClick();
                      }
                    }}
                  >
                    {item.link ? <Link to={item.link}>{button}</Link> : button}
                  </Nav.Item>
                );

                if (!item.tooltip) return content;
                return (
                  <OverlayTrigger
                    key={item.label}
                    placement="auto"
                    overlay={
                      <Tooltip id={`tooltip-${item.label}`}>
                        {item.tooltip}
                      </Tooltip>
                    }
                  >
                    {content}
                  </OverlayTrigger>
                );
              })}
            </Nav>
          </Popover.Body>
        </Popover>
      }
    >
      <Button
        key={'ActionMenuButton'}
        variant={'link text-reset'}
        style={{
          padding: '0px 5px',
        }}
        onClick={(e) => {
          e.stopPropagation();
          document.body.click();
        }}
      >
        <FontAwesomeIcon icon={faEllipsisH} />
      </Button>
    </OverlayTrigger>
  );
}
