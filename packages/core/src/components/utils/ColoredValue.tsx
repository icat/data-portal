import { getGradientValue, toCSSColor, type ColorGradient } from 'helpers';

export function ColoredValue({
  value,
  gradient,
}: {
  value: number | string;
  gradient: ColorGradient;
}) {
  const numValue = Number(value);
  if (isNaN(numValue)) return value;

  return (
    <span
      className="text-center"
      style={{
        backgroundColor: toCSSColor(getGradientValue(gradient, numValue), 0.3),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        gap: 4,
        padding: '0 2px',
      }}
    >
      {value}
    </span>
  );
}
