import React from 'react'; // eslint-disable-line @typescript-eslint/no-unused-vars

export function ORCID({
  orcid,
  isCompactView,
}: {
  orcid: string;
  isCompactView: boolean;
}) {
  return orcid ? (
    <a
      href={`https://orcid.org/${orcid}`}
      target="_blank"
      rel="noopener noreferrer"
      style={{ marginRight: '2px', marginLeft: '4px' }}
    >
      <img
        alt="ORCID logo"
        src="/images/orcid_16x16.png"
        width="16"
        height="16"
      />

      {isCompactView && orcid}
    </a>
  ) : null;
}
