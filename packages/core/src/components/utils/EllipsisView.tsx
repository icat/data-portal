export function EllipsisView({
  children,
  side = 'end',
  style,
  className,
}: {
  children: React.ReactNode | React.ReactNode[];
  side: 'start' | 'end';
  style?: React.CSSProperties;
  className?: string;
}) {
  return (
    <div
      style={{
        direction: side === 'end' ? 'ltr' : 'rtl',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
      }}
    >
      <span
        style={{
          direction: 'ltr',
          unicodeBidi: 'bidi-override',
          ...style,
        }}
        className={className}
      >
        {children}
      </span>
    </div>
  );
}
