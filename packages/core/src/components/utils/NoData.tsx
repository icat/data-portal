import { Alert } from 'react-bootstrap';

export function NoData() {
  return <Alert variant="info">No data has been found.</Alert>;
}
