import { type ButtonProps, Button as RBButton } from 'react-bootstrap';

//Button not working for some reason reported but unsolved here https://github.com/react-bootstrap/react-bootstrap/issues/6283
// see https://gitlab.esrf.fr/icat/data-portal/-/merge_requests/466
export function Button(props: ButtonProps) {
  return <RBButton as="button" {...props} />;
}
