export function Progress({
  valuePercent,
  color = 'lightblue',
  text,
}: {
  valuePercent: number;
  color?: string;
  text?: string | React.ReactElement;
}) {
  const textContent = text ? text : valuePercent.toFixed(1) + '%';
  return (
    <div
      className="border rounded"
      style={{
        position: 'relative',
        minWidth: 70,
        height: 20,
        overflow: 'hidden',
      }}
    >
      <div
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          bottom: 0,
          width: valuePercent + '%',
          backgroundColor: color,
        }}
      />
      <span
        style={{
          position: 'absolute',
          left: '50%',
          top: '50%',
          transform: 'translate(-50%, -50%)',
        }}
        className="text-black"
      >
        {textContent}
      </span>
    </div>
  );
}
