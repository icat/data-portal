import type { CSSProperties } from 'react';
import { ProgressBar, Spinner } from 'react-bootstrap';

export function Loading({
  css,
  progress,
}: {
  css?: CSSProperties;
  progress?: number;
}) {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: '2rem',
        ...css,
      }}
    >
      {progress !== undefined ? (
        <ProgressBar
          className="w-100"
          animated
          variant="info"
          now={progress * 100}
          label={`${Math.round(progress * 100)}%`}
        />
      ) : (
        <Spinner />
      )}
      <br />
      <i>Loading...</i>
    </div>
  );
}
