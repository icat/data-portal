import { useConfig } from 'context';
import { Alert } from 'react-bootstrap';

export function GlobusMessage() {
  const config = useConfig();

  if (!config.ui.globus.messageAlert.enabled) return null;

  return (
    <Alert variant="warning">
      <strong>
        <div
          dangerouslySetInnerHTML={{
            __html: config.ui.globus.messageAlert.message,
          }}
        />
      </strong>
    </Alert>
  );
}
