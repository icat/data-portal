import type { Dataset, Entity } from '@edata-portal/icat-plus-api';
import { DatasetFiles } from 'components/datafile';
import {
  DatasetFilesTabName,
  DatasetList,
  MetadataTab,
} from 'components/dataset';
import { InstrumentWidget } from 'components/metadata';
import { getInputDatasetIds, getInstrumentParameters } from 'helpers';
import { Badge, Container } from 'react-bootstrap';

export type TabDefinition = {
  key: string;
  title: string | JSX.Element;
  content: JSX.Element;
  hidden?: boolean;
};

export function getFilesTab(
  dataset: Dataset,
  showPath: boolean,
  showDatasetDownloadButton: boolean,
): TabDefinition {
  return {
    key: 'files',
    title: <DatasetFilesTabName dataset={dataset} />,
    content: (
      <DatasetFiles
        dataset={dataset}
        showPath={showPath}
        showDatasetDownloadButton={showDatasetDownloadButton}
      />
    ),
  };
}

export function getMetadataTab(entity: Entity): TabDefinition {
  return {
    key: 'metadata',
    title: 'Metadata',
    content: (
      <Container fluid>
        <MetadataTab entity={entity} />
      </Container>
    ),
  };
}

export function getInputsTab(dataset: Dataset): TabDefinition {
  const inputs = getInputDatasetIds(dataset);

  return {
    key: 'Input',
    title: (
      <>
        Input
        <Badge
          bg={'info'}
          style={{
            marginLeft: 5,
          }}
        >
          {inputs.length}
        </Badge>
      </>
    ),
    content: (
      <Container fluid>
        <DatasetList datasetIds={inputs} groupBy="dataset" />
      </Container>
    ),
    hidden: !inputs.length,
  };
}

export function getInstrummentTab(dataset: Dataset): TabDefinition | undefined {
  const instrumentParams = getInstrumentParameters(dataset);
  return instrumentParams.length > 0
    ? {
        key: 'instrument',
        title: 'Instrument',
        content: (
          <Container fluid>
            <InstrumentWidget dataset={dataset} />
          </Container>
        ),
      }
    : undefined;
}
