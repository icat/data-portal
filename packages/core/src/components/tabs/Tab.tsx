import type { TabDefinition } from 'components/tabs/tabDefinition';
import { useBreakpointValue } from 'hooks';
import { useMemo } from 'react';
import { Dropdown, DropdownButton, Nav } from 'react-bootstrap';

export function RenderTabs(properties: {
  tabs: TabDefinition[];
  activeTab: string | undefined;
  setActiveTab: (tab: string) => void;
}) {
  const filteredTabs = useMemo(() => {
    if (!properties.tabs) return [];
    return properties.tabs.filter((t) => !t.hidden);
  }, [properties.tabs]);

  const renderer = useBreakpointValue({
    xs: <RenderTabsDropDown {...properties} tabs={filteredTabs} />,
    lg: <RenterTabsNav {...properties} tabs={filteredTabs} />,
  });

  if (filteredTabs.length <= 1) return null;

  return renderer;
}

export function RenderTabContent({
  tabs,
  activeTab,
}: {
  tabs: TabDefinition[];
  activeTab: string | undefined;
}) {
  const currentTab = findTab(tabs, activeTab);

  return currentTab?.content;
}

function RenderTabsDropDown({
  tabs,
  activeTab,
  setActiveTab,
}: {
  tabs: TabDefinition[];
  activeTab: string | undefined;
  setActiveTab: (tab: string) => void;
}) {
  const dropdownItems = useMemo(() => {
    return tabs?.map((tab) => (
      <Dropdown.Item
        eventKey={tab.key}
        onClick={() => setActiveTab(tab.key)}
        key={tab.key}
      >
        {tab.title}
      </Dropdown.Item>
    ));
  }, [setActiveTab, tabs]);

  if (!tabs?.length) return null;

  const currentTab = findTab(tabs, activeTab);

  return (
    <DropdownButton
      size="sm"
      variant="dark"
      title={currentTab?.title}
      onClick={(e) => {
        e.stopPropagation();
        document.body.click();
      }}
    >
      {dropdownItems}
    </DropdownButton>
  );
}

function RenterTabsNav({
  tabs,
  activeTab,
  setActiveTab,
}: {
  tabs: TabDefinition[];
  activeTab: string | undefined;
  setActiveTab: (tab: string) => void;
}) {
  const currentTab = findTab(tabs, activeTab);

  const tabItems = useMemo(() => {
    return tabs?.map((tab) => (
      <Nav.Item key={tab.key} className="d-flex align-items-end">
        <Nav.Link
          className={
            (currentTab?.key === tab.key ? 'text-black' : '') + ' py-1 px-3'
          }
          eventKey={tab.key}
          onClick={(e) => {
            e.stopPropagation();
            setActiveTab && setActiveTab(tab.key);
          }}
        >
          {tab.title}
        </Nav.Link>
      </Nav.Item>
    ));
  }, [tabs, currentTab?.key, setActiveTab]);

  return (
    <Nav
      variant="tabs"
      activeKey={currentTab?.key}
      style={{
        alignContent: 'end',
        marginRight: '0.5rem',
      }}
      className="align-self-end"
    >
      {tabItems}
    </Nav>
  );
}

function findTab(tabs: TabDefinition[], key: string | undefined) {
  if (!key) return undefined;
  return tabs.find((t) => t.key === key) || tabs.find((t) => t.title === key);
}
