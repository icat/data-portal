import { faClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { RenderTabContent, RenderTabs, TabDefinition } from 'components/tabs';
import { EllipsisView, Loading } from 'components/utils';
import { first } from 'helpers';
import { Suspense, useEffect, useMemo, useState } from 'react';
import { Button } from 'react-bootstrap';
import { createPortal } from 'react-dom';
import { Rnd } from 'react-rnd';

export type WindowProperties = {
  tabs?: TabDefinition[]; // tabs to show

  initialTab?: string; // Initial tab to show

  title?: string | React.ReactNode; // Title of the window

  onClose: () => void; // Callback to close the modal

  children?: React.ReactNode; // Content to show if no tabs are provided

  actionsIcons?: React.ReactNode; // Icons to show in the header
};

function getIndex() {
  const lastIndex = (window as any).lastWindowIndex || 1;
  const index = lastIndex + 1;
  (window as any).lastWindowIndex = index;
  return index;
}

export function Window(properties: WindowProperties) {
  const visibleTabs = useMemo(() => {
    return (
      properties.tabs?.filter((t) => {
        return !t.hidden;
      }) || []
    );
  }, [properties.tabs]);

  const [activeTabKey, setActiveTabKey] = useState(
    properties.initialTab || first(visibleTabs)?.key,
  );

  const [zIndex, setZIndex] = useState(getIndex());

  const initialHeight = Math.round(Math.min(window.innerHeight, 700));
  const initialWidth = Math.round(Math.min(window.innerWidth, 900));

  //random offset to initial position to make it easier to see multiple windows
  const offsetInitialX = Math.round((Math.random() * 2 - 1) * 100);
  const offsetInitialY = Math.round((Math.random() * 2 - 1) * 100);

  const maxInitialX = window.innerWidth - initialWidth;
  const maxInitialY = window.innerHeight - initialHeight;
  const initialXComputed =
    window.innerWidth / 2 - initialWidth / 2 + offsetInitialX;
  const initialYComputed =
    window.innerHeight / 2 - initialHeight / 2 + offsetInitialY;
  const initialX = Math.round(
    Math.max(0, Math.min(maxInitialX, initialXComputed)),
  );
  const initialY = Math.round(
    Math.max(0, Math.min(maxInitialY, initialYComputed)),
  );

  const [height, setHeight] = useState(initialHeight);
  const [width, setWidth] = useState(initialWidth);
  const [x, setX] = useState(initialX);
  const [y, setY] = useState(initialY);

  useEffect(() => {
    const updateValue = function () {
      if (width > window.innerWidth) {
        setWidth(window.innerWidth);
      }
      if (height > window.innerHeight) {
        setHeight(window.innerHeight);
      }
      if (x + width > window.innerWidth) {
        setX(Math.max(0, window.innerWidth - width));
      }
      if (y + height > window.innerHeight) {
        setY(Math.max(0, window.innerHeight - height));
      }
    };
    window.addEventListener('resize', updateValue);
    return () => window.removeEventListener('resize', updateValue);
  }, [width, height, x, y]);

  const windowElement = (
    <Rnd
      className="window bg-white border border-black d-flex flex-column"
      style={{
        overflow: 'hidden',
        borderRadius: '5px 5px 0px 0px',
        boxShadow: 'black 2px 2px 10px',
        zIndex: zIndex,
      }}
      dragHandleClassName="window-drag"
      bounds={'window'}
      size={{
        width,
        height,
      }}
      position={{
        x,
        y,
      }}
      onResize={(e, direction, ref, delta, position) => {
        setWidth(ref.offsetWidth);
        setHeight(ref.offsetHeight);
        setX(position.x);
        setY(position.y);
      }}
      onMouseDown={(e) => {
        e.stopPropagation();
        setZIndex(getIndex());
      }}
      onDrag={(e, data) => {
        setX(data.x);
        setY(data.y);
        e.stopPropagation();
      }}
    >
      <div className="window-header d-flex flex-row bg-secondary text-white align-items-center">
        <h4 className="window-drag py-1 px-4 m-0 d-flex overflow-hidden">
          <EllipsisView side="end">{properties.title}</EllipsisView>
        </h4>
        <div
          className="window-drag d-flex h-100"
          style={{
            flexGrow: 1,
          }}
        />
        <RenderTabs
          tabs={visibleTabs}
          activeTab={activeTabKey}
          setActiveTab={setActiveTabKey}
        />
        <div className="d-flex flex-row align-items-center">
          {properties.actionsIcons}
          <Button
            variant="link"
            onClick={properties.onClose}
            className="p-1"
            size="lg"
          >
            <FontAwesomeIcon className="text-white" icon={faClose} />
          </Button>
        </div>
      </div>
      <Suspense fallback={<Loading />}>
        <div className="modal-body h-100 w-100 overflow-auto p-2">
          {activeTabKey ? (
            <RenderTabContent tabs={visibleTabs} activeTab={activeTabKey} />
          ) : (
            properties.children || null
          )}
        </div>
      </Suspense>
    </Rnd>
  );

  return <>{createPortal(windowElement, document.body)}</>;
}
