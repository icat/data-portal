import { faClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Suspense, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { createPortal } from 'react-dom';

/**
 * This component will make its children fullscreen
 */
export function FullScreen({
  children,
  onExitFullscreen,
  showCloseButton = true,
}: {
  children: React.ReactNode;
  onExitFullscreen?: () => void;
  showCloseButton?: boolean;
}) {
  //exit on escape
  useEffect(() => {
    const handleEsc = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onExitFullscreen && onExitFullscreen();
      }
    };
    window.addEventListener('keydown', handleEsc);
    return () => {
      window.removeEventListener('keydown', handleEsc);
    };
  }, [onExitFullscreen]);

  const elem = (
    <div
      style={{
        position: 'fixed',
        zIndex: 1000,
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
      }}
    >
      {showCloseButton ? (
        <div
          style={{
            position: 'absolute',
            zIndex: 1000,
            right: 0,
            top: 0,
            padding: 10,
          }}
        >
          <Button onClick={onExitFullscreen}>
            <FontAwesomeIcon icon={faClose} />
          </Button>
        </div>
      ) : null}
      <Suspense fallback={<div>Loading...</div>}>{children}</Suspense>
    </div>
  );

  return <>{createPortal(elem, document.body)}</>;
}
