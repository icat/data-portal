export function NoScrollPage({ children }: { children: React.ReactNode }) {
  return (
    <div
      style={{
        position: 'absolute',
        inset: 0,
        backgroundColor: 'white',
        overflow: 'hidden',
      }}
    >
      {children}
    </div>
  );
}
