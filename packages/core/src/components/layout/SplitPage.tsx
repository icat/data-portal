export function SplitPage({
  left,
  right,
  head,
  ratioLeft,
  ratioRight,
}: {
  left: React.ReactNode;
  right: React.ReactNode;
  head?: React.ReactNode;
  ratioLeft?: number;
  ratioRight?: number;
}) {
  return (
    <div className="d-flex h-100 flex-column overflow-hidden gap-2">
      {head}
      <div className="d-flex flex-column flex-md-row flex-grow-1 overflow-hidden gap-2 ">
        {ratioLeft !== 0 ? (
          <SplitPagePanel ratio={ratioLeft}>{left}</SplitPagePanel>
        ) : null}
        {ratioRight !== 0 ? (
          <SplitPagePanel ratio={ratioRight}>{right}</SplitPagePanel>
        ) : null}
      </div>
    </div>
  );
}

function SplitPagePanel({
  children,
  ratio = 1,
}: {
  children: React.ReactNode;
  ratio?: number;
}) {
  return (
    <div
      className="d-flex flex-column flex-grow-1 h-100 overflow-hidden border bg-light"
      style={{
        flexBasis: `${ratio * 100}%`,
      }}
    >
      {children}
    </div>
  );
}
