export const LAYOUT_DIRECTION_OPTIONS = ['TB', 'LR'] as const;

export type LayoutDirection = (typeof LAYOUT_DIRECTION_OPTIONS)[number];

export type FlowChartNode = {
  id: string;
  element: React.ReactNode;
};

export type FlowChartEdge = {
  id: string;
  source: string;
  target: string;
};

export type FlowChartDefinition = {
  nodes: FlowChartNode[];
  edges: FlowChartEdge[];
  defaultLayoutDirection?: LayoutDirection;
  centerOnNode?: string;
};
