import type { FlowChartDefinition } from 'components/layout/flow/FlowChartDefinitions';
import { Loading } from 'components/utils';
import React, { Suspense } from 'react';

const FlowChartComponent = React.lazy(
  () => import('components/layout/flow/FlowChart'),
);

export function FlowChartLazy(definition: FlowChartDefinition) {
  return (
    <Suspense fallback={<Loading />}>
      <FlowChartComponent {...definition} />
    </Suspense>
  );
}
