import {
  FlowChartEdge,
  FlowChartNode,
  LAYOUT_DIRECTION_OPTIONS,
  type FlowChartDefinition,
  type LayoutDirection,
} from 'components/layout/flow/FlowChartDefinitions';
import { FlowChartLayouter } from 'components/layout/flow/FlowChartLayouter';
import { FlowChartNodeRender } from 'components/layout/flow/FlowChartNodeRender';
import { Button } from 'components/utils';
import { useCallback, useMemo, useState } from 'react';
import { ButtonGroup } from 'react-bootstrap';
import ReactFlow, {
  Controls,
  Panel,
  ReactFlowProvider,
  Background,
  BackgroundVariant,
  Node,
  XYPosition,
  Position,
} from 'reactflow';
import 'reactflow/dist/style.css';

const NODE_TYPES = {
  node: FlowChartNodeRender,
};

export type NodePositionsState = Record<string, XYPosition>;

export const UNPOSITIONED_NODE_POSITION: XYPosition = {
  x: -10000,
  y: -10000,
};

const LAYOUT_DIRECTION_LABELS: Record<LayoutDirection, string> = {
  TB: 'Vertical',
  LR: 'Horizontal',
};

export default function FlowChart(definition: FlowChartDefinition) {
  const [layoutDirection, setLayoutDirection] = useState<LayoutDirection>(
    definition?.defaultLayoutDirection || 'LR',
  );

  const [positionState, setPositionState] = useState<NodePositionsState>({});

  const nodesWithPositions = useMemo(
    () =>
      definition.nodes.map((node) =>
        convertToNode(node, definition.edges, positionState, layoutDirection),
      ),
    [definition.nodes, definition.edges, positionState, layoutDirection],
  );

  const updateNodePosition = useCallback(
    (id: string, position: XYPosition) => {
      setPositionState((prev) => ({
        ...prev,
        [id]: position,
      }));
    },
    [setPositionState],
  );

  return (
    <div
      className="d-flex bg-light border rounded border-secondary h-100 w-100"
      style={{
        minHeight: 'inherit',
      }}
    >
      <ReactFlowProvider>
        <ReactFlow
          nodes={nodesWithPositions}
          edges={definition.edges}
          onNodesChange={(changes) => {
            changes.forEach((change) => {
              if (change.type === 'position') {
                //only update to the position of the node are supported
                if (change.position !== undefined) {
                  updateNodePosition(change.id, change.position);
                }
              }
            });
          }}
          nodeTypes={NODE_TYPES}
          style={{ height: '100%', width: '100%', minHeight: 'inherit' }}
        >
          <Background color="#000" variant={BackgroundVariant.Dots} />
          <Controls />
          <FlowChartLayouter
            layoutDirection={layoutDirection}
            updateNodePosition={updateNodePosition}
            key={layoutDirection}
            definition={definition}
          />
          <Panel position="top-right">
            <ButtonGroup className="bg-white">
              {LAYOUT_DIRECTION_OPTIONS.map((direction) => (
                <Button
                  variant="outline-primary"
                  key={direction}
                  onClick={() => setLayoutDirection(direction)}
                  active={layoutDirection === direction}
                  size="sm"
                >
                  {LAYOUT_DIRECTION_LABELS[direction]}
                </Button>
              ))}
            </ButtonGroup>
          </Panel>
        </ReactFlow>
      </ReactFlowProvider>
    </div>
  );
}

function convertToNode(
  node: FlowChartNode,
  edges: FlowChartEdge[],
  positionState: NodePositionsState,
  layoutDirection: LayoutDirection,
): Node<any, 'node'> {
  const position = positionState[node.id] || UNPOSITIONED_NODE_POSITION;
  return {
    id: node.id,
    type: 'node',
    data: {
      definition: node,
      edges: edges.filter(
        (edge) => edge.source === node.id || edge.target === node.id,
      ),
    },
    position,
    sourcePosition: layoutDirection === 'TB' ? Position.Bottom : Position.Right,
    targetPosition: layoutDirection === 'TB' ? Position.Top : Position.Left,
  };
}
