import type {
  FlowChartEdge,
  FlowChartNode,
} from 'components/layout/flow/FlowChartDefinitions';
import { Handle, Position, type NodeProps } from 'reactflow';

export function FlowChartNodeRender(node: NodeProps) {
  const { data } = node;
  const edgesDefinitions: FlowChartEdge[] | undefined = data.edges;
  const nodeDefinition: FlowChartNode | undefined = data.definition;
  const id = node.id;
  const isTarget =
    edgesDefinitions && edgesDefinitions.some((edge) => edge.target === id);
  const isSource =
    edgesDefinitions && edgesDefinitions.some((edge) => edge.source === id);

  return (
    <div>
      {isTarget && (
        <Handle
          type="target"
          position={node.targetPosition ? node.targetPosition : Position.Left}
        />
      )}
      {isSource && (
        <Handle
          type="source"
          position={node.sourcePosition ? node.sourcePosition : Position.Right}
        />
      )}
      {nodeDefinition?.element || null}
    </div>
  );
}
