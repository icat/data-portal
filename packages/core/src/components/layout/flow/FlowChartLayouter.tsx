import {
  NodePositionsState,
  UNPOSITIONED_NODE_POSITION,
} from 'components/layout/flow/FlowChart';
import { useEffect, useMemo, useState } from 'react';
import { useStore, Node, Edge, useReactFlow, XYPosition } from 'reactflow';
import dagre from '@dagrejs/dagre';
import type {
  FlowChartDefinition,
  LayoutDirection,
} from 'components/layout/flow/FlowChartDefinitions';

export function FlowChartLayouter({
  definition,
  layoutDirection,
  updateNodePosition,
}: {
  definition: FlowChartDefinition;
  layoutDirection: LayoutDirection;
  updateNodePosition: (id: string, position: XYPosition) => void;
}) {
  const { fitView, viewportInitialized } = useReactFlow();

  const nodes = useStore((state) => state.getNodes());
  const edges = useStore((state) => state.edges);

  const nodesHaveSize = useMemo(() => {
    return nodes.every((node) => {
      return node.width !== undefined && node.height !== undefined;
    });
  }, [nodes]);

  const [computedNodesPositions, setComputedNodesPositions] =
    useState<NodePositionsState>({});

  useEffect(() => {
    if (nodesHaveSize) {
      const newPositions = computeDagreLayout(nodes, edges, layoutDirection);
      newPositions.forEach((node) => {
        const computedPosition = computedNodesPositions[node.id];
        if (
          !computedPosition ||
          computedPosition.x !== node.position.x ||
          computedPosition.y !== node.position.y
        ) {
          updateNodePosition(node.id, node.position);
          setComputedNodesPositions((prev) => ({
            ...prev,
            [node.id]: node.position,
          }));
        }
      });
    }
  }, [nodesHaveSize, definition, layoutDirection]); // eslint-disable-line react-hooks/exhaustive-deps

  const [doFit, setDoFit] = useState(false);

  const allNodesHavePosition = useMemo(() => {
    return nodes.every((node) => {
      return (
        node.position !== undefined &&
        node.position.x !== UNPOSITIONED_NODE_POSITION.x &&
        node.position.y !== UNPOSITIONED_NODE_POSITION.y
      );
    });
  }, [nodes]);

  useEffect(() => {
    if (allNodesHavePosition && viewportInitialized) {
      setDoFit(true);
    }
  }, [allNodesHavePosition, viewportInitialized]);

  useEffect(() => {
    const doFitFunction = () => {
      fitView({
        maxZoom: 1,
        nodes: definition?.centerOnNode
          ? [
              {
                id: definition.centerOnNode,
              },
            ]
          : undefined,
      });
    };
    if (doFit) {
      doFitFunction();
      setTimeout(doFitFunction, 200); //do again after a little bit because sometimes the first one does not do anything (maybe because not fully initialized despite `viewportInitialized` checked)
    }
  }, [doFit]); // eslint-disable-line react-hooks/exhaustive-deps

  return null;
}

function computeDagreLayout(
  nodes: Node[],
  edges: Edge[],
  layoutDirection: LayoutDirection,
) {
  const g = new dagre.graphlib.Graph();

  g.setGraph({
    rankdir: layoutDirection,
  });
  g.setDefaultEdgeLabel(() => ({}));

  nodes.forEach((node) => {
    g.setNode(node.id, { width: node.width || 0, height: node.height || 0 });
  });

  edges.forEach((edge) => {
    g.setEdge(edge.source, edge.target);
  });

  dagre.layout(g);

  return nodes.map((node) => {
    const nodeLayout = g.node(node.id);
    const position = nodeLayout
      ? {
          x: nodeLayout.x - (node.width || 0) / 2,
          y: nodeLayout.y - (node.height || 0) / 2,
        }
      : UNPOSITIONED_NODE_POSITION;
    return {
      id: node.id,
      position,
    };
  });
}
