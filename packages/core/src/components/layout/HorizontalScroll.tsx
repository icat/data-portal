import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useCallback, useEffect, useRef, useState } from 'react';
import { Button } from 'react-bootstrap';

export function HorizontalScroll({ children }: { children: React.ReactNode }) {
  const ref = useRef<HTMLDivElement>(null);

  const scrollState = useHorizontalScrollState(ref);

  const canScroll =
    !scrollState.isScrolledToLeft || !scrollState.isScrolledToRight;

  const scrollDrag = useHorizontalScrollDrag(ref);

  return (
    <div
      style={{
        position: 'relative',
        overflow: 'hidden',
        padding: 0,
      }}
    >
      <div
        ref={ref}
        style={{
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'nowrap',
          gap: 5,
          overflowX: 'auto',
          paddingRight: 3,
          ...(canScroll ? scrollDrag.styles : {}),
        }}
        {...(canScroll ? scrollDrag.handlers : {})}
      >
        {children}
      </div>
      <div
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          bottom: 0,
          width: 40,
          background:
            'linear-gradient(to right, rgb(255, 255, 255) 0px, rgb(255, 255, 255) 10px, rgba(255, 255, 255, 0))',
          display: scrollState.isScrolledToLeft ? 'none' : 'flex',
          alignContent: 'center',
          justifyContent: 'start',
          alignItems: 'center',
        }}
      >
        <Button
          onClick={() => {
            if (!ref.current) return;
            ref.current.scrollLeft -= 200;
          }}
          variant="link"
          className="p-0"
          size="sm"
        >
          <FontAwesomeIcon icon={faChevronLeft} />
        </Button>
      </div>
      <div
        style={{
          position: 'absolute',
          right: 0,
          top: 0,
          bottom: 0,
          width: 40,
          background:
            'linear-gradient(to left, rgb(255, 255, 255) 0px, rgb(255, 255, 255) 10px, rgba(255, 255, 255, 0))',
          display: scrollState.isScrolledToRight ? 'none' : 'flex',
          alignContent: 'center',
          justifyContent: 'end',
          alignItems: 'center',
        }}
      >
        <Button
          onClick={() => {
            if (!ref.current) return;
            ref.current.scrollLeft += 200;
          }}
          variant="link"
          className="p-0"
          size="sm"
        >
          <FontAwesomeIcon icon={faChevronRight} />
        </Button>
      </div>
    </div>
  );
}

function useHorizontalScrollState(ref: React.RefObject<HTMLDivElement>) {
  const [isScrolledToRight, setIsScrolledToRight] = useState(false);
  const [isScrolledToLeft, setIsScrolledToLeft] = useState(false);

  useEffect(() => {
    const element = ref.current;
    if (!element) return;

    const checkScroll = () => {
      const { scrollLeft, scrollWidth, clientWidth } = element;
      setIsScrolledToLeft(scrollLeft === 0);
      setIsScrolledToRight(scrollLeft + clientWidth >= scrollWidth - 1);
    };

    checkScroll();

    element.addEventListener('scroll', checkScroll);
    window.addEventListener('resize', checkScroll);
    window.addEventListener('click', checkScroll);

    return () => {
      element.removeEventListener('scroll', checkScroll);
      window.removeEventListener('resize', checkScroll);
      window.removeEventListener('click', checkScroll);
    };
  }, [ref]);

  return { isScrolledToLeft, isScrolledToRight };
}

function useHorizontalScrollDrag(ref: React.RefObject<HTMLDivElement>): {
  styles: React.CSSProperties;
  handlers: any;
} {
  const [initialScroll, setInitialScroll] = useState(0);
  const [mouseStartX, setMouseStartX] = useState(0);
  const [isDragging, setIsDragging] = useState(false);

  const cleanSelection = useCallback(() => {
    //unselect any text that got selected during the drag
    window.getSelection()?.removeAllRanges();
  }, []);

  const onMouseDown = useCallback(
    (e: React.MouseEvent) => {
      // initialize the drag
      setInitialScroll(ref.current?.scrollLeft || 0);
      setMouseStartX(e.clientX);
      setIsDragging(true);
    },
    [ref],
  );

  const onMouseMove = useCallback(
    (e: MouseEvent) => {
      if (!ref.current) return;
      if (!isDragging) return;
      //do scroll
      ref.current.scrollLeft = initialScroll - (e.clientX - mouseStartX);
      cleanSelection();
    },
    [initialScroll, isDragging, mouseStartX, ref, cleanSelection],
  );

  const onMouseUp = useCallback(() => {
    setIsDragging(false);
    cleanSelection();
  }, [cleanSelection]);

  useEffect(() => {
    if (isDragging) {
      window.addEventListener('mouseup', onMouseUp);
      window.addEventListener('mousemove', onMouseMove);
    }
    return () => {
      window.removeEventListener('mouseup', onMouseUp);
      window.removeEventListener('mousemove', onMouseMove);
    };
  });

  return {
    styles: {
      cursor: isDragging ? 'grabbing' : 'move',
      userSelect: isDragging ? 'none' : undefined,
    },
    handlers: {
      onMouseDown,
    },
  };
}
