export function InlineLinkedItems({
  children,
  className,
}: {
  children: React.ReactNode | React.ReactNode[];
  className?: string;
}) {
  const childrenArray = Array.isArray(children) ? children : [children];

  return (
    <div
      style={{
        position: 'relative',
      }}
      className={`d-flex flex-row gap-2 ${className || ''}`}
    >
      <div
        style={{
          position: 'absolute',
          top: '50%',
          right: 0,
          left: 0,
          height: 2,
          backgroundColor: 'black',
          transform: 'translateY(-50%)',
        }}
      />
      {childrenArray
        .filter((c) => !!c)
        .map((child, index) => (
          <div key={index} className="d-flex bg-white">
            {child}
          </div>
        ))}
    </div>
  );
}
