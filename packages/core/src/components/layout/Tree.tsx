import 'components/layout/Tree.css';

export type TreeItem = {
  key: string | number;
  item: JSX.Element;
  children?: TreeItem[];
  sticky?: boolean;
};

export function Tree({
  items,
  root = true,
  parentHasMoreItems = false,
}: {
  items: TreeItem[];
  root?: boolean;
  parentHasMoreItems?: boolean;
}) {
  if (items.length === 0) return null;

  return (
    <ul className={root ? 'tree tree-root' : 'tree tree-sub'}>
      {items.map((item, i) => {
        return (
          <li
            className={
              (root ? 'tree tree-root' : 'tree tree-sub') +
              (parentHasMoreItems ? ' tree-item-multiple' : '')
            }
            key={item.key}
          >
            <div
              className="tree-item"
              style={{
                position: item.sticky ? 'sticky' : 'relative',
                top: item.sticky ? 0 : undefined,
                zIndex: item.sticky ? 1 : undefined,
              }}
            >
              {item.item}
            </div>
            {item.children?.length ? (
              <Tree
                items={item.children}
                root={false}
                parentHasMoreItems={items.length > i + 1}
              />
            ) : null}
          </li>
        );
      })}
    </ul>
  );
}
