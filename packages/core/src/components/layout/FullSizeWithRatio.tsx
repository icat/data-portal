import { useCallback, useEffect, useMemo, useRef, useState } from 'react';

export function FullSizeWithRatio({
  children,
  ratioWidth = 1,
  ratioHeight = 1,
}: {
  children: React.ReactNode | React.ReactNode[];
  ratioWidth?: number;
  ratioHeight?: number;
}) {
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);

  const ref = useRef<HTMLDivElement>(null);

  const updateDimensions = useCallback(() => {
    if (ref.current) {
      const { width, height } = ref.current.getBoundingClientRect();
      setWidth(width);
      setHeight(height);
    }
  }, []);
  useEffect(() => {
    updateDimensions();
    window.addEventListener('resize', updateDimensions);
    return () => window.removeEventListener('resize', updateDimensions);
  }, [updateDimensions]);

  const [itemWidth, itemHeight, top, left] = useMemo(() => {
    const aspectRatio = ratioWidth / ratioHeight;

    const containerIsWider = width / height > aspectRatio;

    const resWidth = containerIsWider ? height * aspectRatio : width;
    const resHeight = containerIsWider ? height : width / aspectRatio;

    return [
      resWidth,
      resHeight,
      (height - resHeight) / 2,
      (width - resWidth) / 2,
    ];
  }, [height, ratioHeight, ratioWidth, width]);

  return (
    <div
      ref={ref}
      style={{
        position: 'relative',
        width: '100%',
        height: '100%',
      }}
    >
      <div
        style={{
          position: 'absolute',
          width: `${itemWidth}px`,
          height: `${itemHeight}px`,
          left: `${left}px`,
          top: `${top}px`,
        }}
      >
        {children}
      </div>
    </div>
  );
}
