import { FullScreen } from 'components/layout/FullScreen';
import { useState } from 'react';

/**
 * This component will make its children zoomable
 */
export function Zoomable({ children }: { children: React.ReactNode }) {
  const [fullscreen, setFullscreen] = useState(false);

  return (
    <div
      style={{
        cursor: 'zoom-in',
        width: '100%',
        height: '100%',
        overflow: 'hidden',
      }}
      onClick={() => setFullscreen(!fullscreen)}
    >
      {fullscreen && (
        <FullScreen onExitFullscreen={() => setFullscreen(false)}>
          <div
            style={{
              cursor: 'zoom-out',
              width: '100%',
              height: '100%',
              overflow: 'hidden',
            }}
            onClick={() => setFullscreen(!fullscreen)}
          >
            {children}
          </div>
        </FullScreen>
      )}
      {children}
    </div>
  );
}
