import type { ColumnDef } from '@tanstack/react-table';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { Spinner } from 'react-bootstrap';
import { SelectButton } from 'components/inputs/SelectButton';
import { StatisticLabel } from 'components/dataset/statistics/StatisticLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMagnifyingGlassMinus,
  faMagnifyingGlassPlus,
} from '@fortawesome/free-solid-svg-icons';
import { Button, LazyWrapper } from 'components/utils';
import {
  DATASET_VOLUME,
  DEFINITION_PARAM,
  FILE_COUNT,
  formatDateToDay,
  formatDateToTime,
  getDatasetName,
  getDatasetParamValue,
} from 'helpers';
import { DatasetDownloadButton } from 'components/dataset/DatasetDownloadButton';
import type { ExpandDataset } from 'components/dataset/ExpandDataset';
import { SampleActionButton } from 'components/sample/SampleActions';
import { DatasetActionsButton } from 'components/dataset/DatasetActions';

export function getDatasetTableColumns({
  metadataColumns,
}: {
  metadataColumns?: ColumnDef<ExpandDataset>[];
}): ColumnDef<ExpandDataset, any>[] {
  const columns: ColumnDef<ExpandDataset>[] = [
    {
      accessorFn: (row) => row,
      header: 'View',
      footer: '',
      cell: ({ row, getValue }) => (
        <Button
          key={`expand-${(getValue() as Dataset).id}`}
          size="sm"
          variant="outline-primary"
          style={{ border: 'none' }}
          {...{
            onClick: row.getToggleExpandedHandler(),
          }}
        >
          <FontAwesomeIcon
            icon={
              row.getIsExpanded()
                ? faMagnifyingGlassMinus
                : faMagnifyingGlassPlus
            }
          />
        </Button>
      ),
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      id: 'Selection',
      header: ({ table }) => {
        const datasetIds = table
          .getCoreRowModel()
          .rows.map((row) => row.original.id);
        return (
          <SelectButton
            key={`select-button-all`}
            type={'dataset'}
            ids={datasetIds}
          />
        );
      },
      footer: '',
      cell: (info) => (
        <SelectButton
          key={`select-button-${(info.getValue() as Dataset).id}`}
          type={'dataset'}
          ids={[(info.getValue() as Dataset).id]}
        />
      ),
      enableColumnFilter: false,
    },
    {
      accessorFn: (row) => row,
      header: 'Date',
      footer: 'Date',
      cell: ({ getValue }) => (
        <div>
          {formatDateToDay((getValue() as Dataset).startDate)}
          <span style={{ marginLeft: 5, color: '#777', fontSize: 'smaller' }}>
            {formatDateToTime((getValue() as Dataset).startDate)}
          </span>
        </div>
      ),
      enableColumnFilter: false,
    },
    {
      accessorKey: 'sampleName',
      header: 'Sample',
      footer: 'Sample',
      enableColumnFilter: false,
      cell: (item) => (
        <>
          <SampleActionButton
            sampleId={item.row.original.sampleId}
            investigationId={item.row.original.investigation.id}
          />
          <span>{item.getValue() as String}</span>
        </>
      ),
    },
    {
      accessorFn: (row) => row,
      header: 'Dataset',
      footer: 'Dataset',
      enableColumnFilter: false,
      cell: (info) => (
        <>
          <DatasetActionsButton dataset={info.getValue() as Dataset} />
          <span>{getDatasetName(info.getValue() as Dataset)}</span>
        </>
      ),
    },
    {
      accessorFn: (row) => row,
      header: 'Technique',
      footer: 'Technique',
      cell: (info) =>
        getDatasetParamValue(info.getValue() as Dataset, DEFINITION_PARAM),
      enableColumnFilter: false,
    },
  ];

  if (metadataColumns) {
    columns.push(...metadataColumns);
  }

  columns.push(
    {
      accessorFn: (row) => row,
      header: 'Files',
      enableColumnFilter: false,
      cell: (info) => (
        <StatisticLabel
          count={
            getDatasetParamValue(info.getValue() as Dataset, FILE_COUNT) || ''
          }
          volume={getDatasetParamValue(
            info.getValue() as Dataset,
            DATASET_VOLUME,
          )}
        />
      ),
    },
    {
      accessorFn: (row) => row,
      header: 'Download',
      cell: (info) => (
        <LazyWrapper placeholder={<Spinner size="sm" />}>
          <DatasetDownloadButton dataset={info.getValue() as Dataset} />
        </LazyWrapper>
      ),
      enableColumnFilter: false,
    },
  );
  return columns;
}
