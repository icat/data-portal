import type { ColumnDef, ExpandedState } from '@tanstack/react-table';
import {
  getCoreRowModel,
  getFilteredRowModel,
  useReactTable,
} from '@tanstack/react-table';
import {
  DATASET_LIST_ENDPOINT,
  DATASET_TYPE_ACQUISITION,
  Investigation,
} from '@edata-portal/icat-plus-api';
import { Col } from 'react-bootstrap';
import { Suspense, useState } from 'react';
import type { ExpandDataset } from 'components/dataset/ExpandDataset';
import { useEndpointPagination, useParam } from 'hooks';
import {
  Loading,
  NoData,
  SideNavElement,
  SideNavFilter,
} from 'components/utils';
import { TanstackBootstrapTable } from 'components/table/TanstackBootstrapTable';
import { useViewers, WithSideNav } from 'context';
import { SampleSelector } from 'components/sample';
import { SearchBar } from 'components/inputs';
import { SideNavMetadataFilter } from 'components/filters';

export function GenericDatasetTable({
  investigation,
  instrumentName,
  datasetIds,
  nested,
  columns,
}: {
  investigation: Investigation;
  instrumentName?: string;
  datasetIds?: (string | number)[];
  nested?: boolean;
  columns: ColumnDef<ExpandDataset>[];
}) {
  const [expanded, setExpanded] = useState<ExpandedState>({});
  const [sampleId, setSampleId] = useParam<string>('sampleId', '');
  const [search, setSearch] = useParam<string>('search', '');
  const [filters, setFilters] = useParam<string>('filters', '');

  const viewers = useViewers();

  const datasets = useEndpointPagination({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      ...(investigation
        ? { investigationIds: investigation.id.toString() }
        : {}),
      ...(instrumentName ? { instrumentName } : {}),
      ...(datasetIds?.length ? { datasetIds: datasetIds.join(',') } : {}),
      ...(datasetIds?.length ? {} : { datasetType: DATASET_TYPE_ACQUISITION }),
      sortBy: 'STARTDATE',
      sortOrder: -1,
      nested: nested,
      ...(search ? { search } : {}),
      ...(sampleId ? { sampleId } : {}),
      ...(filters ? { parameters: filters } : {}),
    },
    paginationParams: {
      paginationKey: 'datasets',
    },
  });

  const data = datasets.data.map((dataset) => ({
    ...dataset,
    subRows: [dataset],
  }));

  const table = useReactTable<ExpandDataset>({
    data: data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onExpandedChange: setExpanded,
    getSubRows: (row) => row?.subRows,
    getExpandedRowModel: getCoreRowModel(),

    ...datasets.tablePagination,
    state: {
      ...datasets.tablePagination.state,
      expanded: expanded,
    },
  });

  if (!data.length) {
    return (
      <Col>
        <NoData />
      </Col>
    );
  }

  return (
    <WithSideNav
      sideNav={
        <SideNavElement label="Datasets">
          <Col>
            <SideNavFilter label={'Filter by sample'}>
              <Suspense fallback={<small>Loading samples...</small>}>
                <SampleSelector
                  investigation={investigation}
                  selectedSampleId={Number(sampleId)}
                  updateSelectedSample={setSampleId}
                />
              </Suspense>
            </SideNavFilter>
          </Col>
          <Col>
            <SideNavFilter label={'Search dataset'}>
              <SearchBar
                onUpdate={(value) => setSearch(value)}
                value={search}
                placeholder="name, ..."
              />
            </SideNavFilter>
          </Col>
          <Col>
            <SideNavMetadataFilter
              filterDefinition={{
                label: 'Technique',
                parameter: 'definition',
                type: 'select-multi',
                doPrettyPrint: true,
              }}
              state={{
                currentFilters: filters || '',
                setCurrentFilters: setFilters,
                investigationId: investigation.id.toString(),
              }}
            />
          </Col>
        </SideNavElement>
      }
    >
      <div style={{ fontSize: '0.9em' }}>
        <TanstackBootstrapTable
          dataLength={datasets.totalElements}
          table={table}
          bordered={true}
          expandRender={(d) => (
            <Suspense fallback={<Loading />}>
              {viewers.viewDataset(d, 'tableCell')}
            </Suspense>
          )}
        />
      </div>
    </WithSideNav>
  );
}
