import type { Dataset } from '@edata-portal/icat-plus-api';
import { faGears, faVial, faUserGear } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { isProcessed, isReprocessed } from 'helpers';
import { useMemo } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

export function DatasetTypeIcon({ dataset }: { dataset: Dataset }) {
  const { icon, color, label } = useMemo(() => {
    if (isProcessed(dataset)) {
      return {
        icon: faGears,
        color: 'text-warning',
        label: 'Processed dataset',
      };
    }
    if (isReprocessed(dataset)) {
      return {
        icon: faUserGear,
        color: 'text-info',
        label: 'Reprocessed dataset',
      };
    }
    return { icon: faVial, color: 'text-success', label: 'Raw dataset' };
  }, [dataset]);
  return (
    <OverlayTrigger
      placement="auto"
      overlay={<Tooltip id="tooltip">{label}</Tooltip>}
    >
      <FontAwesomeIcon icon={icon} className={color} />
    </OverlayTrigger>
  );
}
