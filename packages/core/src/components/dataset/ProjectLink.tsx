import { Dataset } from '@edata-portal/icat-plus-api';
import { faSquareArrowUpRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useConfig } from 'context';
import { getParameterValue, PROJECT_NAME_PARAM } from 'helpers';
import { useMemo } from 'react';
import { Badge } from 'react-bootstrap';

function formatProjectUrl(dataset: Dataset, url: string) {
  return url.replace('{datasetId}', String(dataset.id));
}

export function ProjectLink({ dataset }: { dataset: Dataset }) {
  const config = useConfig();

  const project = useMemo(() => {
    const projectKey = getParameterValue(dataset, PROJECT_NAME_PARAM);
    return config.ui.projects.find((p) => p.key === projectKey);
  }, [config.ui.projects, dataset]);

  if (!project) return null;

  return (
    <h3>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href={formatProjectUrl(dataset, project.url)}
      >
        <Badge bg="secondary">
          {project.title}
          <FontAwesomeIcon
            icon={faSquareArrowUpRight}
            style={{ marginLeft: 8 }}
          />
        </Badge>
      </a>
    </h3>
  );
}
