import {
  useEndpointURL,
  type Dataset,
  DATA_DOWNLOAD_ENDPOINT,
  DATASET_STATUS_ONLINE,
} from '@edata-portal/icat-plus-api';
import { DatasetRestoreModal } from 'components/dataset/DatasetRestoreModal';
import { GlobusMessage } from 'components/utils';
import { useConfig } from 'context';
import {
  DATASET_VOLUME,
  flattenOutputs,
  getDatasetParamValue,
  stringifyWithUnitPrefix,
} from 'helpers';
import { useDatasetStatus, useTrackingCallback, useDatasetPath } from 'hooks';
import { useMemo } from 'react';
import { Button, Col, Modal, Row } from 'react-bootstrap';

export function DatasetDownloadModal({
  dataset,
  onClose,
}: {
  dataset: Dataset;
  onClose: () => void;
}) {
  const status = useDatasetStatus(dataset);
  const path = useDatasetPath(dataset);
  const config = useConfig();

  const tracking = useTrackingCallback();

  const ids = useMemo(() => {
    return flattenOutputs([dataset])
      .map((d) => d.id)
      .join(',');
  }, [dataset]);

  const volume = useMemo(() => {
    return flattenOutputs([dataset])
      .map((d) => getDatasetParamValue(d, DATASET_VOLUME))
      .reduce((a, b) => {
        const v = Number(b);
        if (isNaN(v)) return a;
        return a + v;
      }, 0);
  }, [dataset]);

  const downloadUrl = useEndpointURL(DATA_DOWNLOAD_ENDPOINT);

  const globusUrl = useMemo(() => {
    if (!config.ui.globus.enabled) return undefined;

    const locations =
      path?.length && path?.length > 0 ? path : [dataset.location];
    if (locations) {
      for (const location of locations) {
        if (!location) continue;
        const collection = config.ui.globus.collections.find((c) =>
          location.startsWith(c.root),
        );
        if (!collection) continue;

        const globusPath = location.replace(collection.origin, '');
        const params = new URLSearchParams({
          origin_id: collection.originId,
          origin_path: globusPath,
        });

        return `${config.ui.globus.url}${params.toString()}`;
      }
    }
    return undefined;
  }, [
    config.ui.globus.collections,
    config.ui.globus.enabled,
    config.ui.globus.url,
    dataset.location,
    path,
  ]);

  if (status !== DATASET_STATUS_ONLINE)
    return <DatasetRestoreModal dataset={dataset} onClose={onClose} />;

  const httpUrl = downloadUrl({ datasetIds: ids });

  //if there is no globus - only option is http, trigger the download and hide the modal
  if (!globusUrl) {
    window.location.href = downloadUrl({ datasetIds: ids });
    onClose();
    return null;
  }

  return (
    <Modal onHide={onClose} show backdrop="static" keyboard={false} centered>
      <Modal.Header
        closeButton
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <Modal.Title>Download dataset</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <GlobusMessage />
        <Row className="g-2">
          {volume && (
            <Col xs={12}>
              Total uncompressed volume:{' '}
              {stringifyWithUnitPrefix(volume, 2, 'B')}
            </Col>
          )}
          <Col xs={12}>
            <Button
              onClick={() => {
                tracking('Download', 'dataset-http', dataset.location);
                window.open(httpUrl, '_blank');
              }}
            >
              Download with browser (&#60;2GB)
            </Button>
          </Col>
          {globusUrl && (
            <Col xs={12}>
              <Button
                onClick={() => {
                  tracking('Download', 'dataset-globus', dataset.location);
                  window.open(globusUrl, '_blank');
                }}
              >
                Download with Globus
              </Button>
            </Col>
          )}
        </Row>
      </Modal.Body>
    </Modal>
  );
}
