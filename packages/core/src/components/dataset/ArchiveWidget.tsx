import {
  faFileCircleCheck,
  faFileCircleExclamation,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

export function ArchiveWidget({ status }: { status: String | undefined }) {
  if (status) {
    const isArchived = status === 'archived';
    const iconName = isArchived ? faFileCircleCheck : faFileCircleExclamation;
    const className = isArchived ? 'text-info' : 'text-warning';
    return (
      <OverlayTrigger
        delay={{ show: 250, hide: 400 }}
        overlay={
          <Tooltip key={'ArchiveWidgetTooltip'}>
            Archive status: {status}
          </Tooltip>
        }
      >
        <FontAwesomeIcon
          key={'ArchiveWidgetContent'}
          icon={iconName}
          className={className}
        />
      </OverlayTrigger>
    );
  }
  return null;
}
