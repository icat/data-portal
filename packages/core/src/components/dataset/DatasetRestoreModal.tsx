import {
  DATASET_STATUS_ARCHIVED,
  DATASET_STATUS_RESTORING,
  DATA_RESTORE_ENDPOINT,
  Dataset,
  useMutateEndpoint,
} from '@edata-portal/icat-plus-api';
import { useUser } from 'context';
import { flattenOutputs } from 'helpers';
import { useDatasetStatus, useTrackingCallback } from 'hooks';
import { useMemo, useState } from 'react';
import { Modal, Button, Form, Alert } from 'react-bootstrap';

export function DatasetRestoreModal({
  dataset,
  onClose,
}: {
  dataset: Dataset;
  onClose: () => void;
}) {
  const status = useDatasetStatus(dataset);

  return (
    <Modal onHide={onClose} show backdrop="static" keyboard={false} centered>
      <Modal.Header
        closeButton
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <Modal.Title>Request to restore dataset</Modal.Title>
      </Modal.Header>
      {status === DATASET_STATUS_RESTORING && (
        <RestoreInProgressContent onClose={onClose} />
      )}
      {status === DATASET_STATUS_ARCHIVED && (
        <RestoreRequestContent dataset={dataset} onClose={onClose} />
      )}
    </Modal>
  );
}
function RestoreInProgressContent({ onClose }: { onClose: () => void }) {
  return (
    <>
      <Modal.Body>
        <Alert variant="info">
          <strong>
            <p>The dataset is currently being restored.</p>
            <p>The files will be available upon completion</p>
          </strong>
        </Alert>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={onClose}>
          Close
        </Button>
      </Modal.Footer>
    </>
  );
}

function RestoreRequestContent({
  dataset,
  onClose,
}: {
  dataset: Dataset;
  onClose: () => void;
}) {
  const user = useUser();
  const [email, setEmail] = useState('');

  const tracking = useTrackingCallback();

  const isAnonymous = useMemo(() => {
    return !user?.isAuthenticated;
  }, [user]);

  const ids = useMemo(() => {
    return flattenOutputs([dataset])
      .map((d) => d.id)
      .join(',');
  }, [dataset]);

  const restore = useMutateEndpoint({
    endpoint: DATA_RESTORE_ENDPOINT,
    params: {
      datasetIds: ids,
    },
  });

  const handleRestore = () => {
    if (!user || (isAnonymous && !email)) {
      return;
    }
    tracking('Restore', 'Dataset', dataset.location);
    restore
      .mutateAsync({
        body: {
          name: user.name,
          email,
        },
      })
      .then(() => {
        onClose();
      });
  };

  return (
    <>
      <Modal.Body>
        <Alert variant="warning">
          <strong>
            This dataset is archived. You need to request the restoration of the
            dataset before you can download or view the files.
          </strong>
        </Alert>
        <p>The dataset restoration can take time.</p>
        <p>An email notification will be sent upon completion.</p>
        {isAnonymous && (
          <Form>
            <Form.Group className="mb-3" controlId="restoreForm">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                required
                type="email"
                placeholder="name@example.com"
                autoFocus
                defaultValue={email}
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </Form.Group>
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          disabled={restore.isPending}
          onClick={onClose}
        >
          Cancel
        </Button>
        <Button
          variant="primary"
          disabled={restore.isPending || (isAnonymous && !email)}
          onClick={handleRestore}
        >
          Restore
        </Button>
      </Modal.Footer>
    </>
  );
}
