import { SAMPLE_COUNT, DATASET_VOLUME } from 'helpers/statistics';
import { StatisticLabel } from 'components/dataset/statistics/StatisticLabel';
import { PopoverDatasetStatistics } from 'components/dataset/statistics/PopoverDatasetStatistics';
import { OverlayTrigger, Row, Col, Popover } from 'react-bootstrap';

export function DatasetStatistics({
  parameters,
  label,
  showLabel,
  parameterCaption = SAMPLE_COUNT, // Parameter that will be used to show as a caption. Example DATASET_COUNT will show the number of datasets
}: {
  parameters: Record<string, string>;
  label: string;
  showLabel?: boolean;
  parameterCaption: string;
}) {
  const count = parameters[parameterCaption];
  if (!count) return null;
  const volume = parameters[DATASET_VOLUME];

  return (
    <Row>
      {showLabel ? (
        <Col>
          <strong>{label}: </strong>
        </Col>
      ) : null}
      <Col>
        <OverlayTrigger
          placement="auto"
          overlay={
            <Popover>
              <Popover.Header>Dataset Statistics</Popover.Header>
              <Popover.Body>
                <PopoverDatasetStatistics parameters={parameters} />
              </Popover.Body>
            </Popover>
          }
        >
          <span className={'text-right text-info tablenumber'}>
            <StatisticLabel count={count} volume={volume} />
          </span>
        </OverlayTrigger>
      </Col>
    </Row>
  );
}
