import {
  type Dataset,
  useGetEndpoint,
  DATA_COLLECTION_LIST_ENDPOINT,
  DataCollection,
} from '@edata-portal/icat-plus-api';
import { Col, Row } from 'react-bootstrap';
import { DOIBadge } from '@edata-portal/doi';

export function DoiInfo({ dataset }: { dataset: Dataset }) {
  const dois = useGetEndpoint({
    endpoint: DATA_COLLECTION_LIST_ENDPOINT,
    params: {
      datasetId: String(dataset.id),
    },
  });

  if (!dois?.length) return null;

  return (
    <>
      <Row>
        <Col>
          <strong>Related DOI{dois.length > 1 ? 's' : ''}</strong>
        </Col>
      </Row>
      <Row className="g-2">
        {dois.map((doi: DataCollection) => {
          return (
            <Col key={doi.id} xs={'auto'}>
              <DOIBadge doi={doi.doi} />
            </Col>
          );
        })}
      </Row>
      <hr />
    </>
  );
}
