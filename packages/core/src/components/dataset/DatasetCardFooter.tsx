import { ArchiveWidget } from 'components/dataset/ArchiveWidget';
import type { DatasetCardProperties } from 'components/dataset/DatasetCard';
import { DatasetDownloadButton } from 'components/dataset/DatasetDownloadButton';
import { UpdatedSampleWidget } from 'components/dataset/UpdatedSampleWidget';
import { CopyValue, LazyWrapper } from 'components/utils';
import { getDatasetParamValue, getDatasetURL } from 'helpers';
import {
  Card,
  OverlayTrigger,
  Tooltip,
  Button,
  Spinner,
} from 'react-bootstrap';

export function DatasetCardFooter(properties: DatasetCardProperties) {
  const { dataset, classNameFooter, expanded = true } = properties;

  if (!expanded) return null;

  return (
    <Card.Footer
      className={classNameFooter}
      style={{
        display: 'flex',
        alignItems: 'center',
        fontSize: '0.8rem',
        justifyContent: 'space-between',
        flexFlow: 'row wrap',
      }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          gap: '0.5rem',
          flexFlow: 'row wrap',
        }}
      >
        <CopyValue type="value" value={dataset.location} />
        <UpdatedSampleWidget dataset={dataset} />
        <ArchiveWidget
          status={getDatasetParamValue(dataset, '__archiveStatus')}
        />

        <OverlayTrigger
          delay={{ show: 250, hide: 400 }}
          placement="right"
          overlay={
            <Tooltip key={'DatasetCardExploreTooltip'} id="explore-tooltip">
              Open dataset details in a new tab
            </Tooltip>
          }
        >
          <Button
            key={'DatasetCardExploreButton'}
            size="sm"
            onClick={() =>
              window.open(
                getDatasetURL(dataset.id, dataset.investigation.id),
                '_blank',
                'noreferrer',
              )
            }
          >
            Explore
          </Button>
        </OverlayTrigger>
        {properties.hideDownloadButton ? null : (
          <div>
            <LazyWrapper placeholder={<Spinner size="sm" />}>
              <DatasetDownloadButton dataset={dataset} />
            </LazyWrapper>
          </div>
        )}
      </div>
    </Card.Footer>
  );
}
