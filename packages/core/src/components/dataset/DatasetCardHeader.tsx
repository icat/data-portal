import { DatasetActionsButton } from 'components/dataset/DatasetActions';
import type { DatasetCardProperties } from 'components/dataset/DatasetCard';
import { SelectButton } from 'components/inputs/SelectButton';
import { RenderTabs } from 'components/tabs';
import { ActionCardHeader } from 'components/utils';
import { getDatasetName, isAcquisition } from 'helpers';
import { useMemo } from 'react';

export type DatasetCardHeaderProperties = {
  activeTab?: string;
  setActiveTab?: (tab: string) => void;
} & DatasetCardProperties;

export function DatasetCardHeader(properties: DatasetCardHeaderProperties) {
  const {
    dataset,
    setExpanded,
    expanded = true,
    title,
    hideDatasetTitle,
    hideSelectionButton,
  } = properties;

  const isAcquisitionDataset = useMemo(() => {
    return isAcquisition(dataset);
  }, [dataset]);

  const leftAlignedPart = [];
  const rightAlignedPart = [];
  const tabs =
    expanded && properties.tabs && properties.setActiveTab
      ? [
          <RenderTabs
            key={'tabs'}
            tabs={properties.tabs}
            activeTab={properties.activeTab}
            setActiveTab={properties.setActiveTab}
          />,
        ]
      : [];
  if (hideDatasetTitle) {
    leftAlignedPart.push(tabs);
  } else {
    rightAlignedPart.push(tabs);
  }

  if (!hideSelectionButton) {
    rightAlignedPart.push(
      <SelectButton
        key="selectButton"
        type={'dataset'}
        ids={[dataset.id]}
        light={isAcquisitionDataset}
      />,
    );
  }
  rightAlignedPart.push(
    <DatasetActionsButton key="datasetAction" dataset={dataset} />,
  );

  return (
    <ActionCardHeader
      date={dataset.startDate}
      isProcessed={!isAcquisitionDataset}
      title={title ? title : getDatasetName(dataset)}
      leftAlignedItems={leftAlignedPart}
      rightAlignedItems={rightAlignedPart}
      className={properties.classNameHeader}
      expanded={expanded}
      setExpanded={setExpanded}
      hideDatasetTitle={hideDatasetTitle}
    />
  );
}
