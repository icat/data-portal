import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import { getDatasetParamValue } from 'helpers';
import { MetadataTable } from 'components/metadata';

export function UpdatedSampleWidget({ dataset }: { dataset: Dataset }) {
  const originalSampleName = getDatasetParamValue(
    dataset,
    '__original_Sample_name',
  );
  const currentSampleName = getDatasetParamValue(dataset, 'Sample_name');

  if (!originalSampleName) return null;

  return (
    <OverlayTrigger
      placement="auto"
      overlay={
        <Popover key={'sampleupdatedttooltip'}>
          <Popover.Header as="h3">Sample name changed</Popover.Header>
          <Popover.Body>
            <p>
              The sample name of this dataset was changed. This change may have
              affected the location of the dataset.
            </p>
            <p>
              This means that tools like H5Viewer may not work, and the
              displayed location may be incorrect.
            </p>
            <MetadataTable
              parameters={[
                {
                  caption: 'Original sample name',
                  value: originalSampleName,
                },
                {
                  caption: 'Current sample name',
                  value: currentSampleName,
                },
              ]}
            />
          </Popover.Body>
        </Popover>
      }
    >
      <FontAwesomeIcon className="text-warning" icon={faExclamationTriangle} />
    </OverlayTrigger>
  );
}
