import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Placeholder from 'react-bootstrap/Placeholder';
import { CopyValue } from 'components/utils';
import { Button, Card, Col, Container, Row } from 'react-bootstrap';

export function PlaceHolderDatasetCard() {
  return (
    <Card
      className={'dataset-card border-secondary'}
      style={{
        marginBottom: '1rem',
      }}
    >
      <Card.Header className={'bg-dataset-raw'}>
        <Placeholder as={Card.Title} animation="glow">
          <Placeholder xs={6} />
        </Placeholder>
      </Card.Header>
      <Card.Body style={{ overflow: 'auto', padding: 0 }}>
        <Container fluid>
          <Row>
            <Col>
              <Container fluid>
                {[1, 2, 3, 4].map((i) => {
                  return (
                    <Row key={i + '_col1'}>
                      <Col>
                        <Placeholder as={Card.Text} animation="glow">
                          <Placeholder xs={12} />
                        </Placeholder>
                      </Col>
                    </Row>
                  );
                })}
              </Container>
            </Col>
            <Col>
              <Container fluid>
                {[1, 2, 3, 4].map((i) => {
                  return (
                    <Row key={i + '_col2'}>
                      <Col>
                        <Placeholder as={Card.Text} animation="glow">
                          <Placeholder xs={12} />
                        </Placeholder>
                      </Col>
                    </Row>
                  );
                })}
              </Container>
            </Col>

            {[1, 2, 3, 4].map((i) => {
              return (
                <Col key={i + '_col3'}>
                  <Placeholder as={Card.Text} animation="glow">
                    <Placeholder xs={12} />
                  </Placeholder>
                </Col>
              );
            })}
          </Row>
        </Container>
      </Card.Body>
      <Card.Footer
        style={{
          display: 'flex',
          alignItems: 'center',
          fontSize: '0.8rem',
          justifyContent: 'space-between',
          flexFlow: 'row wrap',
        }}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            gap: '0.5rem',
            flexFlow: 'row wrap',
          }}
        >
          <Placeholder as={Card.Text} animation="glow">
            <Placeholder xs={6} />
          </Placeholder>
          <CopyValue type="value" value={''} />
          <Button size="sm">
            <FontAwesomeIcon icon={faDownload} />
          </Button>{' '}
        </div>
      </Card.Footer>
    </Card>
  );
}
