import {
  DATASET_COUNT,
  ACQUISITION_DATASET_COUNT,
  PROCESSED_DATASET_COUNT,
  DATASET_VOLUME,
  ACQUISITION_DATASET_VOLUME,
  PROCESSED_DATASET_VOLUME,
} from 'helpers/statistics';
import { StatisticLabel } from 'components/dataset/statistics/StatisticLabel';
import { getDatasetStatistics } from 'components/dataset/statistics/helper';
import { Stack } from 'react-bootstrap';

/**
 * Popover component with the statistics created from the dataset.parameters
 * @param param0
 * @returns
 */
export function PopoverDatasetStatistics({
  parameters,
}: {
  parameters: Record<string, string>;
}) {
  const statsPerDatasetParameters = getDatasetStatistics({ parameters });

  return (
    <Stack>
      <StatisticLabel
        text="Total"
        count={parameters[DATASET_COUNT]}
        volume={parameters[DATASET_VOLUME]}
      />
      <StatisticLabel
        text="Acquisition"
        count={parameters[ACQUISITION_DATASET_COUNT]}
        volume={parameters[ACQUISITION_DATASET_VOLUME]}
      />
      <StatisticLabel
        text="Processed"
        count={parameters[PROCESSED_DATASET_COUNT]}
        volume={parameters[PROCESSED_DATASET_VOLUME]}
      />
      {statsPerDatasetParameters.length > 0 && (
        <div style={{ marginTop: 4 }}>
          {statsPerDatasetParameters.map((stat, i) => (
            <div key={`param-${i}`}>
              <p style={{ margin: 0, fontWeight: 'bold' }}>{stat.paramName}</p>
              {stat.values.map((value, index) => (
                <div style={{ marginLeft: 4 }} key={`divStat-${index}`}>
                  <StatisticLabel
                    key={`stat-${index}`}
                    text={value.type}
                    count={value.nbDatasets}
                  />
                </div>
              ))}
            </div>
          ))}
        </div>
      )}
    </Stack>
  );
}
