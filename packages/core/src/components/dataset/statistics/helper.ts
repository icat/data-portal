import {
  DATASET_COUNT,
  ACQUISITION_DATASET_COUNT,
  PROCESSED_DATASET_COUNT,
  ARCHIVED_DATASET_COUNT,
  ACQUISITION_ARCHIVED_DATASET_COUNT,
  PROCESSED_ARCHIVED_DATASET_COUNT,
} from 'helpers/statistics';

/** Parameter that will not be shown on the overlay*/
const skippedParameters = [
  ACQUISITION_DATASET_COUNT,
  PROCESSED_DATASET_COUNT,
  DATASET_COUNT,
  ARCHIVED_DATASET_COUNT,
  ACQUISITION_ARCHIVED_DATASET_COUNT,
  PROCESSED_ARCHIVED_DATASET_COUNT,
];

export function getDatasetStatistics({
  parameters,
}: {
  parameters: Record<string, string>;
}) {
  const statsPerDatasetParameters = [];
  const regex = new RegExp(/__(.*)DatasetCount/g);
  for (const [name, value] of Object.entries(parameters)) {
    if (name.match(regex) && !skippedParameters.includes(name)) {
      let clearName = name.replace('__', '').replace('DatasetCount', '');
      const acquisitionRegex = new RegExp(/(.*)Acquisition/g);
      if (clearName.match(acquisitionRegex)) {
        clearName = `Acquisition ${clearName.replace('Acquisition', '')}`;
      }
      const processedRegex = new RegExp(/(.*)Processed/g);
      if (clearName.match(processedRegex)) {
        clearName = `Processed ${clearName.replace('Processed', '')}`;
      }
      const obj = JSON.parse(value);
      const values = [];
      for (const [type, nbDatasets] of Object.entries(obj)) {
        values.push({ type, nbDatasets: Number(nbDatasets) });
      }
      statsPerDatasetParameters.push({ paramName: clearName, values });
    }
  }

  return statsPerDatasetParameters;
}
