import { stringifyWithUnitPrefix } from 'helpers';

export function StatisticLabel({
  text,
  count,
  volume,
}: {
  text?: string;
  count: number | string;
  volume?: number | string;
}) {
  const countNumber = Number(count);
  const volumeNumber = Number(volume);
  if (Number.isNaN(countNumber) || countNumber === 0) return null;
  return (
    <span>
      {text && <>{text}: </>}
      {count}
      {!Number.isNaN(volumeNumber) && volumeNumber !== 0 && (
        <>
          {' '}
          <small className={'text-muted text-nowrap'}>
            {stringifyWithUnitPrefix(volumeNumber, 2, 'B')}
          </small>
        </>
      )}
    </span>
  );
}
