import {
  faCheck,
  faLock,
  faRocket,
  faXmark,
} from '@fortawesome/free-solid-svg-icons';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { ActionMenu, ActionMenuItem } from 'components/utils/ActionMenu';
import { useAvailableWorkflows, useReprocess, useUser } from 'context';
import { getDatasetURL } from 'helpers';
import { usePath, useParam, useItemSelection } from 'hooks';
import { useState } from 'react';
import { DatasetDownloadButton } from 'components/dataset/DatasetDownloadButton';
import { DatasetDownloadModal } from 'components/dataset/DatasetDownloadModal';
import { CopyValue } from 'components/utils';

export function DatasetActionsButton({ dataset }: { dataset: Dataset }) {
  const investigationIdPath = usePath('investigationId');
  const [investigationIdSearch] = useParam('investigationId', '');
  const investigationIdFilter =
    investigationIdPath !== 'undefined'
      ? investigationIdPath
      : investigationIdSearch;

  const investigationId = dataset.investigation.id;
  const datasetId = dataset.id;

  const workflows = useAvailableWorkflows(dataset);

  const reprocess = useReprocess();

  const selection = useItemSelection('dataset', [dataset.id]);

  const user = useUser();

  const [showDownloadModal, setShowDownloadModal] = useState(false);

  const selectionItems: ActionMenuItem[] = user?.isAuthenticated
    ? [
        { node: <hr className="mb-1 mt-1" /> },
        {
          label: selection.isSelected
            ? 'Remove from selection'
            : 'Add to selection',
          icon: selection.isSelected ? faXmark : faCheck,
          onClick: () => {
            selection.toggleSelected();
          },
          active: selection.isSelected,
        },
      ]
    : [];
  const reprocessingItems: ActionMenuItem[] =
    user?.isAuthenticated && workflows.length > 0
      ? [
          { node: <hr className="mb-1 mt-1" /> },
          { node: <strong>Reprocess</strong> },
          ...workflows.map((workflow) => ({
            label: workflow.label,
            tooltip:
              'description' in workflow.input_schema
                ? workflow.input_schema.description
                : 'Reprocess this dataset with this workflow',
            icon: workflow.keywords.roles ? faLock : faRocket,
            align: 'left' as const,
            onClick: () => {
              reprocess({
                datasetIds: [dataset.id],
                workflowId: workflow.id,
              });
            },
          })),
        ]
      : [];

  return (
    <>
      <ActionMenu
        items={[
          {
            hide: investigationIdFilter === investigationId.toString(),
            link: `/investigation/${investigationId}/datasets`,
            label: 'Open experiment page',
          },
          {
            link: getDatasetURL(datasetId, investigationId),
            label: 'Open dataset page',
          },
          ...selectionItems,
          ...reprocessingItems,
          { node: <hr className="mb-1 mt-1" /> },
          {
            hideOnClick: true,
            node: (
              <DatasetDownloadButton
                dataset={dataset}
                onClick={() => setShowDownloadModal(true)}
                doShowModal={false}
              />
            ),
          },
          {
            hideOnClick: true,
            node: (
              <CopyValue
                type="button"
                label="dataset path"
                value={dataset.location}
              />
            ),
          },
        ]}
      />
      {showDownloadModal && (
        <DatasetDownloadModal
          dataset={dataset}
          onClose={() => {
            setShowDownloadModal(false);
          }}
        />
      )}
    </>
  );
}
