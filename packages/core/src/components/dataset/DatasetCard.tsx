import { first, isAcquisition } from 'helpers';
import { useMemo, useState } from 'react';
import { Card } from 'react-bootstrap';
import { DatasetCardHeader } from 'components/dataset/DatasetCardHeader';
import { DatasetCardFooter } from 'components/dataset/DatasetCardFooter';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { RenderTabContent, type TabDefinition } from 'components/tabs';

export type DatasetCardProperties = {
  classNameHeader?: string; // Adds something to the class name of the card header
  classNameBody?: string; // Adds something to the class name of the card body
  classNameFooter?: string; // Adds something to the class name of the card footer

  tabs?: TabDefinition[]; // Additional tabs to show

  title?: string; // Title of the card (default is dataset name)
  dataset: Dataset; // The dataset to show

  setExpanded?: (v: boolean) => void; // Function to call when the card header is clicked (used to expand/collapse the card)
  expanded?: boolean; // Whether the card is expanded or collapsed (body & footer are only shown when expanded)

  hideDatasetTitle?: boolean; // Hide the time and dataset name / title, displayed by default
  hideSelectionButton?: boolean; // Hide the selection button in the header, displayed by default
  hideDownloadButton?: boolean; // Hide the download button in the footer, displayed by default
};

export function DatasetCard({
  children,
  ...properties
}: {
  children?: React.ReactNode;
} & DatasetCardProperties) {
  const { dataset, expanded = true, tabs, classNameBody } = properties;

  const availableTabs = useMemo(() => {
    if (!tabs) return [];
    return tabs.filter((t) => {
      return !t.hidden;
    });
  }, [tabs]);

  const [activeTab, setActiveTab] = useState(first(availableTabs)?.key);

  const isAcquisitionDataset = useMemo(() => {
    return isAcquisition(dataset);
  }, [dataset]);

  return (
    <Card
      className={
        'dataset-card border-secondary ' +
        (isAcquisitionDataset ? '' : 'dataset-card-processed')
      }
    >
      <DatasetCardHeader
        {...properties}
        activeTab={activeTab}
        setActiveTab={setActiveTab}
        tabs={availableTabs}
      />
      {expanded ? (
        <Card.Body
          className={'p2 ' + (classNameBody || '')}
          style={{ overflow: 'auto', maxHeight: '80vh' }}
        >
          {activeTab ? (
            <RenderTabContent activeTab={activeTab} tabs={availableTabs} />
          ) : (
            children
          )}
        </Card.Body>
      ) : null}

      <DatasetCardFooter {...properties} />
    </Card>
  );
}
