import { getDatasetParamValue } from 'helpers';
import type { Dataset } from '@edata-portal/icat-plus-api';
import { Badge } from 'react-bootstrap';

export function DatasetFilesTabName({ dataset }: { dataset: Dataset }) {
  const totalElements = getDatasetParamValue(dataset, '__fileCount');
  return (
    <>
      Files
      <Badge
        bg="info"
        style={{
          marginLeft: 5,
        }}
      >
        {totalElements}
      </Badge>
    </>
  );
}
