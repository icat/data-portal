import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from '@tanstack/react-table';
import type { Entity, Parameter } from '@edata-portal/icat-plus-api';
import { TanstackBootstrapTable } from 'components/table/TanstackBootstrapTable';
import { sortParameters } from 'helpers';
import { useMemo } from 'react';
import { Row, Col } from 'react-bootstrap';

export function MetadataTab({
  entity,
  searchable = true,
  filter = () => true,
}: {
  entity: Entity;
  searchable?: boolean;
  filter?: (p: Parameter) => boolean;
}) {
  const metadata = useMemo(
    () => sortParameters(entity.parameters.filter(filter)),
    [entity.parameters, filter],
  );

  const table = useReactTable<Parameter>({
    data: metadata,
    columns: [
      {
        accessorKey: 'name',
        header: 'Name',
        footer: 'Name',
        enableColumnFilter: searchable,
      },
      {
        accessorKey: 'value',
        header: 'Value',
        footer: 'Value',
        enableColumnFilter: searchable,
      },
      {
        accessorKey: 'units',
        header: 'Units',
        footer: 'Units',
        enableColumnFilter: false,
      },
    ],
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getRowId: (row) => row.id.toString(),
    autoResetAll: false,
  });

  if (metadata.length === 0) {
    return null;
  }

  return (
    <Row>
      <Col>
        <TanstackBootstrapTable dataLength={metadata.length} table={table} />
      </Col>
    </Row>
  );
}
