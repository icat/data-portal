import type { Dataset } from '@edata-portal/icat-plus-api';
import { DatasetCard } from 'components/dataset/DatasetCard';
import {
  GenericDatasetTabsConfig,
  getGenericDatasetTabs,
} from 'components/dataset/generic/genericDatasetTabs';
import { Tree } from 'components/layout';
import { useViewers } from 'context';
import { getDatasetName } from 'helpers';
import { useMemo } from 'react';

export function GenericDatasetCardViewer({
  dataset,
  viewerConfig,
  isTableCell,
}: {
  dataset: Dataset;
  viewerConfig?: (dataset: Dataset) => GenericDatasetTabsConfig;
  isTableCell: boolean;
}) {
  const viewers = useViewers();

  const tabs = useMemo(() => {
    if (!viewerConfig)
      return {
        summaryTab: true,
        filesTab: true,
        metadataTab: true,
        showPathInFilesTab: false,
      };
    return viewerConfig(dataset);
  }, [dataset, viewerConfig]);

  const datasetComponent = (
    <DatasetCard
      key={dataset.id}
      classNameBody="p-1"
      dataset={dataset}
      title={getDatasetName(dataset)}
      tabs={getGenericDatasetTabs(dataset, tabs)}
      hideDatasetTitle={isTableCell}
      hideDownloadButton={isTableCell}
      hideSelectionButton={isTableCell}
    />
  );
  if (!dataset.outputDatasets?.length) return datasetComponent;

  return (
    <Tree
      items={[
        {
          key: dataset.id,
          item: datasetComponent,
          children: dataset.outputDatasets.map((output) => ({
            key: output.id,
            item: viewers.viewDataset(output, 'details'),
          })),
        },
      ]}
    />
  );
}
