import type { Dataset } from '@edata-portal/icat-plus-api';
import { Gallery } from 'components/dataset/gallery';
import { GenericDatasetSummary } from 'components/dataset/generic/GenericDatasetSummary';
import { MetadataCategories, MetadataCategory } from 'components/metadata';
import {
  getFilesTab,
  getInstrummentTab,
  getMetadataTab,
  TabDefinition,
} from 'components/tabs';

export type GenericDatasetTabsConfig = {
  summaryTab:
    | {
        metadata: MetadataCategory[];
        showGallery: boolean;
      }
    | boolean;
  filesTab: boolean;
  showPathInFilesTab: boolean;
  metadataTab: boolean;
};

export function getGenericDatasetTabs(
  dataset: Dataset,
  config: GenericDatasetTabsConfig,
): TabDefinition[] {
  const summary =
    config.summaryTab === false
      ? undefined
      : {
          title: 'Summary',
          key: 'summary',
          content:
            config.summaryTab === true ? (
              <GenericDatasetSummary dataset={dataset} inDatasetCard />
            ) : (
              <div className="d-flex flex-row gap-2">
                <MetadataCategories
                  categories={config.summaryTab.metadata}
                  entity={dataset}
                />
                {config.summaryTab.showGallery && (
                  <Gallery dataset={dataset} inRow={false} />
                )}
              </div>
            ),
        };

  const instrumentTab = getInstrummentTab(dataset);
  return [
    summary,
    instrumentTab,
    config.filesTab
      ? getFilesTab(dataset, config.showPathInFilesTab, false)
      : undefined,
    config.metadataTab ? getMetadataTab(dataset) : undefined,
  ].filter((tab): tab is TabDefinition => tab !== undefined);
}
