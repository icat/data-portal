import type { Dataset } from '@edata-portal/icat-plus-api';
import { Gallery } from 'components/dataset/gallery';
import { MetadataTable } from 'components/metadata';
import { MetadataTableParameters } from 'components/models';
import { CopyValue } from 'components/utils';
import {
  getDatasetName,
  formatDateToDayAndTime,
  getDatasetParam,
  formatDuration,
  isAcquisition,
} from 'helpers';
import { Col, Row } from 'react-bootstrap';

const processedPreset = (dataset: Dataset, inDatasetCard?: boolean) => [
  ...(inDatasetCard
    ? []
    : [
        [
          { caption: 'Name', value: getDatasetName(dataset) },
          {
            caption: 'Location',
            component: <CopyValue type="value" value={dataset.location} />,
          },
        ],
      ]),
  [
    {
      caption: 'Sample',
      value: dataset.sampleName,
    },
    {
      caption: 'Processed on',
      value: formatDateToDayAndTime(dataset.startDate),
    },
    {
      caption: 'Elapsed time',
      value: formatDuration(
        getDatasetParam(dataset, '__elapsedTime')?.value,
        'clock',
      ),
    },
  ],
];

const rawPreset = (dataset: Dataset, inDatasetCard?: boolean) => [
  ...(inDatasetCard
    ? []
    : [
        [
          { caption: 'Name', value: getDatasetName(dataset) },
          {
            caption: 'Start',
            value: formatDateToDayAndTime(dataset.startDate),
          },
          {
            caption: 'Location',
            component: <CopyValue type="value" value={dataset.location} />,
          },
        ],
      ]),

  [
    {
      caption: 'Sample',
      value: dataset.sampleName,
    },
    {
      caption: 'Description',
      parameterName: 'Sample_description',
    },
    {
      caption: 'Technique',
      parameterName: 'definition',
    },
  ],
];

export function GenericDatasetSummary({
  dataset,
  inDatasetCard,
}: {
  dataset: Dataset;
  inDatasetCard?: boolean;
}) {
  const parameters: MetadataTableParameters[] = isAcquisition(dataset)
    ? rawPreset(dataset, inDatasetCard)
    : processedPreset(dataset, inDatasetCard);

  return (
    <Row className="g-2">
      {parameters.map((params, index) => (
        <Col key={index} xs={'auto'}>
          <small>
            <MetadataTable entity={dataset} parameters={params} />
          </small>
        </Col>
      ))}
      <Gallery dataset={dataset} inRow={false} />
    </Row>
  );
}
