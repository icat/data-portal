import type { Dataset } from '@edata-portal/icat-plus-api';
import { GenericDatasetCardViewer } from 'components/dataset/generic/GenericDatasetCardViewer';
import { GenericDatasetTabsConfig } from 'components/dataset/generic/genericDatasetTabs';

export default function GenericDatasetTableCellViewer({
  dataset,
  viewerConfig,
}: {
  dataset: Dataset;
  viewerConfig?: (dataset: Dataset) => GenericDatasetTabsConfig;
}) {
  return (
    <GenericDatasetCardViewer
      dataset={dataset}
      viewerConfig={viewerConfig}
      isTableCell={true}
    />
  );
}
