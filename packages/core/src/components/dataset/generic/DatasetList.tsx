import {
  Dataset,
  DATASET_LIST_ENDPOINT,
  DATASET_TYPE_ACQUISITION,
  Investigation,
  Sample,
  SAMPLE_LIST_ENDPOINT,
  SAMPLE_PAGE_ENDPOINT,
  useAsyncFetchEndpoint,
} from '@edata-portal/icat-plus-api';
import { useQueryClient } from '@tanstack/react-query';
import { Loading, NoData } from 'components/utils';
import { DatasetViewerType, useViewers } from 'context';
import { formatDateToDay } from 'helpers';
import { PaginationMenu, useEndpointPagination, useParam } from 'hooks';
import { LogoHeaderType } from 'components/utils/LogoHeader';
import React, {
  Suspense,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export function DatasetList(
  args: {
    instrumentName?: string;
    investigationId?: string;
    showInvestigation?: boolean;
    parameterFilter?: string;
    datasetViewerType?: DatasetViewerType;
    paginationBarLogo?: LogoHeaderType;
  } & (
    | {
        groupBy: 'dataset';
        datasetIds?: (string | number)[];
        datasets?: Dataset[];
        nested?: boolean;
        search?: string;
        sampleId?: string;
      }
    | {
        groupBy: 'sample';
        sampleIds?: (string | number)[];
        samples?: Sample[];
      }
  ),
) {
  // State to handle scrolling to a specific sample
  const [scrollToSample, setScrollToSample] = useParam('scrollToSample', '');
  // Callback to reset scroll state after scrolling is done
  const onScrollDone = useCallback(() => {
    setScrollToSample(undefined);
  }, [setScrollToSample]);

  // Conditionally render datasets or samples based on `groupBy` argument
  switch (args.groupBy) {
    case 'dataset':
      if (args.datasets) {
        return (
          <DatasetObjectList
            datasetViewerType={args.datasetViewerType}
            datasets={args.datasets}
            {...args}
          />
        );
      }
      return (
        <Suspense fallback={<Loading />}>
          <LoadDatasetListByDataset {...args} />
        </Suspense>
      );
    case 'sample':
      if (args.samples) {
        return (
          <SampleObjectList
            samples={args.samples}
            {...args}
            scrollToSampleId={
              scrollToSample?.length ? scrollToSample : undefined
            }
            onScrollDone={onScrollDone}
          />
        );
      }
      return (
        <Suspense fallback={<Loading />}>
          <LoadDatasetListBySamples
            {...args}
            scrollToSampleId={
              scrollToSample?.length ? scrollToSample : undefined
            }
            onScrollDone={onScrollDone}
          />
        </Suspense>
      );
  }
}

// Component to load sample-based dataset lists dynamically
function LoadDatasetListBySamples({
  instrumentName,
  investigationId,
  parameterFilter,
  sampleIds,
  showInvestigation,
  scrollToSampleId,
  onScrollDone,
  paginationBarLogo,
}: {
  instrumentName?: string;
  investigationId?: string;
  parameterFilter?: string;
  sampleIds?: (string | number)[];
  showInvestigation?: boolean;
  scrollToSampleId?: string;
  onScrollDone?: () => void;
  paginationBarLogo?: LogoHeaderType;
}) {
  // Construct API parameters
  const sampleListParams = useMemo(
    () => ({
      ...(investigationId ? { investigationId: Number(investigationId) } : {}),
      ...(instrumentName ? { instrumentName } : {}),
      ...(parameterFilter ? { parameters: parameterFilter } : {}),
      ...(sampleIds?.length ? { sampleIds: sampleIds.join(',') } : {}),
      includeDatasets: false,
      sortBy: 'DATE' as const,
      sortOrder: -1 as const,
    }),
    [instrumentName, investigationId, parameterFilter, sampleIds],
  );

  const samples = useEndpointPagination({
    endpoint: SAMPLE_LIST_ENDPOINT,
    params: sampleListParams,
    paginationParams: {
      paginationKey: 'samples',
    },
  });

  const findSamplePage = useAsyncFetchEndpoint(SAMPLE_PAGE_ENDPOINT);

  const [lookingForSample, setLookingForSample] = useState(false);

  useEffect(() => {
    if (scrollToSampleId) {
      setLookingForSample(true);
      findSamplePage({
        sampleId: scrollToSampleId,
        ...sampleListParams,
        ...samples.pagination.queryParams,
      }).then((page) => {
        setLookingForSample(false);
        if (page?.page?.currentPage) {
          samples.goToPage(page.page.currentPage);
        }
      });
    }
  }, [scrollToSampleId]); // eslint-disable-line react-hooks/exhaustive-deps

  if (lookingForSample) {
    return <Loading />;
  }

  return (
    <Col>
      <PaginationMenu
        {...samples}
        showScrollToBottom
        logo={paginationBarLogo}
      />
      <Row>
        <SampleObjectList
          samples={samples.data}
          showInvestigation={showInvestigation}
          scrollToSampleId={lookingForSample ? undefined : scrollToSampleId}
          onScrollDone={onScrollDone}
        />
      </Row>
      <PaginationMenu {...samples} showScrollToTop />
    </Col>
  );
}

function SampleObjectList({
  samples,
  showInvestigation = false,
  scrollToSampleId,
  onScrollDone,
}: {
  samples: Sample[];
  showInvestigation?: boolean;
  scrollToSampleId?: string;
  onScrollDone?: () => void;
}) {
  const viewers = useViewers();

  const scrollRef = React.useRef<HTMLDivElement>(null);

  const [highlightedSampleId, setHighlightedSampleId] = useState<string>();

  const queryClient = useQueryClient();

  const doScroll = useCallback(() => {
    if (scrollToSampleId) {
      scrollRef.current?.scrollIntoView({
        behavior: 'instant',
        block: 'start',
      });
    }
  }, [scrollToSampleId]);

  useEffect(() => {
    if (!scrollRef.current || !scrollToSampleId?.length) return;
    setHighlightedSampleId(scrollToSampleId);
    doScroll();
    const interval = setInterval(() => {
      doScroll();
      if (!queryClient.isFetching()) {
        onScrollDone && onScrollDone();
      }
    }, 500);
    return () => clearInterval(interval);
  }, [doScroll, onScrollDone, queryClient, scrollToSampleId]);

  if (samples.length === 0) {
    return (
      <Col>
        <NoData />
      </Col>
    );
  }

  let lastInvestigationId: number | null = null;

  return (
    <Col>
      {samples.map((sample) => {
        const isScroll = scrollToSampleId === sample.id.toString();
        const highlighted = highlightedSampleId === sample.id.toString();
        const res = (
          <React.Fragment key={sample.id}>
            {showInvestigation &&
              lastInvestigationId !== sample.investigation.id && (
                <InvestigationLink
                  key={sample.investigation.id}
                  investigation={sample.investigation}
                />
              )}
            <Row
              key={sample.id}
              className={
                'mb-4' +
                (highlighted ? ' bg-light border border-info py-2' : '')
              }
              ref={isScroll ? scrollRef : null}
            >
              <Col>{viewers.viewSample(sample)}</Col>
            </Row>
          </React.Fragment>
        );
        lastInvestigationId = sample.investigation.id;
        return res;
      })}
    </Col>
  );
}

function LoadDatasetListByDataset({
  instrumentName,
  investigationId,
  parameterFilter,
  datasetIds,
  nested = true,
  search,
  sampleId,
  showInvestigation,
  datasetViewerType,
  logoHeader,
}: {
  instrumentName?: string;
  investigationId?: string;
  parameterFilter?: string;
  datasetIds?: (string | number)[];
  nested?: boolean;
  search?: string;
  sampleId?: string;
  showInvestigation?: boolean;
  datasetViewerType?: DatasetViewerType;
  logoHeader?: LogoHeaderType;
}) {
  const datasets = useEndpointPagination({
    endpoint: DATASET_LIST_ENDPOINT,
    params: {
      ...(investigationId ? { investigationIds: investigationId } : {}),
      ...(instrumentName ? { instrumentName } : {}),
      ...(parameterFilter ? { parameters: parameterFilter } : {}),
      ...(datasetIds?.length ? { datasetIds: datasetIds.join(',') } : {}),
      ...(datasetIds?.length ? {} : { datasetType: DATASET_TYPE_ACQUISITION }),
      sortBy: 'STARTDATE',
      sortOrder: -1,
      nested: nested,
      ...(search ? { search } : {}),
      ...(sampleId ? { sampleId } : {}),
    },
    paginationParams: {
      paginationKey: 'datasets',
    },
  });

  return (
    <Col>
      <PaginationMenu {...datasets} showScrollToBottom logo={logoHeader} />
      <Row>
        <DatasetObjectList
          datasets={datasets.data}
          showInvestigation={showInvestigation}
          datasetViewerType={datasetViewerType}
        />
      </Row>

      <PaginationMenu {...datasets} showScrollToTop />
    </Col>
  );
}

function DatasetObjectList({
  datasets,
  showInvestigation = false,
  datasetViewerType = 'details',
}: {
  datasets: Dataset[];
  showInvestigation?: boolean;
  datasetViewerType?: DatasetViewerType;
}) {
  const viewers = useViewers();

  if (datasets.length === 0) {
    return (
      <Col>
        <NoData />
      </Col>
    );
  }

  let lastInvesigationId: number | null = null;

  return (
    <Col>
      {datasets.map((dataset) => {
        const res = (
          <React.Fragment key={dataset.id}>
            {showInvestigation &&
              lastInvesigationId !== dataset.investigation.id && (
                <InvestigationLink
                  key={dataset.investigation.id}
                  investigation={dataset.investigation}
                  instrumentName={dataset.instrumentName}
                />
              )}
            <Row key={dataset.id} className="mb-4">
              <Col>{viewers.viewDataset(dataset, datasetViewerType)}</Col>
            </Row>
          </React.Fragment>
        );
        lastInvesigationId = dataset.investigation.id;
        return res;
      })}
    </Col>
  );
}

function InvestigationLink({
  investigation,
  instrumentName,
}: {
  investigation: Investigation;
  instrumentName?: string;
}) {
  const navigateToInvestigation = () => {
    return `/investigation/${investigation.id}/datasets`;
  };

  return (
    <Row
      key={investigation.id}
      style={{
        marginBottom: '1rem',
      }}
    >
      <Col className="text-center">
        <Link to={navigateToInvestigation()}>
          <Button variant="link" className="p-0">
            {`${investigation?.name} (${formatDateToDay(
              investigation?.startDate,
            )} on ${instrumentName || investigation.instrument?.name})`}
          </Button>
        </Link>
      </Col>
    </Row>
  );
}
