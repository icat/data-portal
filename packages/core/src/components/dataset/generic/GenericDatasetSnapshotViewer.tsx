import type { Dataset } from '@edata-portal/icat-plus-api';
import { DatasetTypeIcon } from 'components/dataset/DatasetTypeIcon';
import { DatasetImage } from 'components/dataset/gallery';
import {
  GenericDatasetTabsConfig,
  getGenericDatasetTabs,
} from 'components/dataset/generic/genericDatasetTabs';
import { DatasetSnapshot } from 'components/dataset/snapshot';
import { MetadataTable } from 'components/metadata';
import { MetadataTableParameters } from 'components/models';
import { useConfig } from 'context';
import {
  getGalleryItemByName,
  getGalleryItems,
  isImageViewer,
  getDatasetName,
} from 'helpers';

import { useMemo } from 'react';

export type GenericDatasetSnapshotViewerConfig = (dataset: Dataset) => {
  snapshot: SnapshotConfig;
  details: GenericDatasetTabsConfig | false;
};

export type SnashoptConfigItem =
  | { type: 'metadata'; metadata: MetadataTableParameters }
  | { type: 'gallery'; name: string | RegExp };

export type SnapshotConfig = {
  main?: SnashoptConfigItem[];
  showGallery: boolean;
};

function GenericDatasetSnapshotItemViewer({
  dataset,
  viewerItemConfig,
  viewerConfig,
}: {
  dataset: Dataset;
  viewerItemConfig: SnashoptConfigItem;
  viewerConfig: GenericDatasetSnapshotViewerConfig;
}): JSX.Element {
  const datasetConfig = useMemo(
    () => viewerConfig(dataset),
    [dataset, viewerConfig],
  );

  const showWindow = useMemo(() => {
    return !!datasetConfig.details;
  }, [datasetConfig]);

  if (viewerItemConfig.type === 'metadata') {
    return (
      <div className="w-100">
        <MetadataTable
          entity={dataset}
          parameters={viewerItemConfig.metadata}
        />
      </div>
    );
  }
  if (viewerItemConfig.type === 'gallery') {
    const mainGallery =
      viewerItemConfig.type === 'gallery'
        ? getGalleryItemByName(dataset, viewerItemConfig.name)
        : undefined;

    if (mainGallery) {
      return (
        <DatasetImage
          name={mainGallery.name}
          resourceId={mainGallery.id}
          displayFileName={false}
          zoomable={!showWindow}
        />
      );
    }
  }
  return <></>;
}
/**
 * The GenericDatasetSnapshotViewer component is used to display a detailed snapshot view of a dataset. This includes metadata, an optional main gallery item, additional gallery items, and optional details in tab format. It leverages other components like DatasetSnapshot, DatasetImage, and MetadataTable to build a comprehensive dataset viewer.
 * @param dataset The dataset object to be displayed
 * @param viewerConfig A function that generates configuration for the viewer based on the given dataset. The configuration specifies how the snapshot and optional details are displayed.
 * @returns
 */
export function GenericDatasetSnapshotViewer({
  dataset,
  viewerConfig,
}: {
  dataset: Dataset;
  viewerConfig: GenericDatasetSnapshotViewerConfig;
}) {
  const config = useConfig();
  const datasetConfig = useMemo(
    () => viewerConfig(dataset),
    [dataset, viewerConfig],
  );
  const showWindow = useMemo(() => {
    return !!datasetConfig.details;
  }, [datasetConfig]);

  /** Resolve the config of the items that need to be displayed */
  let items = viewerConfig(dataset).snapshot.main?.map((item, i) => (
    <GenericDatasetSnapshotItemViewer
      key={i}
      dataset={dataset}
      viewerConfig={viewerConfig}
      viewerItemConfig={item}
    />
  ));

  const primaryItems = items?.map((e) => {
    return {
      element: e,
      showWindow,
    };
  });

  const gallery = getGalleryItems(dataset).filter((item) =>
    isImageViewer(item.name, config),
  );

  const galleryItems = useMemo(() => {
    return gallery.map((item) => (
      <DatasetImage
        key={item.id}
        name={item.name}
        resourceId={item.id}
        displayFileName={false}
        zoomable={!showWindow}
      />
    ));
  }, [gallery, showWindow]);

  return (
    <DatasetSnapshot
      dataset={dataset}
      width={{
        min: 250,
        max: 350,
      }}
      title={getDatasetName(dataset)}
      extraHeader={<DatasetTypeIcon dataset={dataset} />}
      primaryItems={primaryItems}
      secondaryItems={galleryItems.map((element) => ({
        element,
        showWindow,
      }))}
      windowTabs={
        datasetConfig.details
          ? getGenericDatasetTabs(dataset, datasetConfig.details)
          : undefined
      }
    />
  );
}
