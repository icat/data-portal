import type { Dataset } from '@edata-portal/icat-plus-api';
import { Col } from 'react-bootstrap';
import { DatasetImage } from 'components/dataset/gallery/DatasetImage';
import { GalleryItem, getGalleryItems } from 'helpers/gallery';

/**
 * This function allows to filter by extension
 * @param supportedImagesFormat Example: ["jpeg", "gif"]
 * @param filepath
 * @returns
 **/
function filterByFormat(
  supportedImagesFormat: string[] | undefined,
  filepath: string,
): boolean {
  if (supportedImagesFormat === undefined) return false;

  const supportedExtensions: string[] = supportedImagesFormat.map(
    (format) => `.${format}`,
  );

  const fileExtension = filepath.split('.').pop()?.toLowerCase();
  return supportedExtensions.includes(`.${fileExtension}`);
}

/**
 * Based on the input this function builds a set of <Col> with an image pointing to each resource
 * @param resources
 * @param dataset
 * @param filter
 * @param supportedImagesFormat
 * @returns
 */
export default function DatasetImages({
  resources,
  dataset,
  filter = () => true,
  supportedImagesFormat,
  displayFileName = false,
}: {
  dataset?: Dataset;
  resources?: GalleryItem[];
  filter?: (name: string) => boolean;
  supportedImagesFormat?: string[];
  displayFileName?: boolean;
}) {
  if (dataset && !resources) {
    resources = getGalleryItems(dataset);
  }

  return (
    <>
      {resources &&
        resources
          .filter((resource) =>
            filterByFormat(supportedImagesFormat, resource.name),
          )
          .filter((resource) => filter(resource.name))
          .map((resource) => (
            <Col
              xs={'auto'}
              key={resource.id + '_img'}
              style={{
                maxWidth: 200,
              }}
            >
              <DatasetImage
                name={resource.name}
                resourceId={resource.id}
                displayFileName={displayFileName}
              />
            </Col>
          ))}
    </>
  );
}
