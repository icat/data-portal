import type { Dataset } from '@edata-portal/icat-plus-api';
import { Row } from 'react-bootstrap';
import DatasetImages from 'components/dataset/gallery/DatasetImages';
import { useConfig } from 'context';
import {
  GalleryItem,
  getGalleryItemByName,
  getGalleryItemRecursiveByName,
} from 'helpers/gallery';

const configurationToResources = (
  dataset: Dataset,
  configuration: GalleryResourceConfiguration[],
): GalleryItem[] => {
  const results: GalleryItem[] = [];
  configuration.forEach((c) => {
    const resourceImage = c.datasetName
      ? getGalleryItemRecursiveByName(dataset, c.resourceName, c.datasetName)
      : getGalleryItemByName(dataset, c.resourceName);
    if (resourceImage !== undefined) {
      results.push(resourceImage);
    }
  });

  return results;
};

export type GalleryResourceConfiguration = {
  datasetName?: string;
  resourceName: string;
};

export function Gallery({
  dataset,
  resources,
  configuration,
  filter = () => true,
  displayFileName = false,
  inRow = true,
}: {
  dataset?: Dataset;
  resources?: GalleryItem[];
  filter?: (name: string) => boolean;
  configuration?: GalleryResourceConfiguration[];
  displayFileName?: boolean;
  inRow?: boolean;
}) {
  const config = useConfig();
  const supportedImagesFormat = config.ui.galleryViewer.fileExtensions;
  if (configuration && dataset) {
    resources = configurationToResources(dataset, configuration);
  }
  const content = (
    <DatasetImages
      resources={resources}
      dataset={dataset}
      filter={filter}
      supportedImagesFormat={supportedImagesFormat}
      displayFileName={displayFileName}
    />
  );

  return inRow ? <Row className="g-2">{content}</Row> : content;
}
