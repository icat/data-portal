import {
  useEndpointURL,
  DOWNLOAD_FILE_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { ZoomableImage } from 'components/image/ZoomableImage';

export function DatasetImage({
  resourceId,
  name,
  displayFileName = false,
  zoomable = true,
}: {
  resourceId: string;
  name: string;
  displayFileName?: boolean;
  zoomable?: boolean;
}) {
  const url = useEndpointURL(DOWNLOAD_FILE_ENDPOINT);

  return (
    <ZoomableImage
      src={url({
        resourceId: resourceId,
      })}
      alt={name}
      legend={displayFileName ? name : undefined}
      zoomable={zoomable}
    />
  );
}
