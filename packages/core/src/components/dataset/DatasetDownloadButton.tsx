import {
  type Dataset,
  DATASET_STATUS_RESTORING,
  DATASET_STATUS_ARCHIVED,
} from '@edata-portal/icat-plus-api';
import {
  faDownload,
  faSyncAlt,
  faWarning,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DatasetDownloadModal } from 'components/dataset/DatasetDownloadModal';
import { useDatasetStatus } from 'hooks';
import { Suspense, useState } from 'react';
import { Button } from 'react-bootstrap';

export function DatasetDownloadButton({
  dataset,
  onClick,
  doShowModal = true,
}: {
  dataset: Dataset;
  onClick?: () => void;
  doShowModal?: boolean;
}) {
  const [showModalInternal, setShowModalInternal] = useState(false);

  return (
    <>
      <Button
        size="sm"
        variant="primary"
        onClick={() => {
          if (onClick) {
            onClick();
          } else {
            setShowModalInternal(true);
          }
        }}
      >
        <FontAwesomeIcon icon={faDownload} className="me-2" />
        Download
        <Suspense>
          <ShowStatus dataset={dataset} />
        </Suspense>
      </Button>
      {showModalInternal && doShowModal && (
        <Suspense fallback={null}>
          <DatasetDownloadModal
            dataset={dataset}
            onClose={() => {
              setShowModalInternal(false);
            }}
          />
        </Suspense>
      )}
    </>
  );
}

function ShowStatus({ dataset }: { dataset: Dataset }) {
  const status = useDatasetStatus(dataset);

  if (status === DATASET_STATUS_ARCHIVED) {
    return <FontAwesomeIcon icon={faWarning} className="text-warning ms-2" />;
  }
  if (status === DATASET_STATUS_RESTORING) {
    return (
      <FontAwesomeIcon spin icon={faSyncAlt} className="text-warning ms-2" />
    );
  }
  return null;
}
