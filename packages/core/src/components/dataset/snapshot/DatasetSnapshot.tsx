import type { Dataset } from '@edata-portal/icat-plus-api';
import { faMagnifyingGlassPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DatasetActionsButton } from 'components/dataset/DatasetActions';
import { DatasetWindow } from 'components/dataset/DatasetWindow';
import { SelectButton } from 'components/inputs/SelectButton';
import { Window } from 'components/layout';
import type { TabDefinition } from 'components/tabs';
import { Button, EllipsisView } from 'components/utils';
import { first } from 'helpers';
import { useCallback, useMemo, useState } from 'react';
import { Card } from 'react-bootstrap';

export type DatasetSnapshotItem = {
  onClick?: () => void;
  showWindow?: string | boolean; //string to show specific tab or boolean to show first tab/content
  element: JSX.Element;
};

export type DatasetSnapshotProps = {
  primaryItems?: DatasetSnapshotItem[];
  secondaryItems?: DatasetSnapshotItem[];
  windowTabs?: TabDefinition[];
  width:
    | number
    | {
        min: number;
        max: number;
      };
  title: JSX.Element | string;
  windowTitle?: JSX.Element | string;
  windowHideDatasetButtons?: boolean;
  dataset: Dataset;
  extraHeader?: JSX.Element;
};

export function DatasetSnapshot(props: DatasetSnapshotProps) {
  const [showWindow, setShowWindow] = useState<string | boolean>(false);

  const updateWindow = useCallback((newValue: string | boolean) => {
    setShowWindow((v) => {
      if (v === false) {
        return newValue;
      }
      return false;
    });
  }, []);

  const hasWindow = useMemo(
    () => !!props.windowTabs?.length,
    [props.windowTabs],
  );

  const defaultTabKey = useMemo(() => {
    if (!props.windowTabs) return false;
    return first(props.windowTabs.filter((v) => !v.hidden))?.key;
  }, [props.windowTabs]);

  const primaryItems = useMemo(() => {
    const filtered = props.primaryItems?.filter((item) => !!item) || [];
    if (filtered.length > 0) return filtered;
    return [
      {
        element: <i>Snapshot not defined</i>,
        showWindow: hasWindow,
      },
    ];
  }, [hasWindow, props.primaryItems]);

  return (
    <>
      <Card
        className="text-center"
        style={{
          maxWidth:
            typeof props.width === 'object'
              ? props.width.max * primaryItems.length
              : props.width * primaryItems.length,
          minWidth:
            typeof props.width === 'object'
              ? props.width.min * primaryItems.length
              : props.width * primaryItems.length,
        }}
      >
        <Card.Header
          className="bg-white d-flex flex-row justify-content-between align-items-center flex-nowrap py-0 px-1"
          style={{
            overflow: 'hidden',
          }}
        >
          <div className="d-flex flex-row align-items-center flex-nowrap gap-1">
            <DatasetActionsButton dataset={props.dataset} />
            <EllipsisView side="end">
              <strong>{props.title}</strong>
            </EllipsisView>
            {props.extraHeader}
          </div>

          <div className="d-flex flex-row justify-content-end align-items-center flex-nowrap">
            {hasWindow && defaultTabKey ? (
              <Button
                className="p-0 mx-1"
                size="sm"
                variant="link"
                onClick={() => {
                  defaultTabKey && updateWindow(defaultTabKey);
                }}
              >
                <FontAwesomeIcon
                  icon={faMagnifyingGlassPlus}
                  className="text-info"
                />
              </Button>
            ) : null}
            <SelectButton ids={[props.dataset.id]} type="dataset" />
          </div>
        </Card.Header>
        <Card.Body className="p-0">
          <div className="d-flex flex-column h-100 justify-content-between">
            <div className="d-flex flex-row h-100">
              {primaryItems.map((item, index) => (
                <SnapshotItem
                  key={index}
                  item={item}
                  updateWindow={updateWindow}
                />
              ))}
            </div>

            {!!props.secondaryItems?.length && (
              <div
                className="d-flex flex-row"
                style={{
                  height: 40,
                }}
              >
                {props.secondaryItems.map((item, index) => (
                  <SnapshotItem
                    key={index}
                    item={item}
                    updateWindow={updateWindow}
                    background
                  />
                ))}
              </div>
            )}
          </div>
        </Card.Body>
      </Card>
      {!!showWindow &&
        (props.windowHideDatasetButtons ? (
          <Window
            title={props.windowTitle ?? props.title}
            tabs={props.windowTabs}
            initialTab={typeof showWindow === 'string' ? showWindow : undefined}
            onClose={() => {
              setShowWindow(false);
            }}
          />
        ) : (
          <DatasetWindow
            dataset={props.dataset}
            title={props.windowTitle ?? props.title}
            tabs={props.windowTabs}
            initialTab={typeof showWindow === 'string' ? showWindow : undefined}
            onClose={() => {
              setShowWindow(false);
            }}
          />
        ))}
    </>
  );
}

function SnapshotItem({
  item,
  updateWindow,
  background = false,
}: {
  item: DatasetSnapshotItem;
  updateWindow: (show: string | boolean) => void;
  background?: boolean;
}) {
  const [hovering, setHovering] = useState(false);

  const clickable =
    item.onClick !== undefined ||
    typeof item.showWindow === 'string' ||
    item.showWindow === true;

  return (
    <div
      className="d-flex h-100 w-100"
      style={{
        padding: 1,
        cursor: clickable ? 'pointer' : undefined,
      }}
      onMouseEnter={() => setHovering(true)}
      onMouseLeave={() => setHovering(false)}
      onClick={
        clickable
          ? () => {
              item.onClick && item.onClick();
              item.showWindow && updateWindow(item.showWindow);
            }
          : undefined
      }
    >
      <div
        className={`d-flex h-100 w-100 border rounded justify-content-center align-items-center ${
          background ? 'bg-light' : ''
        }
      ${hovering && clickable ? 'border-info' : 'border-light'}`}
        style={{
          padding: 1,
          pointerEvents: clickable ? 'none' : 'auto',
        }}
      >
        {item.element}
      </div>
    </div>
  );
}
