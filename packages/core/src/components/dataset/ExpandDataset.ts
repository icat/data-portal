import type { Dataset } from '@edata-portal/icat-plus-api';

export interface ExpandDataset extends Dataset {
  subRows?: ExpandDataset[];
}
