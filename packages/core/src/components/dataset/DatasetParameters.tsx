import { Col, Container, Row } from 'react-bootstrap';
import type { Dataset } from '@edata-portal/icat-plus-api';

export function DatasetParameters({
  dataset,
  formatName = true,
  filterPrefix = undefined,
  exclude = [],
}: {
  dataset: Dataset;
  formatName?: boolean;
  filterPrefix?: string;
  exclude?: string[];
}) {
  const parameters = dataset.parameters
    .filter((parameter) => !exclude.includes(parameter.name))
    .filter((parameter) => {
      if (filterPrefix === undefined) return true;
      else return parameter.name.startsWith(filterPrefix);
    })
    .sort((a, b) => a.name.localeCompare(b.name))
    .map(
      (parameter) =>
        ({
          ...parameter,
          name: filterPrefix
            ? parameter.name.slice(filterPrefix.length)
            : parameter.name,
        }) as typeof parameter,
    );

  return (
    <Container fluid className={'overflow-auto'}>
      <Row>
        {parameters.map((parameter) => (
          <DatasetParameter
            key={parameter.name}
            parameter={parameter}
            formatName={formatName}
          />
        ))}
      </Row>
    </Container>
  );
}

export function DatasetParameter({
  parameter,
  formatName = true,
}: {
  parameter: {
    name: string;
    value: string;
    id: number;
    units?: string | undefined;
  };
  formatName?: boolean;
}) {
  const name = formatName
    ? parameter.name.replaceAll('_', ' ')
    : parameter.name;
  const formattedName = formatName
    ? name[0].toUpperCase() + name.slice(1)
    : name;

  return (
    <Row>
      <Col xs={'auto'} style={{ padding: 2 }}>
        <span style={{ display: 'inline-block', width: '180px' }}>
          {formattedName}
        </span>
        <span style={{ width: '100px' }}>
          <strong style={{ width: '150px' }}>{parameter.value}</strong>
        </span>
        {parameter.units !== undefined &&
          parameter.units !== 'NA' &&
          parameter.units !== 'N/A' && <i> {parameter.units}</i>}
      </Col>
    </Row>
  );
}
