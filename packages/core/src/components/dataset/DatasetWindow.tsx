import type { Dataset } from '@edata-portal/icat-plus-api';
import { DatasetActionsButton } from 'components/dataset/DatasetActions';
import { Window, type WindowProperties } from 'components/layout/Window';

export interface DatasetWindowProperties extends WindowProperties {
  dataset: Dataset; // The dataset to show
}

export function DatasetWindow(properties: DatasetWindowProperties) {
  const { dataset } = properties;

  return (
    <Window
      {...properties}
      actionsIcons={
        <>
          {properties.actionsIcons}
          <DatasetActionsButton dataset={dataset} />
        </>
      }
    />
  );
}
