import { faRepeat } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  useIsFetching,
  useIsMutating,
  useQueryClient,
} from '@tanstack/react-query';
import { Button, OverlayTrigger, Spinner, Tooltip } from 'react-bootstrap';

export function LoadingIndicator() {
  const isLoading = useIsFetching();
  const isMutating = useIsMutating();

  const queryClient = useQueryClient();

  const reload = (
    <OverlayTrigger
      placement="top"
      overlay={
        <Tooltip id="tooltip-top">
          <strong>Refresh all data on this page.</strong>
        </Tooltip>
      }
    >
      <Button
        className="p-0"
        variant="link"
        onClick={() => {
          queryClient.invalidateQueries({ type: 'active' });
        }}
        disabled={!!isLoading}
      >
        <FontAwesomeIcon icon={faRepeat} />
      </Button>
    </OverlayTrigger>
  );

  const status =
    isLoading || isMutating ? <Spinner className="mb-1" size={'sm'} /> : reload;

  return <div className="d-flex justify-content-end">{status}</div>;
}
