import { PropsWithChildren, Suspense, useMemo } from 'react';
import { H5GroveProvider } from '@h5web/app';
import '@h5web/lib/styles.css';
import { useUser, useConfig, Loading } from '@edata-portal/core';

interface Props {
  dataFileId: number;
}

function H5Provider(props: PropsWithChildren<Props>) {
  const { dataFileId, children } = props;

  const user = useUser();
  const config = useConfig();
  const sessionId = user?.sessionId;

  const axiosConfig = useMemo(
    () => ({
      params: {
        // Processed by ICAT+
        sessionId,
        datafileId: dataFileId, // transformed to real path to file on disk
        // Forwarded to h5grove
        resolve_links: 'all', // respond with error if a link doesn't resolve
      },
    }),
    [sessionId, dataFileId],
  );

  return (
    <H5GroveProvider
      filepath={dataFileId.toString()}
      url={`${config.api.icat_url}h5grove`} // requests are proxied through ICAT+
      axiosConfig={axiosConfig}
    >
      <Suspense fallback={<Loading />}>{children}</Suspense>
    </H5GroveProvider>
  );
}

export default H5Provider;
