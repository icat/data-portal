import React from 'react';

export const H5Provider = React.lazy(() => import('./H5Provider'));
export const ImageVisForMXData = React.lazy(
  () => import('./ImageVisForMXData'),
);
