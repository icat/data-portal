import { useDataContext } from '@h5web/app';
import { HeatmapVis } from '@h5web/lib';
import ndarray from 'ndarray';
import { useMemo } from 'react';

interface Props {
  title: string;
  imageIndex: number;
}

function ImageVisForMXData(props: Props) {
  const { title, imageIndex } = props;

  const { entitiesStore, valuesStore } = useDataContext();
  const entity = entitiesStore.get('/entry/data/data');

  if (entity.kind !== 'dataset') {
    throw new Error(
      `Expected dataset at /entry/data/data but got ${entity.kind}`,
    );
  }

  if (entity.shape === null) {
    throw new Error(`Expected dataset with non-null shape at /entry/data/data`);
  }

  const data = valuesStore.get({
    dataset: entity as any,
    selection: `${imageIndex},:,:`,
  }) as Uint32Array;

  const dataArray = useMemo(
    () => ndarray(data, entity.shape!.slice(1)),
    [entity, data],
  );

  return (
    <div className="d-flex h-100 w-100">
      <HeatmapVis dataArray={dataArray} domain={[0, 20]} title={title} />
    </div>
  );
}

export default ImageVisForMXData;
