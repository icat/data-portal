# Data Portal

The Data Portal is a web interface that allows users to browse, visualize and access their data and public data.

You can find detailed documentation on the [Data Portal documentation page](https://icat.gitlab-pages.esrf.fr/data-portal/).
