
server {
  listen 0.0.0.0:80;
  gzip         on;
  gzip_vary    on;
  gzip_proxied any;
  gzip_comp_level 6;
  gzip_buffers 16 8k;
  gzip_http_version 1.1;
  gzip_min_length 1000;

  gzip_types text/plain application/javascript text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype;



  root /usr/share/nginx/html;
  index index.html index.htm;

  location / {
    try_files $uri $uri/ /index.html;
    add_header 'Access-Control-Allow-Origin' '*';

    expires           -1;

    # Security headers
    add_header Strict-Transport-Security "max-age=31536000" always; # force browser to switch to HTTPS (for 1 year)
    add_header X-Content-Type-Options "nosniff"; # prevent MIME type sniffing (i.e. believe `Content-Type` specified by server)
  }

  location ~* \.(?:css|js)$ {
    add_header 'Access-Control-Allow-Origin' '*';

    add_header        Cache-Control "no-cache, public, must-revalidate, proxy-revalidate";

    # Security headers
    add_header Strict-Transport-Security "max-age=31536000" always; # force browser to switch to HTTPS (for 1 year)
    add_header X-Content-Type-Options "nosniff"; # prevent MIME type sniffing (i.e. believe `Content-Type` specified by server)
  }

  location ~* \.(?:jpg|jpeg|gif|png|ico|xml|webp)$ {
    add_header 'Access-Control-Allow-Origin' '*';
    
    expires           5m;
    add_header        Cache-Control "public";

    # Security headers
    add_header Strict-Transport-Security "max-age=31536000" always; # force browser to switch to HTTPS (for 1 year)
    add_header X-Content-Type-Options "nosniff"; # prevent MIME type sniffing (i.e. believe `Content-Type` specified by server)
  }

  # Redirect errors so they can be handled in the front-end
  error_page 404             /;
  error_page 500 502 503 504 /;
}