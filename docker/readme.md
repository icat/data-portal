Build image for an app:

```sh
sudo docker build --build-arg APP=cryoet -t cryoet -f docker/apps/Dockerfile .
```

Build and run all apps:

```sh
sudo docker compose up --build
```

Apps are then accessible using a hostname prefix. For example:

`http://cryoet.localhost` or `http://logistics.localhost`
