import { defineConfig } from 'cypress';
import dotenv from 'dotenv';

const CONFIG_APP = 'apps/portal/';

dotenv.config({ path: `.env.local`, override: false });
dotenv.config({ path: `.env`, override: false });

dotenv.config({ path: CONFIG_APP + `.env.local`, override: false });
dotenv.config({ path: CONFIG_APP + `.env`, override: false });

export default defineConfig({
  defaultCommandTimeout: 30000,
  video: true,
  e2e: {
    retries: {
      openMode: undefined,
      runMode: 5,
    },
    baseUrl: process.env.CYPRESS_TEST_APP_URL || 'http://localhost:3000',
    setupNodeEvents(on, config) {
      on('before:browser:launch', (browser, launchOptions) => {
        launchOptions.args.push('--disable-gpu');
        return launchOptions;
      });
    },
    env: {
      ...process.env,
    },
  },
});
