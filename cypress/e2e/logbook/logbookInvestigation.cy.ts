import {
  LOGBOOK_EVENT_COUNT_ENDPOINT,
  LOGBOOK_EVENT_DATES_ENDPOINT,
  LOGBOOK_EVENT_LIST_ENDPOINT,
  LOGBOOK_TAG_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { login } from 'support/data/authentication';
import { goToInvestigation } from 'support/data/investigation';

function goToLogbook() {
  goToInvestigation({
    canAccess: true,
    released: false,
  });
  cy.getBySelector('investigation-logbook').click();
}

describe('Investigation logbook event list', () => {
  beforeEach(() => {
    login('admin');

    goToLogbook();

    stubAPICall({
      endpoint: LOGBOOK_EVENT_LIST_ENDPOINT,
      response: { fixture: 'logbook/events.json' },
      alias: 'getEvents',
    });
    stubAPICall({
      endpoint: LOGBOOK_EVENT_COUNT_ENDPOINT,
      response: { fixture: 'logbook/count.json' },
      alias: 'getEventCount',
    });
    stubAPICall({
      endpoint: LOGBOOK_EVENT_DATES_ENDPOINT,
      response: { fixture: 'logbook/dates.json' },
      alias: 'getEventDates',
    });
    stubAPICall({
      endpoint: LOGBOOK_TAG_LIST_ENDPOINT,
      response: { fixture: 'logbook/tag.json' },
      alias: 'getTags',
    });
  });

  it('should call all the endpoints', () => {
    cy.wait(['@getEvents', '@getEventCount', '@getEventDates', '@getTags']);
  });

  it('should display the logbook events', () => {
    cy.getBySelector('logbook-event').should('exist');
  });

  it('should display the event editor', () => {
    cy.getBySelector('logbook-event-edit-button')
      .should('exist')
      .scrollIntoView()
      .click({});

    //check tinymce visible and functional

    //can click bold button
    cy.get('[role=toolbar]').as('toolbar').scrollIntoView();
    cy.get('@toolbar').click();
    cy.get('button[aria-label=Bold]').as('bold').scrollIntoView();
    cy.get('@bold').click();

    cy.get('iframe[id*=tiny]').then((iframe) => {
      const body = iframe.contents().find('body');
      cy.wrap(body).as('body');

      //can write
      cy.get('@body').scrollIntoView();
      cy.get('@body').click({ force: true });
      cy.get('@body').type('This is a test', { force: true, delay: 100 });
      //can read
      cy.get('@body').contains('This is a test').should('exist');
    });
  });
});
