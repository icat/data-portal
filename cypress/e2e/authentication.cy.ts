import { LOGIN_ROLES, login, logout } from 'support/data/authentication';

describe('Authentication', () => {
  LOGIN_ROLES.forEach((role) => {
    it(`can login as ${role}`, () => {
      login(role);
    });
  });

  it('can logout', () => {
    login('admin');
    logout();
  });
});
