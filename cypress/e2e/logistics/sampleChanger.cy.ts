import {
  GET_TRACKING_SETUP_ENDPOINT,
  PARCEL_LIST_ENDPOINT,
  UPDATE_PARCEL_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { login } from 'support/data/authentication';
import { goToInvestigation } from 'support/data/investigation';

function goToChanger(type: string) {
  goToInvestigation({
    canAccess: true,
    released: false,
    canPrepare: true,
  });
  cy.getBySelector('investigation-prepare').click();
  cy.getBySelector('investigation-changer').click();
  cy.getBySelector(`changer-${type}-btn`).click();
}

['changer', 'simplified'].forEach((type) => {
  describe(`Investigation sample changer view:${type}`, () => {
    beforeEach(() => {
      login('principal_investigator');
      stubAPICall({
        endpoint: GET_TRACKING_SETUP_ENDPOINT,
        response: { fixture: 'logistics/setup/mxSetup.json' },
        alias: 'getSetup',
      });

      stubAPICall({
        endpoint: PARCEL_LIST_ENDPOINT,
        response: { fixture: 'logistics/parcel/mxParcels.json' },
        alias: 'getParcels',
      });
      goToChanger(type);
    });

    it('should call all the endpoints', () => {
      cy.wait(['@getSetup', '@getParcels']);
    });

    it('should display source and target items', () => {
      cy.getBySelector('changer-source-item').should('exist');
      cy.getBySelector('changer-target-empty').should('exist');
      cy.getBySelector('changer-target-assigned').should('exist');
    });

    it('should unload item', () => {
      stubAPICall({
        endpoint: UPDATE_PARCEL_ENDPOINT,
        response: { fixture: 'logistics/parcel/mxParcels.json' },
        alias: 'updateParcels',
      });

      cy.getBySelector('changer-target-assigned').then((e) => {
        const assignedCountBefore = e.length;
        cy.getBySelector('changer-target-unload').first().click();

        cy.wait('@updateParcels');

        cy.getBySelector('changer-target-assigned').should(
          'have.length',
          assignedCountBefore - 1,
        );
      });
    });

    it('should unload all', () => {
      stubAPICall({
        endpoint: UPDATE_PARCEL_ENDPOINT,
        response: { fixture: 'logistics/parcel/mxParcels.json' },
        alias: 'updateParcels',
      });

      cy.getBySelector('changer-unload-all').click();
      cy.wait('@updateParcels');
      cy.getBySelector('changer-target-assigned').should('not.exist');
    });

    it('should assign with drag', () => {
      stubAPICall({
        endpoint: UPDATE_PARCEL_ENDPOINT,
        response: { fixture: 'logistics/parcel/mxParcels.json' },
        alias: 'updateParcels',
      });

      cy.getBySelector('changer-target-assigned').then((e) => {
        const assignedCountBefore = e.length;

        cy.getBySelector('changer-source-item').first().trigger('dragstart');
        cy.getBySelector('changer-target-empty').first().trigger('drop');
        cy.wait('@updateParcels');

        cy.getBySelector('changer-target-assigned').should(
          'have.length',
          assignedCountBefore + 1,
        );
      });
    });

    it('should move with drag', () => {
      stubAPICall({
        endpoint: UPDATE_PARCEL_ENDPOINT,
        response: { fixture: 'logistics/parcel/mxParcels.json' },
        alias: 'updateParcels',
      });

      cy.getBySelector('changer-target-assigned').then((e) => {
        const assignedCountBefore = e.length;

        cy.getBySelector('changer-target-assigned')
          .first()
          .trigger('dragstart');
        cy.getBySelector('changer-target-empty').first().trigger('drop');
        cy.wait('@updateParcels');

        cy.getBySelector('changer-target-assigned').should(
          'have.length',
          assignedCountBefore,
        );
      });
    });
  });
});
