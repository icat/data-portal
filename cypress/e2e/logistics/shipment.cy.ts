import {
  CREATE_SHIPMENT_ENDPOINT,
  INVESTIGATION_ADDRESSES_ENDPOINT,
  SHIPMENT_LIST_ENDPOINT,
  UPDATE_SHIPMENT_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { login } from 'support/data/authentication';
import { goToInvestigation } from 'support/data/investigation';

describe(`Investigation create shipment`, () => {
  beforeEach(() => {
    login('principal_investigator');
    goToInvestigation({
      canAccess: true,
      released: false,
      canPrepare: true,
    });
    stubAPICall({
      endpoint: SHIPMENT_LIST_ENDPOINT,
      response: { fixture: 'emptyList.json' },
      alias: 'getShipments',
    });
    stubAPICall({
      endpoint: INVESTIGATION_ADDRESSES_ENDPOINT,
      response: { fixture: 'logistics/address/multipleAddress.json' },
      alias: 'getInvestigationAddresses',
    });
    cy.getBySelector('investigation-prepare').click();
  });

  it('should call all the endpoints', () => {
    cy.wait(['@getShipments', '@getInvestigationAddresses']);
  });

  it('should create a shipment', () => {
    stubAPICall({
      endpoint: CREATE_SHIPMENT_ENDPOINT,
      response: { fixture: 'logistics/shipment/basicshipment.json' },
      alias: 'createShipment',
    });
    cy.getBySelector('default-sender-address')
      .find('input')
      .click()
      .type('{enter}');
    cy.wait('@createShipment');
  });
});

describe(`Investigation update shipment`, () => {
  beforeEach(() => {
    login('principal_investigator');
    goToInvestigation({
      canAccess: true,
      released: false,
      canPrepare: true,
    });
    stubAPICall({
      endpoint: SHIPMENT_LIST_ENDPOINT,
      response: {
        fixture: 'logistics/shipment/basicshipment.json',
      },
      alias: 'getShipments',
    });
    stubAPICall({
      endpoint: INVESTIGATION_ADDRESSES_ENDPOINT,
      response: { fixture: 'logistics/address/multipleAddress.json' },
      alias: 'getInvestigationAddresses',
    });
    cy.getBySelector('investigation-prepare').click();
  });

  it('should call all the endpoints', () => {
    cy.wait(['@getShipments', '@getInvestigationAddresses']);
  });

  it('should show default sender and return addresses', () => {
    cy.getBySelector('default-sender-address').should('exist');
    cy.getBySelector('default-return-address').should('exist');
  });

  it('should update default sender and return addresses', () => {
    stubAPICall({
      endpoint: UPDATE_SHIPMENT_ENDPOINT,
      response: { fixture: 'logistics/shipment/basicshipment.json' },
      alias: 'updateShipment',
    });
    cy.getBySelector('default-sender-address')
      .find('input')
      .click()
      .type('{enter}');
    cy.wait('@updateShipment');
    cy.getBySelector('default-return-address')
      .find('input')
      .click()
      .type('{enter}');
    cy.wait('@updateShipment');
  });
});
