import { INVESTIGATION_USERS_ADD_ENDPOINT } from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { LoginRole, login } from 'support/data/authentication';
import {
  goToInvestigation,
  type InvestigationSetup,
} from 'support/data/investigation';

function goToInvestigationInfo(
  setup: InvestigationSetup & {
    role: LoginRole;
  },
) {
  login(setup.role);
  goToInvestigation(setup);
  cy.getBySelector('investigation-experiment').click();
}

describe('Investigation info', () => {
  it('reader can access released investigation info', () => {
    goToInvestigationInfo({
      canAccess: true,
      released: true,
      role: 'reader',
    });

    //test page content
    cy.selectorExists('investigation-details');
    cy.selectorExists('investigation-abstract');

    //test navigation options
    cy.selectorExists('investigation-statistics');
    cy.selectorExists('investigation-datasets');
    cy.selectorExists('investigation-logbook');
    cy.selectorNotExists('investigation-prepare');
  });

  it('reader cannot access unreleased investigation info', () => {
    goToInvestigationInfo({
      canAccess: false,
      released: false,
      role: 'reader',
    });

    //test page content
    cy.selectorExists('no-access-alert');
    cy.selectorExists('investigation-details');
    cy.selectorExists('investigation-abstract');

    //test navigation options
    cy.selectorNotExists('investigation-statistics');
    cy.selectorNotExists('investigation-datasets');
    cy.selectorNotExists('investigation-logbook');
    cy.selectorNotExists('investigation-prepare');
  });

  it('principal investigator can see prepare nav', () => {
    goToInvestigationInfo({
      canAccess: true,
      released: true,
      role: 'principal_investigator',
      canPrepare: true,
    });
    cy.selectorExists('investigation-prepare').should('exist');
  });

  it('principal investigator can add participant', () => {
    goToInvestigationInfo({
      canAccess: true,
      released: true,
      role: 'principal_investigator',
      canPrepare: true,
    });

    stubAPICall({
      endpoint: INVESTIGATION_USERS_ADD_ENDPOINT,
      response: { statusCode: 200 },
      matchBody: (body) => {
        return !!Object.keys(body).length;
      },
      alias: 'postInvestigationUsers',
    });

    cy.getBySelector('select-users')
      .should('exist')
      .find('input')
      .click()
      .type('{enter}');
    cy.getBySelector('add-participant-button').click();

    cy.wait('@postInvestigationUsers');
  });
});
