import { INVESTIGATION_LIST_ENDPOINT } from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { login } from 'support/data/authentication';

function goToInvestigationList() {
  cy.getBySelector('experiments-nav').click();
}

function getTable() {
  return cy.getBySelector('investigation-table').should('exist');
}

function checkTableIsNotEmpty() {
  getTable().find('tr').should('have.length.greaterThan', 1);
}

describe('Investigation list', () => {
  beforeEach(() => {
    login();
    stubAPICall({
      endpoint: INVESTIGATION_LIST_ENDPOINT,
      response: { fixture: 'investigationList/investigations.json' },
      alias: 'getInvestigations',
    });
    goToInvestigationList();
  });

  it('should display a non empty investigations list', () => {
    checkTableIsNotEmpty();
  });

  it('search should display the expected proposal', () => {
    goToInvestigationList();
    const searchName = 'tomato';
    stubAPICall({
      endpoint: INVESTIGATION_LIST_ENDPOINT,
      response: { fixture: 'investigationList/investigations_tomato.json' },
      queryParams: { search: searchName },
      alias: 'getTomatoInvestigations',
    });
    cy.getBySelector('search-investigations').type(searchName);
    cy.wait('@getTomatoInvestigations');
    getTable().contains(searchName).should('exist');
    checkTableIsNotEmpty();
  });
});
