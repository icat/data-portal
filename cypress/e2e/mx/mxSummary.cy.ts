import { DATASET_PARAMETERS_ENDPOINT } from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { login } from 'support/data/authentication';
import { goToInvestigation } from 'support/data/investigation';

const PARAMS = [
  'scanType',
  'SampleTrackingContainer_position',
  'SampleTrackingContainer_id',
  'MXAutoprocIntegrationScaling_overall_mean_I_over_sigI',
  'MXAutoprocIntegration_space_group',
];

describe('MX Summary', () => {
  beforeEach(() => {
    login('reader');
    goToInvestigation({
      canAccess: true,
      released: true,
      instrument: 'ID23-1',
      startDate: '2024-01-01T06:30:00.00+01:00',
    });

    PARAMS.forEach((param) => {
      stubAPICall({
        endpoint: DATASET_PARAMETERS_ENDPOINT,
        response: { fixture: `dataset/mx/parameter/${param}.json` },
        queryParams: {
          name: param,
        },
        alias: `getParameterValues${param}`,
      });
    });

    cy.getBySelector('mx-summary-button').click();
  });

  it('should call all the endpoints', () => {
    cy.wait(
      PARAMS.map((param) => {
        return `@getParameterValues${param}`;
      }),
    );
  });

  it('should show containers with samples inside', () => {
    cy.getBySelector('mx-investigation-summary-container')
      .should('exist')
      .each((container) => {
        cy.wrap(container).within(() => {
          cy.getBySelector('mx-sample-circle').should('exist');
        });
      });
  });

  it('should have a selector with draggable values', () => {
    cy.getBySelector('range-selector').within(() => {
      cy.getBySelector('range-selector-cursor-min')
        .should('exist')
        .then((min) => {
          const initialX = min[0].getBoundingClientRect().x;

          const dataTransfer = new DataTransfer();

          cy.wrap(min)
            .trigger('dragstart', { dataTransfer })
            .trigger('dragover', {
              dataTransfer,
              clientX: initialX + 100,
            })
            .trigger('drop', { dataTransfer })
            .then((newMin) => {
              const newX = newMin[0].getBoundingClientRect().x;
              cy.wrap(newX).should('not.equal', initialX);
            });
        });
    });
  });
});
