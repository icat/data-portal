export const API_SERVER_URL = Cypress.env('CYPRESS_ICATPLUS_URL');

export const API_SERVER_HOSTNAME = new URL(API_SERVER_URL).hostname;
export const API_SERVER_PORT = new URL(API_SERVER_URL).port;
export const API_SERVER_HTTPS = new URL(API_SERVER_URL).protocol === 'https:';
