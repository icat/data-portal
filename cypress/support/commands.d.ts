declare namespace Cypress {
  interface Chainable {
    getBySelector(
      testIdAttribute: string,
      args?: any
    ): Chainable<JQuery<HTMLElement>>;
    getBySelectorLike(
      testIdPrefixAttribute: string,
      args?: any
    ): Chainable<JQuery<HTMLElement>>;
    selectorExists(
      testIdAttribute: string,
      args?: any
    ): Chainable<JQuery<HTMLElement>>;
    selectorNotExists(
      testIdAttribute: string,
      args?: any
    ): Chainable<JQuery<HTMLElement>>;
  }
}
