import {
  EWOKS_JOB_LIST_ENDPOINT,
  GET_ADDRESSES_ENDPOINT,
  INSTRUMENT_LIST_ENDPOINT,
  INVESTIGATION_LIST_ENDPOINT,
  PARCEL_LIST_ENDPOINT,
  USER_INSTRUMENT_LIST_ENDPOINT,
  USER_LIST_ENDPOINT,
  USER_LIST_PRINCIPAL_INVESTIGATOR_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';
import { setupInvestigation } from 'support/data/investigation';

export function setupGenericData() {
  setupInvestigation({});

  stubAPICall({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
    response: { fixture: 'generic/instruments.json' },
    alias: 'getInstrumentList',
  });

  stubAPICall({
    endpoint: USER_INSTRUMENT_LIST_ENDPOINT,
    response: { fixture: 'generic/instruments.json' },
    alias: 'getUserInstrumentList',
  });

  stubAPICall({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    alias: 'getEwoksJobList',
  });

  stubAPICall({
    endpoint: PARCEL_LIST_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    alias: 'getParcelList',
  });

  stubAPICall({
    endpoint: USER_LIST_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    alias: 'getUserList',
  });

  stubAPICall({
    endpoint: USER_LIST_ENDPOINT,
    response: { fixture: 'generic/users.json' },
    alias: 'getUserList',
  });

  stubAPICall({
    endpoint: USER_LIST_PRINCIPAL_INVESTIGATOR_ENDPOINT,
    response: { fixture: 'generic/users.json' },
    alias: 'getUserListPI',
  });

  stubAPICall({
    endpoint: GET_ADDRESSES_ENDPOINT,
    response: { fixture: 'logistics/address/multipleAddress.json' },
    alias: 'getUserAddress',
  });
}
