import {
  DATASET_LIST_ENDPOINT,
  DATASET_PARAMETERS_ENDPOINT,
  DATASET_PARAMETER_VALUE_ENDPOINT,
  DMP_QUESTIONNAIRES_ENDPOINT,
  EWOKS_JOB_LIST_ENDPOINT,
  GET_MESSAGES_ENDPOINT,
  INFORMATION_MESSAGE,
  INVESTIGATION_ADDRESSES_ENDPOINT,
  INVESTIGATION_LIST_ENDPOINT,
  INVESTIGATION_USERS_LIST_ENDPOINT,
  SAMPLE_LIST_ENDPOINT,
  SHIPMENT_LIST_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { stubAPICall } from 'support/api/stubAPICall';

const START = '2000-01-01T06:30:00.00+01:00';
const END_NOT_RELEASED = '2099-01-01T06:30:00.00+01:00';
const END_RELEASED = '2010-01-01T06:30:00.00+01:00';

const INVESTIGATION_ID = 1;

export type InvestigationSetup = {
  released?: boolean;
  instrument?: string;
  canAccess?: boolean;
  canPrepare?: boolean;
  startDate?: string;
};

export function setupInvestigation({
  released = false,
  instrument = 'ID00',
  canAccess = true,
  canPrepare = false,
  startDate = START,
}: InvestigationSetup) {
  const investigationName = instrument + '-0000';
  const users = [
    {
      id: 123,
      role: 'Principal investigator',
      user: {
        name: 'principal_investigator',
        fullName: 'principal_investigator',
      },
      fullName: 'principal_investigator',
      name: 'principal_investigator',
      investigationId: INVESTIGATION_ID,
      affiliation: null,
      email: 'test@email.com',
      familyName: 'principal_investigator',
      givenName: 'principal_investigator',
      investigationName,
      orcidId: '0000-0000-0000-0000',
    },
  ];
  stubAPICall({
    endpoint: INVESTIGATION_LIST_ENDPOINT,
    response: {
      body: [
        {
          canAccessDatasets: canAccess,
          name: investigationName,
          startDate: startDate,
          endDate: released ? END_RELEASED : END_NOT_RELEASED,
          id: 1,
          doi: 'TEST-DOI/123456789',
          title: 'Investigation for automated testing',
          visitId: instrument + '-1',
          releaseDate: released ? END_RELEASED : END_NOT_RELEASED,
          summary: 'Investigation for automated testing',
          parameters: {
            __fileCount: '23711',
            __volume: '159747254867',
            __datasetCount: '173',
            __sampleCount: '7',
            __elapsedTime: '0',
            __acquisitionDatasetCount: '43',
            __processedDatasetCount: '130',
            __processedFileCount: '510',
            __acquisitionVolume: '156992136670',
            __processedVolume: '2755118197',
            __acquisitionFileCount: '23201',
            __archivedDatasetCount: '37',
            __archivedRawDatasetCount: '37',
            __archivedProcessedDatasetCount: '0',
          },
          instrument: {
            name: instrument,
            id: INVESTIGATION_ID,
            instrumentScientists: [
              {
                name: 'instrument_scientist',
              },
            ],
          },
          investigationUsers: users,
          meta: {
            page: {
              totalWithoutFilters: 1,
              total: 1,
              totalPages: 1,
              currentPage: 1,
            },
          },
          type: {
            id: 1,
            createId: 'root',
            createTime: startDate,
            modId: 'root',
            modTime: startDate,
            investigations: [],
            name: instrument,
          },
        },
      ],
    },
    alias: 'getInvestigation',
  });
  stubAPICall({
    endpoint: INVESTIGATION_USERS_LIST_ENDPOINT,
    response: { body: users },
    pathParams: { investigationId: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationUsers',
  });
  stubAPICall({
    endpoint: DATASET_PARAMETERS_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { investigationId: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationDatasetParams',
  });
  stubAPICall({
    endpoint: DATASET_PARAMETER_VALUE_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { investigationId: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationDatasetParamsValue',
  });
  stubAPICall({
    endpoint: SAMPLE_LIST_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { investigationId: INVESTIGATION_ID },
    alias: 'getInvestigationSamples',
  });
  stubAPICall({
    endpoint: SHIPMENT_LIST_ENDPOINT,
    response: canPrepare
      ? { fixture: 'logistics/shipment/mxshipment.json' }
      : {
          statusCode: 403,
        },
    queryParams: { investigationId: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationShipment',
  });
  stubAPICall({
    endpoint: EWOKS_JOB_LIST_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { investigationId: INVESTIGATION_ID },
    alias: 'getInvestigationJobs',
  });
  stubAPICall({
    endpoint: DMP_QUESTIONNAIRES_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { investigationId: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationDMPQuestionnaires',
  });
  stubAPICall({
    endpoint: DATASET_LIST_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { investigationIds: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationDatasets',
  });
  stubAPICall({
    endpoint: INVESTIGATION_ADDRESSES_ENDPOINT,
    response: { fixture: 'logistics/address/singleAddress.json' },
    pathParams: { investigationId: INVESTIGATION_ID.toString() },
    alias: 'getInvestigationAddress',
  });
  stubAPICall({
    endpoint: GET_MESSAGES_ENDPOINT,
    response: { fixture: 'emptyList.json' },
    queryParams: { type: INFORMATION_MESSAGE },
    alias: 'getMessages',
  });
}

export function goToInvestigation(setup: InvestigationSetup) {
  setupInvestigation(setup);
  if (setup.canAccess) {
    //go manually with navigation if can access
    cy.getBySelector('experiments-nav').click();
    cy.getBySelector('investigation-name-button').click();
  } else {
    //go directly if cannot access (user cannot navigate to that page but could have received a direct link)
    cy.visit(`/investigation/${INVESTIGATION_ID}/datasets`);
  }
}
