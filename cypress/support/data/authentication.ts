import {
  GET_USER_PREFERENCES_ENDPOINT,
  PUT_USER_PREFERENCES_ENDPOINT,
  SESSION_CREATE_ENDPOINT,
  SESSION_ENDPOINT,
  SESSION_REFRESH_ENDPOINT,
} from '@edata-portal/icat-plus-api';
import { blockAPICalls } from 'support/api/blockAPICalls';
import { setupGenericData } from 'support/data/setupGenericData';
import { stubAPICall } from 'support/api/stubAPICall';

export const LOGIN_ROLES = [
  'reader',
  'admin',
  'instrument_scientist',
  'principal_investigator',
] as const;
export type LoginRole = (typeof LOGIN_ROLES)[number];

export function login(role: LoginRole = 'reader') {
  blockAPICalls();
  setupLoginData();
  setupGenericData();

  stubAPICall({
    endpoint: GET_USER_PREFERENCES_ENDPOINT,
    response: {
      fixture: 'preferences/userpreferences.json',
    },
    alias: 'getUserPreferences',
  });

  stubAPICall({
    endpoint: PUT_USER_PREFERENCES_ENDPOINT,
    response: { fixture: 'preferences/userpreferences.json' },
    alias: 'putUserPreferences',
  });

  cy.visit('/');
  cy.wait('@readerCreateSession');

  if (role !== 'reader') {
    cy.getBySelector('login-nav').click();

    cy.getBySelector('login-tab-Database').click();

    cy.getBySelector('username-input').type(role);
    cy.getBySelector('password-input').type(role);
    cy.getBySelector('login-button').click();

    cy.wait(`@${role}CreateSession`);
    cy.getBySelector('user-nav').should('contain', role);
    cy.getBySelector('login-nav').should('not.exist');
  }
}

function setupLoginData() {
  LOGIN_ROLES.forEach((role) => {
    stubAPICall({
      endpoint: SESSION_CREATE_ENDPOINT,
      response: { fixture: `authentication/${role}_create.json` },
      alias: `${role}CreateSession`,
      matchBody: (body) => body.username === role,
    });

    stubAPICall({
      endpoint: SESSION_REFRESH_ENDPOINT,
      response: { fixture: `authentication/${role}_get.json` },
      alias: `${role}RefreshSession`,
      sessionId: `${role}_session`,
    });

    stubAPICall({
      endpoint: SESSION_ENDPOINT,
      response: { fixture: `authentication/${role}_get.json` },
      alias: `${role}GetSession`,
      sessionId: `${role}_session`,
    });
  });
}

export function logout() {
  cy.getBySelector('user-nav').click();
  cy.getBySelector('logout').click();

  cy.getBySelector('login-nav').should('exist');
}
