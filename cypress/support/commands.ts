/// <reference types="cypress" />
///<reference path="./commands.d.ts" />

// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//

Cypress.Commands.add('getBySelector', (selector, ...args) => {
  return cy.get(`[test-id=${selector}]`, ...args);
});

Cypress.Commands.add('getBySelectorLike', (selector, ...args) => {
  return cy.get(`[test-id*=${selector}]`, ...args);
});

Cypress.Commands.add('selectorExists', (selector, ...args) => {
  return cy.getBySelector(selector, ...args).should('exist');
});

Cypress.Commands.add('selectorNotExists', (selector, ...args) => {
  return cy.getBySelector(selector, ...args).should('not.exist');
});