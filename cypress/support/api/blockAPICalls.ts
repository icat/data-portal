import { API_SERVER_HOSTNAME, API_SERVER_HTTPS, API_SERVER_PORT } from 'config';

export function blockAPICalls() {
  cy.intercept(
    {
      hostname: API_SERVER_HOSTNAME,
      https: API_SERVER_HTTPS,
    },
    (req) => {
      if (new URL(req.url).port === API_SERVER_PORT) {
        throw new Error(
          `No mock provided for this API request: ${req.method} ${req.url}`,
        );
      }
      return;
    },
  ).as('blockedAPICall');
}
