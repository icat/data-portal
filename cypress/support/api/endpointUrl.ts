import type { EndpointDefinition } from '@edata-portal/icat-plus-api';

const URL_COMPONENT = '.+';

export function getEndpointPath<PARAMS>(
  endpoint: EndpointDefinition<any, PARAMS, any>,
  params?: Partial<PARAMS>,
  sessionId?: string,
) {
  const pathRegexp = replacePathParams(endpoint, params);

  const authenticationRegexp = endpoint.authenticated
    ? '/' + (sessionId ?? URL_COMPONENT)
    : '';

  const exp = `^/${endpoint.module}${authenticationRegexp}${pathRegexp}/?(\\?.+)?$`;

  return new RegExp(exp);
}

function replacePathParams<PARAMS>(
  endpoint: EndpointDefinition<any, PARAMS, any>,
  params?: Partial<PARAMS>,
) {
  const path = endpoint.path;
  if (!path?.length) return '';

  const pathParams = path.match(/:[a-zA-Z0-9]+/g);
  if (!pathParams) return path;

  const pathKeys = pathParams.map((param) => param.slice(1));

  const getReplacementForParam = (key: string) => {
    if (params && typeof params === 'object' && key in params)
      return (params as any)[key];
    return URL_COMPONENT;
  };

  return pathKeys.reduce((acc, key) => {
    const value = getReplacementForParam(key);

    return acc.replace(`:${key}`, value);
  }, path);
}
