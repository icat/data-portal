import type { EndpointDefinition } from '@edata-portal/icat-plus-api';
import { API_SERVER_HOSTNAME, API_SERVER_HTTPS, API_SERVER_PORT } from 'config';
import type { StaticResponse } from 'cypress/types/net-stubbing';
import { getEndpointPath } from 'support/api/endpointUrl';

export function stubAPICall<PARAMS>({
  endpoint,
  response,
  alias,
  pathParams,
  queryParams,
  matchBody,
  sessionId,
}: {
  endpoint: EndpointDefinition<any, PARAMS, any>;
  response: StaticResponse | undefined;
  alias: string;
  pathParams?: Partial<PARAMS>;
  queryParams?: Partial<PARAMS>;
  matchBody?: (body: any) => boolean;
  sessionId?: string;
}) {
  const url = getEndpointPath(endpoint, pathParams, sessionId);

  cy.intercept(
    {
      hostname: API_SERVER_HOSTNAME,
      https: API_SERVER_HTTPS,
      method: endpoint.method,
      path: url,
      query: convertParamsToString(queryParams),
    },
    (req) => {
      if (new URL(req.url).port !== API_SERVER_PORT) {
        return;
      }
      if (matchBody && !matchBody(req.body)) {
        return;
      }
      return req.reply(response);
    },
  ).as(alias);
}

function convertParamsToString(
  params: any | undefined,
): Record<string, string> | undefined {
  if (!params || !Object.keys(params).length) {
    return undefined;
  }
  return Object.entries(params).reduce(
    (acc, [key, value]) => {
      acc[key] = String(value);
      return acc;
    },
    {} as Record<string, string>,
  );
}
