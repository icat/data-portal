# Data portal

The Data Portal a web data catalogue frontend based on React framework.

This application provides:

- User management
- Logistics management and experiment preparation
- Scientific data browsing, visualization and download
- Electronic logbook
- Data reprocessing
- DOI minting

The data portal depends on:

- [ICAT+](https://icat.gitlab-pages.esrf.fr/icat-plus/) provides a REST API to access data from ICAT and other sources.
- [ICAT](https://icatproject.org) stores metadata and handles the user authentication mechanism.

See [architecture](architecture.md) for more details or [installation](installation.md) to get started.
