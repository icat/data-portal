# User guides

A selection of user guides are available under the `/help` page.

To extend this selection, you can add new guides in the `apps/portal/src/components/help/HowTo.tsx` file.

They are simply described by a object with the following properties:

- `title`: the title of the guide
- `description`: the steps description of the guide in html or text format
- `video`: an optional youtube video code to complement the guide
