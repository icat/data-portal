# Architecture

## Overview

To mitigate the increasing complexity coming from the diversity of scientific techniques used at ESRF and their specific requirements of visualization, the data-portal project is using a `micro-frontend` architecture.

This means that the application is composed of several independent applications (called micro-frontends) **that have their own dependencies and are loaded dynamically at runtime**.

This is achieved by using `module federation` through [vite](https://vitejs.dev/) and [vite-plugin-federation](https://github.com/originjs/vite-plugin-federation).

All the micro-frontends applications are located in the `apps` folder.

## The `portal` application

This is the main application of the data-portal project.

It is responsible for:

- Being the user's entry point
- Providing the global layout and structure of the application
- Managing the user's authentication
- Managing the application's configuration
- Defining all the available routes
- Loading the appropriate micro-frontends depending on what is being displayed
- Providing generic visualization component for datasets, samples and investigations that do not require technique-specific visualization
- Providing services usable by the other micro-frontends

Here are the main services implemented in portal and accessible to the other micro-frontends:

- access to the ICAT+ API
- access to the user's authentication information
- access to the application's configuration
- display of notifications
- selection of datasets and/or samples of interest
- insertion of technique-specific navigation options in the navigation menu
- display of datasets, samples and investigations using the appropriate visualization component

### Dataset viewers

The portal application is responsible for loading the appropriate dataset viewer depending on the technique of the dataset.
Viewers to be loaded are defined in `dataset.viewer.config.json` ([see configuration](configuration.md)).

### Sample viewers

On the same principle as dataset viewers, the portal application is responsible for loading the appropriate sample viewer depending on the technique of the sample.
Viewers to be loaded are defined in `sample.viewer.config.json` ([see configuration](configuration.md)).

### Investigation viewers

On the same principle as dataset viewers, the portal application is responsible for loading the appropriate investigation viewer depending on the technique of the investigation.
Viewers to be loaded are defined in `investigation.viewer.config.json` ([see configuration](configuration.md)).

## The `logistics` application

This application is responsible for:

- Parcel management
- Addresses management
- Samples, investigation plans and processing plans declaration

## The `logbook` application

This application is responsible for:

- Investigation logbook
- Beamline logbook

## The `cryoet` application

This application is responsible for:

- Cryo-ET dataset visualization
- Cryo-ET investigation visualization

## The `mx` application

This application is responsible for:

- MX dataset visualization
- MX sample visualization
- MX investigation visualization

## The `session-viewer` application

This application is responsible for:

- All other techniques investigation visualization

## The `dataset-viewer` application

This application is responsible for:

- All other techniques dataset visualization
