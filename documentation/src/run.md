# How to run

## In development mode

This assumes you have a running ICAT+ instance and already have installed the project (see [installation](installation.md)) and configured it (see [configuration](configuration.md)).

As detailed in the [architecture](architecture.md) section, the data-portal application is composed of several micro-frontends. This means that you will need to run several applications in parallel to have a fully working data-portal.

For each micro-frontend, you will need to run the following command:

```bash
cd apps/<micro-frontend>
pnpm dev
```

For example, to run the portal application:

```bash
cd apps/portal
pnpm dev
```

You will need to run at least the `portal` application as it is the entry point, and then all the apps you want to work with.

Alternatively, you can run all the micro-frontends at once using the following command:

```bash
# From the root of the project
pnpm dev
```

To verify your modifications, you can run the linters from the root of the project :

```bash
pnpm lint:tsc
pnpm lint:eslint
pnpm lint:prettier
```

## In production mode

To run the application in production mode, we advise you to use docker images.
You can build your own images using the provided [Dockerfile](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/docker/apps/Dockerfile?ref_type=heads) or have them built by the [CI](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/.gitlab/docker.yml?ref_type=heads).
The configuration of the application is embedded in the images, so you will need to build your own images if you want to change the configuration, this is done through docker build arguments using the variables detailed in the [configuration](configuration.md) section.
