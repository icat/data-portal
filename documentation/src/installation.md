# Installation

## Requirements

- pnpm ([installation instructions](https://pnpm.io/installation) - we advise you use the same version as currently used in the CI [here](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/.gitlab-ci.yml))
- A running ICAT+ instance ([installation instructions](https://icat.gitlab-pages.esrf.fr/icat-plus/installation/))

## Installation

### 1. Clone the repository

```bash
git clone git@gitlab.esrf.fr:icat/data-portal.git
#or
https://gitlab.esrf.fr/icat/data-portal.git
```

### 2. Install dependencies

```bash
cd data-portal
pnpm install
```

### 3. Configure the application

[See configuration](configuration.md)

### 4. Test the application

[See tests](tests.md)

### 5. Run the application

[See run](run.md)
