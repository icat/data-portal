# Configuration

The configuration of the data portal is done through json files.

The directory [apps/portal/config](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/apps/portal/config) contains different configuration presets.

Each of them has a set of a configuration files:

- `api.config.json` must match types defined in [packages/core/src/context/config/APIConfig.ts](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/packages/core/src/context/config/APIConfig.ts)
- `ui.config.json` must match types defined in [packages/core/src/context/config/UIConfig.ts](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/packages/core/src/context/config/UIConfig.ts)
- `techniques.config.json` must match types defined in [packages/core/src/context/config/TechniquesConfig.ts](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/packages/core/src/context/config/TechniquesConfig.ts)
- `remotes.config.json` must defines urls for all the micro-frontends
- `investigation.viewer.config.json` must match types defined in [apps/portal/src/components/viewers/InvestigationViewer.tsx](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/apps/portal/src/components/viewers/InvestigationViewer.tsx)
- `dataset.viewer.config.json` must match types defined in [apps/portal/src/components/viewers/DatasetViewer.tsx](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/apps/portal/src/components/viewers/DatasetViewer.tsx)
- `sample.viewer.config.json` must match types defined in [apps/portal/src/components/viewers/SampleViewer.tsx](https://gitlab.esrf.fr/icat/data-portal/-/blob/main/apps/portal/src/components/viewers/SampleViewer.tsx)

Please refer to the types and examples for more information on how to use them.

The app will be using the config present in `public/config` which is not version controlled. You can copy any of the presets there and modify it to suit your needs.

```bash
cp apps/portal/config/local/* apps/portal/public/config/
```

One of the configuration presets will be copied at install time if none is present. To choose which one, you can set the `DRAC_DEFAULT_CONFIG` env variable (default is `local`). This is useful when building the docker image for example:

```bash
docker build --build-arg APP="portal" --build-arg DRAC_DEFAULT_CONFIG="esrf" -f docker/apps/Dockerfile -t portal .
```

You can also mount the configuration directory as volume to change it when running the container:

```bash
docker run -v /path/to/your/config:/usr/share/nginx/html/config -p 80:80 portal
```

## Micro-frontends configuration

You need to configure the deployment URL for each of the micro-frontend apps. See [architecture](architecture.md) for more details on why this is required.

The `remotes.config.json` file must contain the URLs for each of the micro-frontends. The key must match the name of the micro-frontend and the value must be the URL where it is deployed.

The files `investigation.viewer.config.json`, `dataset.viewer.config.json` and `sample.viewer.config.json` contain the configuration for the viewers. They are used to define the components that will be used to display investigations, datasets and samples based on the data itself. They can use remote components (micro-frontends) or a generic local component. See the examples and types definitions for more information.

## Ewoks configuration

[Ewoks](https://ewoks.esrf.fr) is a project to automate data processing and experiments at large-scale facilities as well as making data processing more scientific (reproducible) and FAIR.

The data portal allows users to display the appropriate workflows based on the type of dataset, input workflow parameters, and launch workflows

### Worflow configuration

There is a built-in capability to automatically retrieve workflow descriptions from an EWOKS server instance and present the parameters to users as a web form.

In order to do so, the workflows must include specific metadata that enables the data portal to construct the form and associate each workflow with its corresponding dataset.

This metadata is splitted into keywords, input_schema and ui_schema

#### Keywords

The keywords allows to associate workflows to specific datasets based on their metadata and/or user's metadata.
The keywords are:

```
"keywords": {
      "Cardinality": [1, *] indicates whether the workflow is suitable for one or multiple datasets.
      "definition":  technique associated to the dataset
      "instrumentName": instrument associated to the dataset
      "scanType": scanType associated to the dataset
      "roles": the roles of the user allowed to run this workflow
    },
```

Example:
The following example shows the configuration of a workflow that will be available to the datasets of any technique from the beamline ID23-1 which scantype is "datacollection" or "integration" if and only if the user's role is "instrumentScientist" or "manager"

```
"keywords": {
      "cardinality": "1",
      "definition": "*",
      "instrumentName": "ID23-1",
      "scanType": ["datacollection", "integration"],
      "roles": ["instrumentScientist", "manager"]
    },
```

#### input_schema and ui_schema

The input and ui schema follows the [json-schema](https://json-schema.org/) convention however, it has been extended to populate values based on dataset parameters. A field named populate can be added to specify how the field should be populated.

The following example demonstrates a boolean field that will be populated with the value of the parameter MXAutoprocIntegration_anomalous. If the parameter does not exist, a default value will be applied (in this case, false).

```
 "input_schema": {
      "description": "Automatic processing of X-Ray diffraction images with prepopulate.",
      "properties": {
        "anomalous": {
          "description": "",
          "title": "Anomalous",
          "type" : "boolean",
          "default" : false,
          "populate": [
            {
              "type": "parameter",
              "value": "MXAutoprocIntegration_anomalous"
            }
          ]
        },
    }
 }
```
