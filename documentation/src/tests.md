# How to test

This app is tested with Cypress end to end tests. To run the tests, you need to have the app running locally. You can follow the instructions in the [How to run](run.md) section.

# Configuration

Before running the tests, you need to set the following environment variables:

- `CYPRESS_TEST_APP_URL`: The base URL of the app. This should be `http://localhost:3000` if you are running the app locally.
- `CYPRESS_ICATPLUS_URL`: The URL of the ICAT+ API that is used by the app. This is essential for mocking the API calls in the tests.

## Interactive mode

```bash
pnpm dev:test
```

This will open the Cypress test runner. You can then select the tests you want to run and see how they interacts with the app in a browser.

## Headless mode

```bash
pnpm run:test
```

This will run the tests in headless mode and output the results in the terminal.

## Add a new test

To add a new test, you can create a new file in the `cypress/e2e` folder. You can then write your test using the [Cypress API](https://docs.cypress.io/api/introduction/api.html).

All calls to the ICAT+ API should be blocked with `blockAPICalls` and mocked using the `stubAPICall` function using fixtures in `cypress/fixtures`. See `cypress/e2e/investigationList.cy.ts` for an example.
