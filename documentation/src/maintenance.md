# Maintenance

The app `apps/maintenance` is a simple maintenance page that can be used to display a message to users when the data-portal is down for maintenance.

A Docker image will be build by the CI for this app too. By deploying it, you can override the `portal` to use this app instead.
